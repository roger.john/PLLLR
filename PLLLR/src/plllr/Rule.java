/*
 * Rule.java
 *
 * Created on 8. Februar 2007, 16:26
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */
package plllr;

import bbtree.Utils.AsymmetricComparable;
import java.util.Collection;
import java.io.*;
import bbtree.Writeable;
import java.util.Comparator;

/**
 *
 * @author RJ
 */
public class Rule implements Comparable<Rule>, AsymmetricComparable<NonTerminal>,
                             Writeable, Serializable {

    private static final long serialVersionUID = 0L;
    public NonTerminal lhs;
    public Token[] rhs;
    public int id;
    public String javaCode;

    /** Creates a new instance of Rule */
    public Rule(NonTerminal lhs, Token[] rhs) {
        this(lhs, rhs, 0);
    }

    public Rule(NonTerminal lhs, Token[] rhs, int id) {
        this.lhs = lhs;
        this.rhs = rhs;
        this.id = id;
    }

    public Rule(NonTerminal lhs, Collection<? extends Token> rhs) {
        this.lhs = lhs;
        this.rhs = rhs != null ? rhs.toArray(new Token[rhs.size()]) : null;
    }

    public int getID() {
        return (id);
    }

    public int setID(int id) {
        int h = this.id;
        this.id = id;
        return (h);
    }

    public String getJavaCode() {
        return (javaCode);
    }

    public String setJavaCode(String javaCode) {
        String h = this.javaCode;
        this.javaCode = javaCode;
        return (h);
    }

    public NonTerminal getLeftHandSide() {
        return (lhs);
    }

    public NonTerminal setLeftHandSide(NonTerminal lhs) {
        NonTerminal h = this.lhs;
        this.lhs = lhs;
        return (h);
    }

    public Token[] getRightHandSide() {
        return (rhs);
    }

    public Token[] setRightHandSide(Token[] rhs) {
        Token[] h = this.rhs;
        this.rhs = rhs;
        return (h);
    }

    public Token[] setRightHandSide(Collection<? extends Token> rhs) {
        Token[] h = this.rhs;
        this.rhs = rhs != null ? rhs.toArray(new Token[rhs.size()]) : null;
        return (h);
    }

    public int rhsLength() {
        return (rhs == null ? 0 : rhs.length);
    }

    public String toString() {
        return toStringBuilder(null).toString();
    }

    public StringBuilder toStringBuilder(StringBuilder sb) {
        if(sb == null)
            sb = new StringBuilder();
        sb.append('(');
        boolean quot_open = false;
        if(lhs != null)
            sb.append(lhs.getName());
        sb.append(" = ");
        if(rhs != null)
            for(int i = 0; i < rhs.length; i++)
                if(rhs[i] != null) {
                    if(rhs[i] instanceof Terminal.Character) {
                        if(!quot_open) {
                            sb.append('\'');
                            quot_open = true;
                        }
                        sb.append(Terminal.Character.escapeCodePoint(((Terminal.Character)rhs[i]).
                                charValue()));
                    } else {
                        if(quot_open) {
                            sb.append('\'');
                            sb.append(' ');
                            quot_open = false;
                        }
                        if(rhs[i] instanceof NonTerminal)
                            sb.append(((NonTerminal)rhs[i]).getName());
                        else if(rhs[i] instanceof Terminal)
                            sb.append(rhs[i]);
                        else
                            sb.append("`").append(rhs[i]).append("´");
                    }
                    if((i < rhs.length - 1) && (!quot_open))
                        sb.append(' ');
                }
        if(quot_open) {
            sb.append('\'');
            quot_open = false;
        }
        return sb.append(" (").append(id).append("))");
    }

    public void writeToStream(Writer w) throws IOException {
        if(w == null)
            return;
        w.write('(');
        if(lhs != null)
            w.write(lhs.getName());
        w.write('=');
        if(rhs != null)
            for(int i = 0; i < rhs.length; i++)
                if(rhs[i] != null) {
                    if(rhs[i] instanceof Terminal.Character) {
                        w.write('\'');
                        w.write(Terminal.Character.escapeCodePoint(((Terminal.Character)rhs[i]).
                                charValue()));
                        w.write('\'');
                    } else
                        if(rhs[i] instanceof NonTerminal)
                            w.write(((NonTerminal)rhs[i]).getName());
                        else if(rhs[i] instanceof Terminal)
                            rhs[i].writeToStream(w);
                        else {
                            w.write('`');
                            rhs[i].writeToStream(w);
                            w.write('´');
                        }
                    if((i < rhs.length - 1))
                        w.write(' ');
                }
        w.write(',');
        w.write(Integer.toString(id));
        w.write(')');
    }

    public boolean equals(Object o) {
        if((o == null) || !(o instanceof Rule))
            return (false);
        return ((this == o) || (compareTo((Rule)o) == 0));
    }

    public int hashCode() {
        int h = lhs != null ? lhs.hashCode() : 0;
        for(int i = 0; i < rhs.length; i++)
            if(rhs[i] != null)
                h += rhs[i].hashCode();
        return (h);
    }

    public int compareTo(Rule x) {
        if(x == null)
            return (1);
        if(lhs == null)
            return (x.lhs == null ? 0 : -1);
        int c = lhs.compareTo(x.lhs);
        if(c != 0)
            return (c);
        Token[] rhs2 = x.rhs;
        if(rhs == null)
            return (rhs2 == null ? 0 : -1);
        if(rhs2 == null)
            return (1);
        int m = rhs.length < rhs2.length ? rhs.length : rhs2.length;
        for(int i = 0; i < m; i++)
            if(rhs[i] == null) {
                if(rhs2[i] != null)
                    return (-1);
            } else {
                c = rhs[i].compareTo(rhs2[i]);
                if(c != 0)
                    return (c);
            }
        return (rhs.length == rhs2.length ? (javaCode == null ? (x.javaCode == null ? 0 : -1) : javaCode.
                compareTo(x.javaCode))
                : rhs.length - rhs2.length);
    }

    public int asymmetricCompareTo(NonTerminal x) {
        if(lhs == null)
            return (x == null ? 0 : -1);
        if(x == null)
            return (1);
        return (lhs.compareTo(x));
    }

    public boolean isNullable() {
        return (isNullableFrom(0));
    }

    public boolean isNullableFrom(int pos) {
        if(rhs == null)
            return (true);
        if(pos >= rhs.length)
            return (true);
        for(int i = pos; i < rhs.length; i++) {
            if(rhs[i] == null)
                continue;
            if(rhs[i] instanceof Terminal) {
                if(rhs[i].equals(Terminal.Epsilon))
                    continue;
                return (false);
            }
            if(rhs[i] instanceof NonTerminal) {
                if(((NonTerminal)rhs[i]).isNullable())
                    continue;
                return (false);
            }
            return (false);
        }
        return (true);
    }
    public final static Comparator<Rule> IDComparator = new Comparator<Rule>() {

        public int compare(Rule x, Rule y) {
            if(x == null)
                return (y == null ? 0 : -1);
            if(y == null)
                return (1);
            return (x.id - y.id);
        }
    };
    public static final Rule DEFAULT_NULLING_RULE = new Rule(null, (Token[])null,
                                                             -1);
}
