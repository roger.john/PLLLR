/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package plllr;

/**
 *
 * @author rj
 */
public class ParseProgressPrinter extends Thread {

    private boolean mayRun = false;
    private final Object lock = new Object();
    private Parser parser;
    private long s, t1, t2;
    private int lcp, scp;

    public ParseProgressPrinter(Parser parser) {
        this.parser = parser;
    }

    public Parser getParser() {
        return parser;
    }

    public Parser setParser(Parser parser) {
        if(parser != this.parser) {
            if(mayRun)
                stopMonitoring();
            Parser old = this.parser;
            this.parser = parser;
            return old;
        } else
            return this.parser;
    }

    public void startMonitoring() {
        synchronized(lock) {
            if(mayRun)
                stopMonitoring();
            mayRun = true;
            start();
        }
    }

    public void stopMonitoring() {
        if(mayRun)
            synchronized(lock) {
                mayRun = false;
                lock.notifyAll();
                try {
                    lock.wait();
                } catch(InterruptedException e) {
                    throw new RuntimeException(
                            "progress printer was interrupted", e);
                }
            }
    }

    public void run() {
        synchronized(lock) {
            scp = lcp = parser.cp;
            s = System.nanoTime();
            t1 = s;
//            System.out.printf(
//                    "\rparse: %d symbols read (waiting)",
//                    scp);
            while(mayRun) {
                try {
                    lock.wait(1000L);
                } catch(InterruptedException e) {
                    throw new RuntimeException(
                            "progress printer was interrupted", e);
                }
                if(mayRun) {
                    t2 = System.nanoTime();
                    if(t2 - t1 >= 1000000000L) {
                        System.out.printf(
                                "\rparse: %d symbols read (%.1f symbols/s)    ",
                                parser.cp,
                                (parser.cp - lcp) * 1000000000. / (t2 - t1));
                        t1 = t2;
                        lcp = parser.cp;
                    }
                }
            }
            t2 = System.nanoTime();
            System.out.printf(
                    "\rparse: %d symbols read (%s%.1f symbols/s)    \r\n",
                    parser.cp, scp != 0 ? "~" : "",
                    (parser.cp - scp) * 1000000000. / (t2 - s));
            lock.notifyAll();
        }
    }
}
