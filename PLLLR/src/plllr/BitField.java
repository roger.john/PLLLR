/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package plllr;
import java.io.Serializable;
import java.util.*;

/**
 *
 * @author RJ
 */
public class BitField implements Cloneable,Serializable, Comparable<BitField> {

    private static final long serialVersionUID = 0L;

    private static final int sv = 5;
    private static final int smask = 31;
    private int[] bitWords;
    private static final int NOT  = 0;
    private static final int AND  = 1;
    private static final int OR   = 2;
    private static final int XOR  = 3;
    private static final int NAND = 4;
    private static final int NOR  = 5;
    private static final int EQ   = 6;
    
    private BitField() {
    }
    public BitField(int nBits) {
        bitWords = new int[calcBitWordCount(nBits)];
    }
    public <A extends Comparable<? super A>> BitField(Collection<? extends A> x,A[] a) {
        if(a!=null) {
            grow(a.length);
            addAll(x,a);
        }
    }
    private static int calcBitWordCount(int nBits) {
        if(nBits<0)
            throw(new IllegalArgumentException("number of bits must be non-negative"));
        int nWords = nBits>>sv;
        if((nBits&smask)!=0)
            nWords++;
        return(nWords);
    }
    private void grow(int nBits) {
        if(nBits<0)
            return;
        int k = calcBitWordCount(nBits);
        if((bitWords!=null)&&(k<=bitWords.length))
            return;
        int[] a = new int[k];
        if(bitWords!=null)
            System.arraycopy(bitWords,0,a,0,bitWords.length);
        bitWords = a;
    }
    public boolean equals(Object x) {
        if(this==x)
            return(true);
        else if(x==null)
            return(false);
        else if(x instanceof BitField)
            return(compareTo((BitField)x)==0);
        else
            return(false);
    }
    public int hashCode() {
        int h = 1234;
        if(bitWords==null)
            return(h);
        for (int i = bitWords.length-1;i>=0;i--)
            h ^= bitWords[i] * (i + 1);
        return(h);
    }
    public BitField clone() {
        if(bitWords==null)
            return(new BitField());
        BitField c = new BitField(bitWords.length<<sv);
        System.arraycopy(bitWords,0,c.bitWords,0,bitWords.length);
        return(c);
    }
    public int compareTo(BitField x) {
        if(x==null)
            return(1);
        int m = 0,c = 0,i;
        if(bitWords.length>x.bitWords.length) {
            m = x.bitWords.length;
            for(i=bitWords.length;i>=m;i--)
                if(bitWords[i]!=0)
                    return(1);
        } else if(bitWords.length<x.bitWords.length) {
            m = bitWords.length;
            for(i=x.bitWords.length;i>=m;i--)
                if(x.bitWords[i]!=0)
                    return(-1);
        }
        for(i=m-1;i>=0;i--)
            if((c = bitWords[i]-x.bitWords[i])!=0) {
                if(bitWords[i]<0) 
                    return(x.bitWords[i]<0?c:1);
                else
                    return(x.bitWords[i]<0?-1:c);

            }
        return(c);
    }
    public String toString() {
        if(bitWords==null)
            return("");
        StringBuilder sb = new StringBuilder();
        String t;
        int i,j;
        for(i=bitWords.length-1;i>=0;i--) {
            t = Integer.toHexString(bitWords[i]).toUpperCase();
            j = 8-t.length();
            while(j-->0)
                sb.append('0');
            sb.append(t);
        }
        return(sb.toString());
    }
    public static void op(int op,BitField src1,BitField src2,BitField dst) {
        if(src1==null)
            throw(new IllegalArgumentException("src1 is null"));
        if((src2==null)&&(op!=NOT))
            throw(new IllegalArgumentException("src2 is null"));
        if(dst==null)
            throw(new IllegalArgumentException("dst is null"));
        int k = src1.bitWords!=null?src1.bitWords.length:0,
            l = src2.bitWords!=null?src2.bitWords.length:0,
            m,n,i;
        int[] a,b,c;
        if(op==NOT) {
            a = src1.bitWords;
            b = null;
            n = k;
            m = 0;
        } else {
            if(k<=l) {
                a = src1.bitWords;
                b = src2.bitWords;
                m = k;
                n = l;
            } else {
                a = src2.bitWords;
                b = src1.bitWords;
                m = l;
                n = k;
            }
        }
        if((dst.bitWords==null)||(dst.bitWords.length<n))
            c = new int[n];
        else {
            c = dst.bitWords;
            for(i=c.length-1;i>=n;i--)
                c[i] = 0;
        }
        switch(op) {
            case NOT :  for(i=n-1;i>=0;i--)
                            c[i] = ~a[i];
                        break;
            case AND :  for(i=n-1;i>=m;i--)
                            c[i] = 0;
                        for(i=m-1;i>=0;i--)
                            c[i] = a[i] & b[i];
                        break;
            case OR  :  for(i=n-1;i>=m;i--)
                            c[i] = b[i];
                        for(i=m-1;i>=0;i--)
                            c[i] = a[i] | b[i];
                        break;
            case XOR :  for(i=n-1;i>=m;i--)
                            c[i] = b[i];
                        for(i=m-1;i>=0;i--)
                            c[i] = a[i] ^ b[i];
                        break;
            case NAND:  for(i=n-1;i>=m;i--)
                            c[i] = -1;
                        for(i=m-1;i>=0;i--)
                            c[i] = ~(a[i] & b[i]);
                        break;
            case NOR :  for(i=n-1;i>=m;i--)
                            c[i] = ~b[i];
                        for(i=m-1;i>=0;i--)
                            c[i] = ~(a[i] | b[i]);
                        break;
            case EQ  :  for(i=n-1;i>=m;i--)
                            c[i] = ~b[i];
                        for(i=m-1;i>=0;i--)
                            c[i] = ~(a[i] ^ b[i]);
                        break;
            default  :  throw(new IllegalArgumentException("invalid operation: "+op));
        }
        dst.bitWords = c;
    }
    public BitField not() {
        BitField r = new BitField();
        op(NOT,this,null,r);
        return(r);
    }
    public void notIP() {
        op(AND,this,null,this);
    }
    public BitField and(BitField x) {
        BitField r = new BitField();
        op(AND,this,x,r);
        return(r);
    }
    public void andIP(BitField x) {
        op(AND,this,x,this);
    }
    public BitField or(BitField x) {
        BitField r = new BitField();
        op(OR,this,x,r);
        return(r);
    }
    public void orIP(BitField x) {
        op(OR,this,x,this);
    }
    public BitField xor(BitField x) {
        BitField r = new BitField();
        op(XOR,this,x,r);
        return(r);
    }
    public void xorIP(BitField x) {
        op(XOR,this,x,this);
    }
    public BitField nand(BitField x) {
        BitField r = new BitField();
        op(NAND,this,x,r);
        return(r);
    }
    public void nandIP(BitField x) {
        op(NAND,this,x,this);
    }
    public BitField nor(BitField x) {
        BitField r = new BitField();
        op(NOR,this,x,r);
        return(r);
    }
    public void norIP(BitField x) {
        op(NOR,this,x,this);
    }
    public BitField eq(BitField x) {
        BitField r = new BitField();
        op(EQ,this,x,r);
        return(r);
    }
    public void eqIP(BitField x) {
        op(EQ,this,x,this);
    }
    public boolean isEmpty() {
        if(bitWords==null)
            return(true);
        for(int i=bitWords.length-1;i>=0;i--)
            if(bitWords[i]!=0)
                return(false);
        return(true);
    }
    public boolean contains(BitField x) {
        if((x==null)||(x.bitWords==null))
            return(true);
        if(bitWords==null)
            return(x.isEmpty());
        int m = x.bitWords.length,i;
        if(bitWords.length<x.bitWords.length) {
            m = bitWords.length;
            for(i=x.bitWords.length;i>=m;i--)
                if(x.bitWords[i]!=0)
                    return(false);
        }
        for(i=m-1;i>=0;i--)
            if((bitWords[i]&x.bitWords[i])!=x.bitWords[i])
                return(false);
        return(true);
    }
    public boolean isContainedIn(BitField x) {
        if(x==null)
            return(isEmpty());
        else
            return(x.contains(this));
    }
    public boolean intersects(BitField x) {
        if((x==null)||(x.bitWords==null))
            return(false);
        if(bitWords==null)
            return(false);
        int m = bitWords.length<=x.bitWords.length?bitWords.length:x.bitWords.length;
        for(int i=m-1;i>=0;i--)
            if((bitWords[i]&x.bitWords[i])!=0)
                return(true);
        return(false);
    }
    public boolean get(int id) {
        if((bitWords==null)||(id<0))
            return(false);
        int a = id>>sv,b=id&smask;
        if(a>=bitWords.length)
            return(false);
        return((bitWords[a]&(1<<b))!=0);
    }
    public boolean clear(int id) {
        return(set(id,false));
    }
    public boolean set(int id) {
        return(set(id,true));
    }
    public boolean set(int id,boolean v) {
        if((bitWords==null)||(id<0))
            return(false);
        int a = id>>sv,b=id&smask;
        if(a>=bitWords.length)
            grow(id);
        boolean r = ((bitWords[a]&(1<<b))!=0);
        if(v)
            bitWords[a] |= 1<<b;
        else
            bitWords[a] &= ~(1<<b);
        return(r);
    }
    public void flip(int id) {
        if((bitWords==null)||(id<0))
            return;
        int a = id>>sv,b=id&smask;
        if(a>=bitWords.length)
            grow(id);
        bitWords[a] ^= 1<<b;
    }
    public int nextBit(int startIndex,boolean v) {
        if(bitWords==null)
            return(-1);
        if(startIndex<0)
            startIndex = -1;
        int r = startIndex+1,i = r>>sv,j = (r&smask),k,w = v?1:0;
        if(i>=bitWords.length)
            return(-1);
        k = bitWords[i]>>>j;
        while(i<bitWords.length) {
            while((k!=0)&&((k&1)!=w)) {
                k >>>= 1;
                r++;
            }
            if((k&1)==w)
                return(r);
            k = bitWords[++i];
            r = i<<sv;
        }
        return(-1);
    }
    public int nextSetBit(int startIndex) {
        return(nextBit(startIndex,true));
    }
    public int nextClearBit(int startIndex) {
        return(nextBit(startIndex,false));
    }
    public int lowestBit(boolean value) {
        return(nextBit(-1,value));
    }
    public int lowestSetBit() {
        return(lowestBit(true));
    }
    public int lowestClearBit() {
        return(lowestBit(false));
    }
    public int prevBit(int startIndex,boolean v) {
        if(bitWords==null)
            return(-1);
        if(startIndex<0)
            startIndex = size();
        if(startIndex==0)
            return(-1);
        int r = startIndex-1,i = r>>sv,j = (r&smask),k;
        if(i>=bitWords.length)
            return(-1);
        k = bitWords[i]<<(smask-j);
        while(i>=0) {
            while((k!=0)&&((k<0)!=v)) {
                k <<= 1;
                r--;
            }
            if((k<0)==v)
                return(r);
            k = bitWords[++i];
            r = i<<sv+smask;
        }
        return(-1);
    }
    public int prevSetBit(int startIndex) {
        return(prevBit(startIndex,true));
    }
    public int prevClearBit(int startIndex) {
        return(prevBit(startIndex,false));
    }
    public int highestBit(boolean value) {
        return(prevBit(-1,value));
    }
    public int highestSetBit() {
        return(highestBit(true));
    }
    public int highestClearBit() {
        return(highestBit(false));
    }
    public int size() {
        return(bitWords==null?0:bitWords.length<<sv);
    }
    public int length() {
        return(highestSetBit()+1);
    }
    public int cardinality() {
        if(bitWords==null)
            return(0);
        int n = 0;
        for(int i=bitWords.length-1;i>=0;i--)
            n += Integer.bitCount(bitWords[i]);
        return(n);
    }
    public static <A  extends Comparable<? super A>> int getID(A x,A[] a) {
        if((x==null)||(a==null))
            return(-1);
        int id = Arrays.binarySearch(a,x);
        if(id<0)
            return(-1);
        else
            return(id);
    }
    public <A  extends Comparable<? super A>> boolean contains(A x,A[] a) {
        int id = getID(x,a);
        if(id<0)
            return(false);
        else
            return(get(id));
    }
    public <A  extends Comparable<? super A>> boolean add(A x,A[] a) {
        int id = getID(x,a);
        if(id<0)
            return(false);
        else
            return(!set(id));
    }
    public <A  extends Comparable<? super A>> boolean addAll(Collection<? extends A> x,A[] a) {
        if((x==null)||(a==null))
            return(false);
        boolean r = false;
        synchronized(x) {
            for(A y:x)
                r |= add(y,a);
        }
        return(r);
    }
    public <A  extends Comparable<? super A>> Collection<? super A> addTo(Collection<? super A> x,A[] a) {
        if(x==null)
            return(x);
        java.util.Iterator<A> it = iterate(a);
        while(it.hasNext())
            x.add(it.next());
        return(x);
    }
    public <A  extends Comparable<? super A>> boolean remove(A x,A[] a) {
        int id = getID(x,a);
        if(id<0)
            return(false);
        else
            return(clear(id));
    }
    public <A  extends Comparable<? super A>> boolean removeAll(Collection<? extends A> x,A[] a) {
        if((x==null)||(a==null))
            return(false);
        boolean r = false;
        synchronized(x) {
            for(A y:x)
                r |= remove(y,a);
        }
        return(r);
    }
    public <A  extends Comparable<? super A>> boolean retainAll(Collection<? extends A> x,A[] a) {
        if((x==null)||(a==null)||(bitWords==null))
            return(false);
        boolean r = false;
        for(int i=prevSetBit(a.length);i>=0;i = prevSetBit(i))
            if(!x.contains(a[i]))
                r |= clear(i);
        return(r);
    }
    public <A  extends Comparable<? super A>> java.util.Iterator<A> iterate(A[] b) {
        return(new Iterator<A>(this,b));
    }
    public static class Iterator<A> implements java.util.Iterator<A> {
        private BitField a;
        private A[] b;
        private int current;
        public Iterator(BitField a,A[] b) {
            if((b==null)||((a!=null)&&(a.length()>b.length)))
                throw(new IllegalArgumentException("null array"));
            if(a==null)
                current = -1;
            else
                current = a.nextSetBit(-1);
        }
        public boolean hasNext() {
            return(current!=-1);
        }
        public A next() {
            if(!hasNext())
                throw(new NoSuchElementException("no more elements to iterate"));
            A r = b[current];
            current = a.nextSetBit(current);
            if(current>=b.length)
                current = -1;
            return(r);
        }
        public void remove() {
            if(a==null)
                throw(new IllegalStateException("iteration has not yet started"));
            int id = a.prevSetBit(current);
            if(id==-1)
                throw(new IllegalStateException("iteration has not yet started"));
            a.clear(id);
        }
    }
}
