/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package plllr;

import bbtree.Path;

/**
 *
 * @author rj
 */
public class DynamicFixedThreadPoolExecutor implements ControllableExecutor {
    private final Worker[] threads;
    private volatile int status = 0;
    private final static int MAY_RUN    = 0x01;
    private final static int MAY_WORK   = 0x02;
    private final Path<Runnable> workQueue;
    private int waitingThreads = 0;
    public DynamicFixedThreadPoolExecutor(int numThreads) {
        this(numThreads,false);
    }
    public DynamicFixedThreadPoolExecutor(int numThreads,boolean daemon) {
        if(numThreads<0)
            throw(new IllegalArgumentException("Number of threads must be non-negative, was "+numThreads));
        threads = new Worker[numThreads];
        status |= MAY_RUN | MAY_WORK;
        workQueue = new Path<Runnable>();
        for(int i=0;i<threads.length;i++) {
            threads[i] = new Worker();
            if(daemon)
                threads[i].setDaemon(daemon);
            threads[i].start();
        }
    }
    public void execute(Runnable task) {
        if((status&MAY_RUN)==0)
            throw(new IllegalStateException("Service was shutdown"));
        if(task==null)
            throw(new NullPointerException("Task is null"));
        if(threads.length==0) {
            task.run();
            return;
        }
        synchronized(workQueue) {
            boolean ntfy = (workQueue.isEmpty()&&((status&MAY_WORK)!=0));
            workQueue.append(task);
            if(ntfy)
                workQueue.notifyAll();
        }
    }
    public void start() {
        if((status&MAY_RUN)==0)
            throw(new IllegalStateException("Service was shutdown"));
        if((status&MAY_WORK)!=0)
            throw(new IllegalStateException("Service was already started"));
        status |= MAY_WORK;
        synchronized(workQueue) {
            workQueue.notifyAll();
        }
    }
    public void stop() {
        if((status&MAY_RUN)==0)
            throw(new IllegalStateException("Service was shutdown"));
        if((status&MAY_WORK)==0)
            throw(new IllegalStateException("Service was already stopped"));
        status &= ~MAY_WORK;
    }
    public void shutDown() {
        if((status&MAY_RUN)==0)
            throw(new IllegalStateException("Service was already shutdown"));
        status = 0;
        synchronized(workQueue) {
           workQueue.notifyAll();
        }
    }
    public boolean isStarted() {
        return((status&MAY_WORK)!=0);
    }
    public boolean isShutDown() {
        return((status&MAY_RUN)==0);
    }
    public boolean isTerminated() {
        if((status&MAY_RUN)!=0)
            return(false);
        for(int i=0;i<threads.length;i++)
            if(threads[i].isAlive())
                return(false);
        return(true);
    }
    public int pendingTasks() {
        synchronized(workQueue) {
           return(workQueue.length());
        }
    }
    public int threadCount() {
        return(threads!=null?threads.length:0);
    }
    public void assertCompleted(String msg) {
        synchronized(workQueue) {
            for(int i=0;i<threads.length;i++)
                if(threads[i].getState()==Thread.State.RUNNABLE)
                    throw(new RuntimeException(msg));
        }
    }
    public void awaitCompletion() {
        synchronized(workQueue) {
            while(!workQueue.isEmpty()||(waitingThreads!=threads.length))
                try { workQueue.wait(); }
                catch(InterruptedException e) { throw new RuntimeException(
                        "The thread "+Thread.currentThread()+" was interrupted"); }
        }
    }
    public void awaitTermination() {
        if((status&MAY_RUN)!=0)
            throw(new IllegalStateException("Service was not shutdown yet"));
        for(int i=0;i<threads.length;i++)
            synchronized(threads[i]) {
                while(threads[i].isAlive())
                    try { threads[i].wait(); }
                    catch(InterruptedException e) { throw new RuntimeException(
                            "The thread "+Thread.currentThread()+" was interrupted"); }
            }
    }
    private class Worker extends Thread {
        long exTime = System.nanoTime();
        public synchronized void run() {
            try {
                Runnable task = null;
                while(true) {
                    task = getTask();
                    if(task==null)
                        break;
                    task.run();
                }
            } finally { notifyAll(); synchronized(workQueue) { workQueue.notifyAll(); } }
        }
        private Runnable getTask() {
            synchronized(workQueue) {
                exTime = System.nanoTime()-exTime;
                waitingThreads++;
                while(((status&MAY_RUN)!=0)&&
                        (((status&MAY_WORK)==0)||workQueue.isEmpty())) {
                    if((status&MAY_WORK)!=0)
                        workQueue.notifyAll();
                    try { workQueue.wait(); }
                    catch(InterruptedException e) {}
                }
                if((status&MAY_RUN)==0)
                    return(null);
                waitingThreads--;
                exTime = System.nanoTime();
                return(workQueue.pop());
            }
        }
    }
}
