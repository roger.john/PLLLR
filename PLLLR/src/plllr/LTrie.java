/*
 * Triek.java
 *
 * Created on 12. Februar 2007, 10:02
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package plllr;

import bbtree.Path;

/**
 *
 * @author RJ
 */
public class LTrie<A extends Comparable<? super A>> implements Cloneable {

    public EdgeNode<A> heads = null;

    /** Creates a new instance of Triek */
    public LTrie() {
    }
    public void check() {
        if(heads==null)
            return;
        EdgeNode<A> t = heads;
        A last = null;
        if(t.right==null)
            t = t.down;
        while(t!=null) {
            if(t.right==null)
                System.out.println("Triek.check: multiple or disoredered stop links at head detected");
            else {
                t.right.check();
                if(last==null)
                    last = t.right.x;
                else {
                    if(last.compareTo(t.right.x)>=0)
                        System.out.println("Triek.check: next-links not ordered at head ("+last+","+t.right.x+")");
                    last = t.right.x;

                }
            }
            t = t.down;
       }
    }
    public boolean containsPath(Path<A> y) {
        EdgeNode<A> t = heads;
        if((y==null)||(y.head==null)) {
            return((heads!=null)&&(heads.right==null));
        }
        Path.Node<A> u = y.head;
        while(u!=null) {
            if((t!=null)&&(t.right==null))
                t = t.down;
            while((t!=null)&&(t.right.x.compareTo(u.x)<0))
                t = t.down;
            if((t==null)||(t.right==null)||(t.right.x.compareTo(u.x)>0))
                return(false);
            t = t.right.nextNodes;
            u = u.next;
        }
        return((t==null)||(t.right==null));
    }
    public LTrie<A> insertPath(Path<A> y) {
        if((y==null)||(y.head==null)) {
            if((heads==null)||(heads.right!=null))
                heads = new EdgeNode<A>(null,heads);
            return(this);
        }
        EdgeNode<A> t = null;
        Path.Node<A> u = y.head;
        int c = -1;
        if((heads==null)||((heads.right!=null)&&((c=heads.right.x.compareTo(u.x))>0))) {
            heads = new EdgeNode<A>(new Node<A>(u.x,null),heads);
            t = heads;
        } else {
            t = heads;
            if(c<0)
                while((t.down!=null)&&(t.down.right.x.compareTo(u.x)<0))
                    t = t.down;
            if((c<0)&&((t.down==null)||(t.down.right.x.compareTo(u.x)>0))) {
                t.down = new EdgeNode<A>(new Node<A>(u.x,null),t.down);
                t = t.down;
            } else {
                if(c<0)
                    t = t.down;
                c = -1;
                if(u.next!=null) {
                    u = u.next;
                    while(u!=null) {
                        if((t.right.nextNodes==null)
                            ||((t.right.nextNodes.right!=null)&&((c=t.right.nextNodes.right.x.compareTo(u.x))>0))) {
                                if(t.right.nextNodes==null) {
                                    t.right.nextNodes = new EdgeNode<A>(null,
                                                                        new EdgeNode<A>(new Node<A>(u.x,null),null));
                                    t = t.right.nextNodes.down;
                                } else {
                                    t.right.nextNodes = new EdgeNode<A>(new Node<A>(u.x,null),t.right.nextNodes);
                                    t = t.right.nextNodes;
                                }
                                break;
                        }
                        t = t.right.nextNodes;
                        if(c<0) {
                            while((t.down!=null)&&(t.down.right.x.compareTo(u.x)<0))
                                t = t.down;
                            if((t.down==null)||(t.down.right.x.compareTo(u.x)>0)) {
                                t.down = new EdgeNode<A>(new Node<A>(u.x,null),t.down);
                                t = t.down;
                                break;
                            }
                            t = t.down;
                        }
                        c = -1;
                        if(u.next!=null)
                            u = u.next;
                        else break;
                    }
                }
            }
        }
        if((t!=null)&&(t.right.x.equals(u.x))) {
            u = u.next;
            while(u!=null) {
                t.right.nextNodes = new EdgeNode<A>(new Node<A>(u.x,null),null);
                t = t.right.nextNodes;
                u = u.next;
            }
            if((t.right.nextNodes!=null)&&(t.right.nextNodes.right!=null))
                t.right.nextNodes = new EdgeNode<A>(null,t.right.nextNodes);
        } else
            throw(new RuntimeException("Triek.insertPath: no nodes were inserted"));
//        if(!containsPath(y))
//            System.out.println("Error: path to be inserted couldn't be found: "+y);
        return(this);
    }
    public LTrie<A> deletePath(Path<A> y) {
        if(heads==null)
            return(this);
        EdgeNode<A> t = heads;
        Node<A> l = null;
        if((y==null)||(y.head==null)) {
            if((heads!=null)&&(heads.right==null))
                heads = heads.down;
             return(this);
        }
        A lx = y.head.x;
        Path.Node<A> u = y.head;
        while(u!=null) {
            if((t!=null)&&(t.right==null))
                t = t.down;
            while((t!=null)&&(t.right.x.compareTo(u.x)<0))
                t = t.down;
            if((t==null)||(t.right.x.compareTo(u.x)>0))
                return(this);
            if(u.next==null) {
                if((t.right.nextNodes!=null)&&(t.right.nextNodes.right!=null))
                    return(this);
                if(t.right.nextNodes!=null) //implicates t.right.nextNodes.right==null&&t.right.nextNodes.down!=null
                    {   l = t.right; lx = null; }
                break;
            } else if((t.right.nextNodes!=null)&&(
                    (t.right.nextNodes.right==null)
                    ||!t.right.nextNodes.right.x.equals(u.next.x)
                    ||((t.right.nextNodes.down!=null)&&!t.right.nextNodes.down.right.x.equals(u.next.x))))
                        {   l = t.right; lx = u.next.x; }
            t = t.right.nextNodes;
            u = u.next;
        }
        if(l!=null) {
            if((lx==null)||((l.nextNodes.right!=null)&&l.nextNodes.right.x.equals(lx)))
                l.nextNodes = l.nextNodes.down;
            else {
                t = l.nextNodes;
                while((t.down!=null)&&(t.down.right.x.compareTo(lx)<0))
                    t = t.down;
                if((t.down==null)||!t.down.right.x.equals(lx))
                    throw(new RuntimeException("Trie.deletePath: right-most to-be-conserved node is not Part of Path "+y));
                t.down = t.down.down;
                if((l.nextNodes.right==null)&&(l.nextNodes.down==null))
                    l.nextNodes = null;
            }
        } else {
            if((heads.right!=null)&&(heads.right.x.equals(y.head.x)))
                heads = heads.down;
            else {
                t = heads;
                while((t.down!=null)&&(t.down.right.x.compareTo(lx)<0))
                    t = t.down;
                if((t.down==null)||!t.down.right.x.equals(lx))
                    throw(new RuntimeException("Trie.deletePath: right-most to-be-conserved node does is not Part of Path "+y));
                t.down = t.down.down;
            }
        }
//        if(containsPath(y))
//            System.out.println("Error: path to be deleted is still there: "+y);
        return(this);
    }
    public LTrie<A> clone() {
        LTrie<A> r = new LTrie<A>();
        if(heads!=null)
            r.heads = heads.clone();
        return(r);
    }
    public String toString() {
        if(heads==null)
            return("{}");
        StringBuilder s = new StringBuilder().append("{");
        EdgeNode<A> t = heads;
        if(heads.right==null) {
            s.append("$Epsilon$");
            t = t.down;
            if(t!=null)
                s.append("|");
        }
        while(t!=null) {
            t.right.toString(s);
            t = t.down;
            if(t!=null)
                s.append("|");
            
        }
        return(s.append("}").toString());
    }
    public static class Node<A extends Comparable<? super A>> implements Cloneable {
        public A x;
        public EdgeNode<A> nextNodes;
        public Node(A x,EdgeNode<A> nextNodes) {
            this.x = x;
            this.nextNodes = nextNodes;
        }
        public Node<A> clone() {
            return(new Node<A>(x,nextNodes!=null?nextNodes.clone():null));
        }
        public void finalize() {
            //System.out.println("LTrie.Node "+this+" ("+x+","+nextNodes+") is being finalized.");
        }    
        public String toString() {
            return(toString(new StringBuilder()).toString());
        }
        public StringBuilder toString(StringBuilder s) {
            if(s==null)
                return(null);
            if(nextNodes==null)
                return(s.append(x.toString()));
            EdgeNode<A> t = nextNodes;
            s.append(x);
            if(nextNodes.down!=null)
                s.append("(");
            if(nextNodes.right==null) {
                t = t.down;
                if(t!=null)
                    s.append("$Epsilon$|");
            }
            while(t!=null) {
                t.right.toString(s);
                t = t.down;
                if(t!=null)
                    s.append("|");
            }
            if(nextNodes.down!=null)
                s.append(")");
            return(s);
        }
        public void check() {
            if(x==null)
                System.out.println("Trie.check: null entry in node detected"+this+")");
            EdgeNode<A> t = nextNodes;
            A last = null;
            if(nextNodes==null)
                return;
            if(t.right==null) {
                t = t.down;
                if(t==null)
                    System.out.println("Trie.check: old stop-link detected("+this+")");
            }
            while(t!=null) {
                if(t.right==null)
                    System.out.println("Trie.check: mulitple or disordered stop links detected "+this+")");
                else {
                    t.right.check();
                    if(last==null)
                        last = t.right.x;
                    else {
                        if(last.compareTo(t.right.x)>=0)
                            System.out.println("Trie.check: next-links not ordered ("+this+","+last+","+t.right.x+")");
                        last = t.right.x;

                    }
                }
                t = t.down;
           }
        }
    }
    public static class EdgeNode<A extends Comparable<? super A>> implements Cloneable {
        public Node<A> right;
        public EdgeNode<A> down;
        public EdgeNode(Node<A> right,EdgeNode<A> down) {
            this.right = right;
            this.down = down;
        }
        public EdgeNode<A> clone() {
            EdgeNode<A> r,s = new EdgeNode<A>(right!=null?right.clone():null,down);
            r = s;
            while(s.down!=null) {
                s.down = new EdgeNode<A>(s.down.right!=null?s.down.right.clone():null,s.down.down);
                s = s.down;
            }
            return(r);
        }
        public void finalize() {
            //System.out.println("LTrie.EdgeNode "+this+" ("+right+","+down+") is being finalized.");
        }    
    }
}
