/*
 * TerminalSet.java
 *
 * Created on 19. Oktober 2007, 12:22
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package plllr;
import bbtree.*;
import java.util.*;

/**
 *
 * @author RJ
 */
public class TerminalSet {
    OrderedSet<Terminal> s,sc;
    private int cows = 0;
    
    private static final int COPY_ON_WRITE_S  = 1;
    private static final int COPY_ON_WRITE_SC = 2;
    
    /** Creates a new instance of TerminalSet */
    public TerminalSet() {}
    public TerminalSet clone() {
        TerminalSet c = new TerminalSet();
        c.s  = s;
        c.sc = sc;
        c.cows |= (s!=null)?COPY_ON_WRITE_S:0;
        c.cows |= (sc!=null)?COPY_ON_WRITE_SC:0;
        return(c);
    }
    public TerminalSet makeFinal() {
        if((s!=null)&&!(s instanceof Final)) {
            s = new Final<Terminal>(s);
            cows |= COPY_ON_WRITE_S;
        }
        if((sc!=null)&&!(sc instanceof Final)) {
            sc = new Final<Terminal>(sc);
            cows |= COPY_ON_WRITE_SC;
        }
        return(this);
    }
    public boolean add(Terminal x) {
        if(x==null)
            return(false);
        if(x instanceof Terminal.CharacterClass) {
            if(sc==null) {
                sc = new IBBTree<Terminal>();
                return(sc.add(x));
            }
            if(sc.contains(x))
                return(false);
            if((cows&COPY_ON_WRITE_SC)!=0) {
//                sc = new BBTree<Terminal>((SortedSet<Terminal>)sc);
                sc = sc.clone();
                ((IBBTree<Terminal>)sc).check();
                cows &= ~COPY_ON_WRITE_SC;
            }
            return(sc.add(x));
        } else {
            if(s==null) {
                s = new IBBTree<Terminal>();
                return(s.add(x));
            }
            if(s.contains(x))
                return(false);
            if((cows&COPY_ON_WRITE_S)!=0) {
//                s = new BBTree<Terminal>((SortedSet<Terminal>)s);
                s = s.clone();
                ((IBBTree<Terminal>)s).check();
                cows &= ~COPY_ON_WRITE_S;
            }
            return(s.add(x));
        }
    }
    public boolean remove(Terminal x) {
        if(x==null)
            return(false);
        if(x instanceof Terminal.CharacterClass) {
            if(sc==null)
                return(false);
            if(!sc.contains(x))
                return(false);
            if((cows&COPY_ON_WRITE_SC)!=0) {
//                sc = new BBTree<Terminal>((SortedSet<Terminal>)sc);
                sc = sc.clone();
                ((IBBTree<Terminal>)sc).check();
                cows &= ~COPY_ON_WRITE_SC;
            }
            return(sc.remove(x));
        } else {
            if(s==null)
                return(false);
            if(!s.contains(x))
                return(false);
            if((cows&COPY_ON_WRITE_S)!=0) {
//                s = new BBTree<Terminal>((SortedSet<Terminal>)s);
                s = s.clone();
                ((IBBTree<Terminal>)s).check();
                cows &= ~COPY_ON_WRITE_S;
            }
            return(s.remove(x));
        }
    }
    public boolean addAll(OrderedSet<Terminal> other) {
        if((other==null)||other.isEmpty())
            return(false);
        boolean r = false;
        synchronized(other) {
            Iterator<Terminal> it = other.iterator();
            while(it.hasNext())
                r |= add(it.next());
        }
        return(r);
    }
    public boolean addAll(TerminalSet other) {
        if((other==null)||(other==this))
            return(false);
        boolean r = false;
        Terminal t;
        Iterator<Terminal> it;
        if((other.s!=this.s)&&(other.s!=null)&&!other.s.isEmpty()) {
            if(s==null)
                s = new IBBTree<Terminal>();
            synchronized(other.s) {
                it = other.s.iterator();
                while(it.hasNext()) {
                    t = it.next();
                    if(!s.contains(t)) {
                        if((cows&COPY_ON_WRITE_S)!=0) {
//                            s = new BBTree<Terminal>((SortedSet<Terminal>)s);
                            s = s.clone();
                            ((IBBTree<Terminal>)s).check();
                            cows &= ~COPY_ON_WRITE_S;
                        }
                        r |= s.add(t);
                    }
                }
            }
        }
        if((other.sc!=this.sc)&&(other.sc!=null)&&!other.sc.isEmpty()) {
            if(sc==null)
                sc = new IBBTree<Terminal>();
            synchronized(other.sc) {
                it = other.sc.iterator();
                while(it.hasNext()) {
                    t = it.next();
                    if(!sc.contains(t)) {
                        if((cows&COPY_ON_WRITE_SC)!=0) {
//                            sc = new BBTree<Terminal>((SortedSet<Terminal>)sc);
                            sc = sc.clone();
                            ((IBBTree<Terminal>)sc).check();
                            cows &= ~COPY_ON_WRITE_SC;
                        }
                        r |= sc.add(t);
                    }
                }
            }
        }
        return(r);
    }
    public boolean addAll(TerminalToRuleMap other) {
        if(other==null)
            return(false);
        boolean r = false;
        Terminal t;
        Iterator<TerminalToRuleMap.TEntry> it;
        if((other.s!=null)&&!other.s.isEmpty()) {
            if(s==null)
                s = new IBBTree<Terminal>();
            synchronized(other.s) {
                it = other.s.iterator();
                while(it.hasNext()) {
                    t = it.next().x;
                    if(!s.contains(t)) {
                        if((cows&COPY_ON_WRITE_S)!=0) {
//                            s = new BBTree<Terminal>((SortedSet<Terminal>)s);
                            s = s.clone();
                            ((IBBTree<Terminal>)s).check();
                            cows &= ~COPY_ON_WRITE_S;
                        }
                        r |= s.add(t);
                    }
                }
            }
        }
        if((other.sc!=null)&&!other.sc.isEmpty()) {
            if(sc==null)
                sc = new IBBTree<Terminal>();
            synchronized(other.sc) {
                it = other.sc.iterator();
                while(it.hasNext()) {
                    t = it.next().x;
                    if(!sc.contains(t)) {
                        if((cows&COPY_ON_WRITE_SC)!=0) {
//                            sc = new BBTree<Terminal>((SortedSet<Terminal>)sc);
                            sc = sc.clone();
                            ((IBBTree<Terminal>)sc).check();
                            cows &= ~COPY_ON_WRITE_SC;
                        }
                        r |= sc.add(t);
                    }
                }
            }
        }
        return(r);
    }
    public TerminalSet append(TerminalSet other) {
        if(other==null)
            return(this);
        if(contains(Terminal.Epsilon)) {
            remove(Terminal.Epsilon);
            addAll(other);
        }
        return(this);
    }
    public boolean contains(Terminal x) {
        if(x==null)
            return(false);
        if(x instanceof Terminal.CharacterClass) {
            if(sc==null)
                return(false);
            return(sc.contains(x));
        } else {
            if((s!=null)&&(s.contains(x)))
                return(true);
            if(sc!=null) {
                synchronized(sc) {
                    Iterator<Terminal> it = sc.iterator();
                    while(it.hasNext())
                        if(it.next().matches(x))
                            return(true);
                }
            }
            return(false);
        }
    }
    public Collection<? super Terminal> addTo(Collection<? super Terminal> x) {
        if(x==null)
            return(x);
        x.addAll(s);
        x.addAll(sc);
        return(x);
    }
    public String toString() {
        return("("+(s==null?"{}":s.toString())+","+(sc==null?"{}":sc.toString())+")");
    }
}
