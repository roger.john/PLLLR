/*
 * Grammar.java
 *
 * Created on 8. Februar 2007, 17:23
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */
package plllr;

import bbtree.Path;
import bbtree.*;
import java.io.*;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author RJ
 */
public class Grammar implements Writeable,Serializable {

    private static final long serialVersionUID = 0L;
    Traversable.Set<NonTerminal> N;
    Traversable.Set.Indexed<Rule> P;
    NonTerminal S;
    FirstKSetSupplier fkss;
    transient Class<? extends TerminalReader> lexer;
    public static final Selector<NonTerminal> NULLABLES = new Selector<NonTerminal>() {

        public boolean selects(NonTerminal x) {
            return (x.isNullable());
        }

        public NonTerminal lowerBound() {
            return (null);
        }

        public NonTerminal upperBound() {
            return (null);
        }
    };

    /** Creates a new instance of Grammar */
    Grammar() {
    }

    public static Grammar construct(Traversable.Set<NonTerminal> N,
                                    Traversable.Set.Indexed<Rule> P,
                                    NonTerminal S, String lexer) {
        return (construct(N, P, S, lexer, false, true));
    }

    public static Grammar construct(Traversable.Set<NonTerminal> N,
                                    Traversable.Set.Indexed<Rule> P,
                                    NonTerminal S, String lexer,
                                    boolean calcFirst1Sets) {
        return (construct(N, P, S, lexer, false, calcFirst1Sets));
    }

    static Grammar construct(Traversable.Set<NonTerminal> N,
                             Traversable.Set.Indexed<Rule> P,
                             NonTerminal S, String lexer, boolean compiled,
                             boolean calcFirst1Sets) {
        Grammar g = new Grammar();
        g.N = N;
        g.P = P;
        g.S = S;
        if(!compiled) {
            System.out.println("Compiling grammar...");
            g.calcNullableNonTerminals();
            g.removeNonTerminatingRulesAndSymbols();
            if(calcFirst1Sets)
                g.calcFirst1Sets();
//            if(P!=null) {
//                int i = 0;
//                for(Rule r:P)
//                    r.id = i++;
//            }
            System.out.println("Compilation done!");
        }
        g.distributeRules();
        g.setLexer(lexer);
        return g;
    }
    public void setLexer(String lexer) {
        if(lexer != null && !lexer.isEmpty())
            if(lexer.equalsIgnoreCase("byte"))
                this.lexer = TerminalReader.Byte.class;
            else if(lexer.equalsIgnoreCase("java"))
                this.lexer = TerminalReader.Java.class;
            else if(lexer.equalsIgnoreCase("c"))
                this.lexer = TerminalReader.C.class;
            else
                try {
                    this.lexer = Class.forName(lexer).asSubclass(
                            TerminalReader.class);
                } catch(ClassNotFoundException ex) {
                    throw new RuntimeException("tokenizer "+lexer+" not found",ex);
                } catch(ClassCastException ex) {
                    throw new RuntimeException("tokenizer "+lexer+" is not sub class of "+plllr.TerminalReader.class.getName(),ex);
                }
        else {
            System.out.println("tokenizer for grammar not set, assuming byte-tokenizer");
            this.lexer = TerminalReader.Byte.class;
        }
    }
    public Class<? extends TerminalReader> getLexer() {
        if(lexer==null) {
            System.out.println("tokenizer for grammar not set, assuming byte-tokenizer");
            lexer = TerminalReader.Byte.class;
        }
        return lexer;
    }
    void calcNullableNonTerminals() {
        if(P == null)
            return;
        boolean changed = true;
        int l = 0;
        while(changed) {
            changed = false;
            System.out.print("\rCalculating nullable symbols..."
                             + l + " round" + (l != 1 ? "s" : "") + " passed...");
            for(Rule r : P)
                if(r.isNullable() && !r.lhs.isNullable()) {
                    r.lhs.setNullingRule(r);
                    changed = true;
                }
            l++;
        }
        System.out.println("\rCalculating nullable symbols...done after "
                           + l + " round" + (l != 1 ? "s" : "") + ".");
    }

    void removeNonTerminatingRulesAndSymbols() {
        if((P == null) || (P.size() == 0)) {
            P = null;
            N = null;
            return;
        }
        boolean changed = true;
        int j, k, l = 0, o = N.size(), p = P.size(), dup_rules = 0;
        BBTree<Rule> tr = new BBTree<Rule>();
        BBTree<NonTerminal> tn = new BBTree<NonTerminal>();
        Path<Rule> PP = new Path<Rule>().append(P);
        Iterator<Rule> pi;
        Rule r;
        if(N != null)
            tn.addAll(N.subSet(NULLABLES));
        while(changed) {
            changed = false;
            System.out.print("\rCalculating useless symbols and rules..."
                             + l + " round" + (l != 1 ? "s" : "") + " passed...");
            pi = PP.iterator();
            while(pi.hasNext()) {
                r = pi.next();
                if(tr.contains(r)) {
                    dup_rules++;
                    pi.remove();
                    continue;
                }
                k = r.rhsLength();
                for(j = 0; j < k; j++) {
                    if(r.rhs[j] == null)
                        continue;
                    if(r.rhs[j] instanceof Terminal)
                        continue;
                    if(r.rhs[j] instanceof NonTerminal) {
                        if(tn.contains(r.rhs[j]))
                            continue;
                        break;
                    }
                    throw (new RuntimeException(
                            "pll.Grammar.removeNonTerminatingRulesAndSymbols: "
                            + r.rhs[j].toString() + " in Rule " + r + " is neither a Terminal nor a NonTerminal"));
                }
                if(j == k) {
                    tr.add(r);
                    pi.remove();
                    changed |= (r.lhs != null) && tn.add(r.lhs);
                }
            }
            l++;
        }
        o -= tn.size();
        N = N.without(tn);
        p = PP.length();
        System.out.println(
                "\rCalculating useless symbols and rules...done after "
                + l + " round" + (l != 1 ? "s" : "") + ", removed " + o + " symbol" + (o != 1 ? "s" : "")
                + (o != 0 ? N.toString() + " " : "") + " and "
                + p + " rule" + (p != 1 ? "s" : "") + (p != 0 ? PP.toString() + " " : "")
                + (dup_rules > 0 ? " with " + dup_rules + " duplicate" + (dup_rules != 1 ? "s" : "") : "") + ".");
//        if(tn.isEmpty())
//            N = null;
//        else
        N = tn.toFinal();
//        if(tr.isEmpty())
//            P = null;
//        else
        P = tr.toFinal();
    }

    void calcFirst1Sets() {
        if((N == null) || (P == null) || (N.size() == 0) || (P.size() == 0))
            return;
        int i, j, k, l = 0;
        boolean changed = true;
        Token[] t;
        NonTerminal u, v;
        for(NonTerminal w : N)
            if(w.firstSet == null)
                w.firstSet = new TerminalToRuleMap();
        while(changed) {
            changed = false;
            System.out.print(
                    "\rCalculating first-sets..." + l + " round" + (l != 1 ? "s" : "") + " passed...");
            for(Rule r : P) {
                if((r.rhsLength() == 0) || (r.lhs == null))
                    continue;
                j = 0;
                t = r.rhs;
                u = r.lhs;
                while(j < t.length) {
                    if((t[j] == null) || (t[j] == Terminal.Epsilon)) {
                        j++;
                        continue;
                    }
                    if(t[j] instanceof Terminal)
                        changed |= u.firstSet.addEntry((Terminal)(t[j]), r, j);
                    else if(t[j] instanceof NonTerminal) {
                        v = (NonTerminal)(t[j]);
                        if(!u.equals(v)) {
                            if(v.firstSet.s != null)
                                for(TerminalToRuleMap.TEntry x : v.firstSet.s)
                                    changed |= u.firstSet.addEntry(x.x, r, j);
                            if(v.firstSet.sc != null)
                                for(TerminalToRuleMap.TEntry x : v.firstSet.sc)
                                    changed |= u.firstSet.addEntry(x.x, r, j);
                        } else {
                            if(v.firstSet.s != null)
                                for(TerminalToRuleMap.TEntry x : v.firstSet.s)
                                    changed |= x.addRuleEntry(r, j);
                            if(v.firstSet.sc != null)
                                for(TerminalToRuleMap.TEntry x : v.firstSet.sc)
                                    changed |= x.addRuleEntry(r, j);
                        }
                    }
                    if((!(t[j] instanceof NonTerminal)) || (!((NonTerminal)(t[j])).
                            isNullable()))
                        break;
                    j++;
                }
            }
            l++;
        }
        for(NonTerminal w : N)
            w.firstSet.makeFinal();
        System.out.println(
                "\rCalculating first-sets...done after " + l + " round" + (l != 1 ? "s" : "") + ".");
    }

    void distributeRules() {
        if(P == null || P.isEmpty())
            return;
        if(!P.isFinal())
            P = P.toFinal();
        for(Rule r : P) {
            if(r.lhs == null)
                throw new RuntimeException(
                        "rule without left hand side detected: " + r);
            if(N == null || !N.contains(r.lhs)) {
                System.out.println(
                        "unregistered nonterminal detected, registering: " + r.lhs);
                if(N == null)
                    N = new BBTree<NonTerminal>();
                else if(N.isFinal())
                    N = N.clone();
                N.add(r.lhs);
            }
            if(r.lhs.rules == null || !r.lhs.rules.contains(r)) {
                if(r.lhs.rules == null)
                    r.lhs.rules = new IBBTree<Rule>();
                else if(r.lhs.rules.isFinal())
                    r.lhs.rules = r.lhs.rules.clone();
                r.lhs.rules.add(r);
            }
        }
        if(N != null) {
            if(!N.isFinal())
                N = N.toFinal();
            for(NonTerminal n : N)
                if(n.rules != null && !n.rules.isFinal())
                    n.rules = n.rules.toFinal();
        }
    }

    public Traversable.Set<NonTerminal> getNonTerminals() {
        return N.toFinal();
    }

    public Traversable.Set.Indexed<Rule> getRules() {
        return P.toFinal();
    }

    public NonTerminal getStartSymbol() {
        return S;
    }

    public String toString() {
        return toStringBuilder(null).toString();
    }

    public StringBuilder toStringBuilder(StringBuilder sb) {
        if(sb == null)
            sb = new StringBuilder();
        sb.append('(');
        if(N != null)
            N.toStringBuilder(sb);
        else
            sb.append("{}");
        sb.append(",{");
        if(P != null)
            P.toStringBuilder(sb);
        else
            sb.append("{}");
        sb.append(',');
        S.toStringBuilder(sb);
        return sb.append(')');
    }

    public void writeToStream(Writer w) throws IOException {
        if(w == null)
            return;
        int i = 0;
        ;
        w.write('(');
        if(N != null) {
            w.write('{');
            for(NonTerminal u : N) {
                if(i != 0)
                    w.write("\r\n,");
                else
                    i++;
                if(u != null)
                    u.writeToStream(w);
            }
            w.write('}');
        }
        w.write("\r\n,");
        i = 0;
        if(P != null) {
            w.write('{');
            for(Rule r : P) {
                if(i != 0)
                    w.write("\r\n,");
                else
                    i++;
                if(r != null)
                    r.writeToStream(w);
            }
            w.write('}');
        }
        w.write("\r\n,");
        if(S != null)
            w.write(S.getEscapedName());
        w.write(',');
        w.write(getLexer().getName());
        w.write(')');
    }

    public static Grammar readFromStream(Reader r) throws IOException {
        return (new GrammarReader().readFromStream(r));
    }

    public static TerminalSet first(Token t) {
        TerminalSet r = new TerminalSet();
        if(t instanceof Terminal)
            r.add((Terminal)t);
        else if(t instanceof NonTerminal)
            r.addAll(((NonTerminal)t).firstSet);
        return r;
    }

    public static TerminalSet first(Path<Token> p) {
        return (first(p == null ? null : p.head));
    }

    public static TerminalSet first(Path.Node<Token> tp) {
        TerminalSet r = new TerminalSet();
        r.add(Terminal.Epsilon);
        if(tp == null)
            return (r);
        Path.Node<Token> t = tp;
        while((t != null) && r.contains(Terminal.Epsilon)) {
            r.remove(Terminal.Epsilon);
            if(t.x instanceof Terminal)
                r.add((Terminal)(t.x));
            else if(t.x instanceof NonTerminal) {
                r.addAll(((NonTerminal)(t.x)).firstSet);
                if(((NonTerminal)(t.x)).isNullable())
                    r.add(Terminal.Epsilon);
            }
            t = t.next;
        }
        return (r);
    }

    public static boolean derivesOnlyEpsilon(NonTerminal d) {
        if(d == null || !d.isNullable() || d.firstSet == null)
            return false;
        return d.firstSet.isEmpty();
    }
    private void readObject(java.io.ObjectInputStream s)
            throws java.io.IOException, ClassNotFoundException {
        s.defaultReadObject();
        setLexer((String)s.readObject());
    }

    private void writeObject(java.io.ObjectOutputStream s)
            throws java.io.IOException, ClassNotFoundException {
        s.defaultWriteObject();
        s.writeObject(lexer!=null?lexer.getName():null);
    }

}
