/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package plllr;

import bbtree.BBTree;
import bbtree.Path;
import bbtree.Selector;
import bbtree.Traversable;
import bbtree.TraversalActionPerformer;
import bbtree.Utils.AsymmetricComparable;
import bbtree.Utils.KeyedElementFactory;
import java.io.*;
import plllr.Derivation.Item;

/**
 *
 * @author RJ
 */
public abstract class Parser {

    public static final String VERSION = "1.0";
    public boolean DEBUG = false;  //Gibt an, ob zwischen den Schritten U ausgegeben werden soll.
    protected Grammar G;
    protected Terminal ct;        //Das aktuelle Eingabe-Zeichen.
    protected int cp;             //Die aktuelle Position in der Eingabe.
    protected Path<Derivation.Item.NonTerminal> p;    //Speichert alle erfolgreichen Ableitungen der Eingabe aus der Grammatik.
    protected Traversable.Set<Terminal> expected;    //erwartetes Terminal zum fortführen eines parse-Aufrufs
    protected Derivation.Item csi;
    protected Traversable.Set<Derivation.Item> dcache;
    protected Traversable.Set<NonTerminalItemWithUsedRuleSet> ncache;
    protected Writer graphWriter;
    protected boolean useProductionLookAhead = true;
    protected boolean infixParsing = false;
    protected boolean useTransitiveEdges = false;
    public final IntCounter shifted_symbols = new IntCounter(),//Anzahl der der korrekt erkannten Terminale.
            derivation_nodes_created = new IntCounter(),//Anzahl der erzeugten SPPF-Knoten.
            derivation_edges_created = new IntCounter(),//Anzahl der erzeugten SPPF-Kanten.
            nodes_created = new IntCounter(),//Anzahl der erzeugten Knoten im GSS.
            shift_edges_created = new IntCounter(),//Anzahl der erzeugten Shift-Kanten im GSS.
            shift_edges_visited = new IntCounter(),//Anzahl der w�hrend der Reduktionen benutzten GSS-Shift-Kanten.
            reduce_edges_created = new IntCounter(),//Anzahl der erzeugten Reduce-Kanten im GSS.
            reduce_edges_visited = new IntCounter(),//Anzahl der w�hrend der Reduktionen benutzten GSS-Reduce-Kanten.
            transitive_edges_created = new IntCounter(),//Anzahl der erzeugten Transitions-Kanten im GSS.
            transitive_edges_visited = new IntCounter();//Anzahl der w�hrend der Reduktionen benutzten GSS-Transitions-Kanten.

    //F�r jedes Tupel (a,b) in den Kommandozeilen-Arugumenten wird eine Grammtik aus der Datei gelesen und parse-Vorgang mit der Datei b als Eingabe gestartet.
    //Anschie�end wird das Ergebnis mitsamt einem Tupel (<a>s,<b>n,<c>se,<d>re,<e>sv,<f>rv,<g>dnc,<h>dec) ausgegeben,
    // a in der a die ben�tigte Zeit, b die Zahl der erzeugten Knoten, c die Zahl der erzeugten Shift-Kanten,
    // d die Zahl der erzeugten Reduce-Kanten, e die Zahl der besuchten Shift-Kanten, f die Zahl der besuchten
    // Reduce-Kanten, g die Zahl der f�r den SPPF erzeugten Knoten und h die Zahl der f�r den SPPF erzeugten Kanten angibt.
    //Zus�tzlich wird aus dem gewonnen Ableitungsobjekt die Menge der damit abgeleiten W�rter ausgegeben.
    public static void main(String[] args) {
        GrammarReader r = new GrammarReader();
        Grammar g = null;
        NonTerminal S = null;
        TerminalReader tr;
        Parser p, ps[] = new Parser[7];
        long s, e;
        int i, j, k = 1, tc, nl, pl, numThreads = 0;
        boolean infix = false, debug = false, bf = true, derivGraph = true,
                parseGraph = false, epsilonGraph = true, searched_dot = false,
                found_dot = false, nodeItemLinks = true, topDown = false,
                genericLookAhead = false, productionLookAhead = true,
                reductionLookAhead = false, dynamicThreadPoolExecutor = true,
                useLeo = false, useTransitiveEdges = false;
        Writer parseGraphWriter = null;
        BufferedReader br;
        Process dot;
        String outputFormat = "", line, dotOptions = "";
        System.out.println("\r\nPLLLR v" + VERSION + "...:");
        Path<Derivation.Item.NonTerminal> d;
        Path.Node<Derivation.Item.NonTerminal> dn;
        ParseProgressPrinter ppp = new ParseProgressPrinter(null);
        ControllableExecutor es = null;
        for(i = 0; i < args.length; i++) {
            String x = args[i];
            if(x.startsWith("-v") || x.startsWith("-V"))
                continue;
            if(x.startsWith("-?") || x.startsWith("-h") || x.startsWith(
                    "--help")) {
                showHelp();
                continue;
            }
            if(x.startsWith("-c")) {
                try {
                    x = x.substring(2);
                    System.out.println();
                    System.out.println("Reading grammar: " + x + "...");
                    s = System.nanoTime();
                    g = r.readFromStream(new BufferedReader(new FileReader(x)),
                                         !genericLookAhead, es);
                    e = System.nanoTime();
                    nl = g.N.size();
                    pl = g.P.size();
                    System.out.println(nl + " symbol" + (nl != 1 ? "s" : "") + ", "
                                       + pl + " rule" + (pl != 1 ? "s" : "") + "("
                                       + (((e - s) / 1000) / 1000000.0) + "s).");
                    if(genericLookAhead) {
                        if((g.fkss == null) || (k != g.fkss.k))
                            g.fkss = new FirstKSetSupplier(g, k, es);
                        System.out.print("Writing first" + k + "-set to " + x
                                         + ".first" + k + "set...");
                        Writer w = new BufferedWriter(new FileWriter(
                                x + ".first" + k + "set"));
                        g.fkss.writeToStream(w);
                        w.flush();
                        w.close();
                        System.out.println("ok");
                    } else {
                        if(g.fkss != null)
                            g.calcFirst1Sets();
                        System.out.println(
                                "Writing compiled grammar to " + x + "c...");
                        Writer w = new BufferedWriter(new FileWriter(x + "c"));
                        g.writeToStream(w);
                        w.flush();
                        w.close();
                        System.out.println("ok");
                    }
                    if(epsilonGraph)
                        if(outputFormat.equals("") || !found_dot) {
                            System.out.print("Writing epsilon graph to "
                                             + x + ".eg.dot...");
                            Derivation.writeEpsilonGraph(g,
                                                         new BufferedWriter(new FileWriter(
                                    x + ".eg.dot")), null, nodeItemLinks,
                                                         topDown);
                            System.out.println("ok");
                        } else {
                            System.out.print("Writing epsilon graph to "
                                             + x + ".eg." + outputFormat + "...");
                            dot = Runtime.getRuntime().exec(
                                    "dot -T"
                                    + outputFormat + " -o" + x + ".eg." + outputFormat);
                            br = new BufferedReader(new InputStreamReader(dot.
                                    getErrorStream()));
                            Derivation.writeEpsilonGraph(g,
                                                         new BufferedWriter(new OutputStreamWriter(
                                    dot.getOutputStream())),
                                                         null, nodeItemLinks,
                                                         topDown);
                            line = br.readLine();
                            if(line != null) {
                                System.out.println("dot complains:");
                                while(line != null) {
                                    System.out.println("\t" + line);
                                    line = br.readLine();
                                }
                                br.close();
                                tc = dot.waitFor();
                                System.out.print("DerivationGraph: dot reports ");
                                if(tc == 0)
                                    System.out.println("no errors.");
                                else
                                    System.out.println("error " + tc);
                            } else {
                                br.close();
                                tc = dot.waitFor();
                                if(tc == 0)
                                    System.out.println("done.");
                                else
                                    System.out.println("error " + tc);
                            }
                        }
                } catch(IOException f) {
                    System.out.println("Error while reading grammar from " + x + ": " + f.
                            getMessage());
                    f.printStackTrace();
                } catch(InterruptedException f) {
                    System.out.println("Error while waiting for dot: " + f.
                            getMessage());
                }
                continue;
            }
            if(x.startsWith("-x")) {
                genericLookAhead = true;
                try {
                    x = x.substring(2);
                    System.out.println();
                    System.out.println("Reading grammar: " + x);
                    s = System.nanoTime();
                    g = r.readFromStream(new BufferedReader(new FileReader(x)),
                                         !genericLookAhead, es);
                    e = System.nanoTime();
                    nl = g.N.size();
                    pl = g.P.size();
                    System.out.println(nl + " symbol" + (nl != 1 ? "s" : "") + ", "
                                       + pl + " rule" + (pl != 1 ? "s" : "") + "("
                                       + (((e - s) / 1000) / 1000000.0) + "s).");
                    if(genericLookAhead) {
                        if((g.fkss == null) || (k != g.fkss.k))
                            g.fkss = new FirstKSetSupplier(g, k, es);
                        System.out.print("Writing expanded first" + k + "-set to " + x
                                         + ".xfirst" + k + "set...");
                        Writer w = new BufferedWriter(new FileWriter(
                                x + ".xfirst" + k + "set"));
                        g.fkss.writeExpandedToStream(w);
                        w.flush();
                        w.close();
                        System.out.println("ok");
                    } else {
                        if(g.fkss != null)
                            g.calcFirst1Sets();
                        System.out.println(
                                "Writing compiled grammar to " + x + "c...");
                        Writer w = new BufferedWriter(new FileWriter(x + "c"));
                        g.writeToStream(w);
                        w.flush();
                        w.close();
                        System.out.println("ok");
                    }
                    if(epsilonGraph)
                        if(outputFormat.equals("") || !found_dot) {
                            System.out.print("Writing epsilon graph to "
                                             + x + ".eg.dot...");
                            Derivation.writeEpsilonGraph(g,
                                                         new BufferedWriter(new FileWriter(
                                    x + ".eg.dot")), null, nodeItemLinks,
                                                         topDown);
                            System.out.println("ok");
                        } else {
                            System.out.print("Writing epsilon graph to "
                                             + x + ".eg." + outputFormat + "...");
                            dot = Runtime.getRuntime().exec(
                                    "dot" + (!dotOptions.isEmpty() ? " " + dotOptions : "")
                                    + (!outputFormat.isEmpty() ? " -T" + outputFormat : "")
                                    + " -o" + x + ".eg." + outputFormat);
                            br = new BufferedReader(new InputStreamReader(dot.
                                    getErrorStream()));
                            Derivation.writeEpsilonGraph(g,
                                                         new BufferedWriter(new OutputStreamWriter(
                                    dot.getOutputStream())),
                                                         null, nodeItemLinks,
                                                         topDown);
                            line = br.readLine();
                            if(line != null) {
                                System.out.println("dot complains:");
                                while(line != null) {
                                    System.out.println("\t" + line);
                                    line = br.readLine();
                                }
                                br.close();
                                tc = dot.waitFor();
                                System.out.print("DerivationGraph: dot reports ");
                                if(tc == 0)
                                    System.out.println("no errors.");
                                else
                                    System.out.println("error " + tc);
                            } else {
                                br.close();
                                tc = dot.waitFor();
                                if(tc == 0)
                                    System.out.println("done.");
                                else
                                    System.out.println("error " + tc);
                            }
                        }
                } catch(IOException f) {
                    System.out.println("Error while reading grammar from " + x + ": " + f.
                            getMessage());
                    f.printStackTrace();
                } catch(InterruptedException f) {
                    System.out.println("Error while waiting for dot: " + f.
                            getMessage());
                }
                continue;
            }
            if(x.startsWith("-g")) {
                x = x.substring(2);
                System.out.println("Reading grammar: " + x + "...");
                try {
                    s = System.nanoTime();
                    g = r.readFromStream(new BufferedReader(new FileReader(x)),
                                         !genericLookAhead, es);
                    e = System.nanoTime();
                    if(genericLookAhead) {
                        if((g.fkss == null) || (k != g.fkss.k))
                            g.fkss = new FirstKSetSupplier(g, k, es);
                    } else if(g.fkss != null)
                        g.calcFirst1Sets();
                    if(S!=null) {
                		NonTerminal n = g.N.get(S);
    					if (n != null) {
    						g.S = S = n;
    						System.out.println("Changing start symbol of grammar to " + S + ".");
    					} else
    						System.out.println("NonTerminal " + S + " is not contained in given grammar, ignoring.");
                    }
                    nl = g.N.size();
                    pl = g.P.size();
                    System.out.println(nl + " symbol" + (nl != 1 ? "s" : "") + ", "
                                       + pl + " rule" + (pl != 1 ? "s" : "") + "("
                                       + (((e - s) / 1000) / 1000000.0) + "s).");
                } catch(IOException f) {
                    System.out.println("error: " + f.getMessage());
                    f.printStackTrace();
                    g = null;
                }
                continue;
            }
			if (x.startsWith("-S")) {
				if (x.length() > 2) {
					S = new NonTerminal(x.substring(2));
					if (g != null) {
						NonTerminal n = g.N.get(S);
						if (n != null) {
							g.S = S = n;
							System.out.println("Changing start symbol of grammar to " + S + ".");
						} else
							System.out.println("NonTerminal " + S + " is not contained in given grammar, ignoring.");
					}
				} else
					S = null;
				continue;
			}
            if(x.equals("--pl")) {
                productionLookAhead = true;
                continue;
            }
            if(x.equals("--rl")) {
                reductionLookAhead = true;
                continue;
            }
            if(x.equals("--nopl")) {
                productionLookAhead = false;
                continue;
            }
            if(x.equals("--norl")) {
                reductionLookAhead = false;
                continue;
            }
            if(x.equals("--gl")) {
                genericLookAhead = true;
                continue;
            }
            if(x.equals("--sl")) {
                genericLookAhead = false;
                continue;
            }
            if(x.equals("--nodebug")) {
                debug = false;
                continue;
            }
            if(x.equals("--debug")) {
                debug = true;
                continue;
            }
            if(x.equals("--nodg")) {
                derivGraph = false;
                continue;
            }
            if(x.equals("--dg")) {
                derivGraph = true;
                continue;
            }
            if(x.equals("--noeg")) {
                epsilonGraph = false;
                continue;
            }
            if(x.equals("--eg")) {
                epsilonGraph = true;
                continue;
            }
            if(x.equals("--infix")) {
                infix = true;
                continue;
            }
            if(x.equals("--single")) {
                infix = false;
                continue;
            }
            if(x.equals("--NIL")) {
                nodeItemLinks = true;
                continue;
            }
            if(x.equals("--noNIL")) {
                nodeItemLinks = false;
                continue;
            }
            if(x.equals("--dgtd")) {
                topDown = true;
                continue;
            }
            if(x.equals("--dglr")) {
                topDown = false;
                continue;
            }
            if(x.equals("--leo")) {
                useLeo = true;
                continue;
            }
            if(x.equals("--plllr")) {
                useLeo = false;
                continue;
            }
            if(x.equals("--useTE")) {
                useTransitiveEdges = true;
                continue;
            }
            if(x.equals("--noTE")) {
                useTransitiveEdges = false;
                continue;
            }
            if(x.startsWith("-k")) {
                try {
                    k = Integer.parseInt(x.substring(2));
                } catch(NumberFormatException n) {
                    k = 1;
                }
                if(k < 0)
                    k = 0;
                if(k > 1)
                    genericLookAhead = true;
                if(k == 0) {
                    productionLookAhead = false;
                    reductionLookAhead = false;
                } else {
                    productionLookAhead = true;
                    reductionLookAhead = false;
                }
                continue;
            }
            if(x.startsWith("-t")) {
                try {
                    numThreads = Integer.parseInt(x.substring(2));
                } catch(NumberFormatException n) {
                    numThreads = 0;
                }
                if(numThreads < 0)
                    numThreads = 0;
                if(es != null) {
                    System.out.print("Stopping current thread pool: ");
                    es.shutDown();
                    while(!es.isTerminated()) {
                        es.awaitTermination();
                        System.out.print(".");
                    }
                    es = null;
                    System.out.println("done");
                }
                if(numThreads > 0) {
                    System.out.print(
                            "Starting new "
                            + (dynamicThreadPoolExecutor ? "dynamic" : "static")
                            + " thread pool with "
                            + numThreads + " thread" + (numThreads != 1 ? "s" : "") + ": ");
                    es = dynamicThreadPoolExecutor
                         ? new DynamicFixedThreadPoolExecutor(numThreads, true)
                         : new FixedThreadPoolExecutor(numThreads, true);
                    System.out.println("done");
                }
                if((g != null) && (g.fkss != null))
                    g.fkss.ex = es;
                continue;
            }
            if(x.equals("--nobf")) {
                bf = false;
                continue;
            }
            if(x.equals("--bf")) {
                bf = true;
                continue;
            }
            if(x.equals("--nopg")) {
                parseGraph = false;
                continue;
            }
            if(x.equals("--pg")) {
                parseGraph = true;
                continue;
            }
            if(x.equals("--dyex")) {
                dynamicThreadPoolExecutor = true;
                continue;
            }
            if(x.equals("--stex")) {
                dynamicThreadPoolExecutor = false;
                continue;
            }
            if(x.startsWith("-T")) {
                outputFormat = x.substring(2).toLowerCase();
                if(!(outputFormat.equals("") || searched_dot)) {
                    searched_dot = true;
                    try {
                        dot = Runtime.getRuntime().exec("dot -V");
                        br = new BufferedReader(new InputStreamReader(dot.
                                getErrorStream()));
                        System.out.println("Using graphviz dot:");
                        line = br.readLine();
                        while(line != null) {
                            System.out.println("\t" + line);
                            line = br.readLine();
                        }
                        br.close();
                        dot.waitFor();
                        found_dot = true;
                    } catch(Exception f) {
                        found_dot = false;
                    }
                    if(!found_dot)
                        System.out.println("Could not find graph viz dot.");
                }
                continue;
            }
            if(x.equals("--dotOptions"))
                if(++i < args.length) {
                    dotOptions = args[i];
                    continue;
                } else
                    System.out.println("Error: options for dot expected: " + x);
            if(x.startsWith("-")) {
                System.out.println("Error: unknown option: " + x);
                continue;
            }
            try {
                if(g == null) {
                    System.out.println(
                            "Error: no grammar specified for " + x + "!");
                    continue;
                }
                System.out.println();
                if(useLeo) {
                    if(ps[5] == null)
                        ps[5] = new Leo();
                    p = ps[5];
                } else if(!genericLookAhead)
                    if(!reductionLookAhead) {
                        if(ps[0] == null)
                            ps[0] = new ParserLR0();
                        p = ps[0];
                    } else if(bf) {
                        if(ps[2] == null)
                            ps[2] = new ParserLR1BF();
                        p = ps[2];
                    } else {
                        if(ps[1] == null)
                            ps[1] = new ParserLR1();
                        p = ps[1];
                    }
                else if(!reductionLookAhead) {
                    if(ps[3] == null)
                        ps[3] = new GLParserLR0();
                    p = ps[3];
                } else {
                    if(ps[4] == null)
                        ps[4] = new GLParserLRk();
                    p = ps[4];
                }
                if(k < 0)
                    throw (new RuntimeException("k is out of bounds: " + k));
                p.DEBUG = debug;
                p.useTransitiveEdges = useTransitiveEdges;
                p.shifted_symbols.set(0);
                p.derivation_nodes_created.set(0);
                p.derivation_edges_created.set(0);
                p.nodes_created.set(0);
                p.shift_edges_created.set(0);
                p.shift_edges_visited.set(0);
                p.reduce_edges_created.set(0);
                p.reduce_edges_visited.set(0);
                p.transitive_edges_created.set(0);
                p.transitive_edges_visited.set(0);
                tr = g.getLexer().getConstructor(Reader.class).newInstance(new FileReader(
                        x));
                System.gc();
                //System.out.println();
                System.out.println(
                        "Using " + (useLeo ? "Leo" : "PLLLR(" + k + ")"
                                                     + (useLeo ? "" : (genericLookAhead
                                                                       ? "(gl)(" + (productionLookAhead ? k : 0) + "," + (reductionLookAhead ? k : 0) + ")"
                                                                       : (reductionLookAhead && bf ? "(BF)" : "")
                                                                         + "(" + (productionLookAhead ? 1 : 0) + "," + (reductionLookAhead ? 1 : 0) + ")")))
                        + "-algorithm with " + tr.getClass().getSimpleName()
                        + "-tokenizer to parse" + " " + x + "...");
                if(parseGraph)
                    if(!p.generatesParseGraph()) {
                        System.out.println(
                                "Writing parse data to " + x + ".pg.txt...");
                        parseGraphWriter = new BufferedWriter(new FileWriter(
                                x + ".pg.txt"));
                        dot = null;
                    } else if(outputFormat.isEmpty() || !found_dot) {
                        System.out.println(
                                "Writing parse graph to " + x + ".pg.dot...");
                        parseGraphWriter = new BufferedWriter(new FileWriter(
                                x + ".pg.dot"));
                        dot = null;
                    } else {
                        System.out.println(
                                "Using dot to write parse graph to " + x + ".pg."
                                + outputFormat + "...");
                        dot = Runtime.getRuntime().exec(
                                "dot -Gpackmode=clust"
                                + (!dotOptions.isEmpty() ? " " + dotOptions : "")
                                + (!outputFormat.isEmpty() ? " -T" + outputFormat : "")
                                + " -o" + x + ".pg." + outputFormat);
                        parseGraphWriter = new BufferedWriter(new OutputStreamWriter(dot.
                                getOutputStream()));
                    }
                else {
                    parseGraphWriter = null;
                    dot = null;
                }
                ppp.setParser(p);
                s = System.nanoTime();
                ppp.startMonitoring();
                d = p.parse(tr, g, productionLookAhead, infix, parseGraphWriter);
                ppp.stopMonitoring();
                e = System.nanoTime();
                if(dot != null) {
                    System.out.print("Waiting for dot to render parse graph...");
                    br = new BufferedReader(new InputStreamReader(dot.
                            getErrorStream()));
                    line = br.readLine();
                    if(line != null) {
                        System.out.println("dot complains:");
                        while(line != null) {
                            System.out.println("\t" + line);
                            line = br.readLine();
                        }
                        br.close();
                        tc = dot.waitFor();
                        System.out.print("ParseGraph: dot reports ");
                        if(tc == 0)
                            System.out.println("no errors.");
                        else
                            System.out.println("error " + tc);
                    } else {
                        br.close();
                        tc = dot.waitFor();
                        if(tc == 0)
                            System.out.println("done.");
                        else
                            System.out.println("error " + tc);
                    }
                }
                if((d == null) || (d.head == null)) {
                    System.out.println(
                            "no derivation possible(" + (((e - s) / 1000) / 1000000.0)
                            + "s," + p.nodes_created + "n," + p.shift_edges_created + "se,"
                            + p.reduce_edges_created + "re," + p.transitive_edges_created + "te,"
                            + p.shift_edges_visited + "sv," + p.reduce_edges_visited + "rv,"
                            + p.transitive_edges_visited + "tv," + p.derivation_nodes_created + "dn,"
                            + p.derivation_edges_created + "de," + p.shifted_symbols + "sym)");
                    if(p.ct == null || p.ct == Terminal.End) {
                        tr.close();
                        System.out.println(
                                "unexpected end of input at position " + p.cp
                                + ", expected one of: "
                                + (genericLookAhead ? ((GLParser)p).expectedTrie : p.expected));
                    } else {
                        System.out.println(
                                "parse error at position " + p.cp + " in line " + tr.
                                getLineNumber() + ":" + tr.getCursorPosition()
                                + " : got " + (genericLookAhead ? ((GLParser)p).ctp : p.ct)
                                + ", expected one of: "
                                + (genericLookAhead ? ((GLParser)p).expectedTrie : p.expected));
                        if(tr instanceof TerminalReader.Byte)
                            System.out.println(((TerminalReader.Byte)tr).
                                    formatCurrentLineAndClose());
                        else
                            tr.close();
                    }
                } else {
                    tr.close();
                    System.out.println(
                            "input was accepted(" + (((e - s) / 1000) / 1000000.0)
                            + "s," + p.nodes_created + "n," + p.shift_edges_created + "se,"
                            + p.reduce_edges_created + "re," + p.transitive_edges_created + "te,"
                            + p.shift_edges_visited + "sv," + p.reduce_edges_visited + "rv,"
                            + p.transitive_edges_visited + "tv," + p.derivation_nodes_created + "dn,"
                            + p.derivation_edges_created + "de," + p.shifted_symbols + "sym)");
                    int[] c;
                    boolean amb, cyclic;
                    int o = d.length();
                    System.out.println(
                            o + " derivation graph" + (o == 1 ? " was" : "s were") + " generated:");
                    dn = d.head;
                    j = 0;
                    while(dn != null) {
                        if(dn.x == null)
                            System.out.println(
                                    "error: null entry in derivation list!");
                        else {
                            s = System.nanoTime();
                            c = dn.x.count();
                            //dn.x.unVisit();
                            amb = dn.x.isAmbiguous();
                            //dn.x.unVisit();
                            cyclic = dn.x.isCyclic();
                            //dn.x.unVisit();
                            e = System.nanoTime();
                            System.out.println(
                                    "(" + (((e - s) / 1000) / 1000000.0) + "s,"
                                    + ((NonTerminal)(dn.x.x)).getEscapedName()
                                    + "," + dn.x.lpos + "-" + dn.x.rpos + ","
                                    + c[0] + "ni," + c[1] + "ti," + c[2] + "re,"
                                    + c[3] + "dn," + c[4] + "ie," + (amb ? "ambiguous" : "unambiguous")
                                    + "," + (cyclic ? "cyclic" : "cylce-free") + ")");
                            if(derivGraph)
                                if((outputFormat.isEmpty() && dotOptions.isEmpty()) || !found_dot) {
                                    System.out.print(
                                            "Writing derivation graph to "
                                            + x + "." + dn.x.lpos + "-" + dn.x.rpos + ".dot...");
                                    Derivation.writeGraph(dn.x,
                                                          new BufferedWriter(
                                            new FileWriter(
                                            x + "." + dn.x.lpos + "-" + dn.x.rpos + ".dot")),
                                                          null, nodeItemLinks,
                                                          topDown);
                                    System.out.println("ok");
                                } else {
                                    System.out.print(
                                            "Writing derivation graph to "
                                            + x + "." + dn.x.lpos + "-" + dn.x.rpos + "." + outputFormat + "...");
                                    dot = Runtime.getRuntime().exec(
                                            "dot"
                                            + (!dotOptions.isEmpty() ? " " + dotOptions : "")
                                            + (!outputFormat.isEmpty() ? " -T" + outputFormat : "")
                                            + " -o" + x + "." + dn.x.lpos + "-" + dn.x.rpos + "." + outputFormat);
                                    br = new BufferedReader(new InputStreamReader(dot.
                                            getErrorStream()));
                                    Derivation.writeGraph(dn.x,
                                                          new BufferedWriter(new OutputStreamWriter(
                                            dot.getOutputStream())),
                                                          null,
                                                          nodeItemLinks,
                                                          topDown);
                                    line = br.readLine();
                                    if(line != null) {
                                        System.out.println("dot complains:");
                                        while(line != null) {
                                            System.out.println("\t" + line);
                                            line = br.readLine();
                                        }
                                        br.close();
                                        tc = dot.waitFor();
                                        System.out.print(
                                                "DerivationGraph: dot reports ");
                                        if(tc == 0)
                                            System.out.println("no errors.");
                                        else
                                            System.out.println("error " + tc);
                                    } else {
                                        br.close();
                                        tc = dot.waitFor();
                                        if(tc == 0)
                                            System.out.println("done.");
                                        else
                                            System.out.println("error " + tc);
                                    }
                                }
                        }
                        dn = dn.next;
                        j++;
                    }
                }
                if(p.DEBUG && (d != null) && (d.head != null)) {
                    dn = d.head;
                    while(dn != null) {
                        if(dn.x == null)
                            System.out.println("{}");
                        else {
                            StringWriter sw = new StringWriter();
                            dn.x.writeToStream(sw);
                            System.out.println(sw.toString());
                            System.out.println(dn.x.derive().toString());
                        }
                        dn = dn.next;
                    }
                }
            } catch(Exception f) {
                System.out.println("Error while parsing : " + f.getMessage());
                f.printStackTrace();
            }
        }
        if(es != null) {
            System.out.print("Stopping current thread pool: ");
            es.shutDown();
            while(!es.isTerminated()) {
                es.awaitTermination();
                System.out.print(".");
            }
            es = null;
            System.out.println("done");
        }
    }

    public static void showHelp() {
        System.out.println("usage: java -jar PLLLR.jar cmd*");
        System.out.println("where cmd is one of the following:");
        System.out.println(
                "\t<a>         : parse the file <a> using the previously specified grammar");
        System.out.println(
                "\t-g<a>       : use the grammar in file <a> to parse subsequent files");
        System.out.println(
                "\t-S<NT>      : change the start symbol for the current and subsequent grammars, empty string to use original");
        System.out.println(
                "\t-k<v>       : set look-ahead to <v> (default is 1)");
        System.out.println(
                "\t            ->only effective on subsequent grammar loads");
        System.out.println(
                "\t-c<a>       : read grammar from <a> and write compiled grammer to file");
        System.out.println(
                "\t-x<a>       : read grammar from <a> and write expanded first-set to file");
        System.out.println(
                "\t-T<f>       : if possible, use graphviz dot to convert derivation trees");
        System.out.println("\t            ->to the specified fromat <f>");
        System.out.println(
                "\t--dotOptions: if possible, use graphviz dot to process derivation trees");
        System.out.println(
                "\t            ->using the options specified in the following argument");
        System.out.println(
                "\t-t<v>       : use <v> worker thread(s) (default is 0=>no multithreading)");
        System.out.println("\t            ->affects first-sets calculation only");
//        System.out.println("\t--dyex      : use dynamic thread pool (default)");
//        System.out.println("\t--stex      : use static thread pool");
        System.out.println("\t--pl        : use production look-ahead (default)");
        System.out.println("\t--nopl      : don't use production look-ahead");
        System.out.println("\t--rl        : use reduction look-ahead");
        System.out.println(
                "\t--norl      : don't use reduction look-ahead (default)");
        System.out.println("\t--gl        : use generic look-ahead");
        System.out.println(
                "\t--sl        : use single-token look-ahead (default)");
        System.out.println(
                "\t--bf        : use bitfields for reduction look-ahead sets (default)");
        System.out.println(
                "\t--nobf      : use terminal sets for reduction look-ahead sets (slow)");
        System.out.println(
                "\t--eg        : produce epsilon graph when compiling grammars (default)");
        System.out.println("\t--noeg      : disable output of epsilon graph");
        System.out.println(
                "\t--dg        : enable output of derivation graphs (default)");
        System.out.println("\t--nodg      : disable output of derivation graphs");
        System.out.println("\t--pg        : enable output of parse graphs");
        System.out.println(
                "\t--nopg      : disable output of parse graphs (default)");
        System.out.println("\t--single    : disable infix-parsing (default)");
        System.out.println("\t--infix     : enable infix-parsing");
//        System.out.println(
//                "\t--NIL       : enable links from nodes to items in derivation trees");
//        System.out.println(
//                "\t--noNIL     : disable links from nodes to items in derivation trees");
        System.out.println("\t--dgtd      : draw derivation graph top-down");
        System.out.println(
                "\t--dglr      : draw derivation graph left-right (default)");
        System.out.println("\t--debug     : enable debug-mode");
        System.out.println("\t--nodebug   : disable debug-mode (default)");
        System.out.println("\t--useTE     : use transitive edges (EXPERIMENTAL)");
        System.out.println(
                "\t--noTE      : do not use transitive edges (default)");
        System.out.println("\t--leo       : use Leo's algorithm (EXPERIMENTAL)");
        System.out.println("\t--plllr     : use PLLLR-algorithm (default)");
        System.out.println("\t-?          : show this");
        System.out.println();
    }

    public Derivation.Item.NonTerminal parse(TerminalReader tr, Grammar G)
            throws IOException {
        Path<Derivation.Item.NonTerminal> t = parse(tr, G, true, false);
        if((t != null) && (t.head != null))
            return (t.head.x);
        else
            return (null);
    }

    public Derivation.Item.NonTerminal parse(TerminalReader tr, Grammar G,
                                             boolean useProductionLookAhead)
            throws IOException {
        Path<Derivation.Item.NonTerminal> t = parse(tr, G,
                                                    useProductionLookAhead,
                                                    false);
        if((t != null) && (t.head != null))
            return (t.head.x);
        else
            return (null);
    }

    public synchronized Path<Derivation.Item.NonTerminal> parse(
            TerminalReader tr,
            Grammar G,
            boolean useProductionLookAhead,
            boolean infix_parsing)
            throws IOException {
        return (parse(tr, G, useProductionLookAhead, infix_parsing, null));
    }

    public abstract Path<Derivation.Item.NonTerminal> parse(TerminalReader tr,
                                                            Grammar G,
                                                            boolean useProductionLookAhead,
                                                            boolean infix_parsing,
                                                            Writer gw) throws
            IOException;

    public abstract boolean generatesParseGraph();

    protected void initGraphWriter(Writer gw) throws IOException {
        graphWriter = gw;
        if(graphWriter == null)
            return;
        graphWriter.write("digraph parse");
        graphWriter.write(Derivation.toHexString(System.currentTimeMillis()));
        graphWriter.write(" {\r\n");
        graphWriter.write("\r\n");
        graphWriter.write("\trankdir=RL;\r\n");
        graphWriter.write("\r\n");
        graphWriter.write("\tnode");
        graphWriter.write(Derivation.toHexString(System.identityHashCode(null)));
        graphWriter.write(
                "_0[shape=point,style=filled,color=black,tooltip=\"stack_end\"]");
        graphWriter.write("\r\n");

    }

    protected void writeCurrentGraphCluster(Iterable<? extends Node> U) throws
            IOException {
        if(graphWriter == null)
            return;
        graphWriter.write("\tsubgraph cluster_");
        graphWriter.write(Integer.toString(cp));
        graphWriter.write(" {\r\n");
        for(Node n : U) {
            graphWriter.write("\t\tnode");
            graphWriter.write(Derivation.toHexString(System.identityHashCode(
                    n.tp != null ? n.tp.head : null)));
            graphWriter.write('_');
            graphWriter.write(Integer.toString(n.pos));
            graphWriter.write("[shape=box,label=\"");
            graphWriter.write(n.getLabel());
            if(n.isStartNode())
                graphWriter.write("\",color=green,style=filled];\r\n");
            else
                graphWriter.write("\"];\r\n");
            if(n.edges != null)
                for(Edge e : n.edges) {
                    graphWriter.write("\t\tnode");
                    graphWriter.write(Derivation.toHexString(System.
                            identityHashCode(n.tp != null ? n.tp.head : null)));
                    graphWriter.write('_');
                    graphWriter.write(Integer.toString(n.pos));
                    graphWriter.write("->node");
                    graphWriter.write(
                            Derivation.toHexString(
                            System.identityHashCode(
                            e.target != null && e.target.tp != null ? e.target.tp.head : null)));
                    graphWriter.write('_');
                    graphWriter.write(Integer.toString(
                            e.target != null ? e.target.pos : 0));
                    if(e instanceof Edge.Reduce) {
                        graphWriter.write("[label=\"R");
                        if(((Edge.Reduce)e).r != null)
                            graphWriter.write(Integer.toString(
                                    ((Edge.Reduce)e).r.id));
                        else
                            graphWriter.write('S');
                        graphWriter.write("\"]");
                    } else if(e instanceof Edge.Shift) {
                    } else if(e instanceof Edge.Transitive)
                        graphWriter.write("[style=dotted]");
                    else
                        throw (new RuntimeException("unknown edge-type: " + e));
                    graphWriter.write(";\r\n");
                }
        }
        graphWriter.write("\t}\r\n");
    }

    protected void writeTransitiveEdge(Node n, Edge.Transitive e) throws
            IOException {
        if(graphWriter == null || n == null || e == null || n.pos >= cp)
            return;
        graphWriter.write("\t\tnode");
        graphWriter.write(Derivation.toHexString(System.identityHashCode(
                n.tp != null ? n.tp.head : null)));
        graphWriter.write('_');
        graphWriter.write(Integer.toString(n.pos));
        graphWriter.write("->node");
        graphWriter.write(
                Derivation.toHexString(
                System.identityHashCode(
                e.target != null && e.target.tp != null ? e.target.tp.head : null)));
        graphWriter.write('_');
        graphWriter.write(Integer.toString(
                e.target != null ? e.target.pos : 0));
        graphWriter.write("[style=dotted]");
        graphWriter.write(";\r\n");
    }

    protected void finishGraphWriter() throws IOException {
        if(graphWriter == null)
            return;
        graphWriter.write("\r\n");
        graphWriter.write("}\r\n");
        graphWriter.flush();
        graphWriter.close();
    }
    protected static final TraversalActionPerformer<Node> FINALIZER = new TraversalActionPerformer<Node>() {

        public boolean perform(Node x, int order, Object o) {
            x.makeFinal();
            return (false);
        }
    };

    //Die Knoten des GSS
    protected static class Node implements Comparable<Node>,
                                           AsymmetricComparable<Object> {

        Path<Token> tp;                 //Die noch zu parsenden Token
        int pos;                        //Die Position, in der der Knoten erzeugt wurde
        int last_dpos;                  //Die Position des Eingabe-Terminals, nachdem dieser Knoten im reduce-Schritt benutzt wurde
        Derivation.DistributionNode last_der;   //Dier aktuell zu diesem Knoten assoziierte Verteilerknoten
        Traversable.Set<Edge> edges;    //Die von diesem Knoten ausgehenden Kanten
        Edge.Transitive transitiveEdge; //Die von diesem Knoten ausgehende transitive Kante, falls existent (es kann nur eine geben)

        public Node(Path<Token> tp, int pos) {
            this.tp = tp;
            this.pos = pos;
            this.last_dpos = pos - 1;
            this.last_der = null;
            edges = null;
        }

        public static Node makeStartNode(NonTerminal S, int pos) {
            if(S == null)
                return (null);
            Node r = new Node(new Path<Token>().append(S), pos);
            r.addEdge(Edge.StackEnd);
            return (r);
        }

        public boolean addEdge(Edge e) {
            if(e == null)
                return (false);
            if(edges == null)
                edges = new BBTree<Edge>();
            return (edges.add(e));
        }

        public Edge addOrGetEdge(Edge e) {
            if(e == null)
                return (null);
            if(edges == null)
                edges = new BBTree<Edge>();
            return (edges.addOrGet(e));
        }

        public Edge.Transitive getTransitiveEdge(Parser p) {
//			if(!isProducable()||!isShiftedQuasiComplete(p.f))
//				return null;
//			if(edges==null||edges.isEmpty())
//				return null;
//			Edge e = edges.first();
//			if(e instanceof Edge.Transitive) {
//				p.transitive_edges_visited.inc();
//				return (Edge.Transitive)e;
//			}
            return transitiveEdge;
        }

        public Edge.Transitive createOrGetTransitiveEdge(Parser p) {
            if(transitiveEdge != null)
                return transitiveEdge;
            if(edges == null || edges.isEmpty())
                return null;
            Edge e = edges.first();
            if(e instanceof Edge.Transitive)
//				p.transitive_edges_visited.inc();
//				return (Edge.Transitive)e;
                throw new RuntimeException(
                        "transition edge in adjacency set detected");
            if(edges.size() != 1)
                return null;
            Derivation.ReversedEntry.RuleOrEdgeEntry lEntry = null;
            Derivation.ReversedEntry.EntryWithRuleOrEdgePrevious cEntry = null;
            while(true) {
                Node u = e.target;
                if(e instanceof Edge.Reduce) {
                    p.reduce_edges_visited.inc();
                    if(u == null)
                        return null;
                    Edge.Transitive et = u.isShiftedQuasiComplete(p) ? u.
                            createOrGetTransitiveEdge(p) : null;
                    Derivation.ReversedEntry.RuleEntry rEntry =
                            new Derivation.ReversedEntry.RuleEntry(
                            ((Edge.Reduce)e).r);
                    if(lEntry == null)
                        lEntry = rEntry;
                    if(cEntry != null)
                        cEntry.setPreviousEntry(rEntry);
                    if(et != null)
                        rEntry.previousEntry = new Derivation.ReversedEntry.NonTerminalEntry(
                                ((Edge.Reduce)e).r.lhs, u.pos,
                                et.reversedDerivationPath);
                    p.derivation_edges_created.inc();
                    et = new Edge.Transitive(et != null ? et.target : u, lEntry);
                    if(transitiveEdge == null) {
                        transitiveEdge = et;
                        p.transitive_edges_created.inc();
                        try {
                            p.writeTransitiveEdge(this, et);
                        } catch(IOException i) {
                            throw new RuntimeException(
                                    "error while writing transitive edge", i);
                        }
                    } else
                        throw new RuntimeException(
                                "transitive edge silently created");
                    return et;
                } else if(e instanceof Edge.Shift) {
                    p.shift_edges_visited.inc();
                    if(u.transitiveEdge == null && (u.edges == null || u.edges.
                            size() != 1))
                        return null;
                    Derivation.ReversedEntry.EdgeEntry nEntry = new Derivation.ReversedEntry.EdgeEntry(
                            ((Edge.Shift)e).der);
                    p.derivation_edges_created.inc();
                    if(lEntry == null)
                        lEntry = nEntry;
                    if(cEntry != null)
                        cEntry.setPreviousEntry(nEntry);
                    cEntry = nEntry;
                    if(u.transitiveEdge != null) {
                        cEntry.setPreviousEntry(
                                u.transitiveEdge.reversedDerivationPath);
                        Edge.Transitive et = new Edge.Transitive(
                                u.transitiveEdge.target, lEntry);
                        et.reversedDerivationPath = lEntry;
                        if(transitiveEdge == null) {
                            transitiveEdge = et;
                            p.transitive_edges_created.inc();
                            try {
                                p.writeTransitiveEdge(this, et);
                            } catch(IOException i) {
                                throw new RuntimeException(
                                        "error while writing transitive edge", i);
                            }
                        } else
                            throw new RuntimeException(
                                    "transitive edge silently created");
                        return et;
                    }
                    e = u.edges.first();
                    if(e instanceof Edge.Transitive)
                        throw new RuntimeException(
                                "impossible edge discovered: " + u);
                } else
                    throw new RuntimeException(
                            "impossible edge discovered: " + u);
            }

        }

        public boolean isProducable() {
            return ((tp != null) && (tp.head != null) && (tp.head.x != null)
                    && (tp.head.x instanceof NonTerminal));
        }

        public boolean isEpsilonShiftable() {
            return ((tp != null) && (tp.head != null) && (tp.head.x != null)
                    && (tp.head.x instanceof NonTerminal)
                    && ((NonTerminal)(tp.head.x)).isNullable()
                    && (tp.head.next != null));
        }

        public boolean isShiftable() {
            return ((tp != null) && (tp.head != null) && (tp.head.x != null)
                    && (tp.head.x instanceof Terminal));
        }

        public boolean isShiftableWith(Terminal x) {
            if((tp == null) || (tp.head == null))
                return (false);
            if(tp.head.x == null)
                return (x == null);
            if(!(tp.head.x instanceof Terminal))
                return (false);
            return (((Terminal)(tp.head.x)).matches(x));
        }

        public boolean isStartNode() {
            return ((edges != null) && edges.contains(Edge.StackEnd));
        }

        public int compareTo(Node other) {
            if(other == null)
                return (1);
            int c = (tp == null) ? (other.tp == null) ? 0 : -1 : (other.tp == null) ? 1 : 0;
            if(c != 0)
                return (c);
            if(tp != null)
                c = tp.compareTo(other.tp);
            if(c != 0)
                return (c);
            return (pos - other.pos);
        }

        public int asymmetricCompareTo(Object x) {
            if(x == null)
                return (tp == null ? 0 : 1);
            if(x instanceof Path) {
                if(tp == null)
                    return (-1);
                Path<Token> otp = (Path<Token>)x;
                return (tp.compareTo(otp));
            }
            if(x instanceof Token[]) {
                if(tp == null)
                    return (-1);
                Token[] otp = (Token[])x;
                return (tp.asymmetricCompareTo(otp));
            }
            throw (new IllegalArgumentException(
                    "Argument is neiter path nor array of tokens"));
        }

        public void makeFinal() {
            if((edges != null) && !edges.isFinal())
                edges = edges.toFinal();
        }

        public boolean isReducable() {
            if(tp == null)
                return (true);
            Path.Node<Token> t = tp.head;
            while(t != null) {
                if(t.x != null) {
                    if((t.x instanceof Terminal) && (t.x != Terminal.Epsilon))
                        return (false);
                    if((t.x instanceof NonTerminal) && (!((NonTerminal)(t.x)).
                            isNullable()))
                        return (false);
                }
                t = t.next;
            }
            return (true);
        }

        public boolean isComplete() {
            return tp == null || tp.isEmpty();
        }

        public boolean isShiftedComplete() {
            if(isComplete())
                //throw new RuntimeException("node "+this+" is complete");
                return true;
            return tp.isSingle();
        }

        public boolean isShiftedQuasiComplete(Parser p) {
            return isQuasiComplete(p, true);
        }

        public boolean isQuasiComplete(Parser p) {
            return isQuasiComplete(p, false);
        }

        private boolean isQuasiComplete(Parser p, boolean shifted) {
            if(shifted ? isShiftedComplete() : isComplete())
                return true;
            if(p == null)
                return false;
            Path.Node<Token> t = tp.head;
            if(shifted)
                t = t.next;
            if(p instanceof GLParser) {
                GLParser glp = (GLParser)p;
                while(t != null) {
                    if(t.x == null || t.x == Terminal.Epsilon)
                        continue;
                    if(!(t.x instanceof NonTerminal))
                        return false;
                    NonTerminal d = (NonTerminal)t.x;
                    if(!d.isNullable() || !glp.f.derivesOnlyEpsilon(d))
                        return false;
                    t = t.next;
                }
            } else
                while(t != null) {
                    if(t.x == null || t.x == Terminal.Epsilon)
                        continue;
                    if(!(t.x instanceof NonTerminal))
                        return false;
                    NonTerminal d = (NonTerminal)t.x;
                    if(!d.isNullable() || d.firstSet == null)
                        return false;
                    t = t.next;
                    if(!d.firstSet.isEmpty())
                        return false;
                }
            return true;
        }

        public String toString() {
            StringBuilder sb = new StringBuilder();
            sb.append('(');
            if(tp != null) {
                Path.Node<Token> t = tp.head;
                while(t != null) {
                    if(t.x == null) {
                    } else if(t.x instanceof NonTerminal)
                        sb.append(((NonTerminal)(t.x)).getEscapedName());
                    else
                        sb.append(t.x.toString());
                    t = t.next;
                    if(t != null)
                        sb.append(' ');
                }
            }
            sb.append(',');
            sb.append(pos);
            sb.append(',');
            sb.append(edges != null ? edges.toString() : "{}");
            sb.append(')');
            return (sb.toString());
        }

        public String getLabel() {
            StringBuilder sb = new StringBuilder();
            sb.append('(');
            if(tp != null) {
                Path.Node<Token> t = tp.head;
                while(t != null) {
                    if(t.x == null) {
                    } else if(t.x instanceof NonTerminal)
                        sb.append(((NonTerminal)(t.x)).getEscapedName());
                    else
                        sb.append(Terminal.String.escape(t.x.toString()));
                    t = t.next;
                    if(t != null)
                        sb.append(' ');
                }
            }
            sb.append(',');
            sb.append(pos);
            sb.append(')');
            return (sb.toString());
        }

        public static class PFactory implements
                KeyedElementFactory<Node, Path<Token>> {

            Node last;
            Parser p;

            public PFactory init(Parser p) {
                if(p == null)
                    throw (new NullPointerException("Parser is null"));
                this.p = p;
                return (this);
            }

            public Node lastCreated() {
                return (last);
            }

            public void reset() {
                last = null;
            }

            public Node createElement(Path<Token> tp) {
                if(tp == null)
                    throw (new NullPointerException("Path is null"));
                last = new Node(tp, p.cp);
                return (last);
            }

            public static PFactory getThreadLocal(Parser p) {
                return (TLV.get().init(p));
            }
            private static final ThreadLocal<Node.PFactory> TLV =
                    new ThreadLocal<Node.PFactory>() {

                        protected Node.PFactory initialValue() {
                            return (new Node.PFactory());
                        }
                    };

            @Override
            public Path<Token> extractKey(Node x) {
                return x.tp;
            }
        }

        public static class AFactory implements
                KeyedElementFactory<Node, Token[]> {

            Node last;
            Parser p;

            public AFactory init(Parser p) {
                if(p == null)
                    throw (new NullPointerException("Parser is null"));
                this.p = p;
                return (this);
            }

            public Node lastCreated() {
                return (last);
            }

            public void reset() {
                last = null;
            }

            public Node createElement(Token[] tp) {
                if(tp == null)
                    throw (new NullPointerException("Path is null"));
                last = new Node(Path.valueOf(tp), p.cp);
                return (last);
            }

            public static AFactory getThreadLocal(Parser p) {
                return (TLV.get().init(p));
            }
            private static final ThreadLocal<Node.AFactory> TLV =
                    new ThreadLocal<Node.AFactory>() {

                        protected Node.AFactory initialValue() {
                            return (new Node.AFactory());
                        }
                    };

            @Override
            public Token[] extractKey(Node x) {
                return x.tp != null ? x.tp.toArray(new Token[0]) : null;
            }
        }
    }
//Die Kanten im GSS

    protected abstract static class Edge implements Comparable<Edge> {

        Node target;    //Das Ziel der Kante

        protected abstract int getTypeComparationId();	// Transitive < Shift < Reduce

        public String toString() {
            if(target == null)
                if((this instanceof Reduce) && (((Reduce)this).r == null) || (this instanceof Transitive))
                    return ("start");
                else
                    throw (new RuntimeException(
                            "pll.Parser.Edge.toString: illegal edge detected(1)"));
            if(target.tp == null)
                throw (new RuntimeException(
                        "pll.Parser.Edge.toString: illegal edge detected(2)"));
            if(target.tp.head == null)
                throw (new RuntimeException(
                        "pll.Parser.Edge.toString: illegal edge detected(3)"));
            if(target.tp.head.x == null)
                throw (new RuntimeException(
                        "pll.Parser.Edge.toString: illegal node detected(4)"));
            StringBuilder sb = new StringBuilder();
            if(this instanceof Shift)
                if(target.tp.head.x instanceof NonTerminal)
                    sb.append(((NonTerminal)target.tp.head.x).getEscapedName());
                else
                    sb.append(target.tp.head.x.toString());
            else {
                Path.Node<Token> t = target.tp.head;
                while(t != null) {
                    if(t.x instanceof NonTerminal)
                        sb.append(((NonTerminal)(t.x)).getEscapedName());
                    else
                        sb.append(t.x.toString());
                    t = t.next;
                    if(t != null)
                        sb.append(' ');
                }
            }
            sb.append('(');
            sb.append(target.pos);
            sb.append(',');
            if(this instanceof Reduce)
                sb.append('R').append(((Reduce)this).r.id);
            else if(this instanceof Shift)
                sb.append('S');
            else if(this instanceof Transitive)
                sb.append('T');
            else
                throw new RuntimeException(
                        "unknow edge type discovered: " + this);
            sb.append(')');
            return (sb.toString());
        }
        //Diese Kante entsteht durch einen shift (nicht unbedingt terminal)

        public static class Shift extends Edge {

            Derivation.Item der;    //speichert das bei dem Shift gewonnene Ableitungsobjekt

            public Shift(Node target, Derivation.Item der) {
                this.target = target;
                this.der = der;
            }

            public int compareTo(Edge other) {
                if(other == null)
                    return (1);
                int c = getTypeComparationId() - other.getTypeComparationId();
                if(c != 0)
                    return c;
                if(target == null)
                    return (other.target == null ? 0 : -1);
                return (target.compareTo(other.target));
            }

            protected final int getTypeComparationId() {
                return 1;
            }
        }
        //Diese Kante ensteht bei einem produce1LL0; sie entspricht in gewisser Weise dem rekursiven Abstieg

        public static class Reduce extends Edge {

            Rule r; //Die Regel, die bei dem produce1LL1 benutzt wurde

            public Reduce(Node target, Rule r) {
                this.target = target;
                this.r = r;
            }

            public int compareTo(Edge other) {
                if(other == null)
                    return (1);
                int c = getTypeComparationId() - other.getTypeComparationId();
                if(c != 0)
                    return c;
                if(r == null)
                    return (((Reduce)other).r == null ? 0 : -1);
                c = r.compareTo(((Reduce)other).r);
                if(c != 0)
                    return (c);
                if(target == null)
                    return (other.target == null ? 0 : -1);
                return (target.compareTo(other.target));
            }

            protected final int getTypeComparationId() {
                return 2;
            }
        }
        public static final Edge StackEnd = new Edge.Reduce(null, null);

        public static class Transitive extends Edge {

            Derivation.ReversedEntry.RuleOrEdgeEntry reversedDerivationPath;

            public Transitive(Node target,
                              Derivation.ReversedEntry.RuleOrEdgeEntry reversedDerivationPath) {
                this.target = target;
                this.reversedDerivationPath = reversedDerivationPath;
            }

            public int compareTo(Edge other) {
                if(other == null)
                    return (1);
                int c = getTypeComparationId() - other.getTypeComparationId();
                if(c != 0)
                    return c;
                if(target == null)
                    return (other.target == null ? 0 : -1);
                return (target.compareTo(other.target));
            }

            protected final int getTypeComparationId() {
                return 0;
            }
        }
    }
//Ausw�hler f�r bestimmte Arten von Knoten
    protected static final Selector<Node> PRODUCABLES = new Selector<Node>() {

        public boolean selects(Node x) {
            return (x.isProducable());


        }

        public Node lowerBound() {
            return (null);


        }

        public Node upperBound() {
            return (null);


        }
    };
    protected static final Selector<Node> SHIFTABLES = new Selector<Node>() {

        public boolean selects(Node x) {
            return (x.isShiftable());


        }

        public Node lowerBound() {
            return (null);


        }

        public Node upperBound() {
            return (null);


        }
    };
    protected static final Selector<Node> EPSILONSHIFTABLES = new Selector<Node>() {

        public boolean selects(Node x) {
            return (x.isEpsilonShiftable());


        }

        public Node lowerBound() {
            return (null);


        }

        public Node upperBound() {
            return (null);


        }
    };
    protected static final Selector<Node> REDUCIBLES = new Selector<Node>() {

        public boolean selects(Node x) {
            return (x.isReducable());


        }

        public Node lowerBound() {
            return (null);


        }

        public Node upperBound() {
            return (null);








        }
    };

    protected static class NonTerminalItemWithUsedRuleSet
            implements Comparable<NonTerminalItemWithUsedRuleSet>,
                       AsymmetricComparable<Derivation.Item.NonTerminal> {

        public final Derivation.Item.NonTerminal nti;
        public final BitField usedRules;

        public NonTerminalItemWithUsedRuleSet(Derivation.Item.NonTerminal nti) {
            if((nti == null) || (nti.x == null))
                throw (new NullPointerException("Nonterminal item is null"));
            this.nti = nti;
            NonTerminal n = (NonTerminal)nti.x;
            if(n.rules == null || n.rules.isEmpty())
                throw new NullPointerException("nonterminal has no rules: " + n);
            usedRules = new BitField(n.rules.size());
        }

        public int compareTo(NonTerminalItemWithUsedRuleSet x) {
            if(x == null)
                return (1);
            return (nti.compareTo(x.nti));
        }

        public int asymmetricCompareTo(Derivation.Item.NonTerminal nti) {
            if(nti == null)
                return (1);
            return (this.nti.compareTo(nti));
        }

        public boolean RuleIsUsed(Rule r) {
            if(r == null)
                return (false);
            if(!nti.x.equals(r.lhs))
                throw (new IllegalArgumentException(
                        "Rule " + r + " does not belong to nonterminal " + nti.x));
            int i = ((NonTerminal)nti.x).rules.indexOf(r);
            if(i < 0)
                throw (new IllegalArgumentException(
                        "Rule " + r + " was not found nonterminal"));
            return (usedRules.get(i));
        }

        public boolean useRule(Rule r) {
            if(r == null)
                return (false);
            if(!nti.x.equals(r.lhs))
                throw (new IllegalArgumentException(
                        "Rule " + r + " does not belong to nonterminal " + nti.x));
            int i = ((NonTerminal)nti.x).rules.indexOf(r);
            if(i < 0)
                throw (new IllegalArgumentException(
                        "Rule " + r + " was not found for nonterminal" + nti.x));
            synchronized(usedRules) {
                return (!usedRules.set(i, true));
            }
        }

        public static class Factory implements
                KeyedElementFactory<NonTerminalItemWithUsedRuleSet, Derivation.Item.NonTerminal> {

            NonTerminalItemWithUsedRuleSet last;
            Parser p;

            public Factory() {
            }

            public Factory init(Parser p) {
                if(p == null)
                    throw (new NullPointerException("Parser is null"));
                this.p = p;
                return this;
            }

            public NonTerminalItemWithUsedRuleSet lastCreated() {
                return (last);
            }

            public void reset() {
                last = null;
            }

            public NonTerminalItemWithUsedRuleSet createElement(
                    Derivation.Item.NonTerminal nti) {
                if(nti == null)
                    throw (new NullPointerException("Nonterminal item is null"));
                last = new NonTerminalItemWithUsedRuleSet(nti);
                return (last);
            }

            public static Factory getThreadLocal(Parser p) {
                return NUF.get().init(p);
            }
            public static final ThreadLocal<NonTerminalItemWithUsedRuleSet.Factory> NUF =
                    new ThreadLocal<NonTerminalItemWithUsedRuleSet.Factory>() {

                        protected NonTerminalItemWithUsedRuleSet.Factory initialValue() {
                            return (new NonTerminalItemWithUsedRuleSet.Factory());
                        }
                    };

            @Override
            public Item.NonTerminal extractKey(NonTerminalItemWithUsedRuleSet x) {
                return x.nti;
            }
        }
    }
}
