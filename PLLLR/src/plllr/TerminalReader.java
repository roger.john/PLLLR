/*
 * TokenReader.java
 *
 * Created on 9. Oktober 2007, 11:46
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */
package plllr;

import bbtree.OrderedSet;
import bbtree.Final;
import java.io.*;

/**
 * One of read() or read(Terminal[],int,int) has to be overwritten!
 * @author RJ
 */
public abstract class TerminalReader implements Closeable {

    public static class LineBufferedCursorPositionReader extends BufferedReader {

        private StringBuilder current_line;
        private String last_line;
        private int line_num = 1, cursor_pos;
        private int markedLineNumber = 1, markedCursorPos;
        private boolean skipLF;
        private boolean markedSkipLF;

        public LineBufferedCursorPositionReader(Reader in) {
            super(in);
            current_line = new StringBuilder();
            last_line = "";
        }

        public LineBufferedCursorPositionReader(Reader in, int sz) {
            super(in, sz);
            current_line = new StringBuilder();
            last_line = "";
        }

        public void setLineNumber(int line_number) {
            this.line_num = line_number;
        }

        public int getLineNumber() {
            return line_num;
        }

        public int getCursorPosition() {
            return cursor_pos;
        }

        public String getCurrentLine() {
            return (current_line.toString());
        }

        public String getLastLine() {
            return (last_line);
        }

        public int read() throws IOException {
            synchronized(lock) {
                int c = super.read();
                if((c != -1) && (c != '\n') && (c != '\r')) {
                    cursor_pos++;
                    current_line.appendCodePoint(c);
                }
                if(skipLF) {
                    if(c == '\n') {
                        c = super.read();
                        if((c != -1) && (c != '\n') && (c != '\r')) {
                            cursor_pos++;
                            current_line.appendCodePoint(c);
                        }
                    }
                    skipLF = false;
                }
                switch(c) {
                    case '\r':
                        skipLF = true;
                    case '\n':		/* Fall through */
                        line_num++;
                        cursor_pos = 0;
                        last_line = current_line.toString();
                        current_line.setLength(0);
                        return '\n';
                }
                return c;
            }
        }

        public int read(char cbuf[], int off, int len) throws IOException {
            synchronized(lock) {
                int n = super.read(cbuf, off, len);
                for(int i = off; i < off + n; i++) {
                    int c = cbuf[i];
                    if((c != -1) && (c != '\n') && (c != '\r')) {
                        cursor_pos++;
                        current_line.appendCodePoint(c);
                    }
                    if(skipLF) {
                        skipLF = false;
                        if(c == '\n')
                            continue;
                    }
                    switch(c) {
                        case '\r':
                            skipLF = true;
                        case '\n':          /* Fall through */
                            line_num++;
                            cursor_pos = 0;
                            last_line = current_line.toString();
                            current_line.setLength(0);
                            break;
                    }
                }
                return n;
            }
        }

        public String readLine() throws IOException {
            synchronized(lock) {
                String l = super.readLine();
                skipLF = false;
                if(l != null) {
                    line_num++;
                    cursor_pos = 0;
                    current_line.append(l);
                    last_line = current_line.toString();
                    current_line.setLength(0);
                } else
                    last_line = current_line.toString();
                return l;
            }
        }
        private static final int maxSkipBufferSize = 8192;
        private char skipBuffer[] = null;

        public long skip(long n) throws IOException {
            if(n < 0)
                throw new IllegalArgumentException("skip() value is negative");
            int nn = (int)Math.min(n, maxSkipBufferSize);
            synchronized(lock) {
                if((skipBuffer == null) || (skipBuffer.length < nn))
                    skipBuffer = new char[nn];
                long r = n;
                while(r > 0) {
                    int nc = read(skipBuffer, 0, (int)Math.min(r, nn));
                    if(nc == -1)
                        break;
                    r -= nc;
                }
                return n - r;
            }
        }

        public void mark(int readAheadLimit) throws IOException {
            synchronized(lock) {
                super.mark(readAheadLimit);
                markedLineNumber = line_num;
                markedCursorPos = cursor_pos;
                markedSkipLF = skipLF;
            }
        }

        public void reset() throws IOException {
            super.reset();
            line_num = markedLineNumber;
            cursor_pos = markedCursorPos;
            skipLF = markedSkipLF;

        }

        public String formatCurrentLineAndClose() throws IOException {
            String prev = cursor_pos == 0 ? last_line : current_line.toString(), next = cursor_pos == 0 ? "" : readLine();
            close();
            if(cursor_pos == 0)
                cursor_pos = prev.length();
            String spaces = "";
            for(int i = 0; i < cursor_pos - 1; i++)
                if(prev.charAt(i) == '\t')
                    spaces += "\t";
                else if(prev.charAt(i) == '\f')
                    spaces += "\f";
                else
                    spaces += " ";
            return prev + (next != null ? next : "") + "\r\n" + spaces + "^\r\n";
        }
    }

    public abstract void close() throws IOException;

    public Terminal read() throws IOException {
        Terminal b[] = new Terminal[1];
        if(read(b, 0, 1) == -1)
            return Terminal.End;
        else
            return b[0];
    }

    public int read(Terminal[] buf) throws IOException {
        return read(buf, 0, buf.length);
    }

    public int read(Terminal[] buf, int off, int len) throws IOException {
        if(len <= 0)
            return (0);
        if(buf == null)
            throw (new IOException("pll.TerminalReader.read: buffer is null"));
        if((off < 0) || (off >= buf.length))
            throw (new IOException(
                    "pll.TerminalReader.read: offset is out of range"));
        int i = 0;
        if(len > buf.length)
            len = buf.length;
        while(i < len) {
            buf[i] = read();
            if((buf[i] == null) || (buf[i] == Terminal.End))
                break;
            i++;
        }
        return i > 0 ? i : -1;
    }

    public boolean markSupported() {
        return false;
    }

    public void mark(int readAheadLimit) throws IOException {
        throw new IOException("pll.TerminalReader.mark: operation not supported");
    }

    public void reset() throws IOException {
        throw new IOException(
                "pll.TerminalReader.reset: operation not supported");
    }

    public long skip(long n) throws IOException {
        if(n < 0L)
            throw new IllegalArgumentException(
                    "pll.TerminalReader.skip: skip value is negative");
        int nn = (int)Math.min(n, maxSkipBufferSize);
        Terminal[] skipBuffer = new Terminal[nn];
        long r = n;
        while(r > 0) {
            int nc = read(skipBuffer, 0, (int)Math.min(r, nn));
            if(nc < 0)
                break;
            r -= nc;
        }
        return n - r;
    }

    public boolean ready() throws IOException {
        return false;
    }

    public abstract int getLineNumber();

    public abstract int getCursorPosition();

    public abstract String formatCurrentLineAndClose() throws IOException;
    private static final int maxSkipBufferSize = 8192;

    public static class Byte extends TerminalReader {

        private LineBufferedCursorPositionReader r;
        private int cursor_pos;
        private long nbs;
        private int nc;

        public Byte(Reader r) throws IOException {
            this.r = new LineBufferedCursorPositionReader(r);
            nbs = 0;
            nc = this.r.read();
        }

        public void close() throws IOException {
            if(r != null)
                r.close();
        }

        public boolean ready() throws IOException {
            return (r != null ? r.ready() : false);
        }

        public boolean markSupported() {
            return (r != null ? r.markSupported() : false);
        }

        public void mark(int readAheadLimit) throws IOException {
            if(r != null)
                r.mark(readAheadLimit);
        }

        public void reset() throws IOException {
            if(r != null)
                r.reset();
            nbs = 0;
        }

        private long parseRNumber_u(int min_digits, int max_digits, int radix)
                throws IOException {
            long n = 0;
            int d = 0;
            if(radix <= 1)
                radix = 10;
            if((max_digits <= 0) || (max_digits < min_digits))
                return (0);
            while(d++ < max_digits) {
                if((nc >= '0') && (nc <= '9'))
                    n = n * radix + (nc - '0');
                else if(Character.isLowerCase(nc) && (nc - 'a' < radix - 10))
                    n = n * radix + (nc - 'a' + 10);
                else if(Character.isUpperCase(nc) && (nc - 'A' < radix - 10))
                    n = n * radix + (nc - 'A' + 10);
                else if(d <= min_digits)
                    throw (new IOException(
                            "expected " + (min_digits - d + 1) + " more digits of a base-" + radix + "-number,got " + (nc == -1 ? "to end of input" : "\"" + (char)nc + "\"") + " while parsing an u-escape"));
                else
                    break;
                nc = r.read();
            }
            return (n);
        }

        private int read_u() throws IOException {
            if(nc == -1)
                return (-1);
            int cc = nc;
            nc = r.read();
            if(cc == '\\')
                if(((nbs & 1) == 0) && ((nc == 'u') || (nc == 'U'))) {
                    cc = nc;
                    nc = r.read();
                    if(cc != nc)
                        cc = (int)(cc == 'u' ? parseRNumber_u(4, 4, 16) : parseRNumber_u(
                                1, 6, 16));
                    else {
                        cc = '\\';
                        nbs = 0;
                    }
                } else if(nc == '\\')
                    nbs++;
                else
                    nbs = 0;
            return (cc);
        }

        public Terminal read() throws IOException {
            cursor_pos = r.getCursorPosition();
            int cc = read_u();
            if((cc >= 0) && (cc <= 0xFFFF) && java.lang.Character.
                    isHighSurrogate((char)cc)) {
                int hc = cc;
                cc = read_u();
                if((cc < 0) || (cc > 0xFFFF) || !java.lang.Character.
                        isLowSurrogate((char)cc))
                    throw (new IOException(
                            "expected low surrogate, got " + (cc == -1 ? "to end of input" : "\"" + Terminal.String.
                            escapeCodePoint(cc) + "\"") + " while parsing a supplementary character"));
                cc = java.lang.Character.toCodePoint((char)hc, (char)cc);
            }
            if(cc == -1)
                return (Terminal.End);
            else
                return (new Terminal.Character(cc));
        }

        public int getLineNumber() {
            return (r.getLineNumber());
        }

        public int getCursorPosition() {
            return (cursor_pos);
        }

        public String formatCurrentLineAndClose() throws IOException {
            boolean two_line = cursor_pos > r.getCursorPosition();
            String prev = two_line ? r.getLastLine() : r.getCurrentLine(),
                    next = two_line ? r.getCurrentLine() + r.readLine() : r.
                    readLine();
            r.close();
            if(cursor_pos == 0)
                cursor_pos = prev.length();
            String spaces = "";
            for(int i = 0; i < cursor_pos - 1; i++)
                if(prev.charAt(i) == '\t')
                    spaces += "\t";
                else if(prev.charAt(i) == '\f')
                    spaces += "\f";
                else
                    spaces += " ";
            return (prev + (two_line ? "\r\n" : "") + (next != null ? next : "") + "\r\n" + spaces + "^\r\n" + (two_line ? spaces + "|\r\n" : ""));
        }
    }

    public static class Java extends Byte {

        private static final OrderedSet<String> keywords = new Final(new String[] {
                    "abstract", "continue", "for", "new", "switch", "assert",
                    "default", "if",
                    "package", "synchronized", "boolean", "do", "goto",
                    "private", "this",
                    "break", "double", "implements", "protected", "throw",
                    "byte", "else",
                    "import", "public", "throws", "case", "enum", "instanceof",
                    "return",
                    "transient", "catch", "extends", "int", "short", "try",
                    "char", "final",
                    "interface", "static", "void", "class", "finally", "long",
                    "strictfp",
                    "volatile", "const", "float", "native", "super", "while"
                });

        public static class NullLiteral extends Terminal.Tagged {

            public NullLiteral() {
                super("NullLiteral");
            }
        }

        public static class BooleanLiteral extends Terminal.Tagged {

            private boolean value;

            public BooleanLiteral(boolean value) {
                super("BooleanLiteral");
                this.value = value;
            }

            public BooleanLiteral() {
                this(false);
            }

            public boolean getValue() {
                return (value);
            }

            public boolean setValue(boolean value) {
                boolean h = this.value;
                this.value = value;
                return (h);
            }

            public java.lang.String toString() {
                return ("<BooleanLiteral:" + value + ">");
            }
        }

        public static class CharacterLiteral extends Terminal.Tagged {

            private int value;

            public CharacterLiteral(int value) {
                super("CharacterLiteral");
                this.value = value;
            }

            public CharacterLiteral() {
                this('\0');
            }

            public int getValue() {
                return (value);
            }

            public int setValue(int value) {
                int h = this.value;
                this.value = value;
                return (h);
            }

            public java.lang.String toString() {
                return ("<CharacterLiteral:\'" + Character.escapeCodePoint(value) + "\'>");
            }
        }

        public static class IntegerLiteral extends Terminal.Tagged {

            private long value;
            private boolean is_long;

            public IntegerLiteral(long value, boolean is_long) {
                super("IntegerLiteral");
                this.value = value;
                this.is_long = is_long;
            }

            public IntegerLiteral() {
                this(0, false);
            }

            public long getValue() {
                return (value);
            }

            public long setValue(long value) {
                long h = this.value;
                this.value = value;
                return (h);
            }

            public boolean isLong() {
                return (is_long);
            }

            public boolean setLong(boolean set_long) {
                boolean was_long = is_long;
                is_long = set_long;
                return (was_long);
            }

            public java.lang.String toString() {
                return ("<IntegerLiteral:" + (is_long ? value + "L" : (int)value) + ">");
            }
        }

        public static class FloatingPointLiteral extends Terminal.Tagged {

            private double value;
            private boolean is_double;

            public FloatingPointLiteral(double value, boolean is_double) {
                super("FloatingPointLiteral");
                this.value = value;
                this.is_double = is_double;
            }

            public FloatingPointLiteral() {
                this(0.0, false);
            }

            public double getValue() {
                return (value);
            }

            public double setValue(double value) {
                double h = this.value;
                this.value = value;
                return (h);
            }

            public boolean isDouble() {
                return (is_double);
            }

            public boolean setDouble(boolean set_double) {
                boolean was_double = is_double;
                is_double = set_double;
                return (was_double);
            }

            public java.lang.String toString() {
                return ("<FloatingPointLiteral:" + (is_double ? value + "D" : ((float)value) + "F") + ">");
            }
        }

        public static class StringLiteral extends Terminal.Tagged {

            private java.lang.String value;

            public StringLiteral(java.lang.String value) {
                super("StringLiteral");
                this.value = value;
            }

            public StringLiteral() {
                this("");
            }

            public java.lang.String getValue() {
                return (value);
            }

            public java.lang.String setValue(java.lang.String value) {
                java.lang.String h = this.value;
                this.value = value;
                return (h);
            }

            public java.lang.String toString() {
                return ("<StringLiteral:\"" + Terminal.String.escape(value) + "\">");
            }
        }

        public static class Identifier extends Terminal.Tagged {

            private java.lang.String value;

            public Identifier(java.lang.String value) {
                super("Identifier");
                this.value = value;
            }

            public Identifier() {
                this("");
            }

            public java.lang.String getValue() {
                return (value);
            }

            public java.lang.String setValue(java.lang.String value) {
                java.lang.String h = this.value;
                this.value = value;
                return (h);
            }

            public java.lang.String toString() {
                return ("<Identifier:" + Terminal.String.escape(value) + ">");
            }
        }
        private Terminal nt, ct;
        private int cursor_pos;

        public Java(Reader r) throws IOException {
            super(r);
            ct = null;
            nt = super.read();
        }

        @Override
        public Terminal read() throws IOException {
            return (readNextJToken());
        }

        private Terminal readNextJToken() throws IOException {
            ct = nt;
            if(ct == Terminal.End)
                return (ct);
            int c, d;
            boolean found = false;
            while(!found) {
                cursor_pos = super.getCursorPosition();
                nt = super.read();
//                if(nt==Terminal.End)
//                    return(ct);
                c = currentChar();
                d = nextChar();
                found = true;
                switch(c) {
                    case '+':
                    case '-':
                    case '&':
                    case '|':
                        if((d == c) || (d == '=')) {
                            ct = new Terminal.String(((char)c) + "" + ((char)d));
                            nt = super.read();
                        }
                        break;
                    case '/':
                        if((d == '/') || (d == '*')) {
                            skipComment();
                            found = false;
                            ct = nt;
                            break;
                        }
                    case '=':
                    case '^':
                    case '*':
                    case '!':
                    case '%':
                        if(d == '=') {
                            ct = new Terminal.String(((char)c) + "" + ((char)d));
                            nt = super.read();
                        }
                        break;
                    case '<':
                        if(d == '=') {
                            ct = new Terminal.String(((char)c) + "" + ((char)d));
                            nt = super.read();
                        } else if(d == c) {
                            ct = new Terminal.String(((char)c) + "" + ((char)d));
                            nt = super.read();
                            if(nt == Terminal.End)
                                break;
                            d = ((Terminal.Character)nt).getChar();
                            if(d == '=') {
                                ((Terminal.String)ct).setString(((Terminal.String)ct).
                                        getString() + ((char)d));
                                nt = super.read();
                            }
                        }
                        break;
                    case '>':
                        if(d == '=') {
                            ct = new Terminal.String(((char)c) + "" + ((char)d));
                            nt = super.read();
                        } else if(d == c) {
                            ct = new Terminal.String(((char)c) + "" + ((char)d));
                            nt = super.read();
                            if(nt == Terminal.End)
                                break;
                            d = ((Terminal.Character)nt).getChar();
                            if(d == '=') {
                                ((Terminal.String)ct).setString(((Terminal.String)ct).
                                        getString() + ((char)d));
                                nt = super.read();
                            } else if(d == c) {
                                ((Terminal.String)ct).setString(((Terminal.String)ct).
                                        getString() + ((char)d));
                                nt = super.read();
                                if(nt == Terminal.End)
                                    break;
                                d = ((Terminal.Character)nt).getChar();
                                if(d == '=') {
                                    ((Terminal.String)ct).setString(((Terminal.String)ct).
                                            getString() + ((char)d));
                                    nt = super.read();
                                }
                            }
                        }
                        break;
                    case '\'':
                        ct = parseCharacterLiteral();
                        break;
                    case '\"':
                        ct = parseStringLiteral();
                        break;
                    case '.':
                        if(Character.isDigit(d)) {
                            ct = parseNumeral();
                            break;
                        }
                    case '~':
                    case '(':
                    case ')':
                    case '[':
                    case ']':
                    case '{':
                    case '}':
                    case ',':
                    case ';':
                    case ':':
                    case '?':
                    case '@':
                        break;
                    case -1:
                        break;
                    default:
                        if(Character.isWhitespace(c)) {
                            found = false;
                            ct = nt;
                            continue;
                        } else if(Character.isDigit(c))
                            ct = parseNumeral();
                        else {
                            Identifier id = parseIdentifier();
                            if(id.getValue().equals("null"))
                                ct = new NullLiteral();
                            else if(id.getValue().equals("true"))
                                ct = new BooleanLiteral(true);
                            else if(id.getValue().equals("false"))
                                ct = new BooleanLiteral(false);
                            else if(keywords.contains(id.getValue()))
                                ct = new Terminal.String(id.getValue());
                            else
                                ct = id;
                        }

                }

            }
            return (ct);
        }

        private StringLiteral parseStringLiteral() throws IOException {
            int c = currentChar();
            if(c != '\"')
                throw (new IOException(
                        "expected string-quotes,got " + (c == -1 ? "to end of input" : "\"" + Terminal.String.
                        escapeCodePoint(c) + "\"") + " while parsing a StringLiteral in line " + getLineNumber()));
            StringBuilder sb = new StringBuilder();
            while(true) {
                readNextTerminal();
                c = currentChar();
                if((c == -1) || (c == '\n') || (c == '\r'))
                    throw (new IOException(
                            "expected string-quotes, got to end of " + (c < 0 ? "input" : "line") + " while parsing a StringLiteral in line " + getLineNumber()));
                else if(c == '\"')
                    break;
                else if(c == '\\') {
                    readNextTerminal();
                    c = currentChar();
                    if(c == -1)
                        throw (new IOException(
                                "expected string-quotes, got to end of input while parsing an escape-sequence in a StringLiteral in line " + getLineNumber()));
                    switch(c) {
                        case 'b':
                            sb.append('\b');
                            break;
                        case 't':
                            sb.append('\t');
                            break;
                        case 'n':
                            sb.append('\n');
                            break;
                        case 'f':
                            sb.append('\f');
                            break;
                        case 'r':
                            sb.append('\r');
                            break;
                        case '\"':
                            sb.append('\"');
                            break;
                        case '\'':
                            sb.append('\'');
                            break;
                        case '\\':
                            sb.append('\\');
                            break;
                        default:
                            if((c >= '4') && (c < '7'))
                                sb.appendCodePoint((int)parseRNumber(1, 2, 8));
                            else if((c >= '0') && (c < '3'))
                                sb.appendCodePoint((int)parseRNumber(1, 3, 8));
                            else
                                throw (new IOException(
                                        "expected escape-code, got \\" + ((c == -1) || (c == '\n') || (c == '\r') ? "to end of " + (c < 0 ? "input" : "line") : "\"" + Terminal.String.
                                        escapeCodePoint(c) + "\"") + " while parsing an escape-sequence in a StringLiteral in line " + getLineNumber()));
                    }
                } else
                    sb.appendCodePoint(c);
            }
            ct = new StringLiteral(sb.toString());
            return ((StringLiteral)ct);
        }

        private CharacterLiteral parseCharacterLiteral() throws IOException {
            int c = currentChar(), r = '\0';
            if(c != '\'')
                throw (new IOException(
                        "expected opening character-quote,got " + (c == -1 ? "to end of input" : "\"" + Terminal.Character.
                        escapeCodePoint(c) + "\"") + " while parsing a CharacterLiteral in line " + getLineNumber()));
            readNextTerminal();
            c = currentChar();
            if((c == -1) || (c == '\n') || (c == '\r'))
                throw (new IOException(
                        "expected single character, got to end of " + (c < 0 ? "input" : "line") + " while parsing a CharacterLiteral in line " + getLineNumber()));
            else if(c == '\'')
                throw (new IOException(
                        "empty CharacterLiteral in line " + getLineNumber()));
            else if(c == '\\') {
                readNextTerminal();
                c = currentChar();
                if(c == -1)
                    throw (new IOException(
                            " got to end of input while parsing an escape-sequence in a CharacterLiteral in line " + getLineNumber()));
                switch(c) {
                    case 'b':
                        r = '\b';
                        break;
                    case 't':
                        r = '\t';
                        break;
                    case 'n':
                        r = '\n';
                        break;
                    case 'f':
                        r = '\f';
                        break;
                    case 'r':
                        r = '\r';
                        break;
                    case '\"':
                        r = '\"';
                        break;
                    case '\'':
                        r = '\'';
                        break;
                    case '\\':
                        r = '\\';
                        break;
                    default:
                        if((c >= '4') && (c < '7'))
                            r = (int)parseRNumber(1, 2, 8);
                        else if((c >= '0') && (c < '3'))
                            r = (int)parseRNumber(1, 3, 8);
                        else
                            throw (new IOException(
                                    "expected escape-code, got \\" + ((c == -1) || (c == '\n') || (c == '\r') ? "to end of " + (c < 0 ? "input" : "line") : "\"" + Terminal.Character.
                                    escapeCodePoint(c) + "\"") + " while parsing an escape-sequence in a CharacterLiteral in line " + getLineNumber()));
                }
            } else
                r = c;
            readNextTerminal();
            c = currentChar();
            if(c != '\'')
                throw (new IOException(
                        "expected closing character-quote,got " + (c == -1 ? "to end of input" : "\"" + Terminal.Character.
                        escapeCodePoint(c) + "\"") + " while parsing a CharacterLiteral in line " + getLineNumber()));
            ct = new CharacterLiteral(r);
            return ((CharacterLiteral)ct);
        }

        private Identifier parseIdentifier() throws IOException {
            int c = currentChar();
            if(!Character.isJavaIdentifierStart(c))
                throw (new IOException(
                        "expected java-identifier,got " + (c == -1 ? "to end of input" : "\"" + Terminal.String.
                        escapeCodePoint(c) + "\"") + " while parsing an identifier in line " + getLineNumber()));
            StringBuilder sb = new StringBuilder().appendCodePoint(c);
            c = nextChar();
            while(Character.isJavaIdentifierPart(c)) {
                if(!Character.isIdentifierIgnorable(c))
                    sb.appendCodePoint(c);
                readNextTerminal();
                c = nextChar();
            }
            return (new Identifier(sb.toString()));
        }

        private Terminal parseNumeral() throws IOException {
            Terminal r = null;
            int c = currentChar(), d = nextChar();
            String ds1 = "", ds2 = "", ds3 = "";
            double s = 0.0;
            long t = 0;
            boolean is_longorfloat = false, trailing_zero = (c == '0'),
                    floatp = (c == '.'), hexp = false;
            if((c == '.') || ((c >= '0') && (c <= '9'))) {
                if(trailing_zero && ((d == 'x') || (d == 'X'))) {
                    hexp = true;
                    readNextTerminal();
                    readNextTerminal();
                    c = currentChar();
                    d = nextChar();
                }
                if(c == '.') {
                    //ds1 = "";
                    floatp = true;
                    readNextTerminal();
                    c = currentChar();
                    d = nextChar();
                    ds2 = readDigitString(hexp ? 16 : 10);
                } else {
                    ds1 = readDigitString(hexp ? 16 : 10);
                    d = nextChar();
                    if(d == '.') {
                        floatp = true;
                        readNextTerminal();
                        d = nextChar();
                        if(((d >= '0') && (d <= '9'))
                           || (hexp && (((d >= 'A') && (d <= 'F')) || ((d >= 'a') && (d <= 'f'))))) {
                            readNextTerminal();
                            ds2 = readDigitString(hexp ? 16 : 10);
                        } //else
                        //ds2 = "";
                    }
                }
                d = nextChar();
                if(floatp) {
                    if(hexp)
                        if((d == 'p') || (d == 'P')) {
                            readNextTerminal();
                            readNextTerminal();
                            ds3 = readSignedDigitString(10);
                        } else
                            throw (new IOException(
                                    "exponent required for hexadecimal floating point number, got " + (d == -1 ? "to end of input" : "\"" + Terminal.String.
                                    escapeCodePoint(d) + "\"") + " while parsing a numeral in line " + getLineNumber()));
                    else
                        if((d == 'e') || (d == 'E')) {
                            readNextTerminal();
                            readNextTerminal();
                            ds3 = readSignedDigitString(10);
                        } else
                            ds3 = "";
                    d = nextChar();
                    if((d == 'f') || (d == 'F')) {
                        is_longorfloat = true;
                        readNextTerminal();
                    } else if((d == 'd') || (d == 'D')) {
                        is_longorfloat = false;
                        readNextTerminal();
                    }
                } else
                    if((d == 'l') || (d == 'L')) {
                        is_longorfloat = true;
                        readNextTerminal();
                    } else
                        is_longorfloat = false;
                if(floatp)
                    if(hexp)
                        if(!is_longorfloat)
                            try {
                                r = new FloatingPointLiteral(Double.parseDouble(
                                        "0x" + ds1 + "." + ds2 + "p" + ds3),
                                                             true);
                            } catch(NumberFormatException e) {
                                throw (new IOException("malformed hexadecimal double \"" + (ds1 + "." + ds2 + "p" + ds3) + "\" in line " + getLineNumber() + ":" + e.
                                        getMessage()));
                            }
                        else
                            try {
                                r = new FloatingPointLiteral(Float.parseFloat(
                                        "0x" + ds1 + "." + ds2 + "p" + ds3),
                                                             false);
                            } catch(NumberFormatException e) {
                                throw (new IOException("malformed hexadecimal float \"" + (ds1 + "." + ds2 + "p" + ds3) + "\" in line " + getLineNumber() + ":" + e.
                                        getMessage()));
                            }
                    else
                        if(!is_longorfloat)
                            try {
                                r = new FloatingPointLiteral(Double.parseDouble(ds1 + "." + ds2 + (ds3.
                                        equals("") ? "" : "e" + ds3)), true);
                            } catch(NumberFormatException e) {
                                throw (new IOException("malformed decimal double \"" + (ds1 + "." + ds2 + (ds3.
                                        equals("") ? "" : "e" + ds3)) + "\" in line " + getLineNumber() + ":" + e.
                                        getMessage()));
                            }
                        else
                            try {
                                r = new FloatingPointLiteral(Float.parseFloat(ds1 + "." + ds2 + (ds3.
                                        equals("") ? "" : "e" + ds3)), false);
                            } catch(NumberFormatException e) {
                                throw (new IOException("malformed decimal float \"" + (ds1 + "." + ds2 + (ds3.
                                        equals("") ? "" : "e" + ds3)) + "\" in line " + getLineNumber() + ":" + e.
                                        getMessage()));
                            }
                else
                    if(hexp)
                        if(is_longorfloat)
                            try {
                                r = new IntegerLiteral(parseRawHexLong(ds1),
                                                       true);
                            } catch(NumberFormatException e) {
                                throw (new IOException("malformed hexadecimal long \"" + ds1 + "\" in line " + getLineNumber() + ":" + e.
                                        getMessage()));
                            }
                        else
                            try {
                                r = new IntegerLiteral(parseRawHexInt(ds1),
                                                       false);
                            } catch(NumberFormatException e) {
                                throw (new IOException("malformed hexadecimal int \"" + ds1 + "\" in line " + getLineNumber() + ":" + e.
                                        getMessage()));
                            }
                    else if(trailing_zero)
                        if(is_longorfloat)
                            try {
                                r = new IntegerLiteral(Long.parseLong(ds1, 8),
                                                       true);
                            } catch(NumberFormatException e) {
                                throw (new IOException("malformed octal long \"" + ds1 + "\" in line " + getLineNumber() + ":" + e.
                                        getMessage()));
                            }
                        else
                            try {
                                r = new IntegerLiteral(Integer.parseInt(ds1, 8),
                                                       false);
                            } catch(NumberFormatException e) {
                                throw (new IOException("malformed octal int \"" + ds1 + "\" in line " + getLineNumber() + ":" + e.
                                        getMessage()));
                            }
                    else
                        if(is_longorfloat)
                            try {
                                r = new IntegerLiteral(Long.parseLong(ds1, 10),
                                                       true);
                            } catch(NumberFormatException e) {
                                throw (new IOException("malformed decimal long \"" + ds1 + "\" in line " + getLineNumber() + ":" + e.
                                        getMessage()));
                            }
                        else
                            try {
                                r = new IntegerLiteral(Integer.parseInt(ds1, 10),
                                                       false);
                            } catch(NumberFormatException e) {
                                throw (new IOException("malformed decimal int \"" + ds1 + "\" in line " + getLineNumber() + ":" + e.
                                        getMessage()));
                            }
            } else
                throw (new IOException(
                        "expected digit or decimal point, got " + (c == -1 ? "to end of input" : "\"" + Terminal.String.
                        escapeCodePoint(c) + "\"") + " while parsing a numeral in line " + getLineNumber()));
            return (r);
        }

        public int parseRawHexInt(java.lang.String s) {
            int r = 0;
            int i = 0, l;
            char c;
            if((s == null) || ((l = s.length()) == 0))
                throw (new NumberFormatException("at least one digit expected"));
            if(l > 8)
                throw (new NumberFormatException("too many digits"));
            while(i < l) {
                c = s.charAt(i++);
                r <<= 4;
                if((c >= '0') && (c <= '9'))
                    r += c - '0';
                else if((c >= 'A') && (c <= 'F'))
                    r += c - 'A' + 10;
                else if((c >= 'a') && (c <= 'f'))
                    r += c - 'a' + 10;
                else
                    throw (new NumberFormatException(
                            "wrong digit at position " + i));
            }
            return (r);
        }

        public long parseRawHexLong(java.lang.String s) {
            long r = 0;
            int i = 0, l;
            char c;
            if((s == null) || ((l = s.length()) == 0))
                throw (new NumberFormatException("at least one digit expected"));
            if(l > 16)
                throw (new NumberFormatException("too many digits"));
            while(i < l) {
                c = s.charAt(i++);
                r <<= 4;
                if((c >= '0') && (c <= '9'))
                    r += c - '0';
                else if((c >= 'A') && (c <= 'F'))
                    r += c - 'A' + 10;
                else if((c >= 'a') && (c <= 'f'))
                    r += c - 'a' + 10;
                else
                    throw (new NumberFormatException(
                            "wrong digit at position " + i));
            }
            return (r);
        }

        private int currentChar() throws IOException {
            return ((ct == Terminal.End) ? -1 : ((Terminal.Character)ct).getChar());
        }

        private int nextChar() throws IOException {
            return ((nt == Terminal.End) ? -1 : ((Terminal.Character)nt).getChar());
        }

        private Terminal readNextTerminal() throws IOException {
            ct = nt;
            nt = super.read();
            return (ct);
        }

        private long parseRNumber(int min_digits, int max_digits, int radix)
                throws IOException {
            long n = 0;
            int d = 0;
            int c = currentChar(), e = nextChar();
            boolean negate = false;
            if(radix <= 1)
                radix = 10;
            if((max_digits <= 0) || (max_digits < min_digits))
                return (0);
            if(c == '-') {
                negate = true;
                if(min_digits == 0)
                    min_digits = 1;
            } else if(c == '+') {
                negate = false;
                if(min_digits == 0)
                    min_digits = 1;
            } else {
                d++;
                if((c >= '0') && (c <= '9'))
                    n = n * radix + (c - '0');
                else if((c >= 'a') && (c <= 'z') && (c - 'a' < radix - 10))
                    n = n * radix + (c - 'a' + 10);
                else if((c >= 'A') && (c <= 'Z') && (c - 'A' < radix - 10))
                    n = n * radix + (c - 'A' + 10);
                else if(d <= min_digits)
                    throw (new IOException(
                            "expected " + (min_digits - d + 1) + " more digits of a base-" + radix + "-number,got " + (e == -1 ? "to end of input" : "\"" + Terminal.String.
                            escapeCodePoint(e) + "\"") + " while parsing a number in line " + getLineNumber()));
            }

            while(d++ < max_digits) {
                if((e >= '0') && (e <= '9'))
                    n = n * radix + (e - '0');
                else if((e >= 'a') && (e <= 'z') && (e - 'a' < radix - 10))
                    n = n * radix + (e - 'a' + 10);
                else if((e >= 'A') && (e <= 'Z') && (e - 'A' < radix - 10))
                    n = n * radix + (e - 'A' + 10);
                else if(d <= min_digits)
                    throw (new IOException(
                            "expected " + (min_digits - d + 1) + " more digits of a base-" + radix + "-number,got " + (e == -1 ? "to end of input" : "\"" + Terminal.String.
                            escapeCodePoint(e) + "\"") + " while parsing a number in line " + getLineNumber()));
                else
                    break;
                readNextTerminal();
                e = nextChar();
            }
            return (negate ? -n : n);
        }

        private String readSignedDigitString(int radix) throws IOException {
            int c = currentChar();
            char sign = '+';
            if((c == '-') || (c == '+')) {
                sign = (char)c;
                readNextTerminal();
            }
            return (sign + readDigitString(radix));
        }

        private String readDigitString(int radix) throws IOException {
            if(radix <= 1)
                return ("");
            StringBuilder sb = new StringBuilder();
            int c = currentChar(), e = nextChar();
            if(((c >= '0') && (c <= '9'))
               || ((c >= 'a') && (c <= 'z') && (c - 'a' < radix - 10))
               || ((c >= 'A') && (c <= 'Z') && (c - 'A' < radix - 10)))
                sb.appendCodePoint(c);
            else
                throw (new IOException(
                        "expected at least one digit, got " + (e == -1 ? "to end of input" : "\"" + Terminal.String.
                        escapeCodePoint(e) + "\"") + " while parsing a number in line " + getLineNumber()));
            while(true) {
                if(((e >= '0') && (e <= '9'))
                   || ((e >= 'a') && (e <= 'z') && (e - 'a' < radix - 10))
                   || ((e >= 'A') && (e <= 'Z') && (e - 'A' < radix - 10)))
                    sb.appendCodePoint(e);
                else
                    break;
                readNextTerminal();
                e = nextChar();
            }
            return (sb.toString());
        }

        private void skipComment() throws IOException {
            int c = currentChar(), d = nextChar();
            if(c != '/')
                throw (new IOException("expected comment, got " + Terminal.Character.
                        escapeCodePoint(c) + " in line " + getLineNumber()));
            else if(d == '/') {
                do {
                    readNextTerminal();
                    d = nextChar();
                } while((d != -1) && (d != '\n'));
                if(d != -1)
                    readNextTerminal();
            } else if(d == '*') {
                readNextTerminal();
                d = nextChar();
                while(true)
                    if(d == -1)
                        throw (new IOException(
                                "unexpected end of input while parsing a traditional comment in line " + getLineNumber()));
                    else if(d == '*') {
                        readNextTerminal();
                        d = nextChar();
                        if(d == -1)
                            throw (new IOException(
                                    "unexpected end of input while parsing a traditional comment in line " + getLineNumber()));
                        else if(d == '/') {
                            readNextTerminal();
                            break;
                        }
                    } else {
                        readNextTerminal();
                        d = nextChar();
                    }
            } else
                throw (new IOException("expected comment-spec, got " + Terminal.Character.
                        escapeCodePoint(c)));
        }

        public int getCursorPosition() {
            return (cursor_pos);
        }

        public String formatCurrentLineAndClose() throws IOException {
            boolean two_line = cursor_pos > super.r.getCursorPosition();
            String prev = two_line ? super.r.getLastLine() : super.r.
                    getCurrentLine(),
                    next = two_line ? super.r.getCurrentLine() + super.r.
                    readLine() : super.r.readLine();
            super.r.close();
            if(cursor_pos == 0)
                cursor_pos = prev.length();
            String spaces = "";
            for(int i = 0; i < cursor_pos - 1; i++)
                if(prev.charAt(i) == '\t')
                    spaces += "\t";
                else if(prev.charAt(i) == '\f')
                    spaces += "\f";
                else
                    spaces += " ";
            return (prev + (two_line ? "\r\n" : "") + (next != null ? next : "") + "\r\n" + spaces + "^\r\n" + (two_line ? spaces + "|\r\n" : ""));
        }
    }

    public static class C extends Byte {

        private static final OrderedSet<String> keywords = new Final(new String[] {
                    "auto", "continue", "for", "register", "switch", "extern",
                    "default", "if",
                    "typedef", "do", "goto", "break", "else", "case", "enum",
                    "return",
                    "void", "char", "short", "int", "long", "float", "double",
                    "signed", "unsigned",
                    "static", "volatile", "const", "while", "struct", "union"
                });

        public static class CharacterLiteral extends Terminal.Tagged {

            private int value;

            public CharacterLiteral(int value) {
                super("CharacterLiteral");
                this.value = value;
            }

            public CharacterLiteral() {
                this('\0');
            }

            public int getValue() {
                return (value);
            }

            public int setValue(int value) {
                int h = this.value;
                this.value = value;
                return (h);
            }

            public java.lang.String toString() {
                return ("<CharacterLiteral:\'" + Character.escapeCodePoint(value) + "\'>");
            }
        }

        public static class IntegerLiteral extends Terminal.Tagged {

            private long value;
            private boolean is_long;

            public IntegerLiteral(long value, boolean is_long) {
                super("IntegerLiteral");
                this.value = value;
                this.is_long = is_long;
            }

            public IntegerLiteral() {
                this(0, false);
            }

            public long getValue() {
                return (value);
            }

            public long setValue(long value) {
                long h = this.value;
                this.value = value;
                return (h);
            }

            public boolean isLong() {
                return (is_long);
            }

            public boolean setLong(boolean set_long) {
                boolean was_long = is_long;
                is_long = set_long;
                return (was_long);
            }

            public java.lang.String toString() {
                return ("<IntegerLiteral:" + (is_long ? value + "L" : (int)value) + ">");
            }
        }

        public static class FloatingPointLiteral extends Terminal.Tagged {

            private double value;
            private boolean is_double;

            public FloatingPointLiteral(double value, boolean is_double) {
                super("FloatingPointLiteral");
                this.value = value;
                this.is_double = is_double;
            }

            public FloatingPointLiteral() {
                this(0.0, false);
            }

            public double getValue() {
                return (value);
            }

            public double setValue(double value) {
                double h = this.value;
                this.value = value;
                return (h);
            }

            public boolean isDouble() {
                return (is_double);
            }

            public boolean setDouble(boolean set_double) {
                boolean was_double = is_double;
                is_double = set_double;
                return (was_double);
            }

            public java.lang.String toString() {
                return ("<FloatingPointLiteral:" + (is_double ? value + "D" : ((float)value) + "F") + ">");
            }
        }

        public static class StringLiteral extends Terminal.Tagged {

            private java.lang.String value;

            public StringLiteral(java.lang.String value) {
                super("StringLiteral");
                this.value = value;
            }

            public StringLiteral() {
                this("");
            }

            public java.lang.String getValue() {
                return (value);
            }

            public java.lang.String setValue(java.lang.String value) {
                java.lang.String h = this.value;
                this.value = value;
                return (h);
            }

            public java.lang.String toString() {
                return ("<StringLiteral:\"" + Terminal.String.escape(value) + "\">");
            }
        }

        public static class Identifier extends Terminal.Tagged {

            private java.lang.String value;

            public Identifier(java.lang.String value) {
                super("Identifier");
                this.value = value;
            }

            public Identifier() {
                this("");
            }

            public java.lang.String getValue() {
                return (value);
            }

            public java.lang.String setValue(java.lang.String value) {
                java.lang.String h = this.value;
                this.value = value;
                return (h);
            }

            public java.lang.String toString() {
                return ("<Identifier:" + Terminal.String.escape(value) + ">");
            }
        }
        private Terminal nt, ct;
        private int cursor_pos;

        public C(Reader r) throws IOException {
            super(r);
            ct = null;
            nt = super.read();
        }

        @Override
        public Terminal read() throws IOException {
            return (readNextCToken());
        }

        private Terminal readNextCToken() throws IOException {
            ct = nt;
            if(ct == Terminal.End)
                return (ct);
            int c, d;
            boolean found = false;
            while(!found) {
                cursor_pos = super.getCursorPosition();
                nt = super.read();
//                if(nt==Terminal.End)
//                    return(ct);
                c = currentChar();
                d = nextChar();
                found = true;
                switch(c) {
                    case '-':
                        if(d == '>') {
                            ct = new Terminal.String(((char)c) + "" + ((char)d));
                            nt = super.read();
                            break;
                        }
                    case '+':
                    case '&':
                    case '|':
                        if((d == c) || (d == '=')) {
                            ct = new Terminal.String(((char)c) + "" + ((char)d));
                            nt = super.read();
                        }
                        break;
                    case '/':
                        if((d == '/') || (d == '*')) {
                            skipComment();
                            found = false;
                            ct = nt;
                            break;
                        }
                    case '#':
                        skipPreProcessorDirective();    //ignore pre processor directives for now
                        found = false;
                        ct = nt;
                        break;
                    case '=':
                    case '^':
                    case '*':
                    case '!':
                    case '%':
                        if(d == '=') {
                            ct = new Terminal.String(((char)c) + "" + ((char)d));
                            nt = super.read();
                        }
                        break;
                    case '<':
                    case '>':
                        if(d == '=') {
                            ct = new Terminal.String(((char)c) + "" + ((char)d));
                            nt = super.read();
                        } else if(d == c) {
                            ct = new Terminal.String(((char)c) + "" + ((char)d));
                            nt = super.read();
                            if(nt == Terminal.End)
                                break;
                            d = ((Terminal.Character)nt).getChar();
                            if(d == '=') {
                                ((Terminal.String)ct).setString(((Terminal.String)ct).
                                        getString() + ((char)d));
                                nt = super.read();
                            }
                        }
                        break;
                    case '\'':
                        ct = parseCharacterLiteral();
                        break;
                    case '\"':
                        ct = parseStringLiteral();
                        break;
                    case '.':
                        if(Character.isDigit(d))
                            ct = parseNumeral();
                        else if(d == '.') {
                            nt = super.read();
                            if(nt == Terminal.End)
                                throw (new IOException(
                                        "unexpected end of input while parsing a dot sequence in line " + getLineNumber()));
                            d = ((Terminal.Character)nt).getChar();
                            if(d != '.')
                                throw (new IOException(
                                        "expected one more \".\", got " + nt + " while parsing a dot sequence in line " + getLineNumber()));
                            ct = new Terminal.String("...");
                            nt = super.read();
                        }
                        break;
                    case '~':
                    case '(':
                    case ')':
                    case '[':
                    case ']':
                    case '{':
                    case '}':
                    case ',':
                    case ';':
                    case ':':
                    case '?':
                    case '@':
                        break;
                    case -1:
                        break;
                    default:
                        if(Character.isWhitespace(c)) {
                            found = false;
                            ct = nt;
                            continue;
                        } else if(Character.isDigit(c))
                            ct = parseNumeral();
                        else {
                            Identifier id = parseIdentifier();
                            if(keywords.contains(id.getValue()))
                                ct = new Terminal.String(id.getValue());
                            else
                                ct = id;
                        }

                }

            }
            return (ct);
        }

        private StringLiteral parseStringLiteral() throws IOException {
            int c = currentChar();
            if(c != '\"')
                throw (new IOException(
                        "expected string-quotes,got " + (c == -1 ? "to end of input" : "\"" + Terminal.String.
                        escapeCodePoint(c) + "\"") + " while parsing a StringLiteral in line " + getLineNumber()));
            StringBuilder sb = new StringBuilder();
            while(true) {
                readNextTerminal();
                c = currentChar();
                if((c == -1) || (c == '\n') || (c == '\r'))
                    throw (new IOException(
                            "expected string-quotes, got to end of " + (c < 0 ? "input" : "line") + " while parsing a StringLiteral in line " + getLineNumber()));
                else if(c == '\"')
                    break;
                else if(c == '\\') {
                    readNextTerminal();
                    c = currentChar();
                    if(c == -1)
                        throw (new IOException(
                                "expected string-quotes, got to end of input while parsing an escape-sequence in a StringLiteral in line " + getLineNumber()));
                    switch(c) {
                        case 'b':
                            sb.append('\b');
                            break;
                        case 't':
                            sb.append('\t');
                            break;
                        case 'n':
                            sb.append('\n');
                            break;
                        case 'f':
                            sb.append('\f');
                            break;
                        case 'r':
                            sb.append('\r');
                            break;
                        case '\"':
                            sb.append('\"');
                            break;
                        case '\'':
                            sb.append('\'');
                            break;
                        case '\\':
                            sb.append('\\');
                            break;
                        default:
                            if((c >= '4') && (c < '7'))
                                sb.appendCodePoint((int)parseRNumber(1, 2, 8));
                            else if((c >= '0') && (c < '3'))
                                sb.appendCodePoint((int)parseRNumber(1, 3, 8));
                            else
                                throw (new IOException(
                                        "expected escape-code, got \\" + ((c == -1) || (c == '\n') || (c == '\r') ? "to end of " + (c < 0 ? "input" : "line") : "\"" + Terminal.String.
                                        escapeCodePoint(c) + "\"") + " while parsing an escape-sequence in a StringLiteral in line " + getLineNumber()));
                    }
                } else
                    sb.appendCodePoint(c);
            }
            ct = new StringLiteral(sb.toString());
            return ((StringLiteral)ct);
        }

        private CharacterLiteral parseCharacterLiteral() throws IOException {
            int c = currentChar(), r = '\0';
            if(c != '\'')
                throw (new IOException(
                        "expected opening character-quote,got " + (c == -1 ? "to end of input" : "\"" + Terminal.Character.
                        escapeCodePoint(c) + "\"") + " while parsing a CharacterLiteral in line " + getLineNumber()));
            readNextTerminal();
            c = currentChar();
            if((c == -1) || (c == '\n') || (c == '\r'))
                throw (new IOException(
                        "expected single character, got to end of " + (c < 0 ? "input" : "line") + " while parsing a CharacterLiteral in line " + getLineNumber()));
            else if(c == '\'')
                throw (new IOException(
                        "empty CharacterLiteral in line " + getLineNumber()));
            else if(c == '\\') {
                readNextTerminal();
                c = currentChar();
                if(c == -1)
                    throw (new IOException(
                            " got to end of input while parsing an escape-sequence in a CharacterLiteral in line " + getLineNumber()));
                switch(c) {
                    case 'b':
                        r = '\b';
                        break;
                    case 't':
                        r = '\t';
                        break;
                    case 'n':
                        r = '\n';
                        break;
                    case 'f':
                        r = '\f';
                        break;
                    case 'r':
                        r = '\r';
                        break;
                    case '\"':
                        r = '\"';
                        break;
                    case '\'':
                        r = '\'';
                        break;
                    case '\\':
                        r = '\\';
                        break;
                    default:
                        if((c >= '4') && (c < '7'))
                            r = (int)parseRNumber(1, 2, 8);
                        else if((c >= '0') && (c < '3'))
                            r = (int)parseRNumber(1, 3, 8);
                        else
                            throw (new IOException(
                                    "expected escape-code, got \\" + ((c == -1) || (c == '\n') || (c == '\r') ? "to end of " + (c < 0 ? "input" : "line") : "\"" + Terminal.Character.
                                    escapeCodePoint(c) + "\"") + " while parsing an escape-sequence in a CharacterLiteral in line " + getLineNumber()));
                }
            } else
                r = c;
            readNextTerminal();
            c = currentChar();
            if(c != '\'')
                throw (new IOException(
                        "expected closing character-quote,got " + (c == -1 ? "to end of input" : "\"" + Terminal.Character.
                        escapeCodePoint(c) + "\"") + " while parsing a CharacterLiteral in line " + getLineNumber()));
            ct = new CharacterLiteral(r);
            return ((CharacterLiteral)ct);
        }

        private Identifier parseIdentifier() throws IOException {
            int c = currentChar();
            if(!Character.isJavaIdentifierStart(c))
                throw (new IOException(
                        "expected java-identifier,got " + (c == -1 ? "to end of input" : "\"" + Terminal.String.
                        escapeCodePoint(c) + "\"") + " while parsing an identifier in line " + getLineNumber()));
            StringBuilder sb = new StringBuilder().appendCodePoint(c);
            c = nextChar();
            while(Character.isJavaIdentifierPart(c)) {
                if(!Character.isIdentifierIgnorable(c))
                    sb.appendCodePoint(c);
                readNextTerminal();
                c = nextChar();
            }
            return (new Identifier(sb.toString()));
        }

        private Terminal parseNumeral() throws IOException {
            Terminal r = null;
            int c = currentChar(), d = nextChar();
            String ds1 = "", ds2 = "", ds3 = "";
            double s = 0.0;
            long t = 0;
            boolean is_longorfloat = false, trailing_zero = (c == '0'),
                    floatp = (c == '.'), hexp = false;
            if((c == '.') || ((c >= '0') && (c <= '9'))) {
                if(trailing_zero && ((d == 'x') || (d == 'X'))) {
                    hexp = true;
                    readNextTerminal();
                    readNextTerminal();
                    c = currentChar();
                    d = nextChar();
                }
                if(c == '.') {
                    //ds1 = "";
                    floatp = true;
                    readNextTerminal();
                    c = currentChar();
                    d = nextChar();
                    ds2 = readDigitString(hexp ? 16 : 10);
                } else {
                    ds1 = readDigitString(hexp ? 16 : 10);
                    d = nextChar();
                    if(d == '.') {
                        floatp = true;
                        readNextTerminal();
                        d = nextChar();
                        if(((d >= '0') && (d <= '9'))
                           || (hexp && (((d >= 'A') && (d <= 'F')) || ((d >= 'a') && (d <= 'f'))))) {
                            readNextTerminal();
                            ds2 = readDigitString(hexp ? 16 : 10);
                        } //else
                        //ds2 = "";
                    }
                }
                d = nextChar();
                if(floatp) {
                    if(hexp)
                        if((d == 'p') || (d == 'P')) {
                            readNextTerminal();
                            readNextTerminal();
                            ds3 = readSignedDigitString(10);
                        } else
                            throw (new IOException(
                                    "exponent required for hexadecimal floating point number, got " + (d == -1 ? "to end of input" : "\"" + Terminal.String.
                                    escapeCodePoint(d) + "\"") + " while parsing a numeral in line " + getLineNumber()));
                    else
                        if((d == 'e') || (d == 'E')) {
                            readNextTerminal();
                            readNextTerminal();
                            ds3 = readSignedDigitString(10);
                        } else
                            ds3 = "";
                    d = nextChar();
                    if((d == 'f') || (d == 'F')) {
                        is_longorfloat = true;
                        readNextTerminal();
                    } else if((d == 'd') || (d == 'D')) {
                        is_longorfloat = false;
                        readNextTerminal();
                    }
                } else
                    if((d == 'l') || (d == 'L')) {
                        is_longorfloat = true;
                        readNextTerminal();
                    } else
                        is_longorfloat = false;
                if(floatp)
                    if(hexp)
                        if(!is_longorfloat)
                            try {
                                r = new FloatingPointLiteral(Double.parseDouble(
                                        "0x" + ds1 + "." + ds2 + "p" + ds3),
                                                             true);
                            } catch(NumberFormatException e) {
                                throw (new IOException("malformed hexadecimal double \"" + (ds1 + "." + ds2 + "p" + ds3) + "\" in line " + getLineNumber() + ":" + e.
                                        getMessage()));
                            }
                        else
                            try {
                                r = new FloatingPointLiteral(Float.parseFloat(
                                        "0x" + ds1 + "." + ds2 + "p" + ds3),
                                                             false);
                            } catch(NumberFormatException e) {
                                throw (new IOException("malformed hexadecimal float \"" + (ds1 + "." + ds2 + "p" + ds3) + "\" in line " + getLineNumber() + ":" + e.
                                        getMessage()));
                            }
                    else
                        if(!is_longorfloat)
                            try {
                                r = new FloatingPointLiteral(Double.parseDouble(ds1 + "." + ds2 + (ds3.
                                        equals("") ? "" : "e" + ds3)), true);
                            } catch(NumberFormatException e) {
                                throw (new IOException("malformed decimal double \"" + (ds1 + "." + ds2 + (ds3.
                                        equals("") ? "" : "e" + ds3)) + "\" in line " + getLineNumber() + ":" + e.
                                        getMessage()));
                            }
                        else
                            try {
                                r = new FloatingPointLiteral(Float.parseFloat(ds1 + "." + ds2 + (ds3.
                                        equals("") ? "" : "e" + ds3)), false);
                            } catch(NumberFormatException e) {
                                throw (new IOException("malformed decimal float \"" + (ds1 + "." + ds2 + (ds3.
                                        equals("") ? "" : "e" + ds3)) + "\" in line " + getLineNumber() + ":" + e.
                                        getMessage()));
                            }
                else
                    if(hexp)
                        if(is_longorfloat)
                            try {
                                r = new IntegerLiteral(parseRawHexLong(ds1),
                                                       true);
                            } catch(NumberFormatException e) {
                                throw (new IOException("malformed hexadecimal long \"" + ds1 + "\" in line " + getLineNumber() + ":" + e.
                                        getMessage()));
                            }
                        else
                            try {
                                r = new IntegerLiteral(parseRawHexInt(ds1),
                                                       false);
                            } catch(NumberFormatException e) {
                                throw (new IOException("malformed hexadecimal int \"" + ds1 + "\" in line " + getLineNumber() + ":" + e.
                                        getMessage()));
                            }
                    else if(trailing_zero)
                        if(is_longorfloat)
                            try {
                                r = new IntegerLiteral(Long.parseLong(ds1, 8),
                                                       true);
                            } catch(NumberFormatException e) {
                                throw (new IOException("malformed octal long \"" + ds1 + "\" in line " + getLineNumber() + ":" + e.
                                        getMessage()));
                            }
                        else
                            try {
                                r = new IntegerLiteral(Integer.parseInt(ds1, 8),
                                                       false);
                            } catch(NumberFormatException e) {
                                throw (new IOException("malformed octal int \"" + ds1 + "\" in line " + getLineNumber() + ":" + e.
                                        getMessage()));
                            }
                    else
                        if(is_longorfloat)
                            try {
                                r = new IntegerLiteral(Long.parseLong(ds1, 10),
                                                       true);
                            } catch(NumberFormatException e) {
                                throw (new IOException("malformed decimal long \"" + ds1 + "\" in line " + getLineNumber() + ":" + e.
                                        getMessage()));
                            }
                        else
                            try {
                                r = new IntegerLiteral(Integer.parseInt(ds1, 10),
                                                       false);
                            } catch(NumberFormatException e) {
                                throw (new IOException("malformed decimal int \"" + ds1 + "\" in line " + getLineNumber() + ":" + e.
                                        getMessage()));
                            }
            } else
                throw (new IOException(
                        "expected digit or decimal point, got " + (c == -1 ? "to end of input" : "\"" + Terminal.String.
                        escapeCodePoint(c) + "\"") + " while parsing a numeral in line " + getLineNumber()));
            return (r);
        }

        public int parseRawHexInt(java.lang.String s) {
            int r = 0;
            int i = 0, l;
            char c;
            if((s == null) || ((l = s.length()) == 0))
                throw (new NumberFormatException("at least one digit expected"));
            if(l > 8)
                throw (new NumberFormatException("too many digits"));
            while(i < l) {
                c = s.charAt(i++);
                r <<= 4;
                if((c >= '0') && (c <= '9'))
                    r += c - '0';
                else if((c >= 'A') && (c <= 'F'))
                    r += c - 'A' + 10;
                else if((c >= 'a') && (c <= 'f'))
                    r += c - 'a' + 10;
                else
                    throw (new NumberFormatException(
                            "wrong digit at position " + i));
            }
            return (r);
        }

        public long parseRawHexLong(java.lang.String s) {
            long r = 0;
            int i = 0, l;
            char c;
            if((s == null) || ((l = s.length()) == 0))
                throw (new NumberFormatException("at least one digit expected"));
            if(l > 16)
                throw (new NumberFormatException("too many digits"));
            while(i < l) {
                c = s.charAt(i++);
                r <<= 4;
                if((c >= '0') && (c <= '9'))
                    r += c - '0';
                else if((c >= 'A') && (c <= 'F'))
                    r += c - 'A' + 10;
                else if((c >= 'a') && (c <= 'f'))
                    r += c - 'a' + 10;
                else
                    throw (new NumberFormatException(
                            "wrong digit at position " + i));
            }
            return (r);
        }

        private int currentChar() throws IOException {
            return ((ct == Terminal.End) ? -1 : ((Terminal.Character)ct).getChar());
        }

        private int nextChar() throws IOException {
            return ((nt == Terminal.End) ? -1 : ((Terminal.Character)nt).getChar());
        }

        private Terminal readNextTerminal() throws IOException {
            ct = nt;
            nt = super.read();
            return (ct);
        }

        private long parseRNumber(int min_digits, int max_digits, int radix)
                throws IOException {
            long n = 0;
            int d = 0;
            int c = currentChar(), e = nextChar();
            boolean negate = false;
            if(radix <= 1)
                radix = 10;
            if((max_digits <= 0) || (max_digits < min_digits))
                return (0);
            if(c == '-') {
                negate = true;
                if(min_digits == 0)
                    min_digits = 1;
            } else if(c == '+') {
                negate = false;
                if(min_digits == 0)
                    min_digits = 1;
            } else {
                d++;
                if((c >= '0') && (c <= '9'))
                    n = n * radix + (c - '0');
                else if((c >= 'a') && (c <= 'z') && (c - 'a' < radix - 10))
                    n = n * radix + (c - 'a' + 10);
                else if((c >= 'A') && (c <= 'Z') && (c - 'A' < radix - 10))
                    n = n * radix + (c - 'A' + 10);
                else if(d <= min_digits)
                    throw (new IOException(
                            "expected " + (min_digits - d + 1) + " more digits of a base-" + radix + "-number,got " + (e == -1 ? "to end of input" : "\"" + Terminal.String.
                            escapeCodePoint(e) + "\"") + " while parsing a number in line " + getLineNumber()));
            }

            while(d++ < max_digits) {
                if((e >= '0') && (e <= '9'))
                    n = n * radix + (e - '0');
                else if((e >= 'a') && (e <= 'z') && (e - 'a' < radix - 10))
                    n = n * radix + (e - 'a' + 10);
                else if((e >= 'A') && (e <= 'Z') && (e - 'A' < radix - 10))
                    n = n * radix + (e - 'A' + 10);
                else if(d <= min_digits)
                    throw (new IOException(
                            "expected " + (min_digits - d + 1) + " more digits of a base-" + radix + "-number,got " + (e == -1 ? "to end of input" : "\"" + Terminal.String.
                            escapeCodePoint(e) + "\"") + " while parsing a number in line " + getLineNumber()));
                else
                    break;
                readNextTerminal();
                e = nextChar();
            }
            return (negate ? -n : n);
        }

        private String readSignedDigitString(int radix) throws IOException {
            int c = currentChar();
            char sign = '+';
            if((c == '-') || (c == '+')) {
                sign = (char)c;
                readNextTerminal();
            }
            return (sign + readDigitString(radix));
        }

        private String readDigitString(int radix) throws IOException {
            if(radix <= 1)
                return ("");
            StringBuilder sb = new StringBuilder();
            int c = currentChar(), e = nextChar();
            if(((c >= '0') && (c <= '9'))
               || ((c >= 'a') && (c <= 'z') && (c - 'a' < radix - 10))
               || ((c >= 'A') && (c <= 'Z') && (c - 'A' < radix - 10)))
                sb.appendCodePoint(c);
            else
                throw (new IOException(
                        "expected at least one digit, got " + (e == -1 ? "to end of input" : "\"" + Terminal.String.
                        escapeCodePoint(e) + "\"") + " while parsing a number in line " + getLineNumber()));
            while(true) {
                if(((e >= '0') && (e <= '9'))
                   || ((e >= 'a') && (e <= 'z') && (e - 'a' < radix - 10))
                   || ((e >= 'A') && (e <= 'Z') && (e - 'A' < radix - 10)))
                    sb.appendCodePoint(e);
                else
                    break;
                readNextTerminal();
                e = nextChar();
            }
            return (sb.toString());
        }

        private void skipComment() throws IOException {
            int c = currentChar(), d = nextChar();
            if(c != '/')
                throw (new IOException("expected comment, got " + Terminal.Character.
                        escapeCodePoint(c) + " in line " + getLineNumber()));
            else if(d == '/') {
                do {
                    readNextTerminal();
                    d = nextChar();
                } while((d != -1) && (d != '\n'));
                if(d != -1)
                    readNextTerminal();
            } else if(d == '*') {
                readNextTerminal();
                d = nextChar();
                while(true)
                    if(d == -1)
                        throw (new IOException(
                                "unexpected end of input while parsing a traditional comment in line " + getLineNumber()));
                    else if(d == '*') {
                        readNextTerminal();
                        d = nextChar();
                        if(d == -1)
                            throw (new IOException(
                                    "unexpected end of input while parsing a traditional comment in line " + getLineNumber()));
                        else if(d == '/') {
                            readNextTerminal();
                            break;
                        }
                    } else {
                        readNextTerminal();
                        d = nextChar();
                    }
            } else
                throw (new IOException("expected comment-spec, got " + Terminal.Character.
                        escapeCodePoint(c)));
        }

        private void skipPreProcessorDirective() throws IOException {
            int c = currentChar(), d = nextChar();
            if(c != '#')
                throw (new IOException("expected pre processor directive, got " + Terminal.Character.
                        escapeCodePoint(c) + " in line " + getLineNumber()));
            do {
                readNextTerminal();
                d = nextChar();
            } while((d != -1) && (d != '\n'));
            if(d != -1)
                readNextTerminal();
        }

        public int getCursorPosition() {
            return (cursor_pos);
        }

        public String formatCurrentLineAndClose() throws IOException {
            boolean two_line = cursor_pos > super.r.getCursorPosition();
            String prev = two_line ? super.r.getLastLine() : super.r.
                    getCurrentLine(),
                    next = two_line ? super.r.getCurrentLine() + super.r.
                    readLine() : super.r.readLine();
            super.r.close();
            if(cursor_pos == 0)
                cursor_pos = prev.length();
            String spaces = "";
            for(int i = 0; i < cursor_pos - 1; i++)
                if(prev.charAt(i) == '\t')
                    spaces += "\t";
                else if(prev.charAt(i) == '\f')
                    spaces += "\f";
                else
                    spaces += " ";
            return (prev + (two_line ? "\r\n" : "") + (next != null ? next : "") + "\r\n" + spaces + "^\r\n" + (two_line ? spaces + "|\r\n" : ""));
        }
    }
}
