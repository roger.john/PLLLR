/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package plllr;

import bbtree.Path;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author rj
 */
public class FixedThreadPoolExecutor implements ControllableExecutor {
    private final Worker[] threads;
    private volatile int status = 0;
    private final static int MAY_RUN    = 0x01;
    private final static int MAY_WORK   = 0x02;
    private volatile int ct = 0;
    public FixedThreadPoolExecutor(int numThreads) {
        this(numThreads,false);
    }
    public FixedThreadPoolExecutor(int numThreads,boolean daemon) {
        if(numThreads<0)
            throw(new IllegalArgumentException(getClass().getCanonicalName()+
                    ".constructor: number of threads must be non-negative, was "+numThreads));
        threads = new Worker[numThreads];
        status |= MAY_RUN | MAY_WORK;
        for(int i=0;i<threads.length;i++) {
            threads[i] = new Worker();
            if(daemon)
                threads[i].setDaemon(daemon);
            threads[i].start();
        }
    }
    public void execute(Runnable task) {
        if((status&MAY_RUN)==0)
            throw(new IllegalStateException(getClass().getCanonicalName()+
                    ".execute: service was shutdown"));
        if(task==null)
            throw(new NullPointerException(getClass().getCanonicalName()+
                    ".execute: task is null"));
        if(threads.length==0) {
            task.run();
            return;
        }
        int i = ct;
        synchronized(threads[i].workQueue) {
            boolean ntfy = (threads[i].workQueue.isEmpty()&&((status&MAY_WORK)!=0));
            threads[i].workQueue.getPendingQueue().append(task);
            if(ntfy)
                threads[i].workQueue.notifyAll();
        }
        ct = (i+1)%threads.length;
    }
    public void start() {
        if((status&MAY_RUN)==0)
            throw(new IllegalStateException(getClass().getCanonicalName()+
                    ".start: service was shutdown"));
        if((status&MAY_WORK)!=0)
            throw(new IllegalStateException(getClass().getCanonicalName()+
                    ".start: service was already started"));
        status |= MAY_WORK;
        for(int i=0;i<threads.length;i++) {
            synchronized(threads[i].workQueue) {
                threads[i].workQueue.notifyAll();
            }
        }
    }
    public void stop() {
        if((status&MAY_RUN)==0)
            throw(new IllegalStateException(getClass().getCanonicalName()+
                    ".stop: service was shutdown"));
        if((status&MAY_WORK)==0)
            throw(new IllegalStateException(getClass().getCanonicalName()+
                    ".start: service was already stopped"));
        status &= ~MAY_WORK;
    }
    public void shutDown() {
        if((status&MAY_RUN)==0)
            throw(new IllegalStateException(getClass().getCanonicalName()+
                    ".stop: service was already shutdown"));
        status = 0;
        for(int i=0;i<threads.length;i++) {
            synchronized(threads[i].workQueue) {
                threads[i].workQueue.notifyAll();
            }
        }
    }
    public boolean isStarted() {
        return((status&MAY_WORK)!=0);
    }
    public boolean isShutDown() {
        return((status&MAY_RUN)==0);
    }
    public boolean isTerminated() {
        if((status&MAY_RUN)!=0)
            return(false);
        for(int i=0;i<threads.length;i++)
            if(threads[i].isAlive())
                return(false);
        return(true);
    }
    public int pendingTasks() {
        int r = 0;
        for(int i=0;i<threads.length;i++)
            synchronized(threads[i].workQueue) {
                r += threads[i].workQueue.getActiveQueue().length();
                r += threads[i].workQueue.getPendingQueue().length();
            }
        return(r);
    }
    public int threadCount() {
        return(threads!=null?threads.length:0);
    }
    public void assertCompleted(String msg) {
        for(int i=0;i<threads.length;i++)
            synchronized(threads[i].workQueue) {
                if(threads[i].getState()==Thread.State.RUNNABLE)
                    throw(new RuntimeException(msg));
            }
    }
    public void awaitCompletion() {
        for(int i=0;i<threads.length;i++)
            synchronized(threads[i].workQueue) {
                while(threads[i].isAlive()&&
                        !(threads[i].workQueue.isEmpty()&&threads[i].workQueue.isWaiting))
                    try { threads[i].workQueue.wait(); }
                    catch(InterruptedException e) { throw new RuntimeException(
                            getClass().getCanonicalName()+".awaitCompletion: "+
                            " the thread "+Thread.currentThread()+" was interrupted"); }
                if(!threads[i].isAlive())
                    throw(new RuntimeException("Thread "+threads[i]+" was killed."));
            }
    }
    public void awaitTermination() {
        if((status&MAY_RUN)!=0)
            throw(new IllegalStateException(getClass().getCanonicalName()+
                    ".awaitTermination: service was not shutdown yet"));
        for(int i=0;i<threads.length;i++)
            synchronized(threads[i]) {
                while(threads[i].isAlive())
                    try { threads[i].wait(); }
                    catch(InterruptedException e) { throw new RuntimeException(
                            "The thread "+Thread.currentThread()+" was interrupted"); }
            }
    }
    private class Worker extends Thread {
        private final SwitchingQueue<Runnable> workQueue = new SwitchingQueue<Runnable>();
        long exTime = System.nanoTime();
        public synchronized void run() {
            try {
                Path<Runnable> tasks = null;
                while(true) {
                    tasks = getTasks();
                    if(tasks==null)
                        break;
                    while(!tasks.isEmpty())
                        tasks.pop().run();
                }
            } finally { notifyAll(); synchronized(workQueue) { workQueue.notifyAll(); } }
        }
        private Path<Runnable> getTasks() {
            synchronized(workQueue) {
                if(!workQueue.activeIsEmpty())
                    return(workQueue.getActiveQueue());
                exTime = System.nanoTime()-exTime;
                workQueue.isWaiting = true;
                while(((status&MAY_RUN)!=0)&&
                        (((status&MAY_WORK)==0)||workQueue.pendingIsEmpty())) {
                    if((status&MAY_WORK)!=0)
                        workQueue.notifyAll();
                    try { workQueue.wait(); }
                    catch(InterruptedException e) {}
                }
                workQueue.isWaiting = false;
                if((status&MAY_RUN)==0)
                    return(null);
                workQueue.switchQueues();
                exTime = System.nanoTime();
                return(workQueue.getActiveQueue());
            }
        }
    }
    private static class SwitchingQueue<A> {
        private Path<A> active,pending;
        private boolean isWaiting = false;
        public SwitchingQueue() {
            active = new Path<A>();
            pending = new Path<A>();
        }
        public synchronized void switchQueues() {
            Path<A> h = active;
            active = pending;
            pending = h;
            if(!active.isEmpty())
                notifyAll();
        }
        public synchronized boolean isEmpty() {
            return(active.isEmpty()&&pending.isEmpty());
        }
        public boolean activeIsEmpty() {
            return(active.isEmpty());
        }
        public boolean pendingIsEmpty() {
            return(pending.isEmpty());
        }
        public Path<A> getActiveQueue() {
            return(active);
        }
        public Path<A> getPendingQueue() {
            return(pending);
        }
    }
}
