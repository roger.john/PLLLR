/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package plllr;

import bbtree.Writeable;
import java.io.*;

/**
 *
 * @author RJ
 */
public class Tuple<A extends Comparable<? super A>> implements
        Comparable<Tuple<A>>, Serializable, Writeable {

    private static final long serialVersionUID = 0L;
    public Object[] members;

    public Tuple(int k) {
        this.members = new Object[k];
    }

    public Tuple(Object[] x) {
        this(x, false);
    }

    public Tuple(Object[] x, boolean copy) {
        if((x != null) && (x.length > 0))
            if(copy) {
                members = new Object[x.length];
                System.arraycopy(x, 0, members, 0, members.length);
            } else
                this.members = x;
    }

    public StringBuilder toStringBuilder(StringBuilder s) {
        if(s==null)
            s = new StringBuilder();
        if((members == null) || (members.length == 0))
            return s.append("()");
        s.append('(');
        for(int i = 0; i < members.length; i++) {
            if((members[i] != null))
                s.append(members[i].toString());
            if(i + 1 < members.length)
                s.append(',');
        }
        return s.append(')');
    }
    public String toString() {
        if((members == null) || (members.length == 0))
            return ("()");
        return toStringBuilder(new StringBuilder()).toString();
    }

    public void writeToStream(Writer w) throws IOException {
        if(w == null)
            return;
        w.write(toString());
    }

    public boolean equals(Object o) {
        return ((this == o) || (o instanceof Tuple ? compareTo((Tuple)o) == 0 : false));
    }

    public int hashCode() {
        if(members == null)
            return (0);
        int sum = 0;
        for(int i = 0; i < members.length; i++)
            if(members[i] != null)
                sum += members[i].hashCode();
        return (sum);
    }

    public int compareTo(Tuple<A> x) {
        if(x == null)
            return (1);
        if((members == null) || (members.length == 0))
            return (x.members == null ? 0 : -x.members.length);
        if(x.members == null)
            return (1);
        int c, i, m = members.length <= x.members.length ? members.length : x.members.length;
        for(i = 0; i < m; i++) {
            if(members[i] != null)
                c = ((A)members[i]).compareTo((A)(x.members[i]));
            else
                c = (x.members[i] == null) ? 0 : -1;
            if(c != 0)
                return (c);
        }
        return (members.length - x.members.length);
    }

    public int size() {
        return (members == null ? 0 : members.length);
    }

    public int setSize(int k) {
        int l = members == null ? 0 : members.length;
        if(k != l) {
            Object[] n;
            if(k < l)
                if(k <= 0)
                    members = null;
                else {
                    n = new Object[k];
                    System.arraycopy(members, 0, n, 0, k);
                }
            else {
                n = new Object[k];
                System.arraycopy(members, 0, n, 0, l);
            }
        }
        return (l);
    }

    public A get(int i) {
        return ((A)members[i]);
    }

    public A set(int i, A x) {
        A y = (A)members[i];
        members[i] = x;
        return (y);
    }
}
