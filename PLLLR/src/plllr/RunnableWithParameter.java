/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package plllr;

/**
 *
 * @author rj
 */
public interface RunnableWithParameter<A> {
    public void run(A param);
    public static interface ParameterFactory<A> {
        public A createParameter(Thread t);
        public A lastCreated();
        public void reset();
    }
}
