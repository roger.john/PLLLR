/*
 * IntCounter.java
 *
 * Created on 14. November 2007, 14:51
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package plllr;

/**
 *
 * @author RJ
 */
public class IntCounter {
    
    public volatile int val;
    
    /** Creates a new instance of IntCounter */
    public IntCounter() {
        this(0);
    }
    public IntCounter(int val) {
        this.val = val;
    }
    public int get() {
        return(val);
    }
    public synchronized int set(int nval) {
        int h = val;
        val = nval;
        if(val==0)
            notifyAll();
        return(h);
    }
    public synchronized IntCounter add(int x) {
        val += x;
        if(val==0)
            notifyAll();
        return(this);
    }
    public synchronized IntCounter subtract(int x) {
        val -= x;
        if(val==0)
            notifyAll();
        return(this);
    }
    public synchronized IntCounter invert(int x) {
        val = -val;
        if(val==0)
            notifyAll();
        return(this);
    }
    public synchronized IntCounter inc() {
        val++;
        if(val==0)
            notifyAll();
        return(this);
    }
    public synchronized IntCounter dec() {
        val--;
        if(val==0)
            notifyAll();
        return(this);
    }
    public synchronized void awaitZero() {
        while(val!=0)
           //System.out.print("("+val+")");
            try { wait(); }
            catch(InterruptedException e) {
                throw(new RuntimeException(getClass().getCanonicalName()+".awaitZero: the "+
                        "thread "+Thread.currentThread()+" was interrupted"));
            }

    }
    public String toString() {
        return String.valueOf(val);
    }
}
