/*
 * TerminalToRuleMap.java
 *
 * Created on 1. Oktober 2007, 15:29
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */
package plllr;

import bbtree.*;
import bbtree.Utils.AsymmetricComparable;
import java.io.*;
import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 *
 * @author RJ
 */
public class TerminalToRuleMap implements Writeable, Serializable {

    private static final long serialVersionUID = 0L;
    Traversable.Set<TEntry> s, sc;

    /** Creates a new instance of TerminalToRuleMap */
    public TerminalToRuleMap() {
    }

    public void makeFinal() {
        if((s != null) && !(s instanceof Final)) {
            s = new Final<TEntry>(s);
            for(TEntry x : s)
                x.makeFinal();
        }
        if((sc != null) && !(sc instanceof Final)) {
            sc = new Final<TEntry>(sc);
            for(TEntry x : sc)
                x.makeFinal();
        }
    }

    public boolean isEmpty() {
        return (s == null || s.isEmpty()) && (sc == null || sc.isEmpty());
    }

    public boolean addEntry(Terminal x, Rule r, int p) {
        if((x == null) || (r == null) || (p < 0) || (p >= r.rhsLength()))
            return (false);
        TEntry t;
        if(x instanceof Terminal.CharacterClass) {
            if(((Terminal.CharacterClass)x).isEmpty())
                return (false);
            if(sc == null)
                sc = new BBTree<TEntry>();
            t = getCEntry(x);
            if(t == null) {
                if(sc instanceof Final)
                    sc = sc.clone();
                t = new TEntry(x, new BBTree<RuleEntry>());
                t.s.add(new RuleEntry(r, new int[] {p}));
                sc.add(t);
                return (true);
            }

        } else {
            if(s == null)
                s = new BBTree<TEntry>();
            t = getEntry(x);
            if(t == null) {
                if(s instanceof Final)
                    s = s.clone();
                t = new TEntry(x, new BBTree<RuleEntry>());
                t.s.add(new RuleEntry(r, new int[] {p}));
                s.add(t);
                return (true);
            }
        }
        return (t.addRuleEntry(r, p));
        /*if(t.s==null) {
        t.s = new BBTree<RuleEntry>();
        t.s.add(new RuleEntry(r,new int[] {p}));
        return(true);
        }
        RuleEntry u = t.s.search(new RuleEntry(r));
        if(u==null) {
        if(!(t.s instanceof BBTree))
        t.s = t.s.clone();
        t.s.add(new RuleEntry(r,new int[] {p}));
        return(true);
        } else {*/
        /*
        if(u.p==null) {
        u.p = new int[] {p};
        return(true);
        } else {
        int q = java.util.Arrays.binarySearch(u.p,p);
        if(q>=0)
        return(false);
        q = -q-1;
        int[] n = new int[u.p.length+1];
        System.arraycopy(u.p,0,n,0,q);
        n[q] = p;
        System.arraycopy(u.p,q,n,q+1,u.p.length-q);
        u.p = n;
        return(false);
        }*//*
        return(u.addIndex(p));
        }*/
    }

    public Traversable.Set<RuleEntry> getEntryFor(Terminal x) {
        Traversable.Set<RuleEntry> r = null;
        TEntry y = getEntry(x);
        if((y != null) && (y.s != null))
            r = y.s.clone();
        if((sc != null) && (!sc.isEmpty()))
            for(TEntry z : sc)
                if(z.x.matches(x))
                    if(r == null)
                        r = z.s.clone();
                    else
                        r.addAll(z.s);
        return (r);
//        if(r!=null)
//            return(new OrderedSet.Final<RuleEntry>(r));
//        else
//            return(null);
    }

    public Iterator<RuleEntry> getRuleEntryIteratorFor(Terminal x) {
        if(x == null)
            return (null);
        return (new RuleEntryIterator(this, x));
    }

    public int traverseMatchingRuleEntries(Terminal x,
                                           TraversalActionPerformer<RuleEntry> t,
                                           Object o) {
        if(t == null)
            return (0);
        MatchingTEntryTraverser mtt = MatchingTEntryTraverser.TLV.get();
        int c = mtt.c;
        mtt.c = 0;
        TraversalActionPerformer<RuleEntry> ttt = mtt.t;
        mtt.t = t;
        Terminal y = mtt.x;
        mtt.x = x;
        if(x == null) {
            if(s != null)
                s.traverse(Traversable.IN_ORDER, mtt, o);
            if(sc != null)
                sc.traverse(Traversable.IN_ORDER, mtt, o);
        } else {
            if(s != null) {
                TEntry te = s.getMatch(x);
                if(te != null)
                    mtt.perform(te, Traversable.IN_ORDER, o);
            }
            if(sc != null)
                sc.traverseSelected(Traversable.IN_ORDER, mtt, o, mtt);
        }
        mtt.x = y;
        mtt.t = ttt;
        int r = mtt.c;
        mtt.c = c;
        return r;
    }

    public int countMatchingEntries(Terminal x) {
        return traverseMatchingRuleEntries(x, null, null);
    }

    private TEntry getEntry(Terminal x) {
        if((x == null) || (s == null) || s.isEmpty())
            return (null);
        TEntry y = s.getMatch(x);
        if((y == null) || (y.s == null) || y.s.isEmpty())
            return (null);
        return (y);
    }

    private TEntry getCEntry(Terminal x) {
        if((x == null) || (!(x instanceof Terminal.CharacterClass)) || ((Terminal.CharacterClass)x).
                isEmpty()
           || (sc == null) || sc.isEmpty())
            return (null);
        TEntry y = sc.getMatch(x);
        if((y == null) || (y.s == null) || y.s.isEmpty())
            return (null);
        return (y);
    }

    public String toString() {
        return toStringBuilder(null).toString();
    }

    public StringBuilder toStringBuilder(StringBuilder sb) {
        if(sb == null)
            sb = new StringBuilder();
        sb.append("(");
        if(s == null)
            sb.append("{}");
        else
            s.toStringBuilder(sb);
        sb.append(",");
        if(sc == null)
            sb.append("{}");
        else
            sc.toStringBuilder(sb);
        return sb.append(")");
    }

    public void writeToStream(Writer w) throws IOException {
        if(w == null)
            return;
        w.write('(');
        if(s == null) {
            w.write('{');
            w.write('}');
        } else
            s.writeToStream(w);
        w.write(',');
        if(sc == null) {
            w.write('{');
            w.write('}');
        } else
            sc.writeToStream(w);
        w.write(')');
    }

    public static class RuleEntry implements Comparable<RuleEntry>,
                                             AsymmetricComparable<Rule>,
                                             Writeable, Serializable {

        private static final long serialVersionUID = 0L;
        public Rule r;
        public int[] p;

        public RuleEntry() {
        }

        public RuleEntry(Rule r) {
            this.r = r;
        }

        public RuleEntry(Rule r, int[] p) {
            this.r = r;
            this.p = p;
        }

        public boolean addIndex(int pos) {
            if((pos < 0) || (pos >= (r != null ? r.rhsLength() : 0)))
                return (false);
            if(p == null) {
                p = new int[] {pos};
                return (true);
            }
            int q = java.util.Arrays.binarySearch(p, pos);
            if(q >= 0)
                return (false);
            q = -q - 1;
            int[] n = new int[p.length + 1];
            System.arraycopy(p, 0, n, 0, q);
            n[q] = pos;
            System.arraycopy(p, q, n, q + 1, p.length - q);
            p = n;
            return (true);
        }

        public int compareTo(RuleEntry x) {
            if(x == null)
                return (1);
            if(this.r == null)
                return (x.r == null ? 0 : -1);
            return (this.r.compareTo(x.r));
        }

        public int asymmetricCompareTo(Rule x) {
            if(this.r == null)
                return (x == null ? 0 : -1);
            return (this.r.compareTo(x));
        }

        public String toString() {
            return toStringBuilder(null).toString();
        }

        public StringBuilder toStringBuilder(StringBuilder sb) {
            if(sb == null)
                sb = new StringBuilder();
            sb.append('(');
            if(r != null)
                sb.append(r.id);
            else
                sb.append("null");
            sb.append(',');
            sb.append('[');
            if((p != null) && (p.length != 0))
                for(int i = 0; i < p.length; i++) {
                    sb.append(p[i]);
                    if(i < p.length - 1)
                        sb.append(',');
                }
            sb.append(']');
            sb.append(')');
            return sb;
        }

        public void writeToStream(Writer w) throws IOException {
            if(w == null)
                return;
            w.write('(');
            if(r != null)
                w.write(Integer.toString(r.id));
            w.write(',');
            w.write('[');
            if((p != null) && (p.length != 0))
                for(int i = 0; i < p.length; i++) {
                    w.write(Integer.toString(p[i]));
                    if(i < p.length - 1)
                        w.write(',');
                }
            w.write(']');
            w.write(')');
        }
    }

    static class TEntry implements Comparable<TEntry>,
                                   AsymmetricComparable<Terminal>, Writeable,
                                   Serializable {

        private static final long serialVersionUID = 0L;
        Terminal x;
        Traversable.Set<RuleEntry> s;

        public TEntry() {
        }

        public TEntry(Terminal x) {
            this.x = x;
        }

        public TEntry(Terminal x, Traversable.Set<RuleEntry> s) {
            this.x = x;
            this.s = s;
        }

        public void makeFinal() {
            if(s != null && !(s instanceof Final))
                s = s.toFinal();
        }

        public boolean addRuleEntry(Rule r, int p) {
            if((r == null) || (p < 0) || (p >= r.rhsLength()))
                return (false);
            if(s == null) {
                s = new BBTree<RuleEntry>();
                s.add(new RuleEntry(r, new int[] {p}));
                return (true);
            }
            RuleEntry u = s.getMatch(r);
            if(u == null) {
                if(!(s instanceof BBTree))
                    s = s.clone();
                s.add(new RuleEntry(r, new int[] {p}));
                return (true);
            } else
                return (u.addIndex(p));
        }

        public int compareTo(TEntry x) {
            if(x == null)
                return (1);
            if(this.x == null)
                return (x.x == null ? 0 : -1);
            return (this.x.compareTo(x.x));
        }

        public int asymmetricCompareTo(Terminal x) {
            if(this.x == null)
                return (x == null ? 0 : -1);
            return (this.x.compareTo(x));
        }

        public String toString() {
            return toStringBuilder(null).toString();
        }

        public StringBuilder toStringBuilder(StringBuilder sb) {
            if(sb == null)
                sb = new StringBuilder();
            sb.append('(').append(x).append(',');
            if(s != null)
                s.toStringBuilder(sb);
            else
                sb.append("{}");
            return sb.append(')');
        }

        public void writeToStream(Writer w) throws IOException {
            if(w == null)
                return;
            w.write('(');
            if(x != null)
                x.writeToStream(w);
            w.write(',');
            if(s != null)
                s.writeToStream(w);
            else
                w.write("{}");
            w.write(')');
        }
    }

    private static class RuleEntryIterator implements Iterator<RuleEntry> {

        private TerminalToRuleMap m;
        private Terminal x;
        private RuleEntry nr;
        private Iterator<TEntry> it;
        private Iterator<RuleEntry> ir;
        private boolean has_next;

        public RuleEntryIterator(TerminalToRuleMap m, Terminal x) {
            this.m = m;
            this.x = x;
            if(m.sc != null)
                it = m.sc.iterator();
            else
                it = DUMMYTENTRYITERATOR;
            TEntry a = m.getEntry(x);
            has_next = false;
            if(a != null) {
                ir = a.s.iterator();
                has_next = ir.hasNext();
            }
            while(!has_next && it.hasNext()) {
                a = it.next();
                if(a.x.matches(x) && (a.s != null)) {
                    ir = a.s.iterator();
                    has_next = ir.hasNext();
                }
            }

        }

        public boolean hasNext() {
            return (has_next);
        }

        public RuleEntry next() {
            if(!has_next)
                throw (new NoSuchElementException(
                        "pll.TerminalToRuleMap.RuleEntryIterator: no more elements to iterate"));
            RuleEntry r = ir.next();
            TEntry a;
            has_next = ir.hasNext();
            while(!has_next && it.hasNext()) {
                a = it.next();
                if(a.x.matches(x) && (a.s != null)) {
                    ir = a.s.iterator();
                    has_next = ir.hasNext();
                }
            }
            return (r);
        }

        public void remove() {
            throw (new UnsupportedOperationException(
                    "pll.TerminalToRuleMap.RuleEntryIterator: removal not supported"));
        }
    }
    private static final Iterator<TEntry> DUMMYTENTRYITERATOR = new Iterator<TEntry>() {

        public boolean hasNext() {
            return (false);
        }

        public TEntry next() {
            throw (new NoSuchElementException(
                    "pll.TerminalToRuleMap.DUMMYTENTRYITERATOR: no more elements to iterate"));
        }

        public void remove() {
            throw (new UnsupportedOperationException(
                    "pll.TerminalToRuleMap.DUMMYTENTRYITERATOR: removal not supported"));
        }
    };

    private static class MatchingTEntryTraverser implements Selector<TEntry>,
                                                            TraversalActionPerformer<TEntry> {

        private Terminal x;
        private TraversalActionPerformer<RuleEntry> t;
        private int c;

        private MatchingTEntryTraverser init(Terminal x,
                                             TraversalActionPerformer<RuleEntry> t) {
            this.x = x;
            this.t = t;
            this.c = 0;
            return (this);
        }

        public boolean selects(TEntry n) {
            return ((n != null) && (n.x != null) && (n.x.matches(x)));
        }

        public TEntry upperBound() {
            return (null);
        }

        public TEntry lowerBound() {
            return (null);
        }

        public boolean perform(TEntry te, int order, Object o) {
//            if(!te.x.matches(x))
//                return(false);
//            if(te.s==null)
//                return(false);
            if(t != null)
                c += te.s.traverse(IN_ORDER, t, o);
            return false;
        }
        private static ThreadLocal<MatchingTEntryTraverser> TLV =
                new ThreadLocal<MatchingTEntryTraverser>() {

                    protected MatchingTEntryTraverser initialValue() {
                        return (new MatchingTEntryTraverser());
                    }
                };
    }
}
