/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package plllr;

/**
 *
 * @author rj
 */
public interface ControllableExecutor extends java.util.concurrent.Executor {
    public void start();
    public void stop();
    public void shutDown();
    public boolean isShutDown();
    public boolean isStarted();
    public boolean isTerminated();
    public void awaitTermination();
    public void awaitCompletion();
    public int pendingTasks();
    public int threadCount();
    public void assertCompleted(String msg);
}
