/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package plllr;

import bbtree.*;
import bbtree.OrderedSet.*;
import bbtree.Utils.AsymmetricComparable;
import bbtree.Utils.KeyedElementFactory;
import java.io.IOException;
import java.io.Serializable;
import java.io.Writer;
import java.util.Collection;
import java.util.Comparator;

/**
 *
 * @author RJ
 */
public class TerminalTrie implements Cloneable, Writeable, Serializable,
                                     Iterable<Path<Terminal>>,
                                     Comparable<TerminalTrie>,
                                     Traversable<Path<Terminal>> {

    private static final long serialVersionUID = 0L;
    public static boolean TABBED_WRITING = false;
    /*private*/ static class NodeSet implements Iterable<Node>,
                                    Traversable<Node>, Comparable<NodeSet>,
                                    Serializable {

        private static final long serialVersionUID = 0L;
        Traversable.Set<Node> NC, CC = null;

        public NodeSet() {
            NC = new BBTree<Node>();
        }

        NodeSet(Traversable.Set<Node> nc, Traversable.Set<Node> cc) {
            if(nc == null)
                throw new NullPointerException("NC is null");
            NC = nc;
            CC = cc;
        }
//        NodeSet(Path<Node> p) {
//            if(p!=null) {
//                int in = 0,ic=0,c;
//                boolean sortedNC = true,sortedCC = true;
//                Node lastNC = null,lastCC = null;
//                Path.Node<Node> n = p.head;
//                while(n!=null) {
//                    if(n.x!=null) {
//                        if((n.x.x!=null)&&(n.x.x instanceof Terminal.CharacterClass)) {
//                            c = lastCC!=null?lastCC.compareTo(n.x):-1;
//                            if(c>0)
//                                sortedCC = false;
//                            if(c==0)
//                                throw(new IllegalArgumentException("Duplicate entry discovered"));
//                            ic++;
//                            lastCC = n.x;
//                        } else {
//                            c = lastNC!=null?lastNC.compareTo(n.x):-1;
//                            if(c>0)
//                                sortedNC = false;
//                            if(c==0)
//                                throw(new IllegalArgumentException("Duplicate entry discovered"));
//                            in++;
//                            lastNC = n.x;
//                        }
//                    }
//                    n = n.next;
//                }
//                if((in!=0)||(ic!=0)) {
//                    Object[] bNC = in>0?new Object[in]:null,bCC = ic>0?new Object[ic]:null;
//                    n = p.head;
//                    in = ic = 0;
//                    while(n!=null) {
//                        if(n.x!=null) {
//                            if((n.x.x!=null)&&(n.x.x instanceof Terminal.CharacterClass))
//                                bCC[ic++] = n.x;
//                            else
//                                bNC[in++] = n.x;
//                        }
//                        n = n.next;
//                    }
//                    NC = new Final<Node>(bNC,null,sortedNC);
//                    if(bCC!=null)
//                        CC = new Final<Node>(bCC,null,sortedCC);
//                } else
//                    NC = new Final<Node>((Object[])null);
//            } else
//                NC = new Final<Node>((Object[])null);
//        }

        public boolean isFinal() {
            return NC.isFinal() && ((CC == null) || CC.isFinal());
        }

        public void makeFinal() {
            if(!NC.isFinal()) {
                NC = NC.toFinal();
                for(Node x : NC)
                    x.makeFinal();
            }
            if((CC != null) && !CC.isFinal()) {
                CC = CC.toFinal();
                for(Node x : CC)
                    x.makeFinal();
            }
        }

        public int size() {
            int r = NC.size();
            if(CC != null)
                r += CC.size();
            return r;
        }

        public boolean contains(Node x) {
            if(x == null)
                return false;
            if(x == EndNode)
                return NC.contains(x);
            if(x.x == null)
                return false;
            if(x.x instanceof Terminal.CharacterClass)
                return CC != null ? CC.contains(x) : false;
            return NC.contains(x);
        }

        public Node search(Terminal x) {
            if(x == null)
                return NC.get(EndNode);
            if(x instanceof Terminal.CharacterClass)
                return CC != null ? CC.getMatch(x) : null;
            return NC.getMatch(x);
        }

        public Node ceil(Terminal x) {
            if(x == null)
                return NC.isEmpty() && (CC != null) ? CC.first() : NC.first();
            if(x instanceof Terminal.CharacterClass) {
                if(CC == null)
                    return null;
                return CC.ceilMatch(x);
            }
            Node y = NC.ceilMatch(x);
            if((y == null) && (CC != null))
                return CC.first();
            return y;
        }

        public Node floor(Terminal x) {
            if(x == null)
                return CC != null ? CC.last() : NC.last();
            if((x instanceof Terminal.CharacterClass) && (CC != null)) {
                Node y = CC.floorMatch(x);
                if(y != null)
                    return y;
                else
                    return NC.last();
            }
            return NC.floorMatch(x);
        }

        public boolean add(Node x) {
            if(x == null)
                return false;
            if(x == EndNode)
                return NC.add(EndNode);
            if(x.x instanceof Terminal.CharacterClass) {
                if(CC == null)
                    CC = new BBTree<Node>();
                return CC.add(x);
            } else
                return NC.add(x);
        }

        public boolean add(Terminal x) {
            if(x == null)
                return NC.add(EndNode);
            if(x instanceof Terminal.CharacterClass) {
                if(CC == null)
                    CC = new BBTree<Node>();
                Node.Factory f = NODE_FACTORY.get();
                Node y = CC.addOrGetMatch(x, f);
                return y == f.lastCreated();
            } else {
                Node.Factory f = NODE_FACTORY.get();
                Node y = NC.addOrGetMatch(x, f);
                return y == f.lastCreated();
            }
        }

        public boolean addAll(NodeSet x) {
            if(x == null)
                return false;
            boolean c = NC.addAll(x.NC);
            if(x.CC != null) {
                if(CC == null)
                    CC = new BBTree<Node>();
                c |= CC.addAll(x.CC);
            }
            return c;
        }

        public Node addOrGet(Node x) {
            if(x == null)
                return null;
            if(x == EndNode)
                return NC.addOrGet(EndNode);
            if(x.x instanceof Terminal.CharacterClass) {
                if(CC == null)
                    CC = new BBTree<Node>();
                return CC.addOrGet(x);
            } else
                return NC.addOrGet(x);

        }

        public Node addOrGet(Terminal x, Node.Factory f) {
            if(f == null)
                f = NODE_FACTORY.get();
            if(x == null) {
                if(NC.add(EndNode))
                    f.last = EndNode;
                else
                    f.last = null;
                return EndNode;
            }
            if(x instanceof Terminal.CharacterClass) {
                if(CC == null)
                    CC = new BBTree<Node>();
                return CC.addOrGetMatch(x, f);
            } else
                return NC.addOrGetMatch(x, f);
        }

        public Node get(Node x) {
            if(x == null)
                return null;
            if(x == EndNode)
                return NC.get(EndNode);
            if(x.x instanceof Terminal.CharacterClass)
                if(CC != null)
                    return CC.get(x);
                else
                    return null;
            else
                return NC.get(x);
        }

        public boolean remove(Node x) {
            if(x == null)
                return false;
            if(x == EndNode)
                return NC.remove(EndNode);
            if(x.x instanceof Terminal.CharacterClass)
                if(CC != null)
                    return CC.remove(x);
                else
                    return false;
            else
                return NC.remove(x);
        }

        public Node delete(Terminal x) {
            if((x == null) || (x == Terminal.Epsilon))
                if(NC.remove(EndNode))
                    return EndNode;
                else
                    return null;
            if(x instanceof Terminal.CharacterClass)
                if(CC != null)
                    return CC.removeMatch(x);
                else
                    return null;
            else
                return NC.removeMatch(x);
        }

        public void clear() {
            NC.clear();
            if(CC != null)
                CC.clear();
            CC = null;
        }

        public boolean isEmpty() {
            return NC.isEmpty() && ((CC == null) || CC.isEmpty());
        }

        public ResettableIterator<Node> iterator() {
            if(CC != null)
                return new CompoundIterator<Node>(NC, CC);
            else
                return NC.iterator();
        }

        public String toString() {
            return "(" + NC + (CC != null ? "," + CC : "") + ")";
        }

        public int traverse(int order, TraversalActionPerformer<? super Node> p) {
            return traverse(order, p, null);
        }

        public int traverse(int order, TraversalActionPerformer<? super Node> p,
                            Object o) {
            int r = NC.traverse(order, p, o);
            if(CC != null)
                r += CC.traverse(order, p, o);
            return r;
        }

        public int traverseSelected(int order,
                                    TraversalActionPerformer<? super Node> p,
                                    Selector<Node> s) {
            return traverseSelected(order, p, null, s);
        }

        public int traverseSelected(int order,
                                    TraversalActionPerformer<? super Node> p,
                                    Object o, Selector<Node> s) {
            int r = NC.traverseSelected(order, p, o, s);
            if(CC != null)
                r += CC.traverseSelected(order, p, o, s);
            return r;
        }

        public int traverseRange(int order,
                                 TraversalActionPerformer<? super Node> p,
                                 Node l, Node r) {
            return traverseRange(order, p, null, l, r);
        }

        public int traverseRange(int order,
                                 TraversalActionPerformer<? super Node> p,
                                 Object o, Node l, Node r) {
            int rs = NC.traverseRange(order, p, o, l, r);
            if(CC != null)
                rs += CC.traverseRange(order, p, o, l, r);
            return rs;
        }

        public int traverseRange(int order,
                                 TraversalActionPerformer<? super Node> p,
                                 Node l, boolean left_included, Node r,
                                 boolean right_included) {
            return traverseRange(order, p, null, l, left_included, r,
                                  right_included);
        }

        public int traverseRange(int order,
                                 TraversalActionPerformer<? super Node> p,
                                 Object o, Node l, boolean left_included, Node r,
                                 boolean right_included) {
            int rs = NC.traverseRange(order, p, o, l, left_included, r,
                                      right_included);
            if(CC != null)
                rs += CC.traverseRange(order, p, o, l, left_included, r,
                                       right_included);
            return rs;
        }

        public int traverseSelectedRange(int order,
                                         TraversalActionPerformer<? super Node> p,
                                         Selector<Node> s, Node l, Node r) {
            return traverseSelectedRange(order, p, null, s, l, r);
        }

        public int traverseSelectedRange(int order,
                                         TraversalActionPerformer<? super Node> p,
                                         Object o, Selector<Node> s, Node l,
                                         Node r) {
            int rs = NC.traverseSelectedRange(order, p, o, s, l, r);
            if(CC != null)
                rs += CC.traverseSelectedRange(order, p, o, s, l, r);
            return rs;
        }

        public int traverseSelectedRange(int order,
                                         TraversalActionPerformer<? super Node> p,
                                         Selector<Node> s, Node l,
                                         boolean left_included,
                                         Node r, boolean right_included) {
            return traverseSelectedRange(order, p, null, s, l, left_included, r,
                                          right_included);
        }

        public int traverseSelectedRange(int order,
                                         TraversalActionPerformer<? super Node> p,
                                         Object o, Selector<Node> s, Node l,
                                         boolean left_included,
                                         Node r, boolean right_included) {
            int rs = NC.traverseSelectedRange(order, p, o, s, l, left_included,
                                              r, right_included);
            if(CC != null)
                rs += CC.traverseSelectedRange(order, p, o, s, l, left_included,
                                               r, right_included);
            return rs;
        }

        public int traverseMatches(Terminal x,
                                   TraversalActionPerformer<? super Node> p) {
            return traverseMatches(x, p, null);
        }

        public int traverseMatches(Terminal x,
                                   TraversalActionPerformer<? super Node> p,
                                   Object o) {
            if((p == null) || (x == null))
                return 0;
            int rs = 0;
            Node m = search(x);
            if(m != null) {
                rs++;
                p.perform(m, IN_ORDER, o);
            }
            if(CC != null) {
                MatchSelector ms = MatchSelector.TLV.get();
                Terminal y = ms.x;
                ms.x = x;
                rs += CC.traverseSelected(IN_ORDER, p, o, ms);
                ms.x = y;
            }
            return rs;
        }

        private static class MatchSelector implements Selector<Node> {

            private Terminal x;

            private MatchSelector init(Terminal x) {
                this.x = x;
                return this;
            }

            public boolean selects(Node n) {
                return (n != null) && (n.x != null) && (n.x.matches(x));
            }

            public Node upperBound() {
                return null;
            }

            public Node lowerBound() {
                return null;
            }

            private static MatchSelector getThreadLocal(Terminal x) {
                return TLV.get().init(x);
            }
            private static ThreadLocal<MatchSelector> TLV =
                    new ThreadLocal<MatchSelector>() {

                        protected MatchSelector initialValue() {
                            return new MatchSelector();
                        }
                    };
        }

        public int emptyTo(TraversalActionPerformer<? super Node> p) {
            return emptyTo(p, null);
        }

        public int emptyTo(TraversalActionPerformer<? super Node> p, Object o) {
            if(p == null)
                throw new NullPointerException("Performer is null");
            int h = traverse(IN_ORDER, p, o);
            clear();
            return h;
        }

        public int compareTo(NodeSet o) {
            return compareTo(o, NC);
        }

        public int compareTo(NodeSet o, Comparator<Node> d) {
            if(o == null)
                return 1;
            int c = NC.compareTo(o.NC, d);
            if(c != 0)
                return c;
            if(CC == null)
                return o.CC == null ? 0 : -1;
            return CC.compareTo(o.CC, d);
        }
    }
    private static final ThreadLocal<Node.Factory> NODE_FACTORY =
            new ThreadLocal<Node.Factory>() {

                protected Node.Factory initialValue() {
                    return new Node.Factory();
                }
            };
    final NodeSet heads;

    /** Creates a new instance of Trie */
    public TerminalTrie() {
        heads = new NodeSet();
    }

    public TerminalTrie(TerminalTrie x) {
        if(x == null)
            heads = new NodeSet();
        else if(x.isFinal())
            heads = x.heads;
        else {
            heads = new NodeSet();
            x.heads.traverse(IN_ORDER, CLONER, heads);
        }
    }
//    TerminalTrie(Path<Node> h) {
//        heads = new NodeSet(h);
//    }

    TerminalTrie(NodeSet h) {
        if(h == null)
            throw new NullPointerException("NodeSet is null");
        heads = h;
    }

    public boolean equals(Object o) {
        if(o == this)
            return true;
        if((o == null) || !(o instanceof TerminalTrie))
            return false;
        return compareTo((TerminalTrie)o) == 0;
    }
    private static final Comparator<Node> TRIE_NODE_COMPARATOR = new Comparator<Node>() {

        public int compare(Node x, Node y) {
            if(x == null)
                return y == null ? 0 : -1;
            if(y == null)
                return 1;
            int c = 0;
            if(x.x == null)
                c = y.x == null ? 0 : -1;
            else if(y.x == null)
                c = 1;
            else
                c = x.x.compareTo(y.x);
            if(c != 0)
                return c;
            if(x.nextNodes == null)
                return y.nextNodes == null ? 0 : -1;
            if(y.nextNodes == null)
                return 1;
            c = x.nextNodes.size() - y.nextNodes.size();
            if(c != 0)
                return c;
            if(x.nextNodes.isEmpty())
                throw new RuntimeException("Unclosed path discovered");
            return x.nextNodes.compareTo(y.nextNodes, this);
        }
    };

    public int compareTo(TerminalTrie o) {
        if(o == this)
            return 0;
        if(o == null)
            return 1;
        int c = heads.size() - o.heads.size();
        if(c != 0)
            return c;
        if(heads.isEmpty())
            return 0;
        return heads.compareTo(o.heads, TRIE_NODE_COMPARATOR);
    }

    private static int compare(Node x, Node y) {
        if(x == null)
            return y == null ? 0 : -1;
        if(y == null)
            return 1;
        int i = 0;
        if(x.x == null)
            i = y.x == null ? 0 : -1;
        else if(y.x == null)
            i = 1;
        else
            i = x.x.compareTo(y.x);
        if(i != 0)
            return i;
        if(x.nextNodes == null)
            return y.nextNodes == null ? 0 : -1;
        if(y.nextNodes == null)
            return 1;
        java.util.Iterator<Node> i1 = x.nextNodes.iterator();
        java.util.Iterator<Node> i2 = y.nextNodes.iterator();
        while(i == 0) {
            if(!i1.hasNext())
                return i2.hasNext() ? -1 : 0;
            if(!i2.hasNext())
                return 1;
            i = compare(i1.next(), i2.next());
        }
        return i;
    }

    public boolean containsEmptyPath() {
        return heads.contains(EndNode);
    }

    public boolean containsOnlyEmptyPath() {
        return heads.contains(EndNode) && heads.size() == 1;
    }

    public boolean contains(Terminal y) {
        if((y == null) || (y == Terminal.Epsilon))
            return heads.contains(EndNode);
        Node t = heads.search(y);
        if(t == null)
            return false;
        return (t.nextNodes == null) || t.nextNodes.contains(EndNode);
    }

    public boolean contains(Path<Terminal> y) {
        if((y == null) || (y.head == null))
            return heads.contains(EndNode);
        Path.Node<Terminal> u = y.head;
        while((u != null) && ((u.x == null) || (u.x == Terminal.Epsilon)))
            u = u.next;
        if(u == null)
            return heads.contains(EndNode);
        NodeSet s = heads;
        Node t;
        while(u != null) {
            if((s == null) || ((t = s.search(u.x)) == null))
                return false;
            s = t.nextNodes;
            u = u.next;
            while((u != null) && ((u.x == null) || (u.x == Terminal.Epsilon)))
                u = u.next;
        }
        return (s == null) || s.contains(EndNode);
    }

    public boolean matches(Path<Terminal> y) {
        if(y == null)
            return heads.contains(EndNode);
        return matches(heads, y.head);
    }

    private static boolean matches(NodeSet s, Path.Node<Terminal> u) {
        while((u != null) && ((u.x == null) || (u.x == Terminal.Epsilon)))
            u = u.next;
        if(s == null)
            return u == null;
        if(u == null)
            return s.contains(EndNode);
        Node x = s.NC.getMatch(u.x);
        if((x != null) && matches(x.nextNodes, u.next))
            return true;
        if(s.CC != null)
            return s.CC.traverse(Traversable.PRE_ORDER, PATH_MATCHER, u) < 0;
        return false;
    }
    private static final TraversalActionPerformer<Node> PATH_MATCHER =
            new TraversalActionPerformer<Node>() {

                public boolean perform(Node x, int order, Object o) {
                    Path.Node<Terminal> u = (Path.Node<Terminal>)o;
                    return x.x.matches(u.x) && matches(x.nextNodes, u.next);
                }
            };

    public boolean matchesPrefixOf(Path<Terminal> y) {
        if((y == null) || (y.head == null))
            return heads.contains(EndNode);
        Node x = heads.NC.getMatch(y.head.x);
        if((x != null) && matchesPrefixOf(x.nextNodes, y.head.next))
            return true;
        if(heads.CC != null)
            return heads.CC.traverse(Traversable.PRE_ORDER, PREFIX_MATCHER,
                                      y.head) < 0;
        return false;
    }

    private static boolean matchesPrefixOf(NodeSet s, Path.Node<Terminal> u) {
        if((s == null) || s.contains(EndNode))
            return true;
        while((u != null) && ((u.x == null) || (u.x == Terminal.Epsilon)))
            u = u.next;
        if(u == null)
            return false;
        Node x = s.NC.getMatch(u.x);
        if((x != null) && matchesPrefixOf(x.nextNodes, u.next))
            return true;
        if(s.CC != null)
            return s.CC.traverse(Traversable.PRE_ORDER, PREFIX_MATCHER, u) < 0;
        return false;
    }
    private static final TraversalActionPerformer<Node> PREFIX_MATCHER =
            new TraversalActionPerformer<Node>() {

                public boolean perform(Node x, int order, Object o) {
                    Path.Node<Terminal> u = (Path.Node<Terminal>)o;
                    return x.x.matches(u.x) && matchesPrefixOf(x.nextNodes,
                                                                u.next);
                }
            };

    public boolean matches(Terminal y) {
        if(y == null)
            return heads.contains(EndNode);
        Node t = heads.NC.getMatch(y);
        if((t != null) && ((t.nextNodes == null) || t.nextNodes.contains(EndNode)))
            return true;
        if(heads.CC != null)
            return heads.CC.traverse(Traversable.PRE_ORDER, SINGLE_MATCHER, y) < 0;
        return false;
    }
    private static final TraversalActionPerformer<Node> SINGLE_MATCHER =
            new TraversalActionPerformer<Node>() {

                public boolean perform(Node x, int order, Object o) {
                    Terminal t = (Terminal)o;
                    return x.x.matches(t) && ((x.nextNodes == null) || (x.nextNodes.
                            contains(EndNode)));
                }
            };

    public void clear() {
        if(heads.isEmpty())
            return;
        if(isFinal())
            throw new UnsupportedOperationException(
                    "Final trie cannot be modified");
        heads.clear();
    }

    public boolean add(Terminal y) {
        if((y == null) || (y == Terminal.Epsilon))
            return heads.add(EndNode);
        Node.Factory f = NODE_FACTORY.get();
        Node t = heads.addOrGet(y, f);
        if(t == f.last)
            return true;
        else if(t.nextNodes != null)
            return t.nextNodes.add(EndNode);
        else
            return false;
    }

    public boolean add(Path<Terminal> y) {
        if(y == null)
            return heads.add(EndNode);
        Path.Node<Terminal> u = y.head;
        while((u != null) && ((u.x == null) || (u.x == Terminal.Epsilon)))
            u = u.next;
        if(u == null)
            return heads.add(EndNode);
        NodeSet s = heads;
        Node.Factory f = NODE_FACTORY.get();
        Node t;
        boolean changed = false;
        while(true) {
            t = s.addOrGet(u.x, f);
            if(t == f.last) {
                u = u.next;
                while((u != null) && ((u.x == null) || (u.x == Terminal.Epsilon)))
                    u = u.next;
                if(u != null) {
                    changed |= true;
                    t.nextNodes = new NodeSet();
                    s = t.nextNodes;
                } else
                    return true;
            } else {
                s = t.nextNodes;
                u = u.next;
                while((u != null) && ((u.x == null) || (u.x == Terminal.Epsilon)))
                    u = u.next;
                if(u == null) {
                    if((s != null) && (!s.contains(EndNode)))
                        changed |= s.add(EndNode);
                    break;
                }
                if(s == null) {
                    t.nextNodes = new NodeSet();
                    s = t.nextNodes;
                    changed |= s.add(EndNode);
                }
            }
        }
        return changed;
    }

    public boolean remove(Terminal y) {
        if(isFinal())
            throw new UnsupportedOperationException(
                    "Final trie cannot be modified");
        if(heads.isEmpty())
            return false;
        if((y == null) || (y == Terminal.Epsilon))
            return heads.remove(EndNode);
        Node t = heads.search(y);
        if(t == null)
            return false;
        if((t.nextNodes == null) || (t.nextNodes.contains(EndNode) && (t.nextNodes.
                size() == 1)))
            return heads.remove(t);
        return t.nextNodes.remove(EndNode);
    }

    public boolean remove(Path<Terminal> y) {
        if(isFinal())
            throw new UnsupportedOperationException(
                    "Final trie cannot be modified");
        if(heads.isEmpty())
            return false;
        if(y == null)
            return heads.remove(EndNode);
        Path.Node<Terminal> u = y.head;
        while((u != null) && ((u.x == null) || (u.x == Terminal.Epsilon)))
            u = u.next;
        if(u == null)
            return heads.remove(EndNode);
        NodeSet s = heads, ls = heads;
        Node t, lt = null;
        while(true) {
            t = s.search(u.x);
            if(t == null)
                return false;
            if(s.size() > 1) {
                ls = s;
                lt = t;
            }
            s = t.nextNodes;
            u = u.next;
            while((u != null) && ((u.x == null) || (u.x == Terminal.Epsilon)))
                u = u.next;
            if(u == null) {
                if(s == null)
                    break;
                if(!s.contains(EndNode))
                    return false;
                if(s.size() > 1)
                    return s.remove(EndNode);
                break;
            }
            if(s == null)
                return false;
        }
        if((ls != null) && (lt != null))
            return ls.remove(lt);
        return false;
    }

    public void applyBoundedPlus(int bound) {
        if(bound < 0)
            throw new IllegalArgumentException("Bound is negative");
        if(bound == 0) {
            if(!isEmpty()) {
                clear();
                add(Terminal.Epsilon);
            }
            return;
        }
        if(heads.isEmpty())
            return;
        if(heads.contains(EndNode) && (heads.size() == 1))
            return;
        NodeSet nHeads = new NodeSet();
        heads.traverse(IN_ORDER,
                       BOUNDED_PLUS_APPLIER.get().init(this, bound), nHeads);
        heads.NC = nHeads.NC;
        heads.CC = nHeads.CC;
    }

    private static class BoundedPlusApplier implements
            TraversalActionPerformer<Node> {

        private TerminalTrie x;
        private int k = 0;
        private int bound = 0;

        BoundedPlusApplier init(TerminalTrie x, int bound) {
            this.x = x;
            this.k = 0;
            this.bound = bound;
            return this;
        }

        public boolean perform(Node a, int order, Object o) {
            NodeSet u = (NodeSet)o;
            if(a == EndNode) {
                u.add(EndNode);
                if(k > 1) {
                    k--;
                    x.heads.traverse(IN_ORDER, this, u);
                    k++;
                }
            } else {
                Node b = new Node(a.x);
                if(!u.add(b))
                    throw new RuntimeException("expected empty target trie");
                if(a.nextNodes == null) {
                    if((k > 1) && (bound > 1)) {
                        b.nextNodes = new NodeSet();
                        b.nextNodes.add(EndNode);
                        k--;
                        bound--;
                        x.heads.traverse(IN_ORDER, this, b.nextNodes);
                        k++;
                        bound++;
                    }
                } else if(bound > 1) {
                    b.nextNodes = new NodeSet();
                    bound--;
                    a.nextNodes.traverse(IN_ORDER, this, b.nextNodes);
                    bound++;
                }
            }
            return false;
        }
    }
    private static final ThreadLocal<BoundedPlusApplier> BOUNDED_PLUS_APPLIER =
            new ThreadLocal<BoundedPlusApplier>() {

                protected BoundedPlusApplier initialValue() {
                    return new BoundedPlusApplier();
                }
            };

    public void applyBoundedStar(int bound) {
        if(bound < 0)
            throw new IllegalArgumentException("Bound is negative");
        applyBoundedPlus(bound);
        add(Terminal.Epsilon);
    }

    public boolean isEmpty() {
        return heads.isEmpty();
    }

    public boolean isFinal() {
        return heads.isFinal();
    }

    public void makeFinal() {
        if(!heads.isFinal()) {
            heads.makeFinal();
            heads.traverse(IN_ORDER, FINALIZER);
        }
    }

    public void makeEditable() {
        if(heads.isFinal()) {
            NodeSet nHeads = new NodeSet();
            heads.traverse(IN_ORDER, CLONER, nHeads);
            heads.NC = nHeads.NC;
            heads.CC = nHeads.CC;
        }
    }
    private static final TraversalActionPerformer<Node> FINALIZER = new TraversalActionPerformer<Node>() {

        public boolean perform(Node x, int order, Object o) {
            x.makeFinal();
            return false;
        }
    };

    public TerminalTrie clone() {
        TerminalTrie r = new TerminalTrie();
        heads.traverse(IN_ORDER, CLONER, r.heads);
        return r;
    }
    private static final TraversalActionPerformer<Node> CLONER = new TraversalActionPerformer<Node>() {

        public boolean perform(Node x, int order, Object o) {
            ((NodeSet)o).add(x.clone());
            return false;
        }
    };

    public String toString() {
        if(heads.isEmpty())
            return "()";
        return toStringBuilder(null).toString();
    }

    public StringBuilder toStringBuilder(StringBuilder sb) {
        if(sb == null)
            sb = new StringBuilder();
        if(heads.isEmpty())
            return sb.append("()");
        heads.traverse(IN_ORDER, TRIE_STRING_BUILDER.get().init(),
                       sb.append("("));
        return sb.append(")");
    }

    private static class TrieStringBuilder
            implements TraversalActionPerformer<Node> {

        private boolean first = true;

        TrieStringBuilder init() {
            first = true;
            return this;
        }

        public boolean perform(Node x, int order, Object o) {
            if(order == IN_ORDER) {
                StringBuilder s = (StringBuilder)o;
                if(!first)
                    s.append(',');
                else
                    first = false;
                if(x == EndNode)
                    s.append('#');
                else {
                    if(x.x != null)
                        x.x.toStringBuilder(s);
                    if((x.nextNodes != null) && !((x.nextNodes.size() == 1)
                                                  && x.nextNodes.contains(
                            EndNode))) {
                        s.append('(');
                        if(!x.nextNodes.isEmpty()) {
                            first = true;
                            x.nextNodes.traverse(IN_ORDER, this, s);
                            first = false;
                        } else
                            throw new RuntimeException(
                                    "Unclosed path discovered");
                        s.append(')');
                    }
                }
            }
            return false;
        }
    }
    private static final ThreadLocal<TrieStringBuilder> TRIE_STRING_BUILDER =
            new ThreadLocal<TrieStringBuilder>() {

                protected TrieStringBuilder initialValue() {
                    return new TrieStringBuilder();
                }
            };

    public void writeToStream(Writer w) throws IOException {
        if(w == null)
            return;
        if(heads.isEmpty()) {
            w.write("()");
            return;
        }
        if((heads.size() == 1) && heads.contains(EndNode)) {
            w.write("(#)");
            return;
        }
        w.write('[');
        w.write(Integer.toString(heads.NC.size()));
        if(heads.CC != null) {
            w.write(',');
            w.write(Integer.toString(heads.CC.size()));
        }
        w.write(']');
        w.write('(');
        heads.traverse(IN_ORDER, TRIE_WRITER.get().init(), w);
        if(TABBED_WRITING)
            w.write("\r\n");
        w.write(')');
    }

    private static class TrieWriter implements TraversalActionPerformer<Node> {

        private boolean first = true;
        private int tabCount = 1;

        TrieWriter init() {
            first = true;
            tabCount = 1;
            return this;
        }

        public boolean perform(Node x, int order, Object o) {
            if(order == IN_ORDER)
                try {
                    Writer w = (Writer)o;
                    if(!first)
                        w.write(',');
                    else
                        first = false;
                    if(TABBED_WRITING) {
                        w.write("\r\n");
                        for(int i = 0; i < tabCount; i++)
                            w.write('\t');
                    }
                    if(x == EndNode)
                        w.write('#');
                    else {
                        if(x.x != null)
                            w.write(x.x.toString());
                        if((x.nextNodes != null) && !((x.nextNodes.size() == 1)
                                                      && x.nextNodes.contains(
                                EndNode))) {
                            w.write('[');
                            w.write(Integer.toString(x.nextNodes.NC.size()));
                            if(x.nextNodes.CC != null) {
                                w.write(',');
                                w.write(Integer.toString(x.nextNodes.CC.size()));
                            }
                            w.write(']');
                            w.write('(');
                            if(!x.nextNodes.isEmpty()) {
                                first = true;
                                tabCount++;
                                x.nextNodes.traverse(IN_ORDER, this, w);
                                first = false;
                                tabCount--;
                                if(TABBED_WRITING) {
                                    w.write("\r\n");
                                    for(int i = 0; i < tabCount; i++)
                                        w.write('\t');
                                }
                            } else
                                throw new RuntimeException(
                                        "Unclosed path discovered");
                            w.write(')');
                        }
                    }
                } catch(IOException e) {
                    throw new RuntimeException(
                            "Error while writing trie: ", e);
                }
            return false;
        }
    }
    private static final ThreadLocal<TrieWriter> TRIE_WRITER =
            new ThreadLocal<TrieWriter>() {

                protected TrieWriter initialValue() {
                    return new TrieWriter();
                }
            };

    public void writeExpandedToStream(Writer w) throws IOException {
        if(w == null)
            return;
        if(heads.isEmpty()) {
            w.write("{}");
            return;
        }
        w.write('{');
        traverse(IN_ORDER, EXPANDED_TRIE_WRITER.get().init(), w);
        w.write("\r\n}");
    }

    private static class ExpandedTrieWriter
            implements TraversalActionPerformer<Path<Terminal>> {

        private boolean first = true;

        ExpandedTrieWriter init() {
            first = true;
            return this;
        }

        public boolean perform(Path<Terminal> p, int order, Object o) {
            if(order == IN_ORDER)
                try {
                    Writer w = (Writer)o;
                    if(!first)
                        w.write(',');
                    else
                        first = false;
                    w.write("\r\n\t");
                    w.write(p.toString());
                } catch(IOException e) {
                    throw new RuntimeException(
                            "Error while writing expanded trie: ", e);
                }
            return false;
        }
    }
    private static final ThreadLocal<ExpandedTrieWriter> EXPANDED_TRIE_WRITER =
            new ThreadLocal<ExpandedTrieWriter>() {

                protected ExpandedTrieWriter initialValue() {
                    return new ExpandedTrieWriter();
                }
            };

    public java.util.Iterator<Path<Terminal>> iterator() {
        return new Iterator(this);
    }

    public java.util.Iterator<Path<Terminal>> subSetIterator(
            Selector<Path<Terminal>> s) {
        return new Iterator(this, s);
    }

    public TerminalTrie cloneBounded(int bound) {
        if(bound < 0)
            throw new IllegalArgumentException("Bound is negative");
        TerminalTrie r = new TerminalTrie();
        if(bound == 0) {
            if(!heads.isEmpty())
                r.heads.add(EndNode);
            return r;
        }
        heads.traverse(IN_ORDER, BOUNDED_CLONER.get().init(bound), r.heads);
        return r;
    }

    private static class BoundedCloner implements TraversalActionPerformer<Node> {

        private int bound;

        BoundedCloner init(int bound) {
            this.bound = bound;
            return this;
        }

        public boolean perform(Node x, int order, Object o) {
            Node r = x.x == null ? EndNode : new Node(x.x);
            if(x.nextNodes != null)
                if(bound == 1) {
                    if(x.nextNodes.isEmpty())
                        throw new RuntimeException(
                                getClass().getCanonicalName()
                                + ".cloneBounded: unclosed path discovered");
                } else if((x.nextNodes.size() != 1) || !x.nextNodes.contains(
                        EndNode)) {
                    r.nextNodes = new NodeSet();
                    bound--;
                    x.nextNodes.traverse(IN_ORDER, this, r.nextNodes);
                    bound++;
                }
            ((NodeSet)o).add(r);
            return false;
        }
    }
    private static final ThreadLocal<BoundedCloner> BOUNDED_CLONER =
            new ThreadLocal<BoundedCloner>() {

                protected BoundedCloner initialValue() {
                    return new BoundedCloner();
                }
            };

    public boolean addAll(TerminalTrie x) {
        if((x == null) || (x == this))
            return false;
        if(isFinal())
            throw new UnsupportedOperationException(
                    "Final trie cannot be modified");
        Adder p = ADDER.get().init();
        x.heads.traverse(IN_ORDER, p, heads);
        return p.changed;
    }

    private static class Adder implements TraversalActionPerformer<Node> {

        private boolean changed = false;

        Adder init() {
            changed = false;
            return this;
        }

        public boolean perform(Node x, int order, Object o) {
            NodeSet u = (NodeSet)o;
            Node.Factory f = NODE_FACTORY.get();
            if(x == EndNode)
                changed |= u.add(EndNode);
            else {
                Node t = u.addOrGet(x.x, f);
                if(t == f.last)
                    changed = true;
                if((x.nextNodes == null) || ((x.nextNodes.size() == 1)
                                             && x.nextNodes.contains(EndNode))) {
                    changed |= (t.nextNodes != null) && t.nextNodes.add(EndNode);
                    return false;
                }
                if(t.nextNodes == null) {
                    t.nextNodes = new NodeSet();
                    if(t != f.last)
                        changed |= t.nextNodes.add(EndNode);
                }
                x.nextNodes.traverse(IN_ORDER, this, t.nextNodes);
            }
            return false;
        }
    }
    private static final ThreadLocal<Adder> ADDER = new ThreadLocal<Adder>() {

        protected Adder initialValue() {
            return new Adder();
        }
    };

    public boolean addAllBounded(TerminalTrie x, int bound) {
        if(bound < 0)
            throw new IllegalArgumentException("Bound is negative");
        if((x == null) || x.isEmpty())
            return false;
        if(isFinal())
            throw new UnsupportedOperationException(
                    "Final trie cannot be modified");
        if(x == this)
            return semiBound(bound);
        if(bound == 0)
            if(!x.heads.isEmpty())
                return heads.add(EndNode);
            else
                return false;
        BoundedAdder p = BOUNDED_ADDER.get().init(bound);
        x.heads.traverse(IN_ORDER, p, heads);
        return p.changed;
    }

    private static class BoundedAdder implements TraversalActionPerformer<Node> {

        private int bound = 0;
        private boolean changed = false;

        BoundedAdder init(int bound) {
            this.bound = bound;
            changed = false;
            return this;
        }

        public boolean perform(Node x, int order, Object o) {
            NodeSet u = (NodeSet)o;
            Node.Factory f = NODE_FACTORY.get();
            if(x == EndNode) {
                if(u != null)
                    changed |= u.add(EndNode);
            } else {
                Node t = u.addOrGet(x.x, f);
                if(t == f.last)
                    changed = true;
                if(bound > 1) {
                    if((x.nextNodes == null) || ((x.nextNodes.size() == 1)
                                                 && x.nextNodes.contains(EndNode))) {
                        changed |= (t.nextNodes != null) && t.nextNodes.add(
                                EndNode);
                        return false;
                    }
                    if((t.nextNodes == null)) {
                        t.nextNodes = new NodeSet();
                        if(t != f.last)
                            changed |= t.nextNodes.add(EndNode);
                    }
                    bound--;
                    x.nextNodes.traverse(IN_ORDER, this, t.nextNodes);
                    bound++;
                } else if(t.nextNodes != null)
                    changed |= t.nextNodes.add(EndNode);
            }
            return false;
        }
    }
    private static final ThreadLocal<BoundedAdder> BOUNDED_ADDER =
            new ThreadLocal<BoundedAdder>() {

                protected BoundedAdder initialValue() {
                    return new BoundedAdder();
                }
            };

    public boolean addConcatenation(TerminalTrie[] x) {
        if(isFinal())
            throw new UnsupportedOperationException(
                    "Final trie cannot be modified");
        if((x == null) || (x.length == 0))
            return false;
        for(TerminalTrie y : x) {
            if((y == null) || y.isEmpty())
                return false;
            if(y == this)
                throw new IllegalArgumentException("Cycle detected");
        }
        ConcatenationAdder p = CONCATENATION_ADDER.get().init(x);
        x[0].heads.traverse(IN_ORDER, p, heads);
        return p.changed;
    }

    private static class ConcatenationAdder implements
            TraversalActionPerformer<Node> {

        private TerminalTrie[] x;
        private int i = 0;
        private boolean changed = false;

        ConcatenationAdder init(TerminalTrie[] x) {
            this.x = x;
            i = 0;
            changed = false;
            return this;
        }

        public boolean perform(Node a, int order, Object o) {
            NodeSet u = (NodeSet)o;
            Node.Factory f = NODE_FACTORY.get();
            int j;
            if(a == EndNode) {
                j = i;
                i++;
                while((i < x.length) && ((x[i].heads.size() == 1) && x[i].heads.
                        contains(EndNode)))
                    i++;
                if(i < x.length)
                    x[i].heads.traverse(IN_ORDER, this, u);
                else
                    changed |= u.add(EndNode);
                i = j;
            } else {
                Node b = u.addOrGet(a.x, f);
                if((a.nextNodes == null) || ((a.nextNodes.size() == 1) && a.nextNodes.
                        contains(EndNode))) {
                    j = i;
                    i++;
                    while((i < x.length) && ((x[i].heads.size() == 1) && x[i].heads.
                            contains(EndNode)))
                        i++;
                    if(i < x.length) {
                        if(b.nextNodes == null) {
                            changed = true;
                            b.nextNodes = new NodeSet();
                            if(b != f.last)
                                b.nextNodes.add(EndNode);
                        }
                        x[i].heads.traverse(IN_ORDER, this, b.nextNodes);
                    } else if(b.nextNodes != null)
                        changed |= b.nextNodes.add(EndNode);
                    else if(b == f.last)
                        changed = true;
                    i = j;
                } else {
                    if(b.nextNodes == null) {
                        changed = true;
                        b.nextNodes = new NodeSet();
                        if(b != f.last)
                            b.nextNodes.add(EndNode);
                    }
                    a.nextNodes.traverse(IN_ORDER, this, b.nextNodes);
                }
            }
            return false;
        }
    }
    private static final ThreadLocal<ConcatenationAdder> CONCATENATION_ADDER =
            new ThreadLocal<ConcatenationAdder>() {

                protected ConcatenationAdder initialValue() {
                    return new ConcatenationAdder();
                }
            };

    public boolean addBoundedConcatenation(TerminalTrie[] x, int bound) {
        if(isFinal())
            throw new UnsupportedOperationException(
                    "Final trie cannot be modified");
        if(bound < 0)
            throw new IllegalArgumentException("Bound is negative");
        if((x == null) || (x.length == 0))
            return false;
        for(TerminalTrie y : x) {
            if((y == null) || y.isEmpty())
                return false;
            if(y == this)
                throw new IllegalArgumentException("Cycle detected");
        }
        if(bound == 0)
            return heads.add(EndNode);
        BoundedConcatenationAdder p = BOUNDED_CONCATENATION_ADDER.get().init(x,
                                                                             bound);
        x[0].heads.traverse(IN_ORDER, p, heads);
        return p.changed;
    }

    private static class BoundedConcatenationAdder implements
            TraversalActionPerformer<Node> {

        private TerminalTrie[] x;
        private int i = 0;
        private int bound = 0;
        private boolean changed = false;

        BoundedConcatenationAdder init(TerminalTrie[] x, int bound) {
            this.x = x;
            this.bound = bound;
            i = 0;
            changed = false;
            return this;
        }

        public boolean perform(Node a, int order, Object o) {
            NodeSet u = (NodeSet)o;
            Node.Factory f = NODE_FACTORY.get();
            int j;
            if(a == EndNode) {
                j = i;
                i++;
                while((i < x.length) && ((x[i].heads.size() == 1) && x[i].heads.
                        contains(EndNode)))
                    i++;
                if(i < x.length)
                    x[i].heads.traverse(IN_ORDER, this, u);
                else
                    changed |= u.add(EndNode);
                i = j;
            } else {
                Node b = u.addOrGet(a.x, f);
                if(bound == 1) {
                    if(b == f.last)
                        changed = true;
                    else if(b.nextNodes != null)
                        changed |= b.nextNodes.add(EndNode);
                } else if((a.nextNodes == null) || ((a.nextNodes.size() == 1) && a.nextNodes.
                        contains(EndNode))) {
                    j = i;
                    i++;
                    while((i < x.length) && ((x[i].heads.size() == 1) && x[i].heads.
                            contains(EndNode)))
                        i++;
                    if(i < x.length) {
                        if(b.nextNodes == null) {
                            changed = true;
                            b.nextNodes = new NodeSet();
                            if(b != f.last)
                                b.nextNodes.add(EndNode);
                        }
                        bound--;
                        x[i].heads.traverse(IN_ORDER, this, b.nextNodes);
                        bound++;
                    } else if(b.nextNodes != null)
                        changed |= b.nextNodes.add(EndNode);
                    else if(b == f.last)
                        changed = true;
                    i = j;
                } else {
                    if(b.nextNodes == null) {
                        changed = true;
                        b.nextNodes = new NodeSet();
                        if(b != f.last)
                            b.nextNodes.add(EndNode);
                    }
                    bound--;
                    a.nextNodes.traverse(IN_ORDER, this, b.nextNodes);
                    bound++;
                }
            }
            return false;
        }
    }
    private static final ThreadLocal<BoundedConcatenationAdder> BOUNDED_CONCATENATION_ADDER =
            new ThreadLocal<BoundedConcatenationAdder>() {

                protected BoundedConcatenationAdder initialValue() {
                    return new BoundedConcatenationAdder();
                }
            };

    public boolean addBoundedConcatenationOfFirstSets(
            FirstSetOf<? extends Token>[] x, int bound) {
        if(isFinal())
            throw new UnsupportedOperationException(
                    "Final trie cannot be modified");
        if(bound < 0)
            throw new IllegalArgumentException("Bound is negative");
        if((x == null) || (x.length == 0))
            return false;
        for(FirstSetOf<? extends Token> y : x) {
            if((y == null) || (y.firstSet == null) || y.firstSet.isEmpty())
                return false;
            if(y.firstSet == this)
                throw new IllegalArgumentException("Cycle detected");
        }
        if(bound == 0)
            return heads.add(EndNode);
        BoundedConcatenationOfFirstSetsAdder p =
                BOUNDED_CONCATENATION_OF_FIRST_SETS_ADDER.get().init(x, bound);
        x[0].firstSet.heads.traverse(IN_ORDER, p, heads);
        return p.changed;
    }

    private static class BoundedConcatenationOfFirstSetsAdder
            implements TraversalActionPerformer<Node> {

        private FirstSetOf<? extends Token>[] x;
        private int i = 0;
        private int bound = 0;
        private boolean changed = false;

        BoundedConcatenationOfFirstSetsAdder init(
                FirstSetOf<? extends Token>[] x, int bound) {
            this.x = x;
            this.bound = bound;
            i = 0;
            changed = false;
            return this;
        }

        public boolean perform(Node a, int order, Object o) {
            NodeSet u = (NodeSet)o;
            Node.Factory f = NODE_FACTORY.get();
            int j;
            if(a == EndNode) {
                j = i;
                i++;
                while((i < x.length) && (x[i].firstSet.heads.size() == 1)
                      && x[i].firstSet.heads.contains(EndNode))
                    i++;
                if(i < x.length)
                    x[i].firstSet.heads.traverse(IN_ORDER, this, u);
                else
                    changed |= u.add(EndNode);
                i = j;
            } else {
                Node b = u.addOrGet(a.x, f);
                if(bound == 1) {
                    if(b == f.last)
                        changed = true;
                    else if(b.nextNodes != null)
                        changed |= b.nextNodes.add(EndNode);
                } else if((a.nextNodes == null)
                          || ((a.nextNodes.size() == 1)
                              && a.nextNodes.contains(EndNode))) {
                    j = i;
                    i++;
                    while((i < x.length) && (x[i].firstSet.heads.size() == 1)
                          && x[i].firstSet.heads.contains(EndNode))
                        i++;
                    if(i < x.length) {
                        if(b.nextNodes == null) {
                            changed = true;
                            b.nextNodes = new NodeSet();
                            if(b != f.last)
                                b.nextNodes.add(EndNode);
                        }
                        bound--;
                        x[i].firstSet.heads.traverse(IN_ORDER, this, b.nextNodes);
                        bound++;
                    } else if(b.nextNodes != null)
                        changed |= b.nextNodes.add(EndNode);
                    else if(b == f.last)
                        changed = true;
                    i = j;
                } else {
                    if(b.nextNodes == null) {
                        changed = true;
                        b.nextNodes = new NodeSet();
                        if(b != f.last)
                            b.nextNodes.add(EndNode);
                    }
                    bound--;
                    a.nextNodes.traverse(IN_ORDER, this, b.nextNodes);
                    bound++;
                }
            }
            return false;
        }
    }
    private static final ThreadLocal<BoundedConcatenationOfFirstSetsAdder> BOUNDED_CONCATENATION_OF_FIRST_SETS_ADDER =
            new ThreadLocal<BoundedConcatenationOfFirstSetsAdder>() {

                protected BoundedConcatenationOfFirstSetsAdder initialValue() {
                    return new BoundedConcatenationOfFirstSetsAdder();
                }
            };

    public boolean bound(int bound) {
//        if(isFinal())
//            throw(new UnsupportedOperationException("Final trie cannot be modified"));
        if(bound < 0)
            throw new IllegalArgumentException("Bound is negative");
        if(bound == 0) {
            if(heads.isEmpty())
                return false;
            boolean changed = false;
            if(heads.contains(EndNode))
                changed = heads.size() > 1;
            else
                changed = true;
            heads.clear();
            heads.add(EndNode);
            return changed;
        }
        Bounder p = BOUNDER.get().init(bound);
        heads.traverse(IN_ORDER, p);
        return p.changed;
    }

    private static class Bounder implements TraversalActionPerformer<Node> {

        private int bound = 0;
        private boolean changed = false;

        Bounder init(int bound) {
            this.bound = bound;
            changed = false;
            return this;
        }

        public boolean perform(Node x, int order, Object o) {
            if(bound == 1) {
                changed = ((x.nextNodes != null) && !((x.nextNodes.size() == 1)
                                                      && (x.nextNodes.contains(
                        EndNode))));
                x.nextNodes = null;
            } else if(x.nextNodes != null) {
                bound--;
                x.nextNodes.traverse(IN_ORDER, this);
                bound++;
            }
            return false;
        }
    }
    private static final ThreadLocal<Bounder> BOUNDER = new ThreadLocal<Bounder>() {

        protected Bounder initialValue() {
            return new Bounder();
        }
    };

    public boolean semiBound(int bound) {
        if(isFinal())
            throw new UnsupportedOperationException(
                    "Final trie cannot be modified");
        if(bound < 0)
            throw new IllegalArgumentException("Bound is negative");
        if(bound == 0)
            if(heads.isEmpty())
                return false;
            else
                return heads.add(EndNode);
        SemiBounder p = SEMI_BOUNDER.get().init(bound);
        heads.traverse(IN_ORDER, p);
        return p.changed;
    }

    private static class SemiBounder implements TraversalActionPerformer<Node> {

        private int bound = 0;
        private boolean changed = false;

        SemiBounder init(int bound) {
            this.bound = bound;
            changed = false;
            return this;
        }

        public boolean perform(Node x, int order, Object o) {
            if(bound == 1)
                changed |= ((x.nextNodes != null) && x.nextNodes.add(EndNode));
            else if(x.nextNodes != null) {
                bound--;
                x.nextNodes.traverse(IN_ORDER, this);
                bound++;
            }
            return false;
        }
    }
    private static final ThreadLocal<SemiBounder> SEMI_BOUNDER =
            new ThreadLocal<SemiBounder>() {

                protected SemiBounder initialValue() {
                    return new SemiBounder();
                }
            };

    public int size() {
        Counter p = COUNTER.get().init();
        heads.traverse(IN_ORDER, p);
        return p.count;
    }

    private static class Counter implements TraversalActionPerformer<Node> {

        private int count = 0;

        TerminalTrie.Counter init() {
            count = 0;
            return this;
        }

        public boolean perform(Node x, int order, Object o) {
            if(x.nextNodes == null)
                count++;
            else
                x.nextNodes.traverse(IN_ORDER, this);
            return false;
        }
    }
    private static final ThreadLocal<Counter> COUNTER =
            new ThreadLocal<Counter>() {

                protected Counter initialValue() {
                    return new Counter();
                }
            };
    private static final ThreadLocal<TerminalTrie[]> TRIE_ARRAY_SINGLE =
            new ThreadLocal<TerminalTrie[]>() {

                protected TerminalTrie[] initialValue() {
                    return new TerminalTrie[1];
                }
            };

    public void concat(TerminalTrie x) {
        if(isFinal())
            throw new UnsupportedOperationException(
                    "Final trie cannot be modified");
        if(x == null) {
            heads.clear();
            return;
        }
        TerminalTrie[] ta = TRIE_ARRAY_SINGLE.get();
        ta[0] = x;
        concat(ta);
    }

    public void concat(TerminalTrie[] x) {
        if(isFinal())
            throw new UnsupportedOperationException(
                    "Final trie cannot be modified");
        if((x == null) || (x.length == 0))
            return;
        for(TerminalTrie y : x) {
            if((y == null) || y.isEmpty()) {
                heads.clear();
                return;
            }
            if(y == this)
                throw new IllegalArgumentException("Cycle detected");
        }
        Concatenator p = CONCATENATOR.get().init(x);
        heads.traverse(IN_ORDER, p);
        if(heads.contains(EndNode)) {
            heads.remove(EndNode);
            p.useSuper = true;
            x[0].heads.traverse(IN_ORDER, p, heads);
            p.useSuper = false;
        }
    }

    private static class Concatenator extends ConcatenationAdder {

        private boolean useSuper = false;

        Concatenator init(TerminalTrie[] x) {
            super.init(x);
            useSuper = false;
            return this;
        }

        public boolean perform(Node x, int order, Object o) {
            if(useSuper)
                super.perform(x, order, o);
            else if(x != EndNode)
                if(x.nextNodes == null) {
                    x.nextNodes = new NodeSet();
                    useSuper = true;
                    super.x[0].heads.traverse(IN_ORDER, this, x.nextNodes);
                    useSuper = false;
                } else {
                    x.nextNodes.traverse(IN_ORDER, this);
                    if(x.nextNodes.contains(EndNode)) {
                        x.nextNodes.remove(EndNode);
                        useSuper = true;
                        super.x[0].heads.traverse(IN_ORDER, this, x.nextNodes);
                        useSuper = false;
                    }
                }
            return false;
        }
    }
    private static final ThreadLocal<Concatenator> CONCATENATOR =
            new ThreadLocal<Concatenator>() {

                protected Concatenator initialValue() {
                    return new Concatenator();
                }
            };

    public void concatBounded(TerminalTrie x, int bound) {
        if(isFinal())
            throw new UnsupportedOperationException(
                    "Final trie cannot be modified");
        if(x == null) {
            heads.clear();
            return;
        }
        TerminalTrie[] ta = TRIE_ARRAY_SINGLE.get();
        ta[0] = x;
        concatBounded(ta, bound);
    }

    public void concatBounded(TerminalTrie[] x, int bound) {
        if(isFinal())
            throw new UnsupportedOperationException(
                    "Final trie cannot be modified");
        if(bound < 0)
            throw new IllegalArgumentException("Bound is negative");
        if((x == null) || (x.length == 0))
            return;
        for(TerminalTrie y : x) {
            if((y == null) || y.isEmpty()) {
                heads.clear();
                return;
            }
            if(y == this)
                throw new IllegalArgumentException("Cycle detected");
        }
        if(bound == 0) {
            if(!heads.isEmpty()) {
                heads.clear();
                heads.add(EndNode);
            }
            return;
        }
        BoundedConcatenator p = BOUNDED_CONCATENATOR.get().init(x, bound);
        heads.traverse(IN_ORDER, p);
        if(heads.contains(EndNode)) {
            heads.remove(EndNode);
            p.useSuper = true;
            x[0].heads.traverse(IN_ORDER, p, heads);
            p.useSuper = false;
        }
    }

    private static class BoundedConcatenator extends BoundedConcatenationAdder {

        private boolean useSuper = false;

        BoundedConcatenator init(TerminalTrie[] x, int bound) {
            super.init(x, bound);
            useSuper = false;
            return this;
        }

        public boolean perform(Node x, int order, Object o) {
            if(useSuper)
                super.perform(x, order, o);
            else if(x != EndNode)
                if(super.bound > 1) {
                    super.bound--;
                    if(x.nextNodes == null) {
                        x.nextNodes = new NodeSet();
                        useSuper = true;
                        super.x[0].heads.traverse(IN_ORDER, this, x.nextNodes);
                        useSuper = false;
                    } else {
                        x.nextNodes.traverse(IN_ORDER, this);
                        if(x.nextNodes.contains(EndNode)) {
                            x.nextNodes.remove(EndNode);
                            useSuper = true;
                            super.x[0].heads.traverse(IN_ORDER, this,
                                                      x.nextNodes);
                            useSuper = false;
                        }
                    }
                    super.bound++;
                } else
                    x.nextNodes = null;
            return false;
        }
    }
    private static final ThreadLocal<BoundedConcatenator> BOUNDED_CONCATENATOR =
            new ThreadLocal<BoundedConcatenator>() {

                protected BoundedConcatenator initialValue() {
                    return new BoundedConcatenator();
                }
            };
    /*private*/ static final Node EndNode = new Node(null, null) {

        public boolean equals(Object o) {
            return o == this;
        }

        public int compareTo(Node x) {
            if(x == null)
                return 1;
            if(x == this)
                return 0;
            return -1;
        }

        public int asymmetricCompareTo(Terminal x) {
            if(x == null)
                return 0;
            return -1;
        }

        public StringBuilder toStringBuilder(StringBuilder sb) {
            if(sb == null)
                sb = new StringBuilder();
            return sb.append("#");
        }

        public String toString() {
            return "#";
        }

        public void writeToStream(Writer w) throws IOException {
            if(w == null)
                return;
            w.write('#');
        }

        public Node clone() {
            return this;
        }

        public Node cloneBounded(int bound) {
            return this;
        }

        public boolean bound(int bound) {
            return false;
        }

        public boolean semiBound(int bound) {
            return false;
        }
    };
    /*private*/ static class Node implements Cloneable, Comparable<Node>,
                                 AsymmetricComparable<Terminal>, Writeable,
                                 Serializable {

        private static final long serialVersionUID = 0L;
        public final Terminal x;
        NodeSet nextNodes;

        public Node(Terminal x) {
            if(x == null)
                throw new NullPointerException(
                        getClass().getCanonicalName()
                        + ".constructor: null terminal detected, use EndNode instead");
            this.x = x;
            this.nextNodes = null;
        }

        Node(Terminal x, NodeSet nextNodes) {
            this.x = x;
            this.nextNodes = nextNodes;
        }

        public Node clone() {
            Node r = new Node(x);
            if((nextNodes != null)
               && ((nextNodes.size() != 1) || !nextNodes.contains(EndNode))) {
                r.nextNodes = new NodeSet();
                nextNodes.traverse(IN_ORDER, CLONER, r.nextNodes);
            }
            return r;
        }

        public Node cloneBounded(int bound) {
            if(bound <= 0)
                return EndNode;
            Node r = new Node(x);
            if(nextNodes != null) {
                if(bound == 1) {
                    if(nextNodes.isEmpty())
                        throw new RuntimeException("Unclosed path discovered");
                    return r;
                }
                r.nextNodes = new NodeSet();
                nextNodes.traverse(IN_ORDER,
                                   BOUNDED_CLONER.get().init(bound - 1),
                                   r.nextNodes);
            }
            return r;
        }

        public boolean bound(int bound) {
            if(bound <= 0)
                return false;
            if(bound == 1) {
                boolean changed = ((nextNodes != null)
                                   && !((nextNodes.size() == 1) && (nextNodes.
                        contains(EndNode))));
                nextNodes = null;
                return changed;
            } else if(nextNodes != null) {
                Bounder p = BOUNDER.get().init(bound - 1);
                nextNodes.traverse(IN_ORDER, p);
                return p.changed;
            }
            return false;
        }

        public boolean semiBound(int bound) {
            if(bound <= 0)
                return false;
            if(bound == 1)
                return (nextNodes != null) && nextNodes.add(EndNode);
            else if(nextNodes != null) {
                SemiBounder p = SEMI_BOUNDER.get().init(bound - 1);
                nextNodes.traverse(IN_ORDER, p);
                return p.changed;
            }
            return false;
        }

        public int size() {
            if(nextNodes == null)
                return 1;
            Counter p = COUNTER.get().init();
            nextNodes.traverse(IN_ORDER, p);
            return p.count;
        }

        public void makeFinal() {
            if((nextNodes == null) || nextNodes.isFinal())
                return;
            else {
                nextNodes.makeFinal();
                nextNodes.traverse(IN_ORDER, FINALIZER);
            }
        }

        public String toString() {
            return toStringBuilder(null).toString();
        }

        public boolean equals(Object o) {
            if((o == null) || !(o instanceof Node))
                return false;
            return (this == o) || (compareTo((Node)o) == 0);
        }

        public int compareTo(Node y) {
            if(y == null)
                return 1;
            if(x == null)
                return y.x == null ? 0 : -1;
            if(y.x == null)
                return 1;
            return x.compareTo(y.x);
        }

        public int asymmetricCompareTo(Terminal y) {
            if(x == null)
                return y == null ? 0 : -1;
            if(y == null)
                return 1;
            return x.compareTo(y);
        }

        public StringBuilder toStringBuilder(StringBuilder sb) {
            if(sb == null)
                sb = new StringBuilder();
            if(x != null)
                x.toStringBuilder(sb);
            if(nextNodes == null)
                return sb;
            if(nextNodes.isEmpty())
                throw new RuntimeException("Unclosed path discovered");
            sb.append('(');
            nextNodes.traverse(IN_ORDER, TRIE_STRING_BUILDER.get().init(), sb);
            sb.append(')');
            return sb;
        }

        public void writeToStream(Writer w) throws IOException {
            if(w == null)
                return;
            if(x != null)
                x.writeToStream(w);
            if(nextNodes == null)
                return;
            if(nextNodes.isEmpty())
                throw new RuntimeException("Unclosed path discovered");
            w.write('(');
            nextNodes.traverse(IN_ORDER, TRIE_WRITER.get().init(), w);
            if(TABBED_WRITING)
                w.write("\r\n");
            w.write(')');
        }

        public static class Factory implements
                KeyedElementFactory<Node, Terminal> {

            Node last;

            public Node lastCreated() {
                return last;
            }

            public Node createElement(Terminal x) {
                if(x == null)
                    throw new NullPointerException(
                            getClass().getCanonicalName()
                            + ".createElement: Terminal is null");
                last = new Node(x);
                return last;
            }

            public void reset() {
                last = null;
            }

            @Override
            public Terminal extractKey(Node x) {
                return x.x;
            }
        }
    }

    private static class Iterator implements java.util.Iterator<Path<Terminal>> {

        private final Selector<? super Path<Terminal>> s;
        private final TerminalTrie t;
        private volatile Path<Terminal> last, current;
        private final java.util.LinkedList<java.util.Iterator<Node>> sti;
        private java.util.LinkedList<Path<Terminal>> l;
        private volatile boolean last_removed;

        public Iterator(TerminalTrie t) {
            this(t, null);
        }

        public Iterator(TerminalTrie t, Selector<Path<Terminal>> s) {
            if(t == null)
                throw new IllegalArgumentException(
                        "Tree to be iterated is null!");
            this.t = t;
            last_removed = false;
            if(s != null)
                this.s = s;
            else
                this.s = Selector.Accepter;
            last = null;
            current = null;
            sti = new java.util.LinkedList<java.util.Iterator<Node>>();
            sti.push(t.heads.iterator());
            if(sti.peek().hasNext())
                current = new Path<Terminal>();
            pnext();
            while((current != null) && (!this.s.selects(current)))
                pnext();
        }

        private void pnext() {
            if(sti.isEmpty()) {
                current = null;
                return;
            }
            java.util.Iterator<Node> c = sti.peek();
            while((c == null) || !c.hasNext()) {
                sti.pop();
                if(sti.isEmpty()) {
                    current = null;
                    if(l != null)
                        for(Path<Terminal> p : l)
                            t.remove(p);
                    l = null;
                    return;
                }
                current.pop();
                c = sti.peek();
            }
            Node cn;
            while(c.hasNext()) {
                cn = c.next();
                if(cn == EndNode)
                    return;
                current.push(cn.x);
                if(cn.nextNodes == null) {
                    sti.push(null);
                    return;
                }
                c = cn.nextNodes.iterator();
                sti.push(c);
            }
            throw new RuntimeException(
                    getClass().getCanonicalName() + ".pnext: unclosed path discovered");
        }

        public Path<Terminal> next() {
            if(current == null)
                throw new java.util.NoSuchElementException(
                        getClass().getCanonicalName()
                        + ".next: no more elements to iterate");
            last = current.cloneReversed();
            last_removed = false;
            pnext();
            while((current != null) && (!s.selects(current)))
                pnext();
            return last;
        }

        public boolean hasNext() {
            return current != null;
        }

        public void remove() {
            if(last == null)
                throw new IllegalStateException(
                        getClass().getCanonicalName()
                        + ".remove: iteration has not yet started");
            if(!last_removed) {
                if(current == null) {
                    if(l != null)
                        for(Path<Terminal> p : l)
                            t.remove(p);
                    l = null;
                    t.remove(last);
                } else {
                    if(l == null)
                        l = new java.util.LinkedList<Path<Terminal>>();
                    l.add(last);
                }
                last_removed = true;
            }
        }

        public void finalize() {
            if(l != null)
                for(Path<Terminal> p : l)
                    t.remove(p);
            l = null;
        }
    }

    public static boolean ConcatenationContainsPath(TerminalTrie[] x,
                                                    Path<Terminal> y) {
        return ConcatenationContainsPath(x, true, y);
    }

    public static boolean ConcatenationContainsPath(TerminalTrie[] x,
                                                    boolean firstEpsilonAllowed,
                                                    Path<Terminal> y) {
        if((x == null) || (x.length == 0))
            return false;
        if(x[0] == null)
            return false;
        return ConcatenationContainsPath(x, null, 0,
                                          y != null ? y.head : null, x[0].heads,
                                          firstEpsilonAllowed);
    }

    public static boolean ConcatenationContainsPath(TerminalTrie[] x,
                                                    boolean[] epsilonAllowed,
                                                    Path<Terminal> y) {
        if((x == null) || (x.length == 0))
            return false;
        if(x[0] == null)
            return false;
        if((epsilonAllowed != null) && (epsilonAllowed.length < x.length))
            throw new IllegalArgumentException(
                    "Epsilon array must be not less than trie array");
        return ConcatenationContainsPath(x, epsilonAllowed, 0,
                                          y != null ? y.head : null, x[0].heads,
                                          epsilonAllowed != null ? epsilonAllowed[0] : true);
    }

    private static boolean ConcatenationContainsPath(TerminalTrie[] x,
                                                     boolean[] epsilonAllowed,
                                                     int i,
                                                     Path.Node<Terminal> y,
                                                     NodeSet z, boolean e) {
        if(y == null)
            if(e && ((z == null) || z.contains(EndNode))) {
                if(i + 1 >= x.length)
                    return true;
                if(x[i + 1] == null)
                    return false;
                return ConcatenationContainsPath(x, epsilonAllowed, i + 1, y,
                                                  x[i + 1].heads,
                                                  epsilonAllowed != null ? epsilonAllowed[i + 1] : true);
            } else
                return false;
        if(z == null) {
            if(!e)
                return false;
            if(i + 1 >= x.length)
                return false;
            if(x[i + 1] == null)
                return false;
            return ConcatenationContainsPath(x, epsilonAllowed, i + 1, y,
                                              x[i + 1].heads,
                                              epsilonAllowed != null ? epsilonAllowed[i + 1] : true);
        }
        Node t = z.search(y.x);
        if(t != null)
            if(ConcatenationContainsPath(x, epsilonAllowed, i, y.next,
                                         t.nextNodes, true))
                return true;
        if(e && z.contains(EndNode)) {
            if(i + 1 >= x.length)
                return false;
            if(x[i + 1] == null)
                return false;
            return ConcatenationContainsPath(x, epsilonAllowed, i + 1, y,
                                              x[i + 1].heads,
                                              epsilonAllowed != null ? epsilonAllowed[i + 1] : true);
        }
        return false;
    }

    public static boolean BoundedConcatenationContainsPath(
            TerminalTrie[] x, int bound, Path<Terminal> y) {
        return BoundedConcatenationContainsPath(x, bound, true, y);
    }

    public static boolean BoundedConcatenationContainsPath(
            TerminalTrie[] x, int bound, boolean firstEpsilonAllowed,
            Path<Terminal> y) {
        if((x == null) || (x.length == 0))
            return false;
        if(x[0] == null)
            return false;
        if(bound < 0)
            throw new IllegalArgumentException("Bound is negative");
        return BoundedConcatenationContainsPath(x, bound, null, 0,
                                                 y != null ? y.head : null,
                                                 x[0].heads, firstEpsilonAllowed);
    }

    public static boolean BoundedConcatenationContainsPath(
            TerminalTrie[] x, int bound, boolean[] epsilonAllowed,
            Path<Terminal> y) {
        if((x == null) || (x.length == 0))
            return false;
        if(x[0] == null)
            return false;
        if(bound < 0)
            throw new IllegalArgumentException("Bound is negative");
        if((epsilonAllowed != null) && (epsilonAllowed.length < x.length))
            throw new IllegalArgumentException(
                    "Epsilon array must be not less than trie array");
        return BoundedConcatenationContainsPath(x, bound, epsilonAllowed, 0,
                                                 y != null ? y.head : null,
                                                 x[0].heads,
                                                 epsilonAllowed != null ? epsilonAllowed[0] : true);
    }

    private static boolean BoundedConcatenationContainsPath(TerminalTrie[] x,
                                                            int bound,
                                                            boolean[] epsilonAllowed,
                                                            int i,
                                                            Path.Node<Terminal> y,
                                                            NodeSet z, boolean e) {
        if((y == null) || (bound == 0))
            if(bound == 0 ? (e ? (z == null) || !z.isEmpty()
                             : (z != null) && (z.size() > (z.contains(EndNode) ? 1 : 0)))
               : (e && ((z == null) || z.contains(EndNode)))) {
                if(i + 1 >= x.length)
                    return true;
                if(x[i + 1] == null)
                    return false;
                return BoundedConcatenationContainsPath(x, bound,
                                                         epsilonAllowed, i + 1,
                                                         y,
                                                         x[i + 1].heads,
                                                         epsilonAllowed != null ? epsilonAllowed[i + 1] : true);
            } else
                return false;
        if(z == null) {
            if(!e)
                return false;
            if(i + 1 >= x.length)
                return false;
            if(x[i + 1] == null)
                return false;
            return BoundedConcatenationContainsPath(x, bound, epsilonAllowed,
                                                     i + 1, y,
                                                     x[i + 1].heads,
                                                     epsilonAllowed != null ? epsilonAllowed[i + 1] : true);
        }
        Node t = z.search(y.x);
        if(t != null)
            if(BoundedConcatenationContainsPath(x, bound - 1, epsilonAllowed, i,
                                                y.next, t.nextNodes, true))
                return true;
        if(e && z.contains(EndNode)) {
            if(i + 1 >= x.length)
                return false;
            if(x[i + 1] == null)
                return false;
            return BoundedConcatenationContainsPath(x, bound, epsilonAllowed,
                                                     i + 1, y,
                                                     x[i + 1].heads,
                                                     epsilonAllowed != null ? epsilonAllowed[i + 1] : true);
        }
        return false;
    }

    public static boolean ConcatenationMatchesPath(TerminalTrie[] x,
                                                   Path<Terminal> y) {
        return ConcatenationMatchesPath(x, true, y);
    }

    public static boolean ConcatenationMatchesPath(TerminalTrie[] x,
                                                   boolean firstEpsilonAllowed,
                                                   Path<Terminal> y) {
        if((x == null) || (x.length == 0))
            return false;
        if(x[0] == null)
            return false;
        ConcatenationPathMatcher p = CONCATENATION_PATH_MATCHER.get().init(x,
                                                                           null);
        return ConcatenationMatchesPath(p, y != null ? y.head : null,
                                         x[0].heads, firstEpsilonAllowed);
    }

    public static boolean ConcatenationMatchesPath(TerminalTrie[] x,
                                                   boolean[] epsilonAllowed,
                                                   Path<Terminal> y) {
        if((x == null) || (x.length == 0))
            return false;
        if(x[0] == null)
            return false;
        if((epsilonAllowed != null) && (epsilonAllowed.length < x.length))
            throw new IllegalArgumentException(
                    "Epsilon array must be not less the trie array");
        ConcatenationPathMatcher p = CONCATENATION_PATH_MATCHER.get().init(x,
                                                                           epsilonAllowed);
        return ConcatenationMatchesPath(p, y != null ? y.head : null,
                                         x[0].heads,
                                         epsilonAllowed != null ? epsilonAllowed[0] : true);
    }

    private static boolean ConcatenationMatchesPath(ConcatenationPathMatcher p,
                                                    Path.Node<Terminal> y,
                                                    NodeSet z, boolean e) {
        if(y == null)
            if(e && ((z == null) || z.contains(EndNode))) {
                if(p.i + 1 >= p.x.length)
                    return true;
                if(p.x[p.i + 1] == null)
                    return false;
                p.i++;
                boolean b = ConcatenationMatchesPath(p, y, p.x[p.i].heads,
                                                     p.epsilonAllowed != null ? p.epsilonAllowed[p.i] : true);
                p.i--;
                return b;
            } else
                return false;
        if(z == null) {
            if(!e)
                return false;
            if(p.i + 1 >= p.x.length)
                return false;
            if(p.x[p.i + 1] == null)
                return false;
            p.i++;
            boolean b = ConcatenationMatchesPath(p, y, p.x[p.i].heads,
                                                 p.epsilonAllowed != null ? p.epsilonAllowed[p.i] : true);
            p.i--;
            return b;
        }
        Node t = z.search(y.x);
        if((t != null) && ConcatenationMatchesPath(p, y.next, t.nextNodes, true))
            return true;
        if((z.CC != null) && z.CC.traverse(IN_ORDER, p, y) < 0)
            return true;
        if(e && z.contains(EndNode)) {
            if(p.i + 1 >= p.x.length)
                return false;
            if(p.x[p.i + 1] == null)
                return false;
            p.i++;
            boolean b = ConcatenationMatchesPath(p, y, p.x[p.i].heads,
                                                 p.epsilonAllowed != null ? p.epsilonAllowed[p.i] : true);
            p.i--;
            return b;
        }
        return false;
    }

    private static class ConcatenationPathMatcher
            implements TraversalActionPerformer<Node> {

        private TerminalTrie[] x;
        private boolean[] epsilonAllowed;
        private int i = 0;

        ConcatenationPathMatcher init(TerminalTrie[] x, boolean[] epsilonAllowed) {
            this.x = x;
            this.epsilonAllowed = epsilonAllowed;
            i = 0;
            return this;
        }

        public boolean perform(Node x, int order, Object o) {
            Path.Node<Terminal> y = (Path.Node<Terminal>)o;
            return (x.x != null) && x.x.matches(y.x)
                    && ConcatenationMatchesPath(this, y.next, x.nextNodes, true);
        }
    }
    private static ThreadLocal<ConcatenationPathMatcher> CONCATENATION_PATH_MATCHER =
            new ThreadLocal<ConcatenationPathMatcher>() {

                protected ConcatenationPathMatcher initialValue() {
                    return new ConcatenationPathMatcher();
                }
            };

    public static boolean BoundedConcatenationMatchesPath(
            TerminalTrie[] x, int bound, Path<Terminal> y) {
        return BoundedConcatenationMatchesPath(x, bound, true, y);
    }

    public static boolean BoundedConcatenationMatchesPath(
            TerminalTrie[] x, int bound, boolean firstEpsilonAllowed,
            Path<Terminal> y) {
        if((x == null) || (x.length == 0))
            return false;
        if(x[0] == null)
            return false;
        if(bound < 0)
            throw new IllegalArgumentException("Bound is negative");
        BoundedConcatenationPathMatcher p =
                BOUNDED_CONCATENATION_PATH_MATCHER.get().init(x, bound, null);
        return BoundedConcatenationMatchesPath(p, y != null ? y.head : null,
                                                x[0].heads, firstEpsilonAllowed);
    }

    public static boolean BoundedConcatenationMatchesPath(
            TerminalTrie[] x, int bound, boolean[] epsilonAllowed,
            Path<Terminal> y) {
        if((x == null) || (x.length == 0))
            return false;
        if(x[0] == null)
            return false;
        if(bound < 0)
            throw new IllegalArgumentException("Bound is negative");
        if((epsilonAllowed != null) && (epsilonAllowed.length < x.length))
            throw new IllegalArgumentException(
                    "Epsilon array must be not less the trie array");
        BoundedConcatenationPathMatcher p =
                BOUNDED_CONCATENATION_PATH_MATCHER.get().init(x, bound,
                                                              epsilonAllowed);
        return BoundedConcatenationMatchesPath(p, y != null ? y.head : null,
                                                x[0].heads,
                                                epsilonAllowed != null ? epsilonAllowed[0] : true);
    }

    private static boolean BoundedConcatenationMatchesPath(
            BoundedConcatenationPathMatcher p, Path.Node<Terminal> y,
            NodeSet z, boolean e) {
        if((y == null) || (p.bound == 0))
            if(p.bound == 0 ? (e ? (z == null) || !z.isEmpty()
                               : (z != null) && (z.size() > (z.contains(EndNode) ? 1 : 0)))
               : (e && ((z == null) || z.contains(EndNode)))) {
                if(p.i + 1 >= p.x.length)
                    return true;
                if(p.x[p.i + 1] == null)
                    return false;
                p.i++;
                boolean b = BoundedConcatenationMatchesPath(p, y, p.x[p.i].heads,
                                                            p.epsilonAllowed != null ? p.epsilonAllowed[p.i] : true);
                p.i--;
                return b;
            } else
                return false;
        if(z == null) {
            if(!e)
                return false;
            if(p.i + 1 >= p.x.length)
                return false;
            if(p.x[p.i + 1] == null)
                return false;
            p.i++;
            boolean b = BoundedConcatenationMatchesPath(p, y, p.x[p.i].heads,
                                                        p.epsilonAllowed != null ? p.epsilonAllowed[p.i] : true);
            p.i--;
            return b;
        }
        p.bound--;
        Node t = z.search(y.x);
        if((t != null) && BoundedConcatenationMatchesPath(p, y.next, t.nextNodes,
                                                          true)) {
            p.bound++;
            return true;
        }
        if((z.CC != null) && z.CC.traverse(IN_ORDER, p, y) < 0) {
            p.bound++;
            return true;
        }
        p.bound++;
        if(e && z.contains(EndNode)) {
            if(p.i + 1 >= p.x.length)
                return false;
            if(p.x[p.i + 1] == null)
                return false;
            p.i++;
            boolean b = BoundedConcatenationMatchesPath(p, y, p.x[p.i].heads,
                                                        p.epsilonAllowed != null ? p.epsilonAllowed[p.i] : true);
            p.i--;
            return b;
        }
        return false;
    }

    private static class BoundedConcatenationPathMatcher
            implements TraversalActionPerformer<Node> {

        TerminalTrie[] x;
        boolean[] epsilonAllowed;
        private int i = 0;
        int bound = 0;

        BoundedConcatenationPathMatcher init(
                TerminalTrie[] x, int bound, boolean[] epsilonAllowed) {
            this.x = x;
            this.bound = bound;
            this.epsilonAllowed = epsilonAllowed;
            return this;
        }

        public boolean perform(Node x, int order, Object o) {
            Path.Node<Terminal> y = (Path.Node<Terminal>)o;
            return (x.x != null) && x.x.matches(y.x)
                    && BoundedConcatenationMatchesPath(this, y.next, x.nextNodes,
                                                       true);
        }
    }
    private static ThreadLocal<BoundedConcatenationPathMatcher> BOUNDED_CONCATENATION_PATH_MATCHER =
            new ThreadLocal<BoundedConcatenationPathMatcher>() {

                protected BoundedConcatenationPathMatcher initialValue() {
                    return new BoundedConcatenationPathMatcher();
                }
            };

    public static boolean BoundedConcatenationMatchesPrefixOfPath(
            TerminalTrie[] x, int bound, Path<Terminal> y) {
        return BoundedConcatenationMatchesPrefixOfPath(x, bound, true, y);
    }

    public static boolean BoundedConcatenationMatchesPrefixOfPath(
            TerminalTrie[] x, int bound, boolean firstEpsilonAllowed,
            Path<Terminal> y) {
        if((x == null) || (x.length == 0))
            return false;
        if(x[0] == null)
            return false;
        if(bound < 0)
            throw new IllegalArgumentException("Bound is negative");
        BoundedConcatenationPrefixOfPathMatcher p =
                BOUNDED_CONCATENATION_PREFIX_OF_PATH_MATCHER.get().init(x, bound,
                                                                        null);
        return BoundedConcatenationMatchesPrefixOfPath(p,
                                                        y != null ? y.head : null,
                                                        x[0].heads,
                                                        firstEpsilonAllowed);
    }

    public static boolean BoundedConcatenationMatchesPrefixOfPath(
            TerminalTrie[] x, int bound, boolean[] epsilonAllowed,
            Path<Terminal> y) {
        if((x == null) || (x.length == 0))
            return false;
        if(x[0] == null)
            return false;
        if(bound < 0)
            throw new IllegalArgumentException("Bound is negative");
        if((epsilonAllowed != null) && (epsilonAllowed.length < x.length))
            throw new IllegalArgumentException(
                    "Epsilon array must be not less the trie array");
//        if((y!=null)&&(bound<y.length()))
//            return(false);
        BoundedConcatenationPrefixOfPathMatcher p =
                BOUNDED_CONCATENATION_PREFIX_OF_PATH_MATCHER.get().init(x, bound,
                                                                        epsilonAllowed);
        return BoundedConcatenationMatchesPrefixOfPath(p,
                                                        y != null ? y.head : null,
                                                        x[0].heads,
                                                        epsilonAllowed != null ? epsilonAllowed[0] : true);
    }

    private static boolean BoundedConcatenationMatchesPrefixOfPath(
            BoundedConcatenationPrefixOfPathMatcher p, Path.Node<Terminal> y,
            NodeSet z, boolean e) {
        if((y == null) || (p.bound == 0))
            if(p.bound == 0 ? (e ? (z == null) || !z.isEmpty()
                               : (z != null) && (z.size() > (z.contains(EndNode) ? 1 : 0)))
               : (e && ((z == null) || z.contains(EndNode)))) {
                if(p.i + 1 >= p.x.length)
                    return true;
                if(p.x[p.i + 1] == null)
                    return false;
                p.i++;
                boolean b = BoundedConcatenationMatchesPrefixOfPath(p, y,
                                                                    p.x[p.i].heads,
                                                                    p.epsilonAllowed != null ? p.epsilonAllowed[p.i] : true);
                p.i--;
                return b;
            } else
                return false;
        if(z == null) {
            if(!e)
                return false;
            if(p.i + 1 >= p.x.length)
                return true;
            if(p.x[p.i + 1] == null)
                return false;
            p.i++;
            boolean b = BoundedConcatenationMatchesPrefixOfPath(p, y,
                                                                p.x[p.i].heads,
                                                                p.epsilonAllowed != null ? p.epsilonAllowed[p.i] : true);
            p.i--;
            return b;
        }
        p.bound--;
        Node t = z.search(y.x);
        if((t != null) && BoundedConcatenationMatchesPrefixOfPath(p, y.next,
                                                                  t.nextNodes,
                                                                  true)) {
            p.bound++;
            return true;
        }
        if((z.CC != null) && z.CC.traverse(IN_ORDER, p, y) < 0) {
            p.bound++;
            return true;
        }
        p.bound++;
        if(e && z.contains(EndNode)) {
            if(p.i + 1 >= p.x.length)
                return true;
            if(p.x[p.i + 1] == null)
                return false;
            p.i++;
            boolean b = BoundedConcatenationMatchesPrefixOfPath(p, y,
                                                                p.x[p.i].heads,
                                                                p.epsilonAllowed != null ? p.epsilonAllowed[p.i] : true);
            p.i--;
            return b;
        }
        return false;
    }

    private static class BoundedConcatenationPrefixOfPathMatcher
            implements TraversalActionPerformer<Node> {

        private TerminalTrie[] x;
        private boolean[] epsilonAllowed;
        private int bound = 0;
        private int i = 0;

        BoundedConcatenationPrefixOfPathMatcher init(
                TerminalTrie[] x, int bound, boolean[] epsilonAllowed) {
            this.x = x;
            this.bound = bound;
            this.epsilonAllowed = epsilonAllowed;
            return this;
        }

        public boolean perform(Node x, int order, Object o) {
            Path.Node<Terminal> y = (Path.Node<Terminal>)o;
            return (x.x != null) && x.x.matches(y.x)
                    && BoundedConcatenationMatchesPrefixOfPath(this, y.next,
                                                               x.nextNodes, true);
        }
    }
    private static ThreadLocal<BoundedConcatenationPrefixOfPathMatcher> BOUNDED_CONCATENATION_PREFIX_OF_PATH_MATCHER =
            new ThreadLocal<BoundedConcatenationPrefixOfPathMatcher>() {

                protected BoundedConcatenationPrefixOfPathMatcher initialValue() {
                    return new BoundedConcatenationPrefixOfPathMatcher();
                }
            };

    public static TerminalTrie Concatenation(TerminalTrie[] x) {
        if((x == null) || (x.length == 0) || (x[0] == null))
            return null;
        TerminalTrie r = new TerminalTrie();
        r.addConcatenation(x);
        return r;
    }

    public static TerminalTrie BoundedConcatenation(TerminalTrie[] x, int bound) {
        if(bound < 0)
            throw new IllegalArgumentException(
                    "pll.TerminalTrie.BoundedConcatenation: bound is negative");
        if((x == null) || (x.length == 0) || (x[0] == null))
            return null;
        TerminalTrie r = new TerminalTrie();
        r.addBoundedConcatenation(x, bound);
        return r;
    }

    public static TerminalTrie BoundedConcatenation(
            FirstSetOf<? extends Token>[] x, int bound) {
        if(bound < 0)
            throw new IllegalArgumentException(
                    "pll.TerminalTrie.BoundedConcatenation: bound is negative");
        if((x == null) || (x.length == 0) || (x[0] == null))
            return null;
        TerminalTrie r = new TerminalTrie();
        r.addBoundedConcatenationOfFirstSets(x, bound);
        return r;
    }

    public static void main(String[] args) {
        NodeSet t = new NodeSet();
        Node.Factory f = NODE_FACTORY.get();
        System.out.println("t: " + t);
        t.addOrGet(new Terminal.Character((int)'a'), f);
        System.out.println("t: " + t);
        System.out.println("f.last: " + f.last);
        t.addOrGet(new Terminal.Character((int)'a'), f);
        System.out.println("t: " + t);
        System.out.println("f.last: " + f.last);
    }

    public int traverse(int order,
                        TraversalActionPerformer<? super Path<Terminal>> p) {
        return traverseSelected(order, p, null, null);
    }

    public int traverse(int order,
                        TraversalActionPerformer<? super Path<Terminal>> p,
                        Object o) {
        return traverseSelected(order, p, o, null);
    }

    public int traverseSelected(int order,
                                TraversalActionPerformer<? super Path<Terminal>> p,
                                Selector<Path<Terminal>> s) {
        return traverseSelected(order, p, null, s);
    }

    public int traverseSelected(int order,
                                TraversalActionPerformer<? super Path<Terminal>> p,
                                Object o,
                                Selector<Path<Terminal>> s) {
        return traverseSelectedRange(order, p, o, s, null, true, null, true);
    }

    public int traverseRange(int order,
                             TraversalActionPerformer<? super Path<Terminal>> p,
                             Path<Terminal> l, Path<Terminal> r) {
        return traverseSelectedRange(order, p, null, null, l, true, r, false);
    }

    public int traverseRange(int order,
                             TraversalActionPerformer<? super Path<Terminal>> p,
                             Object o, Path<Terminal> l, Path<Terminal> r) {
        return traverseSelectedRange(order, p, o, null, l, true, r, false);
    }

    public int traverseRange(int order,
                             TraversalActionPerformer<? super Path<Terminal>> p,
                             Path<Terminal> l, boolean left_included,
                             Path<Terminal> r, boolean right_included) {
        return traverseSelectedRange(order, p, null, null,
                                      l, left_included, r, right_included);
    }

    public int traverseRange(int order,
                             TraversalActionPerformer<? super Path<Terminal>> p,
                             Object o, Path<Terminal> l, boolean left_included,
                             Path<Terminal> r, boolean right_included) {
        return traverseSelectedRange(order, p, o, null,
                                      l, left_included, r, right_included);
    }

    public int traverseSelectedRange(int order,
                                     TraversalActionPerformer<? super Path<Terminal>> p,
                                     Selector<Path<Terminal>> s,
                                     Path<Terminal> l, Path<Terminal> r) {
        return traverseSelectedRange(order, p, null, s, l, true, r, false);
    }

    public int traverseSelectedRange(int order,
                                     TraversalActionPerformer<? super Path<Terminal>> p,
                                     Object o, Selector<Path<Terminal>> s,
                                     Path<Terminal> l, Path<Terminal> r) {
        return traverseSelectedRange(order, p, o, s, l, true, r, false);
    }

    public int traverseSelectedRange(int order,
                                     TraversalActionPerformer<? super Path<Terminal>> p,
                                     Selector<Path<Terminal>> s,
                                     Path<Terminal> l, boolean left_included,
                                     Path<Terminal> r, boolean right_included) {
        return traverseSelectedRange(order, p, null, s,
                                      l, left_included, r, right_included);
    }

    public int traverseSelectedRange(int order,
                                     TraversalActionPerformer<? super Path<Terminal>> p,
                                     Object o, Selector<Path<Terminal>> s,
                                     Path<Terminal> l, boolean left_included,
                                     Path<Terminal> r, boolean right_included) {
        if((order == 0) || (p == null))
            return 0;
        Path<Terminal> sl, sr;
        if(s == null) {
            sl = l;
            sr = r;
        } else {
            sl = s.lowerBound();
            if((l != null) && ((sl == null) || l.compareTo(sl) > 0))
                sl = l;
            else
                left_included = (sl == null) || s.selects(sl);
            sr = s.upperBound();
            if((r != null) && ((sr == null) || r.compareTo(sr) < 0))
                sr = r;
            else
                right_included = (sr == null) || s.selects(sr);
        }
        if((sl == null) && (sr == null)) {
            SelectiveTraverser tp = new SelectiveTraverser(p, s);
            heads.traverse(order, tp, o);
            return tp.np;
        }
        SelectiveRangeTraverser tp = new SelectiveRangeTraverser(p, s,
                                                                 sl,
                                                                 left_included,
                                                                 sr,
                                                                 right_included);
        heads.traverseRange(order, tp, o,
                            ((sl != null) && (sl.head != null)) ? heads.ceil(
                sl.head.x) : null,
                            (sl != null) && (sl.head != null) && (sl.head.next == null) ? left_included : true,
                            ((sr != null) && (sr.head != null)) ? heads.floor(
                sr.head.x) : null,
                            (sr != null) && (sr.head != null) && (sr.head.next == null) ? right_included : true);
        return tp.np;
    }

    private static class SelectiveTraverser
            implements TraversalActionPerformer<Node> {

        private final TraversalActionPerformer<? super Path<Terminal>> p;
        private final Selector<Path<Terminal>> s;
        private final Path<Terminal> cp = new Path<Terminal>();
        private int np = 0;

        public SelectiveTraverser(
                TraversalActionPerformer<? super Path<Terminal>> p,
                Selector<Path<Terminal>> s) {
            if(p == null)
                throw new NullPointerException(
                        "TraversalActionPerformer is null");
            this.p = p;
            if(s == null)
                this.s = Selector.Accepter;
            else
                this.s = s;
        }

        public boolean perform(Node x, int order, Object o) {
            if(x == EndNode) {
                np++;
                Path<Terminal> cpr = cp.cloneReversed();
                if(s.selects(cpr) && p.perform(cpr, order, o)) {
                    np = -np;
                    return true;
                }
            } else {
                cp.push(x.x);
                if(x.nextNodes == null) {
                    np++;
                    Path<Terminal> cpr = cp.cloneReversed();
                    if(s.selects(cpr) && p.perform(cp.cloneReversed(), order, o)) {
                        np = -np;
                        cp.pop();
                        return true;
                    }
                } else if(x.nextNodes.traverse(order, this, o) < 0) {
                    np = -np;
                    cp.pop();
                    return true;
                }
                cp.pop();
            }
            return false;
        }

        public int getCount() {
            return np;
        }
    }

    private static class SelectiveRangeTraverser
            implements TraversalActionPerformer<Node> {

        public final TraversalActionPerformer<? super Path<Terminal>> p;
        private final Path<Terminal> cp = new Path<Terminal>();
        public final Selector<Path<Terminal>> s;
        private Path.Node<Terminal> l, r;
        boolean left_included, right_included;
        private int np = 0;

        public SelectiveRangeTraverser(
                TraversalActionPerformer<? super Path<Terminal>> p,
                Selector<Path<Terminal>> s,
                Path<Terminal> l, boolean left_included,
                Path<Terminal> r, boolean right_included) {
            if(p == null)
                throw new NullPointerException(
                        "TraversalActionPerformer is null");
            this.p = p;
            if(s == null)
                this.s = Selector.Accepter;
            else
                this.s = s;
            this.l = l != null ? l.head : null;
            this.r = r != null ? r.head : null;
            this.left_included = left_included;
            this.right_included = right_included;
        }

        public boolean perform(Node x, int order, Object o) {
            if(x == EndNode) {
                np++;
                Path<Terminal> cpr = cp.cloneReversed();
                if(s.selects(cpr) && p.perform(cp.cloneReversed(), order, o)) {
                    np = -np;
                    return true;
                }
            } else {
                cp.push(x.x);
                Path.Node<Terminal> lt = l;
                Path.Node<Terminal> rt = r;
                l = l != null ? l.next : null;
                r = r != null ? r.next : null;
                if(x.nextNodes == null) {
                    np++;
                    Path<Terminal> cpr = cp.cloneReversed();
                    if(s.selects(cpr) && p.perform(cp.cloneReversed(), order, o)) {
                        np = -np;
                        cp.pop();
                        l = lt;
                        r = rt;
                        return true;
                    }
                } else if(x.nextNodes.traverseRange(order, this, o,
                                                    (l != null) ? x.nextNodes.
                        ceil(l.x) : null,
                                                    (l != null) && (l.next == null) ? left_included : true,
                                                    (r != null) ? x.nextNodes.
                        floor(r.x) : null,
                                                    (r != null) && (r.next == null) ? right_included : true) < 0) {
                    np = -np;
                    cp.pop();
                    l = lt;
                    r = rt;
                    return true;
                }
                l = lt;
                r = rt;
                cp.pop();
            }
            return false;
        }
    }

    public boolean containsAll(TerminalTrie x) {
        if((x == null) || x.isEmpty())
            return true;
        if(x == this)
            return true;
        if(isEmpty())
            return false;
        return x.heads.traverse(IN_ORDER, CONTAINMENT_CHECKER, heads) >= 0;
    }
    private static final TraversalActionPerformer<Node> CONTAINMENT_CHECKER =
            new TraversalActionPerformer<Node>() {

                public boolean perform(Node x, int order, Object o) {
                    NodeSet u = (NodeSet)o;
                    if(x == EndNode)
                        return (u != null) && !u.contains(EndNode);
                    else {
                        if(u == null)
                            return true;
                        Node y = u.get(x);
                        if(y == null)
                            return true;
                        if(x.nextNodes == null) {
                            if(y.nextNodes == null)
                                return false;
                            if(y.nextNodes.isEmpty())
                                throw new RuntimeException(
                                        "Unclosed path discovered");
                            return y.nextNodes.contains(EndNode);
                        }
                        if(y.nextNodes == null)
                            return true;
                        if(x.nextNodes.isEmpty() || y.nextNodes.isEmpty())
                            throw new RuntimeException(
                                    "Unclosed path discovered");
                        return x.nextNodes.traverse(IN_ORDER,
                                                     CONTAINMENT_CHECKER,
                                                     y.nextNodes) < 0;
                    }
                }
            };

    public boolean containsConcatenation(TerminalTrie[] x) {
        if((x == null) || (x.length == 0))
            return true;
        for(TerminalTrie y : x)
            if((y == null) || y.isEmpty())
                return true;
        if(isEmpty())
            return false;
        ConcatenationContainmentChecker p = CONCATENATION_CONTAINMENT_CHECKER.
                get().init(x);
        return x[0].heads.traverse(IN_ORDER, p, heads) >= 0;
    }

    private static class ConcatenationContainmentChecker implements
            TraversalActionPerformer<Node> {

        private TerminalTrie[] x;
        private int i = 0;

        ConcatenationContainmentChecker init(TerminalTrie[] x) {
            this.x = x;
            i = 0;
            return this;
        }

        public boolean perform(Node a, int order, Object o) {
            NodeSet u = (NodeSet)o;
            int j;
            if(a == EndNode) {
                j = i;
                i++;
                while((i < x.length) && ((x[i].heads.size() == 1) && x[i].heads.
                        contains(EndNode)))
                    i++;
                if(i < x.length)
                    if(x[i].heads.traverse(IN_ORDER, this, u) < 0) {
                        i = j;
                        return true;
                    } else {
                        i = j;
                        return false;
                    }
                else {
                    i = j;
                    return (u != null) && !u.contains(EndNode);
                }
            } else {
                if(u == null)
                    return true;
                Node b = u.get(a);
                if(b == null)
                    return true;
                if((a.nextNodes == null) || ((a.nextNodes.size() == 1) && a.nextNodes.
                        contains(EndNode))) {
                    j = i;
                    i++;
                    while((i < x.length) && ((x[i].heads.size() == 1) && x[i].heads.
                            contains(EndNode)))
                        i++;
                    if(i < x.length) {
                        if(b.nextNodes == null) {
                            i = j;
                            return true;
                        }
                        if(b.nextNodes.isEmpty())
                            throw new RuntimeException(
                                    "Unclosed path discovered");
                        if(x[i].heads.traverse(IN_ORDER, this, b.nextNodes) < 0) {
                            i = j;
                            return true;
                        } else {
                            i = j;
                            return false;
                        }
                    } else {
                        i = j;
                        return (b.nextNodes != null) && !b.nextNodes.contains(
                                EndNode);
                    }
                } else {
                    if(b.nextNodes == null)
                        return true;
                    return a.nextNodes.traverse(IN_ORDER, this, b.nextNodes) < 0;
                }
            }
        }
    }
    private static final ThreadLocal<ConcatenationContainmentChecker> CONCATENATION_CONTAINMENT_CHECKER =
            new ThreadLocal<ConcatenationContainmentChecker>() {

                protected ConcatenationContainmentChecker initialValue() {
                    return new ConcatenationContainmentChecker();
                }
            };

    public boolean containsBoundedConcatenation(TerminalTrie[] x, int bound) {
        if(bound < 0)
            throw new IllegalArgumentException("Bound is negative");
        if((x == null) || (x.length == 0))
            return true;
        for(TerminalTrie y : x)
            if((y == null) || y.isEmpty())
                return true;
        if(bound == 0)
            return heads.contains(EndNode);
        BoundedConcatenationContainmentChecker p =
                BOUNDED_CONCATENATION_CONTAINMENT_CHECKER.get().init(x, bound);
        return x[0].heads.traverse(IN_ORDER, p, heads) >= 0;
    }

    private static class BoundedConcatenationContainmentChecker
            implements TraversalActionPerformer<Node> {

        private TerminalTrie[] x;
        private int i = 0;
        private int bound = 0;

        BoundedConcatenationContainmentChecker init(TerminalTrie[] x, int bound) {
            this.x = x;
            this.bound = bound;
            i = 0;
            return this;
        }

        public boolean perform(Node a, int order, Object o) {
            NodeSet u = (NodeSet)o;
            int j;
            if(a == EndNode) {
                j = i;
                i++;
                while((i < x.length) && ((x[i].heads.size() == 1) && x[i].heads.
                        contains(EndNode)))
                    i++;
                if(i < x.length)
                    if(x[i].heads.traverse(IN_ORDER, this, u) < 0) {
                        i = j;
                        return true;
                    } else {
                        i = j;
                        return false;
                    }
                else {
                    i = j;
                    return (u != null) && !u.contains(EndNode);
                }
            } else {
                Node b = u.get(a);
                if(b == null)
                    return true;
                if(bound == 1)
                    return (u != null) && !u.contains(EndNode);
                else if((a.nextNodes == null) || ((a.nextNodes.size() == 1) && a.nextNodes.
                        contains(EndNode))) {
                    j = i;
                    i++;
                    while((i < x.length) && ((x[i].heads.size() == 1) && x[i].heads.
                            contains(EndNode)))
                        i++;
                    if(i < x.length) {
                        if(b.nextNodes == null) {
                            i = j;
                            return true;
                        }
                        if(b.nextNodes.isEmpty())
                            throw new RuntimeException(
                                    "Unclosed path discovered");
                        bound--;
                        if(x[i].heads.traverse(IN_ORDER, this, b.nextNodes) < 0) {
                            bound++;
                            i = j;
                            return true;
                        } else {
                            bound++;
                            i = j;
                            return false;
                        }
                    } else {
                        i = j;
                        return (b.nextNodes != null) && !b.nextNodes.contains(
                                EndNode);
                    }
                } else {
                    if(b.nextNodes == null)
                        return true;
                    bound--;
                    if(a.nextNodes.traverse(IN_ORDER, this, b.nextNodes) < 0) {
                        bound++;
                        return true;
                    } else {
                        bound++;
                        return true;
                    }
                }
            }
        }
    }
    private static final ThreadLocal<BoundedConcatenationContainmentChecker> BOUNDED_CONCATENATION_CONTAINMENT_CHECKER =
            new ThreadLocal<BoundedConcatenationContainmentChecker>() {

                protected BoundedConcatenationContainmentChecker initialValue() {
                    return new BoundedConcatenationContainmentChecker();
                }
            };

    public Collection<? super Terminal> addTo(Collection<? super Terminal> x) {
        if(x == null)
            return x;
        for(Node t : heads)
            if(t.x == null)
                x.add(Terminal.Epsilon);
            else
                x.add(t.x);
        return x;
    }

    public int emptyTo(TraversalActionPerformer<? super Path<Terminal>> p) {
        return emptyTo(p, null);
    }

    public int emptyTo(TraversalActionPerformer<? super Path<Terminal>> p,
                       Object o) {
        if(p == null)
            throw new NullPointerException("Performer is null");
        if(heads.isEmpty())
            return 0;
        int h = traverse(IN_ORDER, p, o);
        clear();
        return h;
    }
}
