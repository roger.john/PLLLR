/*
 * Test.java
 *
 * Created on 9. Februar 2007, 01:06
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package plllr;
//import java.util.Vector;

import bbtree.Trie;
import bbtree.Path;
import bbtree.BBTree;




/**
 *
 * @author RJ
 */
public class Test {
    public static void main(String[] args) {
//        Path<Character> a = Path.valueOf("a"),b = Path.valueOf("ab"),c = Path.valueOf("abc"),d = Path.valueOf("acd");
//        Trie<Character> t = new Trie<Character>();
//        System.out.println(a);
//        System.out.println(b);
//        System.out.println(c);
//        System.out.println(d);
//        t.insertPath(a);
//        System.out.println(t);
//        t.insertPath(b);
//        System.out.println(t);
//        t.insertPath(c);
//        System.out.println(t);
//        t.insertPath(d);
//        System.out.println(t);
//        t.insertPath(a);
//        System.out.println(t);
//        System.out.println(t.containsPath(a));
//        System.out.println(t.containsPath(b));
//        System.out.println(t.containsPath(c));
//        System.out.println(t.containsPath(d));
//        for(Path<Character> x:t)
//            System.out.println(x);
//        t.deletePath(a);
//        System.out.println(t);
//        System.out.println(t.containsPath(a));
//        System.out.println(t.containsPath(b));
//        System.out.println(t.containsPath(c));
//        System.out.println(t.containsPath(d));
//        System.out.println("Forcing garbage collection...");
//        System.gc();
//        t.deletePath(d);
//        System.out.println(t);
//        System.out.println(t.containsPath(a));
//        System.out.println(t.containsPath(b));
//        System.out.println(t.containsPath(c));
//        System.out.println(t.containsPath(d));
//        System.out.println("Forcing garbage collection...");
//        System.gc();
        int n = 1<<16,max_length=1<<6;
        int min = Integer.MAX_VALUE,max = 0,i,j,k,l;
        long sum = 0;
        java.util.Random r = args.length>0?new java.util.Random(Long.parseLong(args[0])):new java.util.Random();
        System.gc();
        boolean b[] = new boolean[n];
        BBTree<Path<Character>> bt= new BBTree<Path<Character>>();
        Path<Character>[] p;
        Path<Character> pt;
        java.util.Iterator<Path<Character>> it;
        Character x;
        System.out.print("Now building "+n+" random paths...");
        for(i=0;i<n;i++) {
            j = 1+r.nextInt(max_length+1);
            pt = new Path<Character>();
            for(k=0;k<j;k++) {
                x = new Character((char)('a'+r.nextInt(26)));
                pt.append(x);
            }
            bt.add(pt);
            if(j>max)
                max = j;
            if(j<min)
                min = j;
            sum += j;
        }
         n = bt.size();
        System.out.printf("done\nNow building trie with %1$d paths, lengths between %2$d and %3$d, average %4$.1f...",n,min,max,((double)sum)/((double)n));
        p = new Path[n];
        p = bt.toArray(p);
        bt.clear();
        bt = null;
        Trie<Character> t = new Trie<Character>();
        for(i=0;i<n;i++) {
            t.add(p[i]);
            b[i] = true;
            if(t.contains(p[i])!=b[i])
               System.out.println("Error: "+p[i]+" "+(b[i]?"should":"shouldn't")+" be in the trie!");
            //t.check();
            //System.out.println(t);
        }
        System.out.println("done\n"+/*t+"\n"+*/"Now checking...");
        for(i=0;i<n;i++)
            if(t.contains(p[i])!=b[i])
                System.out.println("Error: "+p[i]+" "+(b[i]?"should":"shouldn't")+" be in the trie!");
        if(false)
            return;
        i = t.size();
        System.out.println("Done checking. Now iterating "+i+" element"+(i==1?"":"s")+" of the trie...");
        j = 0; k = 0; l = 0; pt = null;
        for(Path<Character> px:t) {
            if(!t.contains(px))
               k++;
            j++;
            if(px.compareTo(pt)<=0)
                l++;
        }
        if(j!=i)
            System.out.println("Error: only "+j+" of "+i+" element"+(i==1?"":"s")+" were iterated");
        if(k>0)
            System.out.println("Error: "+k+" element"+(k==1?"":"s")+" were missed");
        if(l>0)
            System.out.println("Error: "+k+" element"+(k==1?"":"s")+" were in wrong order");
        System.out.print("Done. Now truncating trie to length "+(max>>>1));
        t.bound(max>>>1);
        i = t.size(); j = 0; k = 0; l = 0; pt = null;
        for(Path<Character> px:t) {
            if(px.length()>(max>>>1))
               k++;
            j++;
            if(px.compareTo(pt)<=0)
                l++;
        }
        if(j!=i)
            System.out.println("Error: only "+j+" of "+i+" element"+(i==1?"":"s")+" were iterated");
        if(k>0)
            System.out.println("Error: "+k+" element"+(k==1?"":"s")+" were wrong");
        if(l>0)
            System.out.println("Error: "+k+" element"+(k==1?"":"s")+" were in wrong order");
//        System.out.println("Done. Now deleting random half of all paths in trie...");
//        for(i=0;i<n/2;i++) {
//            j = r.nextInt(n);
//            t.deletePath(p[j]);
//            b[j] = false;
//            //t.check();
//            //System.out.println(t);
//        }
//        System.gc();
//        System.out.println("done\nNow checking...");
//        for(i=0;i<n;i++)
//            if(t.containsPath(p[i])!=b[i])
//                System.out.println("Error: "+p[i]+" "+(b[i]?"should":"shouldn't")+" be in the trie");
//        t.check();
//        System.out.println("Done checking. Now adding random half of all paths to trie...");
//        for(i=0;i<n/2;i++) {
//            j = r.nextInt(n);
//            t.insertPath(p[j]);
//            b[j] = true;
//            //t.check();
//            //System.out.println(t);
//        }
//        System.out.println("done\nNow checking...");
//        for(i=0;i<n;i++)
//            if(t.containsPath(p[i])!=b[i])
//                System.out.println("Error: "+p[i]+" "+(b[i]?"should":"shouldn't")+" be in the trie!");
//        System.out.println("Done checking. Now deleting all paths from trie...");
//        for(i=0;i<n;i++) {
//            //System.out.print(p[i]+":"+t);
//            t.deletePath(p[i]);
//            b[i] = false;
//            //System.out.println("=>"+t);
//            //t.check();
//        }
//        System.gc();
//        System.out.println("done\nNow checking...");
//        t.check();
//        for(i=0;i<n;i++)
//            if(t.containsPath(p[i])!=b[i])
//                System.out.println("Error: "+p[i]+" "+(b[i]?"should":"shouldn't")+" be in the trie!");
//        System.out.println("Done checking. Now the trie looks like this: "+t);
//        new Vector<String>();
//        new Vector<Vector<String>>();
//        new Vector<Vector<Vector<String>>>();
//        new Vector<Vector<Vector<Vector<String>>>>();
//        new Vector<Vector<Vector<Vector<Vector<String>>>>>();
//        new Vector<Vector<Vector<Vector<Vector<Vector<String>>>>>>();
//        Trie<Character> t = new Trie<Character>();
//        System.out.println(t);
//        t.insertPath(Path.valueOf("r"));
//        System.out.println(t);
//        t.insertPath(Path.valueOf("ruruz"));
//        System.out.println(t);
//        t.deletePath(Path.valueOf("ruruz"));
//        System.out.println(t);
//        t.check();
//        System.out.println(t);
//        System.gc();
//        ParserLR0.main(args);
//        System.gc();
//        ParserLR1.main(args);
//        System.gc();
//        ParserLR1BF.main(args);
//        System.gc();
//        GrammarReader.main(args);
//        Terminal t;
//        TerminalReader r;
//        for(String s:args) {
//            a+;
//            /*System.out.println("Parsing "+s+":");
//            try {
//                r = new TerminalReader.Java(new java.io.FileReader(s));
//                while((t=r.read())!=Terminal.End)
//                    System.out.print(t+" ");
//            } catch(java.io.IOException e) { System.out.println("\nError while parsing "+s+": "+e.getMessage()); }
//            System.out.println("\r\nDone with "+s);*/
//        }
//        int i,j,k = Integer.parseInt(args[0]);
//        long s,e;
//        Path<Derivation.Item.NonTerminal> d;
//        Path.Node<Derivation.Item.NonTerminal> dn;
//        NonTerminal A = new NonTerminal("A");
//        Rule[] P = new Rule[3];
//        P[0] = new Rule(A,(Token[])null);
//        P[1] = new Rule(A,new Token[] {new Terminal.Character('a')});
//        P[2] = new Rule(A,(Token[])null);
//        Grammar G;
//        ParserLR0 p = new ParserLR0();
//        try {
//            for(j=2;j<=k;j++) {
//                P[0].rhs = new Token[j];
//                for(i=0;i<j;i++)
//                    P[0].rhs[i] = A;
//                G = Grammar.construct(new NonTerminal[] {A},P,A);
//                System.out.print(j+": ");
//                s = System.currentTimeMillis();
//                d = p.parse(new TerminalReader.Byte(new java.io.FileReader(args[1])),G,false);
//                e = System.currentTimeMillis();
//                if((d==null)||(d.head==null))
//                    System.out.println("no derivation possible("+((e-s)/1000.0)
//                        +"s,"+p.nodes_created+"n,"+p.shift_edges_created+"se,"
//                        +p.reduce_edges_created+"re,"+p.shift_edges_visited
//                        +"sv,"+p.reduce_edges_visited+"rv,"
//                        +p.derivation_nodes_created+"dn,"
//                        +p.derivation_edges_created+"de)");
//                else {
//                    System.out.println("input was accepted("+((e-s)/1000.0)
//                        +"s,"+p.nodes_created+"n,"+p.shift_edges_created+"se,"
//                        +p.reduce_edges_created+"re,"+p.shift_edges_visited
//                        +"sv,"+p.reduce_edges_visited+"rv,"
//                        +p.derivation_nodes_created+"dn,"
//                        +p.derivation_edges_created+"de)");
//                    int[] c;
//                    int o = d.length();
//                    System.out.println(j+": "+o+" dervation tree"+(o==1?" was":"s were")+" generated:");
//                    dn = d.head;
//                    while(dn!=null) {
//                        if(dn.x==null)
//                            System.out.println(j+": error: null entry in derivation list!");
//                        else {
//                            c = dn.x.count();
//                            System.out.println(j+": ("+
//                                    ((NonTerminal)(dn.x.x)).getEscapedName()+
//                                    ","+dn.x.pos+"p,"+dn.x.len+"L,"+c[0]+
//                                    "n,"+c[1]+"nI,"+c[2]+"tI,"+c[3]+"rI,"+
//                                    c[4]+"e)");
//                        }
//                        dn = dn.next;
//                    }
//                }
//            }
//        } catch(java.io.IOException ioe) { 
//            System.out.println("pll.test.main: IOException occured: "+ioe.getMessage());
//            ioe.printStackTrace();
//        }
    }
}
