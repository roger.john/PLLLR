/*
 * GrammarReader.java
 *
 * Created on 6. August 2007, 10:00
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */
package plllr;

import bbtree.*;
import bbtree.Utils.AsymmetricComparable;
import bbtree.Utils.KeyedElementFactory;
import java.io.*;
import plllr.FirstKSetSupplier.NonTerminalFirstSetCollection;
import plllr.FirstKSetSupplier.RuleWithFirstSets;

/**
 *
 * @author RJ
 */
public class GrammarReader {

    private int cc = -1, nc = -2, r_c = 1, n_c = 0;
    private long nbs = 0;
    private NonTerminal S;
    private Rule cr, fr;
    private Traversable.Set<NonTerminal> N;
    private Path<Rule> P;
    private Path<NonTerminal> NP;
    private Traversable.Set<Rule> PT;
    private String globalJavaCode = null, lexer = null;
    String compilerFile = null;
    FirstKSetSupplier fkss = null;
    static final FirstSetOf<Terminal> fse;

    static {
        TerminalTrie tt = new TerminalTrie();
        tt.add(Terminal.Epsilon);
        tt.makeFinal();
        fse = new FirstSetOf<Terminal>(Terminal.Epsilon, tt);
    }
    static final int[] STOP_CHARS_1 = new int[] {',', ' ', '\t', '\r', '\n', ')',
                                                 '}'};
    static final int[] STOP_CHARS_2 = new int[] {',', ' ', '\t', '\r', '\n', ')'};
    static final int[] STOP_CHARS_3 = new int[] {'=', ' ', '\t', '\r', '\n'};
    static final int[] STOP_CHARS_4 = new int[] {' ', ')', ','};
    private TerminalReader.LineBufferedCursorPositionReader r = null;
    private BBTree<TokenReplacement> R = new BBTree<TokenReplacement>();

    private static class TokenReplacement implements
            Comparable<TokenReplacement>,
                                                     AsymmetricComparable<Token> {

        public Token x;
        public NonTerminal y;

        public TokenReplacement(Token x) {
            this.x = x;
            this.y = null;
        }

        public TokenReplacement(Token x, NonTerminal y) {
            this.x = x;
            this.y = y;
        }

        public int compareTo(TokenReplacement u) {
            if(u == null)
                return (1);
            if(x == null)
                return (u.x == null ? 0 : -1);
            return (x.compareTo(u.x));
        }

        public int asymmetricCompareTo(Token t) {
            if(x == null)
                return (t == null ? 0 : -1);
            return (x.compareTo(t));
        }

        public static class Factory implements
                KeyedElementFactory<TokenReplacement, Token> {

            public TokenReplacement last;

            public void reset() {
                last = null;
            }

            public TokenReplacement lastCreated() {
                return (last);
            }

            public TokenReplacement createElement(Token x) {
                if(x == null)
                    throw (new NullPointerException("Null argument"));
                last = new TokenReplacement(x);
                return (last);
            }

            @Override
            public Token extractKey(TokenReplacement x) {
                return x.x;
            }
        }
        public static final ThreadLocal<Factory> FACTORY = new ThreadLocal<Factory>() {

            protected Factory initialValue() {
                return (new Factory());
            }
        };
    }

    /** Creates a new instance of GrammarReader */
    public GrammarReader() {
    }

    public static void main(String[] args) {
        GrammarReader r = new GrammarReader();
        Writer w;
        for(String x : args)
            try {
                System.out.println("PLL1LR0 v0.01..." + x + " :");
                Grammar g = r.readFromStream(new FileReader(x));
                w = new FileWriter(x + "c");
                g.writeToStream(w);
                w.flush();
                w.close();
                System.out.println("Compiled grammar was written to " + x + "c");
//                w = new PrintWriter(System.out);
//                g.writeToStream(w);
//                w.flush();
//                String s1 = g.toStringBuilder();
//                System.out.println(s1);
//                StringWriter w = new StringWriter();
//                g.writeToStream(w);
//                String s2 = w.toStringBuilder();
//                System.out.println(s2);
//                g = r.readFromStream(new StringReader(w.toStringBuilder()));
//                String s3 = g.toStringBuilder();
//                System.out.println(s3);
//                w.close();
//                w = new StringWriter();
//                g.writeToStream(w);
//                String s4 = w.toStringBuilder();
//                System.out.println(s4);
//                w.close();
//                System.out.println("compares give "+s1.compareTo(s3)+" and "+s2.compareTo(s4));
            } catch(Exception e) {
                System.out.println("\r\nError while loading file " + x + " : " + e.
                        getMessage());
                e.printStackTrace();
            }
    }

    public synchronized Grammar readFromStream(Reader r) throws IOException {
        return (readFromStream(r, true, null));
    }

    public synchronized Grammar readFromStream(Reader r, boolean calcFirst1Sets)
            throws IOException {
        return (readFromStream(r, calcFirst1Sets, null));
    }

    public synchronized Grammar readFromStream(Reader r, boolean calcFirst1Sets,
                                               ControllableExecutor ex) throws
            IOException {
        if(r == null)
            return (null);
        this.r = new TerminalReader.LineBufferedCursorPositionReader(r);
        int nl, pl;
        nbs = 0;
        S = null;
        cr = fr = null;
        globalJavaCode = lexer = compilerFile = null;
        fkss = null;
        r_c = 0;
        cc = -1;
        nc = this.r.read();
        readNextNWS();
        if(cc == '(') {
            if(NP == null)
                NP = new Path<NonTerminal>();
            else
                NP.clear();
            if((PT == null) || !(PT instanceof BBTree))
                PT = new BBTree<Rule>(Rule.IDComparator);
            else {
                PT.clear();
                PT.reorder(Rule.IDComparator, false);
            }
            readGrammar();
            PT.reorder(null, false);
            return (Grammar.construct(N, PT.toFinal(), S, lexer, true,
                                      calcFirst1Sets));
        } else if(cc == '[') {
            readNextNWS();
            String ch = parseIdentifier();
            if(!ch.startsWith("First") || !ch.endsWith("Set"))
                throw (new IOException("expected a firstKset in line "
                                       + this.r.getLineNumber() + "\r\n"
                                       + this.r.formatCurrentLineAndClose()));
            ch = ch.substring(5, ch.length() - 3);
            int k = 0;
            try {
                k = Integer.parseInt(ch);
            } catch(NumberFormatException nfe) {
                throw (new IOException("expected a nonnegative integer, got " + ch
                                       + " while parsing  the k of a firstKset in line "
                                       + this.r.getLineNumber() + "\r\n"
                                       + this.r.formatCurrentLineAndClose()));
            }
            if(cc != ']')
                throw (new IOException("expected \"]\", got "
                                       + (cc == -1 ? "to end of input" : "\""
                                                                         + Terminal.String.
                        escapeCodePoint(cc) + "\"")
                                       + " while parsing a firstKset in line "
                                       + this.r.getLineNumber() + "\r\n"
                                       + this.r.formatCurrentLineAndClose()));
            readNextNWS();
            if((N == null) || !(N instanceof BBTree))
                N = new BBTree<NonTerminal>();
            else
                N.clear();
            if((PT == null) || !(PT instanceof BBTree))
                PT = new BBTree<Rule>(Rule.IDComparator);
            else {
                PT.clear();
                PT.reorder(Rule.IDComparator, false);
            }
            Grammar g = new Grammar();
            fkss = new FirstKSetSupplier(g, k, ex);
            fkss.nfscc = new BBTree<NonTerminalFirstSetCollection>();
            fkss.tfsc = new BBTree<FirstSetOf<Terminal>>();
            g.fkss = fkss;
            TerminalTrie tt = new TerminalTrie();
            tt.add(Terminal.Epsilon);
            tt.makeFinal();
            fkss.tfsc.add(new FirstSetOf<Terminal>(Terminal.Epsilon, tt));
            tt = new TerminalTrie();
            if(k > 0)
                tt.add(Terminal.End);
            else
                tt.add(Terminal.Epsilon);
            tt.makeFinal();
            fkss.tfsc.add(new FirstSetOf<Terminal>(Terminal.End, tt));
            while(cc != -1)
                readFirstKSetStatement();
            this.r.close();
            if(S == null)
                System.out.println(
                        "Warning: found no start symbol while parsing firstKset");
            else
                g.S = S;
            g.N = N.toFinal();
            PT.reorder(null, false);
            g.P = PT.toFinal();
            fkss.reconstruct();
            g.setLexer(lexer);
            return g;
        } else {
            if((N == null) || !(N instanceof BBTree))
                N = new BBTree<NonTerminal>();
            else
                N.clear();
            if(P == null)
                P = new Path<Rule>();
            else
                P.clear();
            skipWS();
            System.out.print("Parsing grammar...");
            parseGrammar();
            nl = N.size();
            pl = P.length();
            System.out.println("success(" + nl + " symbol" + (nl != 1 ? "s" : "") + ", "
                               + pl + " rule" + (pl != 1 ? "s" : "") + ")!");
            this.r.close();
            if((S == null) && (fr != null))
                S = fr.lhs;
            System.out.print("Expanding grammar...");
            expandBNF();
            nl = N.size();
            pl = P.length();
            System.out.println("success(" + nl + " symbol" + (nl != 1 ? "s" : "") + ", "
                               + pl + " rule" + (pl != 1 ? "s" : "") + ")!");
            return (Grammar.construct(N.toFinal(),
                                      new Final<Rule>(P.toArray(new Rule[P.
                    length()])),
                                      S, lexer, false, calcFirst1Sets));
        }
    }

    private void expandBNF() {
        R.clear();
        n_c = 0;
        int j;
        Path.Node<Rule> n = P.head;
        Path<Token> p = new Path<Token>(), p2 = new Path<Token>();
        while(n != null) {
            if((n.x != null) && (n.x.rhs != null))
                if(n.x.rhs.length == 0)
                    n.x.rhs = null;
                else {
                    for(j = 0; j < n.x.rhs.length; j++)
                        p.append(expandTokenGroups(n.x.rhs[j]));
                    while(!p.isEmpty())
                        p2.append(expandToken(p.pop()));
                    if(p2.isEmpty())
                        n.x.rhs = null;
                    else
                        n.x.rhs = p2.toArray(new Token[p2.length()]);
                    p2.clear();
                }
            n = n.next;
        }
    }

    private Path<Token> expandTokenGroups(Token x) {
        if(x == null)
            return (null);
        Path<Token> r = new Path<Token>();
        if((x instanceof Terminal) || (x instanceof NonTerminal)) {
            if(!x.equals(Terminal.Epsilon))
                r.append(x);
            return (r);
        }
        Token.Group g;
        if(x instanceof Token.Group) {
            g = (Token.Group)x;
            if((g.members == null) || (g.members.length == 0))
                return (null);
            for(int i = 0; i < g.members.length; i++)
                r.append(expandTokenGroups(g.members[i]));
            return (r);
        }
        if(x instanceof Token.Alternation) {
            Token.Alternation a = (Token.Alternation)x;
            if((a.options == null) || (a.options.length == 0))
                return (null);
            r.append(x);
            int i, j, l;
            Path<Token> s = new Path<Token>();
            for(i = 0; i < a.options.length; i++)
                if((a.options[i] != null) && (a.options[i] instanceof Token.Group)) {
                    g = (Token.Group)(a.options[i]);
                    if((g.members == null) || (g.members.length == 0))
                        a.options[i] = null;
                    else if(g.members.length == 1)
                        a.options[i] = g.members[0];
                    else {
                        for(j = 0; j < g.members.length; j++)
                            s.append(expandTokenGroups(g.members[j]));
                        if(s.isEmpty())
                            a.options[i] = null;
                        else if((l = s.length()) == 1)
                            a.options[i] = s.pop();
                        else {
                            g.members = s.toArray(new Token[l]);
                            s.clear();
                        }
                    }
                }
        } else if(x instanceof Token.Optional) {
            Token.Optional a = (Token.Optional)x;
            if((a.option == null) || (a.option.equals(Terminal.Epsilon)))
                return (null);
            r.append(x);
            int j, l;
            Path<Token> s = new Path<Token>();
            if((a.option instanceof Token.Group)) {
                g = (Token.Group)(a.option);
                if((g.members == null) || (g.members.length == 0))
                    a.option = null;
                else if(g.members.length == 1)
                    a.option = g.members[0];
                else {
                    for(j = 0; j < g.members.length; j++)
                        s.append(expandTokenGroups(g.members[j]));
                    if(s.isEmpty())
                        a.option = null;
                    else if((l = s.length()) == 1)
                        a.option = s.pop();
                    else {
                        g.members = s.toArray(new Token[l]);
                        s.clear();
                    }
                }
            }
        } else if(x instanceof Token.Plussed) {
            Token.Plussed a = (Token.Plussed)x;
            if((a.x == null) || (a.x.equals(Terminal.Epsilon)))
                return (null);
            r.append(x);
            int j, l;
            Path<Token> s = new Path<Token>();
            if((a.x instanceof Token.Group)) {
                g = (Token.Group)(a.x);
                if((g.members == null) || (g.members.length == 0))
                    a.x = null;
                else if(g.members.length == 1)
                    a.x = g.members[0];
                else {
                    for(j = 0; j < g.members.length; j++)
                        s.append(expandTokenGroups(g.members[j]));
                    if(s.isEmpty())
                        a.x = null;
                    else if((l = s.length()) == 1)
                        a.x = s.pop();
                    else {
                        g.members = s.toArray(new Token[l]);
                        s.clear();
                    }
                }
            }
        } else if(x instanceof Token.Starred) {
            Token.Starred a = (Token.Starred)x;
            if((a.x == null) || (a.x.equals(Terminal.Epsilon)))
                return (null);
            r.append(x);
            int j, l;
            Path<Token> s = new Path<Token>();
            if((a.x instanceof Token.Group)) {
                g = (Token.Group)(a.x);
                if((g.members == null) || (g.members.length == 0))
                    a.x = null;
                else if(g.members.length == 1)
                    a.x = g.members[0];
                else {
                    for(j = 0; j < g.members.length; j++)
                        s.append(expandTokenGroups(g.members[j]));
                    if(s.isEmpty())
                        a.x = null;
                    else if((l = s.length()) == 1)
                        a.x = s.pop();
                    else {
                        g.members = s.toArray(new Token[l]);
                        s.clear();
                    }
                }
            }
        } else
            throw (new RuntimeException("Unknown token-type: " + x));
        return (r);
    }

    private Path<Token> expandToken(Token x) {
        if(x == null)
            return (null);
        Path<Token> r = new Path<Token>();
        if((x instanceof Terminal) || (x instanceof NonTerminal)) {
            if(!x.equals(Terminal.Epsilon))
                r.append(x);
            return (r);
        }
        if(x instanceof Token.Group) {
            Token.Group g = (Token.Group)x;
            if((g.members == null) || (g.members.length == 0))
                return (null);
            for(int i = 0; i < g.members.length; i++)
                r.append(expandToken(g.members[i]));
            return (r);
        }
        TokenReplacement.Factory trf = TokenReplacement.FACTORY.get();
        TokenReplacement nq = R.addOrGetMatch(x, trf);
        if(nq != trf.last) {
            r.append(nq.y);
            return (r);
        }
        nq.y = new NonTerminal("@" + (n_c++) + ":" + x.toString() + "@");  //Edit: Wiedererkennung von transformierten Ausdrücken
        r.append(nq.y);
        Rule p;
        Path<Token> s;
        if(x instanceof Token.Alternation) {
            Token.Alternation a = (Token.Alternation)x;
            if((a.options == null) || (a.options.length == 0))
                return (r);
            for(int i = 0; i < a.options.length; i++)
                if((a.options[i] == null) || (a.options[i].equals(
                        Terminal.Epsilon)))
                    nq.y.setNullable(true);
                else {
                    s = expandToken(a.options[i]);
                    if((s == null) || s.isEmpty())
                        nq.y.setNullable(true);
                    else {
                        p = new Rule(nq.y, s.toArray(new Token[s.length()]));
                        p.id = r_c++;
                        P.append(p);
                    }
                }
        } else if(x instanceof Token.Optional) {
            nq.y.setNullable(true);
            s = expandToken(((Token.Optional)x).option);
            if((s == null) || s.isEmpty()) {
                nq.y = null;
                n_c--;
                return (null);
            }
            p = new Rule(nq.y, s.toArray(new Token[s.length()]));
            p.id = r_c++;
            P.append(p);
        } else if(x instanceof Token.Plussed) {
            s = expandToken(((Token.Plussed)x).x);
            if((s == null) || s.isEmpty()) {
                nq.y = null;
                n_c--;
                return (null);
            }
            p = new Rule(nq.y, s.toArray(new Token[s.length()]));
            p.id = r_c++;
            P.append(p);
            s.prepend(nq.y);
            p = new Rule(nq.y, s.toArray(new Token[s.length()]));
            p.id = r_c++;
            P.append(p);
        } else if(x instanceof Token.Starred) {
            nq.y.setNullable(true);
            s = expandToken(((Token.Starred)x).x);
            if((s == null) || s.isEmpty()) {
                nq.y = null;
                n_c--;
                return (null);
            }
            s.prepend(nq.y);
            p = new Rule(nq.y, s.toArray(new Token[s.length()]));
            p.id = r_c++;
            P.append(p);
        } else
            throw (new RuntimeException("Unknown token-type: " + x));
        N.add(nq.y);
        return (r);
    }

    private void parseGrammar() throws IOException {
        while(cc != -1)
            parseStatement();
    }

    private void parseStatement() throws IOException {
        if((cc == ';') || (cc == '.')) {
            readNextNWS();
            return;
        }
        String s = parseIdentifier();
        String jt = null;
        if((cc == '=') || (cc == ':') || (cc == '-')) {
            if(cc == ':') {
                readNext();
                if(cc == ':')
                    readNext();
                if(cc == '=')
                    readNextNWS();
                else
                    skipWS();
            } else if(cc == '-') {
                readNext();
                if(cc != '>')
                    throw (new IOException("expected \">\" or \"{\", got " + (cc == -1 ? "to end of input" : "\"" + Terminal.String.
                            escapeCodePoint(cc) + "\"") + " while parsing a rule-statement in line " + r.
                            getLineNumber() + "\r\n" + r.
                            formatCurrentLineAndClose()));
                readNextNWS();
            } else
                readNextNWS();
            NonTerminal t = N.addOrGetMatch(s, NonTerminal.FACTORY.get());
            Token[] toa = parseTokenE();
            if(cc == '{')
                jt = parseJavaBlock();
            if((cc != ';') && (cc != '.'))
                throw (new IOException("expected \";\" or \".\", got " + (cc == -1 ? "to end of input" : "\"" + Terminal.String.
                        escapeCodePoint(cc) + "\"") + " while parsing a rule-statement in line " + r.
                        getLineNumber() + "\r\n" + r.formatCurrentLineAndClose()));
            if((toa != null) && (toa.length == 1)
               && (toa[0] != null) && (toa[0] instanceof Token.Alternation)) {
                Token.Alternation tob = (Token.Alternation)(toa[0]);
                if(tob.options != null)
                    for(int i = 0; i < tob.options.length; i++) {
                        cr = new Rule(t,
                                      ((tob.options[i] != null)
                                       && (tob.options[i] instanceof Token.Group))
                                      ? ((Token.Group)(tob.options[i])).members
                                      : new Token[] {tob.options[i]});
                        cr.setID(r_c);
                        r_c++;
                        if(cr.rhs == null)
                            t.setNullingRule(cr);
                        cr.setJavaCode(jt);
                        P.append(cr);
                        if(fr == null)
                            fr = cr;
                    }

            } else {
                cr = new Rule(t, toa);
                cr.setID(r_c);
                r_c++;
                if(cr.rhs == null)
                    t.setNullingRule(cr);
                P.append(cr);
                if(fr == null)
                    fr = cr;
            }
            readNextNWS();
        } else
            if(s.equalsIgnoreCase("start")) {
                s = parseIdentifier();
                if(S == null)
                    S = N.addOrGetMatch(s, NonTerminal.FACTORY.get());
                else
                    System.out.println(
                            "Ignoring repeated start symbol statement");
                if((cc != ';') && (cc != '.'))
                    throw (new IOException("expected \";\" or \".\", got " + (cc == -1 ? "to end of input" : "\"" + Terminal.String.
                            escapeCodePoint(cc) + "\"") + " while parsing a \"start non-terminal\"-statement in line " + r.
                            getLineNumber() + "\r\n" + r.
                            formatCurrentLineAndClose()));
                readNextNWS();
            } else if(s.equalsIgnoreCase("global")) {
                globalJavaCode = parseJavaBlock();
                if((cc != ';') && (cc != '.'))
                    throw (new IOException("expected \";\" or \".\", got " + (cc == -1 ? "to end of input" : "\"" + Terminal.String.
                            escapeCodePoint(cc) + "\"") + " while parsing a \"global code\"-statement in line " + r.
                            getLineNumber() + "\r\n" + r.
                            formatCurrentLineAndClose()));
                readNextNWS();
            } else if(s.equalsIgnoreCase("lexer")) {
                lexer = parseQualifiedIdentifier();
                if((cc != ';') && (cc != '.'))
                    throw (new IOException("expected \";\" or \".\", got " + (cc == -1 ? "to end of input" : "\"" + Terminal.String.
                            escapeCodePoint(cc) + "\"") + " while parsing a \"lexer\"-statement in line " + r.
                            getLineNumber() + "\r\n" + r.
                            formatCurrentLineAndClose()));
                readNextNWS();
            } else
                throw (new IOException("expected one of \";\",\".\",\":\" or \"=\", got " + (cc == -1 ? "to end of input" : "\"" + Terminal.String.
                        escapeCodePoint(cc) + "\"") + " while parsing a statement in line " + r.
                        getLineNumber() + "\r\n" + r.formatCurrentLineAndClose()));
        //readNextNWS();
    }

    private Token[] parseTokenE() throws IOException {
        Path<Path<Token>> pa = new Path<Path<Token>>();
        Path<Token> p = null;
        Token[] ta = {null};
        boolean lastcomma = false;
        while(true) {
            p = new Path<Token>();
            while((cc != -1) && (cc != '|') && (cc != '{') && (cc != ')') && (cc != ';') && (cc != '.')) {
                p.append(parseTokenF());
                if((lastcomma = (cc == ',')))
                    readNextNWS();
            }
            if(lastcomma)
                System.out.println("pll.GrammarReader: warning: unnecessary comma in line " + r.
                        getLineNumber() + "\r\n" + r.formatCurrentLineAndClose());
            pa.append(p);
            if(cc != '|')
                break;
            readNextNWS();
        }
        switch(pa.length()) {
            case 0:
                return (null);
            case 1:
                if((pa.head == null) || (pa.head.x == null))
                    return (null);
                ta = new Token[pa.head.x.length()];
                return pa.head.x.toArray(ta);
            default:
                Token.Alternation a = new Token.Alternation();
                a.options = new Token[pa.length()];
                int i = 0,
                 l = 0;
                Path.Node<Path<Token>> n = pa.head;
                while(n != null) {
                    if((n.x == null) || ((l = n.x.length()) == 0))
                        a.options[i] = null;
                    else if(l == 1)
                        a.options[i] = n.x.head.x;
                    else
                        a.options[i] = new Token.Group(n.x.toArray(new Token[n.x.
                                length()]));
                    i++;
                    n = n.next;
                }
                ta[0] = a;
                return ta;
        }
    }

    private Token parseTokenF() throws IOException {
        Token t, x;
        switch(cc) {
            case '\'':
                t = Terminal.Character.valueOf(parseEscapedString());
                break;
            case '\"':
                t = new Terminal.String(parseEscapedString());
                break;
            case '<':
                t = new Terminal.Tagged(parseEscapedString());
                break;
            case '[':
                t = parseCharacterClass();
                break;
            case '(':
                readNextNWS();
                Token[] ta = parseTokenE();
                if((ta == null) || (ta.length == 0))
                    t = null;
                else if(ta.length == 1)
                    t = ta[0];
                else
                    t = new Token.Group(ta);
                if(cc != ')')
                    throw (new IOException("expected closing bracket, got " + (cc == -1 ? "to end of input" : "\"" + Terminal.String.
                            escapeCodePoint(cc) + "\"") + " while parsing a group of tokens in line " + r.
                            getLineNumber() + "\r\n" + r.
                            formatCurrentLineAndClose()));
                readNextNWS();
                break;
            case '#':
                t = Terminal.Epsilon;
                readNextNWS();
                break;
            case '$':
                t = Terminal.End;
                readNextNWS();
                break;
            default:
                t = N.addOrGetMatch(parseIdentifier(), NonTerminal.FACTORY.get());
                break;
        }
        switch(cc) {
            case '?':
                t = new Token.Optional(t);
                readNextNWS();
                break;
            case '*':
                t = new Token.Starred(t);
                readNextNWS();
                break;
            case '+':
                t = new Token.Plussed(t);
                readNextNWS();
                break;
        }
        return (t);
    }

    private String parseIdentifier() throws IOException {
        if(!Character.isJavaIdentifierStart(cc))
            throw (new IOException("expected java-identifier, got " + (cc == -1 ? "to end of input" : "\"" + Terminal.String.
                    escapeCodePoint(cc) + "\"") + " while parsing an identifier in line " + r.
                    getLineNumber() + "\r\n" + r.formatCurrentLineAndClose()));
        StringBuilder sb = new StringBuilder().appendCodePoint(cc);
        readNext();
        while(Character.isJavaIdentifierPart(cc)) {
            if(!Character.isIdentifierIgnorable(cc))
                sb.appendCodePoint(cc);
            readNext();
        }
        skipWS();
        return (sb.toString());
    }

    private String parseQualifiedIdentifier() throws IOException {
        if(!Character.isJavaIdentifierStart(cc))
            throw (new IOException("expected java-identifier, got " + (cc == -1 ? "to end of input" : "\"" + Terminal.String.
                    escapeCodePoint(cc) + "\"") + " while parsing an identifier in line " + r.
                    getLineNumber() + "\r\n" + r.formatCurrentLineAndClose()));
        StringBuilder sb = new StringBuilder().appendCodePoint(cc);
        readNext();
        while(cc == '.' || Character.isJavaIdentifierPart(cc)) {
            if(!Character.isIdentifierIgnorable(cc))
                sb.appendCodePoint(cc);
            readNext();
        }
        skipWS();
        return (sb.toString());
    }

    private String parseEscapedString() throws IOException {
        StringBuilder sb = new StringBuilder();
        int ec, t;
        switch(cc) {
            case '\'':
            case '\"':
                ec = cc;
                break;
            case '<':
                ec = '>';
                break;
            case '[':
                ec = ']';
                break;
            case '{':
                ec = '}';
                break;
            default:
                throw (new IOException("expected terminal-quotes, got " + (cc == -1 ? "to end of input" : "\"" + Terminal.String.
                        escapeCodePoint(cc) + "\"") + " while parsing an escaped string in line " + r.
                        getLineNumber() + "\r\n" + r.formatCurrentLineAndClose()));
        }
        readNext();
        while(true) {
            if(cc == -1)
                throw (new IOException("expected terminal-quotes, got to end of input while parsing an escaped string in line " + r.
                        getLineNumber() + "\r\n" + r.formatCurrentLineAndClose()));
            if(cc == ec) {
                readNextNWS();
                break;
            }
            if(cc == '\\') {
                readNext();
                if(cc == -1)
                    throw (new IOException("expected terminal-quotes, got to end of input while parsing an escaped string in line " + r.
                            getLineNumber() + "\r\n" + r.
                            formatCurrentLineAndClose()));
                t = cc;
                if((cc >= '0') && (cc < '7'))
                    sb.appendCodePoint((int)parseRNumber(1, 3, 8));
                else {
                    readNext();
                    switch(t) {
                        case 'a':
                            sb.append('\b');
                            break;
                        case 'b':
                            sb.append('\b');
                            break;
                        case 't':
                            sb.append('\t');
                            break;
                        case 'n':
                            sb.append('\n');
                            break;
                        case 'f':
                            sb.append('\f');
                            break;
                        case 'r':
                            sb.append('\r');
                            break;
                        case '\"':
                            sb.append('\"');
                            break;
                        case '\'':
                            sb.append('\'');
                            break;
                        case '\\':
                            sb.append('\\');
                            break;
                        case '<':
                            sb.append('<');
                            break;
                        case '>':
                            sb.append('>');
                            break;
                        case 'U':
                            sb.appendCodePoint((int)parseRNumber(6, 6, 16));
                            break;
                        case 'u':
                            sb.appendCodePoint((int)parseRNumber(4, 4, 16));
                            break;
                        case 'x':
                            sb.appendCodePoint((int)parseRNumber(2, 2, 16));
                            break;
                        default:
                            sb.append('\\').appendCodePoint(t);
                        //throw(new IOException("expected escape-code, got \\"+(cc==-1?"to end of input":"\""+Terminal.String.escapeCodePoint(cc)+"\"")+" while parsing an escaped string in line "+r.getLineNumber()+"\r\n"+r.formatCurrentLineAndClose()));
                    }
                }
            } else {
                sb.appendCodePoint(cc);
                readNext();
            }
        }
        return (sb.toString());
    }

    private Terminal.CharacterClass parseCharacterClass() throws IOException {
        if(cc != '[')
            throw (new IOException("expected \"[\", got " + (cc == -1 ? "to end of input" : "\"" + Terminal.String.
                    escapeCodePoint(cc) + "\"") + " while parsing a character class in line " + r.
                    getLineNumber() + "\r\n" + r.formatCurrentLineAndClose()));
        Terminal.CharacterClass t = null;
        while(true) {
            if(cc == -1)
                throw (new IOException("expected character, got " + (cc == -1 ? "to end of input" : "\"" + Terminal.String.
                        escapeCodePoint(cc) + "\"") + " while parsing a disjunction of character classes in line " + r.
                        getLineNumber() + "\r\n" + r.formatCurrentLineAndClose()));
            if(t == null)
                t = parseCharacterClassS();
            else
                t = t.join(parseCharacterClassS());
            if(cc != '[')
                break;
        }
        skipWS();
        if(t == null)
            t = Terminal.CharacterClass.Empty;
        return (t);
    }

    private Terminal.CharacterClass parseCharacterClassS() throws IOException {
        if(cc != '[')
            throw new IOException("expected \"[\", got " + (cc == -1 ? "to end of input" : "\"" + Terminal.String.
                    escapeCodePoint(cc) + "\"") + " while parsing a character class in line " + r.
                    getLineNumber() + "\r\n" + r.formatCurrentLineAndClose());
        Terminal.CharacterClass t = null;
        while(true) {
            if(cc == -1)
                throw new IOException("expected character, got " + (cc == -1 ? "to end of input" : "\"" + Terminal.String.
                        escapeCodePoint(cc) + "\"") + " while parsing a character class in line " + r.
                        getLineNumber() + "\r\n" + r.formatCurrentLineAndClose());
            if(t == null)
                t = parseCharacterClassT();
            else
                t = t.intersect(parseCharacterClassT());
            if(cc != '&')
                break;
            readNext();
//            if(cc!='&')
//                throw(new IOException("expected \"&\", got "+(cc==-1?"to end of input":"\""+Terminal.String.escapeCodePoint(cc)+"\"")+" while parsing a conjunction of character classes in line "+r.getLineNumber()+"\r\n"+r.formatCurrentLineAndClose()));
            if(cc == '&')
                readNext();
            if(cc != '[')
                throw new IOException("expected \"[\", got " + (cc == -1 ? "to end of input" : "\"" + Terminal.String.
                        escapeCodePoint(cc) + "\"") + " while parsing a conjunction of character classes in line " + r.
                        getLineNumber() + "\r\n" + r.formatCurrentLineAndClose());
        }
        if(t == null)
            t = Terminal.CharacterClass.Empty;
        return t;
    }

    private Terminal.CharacterClass parseCharacterClassT() throws IOException {
        if(cc != '[')
            throw new IOException("expected \"[\", got " + (cc == -1 ? "to end of input" : "\"" + Terminal.String.
                    escapeCodePoint(cc) + "\"") + " while parsing a character class in line " + r.
                    getLineNumber() + "\r\n" + r.formatCurrentLineAndClose());
        readNext();
        boolean negate = (cc == '^');
        if(negate)
            readNext();
        Terminal.CharacterClass t = null;
        while(true) {
            if(cc == -1)
                throw new IOException("expected character or \"]\", got " + (cc == -1 ? "to end of input" : "\"" + Terminal.String.
                        escapeCodePoint(cc) + "\"") + " while parsing a disjunction of character classes in line " + r.
                        getLineNumber() + "\r\n" + r.formatCurrentLineAndClose());
            if(cc == ']')
                break;
            if(t == null)
                t = parseCharacterClassE();
            else
                t = t.join(parseCharacterClassE());
        }
        readNext();
        if(t == null)
            t = Terminal.CharacterClass.Empty;
        if(negate)
            t = t.complement();
        return t;
    }

    private Terminal.CharacterClass parseCharacterClassE() throws IOException {
        Terminal.CharacterClass t = null;
        while(true) {
            if(cc == -1)
                throw (new IOException("expected character, got " + (cc == -1 ? "to end of input" : "\"" + Terminal.String.
                        escapeCodePoint(cc) + "\"") + " while parsing a character class in line " + r.
                        getLineNumber() + "\r\n" + r.formatCurrentLineAndClose()));
            if(t == null)
                t = parseCharacterClassF();
            else
                t = t.intersect(parseCharacterClassF());
            if(cc != '&')
                break;
            readNext();
//            if(cc!='&')
//                throw(new IOException("expected \"&\", got "+(cc==-1?"to end of input":"\""+Terminal.String.escapeCodePoint(cc)+"\"")+" while parsing a conjunction of character classes in line "+r.getLineNumber()+"\r\n"+r.formatCurrentLineAndClose()));
            if(cc == '&')
                readNext();
        }
        if(t == null)
            t = Terminal.CharacterClass.Empty;
        return (t);
    }

    private Terminal.CharacterClass parseCharacterClassF() throws IOException {
        Terminal.CharacterClass t = null;
        while(true) {
            if(cc == -1)
                throw (new IOException("expected character, got " + (cc == -1 ? "to end of input" : "\"" + Terminal.String.
                        escapeCodePoint(cc) + "\"") + " while parsing a character class in line " + r.
                        getLineNumber() + "\r\n" + r.formatCurrentLineAndClose()));
            if(t == null)
                t = parseCharacterClassG();
            else
                t = t.without(parseCharacterClassG());
            if(t == null)
                t = Terminal.CharacterClass.Empty;
            if(cc != '^')
                break;
            readNext();
        }
        return (t);
    }

    private Terminal.CharacterClass parseCharacterClassG() throws IOException {
        if(cc == -1)
            throw (new IOException("expected character or \"[\", got " + (cc == -1 ? "to end of input" : "\"" + Terminal.String.
                    escapeCodePoint(cc) + "\"") + " while parsing a character class in line " + r.
                    getLineNumber() + "\r\n" + r.formatCurrentLineAndClose()));
        if(cc == '[')
            return (parseCharacterClassT());
        int d = 0, u = 0, t;
        if(cc == '\\') {
            readNext();
            if(cc == -1)
                throw (new IOException("expected escape character, got " + (cc == -1 ? "to end of input" : "\"" + Terminal.String.
                        escapeCodePoint(cc) + "\"") + " while parsing a character class in line " + r.
                        getLineNumber() + "\r\n" + r.formatCurrentLineAndClose()));
            if((cc >= '0') && (cc < '7'))
                d = (int)parseRNumber(1, 3, 8);
            else {
                t = cc;
                readNext();
                boolean negate = false;
                switch(t) {
                    case 'd':
                        return (Terminal.CharacterClass.forName("Digit"));
                    case 'D':
                        return (Terminal.CharacterClass.forName("Digit").
                                complement());
                    case 's':
                        return (Terminal.CharacterClass.forName("Space"));
                    case 'S':
                        return (Terminal.CharacterClass.forName("Space").
                                complement());
                    case 'w':
                        return (Terminal.CharacterClass.forName("Word"));
                    case 'W':
                        return (Terminal.CharacterClass.forName("Word").
                                complement());
                    case 'P':
                        negate = true;
                    case 'p':
                        if(cc != '{')
                            throw (new IOException("expected \'{\', got " + (cc == -1 ? "to end of input" : "\"" + Terminal.String.
                                    escapeCodePoint(cc) + "\"") + " while parsing a POSIX-class in line " + r.
                                    getLineNumber() + "\r\n" + r.
                                    formatCurrentLineAndClose()));
                        Terminal.CharacterClass a = Terminal.CharacterClass.
                                forName(parseEscapedString());
                        return (negate ? a.complement() : a);
                    case 'a':
                        d = '\u0007';
                        break;
                    case 'b':
                        d = '\b';
                        break;
                    case 'c':
                        d = cc ^ 0x40;
                        readNext();
                        break;
                    case 'e':
                        d = '\u001B';
                        break;
                    case 't':
                        d = '\t';
                        break;
                    case 'n':
                        d = '\n';
                        break;
                    case 'f':
                        d = '\f';
                        break;
                    case 'r':
                        d = '\r';
                        break;
                    case '\\':
                        d = '\\';
                        break;
                    case '[':
                        d = '[';
                        break;
                    case ']':
                        d = ']';
                        break;
                    case '-':
                        d = '-';
                        break;
                    case '.':
                        d = '.';
                        break;
                    case 'U':
                        d = (int)parseRNumber(6, 6, 16);
                        break;
                    case 'u':
                        d = (int)parseRNumber(4, 4, 16);
                        break;
                    case 'x':
                        d = (int)parseRNumber(2, 2, 16);
                        break;
                    default:
                        d = t;
                        break;
                }
            }
        } else if(cc == '.') {
            readNext();
            return (new Terminal.CharacterClass.Range(0, 65535));
        } else {
            d = cc;
            readNext();
        }
        if(cc != '-')
            return (new Terminal.CharacterClass.Single(d));
        readNext();
        if(cc == -1)
            throw (new IOException("expected character, got " + (cc == -1 ? "to end of input" : "\"" + Terminal.String.
                    escapeCodePoint(cc) + "\"") + " while parsing a character class in line " + r.
                    getLineNumber() + "\r\n" + r.formatCurrentLineAndClose()));
        if(cc == '\\') {
            if(cc == -1)
                throw (new IOException("expected escape character, got " + (cc == -1 ? "to end of input" : "\"" + Terminal.String.
                        escapeCodePoint(cc) + "\"") + " while parsing a character class in line " + r.
                        getLineNumber() + "\r\n" + r.formatCurrentLineAndClose()));

            if((cc >= '0') && (cc < '7'))
                d = (char)parseRNumber(1, 3, 8);
            else {
                t = cc;
                readNext();
                switch(t) {
                    case 'a':
                        u = '\u0007';
                        break;
                    case 'b':
                        u = '\b';
                        break;
                    case 'e':
                        u = '\u001B';
                        break;
                    case 't':
                        u = '\t';
                        break;
                    case 'n':
                        u = '\n';
                        break;
                    case 'f':
                        u = '\f';
                        break;
                    case 'r':
                        u = '\r';
                        break;
                    case '\\':
                        u = '\\';
                        break;
                    case '[':
                        u = '[';
                        break;
                    case ']':
                        u = ']';
                        break;
                    case '-':
                        u = '-';
                        break;
                    case '.':
                        u = '.';
                        break;
                    case 'U':
                        u = (int)parseRNumber(6, 6, 16);
                        break;
                    case 'u':
                        u = (int)parseRNumber(4, 4, 16);
                        break;
                    case 'x':
                        u = (int)parseRNumber(2, 2, 16);
                        break;
                    default:
                        d = t;
                        break;
                }
            }
        } else {
            u = cc;
            readNext();
        }
        if(u < d) {
            t = u;
            u = d;
            d = t;
        }
        if(u == d)
            return (new Terminal.CharacterClass.Single(u));
        else
            return (new Terminal.CharacterClass.Range(d, u));
    }

    private String parseJavaBlock() throws IOException {
        if(cc != '{')
            throw (new IOException("expected \"{\", got " + (cc == -1 ? "to end of input" : "\"" + Terminal.String.
                    escapeCodePoint(cc) + "\"") + " while parsing a java-block in line " + r.
                    getLineNumber() + "\r\n" + r.formatCurrentLineAndClose()));
        StringBuilder s = new StringBuilder();
        long l = 1;
        readNext();
        while(l > 0) {
            if(cc == -1)
                throw (new IOException("expected \"}\", got to end of input while parsing a java-block in line " + r.
                        getLineNumber() + "\r\n" + r.formatCurrentLineAndClose()));
no_read:    {
                switch(cc) {
                    case '}':
                        l--;
                        if(l > 0)
                            s.append('}');
                        break;
                    case '{':
                        l++;
                        s.append('{');
                        break;
                    case '\"':
                        s.append('\"');
                        readNext();
                        while(cc != '\"') {
                            if(cc == -1)
                                throw (new IOException("expected \"\\\"\", got to end of input while parsing a string-literal in a java-block in line " + r.
                                        getLineNumber() + "\r\n" + r.
                                        formatCurrentLineAndClose()));
                            s.appendCodePoint(cc);
                            if(cc == '\\') {
                                readNext();
                                s.appendCodePoint(cc);
                            }
                            readNext();
                        }
                        s.append('\"');
                        break;
                    case '\'':
                        s.append('\'');
                        readNext();
                        while(cc != '\'') {
                            if(cc == -1)
                                throw (new IOException("expected \"\\\"\", got to end of input while parsing a string-literal in a java-block in line " + r.
                                        getLineNumber() + "\r\n" + r.
                                        formatCurrentLineAndClose()));
                            s.appendCodePoint(cc);
                            if(cc == '\\') {
                                readNext();
                                s.appendCodePoint(cc);
                            }
                            readNext();
                        }
                        s.append('\'');
                        break;
                    case '/':
                        readNext();
                        if(cc == '/') {
                            do
                                readNext();
                            while((cc != -1) && (cc != '\n'));
                            if(cc != -1)
                                readNext();
                        } else if(cc == '*') {
                            readNext();
                            while(true) {
                                if(cc == -1)
                                    throw (new IOException("unexpected end of input while parsing a traditional comment in line " + r.
                                            getLineNumber() + "\r\n" + r.
                                            formatCurrentLineAndClose()));
                                if(cc == '*') {
                                    readNext();
                                    if(cc == -1)
                                        throw (new IOException("unexpected end of input while parsing a traditional comment in line " + r.
                                                getLineNumber() + "\r\n" + r.
                                                formatCurrentLineAndClose()));
                                    if(cc == '/') {
                                        readNext();
                                        break;
                                    }
                                } else
                                    readNext();
                            }
                        } else
                            s.append('/');
                        break no_read;
                    default:
                        s.appendCodePoint(cc);
                }
                readNext();
            }
        }
        return (s.toString());
    }

    private void readGrammar() throws IOException {
        if(cc != '(')
            throw (new IOException("expected \"(\", got " + (cc == -1 ? "to end of input" : "\"" + Terminal.String.
                    escapeCodePoint(cc) + "\"") + " while reading a compiled grammar in line " + r.
                    getLineNumber() + "\r\n" + r.formatCurrentLineAndClose()));
        readNextNWS();
        if(cc != ',')
            readNonTerminalList();
        if(cc != ',')
            throw (new IOException("expected \",\", got " + (cc == -1 ? "to end of input" : "\"" + Terminal.String.
                    escapeCodePoint(cc) + "\"") + " while reading a compiled grammar in line " + r.
                    getLineNumber() + "\r\n" + r.formatCurrentLineAndClose()));
        readNextNWS();
        if(cc != ',')
            readRuleList();
        if(cc != ',')
            throw (new IOException("expected \",\", got " + (cc == -1 ? "to end of input" : "\"" + Terminal.String.
                    escapeCodePoint(cc) + "\"") + " while reading a compiled grammar in line " + r.
                    getLineNumber() + "\r\n" + r.formatCurrentLineAndClose()));
        readNextNWS();
        S = N.getMatch(readNonTerminalName(STOP_CHARS_2));
        if(S == null)
            throw (new IOException("Illegal start symbol"));
        if(cc == ',') {
            readNextNWS();
            lexer = parseQualifiedIdentifier();
        }
        if(cc != ')')
            throw (new IOException("expected \")\", got " + (cc == -1 ? "to end of input" : "\"" + Terminal.String.
                    escapeCodePoint(cc) + "\"") + " while reading a compiled grammar in line " + r.
                    getLineNumber() + "\r\n" + r.formatCurrentLineAndClose()));
        readNextNWS();
    }

    private void readNonTerminalList() throws IOException {
        if(cc != '{') {
            N = new Final<NonTerminal>(null, null, false);
            return;
        }
        readNextNWS();
        NP.append(readNonTerminal());
        while(cc == ',') {
            readNextNWS();
            NP.append(readNonTerminal());
        }
        if(cc != '}')
            throw (new IOException("expected \"}\", got " + (cc == -1 ? "to end of input" : "\"" + Terminal.String.
                    escapeCodePoint(cc) + "\"") + " while reading a nonterminal-list for a compiled grammar in line " + r.
                    getLineNumber() + "\r\n" + r.formatCurrentLineAndClose()));
        readNextNWS();
        N = new Final<NonTerminal>(NP.toArray(new NonTerminal[NP.length()]));
    }

    private NonTerminal readNonTerminal() throws IOException {
        if(cc != '(')
            throw (new IOException("expected \"(\", got " + (cc == -1 ? "to end of input" : "\"" + Terminal.String.
                    escapeCodePoint(cc) + "\"") + " while reading a nonterminal(1) for a compiled grammar in line " + r.
                    getLineNumber() + "\r\n" + r.formatCurrentLineAndClose()));
        readNextNWS();
        NonTerminal t = new NonTerminal(readNonTerminalName(STOP_CHARS_2));
        skipWS();
        if(cc == ',') {
            readNextNWS();
            if(cc != '(') {
                if((cc == ',') || (cc == ')'))
                    t.setNullable(true);
                else {
                    Rule r = new Rule(null, null, (int)parseRNumber(1, 10, 10));
                    Rule s = PT.get(r);
                    if(s != null)
                        r = s;
                    t.setNullingRule(r);
                }
                if(cc == ',') {
                    readNextNWS();
                    t.firstSet = readFirstSet();
                }
            } else
                t.firstSet = readFirstSet();
        }
        if(cc != ')')
            throw (new IOException("expected \")\", got " + (cc == -1 ? "to end of input" : "\"" + Terminal.String.
                    escapeCodePoint(cc) + "\"") + " while reading a nonterminal for a compiled grammar in line " + r.
                    getLineNumber() + "\r\n" + r.formatCurrentLineAndClose()));
        readNextNWS();
        return (t);
    }

    private TerminalToRuleMap readFirstSet() throws IOException {
        TerminalToRuleMap tm = new TerminalToRuleMap();
        if(cc != '(')
            return (tm);
        readNextNWS();
        tm.s = readTSet(false);
        if(cc != ',')
            throw (new IOException("expected \",\", got " + (cc == -1 ? "to end of input" : "\"" + Terminal.String.
                    escapeCodePoint(cc) + "\"") + " while reading a first-set for a compiled grammar in line " + r.
                    getLineNumber() + "\r\n" + r.formatCurrentLineAndClose()));
        readNextNWS();
        tm.sc = readTSet(true);
        if(cc != ')')
            throw (new IOException("expected \")\", got " + (cc == -1 ? "to end of input" : "\"" + Terminal.String.
                    escapeCodePoint(cc) + "\"") + " while reading a first-set for a compiled grammar in line " + r.
                    getLineNumber() + "\r\n" + r.formatCurrentLineAndClose()));
        readNextNWS();
        return (tm);
    }

    private Traversable.Set<TerminalToRuleMap.TEntry> readTSet(boolean charClass)
            throws IOException {
        if(cc != '{')
            return (null);
        readNextNWS();
        if(cc == '}') {
            readNextNWS();
            return (null);
        }
        Path<TerminalToRuleMap.TEntry> pte = new Path<TerminalToRuleMap.TEntry>();
        while(true) {
            pte.append(readTEntry(charClass));
            if(cc == '}')
                break;
            else if(cc == ',')
                readNextNWS();
            else
                throw (new IOException("expected \",\" or \")\", got " + (cc == -1 ? "to end of input" : "\"" + Terminal.String.
                        escapeCodePoint(cc) + "\"") + " while reading a TEntry-set for a compiled grammar in line " + r.
                        getLineNumber() + "\r\n" + r.formatCurrentLineAndClose()));
        }
        readNextNWS();
        return (new Final<TerminalToRuleMap.TEntry>(pte.toArray(new TerminalToRuleMap.TEntry[pte.
                length()]), null, true));
    }

    private TerminalToRuleMap.TEntry readTEntry(boolean charClass) throws
            IOException {
        if(cc != '(')
            return (null);
        readNextNWS();
        TerminalToRuleMap.TEntry t = new TerminalToRuleMap.TEntry();
        if(charClass)
            t.x = readCharacterClass();
        else
            t.x = readNonCharacterClass();
        if(cc != ',')
            throw (new IOException("expected \",\", got " + (cc == -1 ? "to end of input" : "\"" + Terminal.String.
                    escapeCodePoint(cc) + "\"") + " while reading a TEntry for a compiled grammar in line " + r.
                    getLineNumber() + "\r\n" + r.formatCurrentLineAndClose()));
        readNextNWS();
        t.s = readRSet();
        if(cc != ')')
            throw (new IOException("expected \")\", got " + (cc == -1 ? "to end of input" : "\"" + Terminal.String.
                    escapeCodePoint(cc) + "\"") + " while reading a TEntry for a compiled grammar in line " + r.
                    getLineNumber() + "\r\n" + r.formatCurrentLineAndClose()));
        readNextNWS();
        return (t);
    }

    private Traversable.Set<TerminalToRuleMap.RuleEntry> readRSet() throws
            IOException {
        if(cc != '{')
            return (null);
        readNextNWS();
        if(cc == '}') {
            readNextNWS();
            return (null);
        }
        Path<TerminalToRuleMap.RuleEntry> pte = new Path<TerminalToRuleMap.RuleEntry>().
                append(readRuleEntry());
        while(cc == ',') {
            readNextNWS();
            pte.append(readRuleEntry());
        }
        if(cc != '}')
            throw (new IOException("expected \",\" or \")\", got " + (cc == -1 ? "to end of input" : "\"" + Terminal.String.
                    escapeCodePoint(cc) + "\"") + " while reading a RuleEntry-set for a compiled grammar in line " + r.
                    getLineNumber() + "\r\n" + r.formatCurrentLineAndClose()));
        readNextNWS();
        return (new Final<TerminalToRuleMap.RuleEntry>(pte.toArray(new TerminalToRuleMap.RuleEntry[pte.
                length()]), null, true));
    }

    private TerminalToRuleMap.RuleEntry readRuleEntry() throws IOException {
        if(cc != '(')
            return (null);
        readNextNWS();
        if(cc == ',')
            throw (new IOException("expected rule-id, got " + (cc == -1 ? "to end of input" : "\"" + Terminal.String.
                    escapeCodePoint(cc) + "\"") + " while reading a RuleEntry for a compiled grammar in line " + r.
                    getLineNumber() + "\r\n" + r.formatCurrentLineAndClose()));
        TerminalToRuleMap.RuleEntry t = new TerminalToRuleMap.RuleEntry(new Rule(
                null, (Token[])null), null);
        try {
            t.r.id = (int)parseRNumber(1, 10, 10);
        } catch(NumberFormatException nfe) {
            throw (new IOException("expected rule-id, got " + (cc == -1 ? "to end of input" : "\"" + Terminal.String.
                    escapeCodePoint(cc) + "\"") + " while reading a RuleEntry for a compiled grammar in line " + r.
                    getLineNumber() + "\r\n" + r.formatCurrentLineAndClose()));
        }
        Rule r2 = PT.get(t.r);
        if(r2 == null)
            PT.add(t.r);
        else
            t.r = r2;
        if(cc != ',')
            throw (new IOException("expected \",\", got " + (cc == -1 ? "to end of input" : "\"" + Terminal.String.
                    escapeCodePoint(cc) + "\"") + " while reading a RuleEntry for a compiled grammar in line " + r.
                    getLineNumber() + "\r\n" + r.formatCurrentLineAndClose()));
        readNextNWS();
        if(cc == '[') {
            readNextNWS();
            if(cc != ']') {
                int k = 1;
                IntNode head = new IntNode((int)parseRNumber(1, 10, 10));
                IntNode tail = head;
                skipWS();
                while(cc == ',') {
                    readNextNWS();
                    tail.next = new IntNode((int)parseRNumber(1, 10, 10));
                    tail = tail.next;
                    k++;
                    skipWS();
                }
                if(cc != ']')
                    throw (new IOException("expected \"]\", got " + (cc == -1 ? "to end of input" : "\"" + Terminal.String.
                            escapeCodePoint(cc) + "\"") + " while reading an int-list for a compiled grammar in line " + r.
                            getLineNumber() + "\r\n" + r.
                            formatCurrentLineAndClose()));
                t.p = new int[k];
                k = 0;
                while(head != null) {
                    t.p[k++] = head.x;
                    head = head.next;
                }
            }
            readNextNWS();
        }
        if(cc != ')')
            throw (new IOException("expected \")\", got " + (cc == -1 ? "to end of input" : "\"" + Terminal.String.
                    escapeCodePoint(cc) + "\"") + " while reading a RuleEntry for a compiled grammar in line " + r.
                    getLineNumber() + "\r\n" + r.formatCurrentLineAndClose()));
        readNextNWS();
        return (t);
    }

    private Terminal readNonCharacterClass() throws IOException {
        int h;
        StringBuilder sb;
        switch(cc) {
            case '#':
                readNextNWS();
                return (Terminal.Epsilon);
            case '$':
                readNextNWS();
                return (Terminal.End);
            case '\'':
                readNext();
                if(cc == '\\') {
                    readNext();
                    h = unEscape();
                    if(h < 0)
                        throw (new IOException("expected escape-code, got " + (cc == -1 ? "to end of input" : "\"" + Terminal.String.
                                escapeCodePoint(cc) + "\"") + " while reading a character-terminal for a compiled grammar in line " + r.
                                getLineNumber() + "\r\n" + r.
                                formatCurrentLineAndClose()));
                } else {
                    h = cc;
                    readNext();
                }
                if(cc != '\'')
                    throw (new IOException("expected \"\\\'\", got " + (cc == -1 ? "to end of input" : "\"" + Terminal.String.
                            escapeCodePoint(cc) + "\"") + " while reading a character-terminal for a compiled grammar in line " + r.
                            getLineNumber() + "\r\n" + r.
                            formatCurrentLineAndClose()));
                readNextNWS();
                return (new Terminal.Character(h));
            case '\"':
                readNext();
                sb = new StringBuilder();
                while(true) {
                    if((cc == '\"') || (cc == -1))
                        break;
                    if(cc == '\\') {
                        readNext();
                        h = unEscape();
                        if(h < 0) {
                            sb.append('\\').appendCodePoint(cc);
                            readNext();
                        } else
                            sb.appendCodePoint(h);
                    } else {
                        sb.appendCodePoint(cc);
                        readNext();
                    }
                }
                if(cc != '\"')
                    throw (new IOException("expected \"\\\"\", got " + (cc == -1 ? "to end of input" : "\"" + Terminal.String.
                            escapeCodePoint(cc) + "\"") + " while reading a string-terminal for a compiled grammar in line " + r.
                            getLineNumber() + "\r\n" + r.
                            formatCurrentLineAndClose()));
                readNextNWS();
                return (new Terminal.String(sb.toString()));
            case '<':
                readNext();
                sb = new StringBuilder();
                while(true) {
                    if((cc == '>') || (cc == -1))
                        break;
                    if(cc == '\\') {
                        readNext();
                        h = unEscape();
                        if(h < 0) {
                            sb.append('\\').appendCodePoint(cc);
                            readNext();
                        } else
                            sb.appendCodePoint(h);
                    } else {
                        sb.appendCodePoint(cc);
                        readNext();
                    }
                }
                if(cc != '>')
                    throw (new IOException("expected \">\", got " + (cc == -1 ? "to end of input" : "\"" + Terminal.String.
                            escapeCodePoint(cc) + "\"") + " while reading a tagged-terminal for a compiled grammar in line " + r.
                            getLineNumber() + "\r\n" + r.
                            formatCurrentLineAndClose()));
                readNextNWS();
                return (new Terminal.Tagged(sb.toString()));
            default:
                return (null);
        }
    }

    private Terminal readTerminal() throws IOException {
        if(cc == '[')
            return (readCharacterClass());
        else
            return (readNonCharacterClass());
    }

    private Terminal.CharacterClass readCharacterClass() throws IOException {
        if(cc != '[')
            throw (new IOException("expected \"[\", got " + (cc == -1 ? "to end of input" : "\"" + Terminal.String.
                    escapeCodePoint(cc) + "\"") + " while reading a character-class for a compiled grammar in line " + r.
                    getLineNumber() + "\r\n" + r.formatCurrentLineAndClose()));
        readNext();
        if(cc == ']') {
            readNextNWS();
            return (Terminal.CharacterClass.Empty);
        }
        Path<Terminal.CharacterClass> t = new Path<Terminal.CharacterClass>();
        while((cc != ']') && (cc != -1))
            t.append(readCharacterRangeOrSingle());
        if(cc != ']')
            throw (new IOException("expected \"]\", got " + (cc == -1 ? "to end of input" : "\"" + Terminal.String.
                    escapeCodePoint(cc) + "\"") + " while reading a character-class for a compiled grammar in line " + r.
                    getLineNumber() + "\r\n" + r.formatCurrentLineAndClose()));
        readNextNWS();
        return (new Terminal.CharacterClass.Union(t.toArray(new Terminal.CharacterClass[t.
                length()])));
    }

    private Terminal.CharacterClass readCharacterRangeOrSingle() throws
            IOException {
        int l, h;
        if(cc == -1)
            throw (new IOException("got " + (cc == -1 ? "to end of input" : "\"" + Terminal.String.
                    escapeCodePoint(cc) + "\"") + " while reading a character-class for a compiled grammar in line " + r.
                    getLineNumber() + "\r\n" + r.formatCurrentLineAndClose()));
        else if(cc == '\\') {
            readNext();
            l = unEscape();
            if(l < 0)
                throw (new IOException("expected escape-code, got " + (cc == -1 ? "to end of input" : "\"" + Terminal.String.
                        escapeCodePoint(cc) + "\"") + " while reading a character-class for a compiled grammar in line " + r.
                        getLineNumber() + "\r\n" + r.formatCurrentLineAndClose()));
        } else {
            l = cc;
            readNext();
        }
        if(cc != '-')
            return (new Terminal.CharacterClass.Single(l));
        readNext();
        if(cc == -1)
            throw (new IOException("got " + (cc == -1 ? "to end of input" : "\"" + Terminal.String.
                    escapeCodePoint(cc) + "\"") + " while reading a character-range for a compiled grammar in line " + r.
                    getLineNumber() + "\r\n" + r.formatCurrentLineAndClose()));
        else if(cc == '\\') {
            readNext();
            h = unEscape();
            if(l < 0)
                throw (new IOException("expected escape-code, got " + (cc == -1 ? "to end of input" : "\"" + Terminal.String.
                        escapeCodePoint(cc) + "\"") + " while reading a character-range for a compiled grammar in line " + r.
                        getLineNumber() + "\r\n" + r.formatCurrentLineAndClose()));
        } else {
            h = cc;
            readNext();
        }
        return (new Terminal.CharacterClass.Range(l, h));
    }

    private int readRuleList() throws IOException {
        if(cc != '{')
            return (0);
        readNextNWS();
        if(cc == '}') {
            readNextNWS();
            return (0);
        }
        int k = 0;
        readRule();
        k++;
        while(cc == ',') {
            readNextNWS();
            readRule();
            k++;
        }
        if(cc != '}')
            throw (new IOException("expected \"}\", got " + (cc == -1 ? "to end of input" : "\"" + Terminal.String.
                    escapeCodePoint(cc) + "\"") + " while reading a rule-list for a compiled grammar in line " + r.
                    getLineNumber() + "\r\n" + r.formatCurrentLineAndClose()));
        readNextNWS();
        return (k);
    }

    private Rule readRule() throws IOException {
        if(cc != '(')
            return (null);
        readNextNWS();
        String s = readNonTerminalName(STOP_CHARS_3);
        skipWS();
        if(cc != '=')
            throw (new IOException("expected \"=\", got " + (cc == -1 ? "to end of input" : "\"" + Terminal.String.
                    escapeCodePoint(cc) + "\"") + " while reading a rule for a compiled grammar in line " + r.
                    getLineNumber() + "\r\n" + r.formatCurrentLineAndClose()));
        Token t = N.getMatch(s);
        if(t == null)
            throw (new IOException("unknown nonterminal \"" + (new NonTerminal(s).
                    getEscapedName()) + "\" (1st) while reading a rule for a compiled grammar in line " + r.
                    getLineNumber() + "\r\n" + r.formatCurrentLineAndClose()));
        Rule u = new Rule(null, (Token[])null), v;
        u.lhs = (NonTerminal)t;
        readNextNWS();
        Path<Token> x = null;
        if(cc != ',') {
            x = new Path<Token>().append(readToken());
            while((cc != -1) && (cc != ','))
                x.append(readToken());
        }
        if(cc != ',')
            throw (new IOException("expected a rule id, got " + (cc == -1 ? "to end of input" : "\"" + Terminal.String.
                    escapeCodePoint(cc) + "\"") + " while reading a rule for a compiled grammar in line " + r.
                    getLineNumber() + "\r\n" + r.formatCurrentLineAndClose()));
        readNextNWS();
        u.id = (int)parseRNumber(0, 10, 10);
        skipWS();
        if(cc != ')')
            throw (new IOException("expected \")\", got " + (cc == -1 ? "to end of input" : "\"" + Terminal.String.
                    escapeCodePoint(cc) + "\"") + " while reading a rule for a compiled grammar in line " + r.
                    getLineNumber() + "\r\n" + r.formatCurrentLineAndClose()));
        readNextNWS();
        if(x != null)
            u.rhs = x.toArray(new Token[x.length()]);
        v = PT.get(u);
        if(v == null)
            PT.add(u);
        else {
            v.lhs = u.lhs;
            v.rhs = u.rhs;
        }
        return (u);
    }

    private Token readToken() throws IOException {
        switch(cc) {
            case '\'':
            case '\"':
            case '<':
                return (readNonCharacterClass());
            case '[':
                return (readCharacterClass());
            default:
                String s = readNonTerminalName(STOP_CHARS_4);
                skipWS();
                NonTerminal t = N.getMatch(s);
                if(t == null)
                    throw (new IOException("unknown nonterminal \"" + (new NonTerminal(
                            s).getEscapedName()) + "\" (1st) while reading a token for a compiled grammar in line " + r.
                            getLineNumber() + "\r\n" + r.
                            formatCurrentLineAndClose()));
                return (t);
        }
    }

    private String readNonTerminalName(int upchar) throws IOException {
        if(cc == '@') {
            StringBuilder sb = new StringBuilder("@");
            readNext();
            while(cc != -1) {
                if(cc == '\\') {
                    readNext();
                    switch(cc) {
                        case '\"':
                            sb.append('\"');
                            break;
                        default:
                            throw (new IOException("wrong escape sequence \"\\" + ((char)cc) + "\" while reading a special nonterminal for a compiled grammar in line " + r.
                                    getLineNumber() + "\r\n" + r.
                                    formatCurrentLineAndClose()));
                    }
                } else
                    sb.appendCodePoint(cc);
                if(cc == '@')
                    break;
                readNext();
            }
            if(cc != -1)
                readNextNWS();
            else
                throw (new IOException("expected \"@\", got to end of input while reading a special nonterminal for a compiled grammar in line " + r.
                        getLineNumber() + "\r\n" + r.formatCurrentLineAndClose()));
            return (sb.toString());
        } else
            return (readStringUpTo(upchar));
    }

    private void readFirstKSetStatement() throws IOException {
        if(cc == '[') {
            readNextNWS();
            String ch = parseIdentifier();
            if(ch.equalsIgnoreCase("start")) {
                ch = parseIdentifier();
                if(cc != ']')
                    throw (new IOException("expected \"]\", got "
                                           + (cc == -1 ? "to end of input" : "\""
                                                                             + Terminal.String.
                            escapeCodePoint(cc) + "\"")
                                           + " while parsing a firstKset in line "
                                           + this.r.getLineNumber() + "\r\n"
                                           + this.r.formatCurrentLineAndClose()));
                readNextNWS();
                if(this.S != null)
                    System.out.println(
                            "Ignoring duplicate start symbol entry in line "
                            + r.getLineNumber() + "\r\n");
                else {
                    S = new NonTerminal(ch);
                    N.add(S);
                }
            } else if(ch.equalsIgnoreCase("lexer")) {
                lexer = parseQualifiedIdentifier();
                if(cc != ']')
                    throw (new IOException("expected \"]\", got "
                                           + (cc == -1 ? "to end of input" : "\""
                                                                             + Terminal.String.
                            escapeCodePoint(cc) + "\"")
                                           + " while parsing a firstKset in line "
                                           + this.r.getLineNumber() + "\r\n"
                                           + this.r.formatCurrentLineAndClose()));
                readNextNWS();
            } else
                throw (new IOException("expected a firstKset-statement in line "
                                       + r.getLineNumber() + "\r\n"
                                       + r.formatCurrentLineAndClose()));
        } else if(cc == '(') {
            RuleWithFirstSets u = readRuleForFirstKSetSupplier();
            TerminalTrie fs = readTerminalTrie(fkss.k);
            if(u != null)
                u.firstSet = fs;

//            u.firstSet.makeFinal();
        } else if(cc == '{')
            readNullableNonTerminals();
        else
            throw (new IOException("expected a firstKset-statement in line "
                                   + r.getLineNumber() + "\r\n"
                                   + r.formatCurrentLineAndClose()));
    }

    private RuleWithFirstSets readRuleForFirstKSetSupplier() throws IOException {
        if(cc != '(')
            return (null);
        readNextNWS();
        String s = readNonTerminalName(STOP_CHARS_3);
        skipWS();
        if(cc != '=')
            throw (new IOException("expected \"=\", got " + (cc == -1
                                                             ? "to end of input" : "\"" + Terminal.String.
                    escapeCodePoint(cc) + "\"")
                                   + " while reading a rule for a firstKSet in line "
                                   + r.getLineNumber() + "\r\n" + r.
                    formatCurrentLineAndClose()));
        NonTerminal.Factory nf = NonTerminal.FACTORY.get();
        NonTerminal nt = N.addOrGetMatch(s, nf);
        Token t;
        Rule r1 = new Rule(nt, (Token[])null);
        readNextNWS();
        Path<FirstSetOf<? extends Token>> x = null;
        FirstSetOf.Factory<Terminal> fof = FirstSetOf.FACTORY.get();
        NonTerminalFirstSetCollection.Factory nfscf =
                NonTerminalFirstSetCollection.FACTORY.get();
        if(cc != ',') {
            x = new Path<FirstSetOf<? extends Token>>();
            do {
                switch(cc) {
                    case '\'':
                    case '\"':
                    case '<':
                        t = readNonCharacterClass();
                        break;
                    case '[':
                        t = readCharacterClass();
                        break;
                    default:
                        s = readNonTerminalName(STOP_CHARS_2);
                        t = N.addOrGetMatch(s, nf);
                        break;
                }
                if(t instanceof Terminal) {
                    FirstSetOf<Terminal> fsot = fkss.tfsc.addOrGetMatch((Terminal)t, fof);
                    if(fsot == fof.last)
                        if(fkss.k > 0) {
                            fsot.firstSet = new TerminalTrie();
                            fsot.firstSet.add((Terminal)t);
                            fsot.makeFinal();
                        } else
                            fsot.firstSet = fse.firstSet;
                    x.append(fsot);
                } else if(t instanceof NonTerminal)
                    x.append(fkss.nfscc.addOrGetMatch((NonTerminal)t, nfscf));
                else
                    throw (new IOException("Illegal token \"" + t
                                           + "\" while reading a rule for a firstKset in line "
                                           + r.getLineNumber() + "\r\n" + r.
                            formatCurrentLineAndClose()));
                skipWS();
            } while((cc != ',') && (cc != -1));
        }
        if(cc != ',')
            throw (new IOException("expected a rule id, got " + (cc == -1 ? "to end of input" : "\"" + Terminal.String.
                    escapeCodePoint(cc) + "\"") + " while reading a rule for a firstKset in line " + r.
                    getLineNumber() + "\r\n" + r.formatCurrentLineAndClose()));
        readNextNWS();
        r1.id = (int)parseRNumber(0, 10, 10);
        skipWS();
        if(cc != ')')
            throw (new IOException("expected \")\", got " + (cc == -1 ? "to end of input" : "\"" + Terminal.String.
                    escapeCodePoint(cc) + "\"") + " while reading a rule for a firstKset in line " + r.
                    getLineNumber() + "\r\n" + r.formatCurrentLineAndClose()));
        readNextNWS();
        Rule r2 = PT.addOrGet(r1);
        if(r2 != r1) {
            if((r2.lhs == null) || !r2.lhs.equals(r1.lhs))
                throw (new IOException("Rule mismatch while reading rule " + r1.id + " for a firstKset in line " + r.
                        getLineNumber() + "\r\n" + r.formatCurrentLineAndClose()));
            if(r2.rhs == null)
                r1 = r2;
            else {
                System.out.println("Ignoring duplicate rule in line "
                                   + r.getLineNumber() + "\r\n");
                return (null);
            }
        }
        NonTerminalFirstSetCollection nfsc = fkss.nfscc.addOrGetMatch(nt, nfscf);
        if(nfsc.rfsc == null)
            nfsc.rfsc = new IBBTree<RuleWithFirstSets>();
        RuleWithFirstSets u = new RuleWithFirstSets(r1);
        u.lhs = nfsc;
        if(x != null) {
            u.rhs = x.toArray((FirstSetOf<? extends Token>[])java.lang.reflect.Array.
                    newInstance(
                    fse.getClass(), x.length()));
            u.owner.rhs = new Token[u.rhs.length];
            for(int i = 0; i < u.rhs.length; i++)
                u.owner.rhs[i] = (Token) u.rhs[i].owner;
        }
        if(!nfsc.rfsc.add(u))
            throw (new IOException("Rule firstset mismatch while reading rule " + r1.id + " for a firstKset in line " + r.
                    getLineNumber() + "\r\n" + r.formatCurrentLineAndClose()));
        return (u);
    }

    private TerminalTrie readTerminalTrie(int k) throws IOException {
        TerminalTrie u;
        if(cc == '(')
            u = new TerminalTrie(readTerminalTrieNodeSet(k));
        else if(cc == '[')
            u = new TerminalTrie(readTerminalTrieFinalNodeSet(k));
        else if(cc == '{') {
            readNextNWS();
            u = new TerminalTrie();
            while((cc != -1) && (cc != '}')) {
                if(!u.add(readTerminalPath(k)))
                    System.out.println(
                            "Ignorig duplicate entry in terminal trie\r\n");
                while(cc == ',')
                    readNextNWS();
            }
            if(cc != '}')
                throw (new IOException("expected \"}\", got " + (cc == -1
                                                                 ? "to end of input" : "\"" + Terminal.String.
                        escapeCodePoint(cc) + "\"")
                                       + " while reading a terminal trie for a firstKSet in line "
                                       + r.getLineNumber() + "\r\n" + r.
                        formatCurrentLineAndClose()));
            readNextNWS();
        } else
            throw (new IOException("expected \"(\" or \"{\", got " + (cc == -1
                                                                      ? "to end of input" : "\"" + Terminal.String.
                    escapeCodePoint(cc) + "\"")
                                   + " while reading a terminal trie for a firstKSet in line "
                                   + r.getLineNumber() + "\r\n" + r.
                    formatCurrentLineAndClose()));
        if((u.heads.size() == 1) && u.heads.contains(TerminalTrie.EndNode))
            u = fse.firstSet;
        return (u);
    }

    private Path<Terminal> readTerminalPath(int k) throws IOException {
        if(cc != '(')
            throw (new IOException("expected \"(\" , got " + (cc == -1
                                                              ? "to end of input" : "\"" + Terminal.String.
                    escapeCodePoint(cc) + "\"")
                                   + " while reading a path for a terminal trie for a firstKSet in line "
                                   + r.getLineNumber() + "\r\n" + r.
                    formatCurrentLineAndClose()));
        readNextNWS();
        Path<Terminal> u = new Path<Terminal>();
        FirstSetOf<Terminal> fsot;
        FirstSetOf.Factory<Terminal> fof = FirstSetOf.FACTORY.get();
        while((cc != -1) && (cc != ')')) {
            fsot = fkss.tfsc.addOrGetMatch(readTerminal(), fof);
            if(fsot == fof.last)
                if(fkss.k > 0) {
                    fsot.firstSet = new TerminalTrie();
                    fsot.firstSet.add(fsot.owner);
                    fsot.makeFinal();
                } else
                    fsot.firstSet = fse.firstSet;
            u.append(fsot.owner);
        }
        if(cc != ')')
            throw (new IOException("expected \")\", got " + (cc == -1
                                                             ? "to end of input" : "\"" + Terminal.String.
                    escapeCodePoint(cc) + "\"")
                                   + " while reading a path for a terminal trie for a firstKSet in line "
                                   + r.getLineNumber() + "\r\n" + r.
                    formatCurrentLineAndClose()));
        readNextNWS();
        return (u);
    }

    private TerminalTrie.NodeSet readTerminalTrieNodeSet(int k) throws
            IOException {
        if(cc != '(')
            throw (new IOException("expected \"(\", got " + (cc == -1
                                                             ? "to end of input" : "\"" + Terminal.String.
                    escapeCodePoint(cc) + "\"")
                                   + " while reading a nodeset for a firstKSet in line "
                                   + r.getLineNumber() + "\r\n" + r.
                    formatCurrentLineAndClose()));
        TerminalTrie.NodeSet u = new TerminalTrie.NodeSet();
        readNextNWS();
        TerminalTrie.Node t = null;
        FirstSetOf<Terminal> fsot;
        FirstSetOf.Factory<Terminal> fof = FirstSetOf.FACTORY.get();
        while(cc != ')') {
            if(cc == '#') {
                if(!u.add(TerminalTrie.EndNode))
                    System.out.print("Ignoring duplicate node in nodeset in line "
                                     + r.getLineNumber() + "\r\n");
                readNextNWS();
            } else if(cc == ',') {
                System.out.print("Ignoring empty entry in nodeset in line "
                                 + r.getLineNumber() + "\r\n");
                readNextNWS();
            } else if(cc != -1) {
                fsot = fkss.tfsc.addOrGetMatch(readTerminal(), fof);
                if(fsot == fof.last)
                    if(fkss.k > 0) {
                        fsot.firstSet = new TerminalTrie();
                        fsot.firstSet.add(fsot.owner);
                        fsot.makeFinal();
                    } else
                        fsot.firstSet = fse.firstSet;
                t = new TerminalTrie.Node(fsot.owner);
                skipWS();
                if(cc == '[') {
                    System.out.print("Warning: final nodeset encountered"
                                     + " while expecting unfinal nodeset in line "
                                     + r.getLineNumber() + "\r\n");
                    if(k == 0)
                        System.out.print("Warning: nodeset to deep in line "
                                         + r.getLineNumber() + "\r\n");
                    t.nextNodes = readTerminalTrieFinalNodeSet(k - 1);
                } else if(cc == '(') {
                    if(k == 0)
                        System.out.print("Warning: nodeset to deep in line "
                                         + r.getLineNumber() + "\r\n");
                    t.nextNodes = readTerminalTrieNodeSet(k - 1);
                }
                if(!u.add(t))
                    System.out.print("Ignoring duplicate node in nodest in line "
                                     + r.getLineNumber() + "\r\n");
            } else
                throw (new IOException("expected node entry, got " + (cc == -1
                                                                      ? "to end of input" : "\"" + Terminal.String.
                        escapeCodePoint(cc) + "\"")
                                       + " while reading a nodeset for a firstKSet in line "
                                       + r.getLineNumber() + "\r\n" + r.
                        formatCurrentLineAndClose()));
            if(cc == ',')
                readNextNWS();
            else if(cc != ')')
                throw (new IOException("expected \")\", got " + (cc == -1
                                                                 ? "to end of input" : "\"" + Terminal.String.
                        escapeCodePoint(cc) + "\"")
                                       + " while reading a nodeset for a firstKSet in line "
                                       + r.getLineNumber() + "\r\n" + r.
                        formatCurrentLineAndClose()));
        }
        readNextNWS();
        return (u);
    }

    private TerminalTrie.NodeSet readTerminalTrieFinalNodeSet(int k) throws
            IOException {
        if(cc != '[')
            throw (new IOException("expected \"[\", got " + (cc == -1
                                                             ? "to end of input" : "\"" + Terminal.String.
                    escapeCodePoint(cc) + "\"")
                                   + " while reading a final nodeset for a firstKSet in line "
                                   + r.getLineNumber() + "\r\n" + r.
                    formatCurrentLineAndClose()));
        readNextNWS();
        int ncl = (int)parseRNumber(1, 10, 10), ccl = 0;
        skipWS();
        if(ncl < 0)
            throw (new IOException("Negative nodeset size encountered"
                                   + " while reading a final nodeset for a firstKSet in line "
                                   + r.getLineNumber() + "\r\n" + r.
                    formatCurrentLineAndClose()));
        if(cc == ',') {
            ccl = (int)parseRNumber(1, 10, 10);
            skipWS();
            if(ccl < 0)
                throw (new IOException("Negative nodeset size encountered"
                                       + " while reading a final nodeset for a firstKSet in line "
                                       + r.getLineNumber() + "\r\n" + r.
                        formatCurrentLineAndClose()));
        }
        if(cc != ']')
            throw (new IOException("expected \"[\", got " + (cc == -1
                                                             ? "to end of input" : "\"" + Terminal.String.
                    escapeCodePoint(cc) + "\"")
                                   + " while reading a final nodeset for a firstKSet in line "
                                   + r.getLineNumber() + "\r\n" + r.
                    formatCurrentLineAndClose()));
        readNextNWS();
        if(cc != '(')
            throw (new IOException("expected \"(\", got " + (cc == -1
                                                             ? "to end of input" : "\"" + Terminal.String.
                    escapeCodePoint(cc) + "\"")
                                   + " while reading a final nodeset for a firstKSet in line "
                                   + r.getLineNumber() + "\r\n" + r.
                    formatCurrentLineAndClose()));
        readNextNWS();
        TerminalTrie.Node t = null;
        FirstSetOf<Terminal> fsot;
        FirstSetOf.Factory<Terminal> fof = FirstSetOf.FACTORY.get();
        int i = 0;
        TerminalTrie.Node[] nca = ncl > 0 ? new TerminalTrie.Node[ncl] : null,
                cca = ccl > 0 ? new TerminalTrie.Node[ccl] : null;
        while(i < ncl) {
            if(cc == '#') {
                nca[i++] = TerminalTrie.EndNode;
                readNextNWS();
            } else if(cc != -1) {
                fsot = fkss.tfsc.addOrGetMatch(readNonCharacterClass(), fof);
                if(fsot == fof.last)
                    if(fkss.k > 0) {
                        fsot.firstSet = new TerminalTrie();
                        fsot.firstSet.add(fsot.owner);
                        fsot.makeFinal();
                    } else
                        fsot.firstSet = fse.firstSet;
                t = new TerminalTrie.Node(fsot.owner);
                skipWS();
                if(cc == '[') {
                    if(k == 0)
                        System.out.print("Warning: nodeset to deep in line "
                                         + r.getLineNumber() + "\r\n");
                    t.nextNodes = readTerminalTrieFinalNodeSet(k - 1);
                } else if(cc == '(') {
                    System.out.print("Warning: unfinal nodeset encountered"
                                     + " while expecting final nodeset in line "
                                     + r.getLineNumber() + "\r\n");
                    if(k == 0)
                        System.out.print("Warning: nodeset to deep in line "
                                         + r.getLineNumber() + "\r\n");
                    t.nextNodes = readTerminalTrieNodeSet(k - 1);
                }
                nca[i++] = t;
            } else
                throw (new IOException("expected node entry, got " + (cc == -1
                                                                      ? "to end of input" : "\"" + Terminal.String.
                        escapeCodePoint(cc) + "\"")
                                       + " while reading a final nodeset for a firstKSet in line "
                                       + r.getLineNumber() + "\r\n" + r.
                        formatCurrentLineAndClose()));
            if(cc == ',')
                readNextNWS();
            else if(cc != ')')
                throw (new IOException("expected \")\", got " + (cc == -1
                                                                 ? "to end of input" : "\"" + Terminal.String.
                        escapeCodePoint(cc) + "\"")
                                       + " while reading a final nodeset for a firstKSet in line "
                                       + r.getLineNumber() + "\r\n" + r.
                        formatCurrentLineAndClose()));
            else if((i < ncl) || (ccl > 0))
                throw (new IOException("Size mismatch encountered"
                                       + " while reading a final nodeset for a firstKSet in line "
                                       + r.getLineNumber() + "\r\n" + r.
                        formatCurrentLineAndClose()));
        }
        i = 0;
        while(i < ccl) {
            if(cc == '[') {
                fsot = fkss.tfsc.addOrGetMatch(readCharacterClass(), fof);
                if(fsot == fof.last)
                    if(fkss.k > 0) {
                        fsot.firstSet = new TerminalTrie();
                        fsot.firstSet.add(fsot.owner);
                        fsot.makeFinal();
                    } else
                        fsot.firstSet = fse.firstSet;
                t = new TerminalTrie.Node(fsot.owner);
                skipWS();
                if(cc == '[') {
                    if(k == 0)
                        System.out.print("Warning: nodeset to deep in line "
                                         + r.getLineNumber() + "\r\n");
                    t.nextNodes = readTerminalTrieFinalNodeSet(k - 1);
                } else if(cc == '(') {
                    System.out.print("Warning: unfinal nodeset encountered"
                                     + " while expecting final nodeset in line "
                                     + r.getLineNumber() + "\r\n");
                    if(k == 0)
                        System.out.print("Warning: nodeset to deep in line "
                                         + r.getLineNumber() + "\r\n");
                    t.nextNodes = readTerminalTrieNodeSet(k - 1);
                }
                cca[i++] = t;
            } else
                throw (new IOException("Size mismatch encountered"
                                       + " while reading a final nodeset for a firstKSet in line "
                                       + r.getLineNumber() + "\r\n" + r.
                        formatCurrentLineAndClose()));
            if(cc == ',')
                readNextNWS();
            else if(cc != ')')
                throw (new IOException("expected \")\", got " + (cc == -1
                                                                 ? "to end of input" : "\"" + Terminal.String.
                        escapeCodePoint(cc) + "\"")
                                       + " while reading a final nodeset for a firstKSet in line "
                                       + r.getLineNumber() + "\r\n" + r.
                        formatCurrentLineAndClose()));
            else if(i < ccl)
                throw (new IOException("Size mismatch encountered"
                                       + " while reading a final nodeset for a firstKSet in line "
                                       + r.getLineNumber() + "\r\n" + r.
                        formatCurrentLineAndClose()));
        }
        if(cc != ')')
            throw (new IOException("expected \")\", got " + (cc == -1
                                                             ? "to end of input" : "\"" + Terminal.String.
                    escapeCodePoint(cc) + "\"")
                                   + " (possibly size mismatch) while reading a final nodeset"
                                   + " for a firstKSet in line " + r.
                    getLineNumber() + "\r\n"
                                   + r.formatCurrentLineAndClose()));
        readNextNWS();
        TerminalTrie.NodeSet u = new TerminalTrie.NodeSet(
                nca != null ? new Final(nca, null, true) : null,
                cca != null ? new Final(cca, null, true) : null);
        return (u);
    }

    private void readNullableNonTerminals() throws IOException {
        if(cc != '{')
            return;
        readNextNWS();
        NonTerminal t;
        NonTerminal.Factory nf = NonTerminal.FACTORY.get();
        while((cc != -1) && (cc != '}')) {
            if(cc == '(') {
                readNextNWS();
                t = N.addOrGetMatch(readNonTerminalName(STOP_CHARS_1), nf);
                skipWS();
                if(cc != ',')
                    throw (new IOException("expected \",\", got " + (cc == -1
                                                                     ? "to end of input" : "\"" + Terminal.String.
                            escapeCodePoint(cc) + "\"")
                                           + " while reading a nullable symbol for a firstKSet in line "
                                           + r.getLineNumber() + "\r\n" + r.
                            formatCurrentLineAndClose()));
                readNextNWS();
                if(cc != ')') {
                    Rule r = PT.addOrGet(new Rule(t, (Token[])null,
                                                  (int)parseRNumber(1, 10, 10)));
                    if(r != null)
                        t.setNullingRule(r);
                    else
                        t.setNullable(true);
                    skipWS();
                } else
                    t.setNullable(true);
                if(cc != ')')
                    throw (new IOException("expected \")\", got " + (cc == -1
                                                                     ? "to end of input" : "\"" + Terminal.String.
                            escapeCodePoint(cc) + "\"")
                                           + " while reading a nullable symbol for a firstKSet in line "
                                           + r.getLineNumber() + "\r\n" + r.
                            formatCurrentLineAndClose()));
                readNextNWS();
            } else if(cc != ',') {
                t = N.addOrGetMatch(readNonTerminalName(STOP_CHARS_1), nf);
                skipWS();
                t.setNullable(true);
            }
            if(cc == ',')
                readNextNWS();
        }
        if(cc != '}')
            throw (new IOException("expected \"}\", got " + (cc == -1
                                                             ? "to end of input" : "\"" + Terminal.String.
                    escapeCodePoint(cc) + "\"")
                                   + " while reading a nullable nonterminal list for a firstKSet in line "
                                   + r.getLineNumber() + "\r\n" + r.
                    formatCurrentLineAndClose()));
        readNextNWS();
    }

    private String readStringUpTo(int upchar) throws IOException {
        StringBuffer sb = new StringBuffer();
        int h;
        while((cc != upchar) && (cc != -1))
            if(cc != '\\') {
                sb.appendCodePoint(cc);
                readNext();
            } else {
                readNext();
                h = unEscape();
                if(h < 0) {
                    sb.append('\\').appendCodePoint(cc);
                    readNext();
                } else
                    sb.appendCodePoint(h);
            }
        return (sb.toString());
    }

    private String readNonTerminalName(int[] upchars) throws IOException {
        if(cc == '@') {
            StringBuilder sb = new StringBuilder("@");
            readNext();
            while(cc != -1) {
                if(cc == '\\') {
                    readNext();
                    switch(cc) {
                        case '\"':
                            sb.append('\"');
                            break;
                        default:
                            throw (new IOException("wrong escape sequence \"\\" + ((char)cc) + "\" while reading a special nonterminal for a compiled grammar in line " + r.
                                    getLineNumber() + "\r\n" + r.
                                    formatCurrentLineAndClose()));
                    }
                } else
                    sb.appendCodePoint(cc);
                if(cc == '@')
                    break;
                readNext();
            }
            if(cc != -1)
                readNext();
            else
                throw (new IOException("expected \"@\", got to end of input while reading a special nonterminal for a compiled grammar in line " + r.
                        getLineNumber() + "\r\n" + r.formatCurrentLineAndClose()));
            return (sb.toString());
        } else
            return (readStringUpTo(upchars));
    }

    private String readStringUpTo(int[] upchars) throws IOException {
        StringBuffer sb = new StringBuffer();
        int h, i;
        if(upchars == null)
            upchars = new int[0];
out:    while(cc != -1) {
            for(i = 0; i < upchars.length; i++)
                if(cc == upchars[i])
                    break out;
            if(cc != '\\') {
                sb.appendCodePoint(cc);
                readNext();
            } else {
                readNext();
                h = unEscape();
                if(h < 0) {
                    sb.append('\\').appendCodePoint(cc);
                    readNext();
                } else
                    sb.appendCodePoint(h);
            }
        }
        return (sb.toString());
    }

    private int unEscape() throws IOException {
        if((cc >= '0') && (cc < '8'))
            return ((int)parseRNumber(1, 3, 8));
        if((cc >>> 16) != 0)
            return (-1);
        int r = 0;
        switch(cc) {
            case 'a':
                r = '\u0007';
                break;
            case 'b':
                r = '\b';
                break;
            case 'e':
                r = '\u001B';
                break;
            case 't':
                r = '\t';
                break;
            case 'n':
                r = '\n';
                break;
            case 'f':
                r = '\f';
                break;
            case 'r':
                r = '\r';
                break;
            case '.':
            case '-':
            case '\\':
            case '\'':
            case '\"':
            case '(':
            case ')':
            case '<':
            case '>':
            case '[':
            case ']':
                r = cc;
                break;
            case 'U':
                readNext();
                return ((int)parseRNumber(6, 6, 16));
            case 'u':
                readNext();
                return ((int)parseRNumber(4, 4, 16));
            case 'x':
                readNext();
                return ((int)parseRNumber(2, 2, 16));
            default:
                return (-1);
        }
        readNext();
        return (r);
    }

    private long parseRNumber(int min_digits, int max_digits, int radix) throws
            IOException {
        long n = 0;
        int d = 0;
        boolean negate = false;
        if(radix <= 1)
            radix = 10;
        if((max_digits <= 0) || (max_digits < min_digits))
            return (0);
        if(cc == '-') {
            readNext();
            negate = true;
            if(min_digits == 0)
                min_digits = 1;
        } else if(cc == '+') {
            readNext();
            negate = false;
            if(min_digits == 0)
                min_digits = 1;
        }
        while(d++ < max_digits) {
            if((cc >= '0') && (cc <= '9'))
                n = n * radix + (cc - '0');
            else if((cc >= 'a') && (cc <= 'z') && (cc - 'a' < radix - 10))
                n = n * radix + (cc - 'a' + 10);
            else if((cc >= 'A') && (cc <= 'Z') && (cc - 'A' < radix - 10))
                n = n * radix + (cc - 'A' + 10);
            else if(d <= min_digits)
                throw (new IOException("expected " + (min_digits - d + 1) + " more digits of a base-" + radix + "-number,got " + (cc == -1 ? "to end of input" : "\"" + Terminal.String.
                        escapeCodePoint(cc) + "\"") + " while parsing a number in line " + r.
                        getLineNumber() + "\r\n" + r.formatCurrentLineAndClose()));
            else
                break;
            readNext();
        }
        //if(Character.isWhitespace(cc)||(cc=='/'))
        //    readNextNWS();
        return (negate ? -n : n);
    }

    private long parseRNumber_u(int min_digits, int max_digits, int radix)
            throws IOException {
        long n = 0;
        int d = 0;
        if(radix <= 1)
            radix = 10;
        if((max_digits <= 0) || (max_digits < min_digits))
            return (0);
        while(d++ < max_digits) {
            if((nc >= '0') && (nc <= '9'))
                n = n * radix + (nc - '0');
            else if((nc >= 'a') && (nc <= 'z') && (nc - 'a' < radix - 10))
                n = n * radix + (nc - 'a' + 10);
            else if((nc >= 'A') && (nc <= 'Z') && (nc - 'A' < radix - 10))
                n = n * radix + (nc - 'A' + 10);
            else if(d <= min_digits)
                throw (new IOException("expected " + (min_digits - d + 1) + " more digits of a base-" + radix + "-number,got " + (nc == -1 ? "to end of input" : "\"" + (char)nc + "\"") + " while parsing an u-escape in line " + r.
                        getLineNumber() + "\r\n" + r.formatCurrentLineAndClose()));
            else
                break;
            nc = r.read();
        }
        return (n);
    }

    private int readNext_u() throws IOException {
        if(nc == -1) {
            cc = -1;
            return (cc);
        }
        cc = nc;
        nc = r.read();
        if(cc == '\\')
            if(((nbs & 1) == 0) && ((nc == 'u') || (nc == 'U'))) {
                cc = nc;
                nc = r.read();
                if(nc != cc)
                    cc = (int)(cc == 'u' ? parseRNumber_u(4, 4, 16) : parseRNumber_u(
                            1, 6, 16));
                else {
                    cc = '\\';
                    nbs = 0;
                }
            } else if(nc == '\\')
                nbs++;
            else
                nbs = 0;
        return (cc);
    }

    private int readNext() throws IOException {
        readNext_u();
        if((cc >= 0) && (cc <= 0xFFFF) && java.lang.Character.isHighSurrogate(
                (char)cc)) {
            int hc = cc;
            readNext_u();
            if((cc < 0) || (cc > 0xFFFF) || !java.lang.Character.isLowSurrogate(
                    (char)cc))
                throw (new IOException("expected low surrogate, got " + (cc == -1 ? "to end of input" : "\"" + Terminal.String.
                        escapeCodePoint(cc) + "\"") + " while parsing a supplementary character in line " + r.
                        getLineNumber() + "\r\n" + r.formatCurrentLineAndClose()));
            cc = java.lang.Character.toCodePoint((char)hc, (char)cc);
        }
        return (cc);
    }

    private int readNextNWS() throws IOException {
        while(true) {
            readNext();
            while(skipComment()) {
            }
            if(!Character.isWhitespace(cc))
                break;
        }
        return (cc);
    }

    private int skipWS() throws IOException {
        if(Character.isWhitespace(cc) || (cc == '/'))
            readNextNWS();
        return (cc);
    }

    private int readNextNWSNC() throws IOException {
        do
            readNext();
        while(Character.isWhitespace(cc));
        return (cc);
    }

    private int skipWSNC() throws IOException {
        if(Character.isWhitespace(cc))
            readNextNWSNC();
        return (cc);
    }

    private boolean skipComment() throws IOException {
        if(cc != '/')
            return (false);
        readNext();
        if(cc == '/') {
            do
                cc = readNext();
            while((cc != -1) && (cc != '\n'));
            if(cc != -1)
                cc = readNext();
            return (true);
        } else if(cc == '*') {
            cc = readNext();
            while(true) {
                if(cc == -1)
                    throw (new IOException(
                            "unexpected end of input while parsing a traditional comment"));
                if(cc == '*') {
                    cc = readNext();
                    if(cc == -1)
                        throw (new IOException(
                                "unexpected end of input while parsing a traditional comment"));
                    if(cc == '/') {
                        cc = readNext();
                        return (true);
                    }
                } else
                    readNext();
            }
        } else
            throw (new IOException("expectet comment in line " + r.getLineNumber() + "\r\n" + r.
                    formatCurrentLineAndClose()));
    }

    private static class IntNode {

        int x;
        IntNode next;

        public IntNode() {
            this(-1, null);
        }

        public IntNode(int x) {
            this(x, null);
        }

        public IntNode(int x, IntNode next) {
            this.x = x;
            this.next = next;
        }
    }
}
