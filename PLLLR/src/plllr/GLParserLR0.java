/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package plllr;

import bbtree.*;
import java.io.*;
import plllr.FirstKSetSupplier.NonTerminalFirstSetCollection;
import plllr.FirstKSetSupplier.RuleWithFirstSets;

/**
 *
 * @author RJ
 */
public class GLParserLR0 extends GLParser {

    private BBTree<Node> U, V;   //U enthält alle 'aktuellen' Knoten des GSS, V dient als Zwischenpuffer.
    private final Path<Node> L = new Path<Node>();       //Eine Liste von Knoten, die abgearbeitet werden m�ssen.

    /**
     * Creates a new instance of ParserLR0
     */
    public GLParserLR0() {
        U = new BBTree<Node>(); //Die Haupt-Mengen initialisieren
        V = new BBTree<Node>();
        dcache = new BBTree<Derivation.Item>();
        ncache = new BBTree<NonTerminalItemWithUsedRuleSet>();
        expectedTrie = new TerminalTrie();
    }

    public synchronized Path<Derivation.Item.NonTerminal> parse(
            TerminalReader tr,
            FirstKSetSupplier f,
            boolean useProductionLookAhead,
            boolean infixParsing,
            Writer gw)
            throws IOException {
        if(f == null)
            throw new NullPointerException("Null firstKset");
        if(f.g == null)
            throw new NullPointerException("Null grammar");
        if(f.g.S == null)
            throw new NullPointerException("Null start symbol");
        this.f = f;
        this.useProductionLookAhead = useProductionLookAhead;
        this.infixParsing = infixParsing;
        Node t;
        Terminal tl;
        dcache.clear();
        ncache.clear();
        p = new Path<Derivation.Item.NonTerminal>();
        ctp.clear();
        if(tr != null)
            if(f.k == 0) {
                tl = tr.read();
                if(tl == null)
                    ctp.append(Terminal.End);
                else
                    ctp.append(tl);
            } else {
                int i = 0;
                while(i < f.k) {
                    tl = tr.read();
                    if(tl == null) {
                        ctp.append(Terminal.End);
                        break;
                    }
                    ctp.append(tl);
                    if(tl == Terminal.End)
                        break;
                    i++;
                }
            }
        else
            ctp.append(Terminal.End);
        initGraphWriter(gw);
        ct = ctp.first();
        if(ct == Terminal.End) {
            if(f.g.S.isNullable())
                p.append(Derivation.Item.generateEpsilonShiftItem(f.g.S, 0));
            derivation_nodes_created.inc();
            derivation_edges_created.inc();
            finishGraphWriter();
            return p;
        }
        cp = 0;
        U.add(Node.makeStartNode(f.g.S, 0));
        nodes_created.inc();
        reduce_edges_created.inc();
        while(ct != Terminal.End) {
            produce();
            if(DEBUG)
                System.out.println("post-produce(" + cp + "): " + U.toString());
            makeNodesFinal();
            if(!DEBUG)
                dcache.clear();
            writeCurrentGraphCluster(U);
            shift();
            if(DEBUG)
                System.out.println("post-shift(" + cp + "): " + U.toString());
            if(U.isEmpty() && !infixParsing) {
                finishGraphWriter();
                return p;
            } else
                shifted_symbols.inc();
            if(ct != ctp.pop())
                throw new RuntimeException("impossible error occured");
            if(ctp.last() != Terminal.End) {
                tl = tr.read();
                if(tl == null)
                    ctp.append(Terminal.End);
                else
                    ctp.append(tl);
            }
            ct = ctp.first();
            if(ct == null)
                throw (new NullPointerException("Null terminal"));
            reduce();
            if(DEBUG)
                System.out.println("post-reduce(" + cp + "): " + U.toString());
            else {
                dcache.clear();
                ncache.clear();
            }
            if(ct != Terminal.End && infixParsing)
                if(U.add(t = Node.makeStartNode(f.g.S, cp))) {
                    nodes_created.inc();
                    reduce_edges_created.inc();
                } else {
                    t = U.get(t);
                    if(t.addEdge(Edge.StackEnd))
                        reduce_edges_created.inc();
                    else
                        reduce_edges_visited.inc();
                }
        }
        makeNodesFinal();
        finishGraphWriter();
        if(p.isEmpty()) {
            expectedTrie.clear();
            for(Node x : U)
                expectedTrie.addAll(f.firstKSetOf(x.tp));
        }
        U.clear();
        V.clear();
        L.clear();
        if(DEBUG)
            System.out.println("Dcache(" + dcache.size() + "): " + dcache.
                    toString());
        dcache.clear();
        if(DEBUG)
            System.out.println("Ncache(" + ncache.size() + "): " + ncache.
                    toString());
        ncache.clear();
        return (p);
    }

    public boolean generatesParseGraph() {
        return true;
    }

    private void produce() {
        NonTerminal d;
        NonTerminalFirstSetCollection nfsc;
        Node t;
        L.appendSome(U, PRODUCABLES);
        Path.Node<Node> tn = L.head;
        while(tn != null) {
            producer.perform(null, Traversable.IN_ORDER, tn.x);
            tn = tn.next;
        }
        while(!L.isEmpty()) {
            while((t = L.pop()) != null) {
                V.remove(t);
                d = (NonTerminal)(t.tp.head.x);
                if(d == null)
                    continue;
                nfsc = f.getFirstSetCollectionFor(d);
                if(nfsc == null)
                    throw (new RuntimeException(
                            "No firstsets for nonterminal " + d));
                nfsc.rfsc.traverse(Traversable.IN_ORDER, producer, t);
            }
            L.appendSome(V, null);
            V.clear();
        }
    }

    private void makeNodesFinal() {
        U.traverse(TraversalActionPerformer.IN_ORDER, FINALIZER, null);
    }

    private void shift() {
        csi = new Derivation.Item.Terminal(ct, cp);
        cp++;
        U.traverse(BBTree.IN_ORDER, SHIFTER, this);
        if(V.isEmpty()) {
            expectedTrie.clear();
            for(Node x : U)
                expectedTrie.addAll(f.firstKSetOf(x.tp));
        }
        BBTree<Node> h = U;
        U = V;
        V = h;
        if(!U.isEmpty() || infixParsing)
            V.clear();
    }

    private void reduce() {
        Node t;
        U.traverse(Traversable.IN_ORDER, REDUCIBLES_APPENDER, this);
        while(!L.isEmpty())
            while((t = L.pop()) != null) {
                if(t.edges == null)
                    continue;
                if(useTransitiveEdges) {
                    Edge.Transitive et = t.createOrGetTransitiveEdge(this);
                    if(et != null) {
                        reducer.perform(et, Traversable.IN_ORDER, t);
                        continue;
                    }
                }
                t.edges.traverse(Traversable.IN_ORDER, reducer, t);
            }
    }

    private boolean addRightNulledTerminationToRightNullableNode(Node t) {
        if((t == null) || !t.isReducable() || (t.last_dpos == cp))
            return false;
        t.last_dpos = cp;
        t.last_der = t.tp.isEmpty() ? null : new Derivation.TerminationNode(
                t.tp, t.pos);
        return true;
    }
    private static final TraversalActionPerformer<Node> REDUCIBLES_APPENDER = new TraversalActionPerformer<Node>() {

        public boolean perform(Node x, int order, Object o) {
            if(o != null) {
                GLParserLR0 p = (GLParserLR0)o;
                if(p.addRightNulledTerminationToRightNullableNode(x))
                    p.L.append(x);
            }
            return (false);
        }
    };

    private static class Producer implements
            TraversalActionPerformer<RuleWithFirstSets> {

        public final GLParserLR0 p;

        public Producer(GLParserLR0 p) {
            if(p == null)
                throw (new NullPointerException("Parser is null"));
            this.p = p;
        }

        public boolean perform(RuleWithFirstSets rfs, int order, Object o) {
            Node t = (Node)o;
            boolean shift_on = false;
            Node cc;
            Edge ce;
            if(rfs != null) {
                if(rfs.owner.rhsLength() == 0)
                    return false;
                if(p.useProductionLookAhead && (p.f.k > 0)) {
                    TerminalTrie[] ta2 = TRIE_ARRAY_2.get();
                    ta2[0] = rfs.firstSet;
                    ta2[1] = p.f.firstKSetOf(t.tp.head.next);
                    if(!TerminalTrie.BoundedConcatenationMatchesPrefixOfPath(ta2,
                                                                             p.f.k,
                                                                             false,
                                                                             p.ctp))
                        return false;
                }
                Node.AFactory naf = Node.AFactory.getThreadLocal(p);
                cc = p.U.addOrGetMatch(rfs.owner.rhs, naf);
                if(cc == naf.last) {
                    p.nodes_created.inc();
                    if(cc.isProducable()) {
                        p.V.add(cc);
                        shift_on = true;
                    }
                }
                ce = new Edge.Reduce(t, rfs.owner);
                if(cc.addEdge(ce))
                    p.reduce_edges_created.inc();
                else
                    p.reduce_edges_visited.inc();
                if(!shift_on)
                    return false;
                t = cc;
            }
            Node.PFactory npf = Node.PFactory.getThreadLocal(p);
            NonTerminal d = (NonTerminal)(t.tp.head.x);
            Path<Token> gamma;
            while(((d == null) || d.isNullable()) && (t.tp.head.next != null)) {
                gamma = t.tp.shift();
                if(p.useProductionLookAhead && (p.f.k > 0))
                    if(!p.f.firstKSetOf(gamma).matchesPrefixOf(p.ctp))
                        break;
                shift_on = false;
                cc = p.U.addOrGetMatch(gamma, npf);
                if(cc == npf.last) {
                    p.nodes_created.inc();
                    if(cc.isProducable()) {
                        p.V.add(cc);
                        shift_on = true;
                    }
                }
                ce = new Edge.Shift(t, p.dcache.addOrGet(
                        Derivation.Item.generateEpsilonShiftItem(d, p.cp)));
                if(cc.addEdge(ce))
                    p.shift_edges_created.inc();
                else
                    p.shift_edges_visited.inc();
                if(!shift_on)
                    break;
                t = cc;
                d = (NonTerminal)(t.tp.head.x);
            }
            return (false);
        }
    }
    private final Producer producer = new Producer(this);
    private static final TraversalActionPerformer<Node> SHIFTER = new TraversalActionPerformer<Node>() {

        public boolean perform(Node x, int order, Object o) {
            if((x == null) || !x.isShiftable())
                return (false);
            Node y;
            GLParserLR0 p = (GLParserLR0)o;
            if(((Terminal)(x.tp.head.x)).matches(p.ct)) {
                Node.PFactory npf = Node.PFactory.getThreadLocal(p);
                if(npf.p == null)
                    npf.p = p;
                y = p.V.addOrGetMatch(x.tp.shift(), npf);
                if(y == npf.last)
                    p.nodes_created.inc();
                if(y.addEdge(new Edge.Shift(x, p.csi)))
                    p.shift_edges_created.inc();
                else
                    p.shift_edges_visited.inc();
            }
            return (false);
        }
    };

    private static class Reducer implements TraversalActionPerformer<Edge> {

        public final GLParserLR0 p;

        public Reducer(GLParserLR0 p) {
            if(p == null)
                throw (new NullPointerException("Parser is null"));
            this.p = p;
        }

        public boolean perform(Edge e, int order, Object o) {
            Node t = (Node)o, u = e.target;
            if(e instanceof Edge.Reduce || e instanceof Edge.Transitive) {
                boolean tEdge = e instanceof Edge.Transitive;
                if(tEdge)
                    p.transitive_edges_visited.inc();
                else
                    p.reduce_edges_visited.inc();
                Edge.Reduce er = tEdge ? null : (Edge.Reduce)e;
                Edge.Transitive et = tEdge ? (Edge.Transitive)e : null;
                if(!tEdge && er.r == null) {
                    if(u != null)
                        throw (new RuntimeException(
                                "Null rule in reduction edge discovered"));
                    if((p.ct == Terminal.End) || p.infixParsing) {
                        p.p.append(
                                (Derivation.Item.NonTerminal)(t.last_der.head.x.item));
                        p.derivation_nodes_created.inc();
                        p.derivation_edges_created.inc();
                    }
                } else {
                    if(u == null)
                        throw new RuntimeException(
                                (tEdge ? "transitive" : "illegal reduction") + " edge to stack end detected");
                    if(!tEdge)
                        if(!u.tp.head.x.equals(er.r.lhs))
                            throw new RuntimeException(
                                    "illegal reduction edge detected: " + e);
                    Path<Token> beta = u.tp.shift();
                    Node.PFactory npf = Node.PFactory.getThreadLocal(p);
                    Node v = p.U.addOrGetMatch(beta, npf);
                    if(v == npf.last)
                        p.nodes_created.inc();
                    Edge.Shift es = new Edge.Shift(u, null);
                    Edge h = v.addOrGetEdge(es);
                    if(h == es) {
                        p.shift_edges_created.inc();
                        NonTerminalItemWithUsedRuleSet.Factory nuf =
                                NonTerminalItemWithUsedRuleSet.Factory.
                                getThreadLocal(p);
                        NonTerminalItemWithUsedRuleSet ntiwurs = p.ncache.
                                addOrGetMatch(
                                new Derivation.Item.NonTerminal(
                                (NonTerminal)u.tp.head.x, u.pos, p.cp), nuf);
                        if(ntiwurs == nuf.last)
                            p.derivation_nodes_created.inc();
                        es.der = ntiwurs.nti;
                        if(tEdge) {
                            ntiwurs.nti.appendReversedDerivationPathEntry(new Derivation.ReversedDerivationPathEntry(
                                    et.reversedDerivationPath, t.last_der));
                            p.derivation_edges_created.inc();
                        } else if(ntiwurs.useRule(er.r)) {
                            ntiwurs.nti.appendRuleEdge(er.r, t.last_der);
                            p.derivation_edges_created.inc();
                        }
                        if(v.isReducable()) {
                            p.addRightNulledTerminationToRightNullableNode(v);
                            if(u.last_dpos != p.cp) {
                                u.last_dpos = p.cp;
                                u.last_der = new Derivation.DistributionNode();
                                u.last_der.append(
                                        new Derivation.Edge(es.der, v.last_der));
                                p.derivation_nodes_created.inc();
                                p.derivation_edges_created.inc();
                                p.L.append(u);
                            } else {
                                u.last_der.append(
                                        new Derivation.Edge(es.der, v.last_der));
                                p.derivation_nodes_created.inc();
                                p.derivation_edges_created.inc();
                            }
                        }
                    } else {
                        p.shift_edges_visited.inc();
                        es = (Edge.Shift)h;
                        if(tEdge) {
                            ((Derivation.Item.NonTerminal)es.der).
                                    appendReversedDerivationPathEntry(new Derivation.ReversedDerivationPathEntry(
                                    et.reversedDerivationPath, t.last_der));
                            p.derivation_edges_created.inc();
                        } else {
                            NonTerminalItemWithUsedRuleSet ntiwurs = p.ncache.
                                    getMatch(
                                    new Derivation.Item.NonTerminal(er.r.lhs,
                                                                    u.pos, p.cp,
                                                                    null, null));
                            if(ntiwurs.useRule(er.r)) {
                                ntiwurs.nti.appendRuleEdge(er.r, t.last_der);
                                p.derivation_edges_created.inc();
                            }
                        }
                    }
                }
                return tEdge;
            } else if(e instanceof Edge.Shift) {
                p.shift_edges_visited.inc();
                Edge.Shift es = (Edge.Shift)e;
                if(u.last_dpos != p.cp) {
                    u.last_dpos = p.cp;
                    u.last_der = new Derivation.DistributionNode();
                    u.last_der.append(new Derivation.Edge(es.der, t.last_der));
                    p.derivation_nodes_created.inc();
                    p.derivation_edges_created.inc();
                    p.L.append(u);
                } else {
                    u.last_der.append(new Derivation.Edge(es.der, t.last_der));
                    p.derivation_nodes_created.inc();
                    p.derivation_edges_created.inc();
                }
            }
            return false;
        }
    }
    private final Reducer reducer = new Reducer(this);
}
