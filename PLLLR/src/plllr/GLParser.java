/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package plllr;

import bbtree.*;
import java.io.IOException;
import java.io.Writer;

/**
 *
 * @author rj
 */
public abstract class GLParser extends Parser {

    protected final Path<Terminal> ctp = new Path<Terminal>();
    protected FirstKSetSupplier f;
    protected TerminalTrie expectedTrie;
    protected static final ThreadLocal<TerminalTrie[]> TRIE_ARRAY_2 =
            new ThreadLocal<TerminalTrie[]>() {

                protected TerminalTrie[] initialValue() {
                    return (new TerminalTrie[2]);
                }
            };

    public abstract Path<Derivation.Item.NonTerminal> parse(TerminalReader tr,
                                                            FirstKSetSupplier f,
                                                            boolean useProductionLookAhead,
                                                            boolean infixParsing,
                                                            Writer gw) throws
            IOException;

    public synchronized Path<Derivation.Item.NonTerminal> parse(
            TerminalReader tr,
                                                                Grammar g,
                                                                boolean useProductionLookAhead,
                                                                boolean infix_parsing,
                                                                Writer gw)
            throws IOException {
        if(g == null)
            throw (new NullPointerException("Null grammar"));
        shifted_symbols.set(0);
        derivation_nodes_created.set(0);
        derivation_edges_created.set(0);
        nodes_created.set(0);
        shift_edges_created.set(0);
        shift_edges_visited.set(0);
        reduce_edges_created.set(0);
        reduce_edges_visited.set(0);
        Path<Derivation.Item.NonTerminal> r = parse(tr, g.fkss,
                                                    useProductionLookAhead,
                                                    infixParsing, gw);
        if(expectedTrie!=null&&!expectedTrie.isEmpty()) {
            if(expected!=null)
                expected.clear();
            else
                expected = new BBTree<Terminal>();
            for(Path<Terminal> p:expectedTrie.cloneBounded(1)) {
                if(!p.isEmpty())
                    expected.add(p.first());
                else
                    expected.add(Terminal.Epsilon);
            }

        }
        return (r);
    }
}
