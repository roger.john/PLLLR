/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package plllr;

import bbtree.*;
import bbtree.Utils.AsymmetricComparable;
import bbtree.Utils.KeyedElementFactory;
import java.io.IOException;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.Iterator;

/**
 *
 * @author RJ
 */
public class FirstKSetSupplier implements Writeable, Serializable {

    private static final long serialVersionUID = 0L;
    public final int k;
    public final Grammar g;
    private transient boolean changed = false;
    private transient int round;
    OrderedSet<NonTerminalFirstSetCollection> nfscc;
    OrderedSet<FirstSetOf<Terminal>> tfsc;
    private transient final BBTreeMR<FirstSetOf<Path<Token>>> pfsc = new BBTreeMR<FirstSetOf<Path<Token>>>();
    private transient final BBTreeMR<TerminalTrie> lfsc = new BBTreeMR<TerminalTrie>();
    private transient final BBTreeMR<LeftOp> cTable = new BBTreeMR<LeftOp>(),
            uTable = new BBTreeMR<LeftOp>();
    ControllableExecutor ex;

    public FirstKSetSupplier(Grammar g, int k) {
        this(g, k, null);
    }

    public FirstKSetSupplier(Grammar g, int k, ControllableExecutor ex) {
        if(g == null)
            throw (new NullPointerException(getClass().getCanonicalName()
                                            + ".constructor: grammar is null"));
        if(k < 0)
            throw (new IllegalArgumentException(
                    getClass().getCanonicalName()
                    + ".constructor: bound is negative"));
        this.g = g;
        this.k = k;
        this.ex = ex;
        calcFirstSets();
    }

    private void calcFirstSets() {
        if((g.P == null) || g.P.isEmpty())
            return;
        long st = System.nanoTime();
        int i, j;
        changed = true;
        boolean exStarted;
        NonTerminalFirstSetCollection[] t1 = new NonTerminalFirstSetCollection[g.N.
                size()];
        FirstSetOf<Terminal> fse;
        java.util.Iterator<NonTerminal> it = g.N.iterator();
        for(i = 0; it.hasNext(); i++) {
            NonTerminal x = it.next();
            t1[i] = new NonTerminalFirstSetCollection(x);
            t1[i].firstSet = new TerminalTrie();
            if(x.isNullable())
                t1[i].firstSet.add(Terminal.Epsilon);
            t1[i].rfsc = new IBBTree<RuleWithFirstSets>();
        }
        nfscc = new Final<NonTerminalFirstSetCollection>(t1, null, true);
        tfsc = new BBTree<FirstSetOf<Terminal>>();
        TerminalTrie tt = new TerminalTrie();
        tt.add((Terminal)null);
        tt.makeFinal();
        fse = new FirstSetOf<Terminal>(Terminal.Epsilon, tt);
        tfsc.add(fse);
        if(k > 0) {
            tt = new TerminalTrie();
            tt.add(Terminal.End);
            tt.makeFinal();
        }
        tfsc.add(new FirstSetOf<Terminal>(Terminal.End, tt));
        FirstSetOf.Factory<Terminal> fsf = new FirstSetOf.Factory<Terminal>();
        for(Rule r : g.P) {
            NonTerminalFirstSetCollection x = nfscc.getMatch(r.lhs);
            if(x.owner.rules == null)
                x.owner.rules = new IBBTree<Rule>();
            if(!x.owner.rules.contains(r)) {
                if(x.owner.rules.isFinal())
                    throw new RuntimeException("this should not happen");
                x.owner.rules.add(r);
            }
            RuleWithFirstSets rfs = x.rfsc.addOrGet(new RuleWithFirstSets(r));
            if(rfs.firstSet == null)
                rfs.firstSet = new TerminalTrie();
            else
                throw new RuntimeException("this should not happen");
            if(r.isNullable())
                rfs.firstSet.add(Terminal.Epsilon);
            if(r.lhs != null)
                rfs.lhs = nfscc.getMatch(r.lhs);
            if(r.rhsLength() > 0) {
                rfs.rhs = (FirstSetOf<? extends Token>[])java.lang.reflect.Array.
                        newInstance(
                        fse.getClass(), r.rhs.length);
                for(i = 0; i < r.rhs.length; i++)
                    if((r.rhs[i] == null) || (r.rhs[i] == Terminal.Epsilon))
                        rfs.rhs[i] = fse;
                    else if(r.rhs[i] instanceof Terminal) {
                        rfs.rhs[i] = tfsc.addOrGetMatch(
                                (Terminal)(r.rhs[i]), fsf);
                        if(rfs.rhs[i] == fsf.last)
                            if(k > 0) {
                                rfs.rhs[i].firstSet = new TerminalTrie();
                                rfs.rhs[i].firstSet.add(
                                        (Terminal)(r.rhs[i]));
                                rfs.rhs[i].makeFinal();
                            } else
                                rfs.rhs[i].firstSet = fse.firstSet;
                    } else if(r.rhs[i] instanceof NonTerminal)
                        rfs.rhs[i] = nfscc.getMatch(
                                (NonTerminal)(r.rhs[i]));
                    else
                        throw (new RuntimeException("unknown token: "
                                                    + r.rhs[i]));
            }
        }
        for(NonTerminalFirstSetCollection x : nfscc)
            x.rfsc = x.rfsc.toFinal();
        tfsc = tfsc.toFinal();
        round = 0;
        if(ex == null) {
            CalculatorTask ct = new CalculatorTask(this, null);
            CollectorTask cl = new CollectorTask(this, null);
            while(changed) {
                changed = false;
                System.out.print("\rCalculating first" + k + "-sets..." + round
                                 + " round" + (round != 1 ? "s" : "")
                                 + " passed...-");
                for(NonTerminalFirstSetCollection nfsc : nfscc)
                    for(RuleWithFirstSets rfs : nfsc.rfsc) {
                        ct.rfs = rfs;
                        ct.run();
                    }
                System.out.print("\rCalculating first" + k + "-sets..." + round
                                 + " round" + (round != 1 ? "s" : "")
                                 + " passed...|");
                for(NonTerminalFirstSetCollection nfsc : nfscc) {
                    cl.nfsc = nfsc;
                    cl.run();
                }
                round++;
            }
        } else {
            CalculatorTask[][] cats = new CalculatorTask[nfscc.size()][];
            CollectorTask[] cots = new CollectorTask[nfscc.size()];
            i = 0;
            for(NonTerminalFirstSetCollection nfsc : nfscc) {
                cats[i] = new CalculatorTask[nfsc.rfsc.size()];
                j = 0;
                for(RuleWithFirstSets rfs : nfsc.rfsc)
                    cats[i][j++] = new CalculatorTask(this, rfs);
                cots[i++] = new CollectorTask(this, nfsc);
            }
            exStarted = ex.isStarted();
            if(exStarted)
                ex.stop();
            while(changed) {
                changed = false;
                System.out.print("\rCalculating first" + k + "-sets..." + round
                                 + " round" + (round != 1 ? "s" : "")
                                 + " passed...-");
//                for(NonTerminalFirstSetCollection nfsc:nfscc)
//                    for(FirstSetOf<Rule> rfs:nfsc.rfsc)
//                        ex.execute(new CalculatorTask(nfsc,rfs));
                for(i = 0; i < cats.length; i++)
                    for(j = 0; j < cats[i].length; j++)
                        ex.execute(cats[i][j]);
                ex.start();
                ex.awaitCompletion();
                ex.stop();
                System.out.print("\rCalculating first" + k + "-sets..." + round
                                 + " round" + (round != 1 ? "s" : "")
                                 + " passed...|");
//                for(NonTerminalFirstSetCollection nfsc:nfscc)
//                    ex.execute(new CollectorTask(nfsc));
                for(i = 0; i < cots.length; i++)
                    ex.execute(cots[i]);
                ex.start();
                ex.awaitCompletion();
                ex.stop();
                round++;
            }
            if(exStarted)
                ex.start();
        }
        st = System.nanoTime() - st;
        System.out.format("\rCalculating first%d-sets...done after "
                          + "%d round%s(%fs).\r\n", k, round, (round != 1 ? "s"
                                                               : ""),
                          st
                          / 1000000000.0);
        makeFinal();
    }

    void makeFinal() {
        System.out.print("Finalizing first" + k + "-sets...");
        long st = System.nanoTime();
        if(ex != null) {
            boolean exStarted = ex.isStarted();
            if(exStarted)
                ex.stop();
            for(NonTerminalFirstSetCollection nfsc : nfscc) {
                ex.execute(new FinalizerTask(nfsc.firstSet));
                for(RuleWithFirstSets rfs : nfsc.rfsc)
                    ex.execute(new FinalizerTask(rfs.firstSet));
            }
            ex.start();
            ex.awaitCompletion();
            ex.stop();
            if(exStarted)
                ex.start();
        } else
            for(NonTerminalFirstSetCollection nfsc : nfscc)
                nfsc.makeFinal();
        st = System.nanoTime() - st;
        System.out.format("done(%fs).\r\n", st / 1000000000.0);
    }

    void reconstruct() {
        System.out.print("Reconstructing first" + k + "-sets...");
        long st = System.nanoTime();
        if(!tfsc.isFinal())
            tfsc = tfsc.toFinal();
        if(!nfscc.isFinal())
            nfscc = nfscc.toFinal();
        if(ex != null) {
            for(NonTerminalFirstSetCollection nfsc : nfscc)
                ex.execute(new ReconstructionTask(nfsc));
            ex.awaitCompletion();
        } else
            for(NonTerminalFirstSetCollection nfsc : nfscc) {
                if(!nfsc.rfsc.isFinal())
                    nfsc.rfsc = nfsc.rfsc.toFinal();
                if((nfsc.firstSet != null) && nfsc.firstSet.isFinal())
                    continue;
                nfsc.firstSet = new TerminalTrie();
                if(nfsc.owner.isNullable())
                    nfsc.firstSet.add(Terminal.Epsilon);
                for(RuleWithFirstSets rfs : nfsc.rfsc) {
                    if(nfsc.owner.rules == null || !nfsc.owner.rules.contains(
                            rfs.owner)) {
                        if(nfsc.owner.rules == null)
                            nfsc.owner.rules = new IBBTree<Rule>();
                        else if(nfsc.owner.rules.isFinal())
                            nfsc.owner.rules = nfsc.owner.rules.clone();
                        nfsc.owner.rules.add(rfs.owner);
                    }
                    nfsc.firstSet.addAll(rfs.firstSet);
                }
                if(nfsc.owner.rules != null)
                    nfsc.owner.rules = nfsc.owner.rules.toFinal();
                nfsc.firstSet.makeFinal();
                if(nfsc.firstSet.heads.contains(TerminalTrie.EndNode))
                    nfsc.owner.setNullable(true);
            }
        st = System.nanoTime() - st;
        System.out.format("done(%fs).\r\n", st / 1000000000.0);
    }

    public NonTerminalFirstSetCollection getFirstSetCollectionFor(NonTerminal x) {
        if(x == null)
            return (null);
        return (nfscc.getMatch(x));
    }

    public TerminalTrie firstKSetOf(Terminal x) {
        if(x == null)
            return (null);
        FirstSetOf<Terminal> fst = tfsc.getMatch((Terminal)x);
        if(fst != null)
            return (fst.firstSet);
        else
            throw (new RuntimeException("Unknow terminal: " + x));
    }

    public TerminalTrie firstKSetOf(NonTerminal x) {
        if(x == null)
            return (null);
        NonTerminalFirstSetCollection y = nfscc.getMatch(x);
        if(y == null)
            return (null);
        return (y.firstSet);
    }

    public TerminalTrie firstKSetOf(Rule r) {
        if((r == null) || (r.lhs == null))
            return (null);
        NonTerminalFirstSetCollection y = nfscc.getMatch(r.lhs);
        if(y == null)
            return (null);
        FirstSetOf<Rule> rfs = y.rfsc.getMatch(r);
        if(rfs == null)
            return (null);
        return (rfs.firstSet);
    }

    private TerminalTrie firstKSetOf(Token t) {
        if(t == null)
            return (null);
        if(t instanceof NonTerminal)
            return (firstKSetOf((NonTerminal)t));
        TerminalTrie r;
        if(t instanceof Terminal) {
//            r = new TerminalTrie();
//            if(t==Terminal.Epsilon)
//                r.insertPath(null);
//            else
//                r.insertPath(new Path<Terminal>().append((Terminal)t));
//            return(r);
            FirstSetOf<Terminal> fst = tfsc.getMatch((Terminal)t);
            if(fst != null)
                return (fst.firstSet);
            else
                throw (new RuntimeException("Unknow terminal: " + t));
        }
        if(t instanceof Token.Optional) {
            r = firstKSetOf(((Token.Optional)t).option);
            r.add(Terminal.Epsilon);
            return (r);
        }
        if(t instanceof Token.Group) {
            Token[] members = ((Token.Group)t).members;
            if((members == null) || (members.length == 0)) {
                r = new TerminalTrie();
                r.add(Terminal.Epsilon);
            } else {
                TerminalTrie[] m = new TerminalTrie[members.length];
                for(int i = 0; i < m.length; i++)
                    m[i] = firstKSetOf(members[i]);
                r = TerminalTrie.BoundedConcatenation(m, k);
            }
            return (r);
        }
        if(t instanceof Token.Alternation) {
            Token[] members = ((Token.Alternation)t).options;
            if((members == null) || (members.length == 0)) {
                r = new TerminalTrie();
                r.add(Terminal.Epsilon);
            } else {
                r = firstKSetOf(members[0]);
                for(int i = 1; i < members.length; i++)
                    r.addAllBounded(firstKSetOf(members[i]), k);
            }
            return (r);
        }
        if(t instanceof Token.Plussed) {
            Token x = ((Token.Plussed)t).x;
            if(x == null)
                return (null);
            r = firstKSetOf(x);
            r.applyBoundedPlus(k);
            return (r);
        }
        if(t instanceof Token.Starred) {
            Token x = ((Token.Starred)t).x;
            if(x == null)
                return (null);
            r = firstKSetOf(x);
            r.applyBoundedStar(k);
            return (r);
        }
        throw (new RuntimeException("Unknow Token type: " + t));
    }

    public TerminalTrie firstKSetOf(Path<Token> p) {
        if((p == null) || p.isEmpty())
            return (firstKSetOf(Terminal.Epsilon));
        KeyedElementFactory<FirstSetOf<Path<Token>>, Path<Token>> tfsf = PATH_FIRSTSET_CREATOR.
                get().mainFactory;
        FirstSetOf<Path<Token>> a = pfsc.addOrGetMatch(p, tfsf);
        synchronized(a) {
            if(a.firstSet == null) {
                if(p.isSingle())
                    a.firstSet = firstKSetOf(p.head.x);
                else {
                    TerminalTrie[] m = new TerminalTrie[p.length()];
                    int i = 0;
                    for(Path.Node<Token> x = p.head; x != null; x = x.next)
                        m[i++] = firstKSetOf(x.x);
                    a.firstSet = TerminalTrie.BoundedConcatenation(m, k);
                }
                a.firstSet.makeFinal();
            }
        }
        return a.firstSet;
    }

    public TerminalTrie firstKSetOf(Path.Node<Token> p) {
        if(p == null)
            return firstKSetOf(Terminal.Epsilon);
        KeyedElementFactory<FirstSetOf<Path<Token>>, Object> tfsf = PATH_FIRSTSET_CREATOR.
                get();
        FirstSetOf<Path<Token>> a = pfsc.addOrGetMatch(p, tfsf);
        synchronized(a) {
            if(a.firstSet == null) {
                if(p.next == null)
                    a.firstSet = firstKSetOf(p.x);
                else {
                    int l = 0;
                    for(Path.Node<Token> x = p; x != null; x = x.next)
                        l++;
                    TerminalTrie[] m = new TerminalTrie[l];
                    int i = 0;
                    for(Path.Node<Token> x = p; x != null; x = x.next)
                        m[i++] = firstKSetOf(x.x);
                    a.firstSet = TerminalTrie.BoundedConcatenation(m, k);
                }
                a.firstSet.makeFinal();
            }
        }
        return (a.firstSet);
    }

    public boolean derivesOnlyEpsilon(NonTerminal d) {
        if(d == null || !d.isNullable() || k == 0)
            return false;
        return firstKSetOf(d).containsOnlyEmptyPath();
    }
    private static final ThreadLocal<KeyedElementFactory.Cascaded<FirstSetOf<Path<Token>>, Path<Token>, Object>> PATH_FIRSTSET_CREATOR = new ThreadLocal<KeyedElementFactory.Cascaded<FirstSetOf<Path<Token>>, Path<Token>, Object>>() {

        protected KeyedElementFactory.Cascaded<FirstSetOf<Path<Token>>, Path<Token>, Object> initialValue() {
            return new KeyedElementFactory.Cascaded<FirstSetOf<Path<Token>>, Path<Token>, Object>(
                    new FirstSetOf.Factory<Path<Token>>(),
                    new Path.Factory<Token>());
        }
    };

    private static class CalculatorTask implements Runnable {

        public final FirstKSetSupplier fkss;
        public RuleWithFirstSets rfs;

        public CalculatorTask(FirstKSetSupplier fkss, RuleWithFirstSets rfs) {
            if(fkss == null)
                throw (new NullPointerException("FirstKSetSupplier is null"));
            this.fkss = fkss;
            this.rfs = rfs;
        }

        public void run() {
            if(rfs == null)
                throw (new NullPointerException("Null parameter"));
            if(rfs.owner.rhsLength() == 0) {
                if(fkss.round == 0)
                    if(rfs.firstSet.add(Terminal.Epsilon))
                        rfs.changedInRound = fkss.round;
                return;
            }
            boolean mustRecalc = false;
            FirstSetOf<?> y;
            for(int i = 0; i < rfs.rhs.length; i++) {
                y = rfs.rhs[i];
                if((y == null) || (y.firstSet == null) || y.firstSet.isEmpty())
                    return;
                if(y.firstSet == rfs.firstSet)
                    throw (new IllegalArgumentException("Cycle detected"));
                if(y.changedInRound == fkss.round - 1)
                    mustRecalc = true;
            }
            if(mustRecalc && rfs.firstSet.addBoundedConcatenationOfFirstSets(
                    rfs.rhs, fkss.k))
                rfs.changedInRound = fkss.round;
        }
    }

    private static class CollectorTask implements Runnable,
                                                  TraversalActionPerformer<FirstSetOf<Rule>> {

        public final FirstKSetSupplier fkss;
        public NonTerminalFirstSetCollection nfsc;

        public CollectorTask(FirstKSetSupplier fkss,
                             NonTerminalFirstSetCollection t) {
            if(fkss == null)
                throw (new NullPointerException("FirstKSetSupplier is null"));
            this.fkss = fkss;
            this.nfsc = t;
        }

        public void run() {
            nfsc.rfsc.traverse(IN_ORDER, this);
        }

        public boolean perform(FirstSetOf<Rule> rfs, int order, Object o) {
            if(rfs.changedInRound < fkss.round)
                return (false);
            if(nfsc.firstSet.addAll(rfs.firstSet)) {
                nfsc.changedInRound = fkss.round;
                fkss.changed = true;
            }
            return (false);
        }
    }

    private class FinalizerTask implements Runnable {

        public TerminalTrie x;

        public FinalizerTask(TerminalTrie x) {
            this.x = x;
        }

        public void run() {
            if(x != null)
                x.makeFinal();
        }
    }

    private class ReconstructionTask implements Runnable,
                                                TraversalActionPerformer<FirstSetOf<Rule>> {

        public NonTerminalFirstSetCollection nfsc;

        public ReconstructionTask(NonTerminalFirstSetCollection t) {
            this.nfsc = t;
        }

        public void run() {
            if(!nfsc.rfsc.isFinal())
                nfsc.rfsc = nfsc.rfsc.toFinal();
            if((nfsc.firstSet != null) && nfsc.firstSet.isFinal())
                return;
            nfsc.firstSet = new TerminalTrie();
            if(nfsc.owner.isNullable())
                nfsc.firstSet.add(Terminal.Epsilon);
            nfsc.rfsc.traverse(IN_ORDER, this);
            if(nfsc.owner.rules != null)
                nfsc.owner.rules = nfsc.owner.rules.toFinal();
            nfsc.firstSet.makeFinal();
            if(nfsc.firstSet.heads.contains(TerminalTrie.EndNode))
                nfsc.owner.setNullable(true);
        }

        public boolean perform(FirstSetOf<Rule> rfs, int order, Object o) {
            if(nfsc.owner.rules == null || !nfsc.owner.rules.contains(
                    rfs.owner)) {
                if(nfsc.owner.rules == null)
                    nfsc.owner.rules = new IBBTree<Rule>();
                else if(nfsc.owner.rules.isFinal())
                    nfsc.owner.rules = nfsc.owner.rules.clone();
                nfsc.owner.rules.add(rfs.owner);
            }
            nfsc.firstSet.addAll(rfs.firstSet);
            return false;
        }
    }

    public static class NonTerminalFirstSetCollection
            extends FirstSetOf<NonTerminal> {

        private static final long serialVersionUID = 0L;
        public Traversable.Set.Indexed<RuleWithFirstSets> rfsc;

        public NonTerminalFirstSetCollection(NonTerminal nt) {
            super(nt);
        }

        public void makeFinal() {
            super.makeFinal();
            if(owner.rules != null)
                owner.rules = owner.rules.toFinal();
            if((rfsc != null) && !rfsc.isFinal())
                rfsc = rfsc.toFinal();
            for(FirstSetOf<Rule> rfs : rfsc)
                if(!rfs.isFinal())
                    rfs.makeFinal();
        }

        public static class Factory implements
                KeyedElementFactory<NonTerminalFirstSetCollection, NonTerminal> {

            public NonTerminalFirstSetCollection last;

            public void reset() {
                last = null;
            }

            public NonTerminalFirstSetCollection lastCreated() {
                return (last);
            }

            public NonTerminalFirstSetCollection createElement(NonTerminal x) {
                if(x == null)
                    throw (new NullPointerException("Null argument"));
                last = new NonTerminalFirstSetCollection(x);
                return (last);
            }

            @Override
            public NonTerminal extractKey(NonTerminalFirstSetCollection x) {
                return x.owner;
            }
        }
        public static final ThreadLocal<Factory> FACTORY = new ThreadLocal<Factory>() {

            protected Factory initialValue() {
                return (new Factory());
            }
        };
    }

    public static class RuleWithFirstSets extends FirstSetOf<Rule> {

        public FirstSetOf<NonTerminal> lhs;
        public FirstSetOf<? extends Token>[] rhs;

        public RuleWithFirstSets(Rule r) {
            super(r);
        }

        public void makeFinal() {
            super.makeFinal();
            if(!lhs.isFinal())
                lhs.makeFinal();
            if(rhs != null)
                for(FirstSetOf<? extends Token> tfs : rhs)
                    if(!tfs.isFinal())
                        tfs.makeFinal();
        }

        public static class Factory implements
                KeyedElementFactory<RuleWithFirstSets, Rule> {

            public RuleWithFirstSets last;

            public void reset() {
                last = null;
            }

            public RuleWithFirstSets lastCreated() {
                return (last);
            }

            public RuleWithFirstSets createElement(Rule x) {
                if(x == null)
                    throw (new NullPointerException("Null argument"));
                last = new RuleWithFirstSets(x);
                return (last);
            }

            @Override
            public Rule extractKey(RuleWithFirstSets x) {
                return x.owner;
            }
        }
        public static final ThreadLocal<Factory> FACTORY = new ThreadLocal<Factory>() {

            protected Factory initialValue() {
                return (new Factory());
            }
        };
    }

    public void writeToStream(java.io.Writer w) throws java.io.IOException {
        w.write("[First" + k + "Set]\r\n");
        if((g != null) && (g.S != null)) {
            w.write("[start ");
            w.write(g.S.toString());
            w.write("]\r\n");
        }
        if(g != null) {
            w.write("[lexer ");
            w.write(g.getLexer().getName());
            w.write("]\r\n");
        }
        if((g != null) && (g.N != null)) {
            w.write("{");
            Iterator<NonTerminal> it = g.N.subSetIterator(Grammar.NULLABLES);
            NonTerminal t;
            if(it.hasNext()) {
                w.write('(');
                t = it.next();
                w.write(t.toString());
                w.write(',');
                if(t.getNullingRule() != null)
                    w.write(t.getNullingRule().id + "");
                w.write(')');
                while(it.hasNext()) {
                    w.write(',');
                    w.write('(');
                    t = it.next();
                    w.write(t.toString());
                    w.write(',');
                    if(t.getNullingRule() != null)
                        w.write(t.getNullingRule().id + "");
                    w.write(')');
                }
            }
            w.write("}\r\n");
        }
        for(NonTerminalFirstSetCollection nfsc : nfscc)
            if(nfsc.rfsc != null)
                for(FirstSetOf<Rule> rfs : nfsc.rfsc) {
//                    w.write('\t');
                    rfs.owner.writeToStream(w);
                    w.write(' ');
                    rfs.firstSet.writeToStream(w);
                    w.write("\r\n");
                }
    }

    public String toString() {
        return toStringBuilder(null).toString();
    }

    public StringBuilder toStringBuilder(StringBuilder sb) {
        if(sb == null)
            sb = new StringBuilder();
        StringWriter sw = new StringWriter();
        try {
            writeToStream(sw);
        } catch(IOException e) {
            throw new RuntimeException("impossible error", e);
        }
        return sb.append(sw.toString());
    }

    public void writeExpandedToStream(java.io.Writer w) throws
            java.io.IOException {
        w.write("[First" + k + "Set]\r\n");
        if((g != null) && (g.S != null)) {
            w.write("[start ");
            w.write(g.S.toString());
            w.write("]\r\n");
        }
        if(g != null) {
            w.write("[lexer ");
            w.write(g.getLexer().getName());
            w.write("]\r\n");
        }
        if((g != null) && (g.N != null)) {
            w.write("{");
            Iterator<NonTerminal> it = g.N.subSetIterator(Grammar.NULLABLES);
            NonTerminal t;
            if(it.hasNext()) {
                w.write('(');
                t = it.next();
                w.write(t.toString());
                w.write(',');
                if(t.getNullingRule() != null)
                    w.write(t.getNullingRule().id + "");
                w.write(')');
                while(it.hasNext()) {
                    w.write(',');
                    w.write('(');
                    t = it.next();
                    w.write(t.toString());
                    w.write(',');
                    if(t.getNullingRule() != null)
                        w.write(t.getNullingRule().id + "");
                    w.write(')');
                }
            }
            w.write("}\r\n");
        }
        for(NonTerminalFirstSetCollection nfsc : nfscc)
            if(nfsc.rfsc != null)
                for(FirstSetOf<Rule> rfs : nfsc.rfsc) {
//                    w.write('\t');
                    rfs.owner.writeToStream(w);
                    w.write(' ');
                    rfs.firstSet.writeExpandedToStream(w);
                    w.write("\r\n");
                }
    }

    private static class LeftOp implements Comparable<LeftOp>,
                                           AsymmetricComparable<TerminalTrie> {

        private final TerminalTrie leftOp;
        private final BBTreeMR<RightOp> rightOps =
                new BBTreeMR<RightOp>();

        public LeftOp(TerminalTrie x) {
            if(x == null)
                throw (new NullPointerException("Left operand is null"));
            leftOp = x;
        }

        public int compareTo(LeftOp o) {
            if(o == null)
                return (1);
            return (Utils.HashCodeComparator.compare(leftOp, o.leftOp));
        }

        public int asymmetricCompareTo(TerminalTrie o) {
            return (Utils.HashCodeComparator.compare(leftOp, o));
        }

        public static class Factory implements
                KeyedElementFactory<LeftOp, TerminalTrie> {

            public LeftOp last;

            public LeftOp createElement(TerminalTrie x) {
                last = new LeftOp(x);
                return (last);
            }

            public LeftOp lastCreated() {
                return (last);
            }

            public void reset() {
                last = null;
            }

            @Override
            public TerminalTrie extractKey(LeftOp x) {
                return x.leftOp;
            }
            public static final ThreadLocal<Factory> TLV = new ThreadLocal<Factory>() {

                protected Factory initialValue() {
                    return (new Factory());
                }
            };
        }
    }

    private static class RightOp implements Comparable<RightOp>,
                                            AsymmetricComparable<TerminalTrie> {

        private final TerminalTrie rightOp;
        private TerminalTrie result;

        public RightOp(TerminalTrie x) {
            if(x == null)
                throw (new NullPointerException("Right operand is null"));
            rightOp = x;
        }

        public int compareTo(RightOp o) {
            if(o == null)
                return (1);
            return (Utils.HashCodeComparator.compare(rightOp, o.rightOp));
        }

        public int asymmetricCompareTo(TerminalTrie o) {
            return (Utils.HashCodeComparator.compare(rightOp, o));
        }

        public static class Factory implements
                KeyedElementFactory<RightOp, TerminalTrie> {

            public RightOp last;

            public RightOp createElement(TerminalTrie x) {
                last = new RightOp(x);
                return (last);
            }

            public RightOp lastCreated() {
                return (last);
            }

            public void reset() {
                last = null;
            }
            @Override
            public TerminalTrie extractKey(RightOp x) {
                return x.rightOp;
            }
            public static final ThreadLocal<Factory> TLV = new ThreadLocal<Factory>() {

                protected Factory initialValue() {
                    return (new Factory());
                }
            };
        }
    }

    public TerminalTrie getUnion(TerminalTrie x, TerminalTrie y) {
        if((x == null) || (y == null))
            throw (new NullPointerException());
        LeftOp l = uTable.addOrGetMatch(x, LeftOp.Factory.TLV.get());
        RightOp r = l.rightOps.addOrGetMatch(y, RightOp.Factory.TLV.get());
        synchronized(r) {
            if(r.result == null) {
                TerminalTrie z = x.clone();
                z.addAll(y);
                r.result = lfsc.addOrGet(z);
                if(z == r.result)
                    z.makeFinal();
            }
        }
        return (r.result);
    }
    private static final ThreadLocal<TerminalTrie[]> TRIE_ARRAY_2 =
            new ThreadLocal<TerminalTrie[]>() {

                protected TerminalTrie[] initialValue() {
                    return (new TerminalTrie[2]);
                }
            };

    public TerminalTrie getConcatenation(TerminalTrie x, TerminalTrie y) {
        if((x == null) || (y == null))
            throw (new NullPointerException());
        LeftOp l = cTable.addOrGetMatch(x, LeftOp.Factory.TLV.get());
        RightOp r = l.rightOps.addOrGetMatch(y, RightOp.Factory.TLV.get());
        synchronized(r) {
            if(r.result == null) {
                TerminalTrie[] ta2 = TRIE_ARRAY_2.get();
                ta2[0] = x;
                ta2[1] = y;
                TerminalTrie z = new TerminalTrie();
                z.addBoundedConcatenation(ta2, k);
                r.result = lfsc.addOrGet(z);
                if(z == r.result)
                    z.makeFinal();
            }
        }
        return (r.result);
    }
}
