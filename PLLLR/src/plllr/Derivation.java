/*
 * Derivation.java
 *
 * Created on 15. Oktober 2007, 15:27
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */
package plllr;

import bbtree.*;
import java.io.IOException;
import java.io.Serializable;
import java.io.Writer;
import java.util.Iterator;

/**
 *
 * @author RJ
 */
public abstract class Derivation {

    public static final Character NP = new Character('#'),
            RP = new Character('+'),
            RE = new Character('*'),
            NO = new Character('>'),
            IN = new Character(' '),
            IT = new Character(' ');

    private static String toPrintString(Path<Character> d) {
        if(d == null)
            return ("");
        StringBuilder r = new StringBuilder();
        Path.Node<Character> t = d.head;
        while(t != null) {
            r.append(t.x.charValue());
            t = t.next;
        }
        return (r.reverse().toString());
    }

    static String toHexString(int n) {
        StringBuilder sb = new StringBuilder();
        String t = Integer.toHexString(n).toUpperCase();
        int j = 8 - t.length();
        while(j-- > 0)
            sb.append('0');
        sb.append(t);
        return (sb.toString());
    }

    static String toHexString(long n) {
        StringBuilder sb = new StringBuilder();
        String t = Long.toHexString(n).toUpperCase();
        int j = 16 - t.length();
        while(j-- > 0)
            sb.append('0');
        sb.append(t);
        return (sb.toString());
    }

    private static String formatRule(Rule r) {
        if(r == null)
            return ("");
        StringBuilder sb = new StringBuilder();
        if(r.lhs == null)
            sb.append("<null>");
        else
            sb.append(r.lhs.getEscapedName());
        sb.append(" =>");
        if(r.rhs != null)
            for(int i = 0; i < r.rhs.length; i++) {
                if(r.rhs[i] == null)
                    continue;
                sb.append(' ');
                if(r.rhs[i] instanceof plllr.NonTerminal)
                    sb.append(((plllr.NonTerminal)(r.rhs[i])).getEscapedName());
                else
                    sb.append(Terminal.String.escape(r.rhs[i].toString()));
            }
        return (sb.toString());
    }

    public static void writeEpsilonGraph_old(Grammar G, Writer w, String name,
                                             boolean nodeItemLinks,
                                             boolean topDown) throws IOException {
        if(w == null)
            return;
        if((name == null) || name.length() == 0)
            name = "EpsilonGraph" + toHexString(System.currentTimeMillis());
        w.write("digraph ");
        w.write(name);
        w.write(" {\r\n");
        w.write("\r\n");
        if(!topDown) {
            w.write("\trankdir=LR;\r\n");
            w.write("\r\n");
            w.write("\trank=same {\r\n");
        }
        if(G != null) {
            if(G.N != null)
                for(NonTerminal x : G.N)
                    if((x != null) && x.isNullable()) {
                        w.write("\t\ti");
                        w.write(toHexString(System.identityHashCode(x)));
                        w.write("[shape=box,label=\"");
                        w.write(x.getEscapedName());
                        w.write('\"');
                        if(x == G.S)
                            w.write(",color=green,style=filled");
                        w.write("];\r\n");
                    }
            if(!topDown)
                w.write("\t}\r\n");
            w.write("\r\n");
            if(G.P != null)
                for(Rule r : G.P)
                    if((r != null) && r.isNullable() && (r.rhsLength() > 0)) {
                        String lastNode = "i" + toHexString(System.
                                identityHashCode(r.lhs)),
                                currentNode = null;
                        for(int i = 0; i < r.rhs.length; i++) {
                            Token x = r.rhs[i];
                            currentNode = "n" + toHexString(System.
                                    identityHashCode(r))
                                          + toHexString(i);
                            w.write("\t");
                            w.write(currentNode);
                            w.write("[shape=ellipse,label=\"");
                            w.write(((plllr.NonTerminal)x).getEscapedName());
                            w.write("\"];\r\n");
                            w.write("\t");
                            w.write(lastNode);
                            w.write("->");
                            w.write(currentNode);
                            if(lastNode.charAt(0) == 'i') {
                                w.write("[label=\"R");
                                w.write(String.valueOf(r.id));
                                w.write("\",tooltip=\"");
                                w.write(formatRule(r));
                                w.write("\"]");
                            }
                            w.write(";\r\n");
                            if(nodeItemLinks) {
                                w.write("\t");
                                w.write(currentNode);
                                w.write("->i");
                                w.write(toHexString(System.identityHashCode(x)));
                                w.write("[");
                                if(!topDown)
                                    w.write("weight=0,");
                                w.write("style=dotted];\r\n");
                            }
                            lastNode = currentNode;
                        }
                    }
        }
        w.write("\r\n");
        w.write("}\r\n");
        w.flush();
        w.close();

    }

    public static void writeEpsilonGraph(Grammar G, Writer w, String name,
                                         boolean nodeItemLinks, boolean topDown)
            throws IOException {
        if(w == null)
            return;
        if((name == null) || name.length() == 0)
            name = "EpsilonGraph" + toHexString(System.currentTimeMillis());
        w.write("digraph ");
        w.write(name);
        w.write(" {\r\n");
        w.write("\r\n");
        if(!topDown) {
            w.write("\trankdir=LR;\r\n");
            w.write("\r\n");
            w.write("\trank=same {\r\n");
        }
        if(G != null) {
            if(G.N != null)
                for(NonTerminal x : G.N)
                    if((x != null) && x.isNullable()) {
                        w.write("\t\ti");
                        w.write(toHexString(System.identityHashCode(x)));
                        w.write("[shape=box,label=\"");
                        w.write(x.getEscapedName());
                        w.write('\"');
                        if(x == G.S)
                            w.write(",color=green,style=filled");
                        w.write("];\r\n");
                    }
            if(!topDown)
                w.write("\t}\r\n");
            w.write("\r\n");
            BBTree<Path<Token>> nodes = new BBTree<Path<Token>>();
            if(G.P != null)
                for(Rule r : G.P)
                    if((r != null) && r.isNullable() && (r.rhsLength() > 0)) {
                        String lastNode = "i" + toHexString(System.
                                identityHashCode(r.lhs)),
                                currentNode = null;
                        Path<Token> cn = Path.valueOf(r.rhs), tn;
                        boolean goOn = true;
                        while(goOn) {
                            if((tn = nodes.addOrGet(cn)) != cn) {
                                goOn = false;
                                cn = tn;
                            }
                            currentNode = "n" + toHexString(System.
                                    identityHashCode(cn));
                            if(goOn) {
                                w.write("\t");
                                w.write(currentNode);
                                w.write("[shape=ellipse,label=\"");
                                w.write(((plllr.NonTerminal)(cn.first())).
                                        getEscapedName());
                                w.write("\"];\r\n");
                                w.write("\t");
                            }
                            w.write(lastNode);
                            w.write("->");
                            w.write(currentNode);
                            if(lastNode.charAt(0) == 'i') {
                                w.write("[label=\"R");
                                w.write(String.valueOf(r.id));
                                w.write("\",tooltip=\"");
                                w.write(formatRule(r));
                                w.write("\"]");
                            }
                            w.write(";\r\n");
                            if(!goOn)
                                break;
                            if(nodeItemLinks) {
                                w.write("\t");
                                w.write(currentNode);
                                w.write("->i");
                                w.write(toHexString(System.identityHashCode(cn.
                                        first())));
                                w.write("[");
                                if(!topDown)
                                    w.write("weight=0,");
                                w.write("style=dotted];\r\n");
                            }
                            lastNode = currentNode;
                            cn = cn.shift();
                            if(cn.isEmpty())
                                goOn = false;
                        }
                    }
        }
        w.write("\r\n");
        w.write("}\r\n");
        w.flush();
        w.close();

    }

    public static void writeGraph(Derivation.Item S, Writer w, String name,
                                  boolean nodeItemLinks, boolean topDown) throws
            IOException {
        if(w == null)
            return;
        if((name == null) || name.length() == 0)
            name = "DerivationGraph" + toHexString(System.currentTimeMillis());
        Path<Derivation.Edge> e = new Path<Derivation.Edge>();
        Path<Derivation.Item.NonTerminal> i = new Path<Derivation.Item.NonTerminal>();
        Path<DistributionNode> v = new Path<DistributionNode>();
        int ic = 0, nc = 0, vc = 0, ec = 0;
        if(S != null) {
            S.unVisit();
            S.collectNodes(e, i, v);
            S.unVisit();
        }
        w.write("digraph ");
        w.write(name);
        w.write(" {\r\n");
        w.write("\r\n");
        if(!topDown) {
            w.write("\trankdir=LR;\r\n");
            w.write("\r\n");
            w.write("\trank=same {\r\n");
        }
        for(Derivation.Item.NonTerminal it : i) {
            ic++;
            if(it.lpos == it.rpos)
                continue;
            w.write("\t\ti");
            w.write(toHexString(System.identityHashCode(it)));
            w.write("[shape=box,label=\"");
            w.write(((plllr.NonTerminal)(it.x)).getEscapedName());
            w.write(',');
            w.write(String.valueOf(it.lpos));
            w.write(',');
            w.write(String.valueOf(it.rpos));
            w.write('\"');
            if(it == S)
                w.write(",color=green,style=filled");
            w.write("];\r\n");
        }
        if(!topDown)
            w.write("\t}\r\n");
        w.write("\r\n");
        for(DistributionNode vt : v) {
            vc++;
            w.write("\tv");
            w.write(toHexString(System.identityHashCode(vt)));
            if(vt instanceof TerminationNode) {
                w.write("[shape=ellipse,label=\"");
                Path.Node<Token> p = ((TerminationNode)vt).p;
                if(p != null) {
                    if(p.x instanceof NonTerminal)
                        w.write(((NonTerminal)(p.x)).getEscapedName());
                    else
                        w.write(p.x.toString());
                    p = p.next;
                    while(p != null) {
                        w.write(' ');
                        if(p.x instanceof NonTerminal)
                            w.write(((NonTerminal)(p.x)).getEscapedName());
                        else
                            w.write(p.x.toString());
                        p = p.next;
                    }
                }
                int pp = ((TerminationNode)(vt)).pos;
                w.write(',');
                w.write(String.valueOf(pp));
                w.write(',');
                w.write(String.valueOf(pp));
                w.write("\"];\r\n");
            } else
                w.write("[shape=point];\r\n");
        }
        w.write("\r\n");
        for(Derivation.Edge nt : e) {
            nc++;
            w.write("\tn");
            w.write(toHexString(System.identityHashCode(nt)));
            w.write("[shape=ellipse,label=\"");
            if(nt.item.x instanceof plllr.NonTerminal) {
                w.write(((plllr.NonTerminal)(nt.item.x)).getEscapedName());
                w.write(',');
                w.write(String.valueOf(nt.item.lpos));
                w.write(',');
                w.write(String.valueOf(nt.item.rpos));
//                int id = 2;
//                Path.Node<Derivation.Item.NonTerminal> item = i.head;
//                while((item!=null)&&(item.x!=nt.item)) {
//                    id++;
//                    item = item.next;
//                }
//                if(item!=null) {
//                    w.write("\",href=\"#node");
//                    w.write(String.valueOf(id));
//                }
            } else {
                w.write(Terminal.String.escape(nt.item.x.toString()));
                w.write(',');
                w.write(String.valueOf(nt.item.lpos));
                w.write(',');
                w.write(String.valueOf(nt.item.rpos));
            }
            w.write("\"];\r\n");
        }
        w.write("\r\n");
        for(Derivation.Item it : i) {
            if(!(it instanceof Derivation.Item.NonTerminal))
                continue;
            if(((Derivation.Item.NonTerminal)it).rules == null)
                continue;
            Path.Node<Derivation.RuleEdge> t = ((Derivation.Item.NonTerminal)it).rules.head;
            while(t != null) {
                ec++;
                w.write("\ti");
                w.write(toHexString(System.identityHashCode(it)));
                w.write("->v");
                w.write(toHexString(System.identityHashCode(t.x.s)));
                w.write("[label=\"R");
                w.write(String.valueOf(t.x.rule.id));
                w.write("\",tooltip=\"");
                w.write(formatRule(t.x.rule));
                w.write("\"];\r\n");
                t = t.next;
            }
        }
        w.write("\r\n");
        for(Derivation.Edge nt : e) {
            if(nt.s != null) {
                ec++;
                w.write("\tn");
                w.write(toHexString(System.identityHashCode(nt)));
                w.write("->v");
                w.write(toHexString(System.identityHashCode(nt.s)));
                w.write(";\r\n");
            }

            //Test NodeItemLinks - start

            if(nodeItemLinks && (nt.item != null)
               && (nt.item instanceof Derivation.Item.NonTerminal)
               && (nt.item.lpos < nt.item.rpos)) {
                w.write("\tn");
                w.write(toHexString(System.identityHashCode(nt)));
                w.write("->i");
                w.write(toHexString(System.identityHashCode(nt.item)));
                w.write("[");
                if(!topDown)
                    w.write("weight=0,");
                w.write("style=dotted];\r\n");
            }

            //Test NodeItemLinks - end

        }
        w.write("\r\n");
        for(DistributionNode vt : v) {
            Path.Node<Derivation.Edge> t = vt.head;
            while(t != null) {
                ec++;
                w.write("\tv");
                w.write(toHexString(System.identityHashCode(vt)));
                w.write("->n");
                w.write(toHexString(System.identityHashCode(t.x)));
                w.write(";\r\n");
                t = t.next;
            }
        }
        w.write("\r\n");
        w.write("}\r\n");
        w.flush();
        w.close();
    }

    public static class Edge implements Comparable<Edge> {

        private static final long serialVersionUID = 0L;
        public final Item item;
        public DistributionNode s;
        public transient boolean visited = false;

        public Edge(Item item) {
            if(item == null)
                throw new NullPointerException("item");
            this.item = item;
        }

        public Edge(Item item, DistributionNode s) {
            this(item);
            this.s = s;
        }

        public Edge(Item item, Edge next) {
            this(item);
            if(next != null) {
                this.s = new DistributionNode();
                this.s.append(next);
            }
        }

        public int compareTo(Edge other) {
            if(other == null)
                return (1);
            return ((item == null) ? (other.item == null) ? 0 : -1 : (other.item == null) ? 1 : item.
                    compareTo(other.item));
        }

        public String toString() {
            return ("(" + (item != null ? item.toString() : "null") + ")");
        }

        public boolean isAmbiguous() {
            if(visited)
                return (true);
            visited = true;
            if(item.isAmbiguous()) {
                visited = false;
                return (true);
            }
            if(s == null) {
                visited = false;
                return (false);
            }
            boolean amb = s.isAmbiguous();
            visited = false;
            return (amb);
        }

        public boolean isCyclic() {
            if(visited)
                return (true);
            visited = true;
            if(item.isCyclic()) {
                visited = false;
                return (true);
            }
            if(s == null) {
                visited = false;
                return (false);
            }
            boolean cyclic = s.isCyclic();
            visited = false;
            return (cyclic);
        }

        public void unDerive() {
            if(item != null)
                item.unDerive();
            if(s == null)
                return;
            s.unDerive();
        }

        public void unVisit() {
            if(!visited)
                return;
            visited = false;
            if(item != null)
                item.unVisit();
            if(s == null)
                return;
            s.unVisit();
        }

        public void visitAndCount(IntCounter nItems,
                                  IntCounter tItems, IntCounter rEdges,
                                  IntCounter vNodes, IntCounter iEdges) {
            if(visited)
                return;
            visited = true;
            if(item != null)
                item.visitAndCount(nItems, tItems, rEdges, vNodes, iEdges);
            if(s == null)
                return;
            if(iEdges != null)
                iEdges.inc();
            s.visitAndCount(nItems, tItems, rEdges, vNodes, iEdges);
        }

        void writeToStream(Writer w, Path<Character> d) throws IOException {
            w.write(toPrintString(d));
            visited = true;
            w.write('(');
            w.write("\r\n");
            d.prepend(NO);
            item.writeToStream(w, d);
            d.pop();
            w.write(toPrintString(d));
            w.write(',');
            if((s != null) && (s.head != null)) {
                w.write("\r\n");
                d.prepend(NO);
                s.writeToStream(w, d);
                d.pop();
                w.write(toPrintString(d));
            } else
                w.write("{}");
            w.write(')');
            w.write("\r\n");
        }

        public void collectNodes(Path<Derivation.Edge> n,
                                 Path<Derivation.Item.NonTerminal> i,
                                 Path<DistributionNode> v) {
            if(visited)
                return;
            visited = true;
            n.prepend(this);
            item.collectNodes(n, i, v);
            if(s != null)
                s.collectNodes(n, i, v);
        }
    }

    public static abstract class Item implements Comparable<Item>, Writeable,
                                                 Serializable {

        public final Token x;
        public final int lpos, rpos;
        public static final int UNVISITED = 0;
        public static final int VISITED = 1;
        public static final int INUSE = 2;
        public transient int status = UNVISITED;
        protected transient OrderedSet<Path<plllr.Terminal>> last_derivation = null;

        protected Item(Token x, int lpos, int rpos) {
            if(x == null)
                throw (new NullPointerException("Token is null"));
            if((!(x instanceof plllr.Terminal)) && (!(x instanceof plllr.NonTerminal)))
                throw (new IllegalArgumentException(
                        "Illegal type of token: " + x));
            this.x = x;
            if((lpos < 0) || (rpos < lpos))
                throw (new IllegalArgumentException(
                        "Illegal bounds: " + lpos + "," + rpos));
            this.lpos = lpos;
            this.rpos = rpos;
        }

        public int compareTo(Item other) {
            if(other == null)
                return (1);
            int c = lpos - other.lpos;
            if(c != 0)
                return (c);
            c = rpos - other.rpos;
            if(c != 0)
                return (c);
            if(x == null)
                return (other.x == null ? 0 : -1);
            c = x.compareTo(other.x);
            return (c);
        }

        public String toString() {
            return toStringBuilder(null).toString();
        }

        public StringBuilder toStringBuilder(StringBuilder sb) {
            if(sb == null)
                sb = new StringBuilder();
            return sb.append("(").append(x).append(',').append(lpos).append(',').
                    append(rpos).append(')');
        }

        public int[] count() {
            IntCounter nItems = new IntCounter(),
                    tItems = new IntCounter(),
                    rEdges = new IntCounter(),
                    vNodes = new IntCounter(),
                    iEdges = new IntCounter();
            unVisit();
            visitAndCount(nItems, tItems, rEdges, vNodes, iEdges);
            unVisit();
            return (new int[] {nItems.get(), tItems.get(),
                               rEdges.get(), vNodes.get(), iEdges.get()});
        }

        public void writeToStream(Writer w) throws IOException {
            if(w == null)
                return;
            unVisit();
            writeToStream(w, new Path<Character>());
            unVisit();
        }

        public int getLeftPosition() {
            return (lpos);
        }

        public int getRightPosition() {
            return (rpos);
        }

        public int getLength() {
            return (rpos - lpos);
        }

        public abstract void unVisit();

        abstract void writeToStream(Writer w, Path<Character> d) throws
                IOException;

        public abstract boolean isAmbiguous();

        public abstract boolean isCyclic();

        public abstract void visitAndCount(IntCounter nItems,
                                           IntCounter tItems, IntCounter rEdges,
                                           IntCounter vNodes, IntCounter iEdges);

        public abstract void unDerive();

        public abstract OrderedSet<Path<plllr.Terminal>> derive();

        public abstract void collectNodes(Path<Derivation.Edge> n,
                                          Path<Derivation.Item.NonTerminal> i,
                                          Path<DistributionNode> v);

        public static NonTerminal generateEpsilonShiftItem(plllr.NonTerminal x,
                                                           int pos) {
            if((x == null) || !x.isNullable())
                return (null);
            return new NonTerminal(x, pos, pos,/*x.getNullingRule()*/ null,
                                   null);
        }

        public static class Terminal extends Item {

            private static final long serialVersionUID = 0L;

            public Terminal(plllr.Terminal x, int pos) {
                super(x, pos, pos + 1);
            }

            public OrderedSet<Path<plllr.Terminal>> derive() {
                if(last_derivation != null)
                    return last_derivation;
                if(x == null)
                    throw (new RuntimeException(
                            "pll.Derivation.Item.Terminal.derive: illegel item"));
                last_derivation = new Final<Path<plllr.Terminal>>(
                        new Path[] {new Path<plllr.Terminal>().append(
                            (plllr.Terminal)x)}, null, true);
                return last_derivation;
            }

            public void visitAndCount(IntCounter nItems, IntCounter tItems,
                                      IntCounter rEdges, IntCounter vNodes,
                                      IntCounter iEdges) {
                if(status != UNVISITED)
                    return;
                status = VISITED;
                if(tItems != null)
                    tItems.inc();
            }

            public boolean isAmbiguous() {
                return (false);
            }

            public boolean isCyclic() {
                return (false);
            }

            public void unVisit() {
                status = UNVISITED;
            }

            public void unDerive() {
                last_derivation = null;
            }

            public int getLength() {
                return (x == plllr.Terminal.Epsilon ? 0 : 1);
            }

            void writeToStream(Writer w, Path<Character> d) throws IOException {
                w.write(toPrintString(d));
                w.write('(');
                w.write(x.toString());
                w.write(',');
                w.write(String.valueOf(lpos));
                w.write(',');
                w.write(String.valueOf(rpos));
                if(status != UNVISITED) {
                    w.write(',');
                    w.write('v');
                }
                w.write(')');
                w.write("\r\n");
                status = VISITED;
            }

            public void collectNodes(Path<Derivation.Edge> n,
                                     Path<Derivation.Item.NonTerminal> i,
                                     Path<DistributionNode> v) {
                status = VISITED;
            }
        }

        public static class NonTerminal extends Item {

            private static final long serialVersionUID = 0L;
            public Path<RuleEdge> rules;
            public transient Path<ReversedDerivationPathEntry> reversedDerivationPaths;

            public NonTerminal(plllr.NonTerminal x, int lpos, int rpos) {
                this(x, lpos, rpos, null, null);
            }

            public NonTerminal(plllr.NonTerminal x, int lpos, int rpos,
                               RuleEdge re) {
                super(x, lpos, rpos);
                appendRuleEdge(re);
            }

            public NonTerminal(plllr.NonTerminal x, int lpos, int rpos, Rule r,
                               DistributionNode t) {
                super(x, lpos, rpos);
                appendRuleEdge(r, t);
            }

            public NonTerminal(plllr.NonTerminal x, int lpos, int rpos,
                               ReversedDerivationPathEntry reversedDerivationPath) {
                super(x, lpos, rpos);
                appendReversedDerivationPathEntry(reversedDerivationPath);
            }

            public int getLength() {
                return (rpos - lpos);
            }

            public void appendRuleEdge(RuleEdge re) {
                if(re == null)
                    return;
                if(rules == null)
                    this.rules = new Path<RuleEdge>();
                this.rules.append(re);
            }

            public void appendRuleEdge(Rule r, DistributionNode t) {
                if(r == null)
                    return;
                appendRuleEdge(new RuleEdge(r, t));
            }

            public void appendReversedDerivationPathEntry(
                    ReversedDerivationPathEntry reversedDerivationPath) {
                if(reversedDerivationPath == null)
                    return;
                if(reversedDerivationPaths == null)
                    reversedDerivationPaths = new Path<ReversedDerivationPathEntry>();
                reversedDerivationPaths.append(reversedDerivationPath);
            }

            public boolean isAmbiguous() {
                if(status != UNVISITED)
                    return true;
                status = VISITED;
                if(reversedDerivationPaths != null)
                    transformReversedEntries();
                if(rules == null) {
                    status = UNVISITED;
                    return false;
                }
                if(!rules.isSingle()) {
                    status = UNVISITED;
                    return true;
                }
                boolean amb = rules.head.x.isAmbiguous();
                status = UNVISITED;
                return amb;
            }

            public boolean isCyclic() {
                if(status != UNVISITED)
                    return (true);
                status = VISITED;
                if(reversedDerivationPaths != null)
                    transformReversedEntries();
                if(rules == null) {
                    status = UNVISITED;
                    return false;
                }
                Path.Node<RuleEdge> t = rules.head;
                boolean cyclic = false;
                while(!cyclic && (t != null)) {
                    cyclic = t.x.isCyclic();
                    t = t.next;
                }
                status = UNVISITED;
                return cyclic;
            }

            public void unDerive() {
                if(last_derivation == null)
                    return;
                if(reversedDerivationPaths != null)
                    throw new RuntimeException(
                            "derived nonterminal derivation item contains reversed derivation");
                last_derivation = null;
                if(rules != null) {
                    Path.Node<RuleEdge> t = rules.head;
                    while(t != null) {
                        //if(item.x!=null)
                        t.x.unDerive();
                        t = t.next;
                    }
                }
            }

            public void unVisit() {
                if(status == UNVISITED)
                    return;
                if(reversedDerivationPaths != null)
                    throw new RuntimeException(
                            "visited nonterminal derivation item contains reversed derivation");
                status = UNVISITED;
                if(rules != null) {
                    Path.Node<RuleEdge> t = rules.head;
                    while(t != null) {
                        //if(item.x!=null)
                        t.x.unVisit();
                        t = t.next;
                    }
                }
            }

            public void visitAndCount(IntCounter nItems, IntCounter tItems,
                                      IntCounter rEdges, IntCounter vNodes,
                                      IntCounter iEdges) {
                if(status != UNVISITED)
                    return;
                status = VISITED;
                if(reversedDerivationPaths != null)
                    transformReversedEntries();
                if(nItems != null)
                    nItems.inc();
                if(rules != null) {
                    Path.Node<RuleEdge> t = rules.head;
                    while(t != null) {
                        t.x.visitAndCount(nItems, tItems, rEdges, vNodes, iEdges);
                        t = t.next;
                    }
                }
            }

            void writeToStream(Writer w, Path<Character> d) throws IOException {
                if(reversedDerivationPaths != null)
                    transformReversedEntries();
                w.write(toPrintString(d));
                w.write('(');
                w.write(((plllr.NonTerminal)x).getEscapedName());
                w.write(',');
                w.write(String.valueOf(lpos));
                w.write(',');
                w.write(String.valueOf(rpos));
                if(status != UNVISITED) {
                    w.write(',');
                    w.write('v');
                } else {
                    status = VISITED;
                    w.write(',');
                    if((rules != null) && (rules.head != null)) {
                        w.write("\r\n");
                        d.prepend(IN);
                        w.write(toPrintString(d));
                        w.write('[');
                        Path.Node<RuleEdge> t = rules.head;
                        if(t != null) {
                            w.write("\r\n");
                            d.prepend(RP);
                            t.x.writeToStream(w, d);
                            d.pop();
                            t = t.next;
                            while(t != null) {
                                w.write(toPrintString(d));
                                w.write('|');
                                w.write("\r\n");
                                d.prepend(RP);
                                t.x.writeToStream(w, d);
                                d.pop();
                                t = t.next;
                            }
                            w.write(toPrintString(d));
                        }
                        w.write(']');
                        w.write("\r\n");
                        d.pop();
                        w.write(toPrintString(d));
                    } else
                        w.write("[]");
                }
                w.write(')');
                w.write("\r\n");
            }

            public void collectNodes(Path<Derivation.Edge> n,
                                     Path<Derivation.Item.NonTerminal> i,
                                     Path<DistributionNode> v) {
                if(status != UNVISITED)
                    return;
                status = VISITED;
                if(reversedDerivationPaths != null)
                    transformReversedEntries();
                i.prepend(this);
                if(rules != null) {
                    Path.Node<Derivation.RuleEdge> t = rules.head;
                    while(t != null) {
                        t.x.collectNodes(n, i, v);
                        t = t.next;
                    }

                }
            }

            public OrderedSet<Path<plllr.Terminal>> derive() {
                if(last_derivation != null)
                    return (last_derivation);
                if(reversedDerivationPaths != null)
                    transformReversedEntries();
                if(x == null)
                    throw (new RuntimeException(
                            "pll.Derivation.Item.NonTerminal.derive: illegal item: null NonTerminal"));
                if(((rules == null) || (rules.head == null)) && (lpos < rpos))
                    throw (new RuntimeException(
                            "pll.Derivation.Item.NonTerminal.derive: illegal item: no RuleEntries although len==" + (rpos - lpos)));
                if(lpos == rpos)
                    if(((plllr.NonTerminal)x).isNullable()) {
                        last_derivation = new Final<Path<plllr.Terminal>>(
                                new Path[] {new Path<plllr.Terminal>()}, null,
                                true);
                        return (last_derivation);
                    } else
                        throw (new RuntimeException(
                                "pll.Derivation.Item.NonTerminal.derive: epsilon derivation while not nullable"));
                last_derivation = new BBTree<Path<plllr.Terminal>>();
                Path.Node<RuleEdge> t = rules.head;
                Path.Node<Edge> n;
                while(t != null) {
                    if((t.x != null) && (t.x.s != null)) {
                        n = t.x.s.head;
                        while(n != null) {
                            if(n.x == null)
                                throw (new RuntimeException(
                                        "pll.Derivation.Item.NonTerminal.derive: null node in list of RuleEntry"));
                            last_derivation.addAll(deriveRuleRHS(t.x.rule, 0,
                                                                 n.x));
                            n = n.next;
                        }
                    }
                    t = t.next;
                }
                Path<plllr.Terminal> p;
                Iterator<Path<plllr.Terminal>> it = last_derivation.iterator();
                while(it.hasNext()) {
                    p = it.next();
                    if(p.length() != rpos - lpos)
                        throw (new RuntimeException(
                                "pll.Derivation.Item.NonTerminal.derive: derivation length does not match"));
                }
                last_derivation = new Final<Path<plllr.Terminal>>(
                        last_derivation);
                return (last_derivation);
            }

            private static OrderedSet<Path<plllr.Terminal>> deriveRuleRHS(Rule r,
                                                                          int p,
                                                                          Edge n) {
                if(r == null)
                    throw (new RuntimeException(
                            "pll.Derivation.Item.NonTerminal.deriveRuleRHS: null rule (" + r + "," + p + "," + n + ")"));
                if(r.rhsLength() == 0)
                    throw (new RuntimeException(
                            "pll.Derivation.Item.NonTerminal.deriveRuleRHS: epsilon rule (" + r + "," + p + "," + n + ")"));
                if((p < 0) || (p >= r.rhsLength()))
                    throw (new RuntimeException(
                            "pll.Derivation.Item.NonTerminal.deriveRuleRHS: pos out-of-bounds (" + r + "," + p + "," + n + ")"));
                if(r.rhs[p].compareTo(n.item.x) != 0)
                    throw (new RuntimeException(
                            "pll.Derivation.Item.NonTerminal.deriveRuleRHS: pos out-of-bounds (" + r + "," + p + "," + n + ")"));
                OrderedSet<Path<plllr.Terminal>> a = n.item.derive();
                if(p == r.rhsLength() - 1)
                    if((n.s == null) || (n.s.length() == 0))
                        return (a);
                    else
                        throw (new RuntimeException(
                                "pll.Derivation.Item.NonTerminal.deriveRuleRHS: successors at end of rule (" + r + "," + p + "," + n + ")"));
                if((n.s == null) || (n.s.length() == 0))
                    throw (new RuntimeException(
                            "pll.Derivation.Item.NonTerminal.deriveRuleRHS: no successors (" + r + "," + p + "," + n + ")"));
                Path.Node<Edge> t = n.s.head;
                OrderedSet<Path<plllr.Terminal>> b = new BBTree<Path<plllr.Terminal>>();
                while(t != null) {
                    b.addAll(deriveRuleRHS(r, p + 1, t.x));
                    t = t.next;
                }
                synchronized(b) {
                    synchronized(a) {
                        BBTree<Path<plllr.Terminal>> temp = new BBTree<Path<plllr.Terminal>>();
                        Iterator<Path<plllr.Terminal>> itb = b.iterator(), ita;
                        Path<plllr.Terminal> ap, bp;
                        while(itb.hasNext()) {
                            bp = itb.next();
                            ita = a.iterator();
                            while(ita.hasNext()) {
                                ap = ita.next();
                                temp.add(ap.clone().append(bp));
                            }
                        }
                        a = temp.toFinal();
                    }
                }
                //if((a==null)||(a.size()==0))
                //throw(new RuntimeException("pll.Derivation.Item.NonTerminal.deriveRuleRHS: no derivation"));
                //System.out.println("pll.Derivation.Item.NonTerminal.deriveRuleRHS: no derivation");
                return (a);
            }

            public void transformReversedEntries() {
                if(reversedDerivationPaths == null)
                    return;
                if(reversedDerivationPaths.isEmpty()) {
                    reversedDerivationPaths = null;
                    return;
                }
                Path.Node<ReversedDerivationPathEntry> reversedDerivationPathEntry = reversedDerivationPaths.head;
                while(reversedDerivationPathEntry != null) {
                    ReversedEntry.RuleOrEdgeEntry t = reversedDerivationPathEntry.x.leftPart;
                    DistributionNode currentNodePath = reversedDerivationPathEntry.x.rightPart;
                    RuleEdge lastRuleEntry = null;
                    Edge lastNode = null;
                    int dot = 0;
                    while(t != null)
                        if(t.isRuleEntry()) {
                            ReversedEntry.RuleEntry r = (ReversedEntry.RuleEntry)t;
                            lastRuleEntry = new RuleEdge(r.rule,
                                                         currentNodePath);
                            if(lastNode != null)
                                lastNode.s = new TerminationNode(r.rule, dot,
                                                                 rpos);
                            ReversedEntry.NonTerminalEntry u = r.
                                    getPreviousEntry();
                            if(u != null) {
                                t = u.getPreviousEntry();
                                if(t == null)
                                    throw new NullPointerException(
                                            "reversed nonterminal entry with no predecessor detected");
                                lastNode = new Edge(new NonTerminal(u.symbol, currentNodePath.
                                        first().item.lpos, rpos, lastRuleEntry));
                                currentNodePath = new DistributionNode(lastNode);
                                dot = 1;
                                lastRuleEntry = null;
                            } else
                                break;
                        } else {
                            ReversedEntry.EdgeEntry n = (ReversedEntry.EdgeEntry)t;
                            currentNodePath = new DistributionNode(
                                    new Edge(n.item, currentNodePath));
                            dot++;
                            t = n.getPreviousEntry();
                        }
                    if(lastRuleEntry == null)
                        throw new RuntimeException(
                                "reversed derivation path incorrect");
                    appendRuleEdge(lastRuleEntry);
                    reversedDerivationPathEntry = reversedDerivationPathEntry.next;
                }
                reversedDerivationPaths = null;
            }

            private void writeObject(java.io.ObjectOutputStream s)
                    throws java.io.IOException, ClassNotFoundException {
                if(reversedDerivationPaths != null)
                    transformReversedEntries();
                s.defaultWriteObject();
            }
        }
    }

    public static class RuleEdge implements Comparable<RuleEdge>, Serializable {

        private static final long serialVersionUID = 0L;
        public Rule rule;
        public DistributionNode s;
        public transient boolean visited = false;

        public RuleEdge(Rule r) {
            this(r, (DistributionNode)null);
        }

        public RuleEdge(Rule rule, Edge n) {
            this(rule, new DistributionNode(n));
        }

        public RuleEdge(Rule rule, DistributionNode s) {
            this.rule = rule;
            this.s = s;
        }

        public int compareTo(RuleEdge other) {
            if(other == null)
                return (1);
            return ((rule == null) ? (other.rule == null) ? 0 : -1 : (other.rule == null) ? 1 : (rule.id - other.rule.id));
        }

        public boolean isAmbiguous() {
            if(visited)
                return (true);
            visited = true;
            if(s == null) {
                visited = false;
                return (false);
            }
            boolean amb = s.isAmbiguous();
            visited = false;
            return (amb);
        }

        public boolean isCyclic() {
            if(visited)
                return (true);
            visited = true;
            if(s == null) {
                visited = false;
                return (false);
            }
            boolean cyclic = s.isCyclic();
            visited = false;
            return (cyclic);
        }

        public void unDerive() {
            if(s != null)
                s.unDerive();
        }

        public void unVisit() {
            if(!visited)
                return;
            visited = false;
            if(s != null)
                s.unVisit();
        }

        public void visitAndCount(IntCounter nItems, IntCounter tItems,
                                  IntCounter rEdges, IntCounter vNodes,
                                  IntCounter iEdges) {
            if(visited)
                return;
            visited = true;
            if(s == null)
                return;
            if(rEdges != null)
                rEdges.inc();
            s.visitAndCount(nItems, tItems, rEdges, vNodes, iEdges);
        }

        void writeToStream(Writer w, Path<Character> d) throws IOException {
            w.write(toPrintString(d));
            visited = true;
            w.write(rule.toString());
            w.write('(');
            if((s != null) && (s.head != null)) {
                w.write("\r\n");
                d.prepend(RE);
                s.writeToStream(w, d);
                d.pop();
                w.write(toPrintString(d));
            }
            w.write(')');
            w.write("\r\n");
        }

        public void collectNodes(Path<Derivation.Edge> n,
                                 Path<Derivation.Item.NonTerminal> i,
                                 Path<DistributionNode> v) {
            if(visited)
                return;
            visited = true;
            if(s != null)
                s.collectNodes(n, i, v);
        }
    }

    public static class DistributionNode extends Path<Derivation.Edge> {

        private static final long serialVersionUID = 0L;
        public transient boolean visited = false;

        public DistributionNode() {
            super();
        }

        public DistributionNode(Derivation.Edge n) {
            super();
            if(n != null)
                append(n);
        }

        public boolean isAmbiguous() {
            if(visited)
                return (true);
            visited = true;
            if(head == null) {
                visited = false;
                return (false);
            }
            if(head.next != null) {
                visited = false;
                return (true);
            }
            if(head.x == null) {
                visited = false;
                return (false);
            }
            boolean amb = head.x.isAmbiguous();
            visited = false;
            return (amb);
        }

        public boolean isCyclic() {
            if(visited)
                return (true);
            visited = true;
            if(head == null) {
                visited = false;
                return (false);
            }
            Path.Node<Derivation.Edge> t = head;
            boolean cyclic = false;
            while(!cyclic && (t != null)) {
                cyclic = t.x.isCyclic();
                t = t.next;
            }
            visited = false;
            return (cyclic);
        }

        public void unDerive() {
            Path.Node<Derivation.Edge> t = head;
            while(t != null) {
                //if(item.x!=null)
                t.x.unDerive();
                t = t.next;
            }
        }

        public void unVisit() {
            if(!visited)
                return;
            visited = false;
            Path.Node<Derivation.Edge> t = head;
            while(t != null) {
                //if(item.x!=null)
                t.x.unVisit();
                t = t.next;
            }
        }

        public void visitAndCount(IntCounter nItems, IntCounter tItems,
                                  IntCounter rEdges, IntCounter vNodes,
                                  IntCounter iEdges) {
            if(visited)
                return;
            visited = true;
            if(vNodes != null)
                vNodes.inc();
            Path.Node<Derivation.Edge> t = head;
            while(t != null) {
                if(iEdges != null)
                    iEdges.inc();
                //if(item.x!=null)
                t.x.visitAndCount(nItems, tItems, rEdges, vNodes, iEdges);
                t = t.next;
            }
        }

        void writeToStream(Writer w, Path<Character> d) throws IOException {
            w.write(toPrintString(d));
            visited = true;
            w.write('{');
            Path.Node<Derivation.Edge> t = head;
            if(t != null) {
                w.write("\r\n");
                d.prepend(NP);
                t.x.writeToStream(w, d);
                d.pop();
                t = t.next;
                while(t != null) {
                    w.write(toPrintString(d));
                    w.write('|');
                    w.write("\r\n");
                    d.prepend(NP);
                    t.x.writeToStream(w, d);
                    d.pop();
                    t = t.next;
                }
                w.write(toPrintString(d));
            }
            w.write('}');
            w.write("\r\n");
        }

        public void collectNodes(Path<Derivation.Edge> n,
                                 Path<Derivation.Item.NonTerminal> i,
                                 Path<DistributionNode> v) {
            if(visited)
                return;
            visited = true;
            v.prepend(this);
            Path.Node<Derivation.Edge> t = head;
            while(t != null) {
                t.x.collectNodes(n, i, v);
                t = t.next;
            }
        }
    }

    public static class TerminationNode extends DistributionNode {

        private static final long serialVersionUID = 0L;
        public final Path.Node<Token> p;
        public final int pos;

        public TerminationNode(Path<Token> p, int pos) {
            this(p != null ? p.head : null, pos);
        }

        public TerminationNode(Path.Node<Token> p, int pos) {
            this.p = p;
            this.pos = pos;
        }

        public TerminationNode(Rule rule, int dot, int pos) {
            if(rule != null && dot < rule.rhsLength()) {
                this.p = new Path.Node<Token>(rule.rhs[dot++]);
                Path.Node<Token> t = this.p;
                while(dot < rule.rhs.length) {
                    t.next = new Path.Node<Token>(rule.rhs[dot++]);
                    t = t.next;
                }
            } else
                this.p = null;
            this.pos = pos;
        }

        public String toString() {
            StringBuilder sb = new StringBuilder();
            sb.append('(');
            Path.Node<Token> t = p;
            if(t != null) {
                sb.append(t.x instanceof plllr.NonTerminal
                          ? ((plllr.NonTerminal)(t.x)).getEscapedName() : t.x.
                        toString());
                t = t.next;
                while(t != null) {
                    sb.append(',');
                    sb.append(t.x instanceof plllr.NonTerminal
                              ? ((plllr.NonTerminal)(t.x)).getEscapedName() : t.x.
                            toString());
                    t = t.next;
                }
            }
            sb.append(',');
            sb.append(pos + "," + pos);
            sb.append(')');
            return (sb.toString());
        }
    }

    public static interface ReversedEntry {

        public ReversedEntry getPreviousEntry();

        public static interface RuleOrEdgeEntry extends ReversedEntry {

            public boolean isRuleEntry();
        }

        public static interface EntryWithRuleOrEdgePrevious extends
                ReversedEntry {

            public RuleOrEdgeEntry getPreviousEntry();

            public void setPreviousEntry(RuleOrEdgeEntry previousEntry);
        }

        public static class RuleEntry implements RuleOrEdgeEntry {

            public final Rule rule;
            public NonTerminalEntry previousEntry;

            public RuleEntry(Rule rule) {
                this(rule, null);
            }

            public RuleEntry(Rule rule, NonTerminalEntry previousEntry) {
                if(rule == null)
                    throw new NullPointerException("rule");
                this.rule = rule;
                this.previousEntry = previousEntry;
            }

            public NonTerminalEntry getPreviousEntry() {
                return previousEntry;
            }

            public boolean isRuleEntry() {
                return true;
            }
        }

        public static class EdgeEntry implements RuleOrEdgeEntry,
                                                 EntryWithRuleOrEdgePrevious {

            public final Item item;
            public RuleOrEdgeEntry previousEntry;

            public EdgeEntry(Item item) {
                this(item, null);
            }

            public EdgeEntry(Item item, RuleOrEdgeEntry previousEntry) {
                if(item == null)
                    throw new NullPointerException("item");
                this.item = item;
                this.previousEntry = previousEntry;
            }

            public RuleOrEdgeEntry getPreviousEntry() {
                return previousEntry;
            }

            public void setPreviousEntry(RuleOrEdgeEntry previousEntry) {
                this.previousEntry = previousEntry;
            }

            public boolean isRuleEntry() {
                return false;
            }
        }

        public static class NonTerminalEntry implements
                EntryWithRuleOrEdgePrevious {

            public final plllr.NonTerminal symbol;
            public final int lpos;
            public RuleOrEdgeEntry previousEntry;

            public NonTerminalEntry(plllr.NonTerminal symbol, int lpos) {
                this(symbol, lpos, null);
            }

            public NonTerminalEntry(plllr.NonTerminal symbol, int lpos,
                                    RuleOrEdgeEntry previousEntry) {
                if(symbol == null)
                    throw new NullPointerException("symbol");
                this.symbol = symbol;
                if(lpos < 0)
                    throw new IllegalArgumentException(
                            "start position is negative");
                this.lpos = lpos;
                this.previousEntry = previousEntry;
            }

            public RuleOrEdgeEntry getPreviousEntry() {
                return previousEntry;
            }

            public void setPreviousEntry(RuleOrEdgeEntry previousEntry) {
                this.previousEntry = previousEntry;
            }
        }
    }

    public static class ReversedDerivationPathEntry {

        public ReversedEntry.RuleOrEdgeEntry leftPart;
        public DistributionNode rightPart;

        public ReversedDerivationPathEntry(
                ReversedEntry.RuleOrEdgeEntry leftPart,
                DistributionNode rightPart) {
            this.leftPart = leftPart;
            this.rightPart = rightPart;
        }

        public ReversedDerivationPathEntry(
                ReversedEntry.RuleOrEdgeEntry leftPart) {
            this.leftPart = leftPart;
        }
    }
}
