/*
 * NonTerminal.java
 *
 * Created on 8. Februar 2007, 16:14
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */
package plllr;

import bbtree.Traversable;
import bbtree.Utils.AsymmetricComparable;
import bbtree.Utils.KeyedElementFactory;
import java.io.*;

/**
 *
 * @author RJ
 */
public class NonTerminal implements Token, AsymmetricComparable<String> {

    private static final long serialVersionUID = 0L;
    private String s;
    Rule nullingRule;
    Traversable.Set.Indexed<Rule> rules;
    TerminalToRuleMap firstSet;

    /** Creates a new instance of NonTerminal */
    public NonTerminal(String s) {
        this.s = s;
    }

    public String getName() {
        return s;
    }

    public String setName(String s) {
        String h = s;
        this.s = s;
        return h;
    }

    public boolean isNullable() {
        return nullingRule != null;
    }

    public boolean setNullable(boolean n) {
        boolean h = nullingRule != null;
        if(h != n)
            nullingRule = n ? Rule.DEFAULT_NULLING_RULE : null;
        return h;
    }

    public Rule getNullingRule() {
        return nullingRule;
    }

    public Rule setNullingRule(Rule n) {
        Rule h = nullingRule;
        nullingRule = n;
        return h;
    }

    public boolean isReplacement() {
        return s != null && s.charAt(0) == '@';
    }

    public String getEscapedName() {
        if((s == null) || (s.length() == 0))
            return ("");
        StringBuilder sb = new StringBuilder();
        int i = 0, l = s.length(), c;
        while(i < l) {
            c = s.codePointAt(i);
            if(java.lang.Character.isSupplementaryCodePoint(c))
                i++;
            if(c == '\"')
                sb.append('\\');
            sb.append(Terminal.escapeCodePoint(c));
            i++;
        }
        return (sb.toString());
    }

    public java.lang.String toString() {
        if(isReplacement())
            return (s);
        else
            return (getEscapedName());
//        return("("+getEscapedName()+(nullable?(n_rule!=null?","+n_rule.id:",null"):"")
//            +(firstSet!=null?","+firstSet.toString():"")+")");
    }

    public StringBuilder toStringBuilder(StringBuilder sb) {
        if(sb == null)
            sb = new StringBuilder();
        return sb.append(toString());
    }

    public void writeToStream(Writer w) throws IOException {
        if(w == null)
            return;
        w.write('(');
        w.write(toString());
        if(isNullable()) {
            w.write(',');
            //if(nullingRule!=Rule.DEFAULT_NULLING_RULE)
            w.write(Integer.toString(nullingRule.id));
        }
        if(firstSet != null) {
            w.write(',');
            firstSet.writeToStream(w);
        }
        w.write(')');
    }

    public boolean equals(Object o) {
        return ((this == o) || (o instanceof Token ? compareTo((Token)o) == 0 : false));
    }

    public int hashCode() {
        return (s != null ? s.hashCode() : 0);
    }

    public int compareTo(Token x) {
        if(x == null)
            return (1);
        if(x instanceof Terminal)
            return (1);
        if(x instanceof NonTerminal) {
            String t = ((NonTerminal)x).getName();
            if(t == null)
                return (s != null ? 1 : 0);
            if(s == null)
                return (-1);
            return (s.compareTo(t));
        }
        return (-1);
    }

    public int asymmetricCompareTo(String t) {
        if(t == null)
            return (s != null ? 1 : 0);
        if(s == null)
            return (-1);
        return (s.compareTo(t));
    }

    public static class Factory implements
            KeyedElementFactory<NonTerminal, String> {

        public NonTerminal last;

        public void reset() {
            last = null;
        }

        public NonTerminal lastCreated() {
            return last;
        }

        public NonTerminal createElement(String s) {
            if(s == null)
                throw (new NullPointerException("Null argument"));
            last = new NonTerminal(s);
            return last;
        }

        @Override
        public String extractKey(NonTerminal x) {
            return x.s;
        }
    }
    public static final ThreadLocal<Factory> FACTORY = new ThreadLocal<Factory>() {

        protected Factory initialValue() {
            return new Factory();
        }
    };
    public static final NonTerminal NULL = new NonTerminal(null);
}
