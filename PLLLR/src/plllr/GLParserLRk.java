/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package plllr;

import bbtree.*;
import bbtree.Utils.KeyedElementFactory;
import java.io.*;
import plllr.FirstKSetSupplier.NonTerminalFirstSetCollection;
import plllr.FirstKSetSupplier.RuleWithFirstSets;

/**
 *
 * @author RJ
 */
public class GLParserLRk extends GLParser {

    private BBTree<NodeWithLookAhead> U, V;
    private final Path<NodeWithLookAhead> L = new Path<NodeWithLookAhead>();
    private static final ThreadLocal<TerminalTrie[]> TRIE_ARRAY_3 =
            new ThreadLocal<TerminalTrie[]>() {

                protected TerminalTrie[] initialValue() {
                    return (new TerminalTrie[3]);
                }
            };

    /** Creates a new instance of Parser */
    public GLParserLRk() {
        U = new BBTree<NodeWithLookAhead>();
        V = new BBTree<NodeWithLookAhead>();
        dcache = new BBTree<Derivation.Item>();
        ncache = new BBTree<NonTerminalItemWithUsedRuleSet>();
        expectedTrie = new TerminalTrie();
    }

    public synchronized Path<Derivation.Item.NonTerminal> parse(
            TerminalReader tr,
            FirstKSetSupplier f,
            boolean useProductionLookAhead,
            boolean infixParsing,
            Writer gw)
            throws IOException {
        if(f == null)
            throw (new NullPointerException("Null firstKset"));
        if(f.g == null)
            throw (new NullPointerException("Null grammar"));
        if(f.g.S == null)
            throw (new NullPointerException("Null start symbol"));
        this.f = f;
        this.useProductionLookAhead = useProductionLookAhead;
        this.infixParsing = infixParsing;
        NodeWithLookAhead t;
        Terminal tl;
        dcache.clear();
        ncache.clear();
        p = new Path<Derivation.Item.NonTerminal>();
        ctp.clear();
        if(tr != null)
            if(f.k == 0) {
                tl = tr.read();
                if(tl == null)
                    ctp.append(Terminal.End);
                else
                    ctp.append(tl);
            } else {
                int i = 0;
                while(i < f.k) {
                    tl = tr.read();
                    if(tl == null) {
                        ctp.append(Terminal.End);
                        break;
                    }
                    ctp.append(tl);
                    if(tl == Terminal.End)
                        break;
                    i++;
                }
            }
        else
            ctp.append(Terminal.End);
        initGraphWriter(gw);
        ct = ctp.first();
        if(ct == Terminal.End) {
            if(f.g.S.isNullable())
                p.append(Derivation.Item.generateEpsilonShiftItem(f.g.S, 0));
            derivation_nodes_created.inc();
            derivation_edges_created.inc();
            finishGraphWriter();
            return p;
        }
        cp = 0;
        U.add(NodeWithLookAhead.makeStartNode(f.g.S, 0, f));
        nodes_created.inc();
        reduce_edges_created.inc();
        while(ct != Terminal.End) {
            produce();
            if(DEBUG)
                System.out.println("post-produce(" + cp + "): " + U.toString());
            makeNodesFinal();
            if(!DEBUG)
                dcache.clear();
            writeCurrentGraphCluster(U);
            shift();
            if(DEBUG)
                System.out.println("post-shift(" + cp + "): " + U.toString());
            if(U.isEmpty() && !infixParsing) {
                finishGraphWriter();
                return p;
            } else
                shifted_symbols.inc();
            if(ct != ctp.pop())
                throw new RuntimeException("impossible error occured");
            if(ctp.last() != Terminal.End) {
                tl = tr.read();
                if(tl == null)
                    ctp.append(Terminal.End);
                else
                    ctp.append(tl);
            }
            ct = ctp.first();
            if(ct == null)
                throw (new NullPointerException("Null terminal"));
            reduce();
            if(DEBUG)
                System.out.println("post-reduce(" + cp + "): " + U.toString());
            else {
                dcache.clear();
                ncache.clear();
            }
            if(ct != Terminal.End && infixParsing)
                if(U.add(t = NodeWithLookAhead.makeStartNode(f.g.S, cp, f))) {
                    nodes_created.inc();
                    reduce_edges_created.inc();
                } else {
                    t = U.get(t);
                    if(t.addEdge(Edge.StackEnd))
                        reduce_edges_created.inc();
                    else
                        reduce_edges_visited.inc();
                }
        }
        makeNodesFinal();
        finishGraphWriter();
        if(p.isEmpty()) {
            expectedTrie.clear();
            TerminalTrie[] ta2 = TRIE_ARRAY_2.get();
            for(NodeWithLookAhead x : U) {
                ta2[0] = f.firstKSetOf(x.tp);
                ta2[1] = x.la;
                expectedTrie.addConcatenation(ta2);
            }
        }
        U.clear();
        V.clear();
        L.clear();
        if(DEBUG)
            System.out.println("Dcache(" + dcache.size() + "): " + dcache.
                    toString());
        dcache.clear();
        if(DEBUG)
            System.out.println("Ncache(" + ncache.size() + "): " + ncache.
                    toString());
        ncache.clear();
        return p;
    }

    public boolean generatesParseGraph() {
        return true;
    }

    private void produce() {
        NonTerminal d;
        NonTerminalFirstSetCollection nfsc;
        NodeWithLookAhead t;
        L.appendSome(U, PRODUCABLES);
        Path.Node<NodeWithLookAhead> tn = L.head;
        while(tn != null) {
            producer.perform(null, Traversable.IN_ORDER, tn.x);
            tn = tn.next;
        }
        while(!L.isEmpty())
            while((t = L.pop()) != null) {
                V.remove(t);
                d = (NonTerminal)(t.tp.head.x);
                if(d == null)
                    continue;
                nfsc = f.getFirstSetCollectionFor(d);
                if(nfsc == null)
                    throw (new RuntimeException(
                            "No firstsets for nonterminal " + d));
                nfsc.rfsc.traverse(Traversable.IN_ORDER, producer, t);
                L.appendSome(V, null);
                V.clear();
            }
    }
    protected static final Selector<NodeWithLookAhead> PRODUCABLES = new Selector<NodeWithLookAhead>() {

        public boolean selects(NodeWithLookAhead x) {
            return (x.isProducable());
        }

        public NodeWithLookAhead lowerBound() {
            return (null);
        }

        public NodeWithLookAhead upperBound() {
            return (null);
        }
    };

    private void makeNodesFinal() {
        U.traverse(TraversalActionPerformer.IN_ORDER, FINALIZER, null);
    }

    private void shift() {
        csi = new Derivation.Item.Terminal(ct, cp);
        cp++;
        U.traverse(BBTree.IN_ORDER, SHIFTER, this);
        if(V.isEmpty()) {
            expectedTrie.clear();
            TerminalTrie[] ta2 = TRIE_ARRAY_2.get();
            for(NodeWithLookAhead x : U) {
                ta2[0] = f.firstKSetOf(x.tp);
                ta2[1] = x.la;
                expectedTrie.addConcatenation(ta2);
            }
        }
        BBTree<NodeWithLookAhead> h = U;
        U = V;
        V = h;
        if(!U.isEmpty() || infixParsing)
            V.clear();
    }

    private void reduce() {
        NodeWithLookAhead t;
        U.traverse(Traversable.IN_ORDER, REDUCIBLES_APPENDER, this);
        while(!L.isEmpty())
            while((t = L.pop()) != null) {
                if(t.edges == null)
                    continue;
                if(useTransitiveEdges) {
                    Edge.Transitive et = t.createOrGetTransitiveEdge(this);
                    if(et != null) {
                        reducer.perform(et, Traversable.IN_ORDER, t);
                        continue;
                    }
                }
                t.edges.traverse(Traversable.IN_ORDER, reducer, t);
            }
    }

    private boolean addRightNulledTerminationToRightNullableNode(
            NodeWithLookAhead t) {
        if((t == null) || !t.isReducable() || (t.last_dpos == cp))
            return false;
        t.last_dpos = cp;
        t.last_der = t.tp.isEmpty() ? null : new Derivation.TerminationNode(
                t.tp, t.pos);
        return true;
    }

    private static class Producer implements
            TraversalActionPerformer<RuleWithFirstSets> {

        public final GLParserLRk p;

        public Producer(GLParserLRk p) {
            if(p == null)
                throw (new NullPointerException("Parser is null"));
            this.p = p;
        }

        public boolean perform(RuleWithFirstSets rfs, int order, Object o) {
            NodeWithLookAhead t = (NodeWithLookAhead)o;
            boolean shift_on = false;
            TerminalTrie ttt;
            NodeWithLookAhead cc;
            Edge ce;
            if(rfs != null) {
                if(rfs.owner.rhsLength() == 0)
                    return (false);
                TerminalTrie firstSetOfBeta = p.f.firstKSetOf(t.tp.head.next);
                if(p.useProductionLookAhead && (p.f.k > 0)) {
                    TerminalTrie[] ta3 = TRIE_ARRAY_3.get();
                    ta3[0] = rfs.firstSet;
                    ta3[1] = firstSetOfBeta;
                    ta3[2] = t.la;
                    if(!TerminalTrie.BoundedConcatenationMatchesPath(ta3, p.f.k,
                                                                     false,
                                                                     p.ctp))
                        return (false);
                }
                NodeWithLookAhead.AFactory naf = NodeWithLookAhead.AFactory.
                        getThreadLocal(p);
                cc = p.U.addOrGetMatch(rfs.owner.rhs, naf);
                if(cc == naf.last) {
                    p.nodes_created.inc();
                    cc.la = p.f.getConcatenation(firstSetOfBeta, t.la);
                    if(cc.isProducable()) {
                        p.V.add(cc);
                        shift_on = true;
                    }
                } else {
                    ttt = p.f.getUnion(cc.la, p.f.getConcatenation(
                            firstSetOfBeta, t.la));
                    if(cc.la != ttt) {
                        cc.la = ttt;
                        if(cc.isProducable()) {
                            p.V.add(cc);
                            shift_on = true;
                        }
                    }
                }
                ce = new Edge.Reduce(t, rfs.owner);
                if(cc.addEdge(ce))
                    p.reduce_edges_created.inc();
                else
                    p.reduce_edges_visited.inc();
                if(!shift_on)
                    return (false);
                t = cc;
            }
            NodeWithLookAhead.PFactory npf = NodeWithLookAhead.PFactory.
                    getThreadLocal(p);
            NonTerminal d = (NonTerminal)(t.tp.head.x);
            Path<Token> gamma;
            while(((d == null) || d.isNullable()) && (t.tp.head.next != null)) {
                gamma = t.tp.shift();
                if(p.f.k > 0) {
                    TerminalTrie[] ta2 = TRIE_ARRAY_2.get();
                    ta2[0] = p.f.firstKSetOf(gamma);
                    ta2[1] = t.la;
                    if(!TerminalTrie.BoundedConcatenationMatchesPath(ta2, p.f.k,
                                                                     false,
                                                                     p.ctp))
                        return false;
                }
                shift_on = false;
                cc = p.U.addOrGetMatch(gamma, npf);
                if(cc == npf.last) {
                    p.nodes_created.inc();
                    cc.la = t.la;
                    if(cc.isProducable()) {
                        p.V.add(cc);
                        shift_on = true;
                    }
                } else {
                    ttt = p.f.getUnion(cc.la, t.la);
                    if(cc.la != ttt) {
                        cc.la = ttt;
                        if(cc.isProducable()) {
                            p.V.add(cc);
                            shift_on = true;
                        }
                    }
                }
                ce = new Edge.Shift(t, p.dcache.addOrGet(
                        Derivation.Item.generateEpsilonShiftItem(d, p.cp)));
                if(cc.addEdge(ce))
                    p.shift_edges_created.inc();
                else
                    p.shift_edges_visited.inc();
                if(!shift_on)
                    break;
                t = cc;
                d = (NonTerminal)(t.tp.head.x);
            }
            return false;
        }
    }
    private final Producer producer = new Producer(this);
    private static final TraversalActionPerformer<NodeWithLookAhead> SHIFTER = new TraversalActionPerformer<NodeWithLookAhead>() {

        public boolean perform(NodeWithLookAhead x, int order, Object o) {
            if((x == null) || !x.isShiftable())
                return (false);
            NodeWithLookAhead y;
            GLParserLRk p = (GLParserLRk)o;
            if(((Terminal)(x.tp.head.x)).matches(p.ct)) {
                TerminalTrie[] ta2 = GLParser.TRIE_ARRAY_2.get();
                ta2[0] = p.f.firstKSetOf(x.tp);
                ta2[1] = x.la;
                if(!TerminalTrie.BoundedConcatenationMatchesPath(ta2, p.f.k,
                                                                 p.ctp))
                    return false;
                NodeWithLookAhead.PFactory npf = NodeWithLookAhead.PFactory.
                        getThreadLocal(p);
                y = p.V.addOrGetMatch(x.tp.shift(), npf);
                if(y == npf.last) {
                    p.nodes_created.inc();
                    y.la = x.la;
                } else
                    y.la = p.f.getUnion(y.la, x.la);
                if(y.addEdge(new Edge.Shift(x, p.csi)))
                    p.shift_edges_created.inc();
                else
                    p.shift_edges_visited.inc();
            }
            return false;
        }
    };

    private static class Reducer implements TraversalActionPerformer<Edge> {

        public final GLParserLRk p;

        public Reducer(GLParserLRk p) {
            if(p == null)
                throw (new NullPointerException("Parser is null"));
            this.p = p;
        }

        public boolean perform(Edge e, int order, Object o) {
            NodeWithLookAhead t = (NodeWithLookAhead)o, u = (NodeWithLookAhead)e.target;
            if(e instanceof Edge.Reduce || e instanceof Edge.Transitive) {
                boolean tEdge = e instanceof Edge.Transitive;
                if(tEdge)
                    p.transitive_edges_visited.inc();
                else
                    p.reduce_edges_visited.inc();
                Edge.Reduce er = tEdge ? null : (Edge.Reduce)e;
                Edge.Transitive et = tEdge ? (Edge.Transitive)e : null;
                if(!tEdge && er.r == null) {
                    if(u != null)
                        throw (new RuntimeException(
                                "Null rule in reduction edge discovered"));
                    if((p.ct == Terminal.End) || p.infixParsing) {
                        p.p.append(
                                (Derivation.Item.NonTerminal)(t.last_der.head.x.item));
                        p.derivation_nodes_created.inc();
                        p.derivation_edges_created.inc();
                    }
                } else {
                    if(u == null)
                        throw new RuntimeException(
                                (tEdge ? "transitive" : "illegal reduction") + " edge to stack end detected");
                    if(!tEdge)
                        if(!u.tp.head.x.equals(er.r.lhs))
                            throw new RuntimeException(
                                    "illegal reduction edge detected: " + e);
                    Path<Token> beta = u.tp.shift();
                    if(p.f.k > 0) {
                        TerminalTrie[] ta2 = TRIE_ARRAY_2.get();
                        ta2[0] = p.f.firstKSetOf(beta);
                        ta2[1] = u.la;
                        if(!TerminalTrie.BoundedConcatenationMatchesPath(ta2,
                                                                         p.f.k,
                                                                         true,
                                                                         p.ctp))
                            return false;
                    }
                    NodeWithLookAhead.PFactory npf = NodeWithLookAhead.PFactory.
                            getThreadLocal(p);
                    NodeWithLookAhead v = p.U.addOrGetMatch(beta, npf);
                    if(v == npf.last) {
                        p.nodes_created.inc();
                        v.la = u.la;
                    } else
                        v.la = p.f.getUnion(v.la, u.la);
                    Edge.Shift es = new Edge.Shift(u, null);
                    Edge h = v.addOrGetEdge(es);
                    if(h == es) {
                        p.shift_edges_created.inc();
                        NonTerminalItemWithUsedRuleSet.Factory nuf =
                                NonTerminalItemWithUsedRuleSet.Factory.
                                getThreadLocal(p);
                        NonTerminalItemWithUsedRuleSet ntiwurs = null;
                        ntiwurs = p.ncache.addOrGetMatch(
                                new Derivation.Item.NonTerminal(
                                (NonTerminal)u.tp.head.x, u.pos, p.cp),
                                nuf);
                        if(ntiwurs == nuf.last)
                            p.derivation_nodes_created.inc();
                        es.der = ntiwurs.nti;
                        if(tEdge) {
                            ntiwurs.nti.appendReversedDerivationPathEntry(new Derivation.ReversedDerivationPathEntry(
                                    et.reversedDerivationPath, t.last_der));
                            p.derivation_edges_created.inc();
                        } else if(ntiwurs.useRule(er.r)) {
                            ntiwurs.nti.appendRuleEdge(er.r, t.last_der);
                            p.derivation_edges_created.inc();
                        }
                        if(v.isReducable() && u.la.matches(p.ctp)) {
                            p.addRightNulledTerminationToRightNullableNode(v);
                            if(u.last_dpos != p.cp) {
                                u.last_dpos = p.cp;
                                u.last_der = new Derivation.DistributionNode();
                                u.last_der.append(
                                        new Derivation.Edge(es.der, v.last_der));
                                p.derivation_nodes_created.inc();
                                p.derivation_edges_created.inc();
                                p.L.append(u);
                            } else {
                                u.last_der.append(
                                        new Derivation.Edge(es.der, v.last_der));
                                p.derivation_nodes_created.inc();
                                p.derivation_edges_created.inc();
                            }
                        }
                    } else {
                        p.shift_edges_visited.inc();
                        es = (Edge.Shift)h;
                        if(tEdge) {
                            ((Derivation.Item.NonTerminal)es.der).
                                    appendReversedDerivationPathEntry(new Derivation.ReversedDerivationPathEntry(
                                    et.reversedDerivationPath, t.last_der));
                            p.derivation_edges_created.inc();
                        } else {
                            NonTerminalItemWithUsedRuleSet ntiwurs = p.ncache.
                                    getMatch(
                                    new Derivation.Item.NonTerminal(er.r.lhs,
                                                                    u.pos, p.cp));
                            if(ntiwurs.useRule(er.r)) {
                                ntiwurs.nti.appendRuleEdge(er.r, t.last_der);
                                p.derivation_edges_created.inc();
                            }
                        }
                    }
                }
                return tEdge;
            } else if(e instanceof Edge.Shift) {
                p.shift_edges_visited.inc();
                if((p.infixParsing && u.la.contains(Terminal.End)) || u.la.
                        matches(p.ctp)) {
                    Edge.Shift es = (Edge.Shift)e;
                    if(u.last_dpos != p.cp) {
                        u.last_dpos = p.cp;
                        u.last_der = new Derivation.DistributionNode();
                        u.last_der.append(
                                new Derivation.Edge(es.der, t.last_der));
                        p.derivation_nodes_created.inc();
                        p.derivation_edges_created.inc();
                        p.L.append(u);
                    } else {
                        u.last_der.append(
                                new Derivation.Edge(es.der, t.last_der));
                        p.derivation_nodes_created.inc();
                        p.derivation_edges_created.inc();
                    }
                }
            }
            return (false);
        }
    }
    private final Reducer reducer = new Reducer(this);
    private static final TraversalActionPerformer<NodeWithLookAhead> REDUCIBLES_APPENDER = new TraversalActionPerformer<NodeWithLookAhead>() {

        public boolean perform(NodeWithLookAhead x, int order, Object o) {
            if(o != null) {
                GLParserLRk p = (GLParserLRk)o;
                if(x.isReducable() && x.la.matches(p.ctp)) {
                    p.addRightNulledTerminationToRightNullableNode(x);
                    p.L.append(x);
                }
            }
            return (false);
        }
    };

    private static class NodeWithLookAhead extends Node {

        TerminalTrie la = null;

        private NodeWithLookAhead(Path<Token> tp, int pos) {
            super(tp, pos);
        }

        public static NodeWithLookAhead makeStartNode(NonTerminal S, int pos,
                                                      FirstKSetSupplier f) {
            if(S == null)
                return (null);
            NodeWithLookAhead r = new NodeWithLookAhead(new Path<Token>().append(
                    S), pos);
            r.addEdge(Edge.StackEnd);
            r.la = f.firstKSetOf(Terminal.End);
            return (r);
        }

        @Override
        public String toString() {
            StringBuilder sb = new StringBuilder();
            sb.append('(');
            if(tp != null) {
                Path.Node<Token> t = tp.head;
                while(t != null) {
                    if(t.x == null) {
                    } else if(t.x instanceof NonTerminal)
                        sb.append(((NonTerminal)(t.x)).getEscapedName());
                    else
                        sb.append(t.x.toString());
                    t = t.next;
                    if(t != null)
                        sb.append(' ');
                }
            }
            sb.append(',');
            sb.append(pos);
            sb.append(',');
            sb.append(la.toString());
            sb.append(',');
            sb.append(edges != null ? edges.toString() : "{}");
            sb.append(')');
            return (sb.toString());
        }

        private static class PFactory implements
                KeyedElementFactory<NodeWithLookAhead, Path<Token>> {

            NodeWithLookAhead last;
            GLParserLRk p;

            public PFactory init(GLParserLRk p) {
                if(p == null)
                    throw (new NullPointerException("Parser is null"));
                this.p = p;
                return (this);
            }

            public NodeWithLookAhead lastCreated() {
                return (last);
            }

            public void reset() {
                last = null;
            }

            public NodeWithLookAhead createElement(Path<Token> tp) {
                if(tp == null)
                    throw (new NullPointerException("Path is null"));
                last = new NodeWithLookAhead(tp, p.cp);
                return (last);
            }

            public static PFactory getThreadLocal(GLParserLRk p) {
                return (TLV.get().init(p));
            }
            private static final ThreadLocal<NodeWithLookAhead.PFactory> TLV =
                    new ThreadLocal<NodeWithLookAhead.PFactory>() {

                        protected NodeWithLookAhead.PFactory initialValue() {
                            return (new NodeWithLookAhead.PFactory());
                        }
                    };

            @Override
            public Path<Token> extractKey(NodeWithLookAhead x) {
                return x.tp;
            }
        }

        private static class AFactory implements
                KeyedElementFactory<NodeWithLookAhead, Token[]> {

            NodeWithLookAhead last;
            GLParserLRk p;

            public AFactory init(GLParserLRk p) {
                if(p == null)
                    throw (new NullPointerException("Parser is null"));
                this.p = p;
                return (this);
            }

            public NodeWithLookAhead lastCreated() {
                return (last);
            }

            public void reset() {
                last = null;
            }

            public NodeWithLookAhead createElement(Token[] tp) {
                if(tp == null)
                    throw (new NullPointerException("Path is null"));
                last = new NodeWithLookAhead(Path.valueOf(tp), p.cp);
                return (last);
            }

            public static AFactory getThreadLocal(GLParserLRk p) {
                return (TLV.get().init(p));
            }
            private static final ThreadLocal<NodeWithLookAhead.AFactory> TLV =
                    new ThreadLocal<NodeWithLookAhead.AFactory>() {

                        protected NodeWithLookAhead.AFactory initialValue() {
                            return (new NodeWithLookAhead.AFactory());
                        }
                    };

            @Override
            public Token[] extractKey(NodeWithLookAhead x) {
                return x.tp != null ? x.tp.toArray(new Token[0]) : null;
            }
        }
    }
}
