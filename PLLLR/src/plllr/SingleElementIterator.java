/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package plllr;

import bbtree.*;
import java.util.NoSuchElementException;

/**
 *
 * @author RJ
 */
public class SingleElementIterator<A> implements ResettableIterator<A> {
    private A x,y;
    public SingleElementIterator(A x) {
        this.x = x;
        this.y = x;
    }
    public void reset() {
        x = y;
    }
    public OrderedSet<A> iteratedSet() {
        return(null);
    }
    public ResettableIterator<A> iterator() {
        reset();
        return(this);
    }
    public boolean hasNext() {
        return(x!=null);
    }
    public A next() {
        if(x==null)
            throw(new NoSuchElementException(getClass().getCanonicalName()
                    +".next: no more elements to iterate"));
        A r = x;
        x = null;
        return(r);
    }
    public void remove() {
            throw(new UnsupportedOperationException(getClass().getCanonicalName()
                    +".remove: removal not supported"));
    }
    public void finalize() {
        x = y = null;
    }
}
