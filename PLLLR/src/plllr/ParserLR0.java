/*
 * ParserLR0.java
 *
 * Created on 9. Oktober 2007, 10:47
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */
package plllr;

import bbtree.*;
import java.io.*;
import java.util.Iterator;

/**
 *
 * @author RJ
 */
public class ParserLR0 extends Parser {

    private BBTree<Node> U, V;   //U enthält alle 'aktuellen' Knoten des GSS, V dient als Zwischenpuffer.
    private Path<Node> L;       //Eine Liste von Knoten, die abgearbeitet werden m�ssen.
    private Iterator<Node> it;  //Iteriert über Knoten in U oder V.
    private final Rule searchRule = new Rule(null, (Token[])null);

    /**
     * Creates a new instance of ParserLR0
     */
    public ParserLR0() {
        U = new BBTree<Node>(); //Die Haupt-Mengen initialisieren
        V = new BBTree<Node>();
        L = new Path<Node>();
        dcache = new BBTree<Derivation.Item>();
        ncache = new BBTree<NonTerminalItemWithUsedRuleSet>();
        expected = new BBTree<Terminal>();
    }

    public synchronized Path<Derivation.Item.NonTerminal> parse(
            TerminalReader tr,
            Grammar G,
            boolean useProductionLookAhead,
            boolean infixParsing,
            Writer gw)
            throws IOException {
        if(G == null)
            throw new NullPointerException("Null grammar");
        if(G.S == null)
            throw new NullPointerException("Null start symbol");
        shifted_symbols.set(0);
        derivation_nodes_created.set(0);
        derivation_edges_created.set(0);
        nodes_created.set(0);
        shift_edges_created.set(0);
        shift_edges_visited.set(0);
        reduce_edges_created.set(0);
        reduce_edges_visited.set(0);
        this.useProductionLookAhead = useProductionLookAhead;
        this.infixParsing = infixParsing;
        Node t;
        if((G == null) || (G.S == null))
            return (null);
        this.G = G;
        p = new Path<Derivation.Item.NonTerminal>();
        if(tr != null)
            ct = tr.read();
        else
            ct = null;
        initGraphWriter(gw);
        if((ct == null) || (ct == Terminal.End)) {        //Prüft bei leerer Eingabe, ob diese zur Grammatik gehört
            if(G.S.isNullable())
                p.append(Derivation.Item.generateEpsilonShiftItem(G.S, 0));
            derivation_nodes_created.inc();
            derivation_edges_created.inc();
            finishGraphWriter();
            return p;
        }
        cp = 0;
        U.add(Node.makeStartNode(G.S, 0));   //Erzeugt den Start-Knoten in U
        nodes_created.inc();
        reduce_edges_created.inc();
        while(ct != Terminal.End) {
            produce();
            if(DEBUG)
                System.out.println("post-produce(" + cp + "): " + U.toString());
            makeNodesFinal();   //komprimiert die Kanten-Mengen in den Knoten
            if(!DEBUG) {
                dcache.clear();
                ncache.clear();
            }
            writeCurrentGraphCluster(U);
            shift();
            if(DEBUG)
                System.out.println("post-shift(" + cp + "): " + U.toString());
            if(U.isEmpty() && !infixParsing) {
                finishGraphWriter();
                return p;
            } else
                shifted_symbols.inc();
            ct = tr.read();
            if(ct == null)
                ct = Terminal.End;
            reduce();
            if(DEBUG)
                System.out.println("post-reduce(" + cp + "): " + U.toString());
            if((ct != Terminal.End) && infixParsing)
                if(U.add(t = Node.makeStartNode(G.S, cp))) {
                    nodes_created.inc();
                    reduce_edges_created.inc();
                } else {
                    t = U.get(t);
                    if(t.addEdge(Edge.StackEnd))
                        reduce_edges_created.inc();
                    else
                        reduce_edges_visited.inc();
                }
        }
        makeNodesFinal();
        finishGraphWriter();
        if(p.isEmpty()) {
            expected.clear();
            TerminalSet u;
            for(Node x : U) {
                u = Grammar.first(x.tp);
                if(u != null)
                    u.addTo(expected);
                expected.remove(Terminal.Epsilon);
            }
        }
        U.clear();
        V.clear();
        L.clear();
        if(DEBUG)
            System.out.println("Dcache(" + dcache.size() + "): " + dcache.
                    toString());
        dcache.clear();
        if(DEBUG)
            System.out.println("Ncache(" + ncache.size() + "): " + ncache.
                    toString());
        ncache.clear();
        return p;
    }

    public boolean generatesParseGraph() {
        return true;
    }

    private void produce() {
        NonTerminal d;
        Node t;
        L.appendSome(U, PRODUCABLES);
        Path.Node<Node> tn = L.head;
        while(tn != null) {
            producerLL0.perform(null, Traversable.IN_ORDER, tn.x);
            tn = tn.next;
        }
        while(!L.isEmpty()) {
            while((t = L.pop()) != null) {
                V.remove(t);
                d = (NonTerminal)(t.tp.head.x);
                if(d == null)
                    continue;
                if(useProductionLookAhead)
                    d.firstSet.traverseMatchingRuleEntries(ct, producerLL1, t);
                else {
                    if(d.rules == null || d.rules.isEmpty())
                        continue;
                    d.rules.traverse(Traversable.IN_ORDER, producerLL0, t);
                }
            }
            L.appendSome(V, null);
            V.clear();
        }
    }

    private void makeNodesFinal() {
        U.traverse(TraversalActionPerformer.IN_ORDER, FINALIZER, null);
    }

    private void shift() {
        csi = new Derivation.Item.Terminal(ct, cp);
        cp++;
        expected.clear();
        U.traverse(BBTree.IN_ORDER, SHIFTER, this);
        if(V.isEmpty()) {
            TerminalSet u;
            for(Node x : U) {
                u = Grammar.first(x.tp);
                if(u != null)
                    u.addTo(expected);
                expected.remove(Terminal.Epsilon);
            }
        }
        BBTree<Node> h = U;
        U = V;
        V = h;
        if(!U.isEmpty() || infixParsing)
            V.clear();
    }

    protected void shift(Node x) {
        if((x == null) || !x.isShiftable())
            return;
        Node y, z;
//        expected.add((Terminal)(x.tp.head.x));
        if(((Terminal)(x.tp.head.x)).matches(ct)) {
            Node.PFactory npf = Node.PFactory.getThreadLocal(this);
            y = V.addOrGetMatch(x.tp.shift(), npf);
            if(npf.last == y)
                nodes_created.inc();
            if(y.addEdge(new Edge.Shift(x, csi)))
                shift_edges_created.inc();
            else
                shift_edges_visited.inc();
        }
    }

    private void reduce() {
        Node t;
        U.traverse(Traversable.IN_ORDER, REDUCIBLES_APPENDER, this);
        while(!L.isEmpty())
            while((t = L.pop()) != null) {
                if(t.edges == null)
                    continue;
                if(useTransitiveEdges) {
                    Edge.Transitive et = t.createOrGetTransitiveEdge(this);
                    if(et != null) {
                        reducer.perform(et, Traversable.IN_ORDER, t);
                        continue;
                    }
                }
                t.edges.traverse(Traversable.IN_ORDER, reducer, t);
            }
    }

    private boolean addRightNulledTerminationToRightNullableNode(Node t) {
        if((t == null) || !t.isReducable() || (t.last_dpos == cp))
            return (false);
        t.last_dpos = cp;
        t.last_der = t.tp.isEmpty() ? null : new Derivation.TerminationNode(
                t.tp, t.pos);
        return true;
    }
    private static final TraversalActionPerformer<Node> REDUCIBLES_APPENDER = new TraversalActionPerformer<Node>() {

        public boolean perform(Node x, int order, Object o) {
            if(o != null) {
                ParserLR0 p = (ParserLR0)o;
                if(p.addRightNulledTerminationToRightNullableNode(x))
                    p.L.append(x);
            }
            return (false);
        }
    };

    private static class ProducerLL0 implements
            TraversalActionPerformer<Rule> {

        public final ParserLR0 p;

        public ProducerLL0(ParserLR0 p) {
            if(p == null)
                throw new NullPointerException("Parser is null");
            this.p = p;
        }

        public boolean perform(Rule r, int order, Object o) {
            Node t = (Node)o;
            boolean shift_on = false;
            if(r != null) {
                if(r.rhsLength() == 0)
                    return false;
                Node.AFactory naf = Node.AFactory.getThreadLocal(p);
                Node cc = p.U.addOrGetMatch(r.rhs, naf);
                if(naf.last == cc) {
                    p.nodes_created.inc();
                    if(cc.isProducable()) {
                        p.V.add(cc);
                        shift_on = true;
                    }
                }
                Edge ce = new Edge.Reduce(t, r);
                if(cc.addEdge(ce))
                    p.reduce_edges_created.inc();
                else
                    p.reduce_edges_visited.inc();
                if(!shift_on)
                    return false;
                t = cc;
            }
            Node.PFactory npf = Node.PFactory.getThreadLocal(p);
            NonTerminal d = (NonTerminal)(t.tp.head.x);
            Path<Token> gamma;
            while(((d == null) || d.isNullable()) && (t.tp.head.next != null)) {
                gamma = t.tp.shift();
                shift_on = false;
                Node cc = p.U.addOrGetMatch(gamma, npf);
                if(cc == npf.last) {
                    p.nodes_created.inc();
                    if(cc.isProducable()) {
                        p.V.add(cc);
                        shift_on = true;
                    }
                }
                Edge ce = new Edge.Shift(t, p.dcache.addOrGet(
                        Derivation.Item.generateEpsilonShiftItem(d, p.cp)));
                if(cc.addEdge(ce))
                    p.shift_edges_created.inc();
                else
                    p.shift_edges_visited.inc();
                if(!shift_on)
                    break;
                t = cc;
                d = (NonTerminal)(t.tp.head.x);
            }
            return (false);
        }
    }
    private final ProducerLL0 producerLL0 = new ProducerLL0(this);

    private static class ProducerLL1 implements
            TraversalActionPerformer<TerminalToRuleMap.RuleEntry> {

        public final ParserLR0 p;

        public ProducerLL1(ParserLR0 p) {
            if(p == null)
                throw new NullPointerException("Parser is null");
            this.p = p;
        }

        public boolean perform(TerminalToRuleMap.RuleEntry rt, int order,
                               Object o) {
            if(rt == null || rt.r == null || rt.r.rhsLength() == 0
               || rt.p == null || rt.p.length == 0)
                return false;
            Node t = (Node)o;
            int j = 0, i = 0, m = rt.p[rt.p.length - 1];
            Node cc;
            Edge ce;
            Node.AFactory naf = Node.AFactory.getThreadLocal(p);
            Node.PFactory npf = Node.PFactory.getThreadLocal(p);
            while(i <= m) {
                if((j < rt.p.length - 1) && (i == rt.p[j + 1]))
                    j++;
                cc = i == 0 ? p.U.addOrGetMatch(rt.r.rhs, naf)
                     : p.U.addOrGetMatch(t.tp.shift(), npf);
                ce = i == 0 ? new Edge.Reduce(t, rt.r)
                     : new Edge.Shift(t, p.dcache.addOrGet(
                        Derivation.Item.generateEpsilonShiftItem(
                        ((NonTerminal)(t.tp.head.x)), p.cp)));
                if((i == 0 ? naf.last : npf.last) == cc) {
                    p.nodes_created.inc();
                    if((i == rt.p[j]) && cc.isProducable())
                        p.V.add(cc);
                }
                if(cc.addEdge(ce))
                    if(i == 0)
                        p.reduce_edges_created.inc();
                    else
                        p.shift_edges_created.inc();
                else if(i == 0)
                    p.reduce_edges_visited.inc();
                else
                    p.shift_edges_visited.inc();
                t = cc;
                i++;
            }
            return false;
        }
    }
    private final ProducerLL1 producerLL1 = new ProducerLL1(this);
    private static final TraversalActionPerformer<Node> SHIFTER = new TraversalActionPerformer<Node>() {

        public boolean perform(Node x, int order, Object o) {
            if((x == null) || !x.isShiftable())
                return (false);
            Node y;
            ParserLR0 p = (ParserLR0)o;
            if(((Terminal)(x.tp.head.x)).matches(p.ct)) {
                Node.PFactory npf = Node.PFactory.getThreadLocal(p);
                if(npf.p == null)
                    npf.p = p;
                y = p.V.addOrGetMatch(x.tp.shift(), npf);
                if(y == npf.last)
                    p.nodes_created.inc();
                if(y.addEdge(new Edge.Shift(x, p.csi)))
                    p.shift_edges_created.inc();
                else
                    p.shift_edges_visited.inc();
            }
            return (false);
        }
    };

    private static class Reducer implements TraversalActionPerformer<Edge> {

        public final ParserLR0 p;

        public Reducer(ParserLR0 p) {
            if(p == null)
                throw (new NullPointerException("Parser is null"));
            this.p = p;
        }

        public boolean perform(Edge e, int order, Object o) {
            Node t = (Node)o, u = e.target;
            if(e instanceof Edge.Reduce || e instanceof Edge.Transitive) {
                boolean tEdge = e instanceof Edge.Transitive;
                if(tEdge)
                    p.transitive_edges_visited.inc();
                else
                    p.reduce_edges_visited.inc();
                Edge.Reduce er = tEdge ? null : (Edge.Reduce)e;
                Edge.Transitive et = tEdge ? (Edge.Transitive)e : null;
                if(!tEdge && er.r == null) {
                    if(u != null)
                        throw (new RuntimeException(
                                "Null rule in reduction edge discovered"));
                    if((p.ct == Terminal.End) || p.infixParsing) {
                        p.p.append(
                                (Derivation.Item.NonTerminal)(t.last_der.head.x.item));
                        p.derivation_nodes_created.inc();
                        p.derivation_edges_created.inc();
                    }
                } else {
                    if(u == null)
                        throw new RuntimeException(
                                (tEdge ? "transitive" : "illegal reduction") + " edge to stack end detected");
                    if(!tEdge)
                        if(!u.tp.head.x.equals(er.r.lhs))
                            throw new RuntimeException(
                                    "illegal reduction edge detected: " + e);
                    Path<Token> beta = u.tp.shift();
                    Node.PFactory npf = Node.PFactory.getThreadLocal(p);
                    Node v = p.U.addOrGetMatch(beta, npf);
                    if(v == npf.last)
                        p.nodes_created.inc();
                    Edge.Shift es = new Edge.Shift(u, null);
                    Edge h = v.addOrGetEdge(es);
                    if(h == es) {
                        p.shift_edges_created.inc();
                        NonTerminalItemWithUsedRuleSet.Factory nuf =
                                NonTerminalItemWithUsedRuleSet.Factory.
                                getThreadLocal(p);
                        NonTerminalItemWithUsedRuleSet ntiwurs = p.ncache.
                                addOrGetMatch(
                                new Derivation.Item.NonTerminal(
                                (NonTerminal)u.tp.head.x, u.pos, p.cp), nuf);
                        if(ntiwurs == nuf.last)
                            p.derivation_nodes_created.inc();
                        es.der = ntiwurs.nti;
                        if(tEdge) {
                            ntiwurs.nti.appendReversedDerivationPathEntry(new Derivation.ReversedDerivationPathEntry(
                                    et.reversedDerivationPath, t.last_der));
                            p.derivation_edges_created.inc();
                        } else if(ntiwurs.useRule(er.r)) {
                            ntiwurs.nti.appendRuleEdge(er.r, t.last_der);
                            p.derivation_edges_created.inc();
                        }
                        if(v.isReducable()) {
                            p.addRightNulledTerminationToRightNullableNode(v);
                            if(u.last_dpos != p.cp) {
                                u.last_dpos = p.cp;
                                u.last_der = new Derivation.DistributionNode();
                                u.last_der.append(
                                        new Derivation.Edge(es.der, v.last_der));
                                p.derivation_nodes_created.inc();
                                p.derivation_edges_created.inc();
                                p.L.append(u);
                            } else {
                                u.last_der.append(
                                        new Derivation.Edge(es.der, v.last_der));
                                p.derivation_nodes_created.inc();
                                p.derivation_edges_created.inc();
                            }
                        }
                    } else {
                        p.shift_edges_visited.inc();
                        es = (Edge.Shift)h;
                        if(tEdge) {
                            ((Derivation.Item.NonTerminal)es.der).
                                    appendReversedDerivationPathEntry(new Derivation.ReversedDerivationPathEntry(
                                    et.reversedDerivationPath, t.last_der));
                            p.derivation_edges_created.inc();
                        } else {
                            NonTerminalItemWithUsedRuleSet ntiwurs = p.ncache.
                                    getMatch(
                                    new Derivation.Item.NonTerminal(er.r.lhs,
                                                                    u.pos, p.cp,
                                                                    null, null));
                            if(ntiwurs.useRule(er.r)) {
                                ntiwurs.nti.appendRuleEdge(er.r, t.last_der);
                                p.derivation_edges_created.inc();
                            }
                        }
                    }
                }
                return tEdge;
            } else if(e instanceof Edge.Shift) {
                p.shift_edges_visited.inc();
                Edge.Shift es = (Edge.Shift)e;
                if(u.last_dpos != p.cp) {
                    u.last_dpos = p.cp;
                    u.last_der = new Derivation.DistributionNode();
                    u.last_der.append(new Derivation.Edge(es.der, t.last_der));
                    p.derivation_nodes_created.inc();
                    p.derivation_edges_created.inc();
                    p.L.append(u);
                } else {
                    u.last_der.append(new Derivation.Edge(es.der, t.last_der));
                    p.derivation_nodes_created.inc();
                    p.derivation_edges_created.inc();
                }
            }
            return false;
        }
    }
    private final Reducer reducer = new Reducer(this);
}
