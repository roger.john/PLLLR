/*
 * Token.java
 *
 * Created on 7. Februar 2007, 15:34
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */
package plllr;

import bbtree.Writeable;
import java.io.*;

/**
 * 
 * @author RJ
 */
public interface Token extends Comparable<Token>, Writeable, Serializable {

    public String toString();

    public boolean equals(Object o);

    public int hashCode();

    public static class Alternation implements Token {

        private static final long serialVersionUID = 0L;
        public Token[] options;

        public Alternation() {
        }

        public Alternation(Token[] options) {
            this.options = options;
        }

        public String toString() {
            if((options == null) || (options.length == 0))
                return ("");
            return toStringBuilder(null).toString();
        }

        public StringBuilder toStringBuilder(StringBuilder sb) {
            if(sb == null)
                sb = new StringBuilder();
            if((options == null) || (options.length == 0))
                return sb.append("");
            sb.append('(');
            int j;
            for(int i = 0; i < options.length; i++) {
                if((options[i] != null))
                    sb.append(options[i].toString());
                if(i < options.length - 1)
                    sb.append(" | ");
            }
            return sb.append(')');
        }

        public void writeToStream(Writer w) throws IOException {
            if(w == null)
                return;
            w.write(toString());
        }

        public boolean equals(Object o) {
            return ((this == o) || (o instanceof Token ? compareTo((Token)o) == 0 : false));
        }

        public int hashCode() {
            if(options == null)
                return (0);
            int sum = 0;
            for(int i = 0; i < options.length; i++)
                if(options[i] != null)
                    sum += options[i].hashCode();
            return (sum);
        }

        public int compareTo(Token x) {
            if((x == null) || (x instanceof NonTerminal) || (x instanceof Terminal))
                return (1);
            if(x instanceof Alternation) {
                Alternation y = (Alternation)x;
                if((options == null) || (options.length == 0))
                    return (y.options == null ? 0 : -y.options.length);
                if(y.options == null)
                    return (1);
                int c, i;
                for(i = 0;
                    i < (options.length >= y.options.length ? options.length : y.options.length);
                    i++) {
                    if(options[i] != null)
                        c = options[i].compareTo(y.options[i]);
                    else
                        c = (y.options[i] == null) ? 0 : -1;
                    if(c != 0)
                        return (c);
                }
                return (options.length - y.options.length);
            }
            return (getClass().getName().compareTo(x.getClass().getName()));
        }
    }

    public static class Group implements Token {

        private static final long serialVersionUID = 0L;
        public Token[] members;

        public Group() {
        }

        public Group(Token[] members) {
            this.members = members;
        }

        public String toString() {
            if((members == null) || (members.length == 0))
                return ("");
            return toStringBuilder(null).toString();
        }

        public StringBuilder toStringBuilder(StringBuilder sb) {
            if(sb == null)
                sb = new StringBuilder();
            if((members == null) || (members.length == 0))
                return sb.append("");
            sb.append('(');
            for(int i = 0; i < members.length; i++) {
                if((members[i] != null))
                    members[i].toStringBuilder(sb);
                if(i + 1 < members.length)
                    sb.append(' ');
            }
            return sb.append(')');
        }

        public void writeToStream(Writer w) throws IOException {
            if(w == null)
                return;
            w.write(toString());
        }

        public boolean equals(Object o) {
            return ((this == o) || (o instanceof Token ? compareTo((Token)o) == 0 : false));
        }

        public int hashCode() {
            if(members == null)
                return (0);
            int sum = 0;
            for(int i = 0; i < members.length; i++)
                if(members[i] != null)
                    sum += members[i].hashCode();
            return (sum);
        }

        public int compareTo(Token x) {
            if((x == null) || (x instanceof NonTerminal) || (x instanceof Terminal))
                return (1);
            if(x instanceof Group) {
                Group y = (Group)x;
                if((members == null) || (members.length == 0))
                    return (y.members == null ? 0 : -y.members.length);
                if(y.members == null)
                    return (1);
                int c, i, m = members.length <= y.members.length ? members.length : y.members.length;
                for(i = 0; i < m; i++) {
                    if(members[i] != null)
                        c = members[i].compareTo(y.members[i]);
                    else
                        c = (y.members[i] == null) ? 0 : -1;
                    if(c != 0)
                        return (c);
                }
                return (members.length - y.members.length);
            }
            return (getClass().getName().compareTo(x.getClass().getName()));
        }
    }

    public static class Optional implements Token {

        private static final long serialVersionUID = 0L;
        public Token option;

        public Optional() {
        }

        public Optional(Token option) {
            this.option = option;
        }

        public boolean equals(Object o) {
            return ((this == o) || (o instanceof Token ? compareTo((Token)o) == 0 : false));
        }

        public String toString() {
            if(option == null)
                return "";
            return toStringBuilder(null).toString();
        }

        public StringBuilder toStringBuilder(StringBuilder sb) {
            if(sb == null)
                sb = new StringBuilder();
            if(option == null)
                return sb;
            option.toStringBuilder(sb);
            return sb.append('?');
        }

        public void writeToStream(Writer w) throws IOException {
            if(w == null)
                return;
            w.write(toString());
        }

        public int hashCode() {
            return (option == null ? 0 : option.hashCode() + 1);
        }

        public int compareTo(Token x) {
            if((x == null) || (x instanceof NonTerminal) || (x instanceof Terminal))
                return (1);
            if(x instanceof Optional) {
                Optional y = (Optional)x;
                if(option == null)
                    return (y.option == null ? 0 : -1);
                return (option.compareTo(y.option));
            }
            return (getClass().getName().compareTo(x.getClass().getName()));
        }
    }

    public static class Starred implements Token {

        private static final long serialVersionUID = 0L;
        public Token x;

        public Starred() {
        }

        public Starred(Token x) {
            this.x = x;
        }

        public boolean equals(Object o) {
            return ((this == o) || (o instanceof Token ? compareTo((Token)o) == 0 : false));
        }

        public String toString() {
            if(x == null)
                return "";
            return toStringBuilder(null).toString();
        }

        public StringBuilder toStringBuilder(StringBuilder sb) {
            if(sb == null)
                sb = new StringBuilder();
            if(x == null)
                return sb;
            x.toStringBuilder(sb);
            return sb.append('*');
        }

        public void writeToStream(Writer w) throws IOException {
            if(w == null)
                return;
            w.write(toString());
        }

        public int hashCode() {
            return (x == null ? 0 : x.hashCode() + 2);
        }

        public int compareTo(Token y) {
            if((y == null) || (y instanceof NonTerminal) || (y instanceof Terminal))
                return (1);
            if(y instanceof Starred) {
                Starred z = (Starred)y;
                if(x == null)
                    return (z.x == null ? 0 : -1);
                return (x.compareTo(z.x));
            }
            return (getClass().getName().compareTo(x.getClass().getName()));
        }
    }

    public static class Plussed implements Token {

        private static final long serialVersionUID = 0L;
        public Token x;

        public Plussed() {
        }

        public Plussed(Token x) {
            this.x = x;
        }

        public boolean equals(Object o) {
            return ((this == o) || (o instanceof Token ? compareTo((Token)o) == 0 : false));
        }

        public String toString() {
            if(x == null)
                return "";
            return toStringBuilder(null).toString();
        }

        public StringBuilder toStringBuilder(StringBuilder sb) {
            if(sb == null)
                sb = new StringBuilder();
            if(x == null)
                return sb;
            x.toStringBuilder(sb);
            return sb.append('+');
        }

        public void writeToStream(Writer w) throws IOException {
            if(w == null)
                return;
            w.write(toString());
        }

        public int hashCode() {
            return (x == null ? 0 : x.hashCode() + 3);
        }

        public int compareTo(Token y) {
            if((y == null) || (y instanceof NonTerminal) || (y instanceof Terminal))
                return (1);
            if(y instanceof Plussed) {
                Plussed z = (Plussed)y;
                if(x == null)
                    return (z.x == null ? 0 : -1);
                return (x.compareTo(z.x));
            }
            return (getClass().getName().compareTo(x.getClass().getName()));
        }
    }
}
