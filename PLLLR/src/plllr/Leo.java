/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package plllr;

import bbtree.BBTree;
import bbtree.Path;
import bbtree.Traversable;
import bbtree.Traversable.Set;
import bbtree.TraversalActionPerformer;
import bbtree.Utils.AsymmetricComparable;
import bbtree.Utils.KeyedElementFactory;
import bbtree.Value.Comparable.Keyed;
import bbtree.Writeable;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;
import plllr.FirstKSetSupplier.NonTerminalFirstSetCollection;
import plllr.FirstKSetSupplier.RuleWithFirstSets;

/**
 *
 * @author RJ
 */
public class Leo extends GLParser {

    //private Traversable.Set.Indexed<Value.Comparable.Indexed<Traversable.Set<Item>>> I;
    private List<ItemSet> I;
    private ItemSet U;
    private Traversable.Set<Token> VT = new BBTree<Token>();
    private Traversable.Set<Item> VI = new BBTree<Item>();
    private final Path<Token> LT = new Path<Token>();
    private final Path<Item> LI = new Path<Item>();
    private final Keyed<Token, Traversable.Set<Item>> searchKeyed = new Keyed<Token, Set<Item>>();
    private static final NonTerminal START_SYMBOL = new NonTerminal("S'");
    public final Rule startRule = new Rule(START_SYMBOL, new Token[] {null});

    public Leo() {
        expectedTrie = new TerminalTrie();
    }

    public synchronized Path<Derivation.Item.NonTerminal> parse(
            TerminalReader tr,
            FirstKSetSupplier f,
            boolean useProductionLookAhead,
            boolean infixParsing,
            Writer gw)
            throws IOException {
        shifted_symbols.set(0);
        nodes_created.set(0);
        this.infixParsing = infixParsing;
        this.useProductionLookAhead = useProductionLookAhead;
        if((f == null) || (f.g == null) || (f.g.S == null))
            return (null);
        this.f = f;
        startRule.rhs[0] = f.g.S;
        cp = 0;
        Terminal tl;
        if(tr != null)
            if(f.k == 0) {
                tl = tr.read();
                if(tl == null)
                    ctp.append(Terminal.End);
                else
                    ctp.append(tl);
            } else {
                int i = 0;
                while(i < f.k) {
                    tl = tr.read();
                    if(tl == null) {
                        ctp.append(Terminal.End);
                        break;
                    }
                    ctp.append(tl);
                    if(tl == Terminal.End)
                        break;
                    i++;
                }
            }
        else
            ctp.append(Terminal.End);
        ct = ctp.first();
        I = new ArrayList<ItemSet>();
        U = new ItemSet();
        U.addItem(new Item(startRule));
        nodes_created.inc();
        I.add(0, U);
        this.p = new Path<Derivation.Item.NonTerminal>();
        if(ct == Terminal.End && f.g.S.isNullable()) {
            U.addItem(new Item(startRule, 1, 0));
            p.append(new Derivation.Item.NonTerminal(f.g.S, 0, 0));
        }
        while(ct != Terminal.End) {
            produce();
            shift();
            if(DEBUG)
                System.out.println("post-shift(" + cp + "): " + U.toString());
            if(U.isEmpty() && !infixParsing) {
                if(gw != null) {
                    writeParseSetsTo(gw);
                    gw.close();
                }
                return p;
            } else
                shifted_symbols.inc();
            if(ct != ctp.pop())
                throw new RuntimeException("impossible error occured");
            if(ctp.last() != Terminal.End) {
                tl = tr.read();
                if(tl == null)
                    ctp.append(Terminal.End);
                else
                    ctp.append(tl);
            }
            ct = ctp.first();
            reduce();
            if(DEBUG)
                System.out.println("post-reduce(" + cp + "): " + U.toString());
            if((ct != Terminal.End) && infixParsing)
                if(U.addItem(new Item(startRule, 0, cp)))
                    nodes_created.inc();
        }
        if(p.isEmpty()) {
            expectedTrie.clear();
            if(U.items != null && !U.items.isEmpty())
                for(Keyed<Token, Traversable.Set<Item>> x : U.items)
                    if(x.value != null && !x.value.isEmpty())
                        for(Item t : x.value)
                            expectedTrie.addAll(
                                    f.firstKSetOf(t.getRemainingTokens()));
        }
        if(gw != null) {
            writeParseSetsTo(gw);
            gw.close();
        }
        U = null;
        return p;
    }

    private void writeParseSetsTo(Writer w) throws IOException {
        for(int i = 0; i < cp; i++) {
            w.write("I" + i + " = ");
            I.get(i).writeToStream(w);
            w.write(";\r\n");
        }
        w.flush();
    }

    public boolean generatesParseGraph() {
        return false;
    }

    private void produce() {
        if(U.containsItems()) {
            LT.clear();
            VT.clear();
            searchKeyed.key = NonTerminal.NULL;
            U.items.traverseRange(Traversable.IN_ORDER,
                                  PRODUCABLE_NONTERMINALS_APPENDER, this,
                                  searchKeyed, null);
            NonTerminal d;
            NonTerminalFirstSetCollection nfsc;
            while(!LT.isEmpty()) {
                while((d = (NonTerminal)LT.pop()) != null) {
                    VT.remove(d);
                    nfsc = f.getFirstSetCollectionFor(d);
                    nfsc.rfsc.traverse(Traversable.IN_ORDER, producer, null);
                }
                LT.appendSome(VT, null);
                VT.clear();
            }
        }
    }

    private void shift() {
        ItemSet previousSet = U;
        cp++;
        U = new ItemSet();
        I.add(cp, U);
        Traversable.Set<Item> previousItems = previousSet.
                getItemsWithDotInFrontOf(ct);
        if(previousItems == null || previousItems.isEmpty()) {
            expectedTrie.clear();
            if(previousSet.items != null && !previousSet.items.isEmpty())
                for(Keyed<Token, Traversable.Set<Item>> x : previousSet.items)
                    if(x.value != null && !x.value.isEmpty())
                        for(Item t : x.value)
                            expectedTrie.addAll(
                                    f.firstKSetOf(t.getRemainingTokens()));
            return;
        }
        shift(previousItems, U);
    }

    private void shift(Traversable.Set<Item> source, ItemSet target) {
        if(source == null || source.isEmpty())
            return;
        source.traverse(Traversable.IN_ORDER, shifter, target);
    }

    private void reduce() {
        VI.clear();
        Traversable.Set<Item> u = U.getItemsWithDotInFrontOf(Terminal.Epsilon);
        if(u != null && !u.isEmpty())
            u.traverse(Traversable.IN_ORDER, REDUCIBLE_ITEMS_APPENDER, this);
        Item item;
        Item.Transitive tItem;
        while(!LI.isEmpty()) {
            while((item = LI.pop()) != null) {
                VI.remove(item);
                if(item.isComplete() && item.rule != startRule) {
                    tItem = useTransitiveEdges ? getTransitiveItemFor(item) : null;
                    if(tItem != null) {
                        item = tItem.getTransitionedItem();
                        if(U.addItem(item)) {
                            nodes_created.inc();
                            VI.add(item);
                        }
                        if(item.rule == startRule && item.isComplete()
                           && (infixParsing || ct
                                               == Terminal.End))
                            p.append(new Derivation.Item.NonTerminal(
                                    f.g.S, item.pos, cp));
                    } else
                        I.get(item.pos).getItemsWithDotInFrontOf(item.rule.lhs).
                                traverse(Traversable.IN_ORDER, reducer, U);
                }
            }
            LI.appendSome(VI, null);
            VI.clear();
        }
    }

    private Item.Transitive getTransitiveItemFor(Item item) {
        NonTerminal symbol = item.rule.lhs;
        ItemSet u = I.get(item.pos);
        Traversable.Set<Item.Transitive> transitiveItems = u.getTransitiveSetFor(
                symbol);
        if(transitiveItems != null && !transitiveItems.isEmpty()) {
            if(transitiveItems.size() > 1)
                System.out.println("Warning: multiple transitive items for symbol "
                                   + symbol + "discovered: " + transitiveItems);
            return transitiveItems.first();
        }
        Traversable.Set<Item> items = u.getItemsWithDotInFrontOf(symbol);
        if(item.rule != startRule && items.size() == 1) {
            item = items.first();
            if(item.isShiftedQuasiComplete(f)) {
                Item.Transitive tItem = getTransitiveItemFor(item);
                tItem =
                        new Item.Transitive(tItem != null ? tItem : item, symbol);
                return (Item.Transitive)u.addOrGetItem(tItem);
            }
        }
        return null;
    }
    private static final TraversalActionPerformer<Keyed<Token, Traversable.Set<Item>>> PRODUCABLE_NONTERMINALS_APPENDER = new TraversalActionPerformer<Keyed<Token, Traversable.Set<Item>>>() {

        public boolean perform(Keyed<Token, Traversable.Set<Item>> x, int order,
                               Object o) {
            Leo p = (Leo)o;
            if(x.key instanceof NonTerminal) {
                NonTerminal t = (NonTerminal)x.key;
                ((Leo)o).LT.append(t);
                if(t.isNullable() && x.value != null && !x.value.isEmpty())
                    x.value.traverse(Traversable.IN_ORDER, EPSILON_SHIFTER,
                                     p.producer);
            }
            return false;
        }
    };
    private static final TraversalActionPerformer<Item> REDUCIBLE_ITEMS_APPENDER = new TraversalActionPerformer<Item>() {

        public boolean perform(Item x, int order, Object o) {
            if(!x.isComplete())
                throw new IllegalArgumentException("complete item expected");
            ((Leo)o).LI.append(x);
            return false;
        }
    };

    private static class Producer implements
            TraversalActionPerformer<RuleWithFirstSets> {

        public final Leo p;

        public Producer(Leo p) {
            if(p == null)
                throw (new NullPointerException("Recognizer is null"));
            this.p = p;
        }

        public boolean perform(RuleWithFirstSets rfs, int order, Object o) {
            Item t;
            if(rfs != null) {
                Item.Factory f = Item.Factory.getThreadLocal(p);
                if(rfs.owner.rhsLength() == 0)
                    return false;
                if(p.useProductionLookAhead && (p.f.k > 0) && !rfs.firstSet.
                        matchesPrefixOf(p.ctp))
                    return false;
                t = p.U.addOrGetProductionItem(rfs.owner, f);
                if(t == f.last) {
                    p.nodes_created.inc();
                    if(t.isProducable())
                        p.VT.add(t.getSymbolAfterDot());
                } else
                    return false;
            } else
                t = (Item)o;
            while(t.isEpsilonShiftable()) {
                Item u = t.shift();
                t = p.U.addOrGetItem(u);
                if(t == u) {
                    p.nodes_created.inc();
                    if(t.isProducable())
                        p.VT.add(t.getSymbolAfterDot());
                } else
                    return false;
            }
            return false;
        }
    }
    private final Producer producer = new Producer(this);
    private static final TraversalActionPerformer<Item> EPSILON_SHIFTER = new TraversalActionPerformer<Item>() {

        public boolean perform(Item x, int order, Object o) {
            ((Producer)o).perform(null, Traversable.IN_ORDER, x);
            return false;
        }
    };

    private static class Shifter implements TraversalActionPerformer<Item> {

        public final Leo p;

        public Shifter(Leo p) {
            if(p == null)
                throw (new NullPointerException("Recognizer is null"));
            this.p = p;
        }

        public boolean perform(Item item, int order, Object o) {
            ItemSet target = (ItemSet)o;
            Item u = item.shift();
            item = target.addOrGetItem(u);
            if(item == u)
                p.nodes_created.inc();
            else
                return false;
            while(item.isEpsilonShiftable()) {
                u = item.shift();
                item = target.addOrGetItem(u);
                if(item == u)
                    p.nodes_created.inc();
                else
                    return false;
            }
            return false;
        }
    }
    private final Shifter shifter = new Shifter(this);

    private static class Reducer implements TraversalActionPerformer<Item> {

        public final Leo p;

        public Reducer(Leo p) {
            if(p == null)
                throw (new NullPointerException("Recognizer is null"));
            this.p = p;
        }

        public boolean perform(Item item, int order, Object o) {
            ItemSet target = (ItemSet)o;
            Item u = item.shift();
            item = target.addOrGetItem(u);
            if(item == u) {
                p.nodes_created.inc();
                if(item.isComplete())
                    p.VI.add(item);
            } else
                return false;
            while(item.isEpsilonShiftable()) {
                u = item.shift();
                item = p.U.addOrGetItem(u);
                if(item == u) {
                    p.nodes_created.inc();
                    if(item.isComplete())
                        p.VI.add(item);
                } else
                    return false;
            }
            if(item.rule == p.startRule && item.isComplete() && (p.infixParsing
                                                                 || p.ct
                                                                    == Terminal.End))
                p.p.append(new Derivation.Item.NonTerminal(p.f.g.S,
                                                           item.pos,
                                                           p.cp));
            return false;
        }
    }
    private final Reducer reducer = new Reducer(this);
    private final TraversalActionPerformer<Item.Transitive> ITEM_TRANSITIONER = new TraversalActionPerformer<Item.Transitive>() {

        public boolean perform(Item.Transitive tItem, int order, Object o) {
            Leo p = (Leo)o;
            Item item = tItem.getTransitionedItem();
            Item u = p.U.addOrGetItem(item);
            if(item == u) {
                p.nodes_created.inc();
                if(item.isComplete())
                    p.VI.add(item);
            }
            return false;
        }
    };

    private static class Item implements java.lang.Comparable<Item>,
                                         AsymmetricComparable<Rule> {

        public final Rule rule;
        public final int dot, pos;

        public Item(Rule rule) {
            this(rule, 0, 0);
        }

        public Item(Rule rule, int dot, int pos) {
            if(rule == null)
                throw new NullPointerException("Rule");
            if(dot < 0 || dot > rule.rhsLength())
                throw new IndexOutOfBoundsException(
                        "value " + dot + " for dot position is out of bounds");
            this.rule = rule;
            this.dot = dot;
            this.pos = pos;
        }

        public int asymmetricCompareTo(Rule rule) {
            if(rule == null)
                return 1;
            int c = this.rule.compareTo(rule);
            if(c != 0)
                return c;
            if(this.dot != 0)
                return this.dot;
            return isTransitive() ? 1 : 0;
        }

        public int compareTo(Item o) {
            if(o == null)
                return 1;
            int c = this.pos - o.pos;
            if(c != 0)
                return c;
            c = this.rule.compareTo(o.rule);
            if(c != 0)
                return c;
            c = this.dot - o.dot;
            if(c != 0)
                return c;
            boolean tt = this.isTransitive(), to = o.isTransitive();
            if(tt != to)
                return tt ? 1 : -1;
            if(tt)
                return this.getTransitionSymbol().compareTo(o.
                        getTransitionSymbol());
            return 0;
        }

        public Item shift() {
            return shift(pos);
        }

        public Item shift(int newPos) {
            if(isComplete())
                throw new UnsupportedOperationException("Item " + this
                                                        + " is complete");
            return new Item(rule, dot + 1, newPos);
        }

        public boolean isShiftedComplete() {
            return dot + 1 == rule.rhsLength();
        }

        public boolean isComplete() {
            return dot == rule.rhsLength();
        }

        public boolean isShiftedQuasiComplete(FirstKSetSupplier f) {
            return isQuasiComplete(f, true);
        }

        public boolean isQuasiComplete(FirstKSetSupplier f) {
            return isQuasiComplete(f, false);
        }

        private boolean isQuasiComplete(FirstKSetSupplier f, boolean shifted) {
            if(shifted ? isShiftedComplete() : isComplete())
                return true;
            for(int i = (shifted ? dot + 1 : dot); i < rule.rhs.length; i++) {
                if(rule.rhs[i] == null || rule.rhs[i] == Terminal.Epsilon)
                    continue;
                if(!(rule.rhs[i] instanceof NonTerminal))
                    return false;
                NonTerminal d = (NonTerminal)rule.rhs[i];
                if(!d.isNullable() || f == null || f.k == 0)
                    return false;
                NonTerminalFirstSetCollection nfsc =
                        f.getFirstSetCollectionFor(d);
                if(nfsc == null || nfsc.firstSet == null || !nfsc.firstSet.
                        containsOnlyEmptyPath())
                    return false;
            }
            return true;
        }

        public Token getSymbolAfterDot() {
            if(dot < rule.rhsLength())
                return rule.rhs[dot];
            else
                return null;
        }

        public boolean isTransitive() {
            return false;
        }

        public NonTerminal getTransitionSymbol() {
            throw new UnsupportedOperationException("Item " + toString()
                                                    + " is not transitive");
        }

        public boolean equals(Object o) {
            return ((this == o) || ((o instanceof Item) && (compareTo((Item)o)
                                                            == 0)));
        }

        public boolean isProducable() {
            Token t = getSymbolAfterDot();
            return t != null && (t instanceof NonTerminal);
        }

        public boolean isEpsilonShiftable() {
            Token t = getSymbolAfterDot();
            if(t == null)
                return !isComplete();
            return (t instanceof NonTerminal) && ((NonTerminal)t).isNullable();
        }

        public Path<Token> getRemainingTokens() {
            return getRemainingTokens(null);
        }

        public Path<Token> getRemainingTokens(Path<Token> p) {
            if(p == null)
                p = new Path<Token>();
            if(dot < rule.rhsLength())
                for(int i = dot; i < rule.rhs.length; i++)
                    p.append(rule.rhs[i]);
            return p;
        }

        public String toString() {
            StringBuilder sb = new StringBuilder().append('[');
            if(rule.lhs != null)
                sb.append(rule.lhs.toString());
            sb.append(" ->");
            if(rule.rhsLength() > 0) {
                for(int i = 0; i < rule.rhs.length; i++) {
                    if(i == dot)
                        sb.append('.');
                    else
                        sb.append(' ');
                    if(rule.rhs[i] == null)
                        continue;
                    if(rule.rhs[i] instanceof plllr.NonTerminal)
                        sb.append(((plllr.NonTerminal)(rule.rhs[i])).
                                getEscapedName());
                    else
                        sb.append(Terminal.String.escape(rule.rhs[i].toString()));
                }
                if(dot == rule.rhs.length)
                    sb.append('.');
            } else
                sb.append(" .");
            if(isTransitive()) {
                sb.append(',');
                sb.append(getTransitionSymbol().getEscapedName());
            }
            sb.append(',');
            sb.append(pos);
            sb.append(']');
            return sb.toString();
        }

        public static class Transitive extends Item {

            public final NonTerminal symbol;

            public Transitive(Rule rule, NonTerminal symbol, int pos) {
                super(rule, rule != null ? rule.rhsLength() : 0, pos);
                if(symbol == null)
                    throw new NullPointerException("Symbol");
                this.symbol = symbol;
            }

            public Transitive(Item item, NonTerminal symbol) {
                super(item.rule, item.dot, item.pos);
                if(item.isComplete() || !(item.rule.rhs[item.dot] instanceof NonTerminal))
                    throw new IllegalArgumentException("Illegal transitive item construction: "
                                                       + item);
                if(symbol == null)
                    throw new NullPointerException("Symbol");
                this.symbol = symbol;
            }

            public boolean isTransitive() {
                return true;
            }

            public NonTerminal getTransitionSymbol() {
                return symbol;
            }

            public Item getTransitionedItem() {
                return new Item(rule, rule.rhsLength(), pos);
            }
        }

        public static class Factory implements KeyedElementFactory<Item, Rule> {

            Item last;
            Leo p;

            public Factory init(Leo p) {
                if(p == null)
                    throw (new NullPointerException("Recognizer is null"));
                this.p = p;
                return (this);
            }

            public Item lastCreated() {
                return last;
            }

            public void reset() {
                last = null;
            }

            public Item createElement(Rule rule) {
                if(rule == null)
                    throw (new NullPointerException("Rule"));
                last = new Item(rule, 0, p.cp);
                return last;
            }

            public static Factory getThreadLocal(Leo p) {
                return TLV.get().init(p);
            }
            private static final ThreadLocal<Factory> TLV =
                    new ThreadLocal<Factory>() {

                        protected Factory initialValue() {
                            return new Factory();
                        }
                    };

            @Override
            public Rule extractKey(Item x) {
                return x.rule;
            }
        }
    }

    private static class ItemSet implements Writeable {

        public Traversable.Set<Keyed<Token, Traversable.Set<Item>>> items;
        public Traversable.Set<Keyed<NonTerminal, Traversable.Set<Item.Transitive>>> transitiveItems;

        public Item addOrGetProductionItem(Rule rule,
                                           KeyedElementFactory<Item, Rule> f) {
            return addOrGetItemSetWithDotInFrontOf(
                    rule != null ? rule.rhsLength() > 0 ? rule.rhs[0] : null
                    : null).addOrGetMatch(rule, f);
        }

        public Item addOrGetItem(Item t) {
            if(t == null)
                return null;
            if(t.isTransitive())
                return addOrGetTransitiveSetFor(t.getTransitionSymbol()).
                        addOrGet((Item.Transitive)t);
            else
                return addOrGetItemSetWithDotInFrontOf(t.getSymbolAfterDot()).
                        addOrGet(t);
        }

        public boolean addItem(Item t) {
            if(t == null)
                return false;
            if(t.isTransitive())
                return addOrGetTransitiveSetFor(t.getTransitionSymbol()).
                        addOrGet((Item.Transitive)t) == t;
            else
                return addOrGetItemSetWithDotInFrontOf(t.getSymbolAfterDot()).
                        addOrGet(t) == t;
        }

        public Item getItem(Item t) {
            if(t == null)
                return null;
            if(t.isTransitive())
                return getTransitiveSetFor(t.getTransitionSymbol()).get(
                        (Item.Transitive)t);
            else
                return getItemsWithDotInFrontOf(t.getSymbolAfterDot()).get(t);
        }

        public Traversable.Set<Item> addOrGetItemSetWithDotInFrontOf(Token t) {
            if(t == null)
                t = Terminal.Epsilon;
            if(items == null)
                items = new BBTree<Keyed<Token, Traversable.Set<Item>>>();
            Keyed<Token, Traversable.Set<Item>> entry = items.addOrGetMatch(t,
                                                                            FACTORY.
                    get());
            if(entry.value == null)
                entry.value = new BBTree<Item>();
            return entry.value;
        }

        public Traversable.Set<Item> getItemsWithDotInFrontOf(Token t) {
            if(items == null)
                return null;
            if(t == null)
                t = Terminal.Epsilon;
            Keyed<Token, Traversable.Set<Item>> entry = items.getMatch(t);
            return entry != null ? entry.value : null;
        }

        public Traversable.Set<Item.Transitive> addOrGetTransitiveSetFor(
                NonTerminal t) {
            if(t == null)
                return null;
            if(transitiveItems == null)
                transitiveItems =
                        new BBTree<Keyed<NonTerminal, Traversable.Set<Item.Transitive>>>();
            Keyed<NonTerminal, Traversable.Set<Item.Transitive>> entry = transitiveItems.
                    addOrGetMatch(t, TRANSITIVES_FACTORY.get());
            if(entry.value == null)
                entry.value = new BBTree<Item.Transitive>();
            return entry.value;
        }

        public Traversable.Set<Item.Transitive> getTransitiveSetFor(
                NonTerminal t) {
            if(transitiveItems == null)
                return null;
            if(t == null)
                return null;
            Keyed<NonTerminal, Traversable.Set<Item.Transitive>> entry = transitiveItems.
                    getMatch(t);
            return entry != null ? entry.value : null;
        }

        public boolean isEmpty() {
            return !containsItems() && !containsTransitiveItems();
        }

        public boolean containsItems() {
            return items != null && !items.isEmpty();
        }

        public boolean containsTransitiveItems() {
            return transitiveItems != null && !transitiveItems.isEmpty();
        }

        public void makeFinal() {
            if(items != null) {
                items = items.toFinal();
                items.traverse(Traversable.IN_ORDER, FINALIZER);
            }
            if(transitiveItems != null) {
                transitiveItems = transitiveItems.toFinal();
                transitiveItems.traverse(Traversable.IN_ORDER,
                                         TRANSITIVES_FINALIZER);
            }
        }
        private static final TraversalActionPerformer<Keyed<Token, Traversable.Set<Item>>> FINALIZER = new TraversalActionPerformer<Keyed<Token, Traversable.Set<Item>>>() {

            public boolean perform(Keyed<Token, Traversable.Set<Item>> x,
                                   int order, Object o) {
                if(x != null && x.value != null)
                    x.value = x.value.toFinal();
                return false;
            }
        };
        private static final TraversalActionPerformer<Keyed<NonTerminal, Traversable.Set<Item.Transitive>>> TRANSITIVES_FINALIZER = new TraversalActionPerformer<Keyed<NonTerminal, Traversable.Set<Item.Transitive>>>() {

            public boolean perform(
                    Keyed<NonTerminal, Traversable.Set<Item.Transitive>> x,
                    int order, Object o) {
                if(x != null && x.value != null)
                    x.value = x.value.toFinal();
                return false;
            }
        };
        private static final ThreadLocal<Keyed.Factory<Token, Traversable.Set<Item>>> FACTORY =
                new ThreadLocal<Keyed.Factory<Token, Traversable.Set<Item>>>() {

                    protected Keyed.Factory<Token, Traversable.Set<Item>> initialValue() {
                        return new Keyed.Factory<Token, Traversable.Set<Item>>();
                    }
                };
        private static final ThreadLocal<Keyed.Factory<NonTerminal, Traversable.Set<Item.Transitive>>> TRANSITIVES_FACTORY =
                new ThreadLocal<Keyed.Factory<NonTerminal, Traversable.Set<Item.Transitive>>>() {

                    protected Keyed.Factory<NonTerminal, Traversable.Set<Item.Transitive>> initialValue() {
                        return new Keyed.Factory<NonTerminal, Traversable.Set<Item.Transitive>>();
                    }
                };

        public void writeToStream(Writer w) throws IOException {
//			w.write('(');
//			if(items != null)
//				items.writeToStream(w);
//			w.write(',');
//			if(transitiveItems != null)
//				transitiveItems.writeToStream(w);
//			w.write(')');
            w.write(toString());
        }

        public String toString() {
            return toStringBuilder(null).toString();
        }

        public StringBuilder toStringBuilder(StringBuilder sb) {
            if(sb == null)
                sb = new StringBuilder('(');
            if(items != null)
                items.toStringBuilder(sb);
            sb.append(',');
            if(transitiveItems != null)
                transitiveItems.toStringBuilder(sb);
            return sb.append(')');
        }
    }
}
