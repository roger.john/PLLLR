/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package plllr;

import bbtree.*;
import bbtree.Utils.AsymmetricComparable;

/**
 *
 * @author RJ
 */
public class LR0Table {
    public final Traversable.Set.Indexed<Row> rows = null;

    public static class Row implements Comparable<Row>, AsymmetricComparable<State> {
        public final State z;
        public Row(State z) {
            if(z==null)
                throw(new NullPointerException("state is null"));
            this.z = z;
        }
        public int compareTo(Row other) {
            if(other==null)
                return(1);
            return(z.compareTo(other.z));
        }
        public int asymmetricCompareTo(State other) {
            return(z.compareTo(other));
        }
    }
    public static class Item implements Comparable<Item> {
        public final Rule rule;
        public final int index;
        public Item(Rule rule,int index) {
            this.rule = rule;
            this.index = index;
        }
        public int compareTo(Item x) {
            if(x==null)
                return(1);
            if(rule==null)
                return(x.rule==null?index-x.index:-1);
            int c = rule.compareTo(x.rule);
            if(c!=0)
                return(c);
            return(index-x.index);
        }
        public boolean equals(Object o) {
            if((o==null)||!(o instanceof Item))
                return(false);
           return((this==o)||(compareTo((Item)o)==0));
        }
        public int hashCode() {
            return((rule!=null?rule.hashCode():0)+index);
        }
        public String toString() {
            StringBuilder b = new StringBuilder().append("[");
            if(rule!=null) {
                if(rule.lhs!=null)
                    b.append(rule.lhs.getEscapedName());
                b.append(" -> ");
                if(rule.rhs!=null)
                    for(int i=0;i<rule.rhs.length;i++) {
                        if(i==index)
                            b.append('.');
                        else if(i!=0)
                            b.append(' ');
                        if(rule.rhs[i]==null)
                            continue;
                        if(rule.rhs[i] instanceof plllr.NonTerminal)
                            b.append(((plllr.NonTerminal)(rule.rhs[i])).getEscapedName());
                        else
                            b.append(Terminal.String.escape(rule.rhs[i].toString()));
                    }
            } else {
                b.append("S' -> ");
                if(index==0)
                    b.append(".S");
                else if(index==1)
                    b.append("S.");
                else
                    throw(new RuntimeException("Illegal index "+index+" in start state"));
            }
            b.append("]");
            return(b.toString());
        }
    }
    public static class State implements Comparable<State> {
        public OrderedSet<Item> items;
        public State() {
            items = new BBTree<Item>();
        }
        public int compareTo(State o) {
            if(o==null)
                return(1);
            return(items.compareTo(o.items));
        }
    }

    public static abstract class Action implements Comparable<Action> {
        public static final Action Accept = new  Action() {
            public boolean equals(Object o) {
                return(this==o);
            }
            public int compareTo(Action o) {
                if(o==null)
                    return(1);
                else if(this==o)
                    return(0);
                else
                    return(-1);
            }
        };
        public static class Push extends Action {
            public State x;
            private Push() {}
            public Push(State x) {
                this.x = x;
            }
            public int compareTo(Action o) {
                if(o==null)
                    return(1);
                else if(this==o)
                    return(0);
                if(!(o instanceof Push))
                    return(this.getClass().getName().compareTo(o.getClass().getName()));
                State y = ((Push)o).x;
                return(x==null?(y==null?0:-1):x.compareTo(y));
            }
        }
        public static class Reduce extends Action {
            public Rule r;
            public int m;
            private Reduce() {}
            public Reduce(Rule r) {
                this(r,(r!=null)?r.rhsLength():0);
            }
            public Reduce(Rule r,int m) {
                this.r = r;
                if(r!=null)
                    this.m = m<0?0:m>r.rhsLength()?r.rhsLength():m;
                else
                    m = 0;
            }
            public boolean equals(Object o) {
                return((this==o)||((o!=null)&&(o instanceof Reduce)&&(compareTo((Reduce)o)==0)));
            }
            public int compareTo(Action o) {
                if(o==null)
                    return(1);
                else if(this==o)
                    return(0);
                if(!(o instanceof Reduce))
                    return(this.getClass().getName().compareTo(o.getClass().getName()));
                Rule s = ((Reduce)o).r;
                int c = (r==null)?((s==null)?0:-1):r.compareTo(s);
                if(c!=0)
                    return(c);
                return(m-((Reduce)o).m);
            }
        }
    }
}
