/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package plllr;

import bbtree.Utils.AsymmetricComparable;
import bbtree.Utils.KeyedElementFactory;
import java.io.Serializable;

/**
 *
 * @author RJ
 */
public class FirstSetOf<A extends Comparable<? super A>> implements
        Comparable<FirstSetOf<A>>, AsymmetricComparable<A>, Serializable {

    private static final long serialVersionUID = 0L;
    public final A owner;
    TerminalTrie firstSet;
    transient int changedInRound = -1;

    public FirstSetOf(A owner) {
        if(owner == null)
            throw (new NullPointerException("Owner is null"));
        this.owner = owner;
    }

    public FirstSetOf(A owner, TerminalTrie firstSet) {
        if(owner == null)
            throw (new NullPointerException("Owner is null"));
        this.owner = owner;
        this.firstSet = firstSet;
    }

    public int compareTo(FirstSetOf<A> x) {
        return (owner.compareTo(x.owner));
    }

    public int asymmetricCompareTo(A x) {
        if(x == null)
            return (1);
        return (owner.compareTo(x));
    }

    public TerminalTrie getFirstSet() {
        return (firstSet);
    }

    public boolean isFinal() {
        return ((firstSet == null) || firstSet.isFinal());
    }

    public void makeFinal() {
        if(isFinal())
            return;
        firstSet.makeFinal();
    }

    public String toString() {
        return (owner.toString());
    }

    public static class Factory<A extends Comparable<? super A>> implements
            KeyedElementFactory<FirstSetOf<A>, A> {

        public FirstSetOf<A> last;

        public void reset() {
            last = null;
        }

        public FirstSetOf<A> lastCreated() {
            return (last);
        }

        public FirstSetOf<A> createElement(A x) {
            if(x == null)
                throw (new NullPointerException("Null argument"));
            last = new FirstSetOf<A>(x);
            return (last);
        }

        @Override
        public A extractKey(FirstSetOf<A> x) {
            return x.owner;
        }
    }
    public static final ThreadLocal<Factory> FACTORY = new ThreadLocal<Factory>() {

        protected Factory initialValue() {
            return (new Factory());
        }
    };
}
