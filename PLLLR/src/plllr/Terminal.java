/*
 * Terminal.java
 *
 * Created on 7. Februar 2007, 15:36
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */
package plllr;

import bbtree.*;
import java.util.Comparator;
import java.io.*;

/**
 *
 * @author RJ
 */
public abstract class Terminal implements Token {

    public abstract boolean matches(Terminal x);

    public static boolean isPrintable(char x) {
        if((x >= '\u0020') && (x <= '\u007E'))
            return (true);
        return (false);
    }

    public static java.lang.String escapeChar(char x) {
        switch(x) {
            case '\\':
                return ("\\\\");
            case '\r':
                return ("\\r");
            case '\n':
                return ("\\n");
            case '\t':
                return ("\\t");
            case '\b':
                return ("\\b");
            case '\f':
                return ("\\f");
            case '\u0007':
                return ("\\a");
            case '\u001B':
                return ("\\e");
            default:
                if(isPrintable(x))
                    return ("" + x);
                else
                    return (escapeHex(x));
        }
    }

    public static java.lang.String escapeCodePoint(int cp) {
        if(!java.lang.Character.isSupplementaryCodePoint(cp))
            return (escapeChar((char)cp));
        else
            return (escapeHex(cp));
    }

    public static java.lang.String escapeHex(int x) {
        int digits = 0;
        char ec = 0, t;
        StringBuilder sb = new StringBuilder();
        if((0 <= x) && (x < 8))
            return (sb.append('\\').append((char)('0' + x)).toString());
        if((x & 0xFF) == x) {
            digits = 2;
            ec = 'x';
        } else if((x & 0xFFFF) == x) {
            digits = 4;
            ec = 'u';
        } else if((x & 0xFFFFFF) == x) {
            digits = 6;
            ec = 'U';
        } else {
            digits = 8;
            ec = 'X';
        }
        while(digits-- > 0) {
            t = (char)(x & 0xF);
            x >>>= 4;
            t += (t <= 9) ? '0' : 'A' - 10;
            sb.append(t);
        }
        sb.append(ec).append('\\');
        return (sb.reverse().toString());
    }

    public boolean equals(Object x) {
        return ((this == x) || ((x instanceof Token) && (compareTo((Token)x) == 0)));
    }

    public StringBuilder toStringBuilder(StringBuilder sb) {
        if(sb == null)
            sb = new StringBuilder();
        return sb.append(toString());
    }

    public void writeToStream(Writer w) throws IOException {
        if(w == null)
            return;
        w.write(toString());
    }
    public static final Terminal Epsilon = new Terminal() {

        private static final long serialVersionUID = 0L;

        public java.lang.String toString() {
            return ("#");
        }

        public boolean equals(Object o) {
            return (o == this);
        }

        public int hashCode() {
            return (0);
        }

        public int compareTo(Token x) {
            if(x == null)
                return (1);
            if(x == this)
                return (0);
            /*if(x instanceof Terminal) {
            java.lang.String t = x.toString();
            if(t==null)
            return(1);
            return(-t.length());
            }*/
            return (-1);
        }

        public boolean matches(Terminal x) {
            return (this == x);
        }
    };
    public static final Terminal End = new Terminal() {

        private static final long serialVersionUID = 0L;

        public java.lang.String toString() {
            return ("$");
        }

        public boolean equals(Object o) {
            return (o == this);
        }

        public int hashCode() {
            return (-1);
        }

        public int compareTo(Token x) {
            if(x == null)
                return (1);
            if(x == this)
                return (0);
            if(x == Epsilon)
                return (1);
            return (-1);
        }

        public boolean matches(Terminal x) {
            return (this == x);
        }
    };

    public static class Character extends Terminal {

        private static final long serialVersionUID = 0L;
        private int c;

        public static java.lang.String escapeCodePoint(int x) {
            if(x == '\'')
                return ("\\\'");
            else
                return (Terminal.escapeCodePoint(x));
        }

        public Character(int c) {
            setChar(c);
        }

        public int charValue() {
            return c;
        }

        public int getChar() {
            return c;
        }

        public int setChar(int c) {
            if((c < java.lang.Character.MIN_CODE_POINT) || (c > java.lang.Character.MAX_CODE_POINT))
                throw (new RuntimeException("pll.Terminal.Character.setChar: this is not an unicode-character: " + escapeCodePoint(
                        c)));
            int h = this.c;
            this.c = c;
            return h;
        }

        public java.lang.String toString() {
            return ("\'" + escapeCodePoint(c) + "\'");
        }

        public boolean equals(Object o) {
            return (this == o) || (o instanceof Token ? compareTo((Token)o) == 0 : (o instanceof java.lang.Character ? compareTo(
                    (java.lang.Character)o) == 0 : false));
        }

        public int hashCode() {
            return c;
        }

        public int compareTo(Token x) {
            if((x == null) || (x == Epsilon) || (x == End))
                return (1);
            if(!(x instanceof Terminal))
                return (-1);
            if(x instanceof Terminal.Character)
                return (c - ((Terminal.Character)x).charValue());
            if(!(x instanceof Terminal.String))
                return (getClass().getName().compareTo(x.getClass().getName()));
            java.lang.String t = ((Terminal.String)x).getString();
            if((t == null) || (t.length() == 0))
                return (1);
            int d = t.codePointAt(0);
            if(c != d)
                return (c - d);
            if(t.codePointCount(0, t.length()) > 1)
                return (-t.codePointAt(java.lang.Character.
                        isSupplementaryCodePoint(d) ? 2 : 1));
            return 0;
        }

        public int compareTo(java.lang.Character x) {
            if(x == null)
                return (1);
            return c - x.charValue();
        }

        public static Token valueOf(java.lang.String s) {
            if((s == null) || (s.length() == 0))
                return (Epsilon);
            if(s.length() == 1)
                return (new Character(s.charAt(0)));
            if((s.length() == 2) && java.lang.Character.isHighSurrogate(s.charAt(
                    0)))
                return (new Character(s.codePointAt(0)));
            Character[] r = new Character[s.codePointCount(0, s.length())];
            int k = 0;
            for(int i = 0; i < r.length; i++) {
                r[i] = new Character(s.codePointAt(k));
                k++;
                if(java.lang.Character.isSupplementaryCodePoint(r[i].charValue()))
                    k++;
            }
            return new Token.Group(r);
        }

        public static Token valueOf(String s) {
            if((s == null) || (s.length() == 0))
                return Epsilon;
            if(s.length() == 1)
                return new Character(s.getString().charAt(0));
            java.lang.String t = s.getString();
            if((t.length() == 2) && java.lang.Character.isHighSurrogate(t.charAt(
                    0)))
                return new Character(t.codePointAt(0));
            Character[] r = new Character[t.codePointCount(0, t.length())];
            int k = 0;
            for(int i = 0; i < r.length; i++) {
                r[i] = new Character(t.codePointAt(k));
                k++;
                if(java.lang.Character.isSupplementaryCodePoint(r[i].charValue()))
                    k++;
            }
            return new Token.Group(r);
        }

        public boolean matches(Terminal x) {
            return compareTo(x) == 0;
        }
    }

    public static class String extends Terminal {

        private static final long serialVersionUID = 0L;
        private java.lang.String s;

        public static java.lang.String escapeCodePoint(int x) {
            if(x == '\"')
                return "\\\"";
            else
                return Terminal.escapeCodePoint(x);
        }

        public static java.lang.String escape(java.lang.String s) {
            if(s == null)
                return ("");
            StringBuilder sb = new StringBuilder();
            int i = 0, l = s.length(), c;
            while(i < l) {
                c = s.codePointAt(i);
                if(java.lang.Character.isSupplementaryCodePoint(c))
                    i++;
                sb.append(escapeCodePoint(c));
                i++;
            }
            return sb.toString();
        }

        public String() {
            this(null);
        }

        public String(java.lang.String s) {
            this.s = s;
        }

        public java.lang.String getString() {
            return (s);
        }

        public java.lang.String setString(java.lang.String s) {
            java.lang.String h = this.s;
            this.s = s;
            return (h);
        }

        public int length() {
            return (s != null ? s.length() : 0);
        }

        public java.lang.String toString() {
            return ("\"" + escape(s) + "\"");
        }

        public boolean equals(Object o) {
            return ((this == o) || (o instanceof Token ? compareTo((Token)o) == 0 : (o instanceof java.lang.Character ? compareTo(
                    (java.lang.Character)o) == 0 : (o instanceof java.lang.String ? compareTo(
                    (java.lang.String)o) == 0 : false))));
        }

        public int hashCode() {
            return (s != null ? s.hashCode() : 0);
        }

        public int compareTo(Token x) {
            if((x == null) || (x == Epsilon) || (x == End))
                return (1);
            if(!(x instanceof Terminal))
                return (-1);
            if(x instanceof Terminal.Character) {
                int c = ((Terminal.Character)x).charValue();
                if((s == null) || (s.length() == 0))
                    return (-1);
                if((s.length() == 1) || (s.charAt(0) != c))
                    return (s.charAt(0) - c);
                return ((int)s.charAt(1));
            }
            if(!(x instanceof Terminal.String))
                return (getClass().getName().compareTo(x.getClass().getName()));
            java.lang.String t = ((Terminal.String)x).getString();
            if(t == null)
                return (s != null ? 1 : 0);
            return (s.compareTo(t));
        }

        public int compareTo(java.lang.Character x) {
            if(x == null)
                return (1);
            if((s == null) || (s.length() == 0))
                return (-1);
            return (s.compareTo(x.toString()));
        }

        public int compareTo(java.lang.String x) {
            if(x == null)
                return (s == null ? 0 : 1);
            if(s == null)
                return (-1);
            return (s.compareTo(x));
        }

        public boolean matches(Terminal x) {
            return (compareTo(x) == 0);
        }
    }

    public static class Tagged extends Terminal {

        private static final long serialVersionUID = 0L;
        private java.lang.String tag;

        public static java.lang.String escapeCodePoint(char x) {
            switch(x) {
                case '<':
                    return ("\\<");
                case '>':
                    return ("\\>");
            }
            return (Terminal.escapeCodePoint(x));
        }

        public Tagged(java.lang.String tag) {
            this.tag = tag;
        }

        public int compareTo(Token x) {
            if((x == null) || (x == Epsilon) || (x == End))
                return (1);
            if(!(x instanceof Terminal))
                return (-1);
            if(x instanceof Terminal.Tagged) {
                java.lang.String y = ((Terminal.Tagged)x).getTag();
                if(tag == null)
                    return (y == null ? -1 : 0);
                if(y == null)
                    return (1);
                return (tag.compareTo(y));
            }
            return (getClass().getName().compareTo(x.getClass().getName()));
        }

        public boolean equals(Object o) {
            return ((this == o) || (o instanceof Token ? compareTo((Token)o) == 0 : (o instanceof java.lang.String ? compareTo(
                    (java.lang.String)o) == 0 : false)));
        }

        public int compareTo(java.lang.String x) {
            if(x == null)
                return (tag == null ? 0 : 1);
            if(tag == null)
                return (-1);
            return (tag.compareTo(x));
        }

        public java.lang.String toString() {
            if(tag == null)
                return ("<>");
            StringBuilder sb = new StringBuilder().append('<');
            int i = 0, l = tag.length(), c;
            while(i < l) {
                c = tag.codePointAt(i);
                if(java.lang.Character.isSupplementaryCodePoint(c))
                    i++;
                sb.append(escapeCodePoint(c));
                i++;
            }
            return (sb.append(">").toString());
        }

        public int hashCode() {
            return (toString().hashCode());
        }

        public java.lang.String getTag() {
            return (tag);
        }

        public java.lang.String setTag(java.lang.String tag) {
            java.lang.String h = this.tag;
            this.tag = tag;
            return (tag);
        }

        public boolean matches(Terminal x) {
            return (compareTo(x) == 0);
        }
    }

    public static abstract class CharacterClass extends Terminal implements
            Cloneable {

        public abstract boolean contains(int x);

        public abstract boolean isEmpty();

        public abstract int size();

        public abstract int distanceTo(int x);

        public abstract int[] toArray();

        public abstract CharacterClass add(int x);

        public abstract CharacterClass join(CharacterClass x);

        public abstract CharacterClass intersect(CharacterClass x);

        public abstract CharacterClass without(CharacterClass x);

        public abstract CharacterClass complement();

        public abstract Object clone();

        public abstract int compareTo(Token x);

        public abstract java.lang.String toString();

        public abstract int hashCode();

        public boolean matches(Terminal x) {
            if(x == null)
                return (false);
            if(x instanceof Character)
                return (contains(((Character)x).charValue()));
            return (false);
        }

        public static java.lang.String escapeCodePoint(int x) {
            switch(x) {
                case '[':
                    return ("\\[");
                case ']':
                    return ("\\]");
                case '.':
                    return ("\\.");
                case '-':
                    return ("\\-");
            }
            return (Terminal.escapeCodePoint(x));
        }

        public static CharacterClass fromString(java.lang.String s) {
            if((s == null) || (s.length() == 0))
                return (Empty);
            CharacterClass[] t = new CharacterClass[s.codePointCount(0,
                                                                     s.length())];
            int i = 0, k = 0;
            while(i < t.length) {
                t[i] = new Single(s.codePointAt(k));
                if(java.lang.Character.isSupplementaryCodePoint(s.codePointAt(k)))
                    k++;
                k++;
                i++;
            }
            return (new Union(t));
        }
        public static final java.lang.String[] PREDEFINED_CLASSES_NAMES = new java.lang.String[] {
            "None", "All",
            "Lower", "Upper", "ASCII",
            "Alpha", "Digit",
            "Alnum",
            "Word",
            "Punct",
            "Graph", "Print", "Blank",
            "Cntrl",
            "XDigit",
            "Space"
        };
        public static final CharacterClass[] PREDEFINED_CLASSES = new CharacterClass[] {
            new Range(1, 0), new Range(java.lang.Character.MIN_CODE_POINT,
                                       java.lang.Character.MAX_CODE_POINT),
            new Range('a', 'z'), new Range('A', 'Z'), new Range(0, 127),
            new Union(new CharacterClass[] {new Range('A', 'Z'), new Range('a',
                                                                           'z')},
                      true), new Range('0', '9'),
            new Union(new CharacterClass[] {new Range('0', '9'), new Range('A',
                                                                           'Z'), new Range(
                'a', 'z')}, true),
            new Union(new CharacterClass[] {new Range('0', '9'), new Range('A',
                                                                           'Z'), new Single(
                '_'), new Range('a', 'z')}, true),
            new Union(new CharacterClass[] {new Range('!', '/'), new Range(':',
                                                                           '@'), new Range(
                '[', '\u0060'), new Range('{', '~')}, true),
            new Range('!', '~'), new Range(' ', '~'), new Union(new Single('\t'), new Single(
            ' ')),
            new Union(
            new CharacterClass[] {new Range(0, 0x1F), new Single(0x7F)}),
            new Union(new CharacterClass[] {new Range('0', '9'), new Range('A',
                                                                           'F'), new Range(
                'a', 'f')}, true),
            fromString(" \t\n\u000B\f\r")
        };

        static {
            if(PREDEFINED_CLASSES_NAMES.length != PREDEFINED_CLASSES.length)
                System.out.println(
                        "\r\npll.Terminal.CharacterClass.init: predefined character classes and their names are not of the same size");
        }

        public static CharacterClass forName(java.lang.String s) {
            if((s == null) || (s.length() == 0))
                return (Empty);
            if(PREDEFINED_CLASSES_NAMES.length != PREDEFINED_CLASSES.length)
                System.out.println(
                        "\r\npll.Terminal.CharacterClass.forName: predefined character classes and their names are not of the same size");
            int i = 0;
            while(i < PREDEFINED_CLASSES_NAMES.length)
                if(s.equalsIgnoreCase(PREDEFINED_CLASSES_NAMES[i]))
                    break;
                else
                    i++;
            if(i >= PREDEFINED_CLASSES_NAMES.length)
                return (Empty);
            if(i >= PREDEFINED_CLASSES.length) {
                System.out.println(
                        "\r\npll.Terminal.CharacterClass.forName: found a name but not its corresponding character class (" + i + ">=" + PREDEFINED_CLASSES.length);
                return (Empty);
            }
            if(PREDEFINED_CLASSES[i] != null)
                return ((CharacterClass)(PREDEFINED_CLASSES[i].clone()));
            else {
                System.out.println(
                        "\r\npll.Terminal.CharacterClass.forName: null entry at position " + i + " in predefined character classe");
                return (Empty);
            }
        }
        public static final CharacterClass Empty = new CharacterClass() {

            public boolean contains(int x) {
                return (false);
            }

            public boolean isEmpty() {
                return (true);
            }

            public int size() {
                return (0);
            }

            public int distanceTo(int x) {
                return (Integer.MAX_VALUE);
            }

            public int[] toArray() {
                return (null);
            }

            public CharacterClass add(int x) {
                return (new Single(x));
            }

            public CharacterClass join(CharacterClass x) {
                return ((x == null) ? this : x);
            }

            public CharacterClass intersect(CharacterClass x) {
                return (this);
            }

            public CharacterClass without(CharacterClass x) {
                return (this);
            }

            public CharacterClass complement() {
                return (new Range(java.lang.Character.MIN_CODE_POINT,
                                  java.lang.Character.MAX_CODE_POINT));
            }

            public Object clone() {
                return (this);
            }

            public int compareTo(Token x) {
                if((x == null) || (x == Epsilon) || (x == End))
                    return (1);
                if(x == this)
                    return (0);
                if(!(x instanceof Terminal))
                    return (-1);
                if(!(x instanceof CharacterClass))
                    return (1);
                return (-1);
            }

            public java.lang.String toString() {
                return ("[]");
            }

            public int hashCode() {
                return (-2);
            }
        };

        public static class Single extends CharacterClass {

            private static final long serialVersionUID = 0L;
            private int x;

            public Single(int x) {
                setX(x);
            }

            public int getX() {
                return (x);
            }

            public int setX(int x) {
                if((x < java.lang.Character.MIN_CODE_POINT) || (x > java.lang.Character.MAX_CODE_POINT))
                    throw (new RuntimeException("pll.Terminal.CharacterClass.Single.setX: this is not an unicode-character: " + escapeCodePoint(
                            x)));
                int h = this.x;
                this.x = x;
                return (h);
            }

            public boolean contains(int x) {
                return (this.x == x);
            }

            public boolean isEmpty() {
                return (false);
            }

            public int size() {
                return (1);
            }

            public int[] toArray() {
                int[] r = new int[1];
                r[0] = x;
                return (r);
            }

            public CharacterClass add(int x) {
                if(x == this.x)
                    return (this);
                if(x == this.x + 1)
                    return (new Range(this.x, x));
                if(x == this.x - 1)
                    return (new Range(x, this.x));
                return (new Union(new Single(x), this));
            }

            public CharacterClass join(CharacterClass x) {
                if(x == null)
                    return (this);
                return (x.add(this.x));
            }

            public CharacterClass intersect(CharacterClass x) {
                if((x == null) || (!x.contains(this.x)))
                    return (null);
                return (this);
            }

            public CharacterClass without(CharacterClass x) {
                if((x == null) || (!x.contains(this.x)))
                    return (this);
                return (null);
            }

            public CharacterClass complement() {
                if(x == java.lang.Character.MIN_CODE_POINT)
                    return (new Range(java.lang.Character.MIN_CODE_POINT + 1,
                                      java.lang.Character.MAX_CODE_POINT));
                if(x == java.lang.Character.MAX_CODE_POINT)
                    return (new Range(java.lang.Character.MIN_CODE_POINT,
                                      java.lang.Character.MAX_CODE_POINT - 1));
                return (new Union(new Range(java.lang.Character.MIN_CODE_POINT,
                                            x - 1),
                                  new Range(x + 1,
                                            java.lang.Character.MAX_CODE_POINT)));
            }

            public int compareTo(Token x) {
                if((x == null) || (x == Epsilon) || (x == End) || (x == Empty))
                    return (1);
                if(!(x instanceof Terminal))
                    return (-1);
                if(x instanceof Single)
                    return (this.x - ((Single)x).x);
                return (getClass().getName().compareTo(x.getClass().getName()));
            }

            public java.lang.String toString() {
                return ("[" + escapeCodePoint(x) + "]");
            }

            public int hashCode() {
                return (x);
            }

            public int distanceTo(int x) {
                return (x - this.x);
            }

            public Object clone() {
                return (new Single(x));
            }
        }

        public static class Range extends CharacterClass {

            private static final long serialVersionUID = 0L;
            private int low, high;

            public Range(int l, int h) {
                setBorders(l, h);
            }

            int getLowBorder() {
                return (low);
            }

            int setLowBorder(int low) {
                if((low < java.lang.Character.MIN_CODE_POINT) || (low > java.lang.Character.MAX_CODE_POINT))
                    throw (new RuntimeException("pll.Terminal.CharacterClass.Range.setLowBorder: this is not an unicode-character: " + escapeCodePoint(
                            low)));
                int h = this.low;
                this.low = low;
                return (h);
            }

            int getHighBorder() {
                return (high);
            }

            int setHighBorder(int high) {
                if((high < java.lang.Character.MIN_CODE_POINT) || (high > java.lang.Character.MAX_CODE_POINT))
                    throw (new RuntimeException("pll.Terminal.CharacterClass.Range.setHighBorder: this is not an unicode-character: " + escapeCodePoint(
                            high)));
                int h = this.high;
                this.high = high;
                return (h);
            }

            int setBorders(int low, int high) {
                setLowBorder(low);
                setHighBorder(high);
                return (high - low);
            }

            public boolean contains(int x) {
                return ((x >= low) && (x <= high));
            }

            public boolean isEmpty() {
                return (low > high);
            }

            public int size() {
                if(isEmpty())
                    return (0);
                return (high - low + 1);
            }

            public int[] toArray() {
                if(isEmpty())
                    return (null);
                int[] r = new int[size()];
                for(int i = 0; i < r.length; i++)
                    r[i] = low + i;
                return (r);
            }

            public CharacterClass add(int x) {
                if(contains(x))
                    return (this);
                if(isEmpty())
                    return (new Single(x));
                else if(low == x + 1)
                    return (new Range(x, high));
                else if(high == x - 1)
                    return (new Range(low, x));
                else
                    return (x < low ? new Union(new Single(x), this)
                            : new Union(this, new Single(x)));
            }

            public CharacterClass join(CharacterClass x) {
                if(x == null)
                    return (isEmpty() ? Empty : this);
                if(isEmpty())
                    return (x);
                if(x.isEmpty())
                    return (this);
                if(x instanceof Single)
                    return (add(((Single)x).x));
                if(x instanceof Union)
                    return (x.join(this));
                if(!(x instanceof Range))
                    throw (new RuntimeException("Terminal.CharacterClass.Range.join: expectet Range, got " + x.
                            getClass()));
                Range y = (Range)x;
                if(y.low >= low)
                    if(y.low <= high)
                        if(y.high > high)
                            if(y.low == low)
                                return (y);
                            else
                                return (new Range(low, y.high));
                        else
                            return (this);
                    else if(y.low == high + 1)
                        return (new Range(low, y.high));
                    else
                        return (new Union(new CharacterClass[] {this, x}, true));
                else if(low <= y.high)
                    if(y.high >= high)
                        return (y);
                    else
                        return (new Range(y.low, high));
                else if(low == y.high + 1)
                    return (new Range(y.low, high));
                else
                    return (new Union(new CharacterClass[] {x, this}, true));
            }

            public CharacterClass intersect(CharacterClass x) {
                if((x == null) || isEmpty() || x.isEmpty())
                    return (Empty);
                if(x instanceof Single)
                    return (x.intersect(this));
                if(x instanceof Union)
                    return (x.intersect(this));
                if(!(x instanceof Range))
                    throw (new RuntimeException("Terminal.CharacterClass.CharacterSet.Range.intersect: expectet Range, got " + x.
                            getClass()));
                Range y = (Range)x;
                if(y.low >= low)
                    if(y.high <= high)
                        return (y);
                    else if(y.low > high)
                        return (Empty);
                    else if(y.low == high)
                        return (new Single(high));
                    else
                        return (new Range(y.low, low));
                else {
                    if(high <= y.high)
                        return (this);
                    if(low > y.high)
                        return (Empty);
                    if(low == y.high)
                        return (new Single(low));
                    else
                        return (new Range(low, y.high));
                }
            }

            public CharacterClass without(CharacterClass x) {
                if(isEmpty())
                    return (Empty);
                if((x == null) || x.isEmpty())
                    return (this);
                if(x instanceof Single) {
                    Single y = (Single)x;
                    if((low > y.x) || (y.x > high))
                        return (this);
                    switch(high - low) {
                        case 0:
                            return (Empty);
                        case 1:
                            if(y.x == low)
                                return (new Single(high));
                            else
                                return (new Single(low));
                        default:
                            if(y.x == low)
                                return (new Range(low + 1, high));
                            else if(y.x == high)
                                return (new Range(low, high - 1));
                            else if(y.x == low + 1)
                                if(y.x == high - 1)
                                    return (new Union(new CharacterClass[] {new Single(
                                                low), new Single(high)}, true));
                                else
                                    return (new Union(new CharacterClass[] {new Single(
                                                y.x - 1), new Range(y.x + 1,
                                                                    high)}, true));
                            else if(y.x == high - 1)
                                return (new Union(new CharacterClass[] {new Range(
                                            low, y.x - 1), new Single(high)},
                                                  true));
                            else
                                return (new Union(new CharacterClass[] {new Range(
                                            low, y.x - 1), new Range(y.x + 1,
                                                                     high)},
                                                  true));
                    }
                }
                if(x instanceof Union) {
                    Union y = (Union)x;
                    CharacterClass r = this;
                    for(int i = 0; i < y.m.length; i++)
                        if((r = r.without(y.m[i])).isEmpty())
                            return (Empty);
                    return (r);
                }
                if(!(x instanceof Range))
                    throw (new RuntimeException("Terminal.CharacterClass.CharacterSet.Range.intersect: expectet Range, got " + x.
                            getClass()));
                Range y = (Range)x;
                if(y.low <= low) {
                    if(y.high < low)
                        return (this);
                    if(y.high >= high)
                        return (Empty);
                    else if(y.high + 1 == high)
                        return (new Single(high));
                    else
                        return (new Range(y.high + 1, high));
                } else {
                    if(y.high >= high)
                        if(y.low - 1 == low)
                            return (new Single(low));
                        else
                            return (new Range(low, y.low - 1));
                    CharacterClass r1 = null, r2 = null;
                    if(y.low - 1 == low) {
                        r1 = new Single(low);
                        if(y.high + 1 == high)
                            r2 = new Single(high);
                        else
                            r2 = new Range(y.high + 1, high);
                    } else {
                        r1 = new Range(low, y.low - 1);
                        if(y.high + 1 == high)
                            r2 = new Single(high);
                        else
                            r2 = new Range(y.high + 1, high);
                    }
                    return (new Union(new CharacterClass[] {r1, r2}, true));
                }
            }

            public CharacterClass complement() {
                if(isEmpty())
                    return (new Range(java.lang.Character.MIN_CODE_POINT,
                                      java.lang.Character.MAX_CODE_POINT));
                else if(low == java.lang.Character.MIN_CODE_POINT)
                    if(high == java.lang.Character.MAX_CODE_POINT)
                        return (Empty);
                    else
                        return (new Range(high + 1,
                                          java.lang.Character.MAX_CODE_POINT));
                else if(high == java.lang.Character.MAX_CODE_POINT)
                    return (new Range(java.lang.Character.MIN_CODE_POINT,
                                      low - 1));
                else
                    return (new Union(new CharacterClass[] {new Range(
                                java.lang.Character.MIN_CODE_POINT, low - 1), new Range(
                                high + 1, java.lang.Character.MAX_CODE_POINT)},
                                      true));
            }

            public int compareTo(Token x) {
                if((x == null) || (x == Epsilon) || (x == End) || (x == Empty))
                    return (1);
                if(!(x instanceof Terminal))
                    return (-1);
                if(x instanceof Range) {
                    Range y = (Range)x;
                    if(y.isEmpty())
                        return (isEmpty() ? 0 : 1);
                    if(isEmpty())
                        return (-1);
                    if(y.low != this.low)
                        return (this.low - y.low);
                    return (this.high - y.high);
                }
                return (getClass().getName().compareTo(x.getClass().getName()));
            }

            public java.lang.String toString() {
                return ("[" + escapeCodePoint(low) + "-" + escapeCodePoint(high) + "]");
            }

            public int hashCode() {
                return (high + low);
            }

            public int distanceTo(int x) {
                if(low > high)
                    return (Integer.MAX_VALUE);
                if(x <= low)
                    return (x - low);
                if(x >= high)
                    return (x - high);
                return (0);
            }

            public Object clone() {
                if(low <= high)
                    return (new Range(low, high));
                else
                    return (null);
            }
        }

        public static class Union extends CharacterClass {

            private static final long serialVersionUID = 0L;
            private static final Comparator<CharacterClass> CompressionComparator = new Comparator<CharacterClass>() {

                public int compare(CharacterClass x, CharacterClass y) {
                    if(x == null)
                        return (y == null ? 0 : -1);
                    if(y == null)
                        return (1);
                    if(x.isEmpty())
                        return (y.isEmpty() ? 0 : -1);
                    if(y.isEmpty())
                        return (1);
                    if(x instanceof Single) {
                        int i = y.distanceTo(((Single)x).x);
                        return ((i == 1) || (i == 0) || (i == -1) ? 0 : i);
                    }
                    if(y instanceof Single) {
                        int i = -x.distanceTo(((Single)y).x);
                        return ((i == 1) || (i == 0) || (i == -1) ? 0 : i);
                    }
                    if((x instanceof Range) && (y instanceof Range)) {
                        Range a = (Range)x;
                        Range b = (Range)y;
                        if(a.high < b.low - 1)
                            return (-1);
                        if(b.high < a.low - 1)
                            return (1);
                        return (0);
                    }
                    return (x.compareTo(y));
                }

                public boolean equals(Object o) {
                    return (o == this);
                }
            };

            private static class CompressionMerger implements
                    Merger<CharacterClass> {

                public int merges = 0;
                public CharacterClass lastMergeResult = null;

                public Merger<CharacterClass> clone() {
                    return (new CompressionMerger());
                }

                public CharacterClass merge(CharacterClass x, CharacterClass y) {
                    if(x == null)
                        return (y);
                    if(y == null)
                        return (x);
                    if(x.isEmpty())
                        return (y);
                    if(y.isEmpty())
                        return (x);
                    if(x instanceof Single)
                        if(y instanceof Single)
                            switch(((Single)x).x - ((Single)y).x) {
                                case -1:
                                    merges++;
                                    return (lastMergeResult = new Range(
                                            ((Single)x).x, ((Single)y).x));
                                case 0:
                                    return (x);
                                case 1:
                                    merges++;
                                    return (lastMergeResult = new Range(
                                            ((Single)y).x, ((Single)x).x));
                                default:
                                    throw (new RuntimeException(
                                            "pll.Terminal.CharacterClass.Union.CompressionMerger.merge: " + x + " and " + y + " are not adjacent"));
                            }
                        else if(y instanceof Range)
                            switch(((Range)y).distanceTo(((Single)x).x)) {
                                case -1:
                                    merges++;
                                    ((Range)y).low = ((Single)x).x;
                                    return (lastMergeResult = y);
                                case 0:
                                    merges++;
                                    return (lastMergeResult = y);
                                case 1:
                                    merges++;
                                    ((Range)y).high = ((Single)x).x;
                                    return (lastMergeResult = y);
                                default:
                                    throw (new RuntimeException(
                                            "pll.Terminal.CharacterClass.Union.CompressionMerger.merge: " + x + " and " + y + " are not adjacent"));
                            }
                        else
                            throw (new RuntimeException(
                                    "pll.Terminal.CharacterClass.Union.CompressionMerger.merge: cannot merge " + x + " with " + y));
                    else if(x instanceof Range)
                        if(y instanceof Single)
                            switch(((Range)x).distanceTo(((Single)y).x)) {
                                case -1:
                                    merges++;
                                    ((Range)x).low = ((Single)y).x;
                                    return (lastMergeResult = x);
                                case 0:
                                    return (x);
                                case 1:
                                    merges++;
                                    ((Range)x).high = ((Single)y).x;
                                    return (lastMergeResult = x);
                                default:
                                    throw (new RuntimeException(
                                            "pll.Terminal.CharacterClass.Union.CompressionMerger.merge: " + x + " and " + y + " are not adjacent"));
                            }
                        else if(y instanceof Range) {
                            Range a = (Range)x;
                            Range b = (Range)y;
                            if(a.low <= b.low)
                                if(a.high >= b.high)
                                    return (a);
                                else if(a.high >= b.low - 1) {
                                    merges++;
                                    a.high = b.high;
                                    return (lastMergeResult = a);
                                } else
                                    throw (new RuntimeException(
                                            "pll.Terminal.CharacterClass.Union.CompressionMerger.merge: " + x + " and " + y + " are not adjacent or overlapping"));
                            else if(b.high >= a.high) {
                                merges++;
                                return (lastMergeResult = b);
                            } else if(b.high >= a.low - 1) {
                                merges++;
                                a.low = b.low;
                                return (lastMergeResult = a);
                            } else
                                throw (new RuntimeException(
                                        "pll.Terminal.CharacterClass.Union.CompressionMerger.merge: " + x + " and " + y + " are not adjacent or overlapping"));
                        } else
                            throw (new RuntimeException(
                                    "pll.Terminal.CharacterClass.Union.CompressionMerger.merge: cannot merge " + x + " with " + y));
                    else
                        throw (new RuntimeException(
                                "pll.Terminal.CharacterClass.Union.CompressionMerger.merge: cannot merge " + x + " with " + y));
                }

                public void reset() {
                    merges = 0;
                    lastMergeResult = null;
                }

                public boolean hasChanged() {
                    return (merges > 0);
                }
            }
            CharacterClass[] m;

            public Union(CharacterClass[] m) {
                this.m = m;
                compress();
            }

            Union(CharacterClass[] m, boolean alreadyCompressed) {
                this.m = m;
                if(!alreadyCompressed)
                    compress();
            }

            public Union(CharacterClass a, CharacterClass b) {
                m = new CharacterClass[] {a, b};
                compress();
            }

            private void compress() {
                if(isEmpty())
                    return;
                CompressionMerger c = new CompressionMerger();
                BBTree<CharacterClass> b = new BBTree<CharacterClass>(
                        CompressionComparator);
                int i, j;
                for(i = 0; i < m.length; i++)
                    if((m[i] == null) || (m[i].isEmpty()))
                        continue;
                    else if((m[i] instanceof Range) || (m[i] instanceof Single))
                        b.add(m[i], c);
                    else if(m[i] instanceof Union) {
                        Union u = (Union)m[i];
                        for(j = 0; j < u.m.length; j++)
                            if((u.m[j] == null) || (u.m[j].isEmpty()))
                                continue;
                            else if((u.m[j] instanceof Range) || (u.m[j] instanceof Single))
                                b.add(u.m[j], c);
                            else
                                throw (new RuntimeException("pll.Terminal.CharacterClass.Union.compress: unexpected Type: " + u.m[j].
                                        getClass().getName()));

                    } else
                        throw (new RuntimeException("pll.Terminal.CharacterClass.Union.compress: unexpected Type: " + m[i].
                                getClass().getName()));
                while(c.hasChanged()) {
                    c.reset();
                    b.reorder(CompressionComparator, c, false);
                }
                if(b.size() > 0)
                    m = b.toArray(new CharacterClass[b.size()]);
                else
                    m = null;
                b.clear();
            }

            public boolean contains(int x) {
                if((m == null) || (m.length == 0))
                    return (false);
                return (search(x, 0, m.length - 1) >= 0);
            }

            private int search(int x, int low, int high) {
                if(low > high)
                    return (-1);
                int mid = (low + high) >>> 1;
                int d = m[mid].distanceTo(x);
                if(d == 0)
                    return (mid);
                else if(d < 0)
                    return (search(x, low, mid - 1));
                else
                    return (search(x, mid + 1, high));
            }

            public boolean isEmpty() {
                return ((m == null) || (m.length == 0));
            }

            public int size() {
                if((m == null) || (m.length == 0))
                    return (0);
                int s = 0;
                for(int i = 0; i < m.length; i++)
                    s += m[i].size();
                return (s);
            }

            public int[] toArray() {
                if((m == null) || (m.length == 0))
                    return (null);
                int[] r = new int[size()];
                int i, k = 0, j, h;
                for(i = 0; i < m.length; i++)
                    if(m[i] instanceof Single)
                        r[k++] = ((Single)m[i]).x;
                    else if(m[i] instanceof Range) {
                        h = ((Range)m[i]).high;
                        for(j = ((Range)m[i]).low; j <= h; j++)
                            r[k++] = j;
                    } else
                        throw (new RuntimeException(
                                "pll.Terminal.CharacterClass.Union.toArray: unknown member type"));
                return (r);
            }

            public CharacterClass add(int x) {
                if(contains(x))
                    return (this);
                else if((m == null) || (m.length == 0))
                    return (new Single(x));
                else
                    return (new Union(this, new Single(x)));
            }

            public CharacterClass join(CharacterClass x) {
                if((m == null) || (m.length == 0))
                    return (x.isEmpty() ? Empty : x);
                else if((x == null) || x.isEmpty())
                    return (this);
                else
                    return (new Union(this, x));
            }

            public CharacterClass intersect(CharacterClass x) {
                if((m == null) || (m.length == 0) || (x == null) || x.isEmpty())
                    return (Empty);
                int i, k = 0;
                CharacterClass[] r = new CharacterClass[m.length];
                for(i = 0; i < m.length; i++)
                    r[i] = m[i].intersect(x);
                return (new Union(r));
            }

            public CharacterClass without(CharacterClass x) {
                if((m == null) || (m.length == 0))
                    return (Empty);
                if((x == null) || x.isEmpty())
                    return (this);
                int i, k = 0;
                CharacterClass[] r = new CharacterClass[m.length];
                for(i = 0; i < m.length; i++)
                    r[i] = m[i].without(x);
                return (new Union(r));
            }

            public CharacterClass complement() {
                if(isEmpty())
                    return (new Range(java.lang.Character.MIN_CODE_POINT,
                                      java.lang.Character.MAX_CODE_POINT));
                int k = 1;
                if(m[0].contains(java.lang.Character.MIN_CODE_POINT))
                    k--;
                if(m[m.length - 1].contains(java.lang.Character.MAX_CODE_POINT))
                    k--;
                CharacterClass[] m2 = new CharacterClass[m.length + k];
                int d, u;
                if(m[0].contains(java.lang.Character.MIN_CODE_POINT))
                    k = 0;
                else if(m[0].contains(java.lang.Character.MIN_CODE_POINT + 1)) {
                    k = 1;
                    m2[0] = new Single(java.lang.Character.MIN_CODE_POINT);
                } else {
                    k = 1;
                    if(m[0] instanceof Single)
                        u = ((Single)m[0]).x - 1;
                    else if(m[0] instanceof Range)
                        u = ((Range)m[0]).low - 1;
                    else
                        throw (new RuntimeException(
                                "pll.Terminal.CharacterClass.Union.complement: unsupportet type in union: " + m[0]));
                    m2[0] = new Range(java.lang.Character.MIN_CODE_POINT, u);
                }
                for(int i = 0; i < m.length; i++) {
                    if(m[i] instanceof Single)
                        d = ((Single)m[i]).x;
                    else if(m[i] instanceof Range)
                        d = ((Range)m[i]).high;
                    else
                        throw (new RuntimeException(
                                "pll.Terminal.CharacterClass.Union.complement: unsupportet type in union: " + m[i]));
                    if(i < m.length - 1)
                        if(m[i + 1] instanceof Single)
                            u = ((Single)m[i + 1]).x - 1;
                        else if(m[i] instanceof Range)
                            u = ((Range)m[i + 1]).low - 1;
                        else
                            throw (new RuntimeException(
                                    "pll.Terminal.CharacterClass.Union.complement: unsupportet type in union: " + m[i + 1]));
                    else
                        u = java.lang.Character.MAX_CODE_POINT;
                    switch(u - d) {
                        case 0:
                            break;
                        case 1:
                            m2[k] = new Single(u);
                            k++;
                            break;
                        default:
                            m2[k] = new Range(d + 1, u);
                            k++;
                            break;
                    }
                }
                return (new Union(m2, true));
            }

            public int compareTo(Token x) {
                if((x == null) || (x == Epsilon) || (x == End) || (x == Empty))
                    return (1);
                if(!(x instanceof Terminal))
                    return (-1);
                if(x instanceof Union) {
                    Union y = (Union)x;
                    if(y.isEmpty())
                        return (isEmpty() ? 0 : 1);
                    if(isEmpty())
                        return (-1);
                    int c, i, k = m.length <= y.m.length ? m.length : y.m.length;
                    for(i = 0; i < k; i++) {
                        c = m[i].compareTo(y.m[i]);
                        if(c != 0)
                            return (c);
                    }
                    return (m.length - y.m.length);
                }
                return (getClass().getName().compareTo(x.getClass().getName()));
            }

            public java.lang.String toString() {
                if(m == null)
                    return ("[]");
                StringBuilder sb = new StringBuilder();
                sb.append('[');
                for(int i = 0; i < m.length; i++)
                    if((m[i] == null) || m[i].isEmpty())
                        throw (new RuntimeException(
                                "pll.Terminal.CharacterClass.Union.toString: empty entry found at position " + i + ": " + m[i]));
                    else if(m[i] instanceof Single)
                        sb.append(escapeCodePoint(((Single)m[i]).x));
                    else if(m[i] instanceof Range) {
                        sb.append(escapeCodePoint(((Range)m[i]).low));
                        sb.append('-');
                        sb.append(escapeCodePoint(((Range)m[i]).high));
                    } else
                        throw (new RuntimeException("pll.Terminal.CharacterClass.Union.toString: unsupportet member found at postion " + i + ": " + m[i].
                                toString()));

                sb.append(']');
                return (sb.toString());
            }

            public int hashCode() {
                if(m == null)
                    return (-10);
                int s = -10;
                for(int i = 0; i < m.length; i++)
                    s += m[i].hashCode();
                return (s);
            }

            public Object clone() {
                if((m == null) || (m.length == 0))
                    return (Empty);
                CharacterClass[] m2 = new CharacterClass[m.length];
                for(int i = 0; i < m2.length; i++)
                    m2[i] = (CharacterClass)m[i].clone();
                return (new Union(m2, true));
            }

            public int distanceTo(int x) {
                if((m == null) || (m.length == 0))
                    return (Integer.MAX_VALUE);
                if(m.length == 1)
                    return (m[0].distanceTo(x));
                return (closestMemberTo(x)[1]);
            }

            int[] closestMemberTo(int x) {
                if((m == null) || (m.length == 0))
                    return (null);
                int min_d = m[0].distanceTo(x), min_i = 0, cur_d;
                for(int i = 1; i < m.length; i++) {
                    cur_d = m[i].distanceTo(x);
                    if(Math.abs(cur_d) < Math.abs(min_d)) {
                        min_d = cur_d;
                        min_i = i;
                    }
                }
                return (new int[] {min_i, min_d});
            }
        }
    }
}
