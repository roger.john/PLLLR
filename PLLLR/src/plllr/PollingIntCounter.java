/*
 * IntCounter.java
 *
 * Created on 14. November 2007, 14:51
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package plllr;

/**
 *
 * @author RJ
 */
public class PollingIntCounter {
    
    public int val;
    
    /** Creates a new instance of IntCounter */
    public PollingIntCounter() {
        this(0);
    }
    public PollingIntCounter(int val) {
        this.val = val;
    }
    public int get() {
        return(val);
    }
    public int set(int nval) {
        int h = val;
        val = nval;
        return(h);
    }
    public PollingIntCounter add(int x) {
        val += x;
        return(this);
    }
    public PollingIntCounter subtract(int x) {
        val -= x;
        return(this);
    }
    public PollingIntCounter invert(int x) {
        val = -val;
        return(this);
    }
    public PollingIntCounter inc() {
        val++;
        return(this);
    }
    public PollingIntCounter dec() {
        val--;
        return(this);
    }
    public void awaitZero() {
        awaitValue(0,10L);
    }
    public void awaitZero(long time) {
        awaitValue(0,time);
    }
    public void awaitValue(int val) {
        awaitValue(val,10L);
    }
    public void awaitValue(int val,long time) {
        while(this.val!=val)
            try { Thread.sleep(time); }
            catch(InterruptedException e) {
                throw(new RuntimeException(getClass().getCanonicalName()+".awaitValue: the "+
                        "thread "+Thread.currentThread()+" was interrupted"));
            }

    }
}
