/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package plllr;

import bbtree.*;
import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 *
 * @author RJ
 */
public class CompoundIterator<A> implements ResettableIterator<A> {
    private Iterator[] it;
    private int i;
    private Iterator lit = null;
    public CompoundIterator(Iterable<A>... im) {
        if(im==null)
            throw(new NullPointerException("Null array"));
        this.it = new Iterator[im.length];
        for(i=0;i<it.length;i++)
            it[i] = im[i].iterator();
        i = 0;
        while((i<it.length)&&!it[i].hasNext()) {
            it[i] = null;
            i++;
        }
    }
    public CompoundIterator(Iterator... it) {
        if(it==null)
            throw(new NullPointerException(getClass().getCanonicalName()
                    +".contructor: null array"));
        this.it = it;
        i = 0;
        while((i<it.length)&&!it[i].hasNext()) {
            it[i] = null;
            i++;
        }
    }
    public void reset() {
        for(i=0;i<it.length;i++)
            if((it[i]!=null)&&(it[i] instanceof ResettableIterator))
                ((ResettableIterator<A>)it[i]).reset();
        i = 0;
        while((i<it.length)&&!it[i].hasNext()) {
            it[i] = null;
            i++;
        }
    }
    public OrderedSet<A> iteratedSet() {
        return(null);
    }
    public ResettableIterator<A> iterator() {
        reset();
        return(this);
    }
    public boolean hasNext() {
        return(i<it.length);
    }
    public A next() {
        if(i>=it.length)
            throw(new NoSuchElementException(getClass().getCanonicalName()
                    +".next: no more elements to iterate"));
        A r = (A)(it[i].next());
        lit = it[i];
        while((i<it.length)&&!it[i].hasNext()) {
            it[i] = null;
            i++;
        }
        return(r);
    }
    public void remove() {
        if(lit==null)
            throw(new IllegalStateException(getClass().getCanonicalName()
                    +".remove: iteration has not started yet"));
        lit.remove();
    }
}
