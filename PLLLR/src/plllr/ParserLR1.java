/*
 * Parser.java
 *
 * Created on 9. Oktober 2007, 10:47
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */
package plllr;

import bbtree.BBTree;
import bbtree.Path;
import bbtree.Selector;
import bbtree.Traversable;
import bbtree.TraversalActionPerformer;
import bbtree.Utils.AsymmetricComparable;
import bbtree.Utils.KeyedElementFactory;
import java.io.IOException;
import java.io.Writer;


/**
 *
 * @author RJ
 */
public class ParserLR1 extends Parser {

    private BBTree<NodeWithLookAhead> U, V;
    private Path<NodeWithLookAhead> L;
    private BBTree<FSoP> F;

    /** Creates a new instance of Parser */
    public ParserLR1() {
        U = new BBTree<NodeWithLookAhead>();
        V = new BBTree<NodeWithLookAhead>();
        L = new Path<NodeWithLookAhead>();
        F = new BBTree<FSoP>();
        dcache = new BBTree<Derivation.Item>();
        ncache = new BBTree<NonTerminalItemWithUsedRuleSet>();
        expected = new BBTree<Terminal>();
    }

    public synchronized Path<Derivation.Item.NonTerminal> parse(
            TerminalReader tr,
            Grammar G,
            boolean useProductionLookAhead,
            boolean infixParsing,
            Writer gw)
            throws IOException {
        if(G == null)
            throw new NullPointerException("Null grammar");
        if(G.S == null)
            throw new NullPointerException("Null start symbol");
        shifted_symbols.set(0);
        derivation_nodes_created.set(0);
        derivation_edges_created.set(0);
        nodes_created.set(0);
        shift_edges_created.set(0);
        shift_edges_visited.set(0);
        reduce_edges_created.set(0);
        reduce_edges_visited.set(0);
        this.useProductionLookAhead = useProductionLookAhead;
        this.infixParsing = infixParsing;
        NodeWithLookAhead t;
        if((G == null) || (G.S == null))
            return (null);
        this.G = G;
        p = new Path<Derivation.Item.NonTerminal>();
        if(tr != null)
            ct = tr.read();
        else
            ct = null;
        initGraphWriter(gw);
        if((ct == null) || (ct == Terminal.End)) {        //Prüft bei leerer Eingabe, ob diese zur Grammatik gehört
            if(G.S.isNullable())
                p.append(Derivation.Item.generateEpsilonShiftItem(G.S, 0));
            derivation_nodes_created.inc();
            derivation_edges_created.inc();
            finishGraphWriter();
            return p;
        }
        cp = 0;
        U.add(NodeWithLookAhead.makeStartNode(G.S, 0));   //Erzeugt den Start-Knoten in U
        nodes_created.inc();
        reduce_edges_created.inc();
        while(ct != Terminal.End) {
            produce();
            if(DEBUG)
                System.out.println("post-produce(" + cp + "): " + U.toString());
            makeNodesFinal();   //komprimiert die Kanten-Mengen in den Knoten
            if(!DEBUG) {
                dcache.clear();
                ncache.clear();
            }
            writeCurrentGraphCluster(U);
            shift();
            if(DEBUG)
                System.out.println("post-shift(" + cp + "): " + U.toString());
            if(U.isEmpty() && !infixParsing) {
                finishGraphWriter();
                return p;
            } else
                shifted_symbols.inc();
            ct = tr.read();
            if(ct == null)
                ct = Terminal.End;
            reduce();
            if(DEBUG)
                System.out.println("post-reduce(" + cp + "): " + U.toString());
            if((ct != Terminal.End) && infixParsing)
                if(U.add(t = NodeWithLookAhead.makeStartNode(G.S, cp))) {
                    nodes_created.inc();
                    reduce_edges_created.inc();
                } else {
                    t = U.get(t);
                    if(t.addEdge(Edge.StackEnd))
                        reduce_edges_created.inc();
                    else
                        reduce_edges_visited.inc();
                }
        }
        makeNodesFinal();
        finishGraphWriter();
        if(p.isEmpty()) {
            expected.clear();
            TerminalSet u;
            for(NodeWithLookAhead x : U) {
                u = Grammar.first(x.tp);
                if(u != null)
                    u.addTo(expected);
                expected.remove(Terminal.Epsilon);
                if(u.contains(Terminal.Epsilon))
                    if(x.la != null)
                        x.la.addTo(expected);
            }
        }
        U.clear();
        V.clear();
        L.clear();
        F.clear();
        if(DEBUG)
            System.out.println("Dcache(" + dcache.size() + "): " + dcache.
                    toString());
        dcache.clear();
        if(DEBUG)
            System.out.println("Ncache(" + ncache.size() + "): " + ncache.
                    toString());
        ncache.clear();
        return p;
    }

    public boolean generatesParseGraph() {
        return true;
    }

    private void produce() {
        NonTerminal d;
        NodeWithLookAhead t;
        L.appendSome(U, PRODUCABLES);
        Path.Node<NodeWithLookAhead> tn = L.head;
        FSoP.Factory fsf = FSoP.Factory.getThreadLocal();
        FSoP f;
        TerminalSet ts;
        while(tn != null) {
            producerLL0.perform(null, Traversable.IN_ORDER, tn.x);
            tn = tn.next;
        }
        while(!L.isEmpty()) {
            while((t = L.pop()) != null) {
                V.remove(t);
                d = (NonTerminal)(t.tp.head.x);
                if(d == null)
                    continue;
                if((t.tp == null) || (t.tp.head == null))
                    f = F.addOrGetMatch((Path.Node<Token>)null, fsf);
                else
                    f = F.addOrGetMatch(t.tp.head.next, fsf);
                if(f == fsf.last)
                    f.calcFirstSet();
                ts = f.t.clone().append(t.la);
                if(useProductionLookAhead) {
                    producerLL1.ts = ts;
                    d.firstSet.traverseMatchingRuleEntries(ct, producerLL1, t);
                } else {
                    if(d.rules == null || d.rules.isEmpty())
                        continue;
                    producerLL0.ts = ts;
                    d.rules.traverse(Traversable.IN_ORDER, producerLL0, t);
                }
            }
            L.appendSome(V, null);
            V.clear();
        }
    }

    private void makeNodesFinal() {
        U.traverse(TraversalActionPerformer.IN_ORDER, FINALIZER, null);
    }

    private void shift() {
        csi = new Derivation.Item.Terminal(ct, cp);
        cp++;
        expected.clear();
        U.traverse(BBTree.IN_ORDER, SHIFTER, this);
        if(V.isEmpty()) {
            TerminalSet u;
            for(NodeWithLookAhead x : U) {
                u = Grammar.first(x.tp);
                if(u != null)
                    u.addTo(expected);
                expected.remove(Terminal.Epsilon);
                if(u.contains(Terminal.Epsilon))
                    if(x.la != null)
                        x.la.addTo(expected);
            }
        }
        BBTree<NodeWithLookAhead> h = U;
        U = V;
        V = h;
        if(!U.isEmpty() || infixParsing)
            V.clear();
    }

    protected void shift(Node x) {
        if((x == null) || !x.isShiftable())
            return;
        NodeWithLookAhead y, z;
//        expected.add((Terminal)(x.tp.head.x));
        if(((Terminal)(x.tp.head.x)).matches(ct)) {
            NodeWithLookAhead.PFactory npf = NodeWithLookAhead.PFactory.
                    getThreadLocal(this);
            y = V.addOrGetMatch(x.tp.shift(), npf);
            if(npf.last == y)
                nodes_created.inc();
            if(y.addEdge(new Edge.Shift(x, csi)))
                shift_edges_created.inc();
            else
                shift_edges_visited.inc();
        }
    }

    private void reduce() {
        NodeWithLookAhead t;
        U.traverse(Traversable.IN_ORDER, REDUCIBLES_APPENDER, this);
        while(!L.isEmpty())
            while((t = L.pop()) != null) {
                if(t.edges == null)
                    continue;
                if(useTransitiveEdges) {
                    Edge.Transitive et = t.createOrGetTransitiveEdge(this);
                    if(et != null) {
                        reducer.perform(et, Traversable.IN_ORDER, t);
                        continue;
                    }
                }
                t.edges.traverse(Traversable.IN_ORDER, reducer, t);
            }
    }

    private boolean addRightNulledTerminationToRightNullableNode(
            NodeWithLookAhead t) {
        if((t == null) || !t.isReducable() || (t.last_dpos == cp))
            return (false);
        t.last_dpos = cp;
        t.last_der = t.tp.isEmpty() ? null : new Derivation.TerminationNode(
                t.tp, t.pos);
        return true;
    }

    private static class ProducerLL0 implements
            TraversalActionPerformer<Rule> {

        public final ParserLR1 p;
        TerminalSet ts;

        public ProducerLL0(ParserLR1 p) {
            if(p == null)
                throw new NullPointerException("Parser is null");
            this.p = p;
        }

        public boolean perform(Rule r, int order, Object o) {
            NodeWithLookAhead t = (NodeWithLookAhead)o;
            boolean shift_on = false;
            FSoP f;
            NodeWithLookAhead cc;
            Edge ce;
            if(r != null) {
                if(r.rhsLength() == 0)
                    return false;
                NodeWithLookAhead.AFactory naf = NodeWithLookAhead.AFactory.
                        getThreadLocal(p);
                cc = p.U.addOrGetMatch(r.rhs, naf);
                if(naf.last == cc) {
                    p.nodes_created.inc();
                    cc.la = ts.clone();
                    if(cc.isProducable()) {
                        p.V.add(cc);
                        shift_on = true;
                    }
                } else if(cc.la.addAll(ts) && cc.isProducable()) {
                    p.V.add(cc);
                    shift_on = true;
                }
                ce = new Edge.Reduce(t, r);
                if(cc.addEdge(ce))
                    p.reduce_edges_created.inc();
                else
                    p.reduce_edges_visited.inc();
                if(!shift_on)
                    return false;
                t = cc;
            }
            NodeWithLookAhead.PFactory npf = NodeWithLookAhead.PFactory.
                    getThreadLocal(p);
            FSoP.Factory fsf = FSoP.Factory.getThreadLocal();
            NonTerminal d = (NonTerminal)(t.tp.head.x);
            while(((d == null) || d.isNullable()) && (t.tp.head.next != null)) {
                shift_on = false;
                f = p.F.addOrGetMatch(t.tp.head.next, fsf);
                if(f == fsf.last)
                    f.calcFirstSet();
                if(!f.t.contains(p.ct))
                    return false;
                cc = p.U.addOrGetMatch(t.tp.shift(), npf);
                if(cc == npf.last) {
                    p.nodes_created.inc();
                    cc.la = t.la.clone();
                    if(cc.isProducable()) {
                        p.V.add(cc);
                        shift_on = true;
                    }
                } else if(cc.la.addAll(t.la) && cc.isProducable()) {
                    p.V.add(cc);
                    shift_on = true;
                }
                ce = new Edge.Shift(t, p.dcache.addOrGet(
                        Derivation.Item.generateEpsilonShiftItem(d, p.cp)));
                if(cc.addEdge(ce))
                    p.shift_edges_created.inc();
                else
                    p.shift_edges_visited.inc();
                if(!shift_on)
                    return false;
                t = cc;
                d = (NonTerminal)(t.tp.head.x);
            }
            return false;
        }
    }
    private final ProducerLL0 producerLL0 = new ProducerLL0(this);

    private static class ProducerLL1 implements
            TraversalActionPerformer<TerminalToRuleMap.RuleEntry> {

        public final ParserLR1 p;
        TerminalSet ts;

        public ProducerLL1(ParserLR1 p) {
            if(p == null)
                throw new NullPointerException("Parser is null");
            this.p = p;
        }

        public boolean perform(TerminalToRuleMap.RuleEntry rt, int order,
                               Object o) {
            if(rt == null || rt.r == null || rt.r.rhsLength() == 0
               || rt.p == null || rt.p.length == 0)
                return false;
            NodeWithLookAhead t = (NodeWithLookAhead)o;
            int j = 0, i = 0, m = rt.p[rt.p.length - 1];
            NodeWithLookAhead cc;
            Edge ce;
            NodeWithLookAhead.AFactory naf = NodeWithLookAhead.AFactory.
                    getThreadLocal(p);
            NodeWithLookAhead.PFactory npf = NodeWithLookAhead.PFactory.
                    getThreadLocal(p);
            while(i <= m) {
                if((j < rt.p.length - 1) && (i == rt.p[j + 1]))
                    j++;
                cc = i == 0 ? p.U.addOrGetMatch(rt.r.rhs, naf)
                     : p.U.addOrGetMatch(t.tp.shift(), npf);
                ce = i == 0 ? new Edge.Reduce(t, rt.r)
                     : new Edge.Shift(t, p.dcache.addOrGet(
                        Derivation.Item.generateEpsilonShiftItem(
                        ((NonTerminal)(t.tp.head.x)), p.cp)));
                if((i == 0 ? naf.last : npf.last) == cc) {
                    p.nodes_created.inc();
                    cc.la = i == 0 ? ts.clone() : t.la.clone();
                    if((i == rt.p[j]) && cc.isProducable())
                        p.V.add(cc);
                    if(cc.addEdge(ce))
                        if(i == 0)
                            p.reduce_edges_created.inc();
                        else
                            p.shift_edges_created.inc();
                    else if(i == 0)
                        p.reduce_edges_visited.inc();
                    else
                        p.shift_edges_visited.inc();
                } else {
                    if(cc.addEdge(ce))
                        if(i == 0)
                            p.reduce_edges_created.inc();
                        else
                            p.shift_edges_created.inc();
                    else if(i == 0)
                        p.reduce_edges_visited.inc();
                    else
                        p.shift_edges_visited.inc();
                    if(cc.la.addAll(i == 0 ? ts : t.la)) {
                        if((i == rt.p[j]) && cc.isProducable())
                            p.V.add(cc);
                    } else
                        return false;
                }
                t = cc;
                i++;
            }
            return false;
        }
    }
    private final ProducerLL1 producerLL1 = new ProducerLL1(this);
    private static final TraversalActionPerformer<NodeWithLookAhead> SHIFTER = new TraversalActionPerformer<NodeWithLookAhead>() {

        public boolean perform(NodeWithLookAhead x, int order, Object o) {
            if((x == null) || !x.isShiftable())
                return (false);
            NodeWithLookAhead y;
            ParserLR1 p = (ParserLR1)o;
            if(((Terminal)(x.tp.head.x)).matches(p.ct)) {
                NodeWithLookAhead.PFactory npf = NodeWithLookAhead.PFactory.
                        getThreadLocal(p);
                if(npf.p == null)
                    npf.p = p;
                y = p.V.addOrGetMatch(x.tp.shift(), npf);
                if(y == npf.last) {
                    p.nodes_created.inc();
                    y.la = x.la.clone();
                } else
                    y.la.addAll(x.la);
                if(y.addEdge(new Edge.Shift(x, p.csi)))
                    p.shift_edges_created.inc();
                else
                    p.shift_edges_visited.inc();
            }
            return (false);
        }
    };

    private static class Reducer implements TraversalActionPerformer<Edge> {

        public final ParserLR1 p;

        public Reducer(ParserLR1 p) {
            if(p == null)
                throw (new NullPointerException("Parser is null"));
            this.p = p;
        }

        public boolean perform(Edge e, int order, Object o) {
            NodeWithLookAhead t = (NodeWithLookAhead)o, u = (NodeWithLookAhead)e.target;
            if(e instanceof Edge.Reduce || e instanceof Edge.Transitive) {
                boolean tEdge = e instanceof Edge.Transitive;
                if(tEdge)
                    p.transitive_edges_visited.inc();
                else
                    p.reduce_edges_visited.inc();
                Edge.Reduce er = tEdge ? null : (Edge.Reduce)e;
                Edge.Transitive et = tEdge ? (Edge.Transitive)e : null;
                if(!tEdge && er.r == null) {
                    if(u != null)
                        throw (new RuntimeException(
                                "Null rule in reduction edge discovered"));
                    if((p.ct == Terminal.End) || p.infixParsing) {
                        p.p.append(
                                (Derivation.Item.NonTerminal)(t.last_der.head.x.item));
                        p.derivation_nodes_created.inc();
                        p.derivation_edges_created.inc();
                    }
                } else {
                    if(u == null)
                        throw new RuntimeException(
                                (tEdge ? "transitive" : "illegal reduction") + " edge to stack end detected");
                    if(!tEdge)
                        if(!u.tp.head.x.equals(er.r.lhs))
                            throw new RuntimeException(
                                    "illegal reduction edge detected: " + e);
                    FSoP.Factory fsf = FSoP.Factory.getThreadLocal();
                    FSoP f = p.F.addOrGetMatch(u.tp.head.next, fsf);
                    if(f == fsf.last)
                        f.calcFirstSet();
                    if(!(p.infixParsing && f.t.contains(
                            Terminal.Epsilon)
                         && u.la.contains(Terminal.End)))
                        if(!(f.t.contains(p.ct) || (f.t.contains(
                                Terminal.Epsilon) && u.la.contains(p.ct))))
                            return false;
                    Path<Token> beta = u.tp.shift();
                    NodeWithLookAhead.PFactory npf = NodeWithLookAhead.PFactory.
                            getThreadLocal(p);
                    NodeWithLookAhead v = p.U.addOrGetMatch(beta, npf);
                    if(v == npf.last) {
                        p.nodes_created.inc();
                        v.la = u.la.clone();
                    } else
                        v.la.addAll(u.la);
                    Edge.Shift es = new Edge.Shift(u, null);
                    Edge h = v.addOrGetEdge(es);
                    if(h == es) {
                        p.shift_edges_created.inc();
                        NonTerminalItemWithUsedRuleSet.Factory nuf =
                                NonTerminalItemWithUsedRuleSet.Factory.
                                getThreadLocal(p);
                        NonTerminalItemWithUsedRuleSet ntiwurs = p.ncache.
                                addOrGetMatch(
                                new Derivation.Item.NonTerminal(
                                (NonTerminal)u.tp.head.x, u.pos, p.cp), nuf);
                        if(ntiwurs == nuf.last)
                            p.derivation_nodes_created.inc();
                        es.der = ntiwurs.nti;
                        if(tEdge) {
                            ntiwurs.nti.appendReversedDerivationPathEntry(new Derivation.ReversedDerivationPathEntry(
                                    et.reversedDerivationPath, t.last_der));
                            p.derivation_edges_created.inc();
                        } else if(ntiwurs.useRule(er.r)) {
                            ntiwurs.nti.appendRuleEdge(er.r, t.last_der);
                            p.derivation_edges_created.inc();
                        }
                        if(v.isReducable() && u.la.contains(p.ct)) {
                            p.addRightNulledTerminationToRightNullableNode(v);
                            if(u.last_dpos != p.cp) {
                                u.last_dpos = p.cp;
                                u.last_der = new Derivation.DistributionNode();
                                u.last_der.append(
                                        new Derivation.Edge(es.der, v.last_der));
                                p.derivation_nodes_created.inc();
                                p.derivation_edges_created.inc();
                                p.L.append(u);
                            } else {
                                u.last_der.append(
                                        new Derivation.Edge(es.der, v.last_der));
                                p.derivation_nodes_created.inc();
                                p.derivation_edges_created.inc();
                            }
                        }
                    } else {
                        p.shift_edges_visited.inc();
                        es = (Edge.Shift)h;
                        if(tEdge) {
                            ((Derivation.Item.NonTerminal)es.der).
                                    appendReversedDerivationPathEntry(new Derivation.ReversedDerivationPathEntry(
                                    et.reversedDerivationPath, t.last_der));
                            p.derivation_edges_created.inc();
                        } else {
                            NonTerminalItemWithUsedRuleSet ntiwurs = p.ncache.
                                    getMatch(
                                    new Derivation.Item.NonTerminal(er.r.lhs,
                                                                    u.pos, p.cp,
                                                                    null, null));
                            if(ntiwurs.useRule(er.r)) {
                                ntiwurs.nti.appendRuleEdge(er.r, t.last_der);
                                p.derivation_edges_created.inc();
                            }
                        }
                    }
                }
                return tEdge;
            } else if(e instanceof Edge.Shift) {
                p.shift_edges_visited.inc();
                if(u.la.contains(p.ct)
                   || (p.infixParsing && u.la.contains(Terminal.End))) {
                    Edge.Shift es = (Edge.Shift)e;
                    if(u.last_dpos != p.cp) {
                        u.last_dpos = p.cp;
                        u.last_der = new Derivation.DistributionNode();
                        u.last_der.append(
                                new Derivation.Edge(es.der, t.last_der));
                        p.derivation_nodes_created.inc();
                        p.derivation_edges_created.inc();
                        p.L.append(u);
                    } else {
                        u.last_der.append(
                                new Derivation.Edge(es.der, t.last_der));
                        p.derivation_nodes_created.inc();
                        p.derivation_edges_created.inc();
                    }
                }
            }
            return false;
        }
    }
    private final Reducer reducer = new Reducer(this);
    private static final Selector<NodeWithLookAhead> PRODUCABLES = new Selector<NodeWithLookAhead>() {

        public boolean selects(NodeWithLookAhead x) {
//            if(x==null)
//                return(false);
            return (x.isProducable());
        }

        public NodeWithLookAhead lowerBound() {
            return (null);
        }

        public NodeWithLookAhead upperBound() {
            return (null);
        }
    };
    private static final TraversalActionPerformer<NodeWithLookAhead> REDUCIBLES_APPENDER = new TraversalActionPerformer<NodeWithLookAhead>() {

        public boolean perform(NodeWithLookAhead x, int order, Object o) {
            if(o != null) {
                ParserLR1 p = (ParserLR1)o;
                if(x.la.contains(p.ct) && p.
                        addRightNulledTerminationToRightNullableNode(x))
                    p.L.append(x);
            }
            return (false);
        }
    };

    private static class NodeWithLookAhead extends Node {

        TerminalSet la = null;

        public NodeWithLookAhead(Path<Token> tp, int pos) {
            super(tp, pos);
        }

        public static NodeWithLookAhead makeStartNode(NonTerminal S, int pos) {
            if(S == null)
                return (null);
            NodeWithLookAhead r = new NodeWithLookAhead(new Path<Token>().append(
                    S), pos);
            r.addEdge(Edge.StackEnd);
            r.la = new TerminalSet();
            r.la.add(Terminal.End);
            return (r);
        }

        public void makeFinal() {
            super.makeFinal();
            if(la != null)
                la.makeFinal();
        }

        @Override
        public String toString() {
            StringBuilder sb = new StringBuilder();
            sb.append('(');
            if(tp != null) {
                Path.Node<Token> t = tp.head;
                while(t != null) {
                    if(t.x == null) {
                    } else if(t.x instanceof NonTerminal)
                        sb.append(((NonTerminal)(t.x)).getEscapedName());
                    else
                        sb.append(t.x.toString());
                    t = t.next;
                    if(t != null)
                        sb.append(' ');
                }
            }
            sb.append(',');
            sb.append(pos);
            sb.append(',');
            sb.append(la.toString());
            sb.append(',');
            sb.append(edges != null ? edges.toString() : "{}");
            sb.append(')');
            return (sb.toString());
        }

        public static class PFactory implements
                KeyedElementFactory<NodeWithLookAhead, Path<Token>> {

            NodeWithLookAhead last;
            ParserLR1 p;

            public PFactory init(ParserLR1 p) {
                if(p == null)
                    throw (new NullPointerException("Parser is null"));
                this.p = p;
                return (this);
            }

            public NodeWithLookAhead lastCreated() {
                return (last);
            }

            public void reset() {
                last = null;
            }

            public NodeWithLookAhead createElement(Path<Token> tp) {
                if(tp == null)
                    throw (new NullPointerException("Path is null"));
                last = new NodeWithLookAhead(tp, p.cp);
                return (last);
            }

            public static PFactory getThreadLocal(ParserLR1 p) {
                return (TLV.get().init(p));
            }
            private static final ThreadLocal<PFactory> TLV =
                    new ThreadLocal<PFactory>() {

                        protected PFactory initialValue() {
                            return (new PFactory());
                        }
                    };

            @Override
            public Path<Token> extractKey(NodeWithLookAhead x) {
                return x.tp;
            }
        }

        public static class AFactory implements
                KeyedElementFactory<NodeWithLookAhead, Token[]> {

            NodeWithLookAhead last;
            ParserLR1 p;

            public AFactory init(ParserLR1 p) {
                if(p == null)
                    throw (new NullPointerException("Parser is null"));
                this.p = p;
                return (this);
            }

            public NodeWithLookAhead lastCreated() {
                return (last);
            }

            public void reset() {
                last = null;
            }

            public NodeWithLookAhead createElement(Token[] tp) {
                if(tp == null)
                    throw (new NullPointerException("Path is null"));
                last = new NodeWithLookAhead(Path.valueOf(tp), p.cp);
                return (last);
            }

            public static AFactory getThreadLocal(ParserLR1 p) {
                return (TLV.get().init(p));
            }
            private static final ThreadLocal<AFactory> TLV =
                    new ThreadLocal<AFactory>() {

                        protected AFactory initialValue() {
                            return (new AFactory());
                        }
                    };

            @Override
            public Token[] extractKey(NodeWithLookAhead x) {
                return x.tp != null ? x.tp.toArray(new Token[0]) : null;
            }
        }
    }

    private static class FSoP implements Comparable<FSoP>,
                                         AsymmetricComparable<Path.Node<Token>> {

        Path.Node<Token> p;
        TerminalSet t;

        public FSoP(Path<Token> p) {
            this(p == null ? null : p.head, false);
        }

        public FSoP(Path<Token> p, boolean shift) {
            this(p == null ? null : p.head, shift);
        }

        public FSoP(Path.Node<Token> p) {
            this(p, false);
        }

        public FSoP(Path.Node<Token> p, boolean shift) {
            if(p == null)
                this.p = null;
            else
                this.p = shift ? p.next : p;
        }

        public FSoP calcFirstSet() {
            if(this.t != null)
                return (this);
            this.t = Grammar.first(this.p).makeFinal();
            return (this);
        }

        public int compareTo(FSoP other) {
            if(other == null)
                return (1);
            return (asymmetricCompareTo(other.p));
        }

        public int asymmetricCompareTo(Path.Node<Token> p) {
            if(p == null)
                return (this.p == null ? 0 : 1);
            Path.Node<Token> s = this.p, t = p;
            int c = 0;
            while((s != null) && (t != null)) {
                c = (s.x != null) && (t.x != null) ? s.x.compareTo(t.x) : (s.x != null ? 1 : (t.x != null ? -1 : 0));
                if(c != 0)
                    return (c);
                s = s.next;
                t = t.next;
            }
            if(s == null)
                return (t == null ? 0 : -1);
            return (1);
        }

        public static class Factory implements
                KeyedElementFactory<FSoP, Path.Node<Token>> {

            public FSoP last;

            public FSoP createElement(Path.Node<Token> p) {
                last = new FSoP(p, false);
                return (last);
            }

            public FSoP lastCreated() {
                return (last);
            }

            public void reset() {
                last = null;
            }

            public static Factory getThreadLocal() {
                return TLV.get();
            }
            private static final ThreadLocal<Factory> TLV =
                    new ThreadLocal<Factory>() {

                        protected Factory initialValue() {
                            return (new Factory());
                        }
                    };

            @Override
            public Path.Node<Token> extractKey(FSoP x) {
                return x.p;
            }
        }
    }
}
