/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package plllr;

import bbtree.Path;
import bbtree.*;
import java.io.Serializable;
import java.util.*;

/**
 *
 * @author RJ
 */
public class TerminalContext implements Serializable {

    private static final long serialVersionUID = 0L;
    private Terminal[] ts;
    private Terminal.CharacterClass[] tsc;
    private int terminal_count;
    private Grammar G;
    private int epsid;

    private TerminalContext() {
    }

    public TerminalContext(Grammar G) {
        this.G = G;
        extractTerminals();
        epsid = getBitIndexOfTNC(Terminal.Epsilon);
    }

    private void extractTerminals() {
        BBTree<Terminal> ts = new BBTree<Terminal>();
        BBTree<Terminal.CharacterClass> tsc = new BBTree<Terminal.CharacterClass>();
        ts.add(Terminal.Epsilon);
        ts.add(Terminal.End);
        if((G != null) && (G.P != null)) {
            int i, l;
            for(Rule r : G.P) {
                l = r.rhsLength();
                for(i = 0; i < l; i++)
                    if((r.rhs[i] != null) && (r.rhs[i] instanceof Terminal))
                        if(r.rhs[i] instanceof Terminal.CharacterClass)
                            tsc.add((Terminal.CharacterClass)(r.rhs[i]));
                        else
                            ts.add((Terminal)(r.rhs[i]));
            }
        }
        this.ts = ts.toArray(new Terminal[ts.size()]);
        this.tsc = tsc.toArray(new Terminal.CharacterClass[tsc.size()]);
        terminal_count = this.ts.length + this.tsc.length;
    }

    public int getTerminalCount() {
        return (terminal_count);
    }

    private int getBitIndexOfTNC(Terminal x) {
        if((x == null) || (ts == null))
            return (-1);
        int l = 0, h = ts.length - 1, m, c;
        while(l < h) {
            m = (l + h) >>> 1;
            c = x.compareTo(ts[m]);
            if(c == 0)
                return (m);
            else if(c < 0)
                h = m - 1;
            else
                l = m + 1;
        }
        if((l == h) && (x.compareTo(ts[l]) == 0))
            return (l);
        else
            return (-1);
    }

    private int getBitIndexOfTC(Terminal.CharacterClass x) {
        if((x == null) || (ts == null))
            return (-1);
        int l = 0, h = tsc.length - 1, m, c;
        while(l < h) {
            m = (l + h) >>> 1;
            c = x.compareTo(tsc[m]);
            if(c == 0)
                return (m + ts.length);
            else if(c < 0)
                h = m - 1;
            else
                l = m + 1;
        }
        if((l == h) && (x.compareTo(tsc[l]) == 0))
            return (l + ts.length);
        else
            return (-1);
    }

    public int getBitIndexOf(Terminal x) {
        if(x == Terminal.Epsilon)
            return (epsid);
        if(x == null)
            return (-1);
        int r;
        if(x instanceof Terminal.CharacterClass)
            r = getBitIndexOfTC((Terminal.CharacterClass)x);
        else
            r = getBitIndexOfTNC(x);
        /* if(r<0)
        throw(new RuntimeException("unknown terminal: "+x));*/
        return (r);
    }

    public Terminal getTerminalAt(int id) {
        if((id < 0) || (id >= terminal_count))
            return (null);
        if(id < ts.length)
            return (ts[id]);
        else
            return (tsc[id - ts.length]);
    }

    public BitField newSet() {
        return (new BitField(terminal_count));
    }

    public boolean contains(Terminal x, BitField a) {
        if((a == null) || (x == null))
            return (false);
        return (a.get(getBitIndexOf(x)));
    }

    public boolean matches(Terminal x, BitField a) {
        if((a == null) || (x == null))
            return (false);
        if(a.get(getBitIndexOf(x)))
            return (true);
        for(int i = a.nextSetBit(ts.length - 1);
            (i >= 0) && (i < terminal_count); i = a.nextSetBit(i))
            if(tsc[i - ts.length].matches(x))
                return (true);
        return (false);
    }

    public boolean add(Terminal x, BitField a) {
        int id = getBitIndexOf(x);
        if(id < 0)
            return (false);
        else
            return (!a.set(id));
    }

    public boolean addAll(Collection<? extends Terminal> x, BitField a) {
        if((x == null) || (a == null))
            return (false);
        boolean r = false;
        synchronized(x) {
            for(Terminal y : x)
                r |= add(y, a);
        }
        return (r);
    }

    public Collection<? super Terminal> addTo(Collection<? super Terminal> x,
                                              BitField a) {
        if((x == null) || (a == null))
            return (x);
        for(int i = 0; i < ts.length; i++)
            if(a.get(i))
                x.add(ts[i]);
        for(int i = 0; i < tsc.length; i++)
            if(a.get(i + tsc.length))
                x.add(tsc[i]);
        return (x);
    }

    public boolean addAll(TerminalToRuleMap x, BitField a) {
        if((x == null) || (a == null))
            return (false);
        boolean r = false;
        Iterator<TerminalToRuleMap.TEntry> it;
        if((x.s != null) && !x.s.isEmpty())
            synchronized(x.s) {
                it = x.s.iterator();
                while(it.hasNext())
                    add(it.next().x, a);
            }
        if((x.sc != null) && !x.sc.isEmpty())
            synchronized(x.sc) {
                it = x.sc.iterator();
                while(it.hasNext())
                    add(it.next().x, a);
            }
        return (r);
    }

    public boolean addAll(BitField x, BitField a) {
        if((x == null) || (a == null))
            return (false);
        if(a.contains(x))
            return (false);
        a.orIP(x);
        return (true);
    }

    public boolean remove(Terminal x, BitField a) {
        int id = getBitIndexOf(x);
        if(id < 0)
            return (false);
        else
            return (a.clear(id));
    }

    public boolean removeAll(Collection<? extends Terminal> x, BitField a) {
        if((x == null) || (a == null))
            return (false);
        boolean r = false;
        synchronized(x) {
            for(Terminal y : x)
                r |= remove(y, a);
        }
        return (r);
    }

    public boolean retainAll(Collection<? extends Terminal> x, BitField a) {
        if((x == null) || (a == null))
            return (false);
        boolean r = false;
        for(int i = a.prevSetBit(terminal_count); i >= 0; i = a.prevSetBit(i))
            if(!x.contains(i < ts.length ? ts[i] : tsc[i - ts.length]))
                r |= a.clear(i);
        return (r);
    }

    public boolean append(BitField x, BitField a) {
        if((x == null) || (a == null))
            return (false);
        if(!a.get(epsid))
            return (false);
        a.clear(epsid);
        return (addAll(x, a));
    }

    public BitField first(Path<Token> p) {
        return (first(p.head));
    }

    public BitField first(Path.Node<Token> p) {
        BitField r = newSet();
        r.set(epsid);
        if(p == null)
            return (r);
        Path.Node<Token> t = p;
        while((t != null) && contains(Terminal.Epsilon, r)) {
            r.clear(epsid);
            if(t.x instanceof Terminal)
                add((Terminal)(t.x), r);
            else if(t.x instanceof NonTerminal) {
                addAll(((NonTerminal)(t.x)).firstSet, r);
                if(((NonTerminal)(t.x)).isNullable())
                    r.set(epsid);
            }
            t = t.next;
        }
        return (r);
    }

    public String toString(BitField a) {
        if(a == null)
            return ("{}");
        StringBuilder sb = new StringBuilder();
        sb.append('{');
        int i = a.prevSetBit(terminal_count);
        if(i >= 0)
            sb.append(getTerminalAt(i).toString());
        while((i = a.prevSetBit(i)) >= 0)
            sb.append(',').append(getTerminalAt(i));
        sb.append('}');
        return (sb.toString());
    }
}
