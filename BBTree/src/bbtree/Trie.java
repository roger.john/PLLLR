/*
 * Trie.java
 *
 * Created on 14. Februar 2007, 12:10
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */
package bbtree;

import bbtree.Utils.AsymmetricComparable;
import bbtree.Utils.KeyedElementFactory;
import java.io.IOException;
import java.io.Serializable;
import java.io.Writer;
import java.util.Comparator;

/**
 *
 * @author RJ
 */
public class Trie<A extends Comparable<? super A>> implements Cloneable,
                                                              Writeable,
                                                              Serializable,
                                                              Iterable<Path<A>>,
                                                              Comparable<Trie<A>>,
                                                              Traversable<Path<A>> {

    private static final long serialVersionUID = 0;
    public static final boolean TABBED_WRITING = false;
    private static final ThreadLocal<Node.Factory> NODE_FACTORY =
            new ThreadLocal<Node.Factory>() {

                protected Node.Factory initialValue() {
                    return (new Node.Factory());
                }
            };
    private Traversable.Set<Node<A>> heads = new BBTree<Node<A>>();

    /** Creates a new instance of Trie */
    public Trie() {
    }

    public boolean equals(Object o) {
        if(o == this)
            return (true);
        if((o == null) || !(o instanceof Trie))
            return (false);
        return (compareTo((Trie<A>)o) == 0);
    }
    private static final Comparator<Node> TRIE_NODE_COMPARATOR = new Comparator<Node>() {

        public int compare(Node x, Node y) {
            if(x == null)
                return (y == null ? 0 : -1);
            if(y == null)
                return (1);
            int c = 0;
            if(x.x == null)
                c = y.x == null ? 0 : -1;
            else if(y.x == null)
                c = 1;
            else
                c = x.x.compareTo(y.x);
            if(c != 0)
                return (c);
            if(x.nextNodes == null)
                return (y.nextNodes == null ? 0 : -1);
            if(y.nextNodes == null)
                return (1);
            c = x.nextNodes.size() - y.nextNodes.size();
            if(c != 0)
                return (c);
            if(x.nextNodes.isEmpty())
                throw (new RuntimeException("Unclosed path discovered"));
            return (x.nextNodes.compareTo(y.nextNodes, this));
        }
    };

    public int compareTo(Trie<A> o) {
        if(o == this)
            return (0);
        if(o == null)
            return (1);
        if(heads.isEmpty())
            return (o.heads.isEmpty() ? 0 : -1);
        int c = heads.size() - o.heads.size();
        if(c != 0)
            return (c);
        if(heads.isEmpty())
            return (0);
        return (heads.compareTo(o.heads, TRIE_NODE_COMPARATOR));
    }

    private static int compare(Node x, Node y) {
        if(x == null)
            return (y == null ? 0 : -1);
        if(y == null)
            return (1);
        int i = 0;
        if(x.x == null)
            i = y.x == null ? 0 : -1;
        else if(y.x == null)
            i = 1;
        else
            i = x.x.compareTo(y.x);
        if(i != 0)
            return (i);
        if(x.nextNodes == null)
            return (y.nextNodes == null ? 0 : -1);
        if(y.nextNodes == null)
            return (1);
        java.util.Iterator<Node> i1 = x.nextNodes.iterator();
        java.util.Iterator<Node> i2 = y.nextNodes.iterator();
        while(i == 0) {
            if(!i1.hasNext())
                return (i2.hasNext() ? -1 : 0);
            if(!i2.hasNext())
                return (1);
            i = compare(i1.next(), i2.next());
        }
        return (i);
    }

    public boolean containsEmptyPath() {
        return heads.contains(EndNode);
    }

    public boolean containsOnlyEmptyPath() {
        return heads.contains(EndNode) && heads.size() == 1;
    }

    public boolean contains(A y) {
        if(y == null)
            return (heads.contains(EndNode));
        Node<A> t = heads.getMatch(y);
        if(t == null)
            return (false);
        return ((t.nextNodes == null) || t.nextNodes.contains(EndNode));
    }

    public boolean contains(Path<A> y) {
        if((y == null) || (y.head == null))
            return (heads.contains(EndNode));
        Path.Node<A> u = y.head;
        OrderedSet<Node<A>> s = heads;
        Node<A> t;
        while(u != null) {
            if((s == null) || ((t = s.getMatch(u.x)) == null))
                return (false);
            s = t.nextNodes;
            u = u.next;
        }
        return ((s == null) || s.contains(EndNode));
    }

    public boolean add(A y) {
        if(y == null)
            return (heads.add(EndNode));
        Node.Factory<A> f = NODE_FACTORY.get();
        Node<A> t = heads.addOrGetMatch(y, f);
        if(t == f.last)
            return (true);
        else if(t.nextNodes != null)
            return (t.nextNodes.add(EndNode));
        else
            return (false);
    }

    public boolean add(Path<A> y) {
        if(y == null)
            return (heads.add(EndNode));
        Path.Node<A> u = y.head;
        while((u != null) && (u.x == null))
            u = u.next;
        if(u == null)
            return (heads.add(EndNode));
        OrderedSet<Node<A>> s = heads;
        Node.Factory f = NODE_FACTORY.get();
        Node t;
        boolean changed = false;
        while(true) {
            t = s.addOrGetMatch(u.x, f);
            if(t == f.last) {
                u = u.next;
                while((u != null) && (u.x == null))
                    u = u.next;
                if(u != null) {
                    changed |= true;
                    t.nextNodes = new BBTree();
                    s = t.nextNodes;
                } else
                    return (true);
            } else {
                s = t.nextNodes;
                u = u.next;
                while((u != null) && (u.x == null))
                    u = u.next;
                if(u == null) {
                    if((s != null) && (!s.contains(EndNode)))
                        changed |= s.add(EndNode);
                    break;
                }
                if(s == null) {
                    t.nextNodes = new BBTree();
                    s = t.nextNodes;
                    changed |= s.add(EndNode);
                }
            }
        }
        return (changed);
    }

    public boolean remove(A y) {
        if(isFinal())
            throw (new UnsupportedOperationException(
                    "Final trie cannot be modified"));
        if(heads.isEmpty())
            return (false);
        if(y == null)
            return (heads.remove(EndNode));
        Node t = heads.getMatch(y);
        if(t == null)
            return (false);
        if((t.nextNodes == null) || (t.nextNodes.contains(EndNode) && (t.nextNodes.
                size() == 1)))
            return (heads.remove(t));
        return (t.nextNodes.remove(EndNode));
    }

    public boolean remove(Path<A> y) {
        if(isFinal())
            throw (new UnsupportedOperationException(
                    "Final trie cannot be modified"));
        if(heads.isEmpty())
            return (false);
        if((y == null) || (y.head == null))
            return (heads.remove(EndNode));
        Path.Node<A> u = y.head;
        while((u != null) && (u.x == null))
            u = u.next;
        if(u == null)
            return (heads.remove(EndNode));
        OrderedSet<Node<A>> s = heads, ls = heads;
        Node t, lt = null;
        while(true) {
            t = s.getMatch(u.x);
            if(t == null)
                return (false);
            if(s.size() > 1) {
                ls = s;
                lt = t;
            }
            s = t.nextNodes;
            u = u.next;
            while((u != null) && (u.x == null))
                u = u.next;
            if(u == null) {
                if(s == null)
                    break;
                if(!s.contains(EndNode))
                    return (false);
                if(s.size() > 1)
                    return (s.remove(EndNode));
                break;
            }
            if(s == null)
                return (false);
        }
        if((ls != null) && (lt != null))
            return (ls.remove(lt));
        return (false);
    }

    public void clear() {
        if(isFinal())
            throw (new UnsupportedOperationException(
                    "Final trie cannot be modified"));
        heads.clear();
    }

    private static class BoundedPlusApplier<A extends Comparable<? super A>>
            implements TraversalActionPerformer<Node<A>> {

        private Trie<A> x;
        private int k, bound;

        BoundedPlusApplier<A> init(Trie<A> x, int bound) {
            this.x = x;
            this.k = 0;
            this.bound = bound;
            return (this);
        }

        public boolean perform(Node<A> a, int order, Object o) {
            Traversable.Set<Node<A>> u = (Traversable.Set<Node<A>>)o;
            if(a == EndNode) {
                u.add(EndNode);
                if(k > 1) {
                    k--;
                    x.heads.traverse(IN_ORDER, this, u);
                    k++;
                }
            } else {
                Node b = new Node(a.x);
                if(!u.add(b))
                    throw (new RuntimeException("expected empty target trie"));
                if(a.nextNodes == null) {
                    if((k > 1) && (bound > 1)) {
                        b.nextNodes = new BBTree<Node<A>>();
                        b.nextNodes.add(EndNode);
                        k--;
                        bound--;
                        x.heads.traverse(IN_ORDER, this, b.nextNodes);
                        k++;
                        bound++;
                    }
                } else if(bound > 1) {
                    b.nextNodes = new BBTree<Node<A>>();
                    bound--;
                    a.nextNodes.traverse(IN_ORDER, this, b.nextNodes);
                    bound++;
                }
            }
            return (false);
        }
    }
    private static final ThreadLocal<BoundedPlusApplier> BOUNDED_PLUS_APPLIER =
            new ThreadLocal<BoundedPlusApplier>() {

                protected BoundedPlusApplier initialValue() {
                    return (new BoundedPlusApplier());
                }
            };

    public void applyBoundedPlus(int bound) {
        if(bound < 0)
            throw (new IllegalArgumentException("Bound is negative"));
        if(bound == 0) {
            if(!isEmpty()) {
                clear();
                add((A)null);
            }
            return;
        }
        if(heads.isEmpty())
            return;
        if(heads.contains(EndNode) && (heads.size() == 1))
            return;
        BBTree<Node<A>> nHeads = new BBTree<Node<A>>();
        heads.traverse(Traversable.IN_ORDER,
                       BOUNDED_PLUS_APPLIER.get().init(this, bound), nHeads);
        heads = nHeads;
    }

    public void applyBoundedStar(int bound) {
        if(bound < 0)
            throw (new IllegalArgumentException("Bound is negative"));
        applyBoundedPlus(bound);
        add((A)null);
    }

    public boolean isEmpty() {
        return (heads.isEmpty());
    }

    public boolean isFinal() {
        return (heads.isFinal());
    }

    public void makeFinal() {
        if(heads.isFinal())
            return;
        else {
            heads = heads.toFinal();
            heads.traverse(Traversable.IN_ORDER, FINALIZER);
        }
    }

    public void makeEditable() {
        if(heads.isFinal()) {
            Traversable.Set<Node<A>> oHeads = heads;
            heads = new BBTree<Node<A>>();
            oHeads.traverse(IN_ORDER, CLONER, heads);
        }
    }
    private static final TraversalActionPerformer<Node<?>> FINALIZER =
            new TraversalActionPerformer<Node<?>>() {

                public boolean perform(Node<?> x, int order, Object o) {
                    x.makeFinal();
                    return (false);
                }
            };

    public Trie<A> clone() {
        Trie<A> r = new Trie<A>();
        heads.traverse(Traversable.IN_ORDER, CLONER, r.heads);
        return (r);
    }
    private static final TraversalActionPerformer<Node<?>> CLONER =
            new TraversalActionPerformer<Node<?>>() {

                public boolean perform(Node<?> x, int order, Object o) {
                    ((Traversable.Set<Node<?>>)o).add(x.clone());
                    return (false);
                }
            };

    public String toString() {
        if(heads.isEmpty())
            return ("()");
        return toStringBuilder(null).toString();
    }

    public StringBuilder toStringBuilder(StringBuilder sb) {
        if(sb == null)
            sb = new StringBuilder();
        if(heads.isEmpty())
            return sb.append("()");
        heads.traverse(IN_ORDER, TRIE_STRING_BUILDER.get().init(),
                       sb.append("("));
        return sb.append(")");
    }

    private static class TrieStringBuilder<A extends Comparable<? super A>>
            implements TraversalActionPerformer<Node<A>> {

        private boolean first = true;

        TrieStringBuilder<A> init() {
            first = true;
            return (this);
        }

        public boolean perform(Node<A> x, int order, Object o) {
            if(order == IN_ORDER) {
                StringBuilder s = (StringBuilder)o;
                if(!first)
                    s.append(',');
                else
                    first = false;
                if(x == EndNode)
                    s.append('#');
                else {
                    if(x.x != null)
                        if(x instanceof Writeable)
                            ((Writeable)x).toStringBuilder(s);
                        else
                            s.append(x.x.toString());
                    if((x.nextNodes != null) && !((x.nextNodes.size() == 1)
                                                  && x.nextNodes.contains(
                            EndNode))) {
                        s.append('(');
                        if(!x.nextNodes.isEmpty()) {
                            first = true;
                            x.nextNodes.traverse(IN_ORDER, this, s);
                            first = false;
                        } else
                            throw (new RuntimeException(
                                    "Unclosed path discovered"));
                        s.append(')');
                    }
                }
            }
            return (false);
        }
    }
    private static final ThreadLocal<TrieStringBuilder> TRIE_STRING_BUILDER =
            new ThreadLocal<TrieStringBuilder>() {

                protected TrieStringBuilder initialValue() {
                    return (new TrieStringBuilder());
                }
            };

    public void writeToStream(Writer w) throws IOException {
        if(w == null)
            return;
        if(heads.isEmpty()) {
            w.write("()");
            return;
        }
        if((heads.size() == 1) && heads.contains(EndNode)) {
            w.write("(#)");
            return;
        }
        w.write('(');
        heads.traverse(IN_ORDER, TRIE_WRITER.get().init(), w);
        if(TABBED_WRITING)
            w.write("\r\n");
        w.write(')');
    }

    private static class TrieWriter<A extends Comparable<? super A>>
            implements TraversalActionPerformer<Node<A>> {

        private boolean first = true;
        private int tabCount = 1;

        TrieWriter<A> init() {
            first = true;
            tabCount = 1;
            return (this);
        }

        public boolean perform(Node<A> x, int order, Object o) {
            if(order == IN_ORDER)
                try {
                    Writer w = (Writer)o;
                    if(!first)
                        w.write(',');
                    else
                        first = false;
                    if(TABBED_WRITING) {
                        w.write("\r\n");
                        for(int i = 0; i < tabCount; i++)
                            w.write('\t');
                    }
                    if(x == EndNode)
                        w.write('#');
                    else {
                        if(x.x != null)
                            if(x instanceof Writeable)
                                ((Writeable)x.x).writeToStream(w);
                            else
                                w.write(x.x.toString());
                        if((x.nextNodes != null) && !((x.nextNodes.size() == 1)
                                                      && x.nextNodes.contains(
                                EndNode))) {
                            w.write('(');
                            if(!x.nextNodes.isEmpty()) {
                                first = true;
                                tabCount++;
                                x.nextNodes.traverse(IN_ORDER, this, w);
                                first = false;
                                tabCount--;
                                if(TABBED_WRITING) {
                                    w.write("\r\n");
                                    for(int i = 0; i < tabCount; i++)
                                        w.write('\t');
                                }
                            } else
                                throw (new RuntimeException(
                                        "Unclosed path discovered"));
                            w.write(')');
                        }
                    }
                } catch(IOException e) {
                    throw (new RuntimeException(
                            "Error while writing trie: ", e));
                }
            return (false);
        }
    }
    private static final ThreadLocal<TrieWriter> TRIE_WRITER =
            new ThreadLocal<TrieWriter>() {

                protected TrieWriter initialValue() {
                    return (new TrieWriter());
                }
            };

    public void writeExpandedToStream(Writer w) throws IOException {
        if(w == null)
            return;
        if(heads.isEmpty()) {
            w.write("{}");
            return;
        }
        w.write('{');
        traverse(IN_ORDER, EXPANDED_TRIE_WRITER.get().init(), w);
        w.write("\r\n}");
    }

    private static class ExpandedTrieWriter<A extends Comparable<? super A>>
            implements TraversalActionPerformer<Path<A>> {

        private boolean first = true;

        ExpandedTrieWriter<A> init() {
            first = true;
            return (this);
        }

        public boolean perform(Path<A> p, int order, Object o) {
            if(order == IN_ORDER)
                try {
                    Writer w = (Writer)o;
                    if(!first)
                        w.write(',');
                    else
                        first = false;
                    w.write("\r\n\t");
                    w.write(p.toString());
                } catch(IOException e) {
                    throw (new RuntimeException(
                            "Error while writing expanded trie: ", e));
                }
            return (false);
        }
    }
    private static final ThreadLocal<ExpandedTrieWriter> EXPANDED_TRIE_WRITER =
            new ThreadLocal<ExpandedTrieWriter>() {

                protected ExpandedTrieWriter initialValue() {
                    return (new ExpandedTrieWriter());
                }
            };

    public java.util.Iterator<Path<A>> iterator() {
        return (new Iterator<A>(this));
    }

    public java.util.Iterator<Path<A>> subSetIterator(Selector<Path<A>> s) {
        return (new Iterator<A>(this, s));
    }

    public Trie<A> cloneBounded(int bound) {
        if(bound < 0)
            throw (new IllegalArgumentException("Bound is negative"));
        Trie<A> r = new Trie<A>();
        if(bound == 0) {
            if(!heads.isEmpty())
                r.heads.add(EndNode);
            return (r);
        }
        heads.traverse(Traversable.IN_ORDER, BOUNDED_CLONER.get().init(bound),
                       r.heads);
        return (r);
    }

    private static class BoundedCloner<A extends Comparable<? super A>>
            implements TraversalActionPerformer<Node<A>> {

        private int bound;

        BoundedCloner<A> init(int bound) {
            this.bound = bound;
            return (this);
        }

        public boolean perform(Node<A> x, int order, Object o) {
            Node<A> r = x.x == null ? EndNode : new Node<A>(x.x);
            if(x.nextNodes != null)
                if(bound == 1) {
                    if(x.nextNodes.isEmpty())
                        throw (new RuntimeException(
                                getClass().getCanonicalName()
                                + ".cloneBounded: unclosed path discovered"));
                } else if((x.nextNodes.size() != 1) || !x.nextNodes.contains(
                        EndNode)) {
                    r.nextNodes = new BBTree<Node<A>>();
                    bound--;
                    x.nextNodes.traverse(IN_ORDER, this, r.nextNodes);
                    bound++;
                }
            ((Traversable.Set<Node<A>>)o).add(r);
            return (false);
        }
    }
    private static final ThreadLocal<BoundedCloner> BOUNDED_CLONER =
            new ThreadLocal<BoundedCloner>() {

                protected BoundedCloner initialValue() {
                    return (new BoundedCloner());
                }
            };

    public boolean addAll(Trie<A> x) {
        if((x == null) || (x == this))
            return (false);
        if(isFinal())
            throw (new UnsupportedOperationException(
                    "Final trie cannot be modified"));
        Adder<A> p = ADDER.get().init();
        x.heads.traverse(Traversable.IN_ORDER, p, heads);
        return (p.changed);
    }

    private static class Adder<A extends Comparable<? super A>>
            implements TraversalActionPerformer<Node<A>> {

        private boolean changed = false;

        Adder<A> init() {
            changed = false;
            return (this);
        }

        public boolean perform(Node<A> x, int order, Object o) {
            Traversable.Set<Node<A>> u = (Traversable.Set<Node<A>>)o;
            Node.Factory<A> f = NODE_FACTORY.get();
            if(x == EndNode)
                changed |= u.add(EndNode);
            else {
                Node<A> t = u.addOrGetMatch(x.x, f);
                if(t == f.last)
                    changed = true;
                if((x.nextNodes == null) || ((x.nextNodes.size() == 1)
                                             && x.nextNodes.contains(EndNode))) {
                    changed |= (t.nextNodes != null) && t.nextNodes.add(EndNode);
                    return (false);
                }
                if(t.nextNodes == null) {
                    t.nextNodes = new BBTree<Node<A>>();
                    if(t != f.last)
                        changed |= t.nextNodes.add(EndNode);
                }
                x.nextNodes.traverse(IN_ORDER, this, t.nextNodes);
            }
            return (false);
        }
    }
    private static final ThreadLocal<Adder> ADDER = new ThreadLocal<Adder>() {

        protected Adder initialValue() {
            return (new Adder());
        }
    };

    public boolean addAllBounded(Trie<A> x, int bound) {
        if(bound < 0)
            throw (new IllegalArgumentException("Bound is negative"));
        if((x == null) || x.isEmpty())
            return (false);
        if(isFinal())
            throw (new UnsupportedOperationException(
                    "Final trie cannot be modified"));
        if(x == this)
            return (semiBound(bound));
        if(bound == 0)
            if(!x.heads.isEmpty())
                return (heads.add(EndNode));
            else
                return (false);
        BoundedAdder<A> p = BOUNDED_ADDER.get().init(bound);
        x.heads.traverse(Traversable.IN_ORDER, p, heads);
        return (p.changed);
    }

    private static class BoundedAdder<A extends Comparable<? super A>>
            implements TraversalActionPerformer<Node<A>> {

        private int bound = 0;
        private boolean changed = false;

        BoundedAdder<A> init(int bound) {
            this.bound = bound;
            changed = false;
            return (this);
        }

        public boolean perform(Node<A> x, int order, Object o) {
            Traversable.Set<Node<A>> u = (Traversable.Set<Node<A>>)o;
            Node.Factory<A> f = NODE_FACTORY.get();
            if(x == EndNode) {
                if(u != null)
                    changed |= u.add(EndNode);
            } else {
                Node<A> t = u.addOrGetMatch(x.x, f);
                if(t == f.last)
                    changed = true;
                if(bound > 1) {
                    if((x.nextNodes == null) || ((x.nextNodes.size() == 1)
                                                 && x.nextNodes.contains(EndNode))) {
                        changed |= (t.nextNodes != null) && t.nextNodes.add(
                                EndNode);
                        return (false);
                    }
                    if((t.nextNodes == null)) {
                        t.nextNodes = new BBTree<Node<A>>();
                        if(t != f.last)
                            changed |= t.nextNodes.add(EndNode);
                    }
                    bound--;
                    x.nextNodes.traverse(IN_ORDER, this, t.nextNodes);
                    bound++;
                } else if(t.nextNodes != null)
                    changed |= t.nextNodes.add(EndNode);
            }
            return (false);
        }
    }
    private static final ThreadLocal<BoundedAdder> BOUNDED_ADDER =
            new ThreadLocal<BoundedAdder>() {

                protected BoundedAdder initialValue() {
                    return (new BoundedAdder());
                }
            };

    public boolean addConcatenation(Trie<A>[] x) {
        if(isFinal())
            throw (new UnsupportedOperationException(
                    "Final trie cannot be modified"));
        if((x == null) || (x.length == 0))
            return (false);
        for(Trie<A> y : x) {
            if((y == null) || y.isEmpty())
                return (false);
            if(y == this)
                throw (new IllegalArgumentException("Cycle detected"));
        }
        ConcatenationAdder<A> p = CONCATENATION_ADDER.get().init(x);
        x[0].heads.traverse(Traversable.IN_ORDER, p, heads);
        return (p.changed);
    }

    private static class ConcatenationAdder<A extends Comparable<? super A>>
            implements TraversalActionPerformer<Node<A>> {

        private Trie<A>[] x;
        private int i = 0;
        private boolean changed = false;

        ConcatenationAdder<A> init(Trie<A>[] x) {
            this.x = x;
            i = 0;
            changed = false;
            return (this);
        }

        public boolean perform(Node<A> a, int order, Object o) {
            Traversable.Set<Node<A>> u = (Traversable.Set<Node<A>>)o;
            Node.Factory<A> f = NODE_FACTORY.get();
            int j;
            if(a == EndNode) {
                j = i;
                i++;
                while((i < x.length) && ((x[i].heads.size() == 1) && x[i].heads.
                        contains(EndNode)))
                    i++;
                if(i < x.length)
                    x[i].heads.traverse(IN_ORDER, this, u);
                else
                    changed |= u.add(EndNode);
                i = j;
            } else {
                Node<A> b = u.addOrGetMatch(a.x, f);
                if((a.nextNodes == null) || ((a.nextNodes.size() == 1) && a.nextNodes.
                        contains(EndNode))) {
                    j = i;
                    i++;
                    while((i < x.length) && ((x[i].heads.size() == 1) && x[i].heads.
                            contains(EndNode)))
                        i++;
                    if(i < x.length) {
                        if(b.nextNodes == null) {
                            changed = true;
                            b.nextNodes = new BBTree<Node<A>>();
                            if(b != f.last)
                                b.nextNodes.add(EndNode);
                        }
                        x[i].heads.traverse(IN_ORDER, this, b.nextNodes);
                    } else if(b.nextNodes != null)
                        changed |= b.nextNodes.add(EndNode);
                    else if(b == f.last)
                        changed = true;
                    i = j;
                } else {
                    if(b.nextNodes == null) {
                        changed = true;
                        b.nextNodes = new BBTree<Node<A>>();
                        if(b != f.last)
                            b.nextNodes.add(EndNode);
                    }
                    a.nextNodes.traverse(IN_ORDER, this, b.nextNodes);
                }
            }
            return (false);
        }
    }
    private static final ThreadLocal<ConcatenationAdder> CONCATENATION_ADDER =
            new ThreadLocal<ConcatenationAdder>() {

                protected ConcatenationAdder initialValue() {
                    return (new ConcatenationAdder());
                }
            };

    public boolean addBoundedConcatenation(Trie<A>[] x, int bound) {
        if(isFinal())
            throw (new UnsupportedOperationException(
                    "Final trie cannot be modified"));
        if(bound < 0)
            throw (new IllegalArgumentException("Bound is negative"));
        if((x == null) || (x.length == 0))
            return (false);
        for(Trie<A> y : x) {
            if((y == null) || y.isEmpty())
                return (false);
            if(y == this)
                throw (new IllegalArgumentException("Cycle detected"));
        }
        if(bound == 0)
            return (heads.add(EndNode));
        BoundedConcatenationAdder<A> p = BOUNDED_CONCATENATION_ADDER.get().init(
                x, bound);
        x[0].heads.traverse(Traversable.IN_ORDER, p, heads);
        return (p.changed);
    }

    private static class BoundedConcatenationAdder<A extends Comparable<? super A>>
            implements TraversalActionPerformer<Node<A>> {

        private Trie<A>[] x;
        private int i = 0;
        private int bound = 0;
        private boolean changed = false;

        BoundedConcatenationAdder<A> init(Trie<A>[] x, int bound) {
            this.x = x;
            this.bound = bound;
            i = 0;
            changed = false;
            return (this);
        }

        public boolean perform(Node<A> a, int order, Object o) {
            Traversable.Set<Node<A>> u = (Traversable.Set<Node<A>>)o;
            Node.Factory<A> f = NODE_FACTORY.get();
            int j;
            if(a == EndNode) {
                j = i;
                i++;
                while((i < x.length) && ((x[i].heads.size() == 1) && x[i].heads.
                        contains(EndNode)))
                    i++;
                if(i < x.length)
                    x[i].heads.traverse(IN_ORDER, this, u);
                else
                    changed |= u.add(EndNode);
                i = j;
            } else {
                Node<A> b = u.addOrGetMatch(a.x, f);
                if(bound == 1) {
                    if(b == f.last)
                        changed = true;
                    else if(b.nextNodes != null)
                        changed |= b.nextNodes.add(EndNode);
                } else if((a.nextNodes == null) || ((a.nextNodes.size() == 1) && a.nextNodes.
                        contains(EndNode))) {
                    j = i;
                    i++;
                    while((i < x.length) && ((x[i].heads.size() == 1) && x[i].heads.
                            contains(EndNode)))
                        i++;
                    if(i < x.length) {
                        if(b.nextNodes == null) {
                            changed = true;
                            b.nextNodes = new BBTree<Node<A>>();
                            if(b != f.last)
                                b.nextNodes.add(EndNode);
                        }
                        bound--;
                        x[i].heads.traverse(IN_ORDER, this, b.nextNodes);
                        bound++;
                    } else if(b.nextNodes != null)
                        changed |= b.nextNodes.add(EndNode);
                    else if(b == f.last)
                        changed = true;
                    i = j;
                } else {
                    if(b.nextNodes == null) {
                        changed = true;
                        b.nextNodes = new BBTree<Node<A>>();
                        if(b != f.last)
                            b.nextNodes.add(EndNode);
                    }
                    bound--;
                    a.nextNodes.traverse(IN_ORDER, this, b.nextNodes);
                    bound++;
                }
            }
            return (false);
        }
    }
    private static final ThreadLocal<BoundedConcatenationAdder> BOUNDED_CONCATENATION_ADDER =
            new ThreadLocal<BoundedConcatenationAdder>() {

                protected BoundedConcatenationAdder initialValue() {
                    return (new BoundedConcatenationAdder());
                }
            };

    public boolean bound(int bound) {
//        if(isFinal())
//            throw(new UnsupportedOperationException("Final trie cannot be modified"));
        if(bound < 0)
            throw (new IllegalArgumentException("Bound is negative"));
        if(bound == 0) {
            if(heads.isEmpty())
                return (false);
            boolean changed = false;
            if(heads.contains(EndNode))
                changed = heads.size() > 1;
            else
                changed = true;
            heads.clear();
            heads.add(EndNode);
            return (changed);
        }
        Bounder p = BOUNDER.get().init(bound);
        heads.traverse(Traversable.IN_ORDER, p);
        return (p.changed);
    }

    private static class Bounder implements TraversalActionPerformer<Node<?>> {

        private int bound = 0;
        private boolean changed = false;

        Bounder init(int bound) {
            this.bound = bound;
            changed = false;
            return (this);
        }

        public boolean perform(Node<?> x, int order, Object o) {
            if(bound == 1) {
                changed = ((x.nextNodes != null) && !((x.nextNodes.size() == 1)
                                                      && (x.nextNodes.contains(
                        EndNode))));
                x.nextNodes = null;
            } else if(x.nextNodes != null) {
                bound--;
                x.nextNodes.traverse(IN_ORDER, this);
                bound++;
            }
            return (false);
        }
    }
    private static final ThreadLocal<Bounder> BOUNDER = new ThreadLocal<Bounder>() {

        protected Bounder initialValue() {
            return (new Bounder());
        }
    };

    public boolean semiBound(int bound) {
        if(isFinal())
            throw (new UnsupportedOperationException(
                    "Final trie cannot be modified"));
        if(bound < 0)
            throw (new IllegalArgumentException("Bound is negative"));
        if(bound == 0)
            if(heads.isEmpty())
                return (false);
            else
                return (heads.add(EndNode));
        SemiBounder p = SEMI_BOUNDER.get().init(bound);
        heads.traverse(Traversable.IN_ORDER, p);
        return (p.changed);
    }

    private static class SemiBounder implements
            TraversalActionPerformer<Node<?>> {

        private int bound = 0;
        private boolean changed = false;

        SemiBounder init(int bound) {
            this.bound = bound;
            changed = false;
            return (this);
        }

        public boolean perform(Node<?> x, int order, Object o) {
            if(bound == 1)
                changed |= ((x.nextNodes != null) && x.nextNodes.add(EndNode));
            else if(x.nextNodes != null) {
                bound--;
                x.nextNodes.traverse(IN_ORDER, this);
                bound++;
            }
            return (false);
        }
    }
    private static final ThreadLocal<SemiBounder> SEMI_BOUNDER =
            new ThreadLocal<SemiBounder>() {

                protected SemiBounder initialValue() {
                    return (new SemiBounder());
                }
            };

    public int size() {
        Counter p = COUNTER.get().init();
        heads.traverse(Traversable.IN_ORDER, p);
        return (p.count);
    }

    private static class Counter implements TraversalActionPerformer<Node<?>> {

        private int count = 0;

        Trie.Counter init() {
            count = 0;
            return (this);
        }

        public boolean perform(Node<?> x, int order, Object o) {
            if(x.nextNodes == null)
                count++;
            else
                x.nextNodes.traverse(IN_ORDER, this);
            return (false);
        }
    }
    private static final ThreadLocal<Counter> COUNTER =
            new ThreadLocal<Counter>() {

                protected Counter initialValue() {
                    return (new Counter());
                }
            };
    private static final ThreadLocal<Trie[]> TRIE_ARRAY_SINGLE =
            new ThreadLocal<Trie[]>() {

                protected Trie[] initialValue() {
                    return (new Trie[1]);
                }
            };

    public void concat(Trie<A> x) {
        if(isFinal())
            throw (new UnsupportedOperationException(
                    "Final trie cannot be modified"));
        if(x == null) {
            heads.clear();
            return;
        }
        Trie<A>[] ta = TRIE_ARRAY_SINGLE.get();
        ta[0] = x;
        concat(ta);
    }

    public void concat(Trie<A>[] x) {
        if(isFinal())
            throw (new UnsupportedOperationException(
                    "Final trie cannot be modified"));
        if((x == null) || (x.length == 0))
            return;
        for(Trie<A> y : x) {
            if((y == null) || y.isEmpty()) {
                heads.clear();
                return;
            }
            if(y == this)
                throw (new IllegalArgumentException("Cycle detected"));
        }
        Concatenator<A> p = CONCATENATOR.get().init(x);
        heads.traverse(Traversable.IN_ORDER, p);
        if(heads.contains(EndNode)) {
            heads.remove(EndNode);
            p.useSuper = true;
            x[0].heads.traverse(Traversable.IN_ORDER, p, heads);
            p.useSuper = false;
        }
    }

    private static class Concatenator<A extends Comparable<? super A>>
            extends ConcatenationAdder<A> {

        private boolean useSuper = false;

        Concatenator<A> init(Trie<A>[] x) {
            super.init(x);
            useSuper = false;
            return (this);
        }

        public boolean perform(Node<A> x, int order, Object o) {
            if(useSuper)
                super.perform(x, order, o);
            else if(x != EndNode)
                if(x.nextNodes == null) {
                    x.nextNodes = new BBTree<Node<A>>();
                    useSuper = true;
                    super.x[0].heads.traverse(IN_ORDER, this, x.nextNodes);
                    useSuper = false;
                } else {
                    x.nextNodes.traverse(IN_ORDER, this);
                    if(x.nextNodes.contains(EndNode)) {
                        x.nextNodes.remove(EndNode);
                        useSuper = true;
                        super.x[0].heads.traverse(IN_ORDER, this, x.nextNodes);
                        useSuper = false;
                    }
                }
            return (false);
        }
    }
    private static final ThreadLocal<Concatenator> CONCATENATOR =
            new ThreadLocal<Concatenator>() {

                protected Concatenator initialValue() {
                    return (new Concatenator());
                }
            };

    public void concatBounded(Trie<A> x, int bound) {
        if(isFinal())
            throw (new UnsupportedOperationException(
                    "Final trie cannot be modified"));
        if(x == null) {
            heads.clear();
            return;
        }
        Trie<A>[] ta = TRIE_ARRAY_SINGLE.get();
        ta[0] = x;
        concatBounded(ta, bound);
    }

    public void concatBounded(Trie<A>[] x, int bound) {
        if(isFinal())
            throw (new UnsupportedOperationException(
                    "Final trie cannot be modified"));
        if(bound < 0)
            throw (new IllegalArgumentException("Bound is negative"));
        if((x == null) || (x.length == 0))
            return;
        for(Trie<A> y : x) {
            if((y == null) || y.isEmpty()) {
                heads.clear();
                return;
            }
            if(y == this)
                throw (new IllegalArgumentException("Cycle detected"));
        }
        if(bound == 0) {
            if(!heads.isEmpty()) {
                heads.clear();
                heads.add(EndNode);
            }
            return;
        }
        BoundedConcatenator<A> p = BOUNDED_CONCATENATOR.get().init(x, bound);
        heads.traverse(Traversable.IN_ORDER, p);
        if(heads.contains(EndNode)) {
            heads.remove(EndNode);
            p.useSuper = true;
            x[0].heads.traverse(Traversable.IN_ORDER, p, heads);
            p.useSuper = false;
        }
    }

    private static class BoundedConcatenator<A extends Comparable<? super A>>
            extends BoundedConcatenationAdder<A> {

        private boolean useSuper = false;

        BoundedConcatenator<A> init(Trie<A>[] x, int bound) {
            super.init(x, bound);
            useSuper = false;
            return (this);
        }

        public boolean perform(Node<A> x, int order, Object o) {
            if(useSuper)
                super.perform(x, order, o);
            else if(x != EndNode)
                if(super.bound > 1) {
                    super.bound--;
                    if(x.nextNodes == null) {
                        x.nextNodes = new BBTree<Node<A>>();
                        useSuper = true;
                        super.x[0].heads.traverse(IN_ORDER, this, x.nextNodes);
                        useSuper = false;
                    } else {
                        x.nextNodes.traverse(IN_ORDER, this);
                        if(x.nextNodes.contains(EndNode)) {
                            x.nextNodes.remove(EndNode);
                            useSuper = true;
                            super.x[0].heads.traverse(IN_ORDER, this,
                                                      x.nextNodes);
                            useSuper = false;
                        }
                    }
                    super.bound++;
                } else
                    x.nextNodes = null;
            return (false);
        }
    }
    private static final ThreadLocal<BoundedConcatenator> BOUNDED_CONCATENATOR =
            new ThreadLocal<BoundedConcatenator>() {

                protected BoundedConcatenator initialValue() {
                    return (new BoundedConcatenator());
                }
            };
    private static final Node EndNode = new Node(null, null) {

        public boolean equals(Object o) {
            return (o == this);
        }

        public int compareTo(Node x) {
            if(x == null)
                return (1);
            if(x == this)
                return (0);
            return (-1);
        }

        public int asymmetricCompareTo(Object x) {
            if(x == null)
                return (0);
            return (-1);
        }

        public StringBuilder toStringBuilder(StringBuilder sb) {
            if(sb == null)
                sb = new StringBuilder();
            return (sb.append("#"));
        }

        public String toString() {
            return ("#");
        }

        public void writeToStream(Writer w) throws IOException {
            if(w == null)
                return;
            w.write('#');
        }

        public Node clone() {
            return (this);
        }

        public Node cloneBounded(int bound) {
            return (this);
        }

        public boolean bound(int bound) {
            return (false);
        }

        public boolean semiBound(int bound) {
            return (false);
        }
    };

    private static class Node<A extends Comparable<? super A>> implements
            Cloneable,
            Comparable<Node<A>>,
            Writeable, Serializable,
            AsymmetricComparable<A> {

        private static final long serialVersionUID = 0;
        public final A x;
        private Traversable.Set<Node<A>> nextNodes;

        public Node(A x) {
            if(x == null)
                throw (new NullPointerException(
                        getClass().getCanonicalName()
                        + ".contructor: null terminal detected, use EndNode instead"));
            this.x = x;
            this.nextNodes = null;
        }

        private Node(A x, Traversable.Set<Node<A>> nextNodes) {
            this.x = x;
            this.nextNodes = nextNodes;
        }

        public Node clone() {
            Node r = new Node(x);
            if((nextNodes != null)
               && ((nextNodes.size() != 1) || !nextNodes.contains(EndNode))) {
                r.nextNodes = new BBTree<Node<A>>();
                nextNodes.traverse(Traversable.IN_ORDER, CLONER, r.nextNodes);
            }
            return (r);
        }

        public Node cloneBounded(int bound) {
            if(bound <= 0)
                return (EndNode);
            Node r = new Node(x);
            if(nextNodes != null) {
                if(bound == 1) {
                    if(nextNodes.isEmpty())
                        throw (new RuntimeException("Unclosed path discovered"));
                    return (r);
                }
                r.nextNodes = new BBTree<Node<A>>();
                nextNodes.traverse(Traversable.IN_ORDER,
                                   BOUNDED_CLONER.get().init(bound - 1),
                                   r.nextNodes);
            }
            return (r);
        }

        public boolean bound(int bound) {
            if(bound <= 0)
                return (false);
            if(bound == 1) {
                boolean changed = ((nextNodes != null)
                                   && !((nextNodes.size() == 1) && (nextNodes.
                        contains(EndNode))));
                nextNodes = null;
                return (changed);
            } else if(nextNodes != null) {
                Bounder p = BOUNDER.get().init(bound - 1);
                nextNodes.traverse(Traversable.IN_ORDER, p);
                return (p.changed);
            }
            return (false);
        }

        public boolean semiBound(int bound) {
            if(bound <= 0)
                return (false);
            if(bound == 1)
                return ((nextNodes != null) && nextNodes.add(EndNode));
            else if(nextNodes != null) {
                SemiBounder p = SEMI_BOUNDER.get().init(bound - 1);
                nextNodes.traverse(Traversable.IN_ORDER, p);
                return (p.changed);
            }
            return (false);
        }

        public int size() {
            if(nextNodes == null)
                return (1);
            Counter p = COUNTER.get().init();
            nextNodes.traverse(Traversable.IN_ORDER, p);
            return (p.count);
        }

        public void makeFinal() {
            if((nextNodes == null) || nextNodes.isFinal())
                return;
            else {
                nextNodes = nextNodes.toFinal();
                nextNodes.traverse(Traversable.IN_ORDER, FINALIZER);
            }
        }

        public String toString() {
            return (toStringBuilder(null).toString());
        }

        public boolean equals(Object o) {
            if((o == null) || !(o instanceof Node))
                return (false);
            return ((this == o) || (compareTo((Node<A>)o) == 0));
        }

        public int compareTo(Node<A> y) {
            if(y == null)
                return (1);
            if(x == null)
                return (y.x == null ? 0 : -1);
            if(y.x == null)
                return (1);
            return (x.compareTo(y.x));
        }

        public int asymmetricCompareTo(A y) {
            if(x == null)
                return (y == null ? 0 : -1);
            if(y == null)
                return (1);
            return (x.compareTo(y));
        }

        public StringBuilder toStringBuilder(StringBuilder sb) {
            if(sb == null)
                sb = new StringBuilder();
            if(x != null)
                if(x instanceof Writeable)
                    ((Writeable)x).toStringBuilder(sb);
                else
                    sb.append(x);
            if(nextNodes == null)
                return (sb);
            if(nextNodes.isEmpty())
                throw (new RuntimeException("Unclosed path discovered"));
            sb.append('(');
            nextNodes.traverse(IN_ORDER, TRIE_STRING_BUILDER.get().init(), sb);
            sb.append(')');
            return (sb);
        }

        public void writeToStream(Writer w) throws IOException {
            if(w == null)
                return;
            if(x != null)
                if(x instanceof Writeable)
                    ((Writeable)x).writeToStream(w);
                else
                    w.write(x.toString());
            if(nextNodes == null)
                return;
            if(nextNodes.isEmpty())
                throw (new RuntimeException("Unclosed path discovered"));
            w.write('(');
            nextNodes.traverse(IN_ORDER, TRIE_WRITER.get().init(), w);
            if(TABBED_WRITING)
                w.write("\r\n");
            w.write(')');
        }

        public static class Factory<A extends Comparable<? super A>>
                implements KeyedElementFactory<Node<A>, A> {

            Node<A> last;

            public Node<A> lastCreated() {
                return (last);
            }

            public Node<A> createElement(A x) {
                if(x == null)
                    throw (new NullPointerException(
                            getClass().getCanonicalName()
                            + ".createElement: Terminal is null"));
                last = new Node<A>(x);
                return (last);
            }

            public void reset() {
                last = null;
            }

            public A extractKey(Node<A> x) {
                return x.x;
            }
        }
    }

    private static class Iterator<A extends Comparable<? super A>> implements
            java.util.Iterator<Path<A>> {

        private final Selector<? super Path<A>> s;
        private final Trie<A> t;
        private volatile Path<A> last, current;
        private final java.util.LinkedList<java.util.Iterator<Node<A>>> sti;
        private java.util.LinkedList<Path<A>> l;
        private volatile boolean last_removed;

        public Iterator(Trie<A> t) {
            this(t, null);
        }

        public Iterator(Trie<A> t, Selector<Path<A>> s) {
            if(t == null)
                throw (new IllegalArgumentException(
                        getClass().getCanonicalName()
                        + ".constructor: tree to be iterated is null!"));
            this.t = t;
            last_removed = false;
            if(s != null)
                this.s = s;
            else
                this.s = Selector.Accepter;
            last = null;
            current = null;
            sti = new java.util.LinkedList<java.util.Iterator<Node<A>>>();
            sti.push(t.heads.iterator());
            if(sti.peek().hasNext())
                current = new Path<A>();
            pnext();
            while((current != null) && (!this.s.selects(current)))
                pnext();
        }

        private void pnext() {
            if(sti.isEmpty()) {
                current = null;
                return;
            }
            java.util.Iterator<Node<A>> c = sti.peek();
            while((c == null) || !c.hasNext()) {
                sti.pop();
                if(sti.isEmpty()) {
                    current = null;
                    if(l != null)
                        for(Path<A> p : l)
                            t.remove(p);
                    l = null;
                    return;
                }
                current.pop();
                c = sti.peek();
            }
            Node<A> cn;
            while(c.hasNext()) {
                cn = c.next();
                if(cn == EndNode)
                    return;
                current.push(cn.x);
                if(cn.nextNodes == null) {
                    sti.push(null);
                    return;
                }
                c = cn.nextNodes.iterator();
                sti.push(c);
            }
            throw (new RuntimeException(
                    getClass().getCanonicalName() + ".pnext: unclosed path discovered"));
        }

        public Path<A> next() {
            if(current == null)
                throw (new java.util.NoSuchElementException(
                        getClass().getCanonicalName()
                        + ".next: no more elements to iterate"));
            last = current.cloneReversed();
            last_removed = false;
            pnext();
            while((current != null) && (!s.selects(current)))
                pnext();
            return (last);
        }

        public boolean hasNext() {
            return (current != null);
        }

        public void remove() {
            if(last == null)
                throw (new IllegalStateException(
                        getClass().getCanonicalName()
                        + ".remove: iteration has not yet started"));
            if(last_removed) {
                if(current == null)
                    t.remove(last);
                else {
                    if(l == null)
                        l = new java.util.LinkedList<Path<A>>();
                    l.add(last);
                }
                last_removed = true;
            }
        }

        public void finalize() {
            if(l != null)
                for(Path<A> p : l)
                    t.remove(p);
            l = null;
        }
    }

    public static <A extends Comparable<? super A>> boolean ConcatenationContainsPath(
            Trie<A>[] x, Path<A> y) {
        return (ConcatenationContainsPath(x, null, y));
    }

    public static <A extends Comparable<? super A>> boolean ConcatenationContainsPath(
            Trie<A>[] x, boolean[] epsilonAllowed, Path<A> y) {
        if((x == null) || (x.length == 0))
            return (false);
        if(x[0] == null)
            return (false);
        if((epsilonAllowed != null) && (epsilonAllowed.length < x.length))
            throw (new IllegalArgumentException(
                    "pll.Trie.ConcatenationContainsPath:"
                    + " epsilon array must be not less the trie array"));
        return (ConcatenationContainsPath(x, epsilonAllowed, 0,
                                          y != null ? y.head : null,
                                          x[0].heads,
                                          epsilonAllowed != null ? epsilonAllowed[0] : true));
    }

    private static <A extends Comparable<? super A>> boolean ConcatenationContainsPath(
            Trie<A>[] x, boolean[] epsilonAllowed, int i, Path.Node<A> y,
            OrderedSet<Node<A>> z, boolean e) {
        if(y == null)
            if(e && ((z == null) || z.contains(EndNode))) {
                if(i + 1 >= x.length)
                    return (true);
                if(x[i + 1] == null)
                    return (false);
                return (ConcatenationContainsPath(x, epsilonAllowed, i + 1, y,
                                                  x[i + 1].heads,
                                                  epsilonAllowed != null ? epsilonAllowed[i + 1] : true));
            } else
                return (false);
        if(z == null) {
            if(!e)
                return (false);
            if(i + 1 >= x.length)
                return (false);
            if(x[i + 1] == null)
                return (false);
            return (ConcatenationContainsPath(x, epsilonAllowed, i + 1, y,
                                              x[i + 1].heads,
                                              epsilonAllowed != null ? epsilonAllowed[i + 1] : true));
        }
        Node<A> t = z.getMatch(y.x);
        if(t != null)
            if(ConcatenationContainsPath(x, epsilonAllowed, i, y.next,
                                         t.nextNodes, true))
                return (true);
        if(e && z.contains(EndNode)) {
            if(i + 1 >= x.length)
                return (false);
            if(x[i + 1] == null)
                return (false);
            return (ConcatenationContainsPath(x, epsilonAllowed, i + 1, y,
                                              x[i + 1].heads,
                                              epsilonAllowed != null ? epsilonAllowed[i + 1] : true));
        }
        return (false);
    }

    public static <A extends Comparable<? super A>> boolean BoundedConcatenationContainsPath(
            Trie<A>[] x, int bound, Path<A> y) {
        return (BoundedConcatenationContainsPath(x, bound, null, y));
    }

    public static <A extends Comparable<? super A>> boolean BoundedConcatenationContainsPath(
            Trie<A>[] x, int bound,
            boolean[] epsilonAllowed,
            Path<A> y) {
        if((x == null) || (x.length == 0))
            return (false);
        if(x[0] == null)
            return (false);
        if(bound < 0)
            throw (new IllegalArgumentException(
                    "pll.Trie.BoundedConcatenationContainsPath: bound is negative"));
        if((epsilonAllowed != null) && (epsilonAllowed.length < x.length))
            throw (new IllegalArgumentException(
                    "pll.Trie.ConcatenationContainsPath:"
                    + " epsilon array must be not less the trie array"));
//        if((y!=null)&&(bound<y.length()))
//            return(false);
        return (BoundedConcatenationContainsPath(x, bound, epsilonAllowed, 0,
                                                 y != null ? y.head : null,
                                                 x[0].heads,
                                                 epsilonAllowed != null ? epsilonAllowed[0] : true));
    }

    private static <A extends Comparable<? super A>> boolean BoundedConcatenationContainsPath(
            Trie<A>[] x, int bound,
            boolean[] epsilonAllowed,
            int i,
            Path.Node<A> y,
            OrderedSet<Node<A>> z,
            boolean e) {
        if((y == null) || (bound == 0))
            if(bound == 0 ? (e ? (z == null) || !z.isEmpty()
                             : (z != null) && (z.size() > (z.contains(EndNode) ? 1 : 0)))
               : (e && ((z == null) || z.contains(EndNode)))) {
                if(i + 1 >= x.length)
                    return (true);
                if(x[i + 1] == null)
                    return (false);
                return (BoundedConcatenationContainsPath(x, bound,
                                                         epsilonAllowed, i + 1,
                                                         y,
                                                         x[i + 1].heads,
                                                         epsilonAllowed != null ? epsilonAllowed[i + 1] : true));
            } else
                return (false);
        if(z == null) {
            if(!e)
                return (false);
            if(i + 1 >= x.length)
                return (false);
            if(x[i + 1] == null)
                return (false);
            return (BoundedConcatenationContainsPath(x, bound, epsilonAllowed,
                                                     i + 1, y,
                                                     x[i + 1].heads,
                                                     epsilonAllowed != null ? epsilonAllowed[i + 1] : true));
        }
        Node<A> t = z.getMatch(y.x);
        if(t != null)
            if(BoundedConcatenationContainsPath(x, bound - 1, epsilonAllowed, i,
                                                y.next, t.nextNodes, true))
                return (true);
        if(e && z.contains(EndNode)) {
            if(i + 1 >= x.length)
                return (false);
            if(x[i + 1] == null)
                return (false);
            return (BoundedConcatenationContainsPath(x, bound, epsilonAllowed,
                                                     i + 1, y,
                                                     x[i + 1].heads,
                                                     epsilonAllowed != null ? epsilonAllowed[i + 1] : true));
        }
        return (false);
    }

    public static <A extends Comparable<? super A>> Trie<A> Concatenation(
            Trie<A>[] x) {
        if((x == null) || (x.length == 0) || (x[0] == null))
            return (null);
        Trie<A> r = new Trie<A>();
        r.addConcatenation(x);
        return (r);
    }

    public static <A extends Comparable<? super A>> Trie<A> BoundedConcatenation(
            Trie<A>[] x, int bound) {
        if(bound < 0)
            throw (new IllegalArgumentException(
                    "pll.Trie.BoundedConcatenation: bound is negative"));
        if((x == null) || (x.length == 0) || (x[0] == null))
            return (null);
        Trie<A> r = new Trie<A>();
        r.addBoundedConcatenation(x, bound);
        return (r);
    }

    public int traverse(int order, TraversalActionPerformer<? super Path<A>> p) {
        return (traverseSelected(order, p, null, null));
    }

    public int traverse(int order, TraversalActionPerformer<? super Path<A>> p,
                        Object o) {
        return (traverseSelected(order, p, o, null));
    }

    public int traverseSelected(int order,
                                TraversalActionPerformer<? super Path<A>> p,
                                Selector<Path<A>> s) {
        return (traverseSelected(order, p, null, s));
    }

    public int traverseSelected(int order,
                                TraversalActionPerformer<? super Path<A>> p,
                                Object o,
                                Selector<Path<A>> s) {
        return (traverseSelectedRange(order, p, o, s, null, true, null, true));
    }

    public int traverseRange(int order,
                             TraversalActionPerformer<? super Path<A>> p,
                             Path<A> l, Path<A> r) {
        return (traverseSelectedRange(order, p, null, null, l, true, r, false));
    }

    public int traverseRange(int order,
                             TraversalActionPerformer<? super Path<A>> p,
                             Object o, Path<A> l, Path<A> r) {
        return (traverseSelectedRange(order, p, o, null, l, true, r, false));
    }

    public int traverseRange(int order,
                             TraversalActionPerformer<? super Path<A>> p,
                             Path<A> l, boolean left_included,
                             Path<A> r, boolean right_included) {
        return (traverseSelectedRange(order, p, null, null, l, left_included, r,
                                      right_included));
    }

    public int traverseRange(int order,
                             TraversalActionPerformer<? super Path<A>> p,
                             Object o, Path<A> l, boolean left_included,
                             Path<A> r, boolean right_included) {
        return (traverseSelectedRange(order, p, o, null, l, left_included, r,
                                      right_included));
    }

    public int traverseSelectedRange(int order,
                                     TraversalActionPerformer<? super Path<A>> p,
                                     Selector<Path<A>> s, Path<A> l, Path<A> r) {
        return (traverseSelectedRange(order, p, null, s, l, true, r, false));
    }

    public int traverseSelectedRange(int order,
                                     TraversalActionPerformer<? super Path<A>> p,
                                     Object o, Selector<Path<A>> s, Path<A> l,
                                     Path<A> r) {
        return (traverseSelectedRange(order, p, o, s, l, true, r, false));
    }

    public int traverseSelectedRange(int order,
                                     TraversalActionPerformer<? super Path<A>> p,
                                     Selector<Path<A>> s, Path<A> l,
                                     boolean left_included,
                                     Path<A> r, boolean right_included) {
        return (traverseSelectedRange(order, p, null, s, l, left_included, r,
                                      right_included));
    }

    public int traverseSelectedRange(int order,
                                     TraversalActionPerformer<? super Path<A>> p,
                                     Object o, Selector<Path<A>> s, Path<A> l,
                                     boolean left_included,
                                     Path<A> r, boolean right_included) {
        if((order == 0) || (p == null))
            return (0);
        Path<A> sl, sr;
        if(s == null) {
            sl = l;
            sr = r;
        } else {
            sl = s.lowerBound();
            if((l != null) && ((sl == null) || l.compareTo(sl) > 0))
                sl = l;
            else
                left_included = (sl == null) || s.selects(sl);
            sr = s.upperBound();
            if((r != null) && ((sr == null) || r.compareTo(sr) < 0))
                sr = r;
            else
                right_included = (sr == null) || s.selects(sr);
        }
        if((sl == null) && (sr == null)) {
            SelectiveTraverser<A> tp = new SelectiveTraverser<A>(p, s);
            heads.traverse(order, tp, o);
            return (tp.np);
        }
        SelectiveRangeTraverser<A> tp = new SelectiveRangeTraverser<A>(p, s,
                                                                       sl,
                                                                       left_included,
                                                                       sr,
                                                                       right_included);
        heads.traverseRange(order, tp, o,
                            ((sl != null) && (sl.head != null)) ? heads.
                ceilMatch(sl.head.x) : null,
                            (sl != null) && (sl.head != null) && (sl.head.next == null) ? left_included : true,
                            ((sr != null) && (sr.head != null)) ? heads.
                floorMatch(sr.head.x) : null,
                            (sr != null) && (sr.head != null) && (sr.head.next == null) ? right_included : true);
        return (tp.np);
    }

    private static class SelectiveTraverser<A extends Comparable<? super A>>
            implements TraversalActionPerformer<Node<A>> {

        private final TraversalActionPerformer<? super Path<A>> p;
        private final Selector<Path<A>> s;
        private final Path<A> cp = new Path<A>();
        private int np = 0;

        public SelectiveTraverser(
                TraversalActionPerformer<? super Path<A>> p,
                Selector<Path<A>> s) {
            if(p == null)
                throw (new NullPointerException(
                        "TraversalActionPerformer is null"));
            this.p = p;
            if(s == null)
                this.s = Selector.Accepter;
            else
                this.s = s;
        }

        public boolean perform(Node<A> x, int order, Object o) {
            if(x == EndNode) {
                np++;
                Path<A> cpr = cp.cloneReversed();
                if(s.selects(cpr) && p.perform(cpr, order, o)) {
                    np = -np;
                    return (true);
                }
            } else {
                cp.push(x.x);
                if(x.nextNodes == null) {
                    np++;
                    Path<A> cpr = cp.cloneReversed();
                    if(s.selects(cpr) && p.perform(cp.cloneReversed(), order, o)) {
                        np = -np;
                        cp.pop();
                        return (true);
                    }
                } else if(x.nextNodes.traverse(order, this, o) < 0) {
                    np = -np;
                    cp.pop();
                    return (true);
                }
                cp.pop();
            }
            return (false);
        }

        public int getCount() {
            return (np);
        }
    }

    private static class SelectiveRangeTraverser<A extends Comparable<? super A>>
            implements TraversalActionPerformer<Node<A>> {

        public final TraversalActionPerformer<? super Path<A>> p;
        private final Path<A> cp = new Path<A>();
        public final Selector<Path<A>> s;
        private Path.Node<A> l, r;
        boolean left_included, right_included;
        private int np = 0;

        public SelectiveRangeTraverser(
                TraversalActionPerformer<? super Path<A>> p,
                Selector<Path<A>> s, Path<A> l,
                boolean left_included,
                Path<A> r, boolean right_included) {
            if(p == null)
                throw (new NullPointerException(
                        "TraversalActionPerformer is null"));
            this.p = p;
            if(s == null)
                this.s = Selector.Accepter;
            else
                this.s = s;
            this.l = l != null ? l.head : null;
            this.r = r != null ? r.head : null;
            this.left_included = left_included;
            this.right_included = right_included;
        }

        public boolean perform(Node<A> x, int order, Object o) {
            if(x == EndNode) {
                np++;
                Path<A> cpr = cp.cloneReversed();
                if(s.selects(cpr) && p.perform(cp.cloneReversed(), order, o)) {
                    np = -np;
                    return (true);
                }
            } else {
                cp.push(x.x);
                Path.Node<A> lt = l;
                Path.Node<A> rt = r;
                l = l != null ? l.next : null;
                r = r != null ? r.next : null;
                if(x.nextNodes == null) {
                    np++;
                    Path<A> cpr = cp.cloneReversed();
                    if(s.selects(cpr) && p.perform(cp.cloneReversed(), order, o)) {
                        np = -np;
                        cp.pop();
                        l = lt;
                        r = rt;
                        return (true);
                    }
                } else if(x.nextNodes.traverseRange(order, this, o,
                                                    (l != null) ? x.nextNodes.
                        ceilMatch(l.x) : null,
                                                    (l != null) && (l.next == null) ? left_included : true,
                                                    (r != null) ? x.nextNodes.
                        floorMatch(r.x) : null,
                                                    (r != null) && (r.next == null) ? right_included : true) < 0) {
                    np = -np;
                    cp.pop();
                    l = lt;
                    r = rt;
                    return (true);
                }
                l = lt;
                r = rt;
                cp.pop();
            }
            return (false);
        }
    }

    public boolean containsAll(Trie x) {
        if((x == null) || x.isEmpty())
            return (true);
        if(x == this)
            return (true);
        if(isEmpty())
            return (false);
        return (x.heads.traverse(IN_ORDER, CONTAINMENT_CHECKER, heads) >= 0);
    }
    private static final TraversalActionPerformer<Node> CONTAINMENT_CHECKER =
            new TraversalActionPerformer<Node>() {

                public boolean perform(Node x, int order, Object o) {
                    Traversable.Set<Node> u = (Traversable.Set<Node>)o;
                    if(x == EndNode)
                        return ((u != null) && !u.contains(EndNode));
                    else {
                        if(u == null)
                            return (true);
                        Node y = u.get(x);
                        if(y == null)
                            return (true);
                        if(x.nextNodes == null) {
                            if(y.nextNodes == null)
                                return (false);
                            if(y.nextNodes.isEmpty())
                                throw (new RuntimeException(
                                        "Unclosed path discovered"));
                            return (y.nextNodes.contains(EndNode));
                        }
                        if(y.nextNodes == null)
                            return (true);
                        if(x.nextNodes.isEmpty() || y.nextNodes.isEmpty())
                            throw (new RuntimeException(
                                    "Unclosed path discovered"));
                        return (x.nextNodes.traverse(IN_ORDER,
                                                     CONTAINMENT_CHECKER,
                                                     y.nextNodes) < 0);
                    }
                }
            };

    public boolean containsConcatenation(Trie[] x) {
        if((x == null) || (x.length == 0))
            return (true);
        for(Trie y : x)
            if((y == null) || y.isEmpty())
                return (true);
        if(isEmpty())
            return (false);
        ConcatenationContainmentChecker p = CONCATENATION_CONTAINMENT_CHECKER.
                get().init(x);
        return (x[0].heads.traverse(IN_ORDER, p, heads) >= 0);
    }

    private static class ConcatenationContainmentChecker
            implements TraversalActionPerformer<Node> {

        private Trie[] x;
        private int i = 0;

        ConcatenationContainmentChecker init(Trie<?>[] x) {
            this.x = x;
            i = 0;
            return (this);
        }

        public boolean perform(Node a, int order, Object o) {
            Traversable.Set<Node> u = (Traversable.Set<Node>)o;
            int j;
            if(a == EndNode) {
                j = i;
                i++;
                while((i < x.length) && ((x[i].heads.size() == 1) && x[i].heads.
                        contains(EndNode)))
                    i++;
                if(i < x.length)
                    if(x[i].heads.traverse(IN_ORDER, this, u) < 0) {
                        i = j;
                        return (true);
                    } else {
                        i = j;
                        return (false);
                    }
                else {
                    i = j;
                    return ((u != null) && !u.contains(EndNode));
                }
            } else {
                if(u == null)
                    return (true);
                Node b = u.get(a);
                if(b == null)
                    return (true);
                if((a.nextNodes == null) || ((a.nextNodes.size() == 1) && a.nextNodes.
                        contains(EndNode))) {
                    j = i;
                    i++;
                    while((i < x.length) && ((x[i].heads.size() == 1) && x[i].heads.
                            contains(EndNode)))
                        i++;
                    if(i < x.length) {
                        if(b.nextNodes == null) {
                            i = j;
                            return (true);
                        }
                        if(b.nextNodes.isEmpty())
                            throw (new RuntimeException(
                                    "Unclosed path discovered"));
                        if(x[i].heads.traverse(IN_ORDER, this, b.nextNodes) < 0) {
                            i = j;
                            return (true);
                        } else {
                            i = j;
                            return (false);
                        }
                    } else {
                        i = j;
                        return ((b.nextNodes != null) && !b.nextNodes.contains(
                                EndNode));
                    }
                } else {
                    if(b.nextNodes == null)
                        return (true);
                    return (a.nextNodes.traverse(IN_ORDER, this, b.nextNodes) < 0);
                }
            }
        }
    }
    private static final ThreadLocal<ConcatenationContainmentChecker> CONCATENATION_CONTAINMENT_CHECKER =
            new ThreadLocal<ConcatenationContainmentChecker>() {

                protected ConcatenationContainmentChecker initialValue() {
                    return (new ConcatenationContainmentChecker());
                }
            };

    public boolean containsBoundedConcatenation(Trie[] x, int bound) {
        if(bound < 0)
            throw (new IllegalArgumentException("Bound is negative"));
        if((x == null) || (x.length == 0))
            return (true);
        for(Trie y : x)
            if((y == null) || y.isEmpty())
                return (true);
        if(bound == 0)
            return (heads.contains(EndNode));
        BoundedConcatenationContainmentChecker p =
                BOUNDED_CONCATENATION_CONTAINMENT_CHECKER.get().init(x, bound);
        return (x[0].heads.traverse(IN_ORDER, p, heads) >= 0);
    }

    private static class BoundedConcatenationContainmentChecker
            implements TraversalActionPerformer<Node> {

        private Trie[] x;
        private int i = 0;
        private int bound = 0;

        BoundedConcatenationContainmentChecker init(Trie[] x, int bound) {
            this.x = x;
            this.bound = bound;
            i = 0;
            return (this);
        }

        public boolean perform(Node a, int order, Object o) {
            Traversable.Set<Node> u = (Traversable.Set<Node>)o;
            int j;
            if(a == EndNode) {
                j = i;
                i++;
                while((i < x.length) && ((x[i].heads.size() == 1) && x[i].heads.
                        contains(EndNode)))
                    i++;
                if(i < x.length)
                    if(x[i].heads.traverse(IN_ORDER, this, u) < 0) {
                        i = j;
                        return (true);
                    } else {
                        i = j;
                        return (false);
                    }
                else {
                    i = j;
                    return ((u != null) && !u.contains(EndNode));
                }
            } else {
                Node b = u.get(a);
                if(b == null)
                    return (true);
                if(bound == 1)
                    return ((u != null) && !u.contains(EndNode));
                else if((a.nextNodes == null) || ((a.nextNodes.size() == 1) && a.nextNodes.
                        contains(EndNode))) {
                    j = i;
                    i++;
                    while((i < x.length) && ((x[i].heads.size() == 1) && x[i].heads.
                            contains(EndNode)))
                        i++;
                    if(i < x.length) {
                        if(b.nextNodes == null) {
                            i = j;
                            return (true);
                        }
                        if(b.nextNodes.isEmpty())
                            throw (new RuntimeException(
                                    "Unclosed path discovered"));
                        bound--;
                        if(x[i].heads.traverse(IN_ORDER, this, b.nextNodes) < 0) {
                            bound++;
                            i = j;
                            return (true);
                        } else {
                            bound++;
                            i = j;
                            return (false);
                        }
                    } else {
                        i = j;
                        return ((b.nextNodes != null) && !b.nextNodes.contains(
                                EndNode));
                    }
                } else {
                    if(b.nextNodes == null)
                        return (true);
                    bound--;
                    if(a.nextNodes.traverse(IN_ORDER, this, b.nextNodes) < 0) {
                        bound++;
                        return (true);
                    } else {
                        bound++;
                        return (true);
                    }
                }
            }
        }
    }
    private static final ThreadLocal<BoundedConcatenationContainmentChecker> BOUNDED_CONCATENATION_CONTAINMENT_CHECKER =
            new ThreadLocal<BoundedConcatenationContainmentChecker>() {

                protected BoundedConcatenationContainmentChecker initialValue() {
                    return (new BoundedConcatenationContainmentChecker());
                }
            };

    public int emptyTo(TraversalActionPerformer<? super Path<A>> p) {
        return (emptyTo(p, null));
    }

    public int emptyTo(TraversalActionPerformer<? super Path<A>> p, Object o) {
        if(p == null)
            throw (new NullPointerException("Performer is null"));
        if(heads.isEmpty())
            return (0);
        int h = traverse(IN_ORDER, p, o);
        clear();
        return (h);
    }
}
