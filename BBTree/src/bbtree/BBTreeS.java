/*
 * BBTreeS.java
 *
 * Created on 21. Juli 2006, 12:34
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */
package bbtree;

import bbtree.Utils.AsymmetricComparator;
import bbtree.Utils.KeyedElementFactory;
import java.util.*;
import java.io.*;

/**
 * A BBTreeS is binary balanced tree, also known as AVL-tree. It is a direct implementation of {@link bbtree.OrderedSet} and provides log(n) time cost for all primitive operations.
 * @author RJ
 */
public class BBTreeS<A> implements Traversable.Set<A> {

    //private constants for balancing
    private static final int BALANCED = 0;
    private static final int LEFT_HIGH = -1;
    private static final int RIGHT_HIGH = 1;
    //private constants for traversing
    private static final int FROM_TOP = 0;
    private static final int DOWN_LEFT = 1;
    private static final int DOWN_RIGHT = 2;
    //private return codes for structural modfying
    private static final int DEPTH_CHANGED = 1;
    private static final int LEAF_DELETED = 2;
    //vars that describe a binary balanced tree
    private transient Node<A> root = null;
    private Comparator<? super A> comp = null;
    private int count = 0;
    //global var for data fetching
    private transient A current_x = null;

    /**
     * Creates a new (empty) instance of BBTreeS with natural ordering.
     */
    public BBTreeS() {
        this((Comparator<? super A>)null, false);
    }

    public BBTreeS(boolean reverseOrder) {
        this((Comparator<? super A>)null, reverseOrder);
    }

    /**
     * Creates a new (empty) instance of BBTreeS with the specified comparator for ordering.
     * If the given comparator is null, natural ordering is used.
     * @param c the comparator which shall be used for ordering the elements of this tree.
     * If it is null, natural ordering is used.
     */
    public BBTreeS(Comparator<? super A> c) {
        this(c, false);
    }

    public BBTreeS(Comparator<? super A> c, boolean reverseOrder) {
        this.comp = Utils.wrapComparator(c, reverseOrder);
    }

    public BBTreeS(Collection<? extends A> c) {
        this(c, false);
    }

    public BBTreeS(Collection<? extends A> c, boolean reverseOrder) {
        this((Comparator<? super A>)null, reverseOrder);
        addAll(c);
    }

    public BBTreeS(SortedSet<? extends A> c) {
        this(c, false);
    }

    public BBTreeS(SortedSet<? extends A> s, boolean reverseOrder) {
        this((Comparator<? super A>)s.comparator(), reverseOrder);
        addAll(s);
    }

    public int compare(A x, A y) {
        return comp.compare(x, y);
    }

    public <B> int asymmetricCompare(A x, B y) {
        return ((AsymmetricComparator<A>)comp).asymmetricCompare(x, y);
    }

    public Comparator<? super A> comparator() {
        return comp != Utils.NATURAL_COMPARATOR ? comp : null;
    }

    public synchronized boolean isEmpty() {
        return root == null;
    }

    public synchronized int size() {
        return count;
    }

    public synchronized int maxSearchDepth() {
        return pmaxSearchDepth(root);
    }

    public synchronized boolean contains(Object o) {
        return o == null ? false : get((A)o) != null;
    }

    public synchronized boolean containsAll(Collection<?> c) {
        return containsAll(c, null);
    }

    public synchronized boolean containsAll(Collection<?> c, Selector<?> s) {
        if((c == null) || (c == this))
            return true;
        if(c instanceof Traversable) {
            TraversalActionPerformer.LogicalJunctor<Object> a =
                    new TraversalActionPerformer.LogicalJunctor<Object>(
                    TraversalActionPerformer.LOGICAL_AND,
                    new Selector.Conjunctor(
                    (Selector<Object>)this, (Selector<Object>)s));
            ((Traversable<Object>)c).traverse(IN_ORDER, a);
            return a.getState();
        }
        if(s != null) {
            for(Object o : c)
                if(!((Selector<Object>)s).selects(o) || !contains(o))
                    return false;
        } else
            for(Object o : c)
                if(!contains(o))
                    return false;
        return true;
    }

    public boolean isContainedIn(Collection<?> c) {
        if(c == null)
            return false;
        if(c == this)
            return true;
        TraversalActionPerformer.LogicalJunctor<A> a =
                new TraversalActionPerformer.LogicalJunctor<A>(
                TraversalActionPerformer.LOGICAL_AND,
                (c instanceof OrderedSet) ? (Selector<A>)c
                : new Selector.ContainmentChecker<A>(c));
        traverse(IN_ORDER, a);
        return a.getState();
    }

    public synchronized A get(A x) {
        if(x == null)
            return null;
        Node<A> t = root;
        int c;
        while(t != null) {
            c = comp.compare(x, t.val);
            if(c == 0)
                return t.val;
            else if(c < 0)
                t = t.left;
            else
                t = t.right;
        }
        return null;
    }

    public synchronized A set(A x) {
        if(x == null)
            return null;
        Merger.Setter<A> m = new Merger.Setter<A>();
        add(x, m);
        return m.old;
    }

    public synchronized A getComparable(Comparable<? super A> x) {
        if(x == null)
            return null;
        Node<A> t = root;
        int c;
        while(t != null) {
            c = x.compareTo(t.val);
            if(c == 0)
                return t.val;
            else if(c < 0)
                t = t.left;
            else
                t = t.right;
        }
        return null;
    }

    public synchronized <B> A getMatch(B x) {
        if(x == null)
            return null;
        Node<A> t = root;
        int c;
        while(t != null) {
            c = -((AsymmetricComparator<A>)comp).asymmetricCompare(t.val,x);
            if(c == 0)
                return t.val;
            else if(c < 0)
                t = t.left;
            else
                t = t.right;
        }
        return null;
    }

    public synchronized A first() {
        if(root == null)
            return null;
        Node<A> t = root;
        while(t.left != null)
            t = t.left;
        return t.val;
    }

    public synchronized A last() {
        if(root == null)
            return null;
        Node<A> t = root;
        while(t.right != null)
            t = t.right;
        return t.val;
    }

    public synchronized A ceiling(A x) {
        if(x == null)
            return first();
        Node<A> t = root;
        A y = null;
        int c;
        while(t != null) {
            c = comp.compare(x, t.val);
            if(c == 0)
                return t.val;
            if(c > 0)
                t = t.right;
            else {
                y = t.val;
                t = t.left;
            }
        }
        return y;
    }

    public synchronized A ceilComparable(Comparable<? super A> x) {
        if(x == null)
            return first();
        Node<A> t = root;
        A y = null;
        int c;
        while(t != null) {
            c = x.compareTo(t.val);
            if(c == 0)
                return t.val;
            if(c > 0)
                t = t.right;
            else {
                y = t.val;
                t = t.left;
            }
        }
        return y;
    }

    public synchronized <B> A ceilMatch(B x) {
        if(x == null)
            return first();
        Node<A> t = root;
        A y = null;
        int c;
        while(t != null) {
            c = ((AsymmetricComparator<A>)comp).asymmetricCompare(t.val,x);
            if(c == 0)
                return t.val;
            if(c < 0)
                t = t.right;
            else {
                y = t.val;
                t = t.left;
            }
        }
        return y;
    }

    public synchronized A floor(A x) {
        if(x == null)
            return last();
        Node<A> t = root;
        A y = null;
        int c;
        while(t != null) {
            c = comp.compare(x, t.val);
            if(c == 0)
                return t.val;
            if(c > 0) {
                y = t.val;
                t = t.right;
            } else
                t = t.left;
        }
        return y;
    }

    public synchronized A floorComparable(Comparable<? super A> x) {
        if(x == null)
            return last();
        Node<A> t = root;
        A y = null;
        int c;
        while(t != null) {
            c = x.compareTo(t.val);
            if(c == 0)
                return t.val;
            if(c > 0) {
                y = t.val;
                t = t.right;
            } else
                t = t.left;
        }
        return y;
    }

    public synchronized <B> A floorMatch(B x) {
        if(x == null)
            return last();
        Node<A> t = root;
        A y = null;
        int c;
        while(t != null) {
            c = ((AsymmetricComparator<A>)comp).asymmetricCompare(t.val,x);
            if(c == 0)
                return t.val;
            if(c < 0) {
                y = t.val;
                t = t.right;
            } else
                t = t.left;
        }
        return y;
    }

    public synchronized A higher(A x) {
        if(x == null)
            return first();
        Node<A> t = root;
        A y = null;
        while(t != null)
            if(comp.compare(x, t.val) < 0) {
                y = t.val;
                t = t.left;
            } else
                t = t.right;
        return y;
    }

    public synchronized A higherComparable(Comparable<? super A> x) {
        if(x == null)
            return first();
        Node<A> t = root;
        A y = null;
        while(t != null)
            if(x.compareTo(t.val) < 0) {
                y = t.val;
                t = t.left;
            } else
                t = t.right;
        return y;
    }

    public synchronized <B> A higherMatch(B x) {
        if(x == null)
            return first();
        Node<A> t = root;
        A y = null;
        while(t != null)
            if(((AsymmetricComparator<A>)comp).asymmetricCompare(t.val,x) > 0) {
                y = t.val;
                t = t.left;
            } else
                t = t.right;
        return y;
    }

    public synchronized A lower(A x) {
        if(x == null)
            return last();
        Node<A> t = root;
        A y = null;
        while(t != null)
            if(comp.compare(x, t.val) > 0) {
                y = t.val;
                t = t.right;
            } else
                t = t.left;
        return y;
    }

    public synchronized A lowerComparable(Comparable<? super A> x) {
        if(x == null)
            return last();
        Node<A> t = root;
        A y = null;
        while(t != null)
            if(x.compareTo(t.val) > 0) {
                y = t.val;
                t = t.right;
            } else
                t = t.left;
        return y;
    }

    public synchronized <B> A lowerMatch(B x) {
        if(x == null)
            return last();
        Node<A> t = root;
        A y = null;
        while(t != null)
            if(((AsymmetricComparator<A>)comp).asymmetricCompare(t.val,x) < 0) {
                y = t.val;
                t = t.right;
            } else
                t = t.left;
        return y;
    }

    public boolean add(A x) {
        return add(x, Merger.Keeper);
    }

    public synchronized A addOrGet(A x) {
        add(x, null);
        return current_x;
    }

    public synchronized boolean add(A x, Merger<A> m) {
        if(x == null)
            throw new NullPointerException("Null elements are not allowed");
        this.current_x = x;
        if(root == null) {
            if(m != null)
                m.merge(null, current_x);
            root = new Node<A>(x);
            count = 1;
            return true;
        }
        int old_count = count;
        padd(root, m);
        assert count >= old_count;
        return count > old_count;
    }

    public synchronized <B> A addOrGetMatch(B x, KeyedElementFactory<A, B> f) {
        if(f == null)
            throw new NullPointerException("Element factory is null");
        f.reset();
        if(root == null) {
            A y = f.createElement(x);
            if(y == null)
                throw new NullPointerException(
                        "Element factory created null element");
            root = new Node<A>(y);
            count = 1;
            return y;
        }
        int old_count = count;
        paddOrGetMatch(root, x, f);
        assert count >= old_count;
        return current_x;
    }

    public boolean addAll(Collection<? extends A> c) {
        return addAll(c, null, false, Merger.Keeper);
    }

    public boolean addAll(Collection<? extends A> c, Merger<A> m) {
        return addAll(c, null, false, m);
    }

    public boolean addAll(Collection<? extends A> c, Selector<? super A> s,
                          boolean abort_on_illegal_argument) {
        return addAll(c, s, abort_on_illegal_argument, Merger.Keeper);
    }

    public synchronized boolean addAll(Collection<? extends A> c,
                                       Selector<? super A> s,
                                       boolean abort_on_illegal_argument,
                                       Merger<A> m) {
        if(c == null)
            return false;
        int old_count = count;
        if(c instanceof Traversable)
            if(((Traversable<A>)c).traverse(IN_ORDER,
                                            new TraversalActionPerformer.OrderedSetAdder<A>(
                    this, s, abort_on_illegal_argument, m)) < 0)
                throw new IllegalArgumentException(
                        "argument not selected by selector");
            else if(s != null)
                for(A x : c)
                    if(s.selects(x))
                        add(x, m);
                    else if(abort_on_illegal_argument)
                        throw new IllegalArgumentException(
                                "argument not selected by selector");
                    else
                        for(A y : c)
                            add(y, m);
        assert count >= old_count;
        return count > old_count;
    }

    public boolean removeAll(Collection<?> c) {
        return removeAll(c, null);
    }

    public synchronized <B> boolean removeAll(Collection<B> c,
                                              Selector<? super B> s) {
        if(c == null)
            return false;
        if(c == this)
            return removeAll((Selector<A>)s);
        int old_count = count;
        if(c instanceof Traversable)
            ((Traversable<B>)c).traverseSelected(IN_ORDER,
                                                 new TraversalActionPerformer.CollectionDropper<B>(
                    (Collection<Object>)this), (Selector<B>)s);
        else if(s != null)
            for(B o : c)
                if(s.selects(o))
                    remove(o);
                else
                    for(B p : c)
                        remove(p);
        assert count >= old_count;
        return count > old_count;
    }

    public synchronized boolean removeAll(Selector<? super A> s) {
        int old_count = count;
        if(s == null) {
            clear();
            return old_count > 0;
        }
        Node<A> l = toLinkedList(), r;
        count = 0;
        while(l != null) {
            r = l.right;
            if(!s.selects(l.val))
                addNode(l, null);
            l = r;
        }
        assert count <= old_count;
        return count < old_count;
    }

    public synchronized boolean retainAll(Collection<?> c) {
        if(c == null) {
            int old_count = count;
            clear();
            return old_count > 0;
        }
        if(c == this)
            return false;
        return retainAll((Selector<A>)((c instanceof OrderedSet)
                                        ? c : new Selector.ContainmentChecker<A>(
                (Collection<Object>)c)));
    }

    public synchronized boolean retainAll(Selector<? super A> s) {
        if(s == null)
            return false;
        int old_count = count;
        Node<A> l = toLinkedList(), r;
        count = 0;
        while(l != null) {
            r = l.right;
            if(s.selects(l.val))
                addNode(l, null);
            l = r;
        }
        assert count <= old_count;
        return count < old_count;
    }

    public synchronized boolean remove(Object x) {
        if((x == null) || (root == null))
            return false;
        current_x = null;
        if(premove(root, (A)x) == LEAF_DELETED) {
            root = null;
            if(count != 0)
                System.err.println(
                        "Error in BBTree: delete: tree should be empty now");
        }
        //check();
        return current_x != null;
    }

    public synchronized A removeComparable(Comparable<? super A> x) {
        if((x == null) || (root == null))
            return null;
        current_x = null;
        if(premoveComparable(root, x) == LEAF_DELETED) {
            root = null;
            if(count != 0)
                System.err.println(
                        "Error in BBTree: delete: tree should be empty now");
        }
        //check();
        return current_x;
    }

    public synchronized <B> A removeMatch(B x) {
        if((x == null) || (root == null))
            return null;
        current_x = null;
        if(premoveMatch(root, x) == LEAF_DELETED) {
            root = null;
            if(count != 0)
                System.err.println(
                        "Error in BBTree: delete: tree should be empty now");
        }
        //check();
        return current_x;
    }

    public int traverse(int order, TraversalActionPerformer<? super A> p) {
        return traverseSelectedRange(order, p, null, null, null, false, null,
                                      false);
    }

    public int traverse(int order, TraversalActionPerformer<? super A> p,
                        Object o) {
        return traverseSelectedRange(order, p, o, null, null, false, null,
                                      false);
    }

    public int traverseSelected(int order, TraversalActionPerformer<? super A> p,
                                Selector<A> s) {
        return traverseSelectedRange(order, p, null, s, null, false, null,
                                      false);
    }

    public int traverseSelected(int order, TraversalActionPerformer<? super A> p,
                                Object o, Selector<A> s) {
        return traverseSelectedRange(order, p, o, s, null, false, null, false);
    }

    public int traverseRange(int order, TraversalActionPerformer<? super A> p,
                             A l, A r) {
        return traverseSelectedRange(order, p, null, null, l, true, r, false);
    }

    public int traverseRange(int order, TraversalActionPerformer<? super A> p,
                             Object o, A l, A r) {
        return traverseSelectedRange(order, p, o, null, l, true, r, false);
    }

    public int traverseRange(int order, TraversalActionPerformer<? super A> p,
                             A l, boolean left_included, A r,
                             boolean right_included) {
        return traverseSelectedRange(order, p, null, null, l, left_included, r,
                                      right_included);
    }

    public int traverseRange(int order, TraversalActionPerformer<? super A> p,
                             Object o,
                             A l, boolean left_included, A r,
                             boolean right_included) {
        return traverseSelectedRange(order, p, o, null, l, left_included, r,
                                      right_included);
    }

    public int traverseSelectedRange(int order,
                                     TraversalActionPerformer<? super A> p,
                                     Selector<A> s, A l, A r) {
        return traverseSelectedRange(order, p, null, s, l, true, r, false);
    }

    public int traverseSelectedRange(int order,
                                     TraversalActionPerformer<? super A> p,
                                     Object o, Selector<A> s, A l, A r) {
        return traverseSelectedRange(order, p, o, s, l, true, r, false);
    }

    public int traverseSelectedRange(int order,
                                     TraversalActionPerformer<? super A> p,
                                     Selector<A> s, A l, boolean left_included,
                                     A r, boolean right_included) {
        return traverseSelectedRange(order, p, null, s, l, left_included, r,
                                      right_included);
    }

    public synchronized int traverseSelectedRange(int order,
                                                  TraversalActionPerformer<? super A> p,
                                                  Object o, Selector<A> s, A l,
                                                  boolean left_included,
                                                  A r, boolean right_included) {
        if((p == null) || (root == null))
            return 0;
        A sl, sr;
        if(s != null) {
            sl = s.lowerBound();
            if((l != null) && ((sl == null) || (comp.compare(sl, l) < 0)))
                sl = l;
            else
                left_included = (sl == null) || s.selects(sl);
            sr = s.upperBound();
            if((r != null) && ((sr == null) || (comp.compare(sr, r) > 0)))
                sr = r;
            else
                right_included = (sr == null) || s.selects(sr);
        } else {
            sl = l;
            sr = r;
        }
        return ptraverse(root, order, p, o, s, sl, left_included, sr,
                          right_included);
    }

    public synchronized void clear() {
        pdestroy(root);
        root = null;
        count = 0;
    }

    public synchronized BBTreeS<A> clone() {
        BBTreeS<A> a = new BBTreeS<A>(this.comp != this ? this.comp : null);
        a.root = pclone(this.root);
        a.count = this.count;
        return a;
    }

    public synchronized Object[] toArray() {
        TraversalActionPerformer.ArrayBuilder<A> a =
                new TraversalActionPerformer.ArrayBuilder<A>(count);
        traverse(IN_ORDER, a);
        return a.toArray();
    }

    public synchronized <B> B[] toArray(B[] b) {
        TraversalActionPerformer.ArrayBuilder<A> a = null;
        if(b.length >= count)
            a = new TraversalActionPerformer.ArrayBuilder<A>(b);
        else
            a = new TraversalActionPerformer.ArrayBuilder<A>(
                    (B[])java.lang.reflect.Array.newInstance(
                    b.getClass().getComponentType(), count));
        traverse(IN_ORDER, a);
        return (B[])a.toArray();
    }

    public synchronized <B> B[] toArray(Class<B> componentType) {
        TraversalActionPerformer.ArrayBuilder<A> a = new TraversalActionPerformer.ArrayBuilder<A>(
                (B[])java.lang.reflect.Array.newInstance(
                componentType, count));
        traverse(IN_ORDER, a);
        return (B[])a.toArray();
    }

    public synchronized BBTreeS<A> reorder(Comparator<? super A> c,
                                           boolean make_new) {
        return reorder(c, null, make_new);
    }

    public synchronized BBTreeS<A> reorder(Comparator<? super A> c, Merger<A> m,
                                           boolean make_new) {
        if(make_new) {
            if((c == this) || ((c == null) && (comp == this)))
                return clone();
            BBTreeS<A> r = new BBTreeS<A>(c != this ? c : null);
            traverse(PRE_ORDER, new TraversalActionPerformer.OrderedSetAdder<A>(
                    r, m));
            return r;
        }
        if((c == this) || ((c == null) && (comp == this)))
            return this;
        Node<A> l = toLinkedList(), r;
        if(c != null)
            comp = c;
        else
            comp = this;
        count = 0;
        while(l != null) {
            r = l.right;
            addNode(l, m);
            l = r;
        }
        return this;
    }

    public synchronized BBTreeS<A> copySubSet(Selector<A> s) {
        if(s == null)
            return this.clone();
        BBTreeS<A> t = new BBTreeS<A>(comp != this ? comp : null);
        traverseSelected(PRE_ORDER,
                         new TraversalActionPerformer.CollectionAdder<A>(
                t, s, false), s);
        return t;
    }

    public BBTreeS<A> copySubSet(A fromElement, A toElement) {
        return copySubSet(fromElement, true, toElement, false);
    }

    public synchronized BBTreeS<A> copySubSet(A min, boolean min_included, A max,
                                              boolean max_included) {
        return copySubSet(new Selector.RangeSelector<A>(min, min_included, max,
                                                         max_included, comp));
    }

    public BBTreeS<A> copyHeadSet(A toElement) {
        return copyHeadSet(toElement, false);
    }

    public synchronized BBTreeS<A> copyHeadSet(A max, boolean max_included) {
        return copySubSet(
                new Selector.HalfRangeSelector<A>(max, max_included,
                                                  Selector.LEFT, comp));
    }

    public BBTreeS<A> copyTailSet(A fromElement) {
        return copyTailSet(fromElement, true);
    }

    public synchronized BBTreeS<A> copyTailSet(A min, boolean min_included) {
        return copySubSet(new Selector.HalfRangeSelector<A>(min, min_included,
                                                             Selector.RIGHT,
                                                             comp));
    }

    public synchronized BBTreeS<A> join(Collection<? extends A> c) {
        BBTreeS<A> r = this.clone();
        r.addAll(c);
        return r;
    }

    public synchronized BBTreeS<A> intersect(Collection<? super A> c) {
        BBTreeS<A> r = new BBTreeS<A>(comp != this ? comp : null);
        if(c == null)
            return r;
        traverse(IN_ORDER, new TraversalActionPerformer.CollectionAdder<A>(r,
                                                                           (c instanceof OrderedSet) ? (Selector<A>)c : new Selector.ContainmentChecker<A>(
                c), false));
        return r;
    }

    public synchronized BBTreeS<A> without(Collection<? super A> c) {
        if(c == null)
            return this.clone();
        BBTreeS<A> r = new BBTreeS<A>(comp != this ? comp : null);
        if(c == null)
            return r;
        traverse(IN_ORDER,
                 new TraversalActionPerformer.CollectionAdder<A>(r,
                                                                 new Selector.Inverter<A>(
                (c instanceof OrderedSet) ? (Selector<A>)c : new Selector.ContainmentChecker<A>(
                c)),
                                                                 false));
        return r;
    }

    public String toString() {
        return toStringBuilder(null).toString();
    }

    public StringBuilder toStringBuilder(StringBuilder sb) {
        TraversalActionPerformer.RepresentationStringBuilder<A> b =
                new TraversalActionPerformer.RepresentationStringBuilder<A>(sb);
        traverse(IN_ORDER, b);
        return b.getClosedStringBuilder();
    }

    public void writeToStream(Writer w) throws IOException {
        if(w == null)
            return;
        w.write('{');
        TraversalActionPerformer.RepresentationWriter<A> v =
                new TraversalActionPerformer.RepresentationWriter<A>(w);
        traverse(IN_ORDER, v);
        w.write('}');
    }

    public boolean subSetOf(Collection<?> s) {
        return isContainedIn(s);
    }

    public boolean superSetOf(Collection<?> s) {
        if(s == null)
            return true;
        return containsAll(s);
    }

    public synchronized int compareTo(OrderedSet<A> b) {
        return compareTo(b, comp);
    }

    public synchronized int compareTo(OrderedSet<A> b, Comparator<? super A> c) {
        if(b == null)
            return 1;
        if(this == b)
            return 0;
        if(b instanceof Final)
            return -b.compareTo(this);
        if(b instanceof Traversable) {
            TraversalActionPerformer.NavigableSetComparator<A> sc =
                    TraversalActionPerformer.NavigableSetComparator.
                    getThreadLocal(c);
            if(((Traversable<A>)b).traverse(IN_ORDER, sc, this) == 0)
                return first() == null ? 0 : 1;
            if((sc.c == 0) && (sc.last != null) && (higher(sc.last) != null))
                return 1;
            return -sc.c;
        } else {
            TraversalActionPerformer.NavigableSetComparator<A> sc =
                    TraversalActionPerformer.NavigableSetComparator.
                    getThreadLocal(c);
            if(traverse(IN_ORDER, sc, b) == 0)
                return b.first() == null ? 0 : -1;
            if((sc.c == 0) && (sc.last != null) && (b.higher(sc.last) != null))
                return -1;
            return sc.c;
        }
    }

    public synchronized boolean equals(Object o) {
        if(o == null)
            return false;
        if(o == this)
            return true;
        if(o instanceof OrderedSet)
            return compareTo((OrderedSet<A>)o) == 0;
        if(o instanceof Collection)
            return this.containsAll((Collection)o) && isContainedIn(
                    (Collection)o);
        else
            return false;
    }

    public int hashCode() {
        TraversalActionPerformer.Hasher<A> h =
                TraversalActionPerformer.Hasher.getThreadLocal();
        this.traverse(IN_ORDER, h);
        return h.getHash();
    }

    public boolean selects(A x) {
        return contains(x);
    }

    public A upperBound() {
        return last();
    }

    public A lowerBound() {
        return first();
    }

    public ResettableIterator<A> iterator() {
        return subSetIterator(null);
    }

    public ResettableIterator<A> descendingIterator() {
        return descendingSubSetIterator(null);
    }

    public ResettableIterator<A> subSetIterator(Selector<A> f) {
        return Thread.holdsLock(this)
                ? new ResettableIterator.NonLockingIterator<A>(this, f)
                : new ResettableIterator.LockingIterator<A>(this, f);
    }

    public ResettableIterator<A> descendingSubSetIterator(Selector<A> f) {
        return Thread.holdsLock(this)
                ? new ResettableIterator.NonLockingIterator<A>(this, f, true)
                : new ResettableIterator.LockingIterator<A>(this, f, true);
    }

    public ResettableIterator<A> subSetIterator(A fromElement, A toElement) {
        return subSetIterator(fromElement, true, toElement, false);
    }

    public ResettableIterator<A> descendingSubSetIterator(
            A fromElement, A toElement) {
        return descendingSubSetIterator(fromElement, true, toElement, false);
    }

    public ResettableIterator<A> subSetIterator(
            A min, boolean min_included, A max, boolean max_included) {
        return subSetIterator(new Selector.RangeSelector<A>(min, min_included,
                                                             max, max_included,
                                                             (SortedSet<A>)this));
    }

    public ResettableIterator<A> descendingSubSetIterator(
            A min, boolean min_included, A max, boolean max_included) {
        return subSetIterator(new Selector.RangeSelector<A>(
                min, min_included, max, max_included, (SortedSet<A>)this));
    }

    public ResettableIterator<A> headSetIterator(A toElement) {
        return headSetIterator(toElement, false);
    }

    public ResettableIterator<A> descendingHeadSetIterator(A toElement) {
        return descendingHeadSetIterator(toElement, false);
    }

    public ResettableIterator<A> headSetIterator(A max, boolean max_included) {
        return subSetIterator(new Selector.HalfRangeSelector<A>(
                max, max_included, Selector.LEFT, (SortedSet<A>)this));
    }

    public ResettableIterator<A> descendingHeadSetIterator(
            A max, boolean max_included) {
        return descendingSubSetIterator(new Selector.HalfRangeSelector<A>(
                max, max_included, Selector.LEFT, (SortedSet<A>)this));
    }

    public ResettableIterator<A> tailSetIterator(A fromElement) {
        return tailSetIterator(fromElement, true);
    }

    public ResettableIterator<A> descendingTailSetIterator(A fromElement) {
        return descendingTailSetIterator(fromElement, true);
    }

    public ResettableIterator<A> tailSetIterator(A min, boolean min_included) {
        return subSetIterator(new Selector.HalfRangeSelector<A>(
                min, min_included, Selector.RIGHT, (SortedSet<A>)this));
    }

    public ResettableIterator<A> descendingTailSetIterator(
            A min, boolean min_included) {
        return descendingSubSetIterator(new Selector.HalfRangeSelector<A>(
                min, min_included, Selector.RIGHT, (SortedSet<A>)this));
    }

    public Traversable.Set<A> descendingSet() {
        return descendingSubSet(null);
    }

    public Traversable.Set<A> subSet(Selector<A> f) {
        if(f == null)
            return this;
        return new SubSetStub(this, f);
    }

    public Traversable.Set<A> descendingSubSet(Selector<A> f) {
        return new SubSetStub(this, f, true);
    }

    public Traversable.Set<A> subSet(A fromElement, A toElement) {
        return subSet(fromElement, true, toElement, false);
    }

    public Traversable.Set<A> descendingSubSet(A fromElement, A toElement) {
        return descendingSubSet(fromElement, true, toElement, false);
    }

    public Traversable.Set<A> subSet(
            A min, boolean min_included, A max, boolean max_included) {
        return subSet(new Selector.RangeSelector<A>(
                min, min_included, max, max_included, (SortedSet<A>)this));
    }

    public Traversable.Set<A> descendingSubSet(
            A min, boolean min_included, A max, boolean max_included) {
        return descendingSubSet(new Selector.RangeSelector<A>(
                min, min_included, max, max_included, (SortedSet<A>)this));
    }

    public Traversable.Set<A> headSet(A toElement) {
        return headSet(toElement, false);
    }

    public Traversable.Set<A> descendingHeadSet(A toElement) {
        return descendingHeadSet(toElement, false);
    }

    public Traversable.Set<A> headSet(A max, boolean max_included) {
        return subSet(new Selector.HalfRangeSelector<A>(
                max, max_included, Selector.LEFT, (SortedSet<A>)this));
    }

    public Traversable.Set<A> descendingHeadSet(A max, boolean max_included) {
        return descendingSubSet(new Selector.HalfRangeSelector<A>(
                max, max_included, Selector.LEFT, (SortedSet<A>)this));
    }

    public Traversable.Set<A> tailSet(A fromElement) {
        return tailSet(fromElement, true);
    }

    public Traversable.Set<A> descendingTailSet(A fromElement) {
        return descendingTailSet(fromElement, true);
    }

    public Traversable.Set<A> tailSet(A min, boolean min_included) {
        return subSet(new Selector.HalfRangeSelector<A>(
                min, min_included, Selector.RIGHT, (SortedSet<A>)this));
    }

    public Traversable.Set<A> descendingTailSet(A min, boolean min_included) {
        return subSet(new Selector.HalfRangeSelector<A>(
                min, min_included, Selector.RIGHT, (SortedSet<A>)this));
    }

    public synchronized Final<A> toFinal() {
        return new Final(this);
    }

    public boolean isFinal() {
        return false;
    }

    public synchronized A pollFirst() {
        A y = first();
        remove(y);
        return y;
    }

    public synchronized A pollLast() {
        A y = last();
        remove(y);
        return y;
    }

    private Node<A> toLinkedList() {
        Node<A> r = null;
        if(root == null)
            return r;
        Node[] s = new Node[pmaxSearchDepth(root)];
        int[] cr = new int[s.length];
        int sp = -1, cc;
        Node<A> t = root, l = null;
        cc = FROM_TOP;
        while((sp >= 0) || (cc != DOWN_RIGHT))
            switch(cc) {
                case FROM_TOP: // pre-order
                    if(t.left != null) {
                        sp++;
                        s[sp] = t;
                        cr[sp] = DOWN_LEFT;
                        t = t.left;
                        cc = FROM_TOP;
                    } else
                        cc = DOWN_LEFT;
                    break;
                case DOWN_LEFT:  // in-order
                    r = t;
                    r.left = l;
                    if(l != null)
                        l.right = r;
                    l = r;
                    if(t.right != null) {
                        sp++;
                        s[sp] = t;
                        cr[sp] = DOWN_RIGHT;
                        t = t.right;
                        cc = FROM_TOP;
                    } else
                        cc = DOWN_RIGHT;
                    break;
                case DOWN_RIGHT: // post-order
                    t = s[sp];
                    cc = cr[sp];
                    sp--;
                    break;
                default:
                    break;
            }
        root = null;
        if(r != null)
            while(r.left != null)
                r = r.left;
        return r;
    }

    private int pdestroy(Node<A> t) {
        if(t == null)
            return 0;
        int hl = pdestroy(t.left), hr = pdestroy(t.right);
        t.val = null;
        t.left = null;
        t.right = null;
//        ccount++;
        return 1 + (hl < hr ? hr : hl);
    }

    private Node<A> pclone(Node<A> t) {
        if(t == null)
            return null;
        Node<A> r = new Node<A>(t.val);
        r.left = pclone(t.left);
        r.right = pclone(t.right);
        return r;
    }

    private int pmaxSearchDepth(Node<A> t) {
        if(t == null)
            return 0;
        if(t.balance >= 0)
            return pmaxSearchDepth(t.left) + t.balance + 1;
        else
            return pmaxSearchDepth(t.right) - t.balance + 1;
    }

    private int pcheck(Node<A> t) {
        if(t == null)
            return 0;
        if(t.val == null)
            System.out.println("Error in BBTree: check: null entry in node");
        int hl = pcheck(t.left);
        int hr = pcheck(t.right);
        if(hr - hl != t.balance)
            System.out.println(
                    "Error in BBTree: check: balance in node " + t.val + " incorrect: " + t.balance + ", should be " + (hr - hl));
        if((hr - hl < -1) || (hr - hl > 1))
            System.out.println(
                    "Error in BBTree: check: balance in node " + t.val + " out of bounds: " + (hr - hl));
        return 1 + (hl < hr ? hr : hl);
    }

    private int ptraverse(Node<A> t, int traverse_order,
                          TraversalActionPerformer traverse_action, Object o,
                          Selector<? super A> s, A l, boolean li,
                          A r, boolean ri) {
        if(t == null)
            return 0;
        int a = 1, h, c1 = l != null ? comp.compare(l, t.val) : -1,
                c2 = r != null ? comp.compare(r, t.val) : 1;
        boolean sel = s != null ? s.selects(t.val)
                                  && (li ? c1 <= 0 : c1 < 0) && (ri ? c2 >= 0 : c2 > 0)
                      : (li ? c1 <= 0 : c1 < 0) && (ri ? c2 >= 0 : c2 > 0);
        if(sel && ((traverse_order & PRE_ORDER) != 0))
            if(traverse_action.perform(t.val, PRE_ORDER, o))
                return -a;
        if((traverse_order & REVERSE_ORDER) == 0) {
            if(c1 < 0) {
                h = ptraverse(t.left, traverse_order, traverse_action, o, s, l,
                              li, r, ri);
                if(h < 0)
                    return h - a;
                a += h;
            }
            if(sel && ((traverse_order & IN_ORDER) != 0))
                if(traverse_action.perform(t.val, IN_ORDER, o))
                    return -a;
            if(c2 > 0) {
                h = ptraverse(t.right, traverse_order, traverse_action, o, s, l,
                              li, r, ri);
                if(h < 0)
                    return h - a;
                a += h;
            }
            if(sel && ((traverse_order & POST_ORDER) != 0))
                if(traverse_action.perform(t.val, POST_ORDER, o))
                    return -a;
        } else {
            if(c2 > 0) {
                h = ptraverse(t.right, traverse_order, traverse_action, o, s, l,
                              li, r, ri);
                if(h < 0)
                    return h - a;
                a += h;
            }
            if(sel && ((traverse_order & IN_ORDER) != 0))
                if(traverse_action.perform(t.val, IN_ORDER, o))
                    return -a;
            if(c1 < 0) {
                h = ptraverse(t.left, traverse_order, traverse_action, o, s, l,
                              li, r, ri);
                if(h < 0)
                    return h - a;
                a += h;
            }
            if(sel && ((traverse_order & POST_ORDER) != 0))
                if(traverse_action.perform(t.val, POST_ORDER, o))
                    return -a;
        }
        return a;
    }

    private boolean addNode(Node<A> x, Merger<A> m) {
        if((x == null) || (x.val == null))
            return false;
        x.left = null;
        x.right = null;
        x.balance = BALANCED;
        if(root == null) {
            root = x;
            count = 1;
            return true;
        }
        int old_count = count;
        paddNode(root, x, m);
        //check();
        assert count >= old_count;
        return count > old_count;
    }

    private boolean paddNode(Node<A> t, Node<A> x, Merger<A> m) {
        int c = comp.compare(x.val, t.val);
        if(c == 0) {
            if(m != null)
                t.val = m.merge(t.val, x.val);
            else
                x.val = t.val;
            return false;
        }
        if(c < 0)
            if(t.left != null)
                if(paddNode(t.left, x, m))
                    return t.rebalanceAfterLeftExcursion(true);
                else
                    return false;
            else {
                t.left = x;
                t.balance--;
                count++;
                return t.right == null;
            }
        else if(t.right != null)
            if(paddNode(t.right, x, m))
                return t.rebalanceAfterRightExcursion(true);
            else
                return false;
        else {
            t.right = x;
            t.balance++;
            count++;
            return t.left == null;
        }
    }

    private boolean padd(Node<A> t, Merger<A> m) {
        int c = comp.compare(current_x, t.val);
        if(c == 0) {
            if(m != null) {
                t.val = m.merge(t.val, current_x);
                current_x = t.val;
                if(t.val == null)
                    throw new NullPointerException("Merger returned null");
            } else
                current_x = t.val;
            return false;
        }
        if(c < 0)
            if(t.left != null)
                if(padd(t.left, m))
                    return t.rebalanceAfterLeftExcursion(true);
                else
                    return false;
            else {
                if(m != null)
                    m.merge(null, current_x);
                t.left = new Node<A>(current_x);
                t.balance--;
                count++;
                return t.right == null;
            }
        else if(t.right != null)
            if(padd(t.right, m))
                return t.rebalanceAfterRightExcursion(true);
            else
                return false;
        else {
            if(m != null)
                m.merge(null, current_x);
            t.right = new Node<A>(current_x);
            t.balance++;
            count++;
            return t.left == null;
        }
    }

    private <B> boolean paddOrGetMatch(Node<A> t, B x,
                                       KeyedElementFactory<A, B> f) {
        /*if(t==null)
        return(false);*/
        int c = -((AsymmetricComparator<A>)comp).asymmetricCompare(t.val,x);
        if(c == 0) {
            current_x = t.val;
            return false;
        }
        if(c < 0)
            if(t.left != null)
                if(paddOrGetMatch(t.left, x, f))
                    return t.rebalanceAfterLeftExcursion(true);
                else
                    return false;
            else {
                A y = f.createElement(x);
                if(y == null)
                    throw new NullPointerException(
                            "Element factory created null element");
                t.left = new Node<A>(y);
                t.balance--;
                count++;
                current_x = y;
                return t.right == null;
            }
        else if(t.right != null)
            if(paddOrGetMatch(t.right, x, f))
                return t.rebalanceAfterRightExcursion(true);
            else
                return false;
        else {
            A y = f.createElement(x);
            if(y == null)
                throw new NullPointerException(
                        "Element factory created null element");
            t.right = new Node<A>(y);
            t.balance++;
            count++;
            current_x = y;
            return t.left == null;
        }
    }

    private int premove(Node<A> t, A x) {
        if(t == null)
            return 0;
        int c = comp.compare(x, t.val);
        if(c <= 0) {
            if(c == 0) {
                current_x = t.val;
                if((t.right != null) && (t.left != null)) {
                    Node<A> r = t.left;
                    while(r.right != null)
                        r = r.right;
                    t.val = r.val;
                    r.val = current_x;
                } else {
                    count--;
                    if((t.left == null) && (t.right == null)) {
                        t.val = null;
                        return LEAF_DELETED;
                    } else if(t.left != null) {
                        t.val = t.left.val;
                        t.right = t.left.right;
                        t.left.val = null;
                        t.balance = t.left.balance;
                        t.left = t.left.left;
                        if((t.left != null) || (t.right != null))
                            throw new RuntimeException(
                                    "Left subtree expected to be leaf");
                    } else {
                        t.val = t.right.val;
                        t.left = t.right.left;
                        t.right.val = null;
                        t.balance = t.right.balance;
                        t.right = t.right.right;
                        if((t.left != null) || (t.right != null))
                            throw new RuntimeException(
                                    "Rigt subtree expected to be leaf");
                    }
                    return DEPTH_CHANGED;
                }
            }
            switch(premove(t.left, x)) {
                case LEAF_DELETED:
                    t.left = null;
                case DEPTH_CHANGED:
                    if(t.rebalanceAfterRightExcursion(false))
                        return DEPTH_CHANGED;
                case 0:
                    return 0;
                default:
                    throw new RuntimeException("Unknown return code");
            }
        } else
            switch(premove(t.right, x)) {
                case LEAF_DELETED:
                    t.right = null;
                case DEPTH_CHANGED:
                    if(t.rebalanceAfterLeftExcursion(false))
                        return DEPTH_CHANGED;
                case 0:
                    return 0;
                default:
                    throw new RuntimeException("Unknown return code");
            }
    }

    private int premoveComparable(Node<A> t, Comparable<? super A> x) {
        if(t == null)
            return 0;
        int c = x.compareTo(t.val);
        if(c <= 0) {
            if(c == 0) {
                current_x = t.val;
                if((t.right != null) && (t.left != null)) {
                    Node<A> r = t.left;
                    while(r.right != null)
                        r = r.right;
                    t.val = r.val;
                    r.val = current_x;
                } else {
                    count--;
                    if((t.left == null) && (t.right == null)) {
                        t.val = null;
                        return LEAF_DELETED;
                    } else if(t.left != null) {
                        t.val = t.left.val;
                        t.right = t.left.right;
                        t.left.val = null;
                        t.balance = t.left.balance;
                        t.left = t.left.left;
                        if((t.left != null) || (t.right != null))
                            throw new RuntimeException(
                                    "Left subtree expected to be leaf");
                    } else {
                        t.val = t.right.val;
                        t.left = t.right.left;
                        t.right.val = null;
                        t.balance = t.right.balance;
                        t.right = t.right.right;
                        if((t.left != null) || (t.right != null))
                            throw new RuntimeException(
                                    "Rigt subtree expected to be leaf");
                    }
                    return DEPTH_CHANGED;
                }
            }
            switch(premoveComparable(t.left, x)) {
                case LEAF_DELETED:
                    t.left = null;
                case DEPTH_CHANGED:
                    if(t.rebalanceAfterRightExcursion(false))
                        return DEPTH_CHANGED;
                case 0:
                    return 0;
                default:
                    throw new RuntimeException("Unknown return code");
            }
        } else
            switch(premoveComparable(t.right, x)) {
                case LEAF_DELETED:
                    t.right = null;
                case DEPTH_CHANGED:
                    if(t.rebalanceAfterLeftExcursion(false))
                        return DEPTH_CHANGED;
                case 0:
                    return 0;
                default:
                    throw new RuntimeException("Unknown return code");
            }
    }

    private <B> int premoveMatch(Node<A> t, B x) {
        if(t == null)
            return 0;
        int c = ((AsymmetricComparator<A>)comp).asymmetricCompare(t.val,x);
        if(c >= 0) {
            if(c == 0) {
                current_x = t.val;
                if((t.right != null) && (t.left != null)) {
                    Node<A> r = t.left;
                    while(r.right != null)
                        r = r.right;
                    t.val = r.val;
                    r.val = current_x;
                } else {
                    count--;
                    if((t.left == null) && (t.right == null)) {
                        t.val = null;
                        return LEAF_DELETED;
                    } else if(t.left != null) {
                        t.val = t.left.val;
                        t.right = t.left.right;
                        t.left.val = null;
                        t.balance = t.left.balance;
                        t.left = t.left.left;
                        if((t.left != null) || (t.right != null))
                            throw new RuntimeException(
                                    "Left subtree expected to be leaf");
                    } else {
                        t.val = t.right.val;
                        t.left = t.right.left;
                        t.right.val = null;
                        t.balance = t.right.balance;
                        t.right = t.right.right;
                        if((t.left != null) || (t.right != null))
                            throw new RuntimeException(
                                    "Rigt subtree expected to be leaf");
                    }
                    return DEPTH_CHANGED;
                }
            }
            switch(premoveMatch(t.left, x)) {
                case LEAF_DELETED:
                    t.left = null;
                case DEPTH_CHANGED:
                    if(t.rebalanceAfterRightExcursion(false))
                        return DEPTH_CHANGED;
                case 0:
                    return 0;
                default:
                    throw new RuntimeException("Unknown return code");
            }
        } else
            switch(premoveMatch(t.right, x)) {
                case LEAF_DELETED:
                    t.right = null;
                case DEPTH_CHANGED:
                    if(t.rebalanceAfterLeftExcursion(false))
                        return DEPTH_CHANGED;
                case 0:
                    return 0;
                default:
                    throw new RuntimeException("Unknown return code");
            }
    }

    private static class Node<A> {

        A val;
        Node<A> left = null, right = null;
        int balance = BALANCED;

        /** Creates a new instance of Node */
        Node(A val) {
            this.val = val;
        }

        Node(A val, Node<A> left, Node<A> right) {
            this.val = val;
            this.left = left;
            this.right = right;
        }

        public A getValue() {
            return val;
        }

        private boolean rebalanceAfterRightExcursion(boolean elementAdded) {
            switch(balance) {
                case BALANCED:
                    balance = RIGHT_HIGH;
                    return elementAdded;
                case LEFT_HIGH:
                    balance = BALANCED;
                    return !elementAdded;
                case RIGHT_HIGH:
                    Node<A> r = right;
                    A temp = val;
                    switch(r.balance) {
                        case BALANCED:
                            val = r.val;
                            r.val = temp;
                            right = r.right;
                            r.right = r.left;
                            r.left = left;
                            left = r;
                            r.balance = RIGHT_HIGH;
                            balance = LEFT_HIGH;
                            return elementAdded;
                        case LEFT_HIGH:
                            Node<A> rl = r.left;
                            switch(rl.balance) {
                                case BALANCED:
                                    val = rl.val;
                                    rl.val = temp;
                                    r.left = rl.right;
                                    rl.right = rl.left;
                                    rl.left = left;
                                    left = rl;
                                    left.balance = BALANCED;
                                    right.balance = BALANCED;
                                    balance = BALANCED;
                                    return !elementAdded;
                                case LEFT_HIGH:
                                    val = rl.val;
                                    rl.val = temp;
                                    r.left = rl.right;
                                    rl.right = rl.left;
                                    rl.left = left;
                                    left = rl;
                                    left.balance = BALANCED;
                                    right.balance = RIGHT_HIGH;
                                    balance = BALANCED;
                                    return !elementAdded;
                                case RIGHT_HIGH:
                                    val = rl.val;
                                    rl.val = temp;
                                    r.left = rl.right;
                                    rl.right = rl.left;
                                    rl.left = left;
                                    left = rl;
                                    left.balance = LEFT_HIGH;
                                    right.balance = BALANCED;
                                    balance = BALANCED;
                                    return !elementAdded;
                                default:
                                    throw new RuntimeException(
                                            "Unbalanced child node discovered");
                            }
                        case RIGHT_HIGH:
                            val = r.val;
                            r.val = temp;
                            right = r.right;
                            r.right = r.left;
                            r.left = left;
                            left = r;
                            r.balance = BALANCED;
                            balance = BALANCED;
                            return !elementAdded;
                        default:
                            throw new RuntimeException(
                                    "Unbalanced child node discovered");
                    }
                default:
                    throw new RuntimeException("Unbalanced node discovered");
            }
        }

        private boolean rebalanceAfterLeftExcursion(boolean elementAdded) {
            switch(balance) {
                case BALANCED:
                    balance = LEFT_HIGH;
                    return elementAdded;
                case RIGHT_HIGH:
                    balance = BALANCED;
                    return !elementAdded;
                case LEFT_HIGH:
                    Node<A> l = left;
                    A temp = val;
                    switch(l.balance) {
                        case BALANCED:
                            val = l.val;
                            l.val = temp;
                            left = l.left;
                            l.left = l.right;
                            l.right = right;
                            right = l;
                            l.balance = LEFT_HIGH;
                            balance = RIGHT_HIGH;
                            return elementAdded;
                        case LEFT_HIGH:
                            val = l.val;
                            l.val = temp;
                            left = l.left;
                            l.left = l.right;
                            l.right = right;
                            right = l;
                            l.balance = BALANCED;
                            balance = BALANCED;
                            return !elementAdded;
                        case RIGHT_HIGH:
                            Node<A> lr = l.right;
                            switch(lr.balance) {
                                case BALANCED:
                                    val = lr.val;
                                    lr.val = temp;
                                    l.right = lr.left;
                                    lr.left = lr.right;
                                    lr.right = right;
                                    right = lr;
                                    left.balance = BALANCED;
                                    right.balance = BALANCED;
                                    balance = BALANCED;
                                    return !elementAdded;
                                case LEFT_HIGH:
                                    val = lr.val;
                                    lr.val = temp;
                                    l.right = lr.left;
                                    lr.left = lr.right;
                                    lr.right = right;
                                    right = lr;
                                    left.balance = BALANCED;
                                    right.balance = RIGHT_HIGH;
                                    balance = BALANCED;
                                    return !elementAdded;
                                case RIGHT_HIGH:
                                    val = lr.val;
                                    lr.val = temp;
                                    l.right = lr.left;
                                    lr.left = lr.right;
                                    lr.right = right;
                                    right = lr;
                                    left.balance = LEFT_HIGH;
                                    right.balance = BALANCED;
                                    balance = BALANCED;
                                    return !elementAdded;
                                default:
                                    throw new RuntimeException(
                                            "Unbalanced child node discovered");
                            }
                        default:
                            throw new RuntimeException(
                                    "Unbalanced child node discovered");
                    }
                default:
                    throw new RuntimeException("Unbalanced node discovered");
            }
        }
    }

    /**/
    private static class SubSetStub<A> implements Traversable.Set<A> {

        private final BBTreeS<A> s;
        private final Selector<A> f;
        private final boolean reverseOrder;
        private static final long serialVersionUID = 0;

        public SubSetStub(BBTreeS<A> s) {
            this(s, null, false);
        }

        public SubSetStub(BBTreeS<A> s, Selector<A> f) {
            this(s, f, false);
        }

        public SubSetStub(BBTreeS<A> s, boolean reverseOrder) {
            this(s, null, reverseOrder);
        }

        public SubSetStub(BBTreeS<A> s, Selector<A> f, boolean reverseOrder) {
            if(s == null)
                throw new NullPointerException("Null set");
            this.s = s;
            if(f != null)
                this.f = f;
            else
                this.f = Selector.Accepter;
            this.reverseOrder = reverseOrder;
        }

        public boolean contains(Object o) {
            return f.selects((A)o) && s.contains(o);
        }

        public boolean containsAll(Collection<?> c) {
            return s.containsAll(c, f);
        }

        public boolean containsAll(Collection<?> c, Selector<?> g) {
            return s.containsAll(c, new Selector.Conjunctor<Object>(
                    (Selector<Object>)g, (Selector<Object>)f));
        }

        public boolean add(A x, Merger<A> m) {
            if(!f.selects(x))
                throw new IllegalArgumentException(
                        "Element not permitted in subset");
            return s.add(x, m);
        }

        public boolean add(A x) {
            return add(x, null);
        }

        public boolean addAll(Collection<? extends A> c) {
            return s.addAll(c, f, true, null);
        }

        public boolean addAll(Collection<? extends A> c, Merger<A> m) {
            return s.addAll(c, f, true, m);
        }

        public boolean addAll(Collection<? extends A> c, Selector<? super A> s,
                              boolean abort_on_illegal_argument) {
            return addAll(c, s, abort_on_illegal_argument, null);
        }

        public boolean addAll(Collection<? extends A> c, Selector<? super A> s,
                              boolean abort_on_illegal_argument, Merger<A> m) {
            if(s == null)
                return this.s.addAll(c, f, true, m);
            boolean changed = false;
            for(A x : c)
                if(s.selects(x))
                    changed |= add(x, m);
                else if(abort_on_illegal_argument)
                    throw new IllegalArgumentException(
                            "argument not selected by selector");
            return changed;
        }

        public A addOrGet(A x) {
            if(!f.selects(x))
                throw new IllegalArgumentException(
                        "Element not permitted in subset");
            return s.addOrGet(x);
        }

        public <B> A addOrGetMatch(B x, KeyedElementFactory<A, B> ef) {
            if(ef == null)
                throw new NullPointerException("Element factory is null");
            A y = ef.createElement(x);
            if(y == null)
                throw new NullPointerException(
                        "Element factory created null element");
            if(!f.selects(y))
                throw new IllegalArgumentException(
                        "Element not permitted in subset");
            return s.addOrGet(y);
        }

        public boolean remove(Object o) {
            if((o == null) || !f.selects((A)o))
                return false;
            return s.remove(o);
        }

        public boolean removeAll(Collection<?> c) {
            if(c == null)
                return false;
            return s.removeAll((Collection<A>)c, f);
        }

        public boolean removeAll(Selector<? super A> t) {
            if(t == null)
                return false;
            return s.removeAll(new Selector.Conjunctor<A>(f, (Selector<A>)t));
        }

        public boolean retainAll(Collection<?> c) {
            if(c == null)
                return false;
            else
                return retainAll(new Selector.Disjunctor<A>(f,
                                                             (c instanceof OrderedSet) ? (Selector<A>)c : new Selector.ContainmentChecker<A>(
                        (Collection<Object>)c)));
        }

        public boolean retainAll(Selector<? super A> t) {
            if(t == null)
                return false;
            return s.retainAll(new Selector.Disjunctor<A>(
                    new Selector.Inverter<A>(f), (Selector<A>)t));
        }

        public A removeComparable(Comparable<? super A> x) {
            if(x == null)
                return null;
            synchronized(s) {
                A y = s.getComparable(x);
                if((y != null) && f.selects(y))
                    y = s.removeComparable(x);
                else
                    y = null;
                return y;
            }
        }

        public <B> A removeMatch(B x) {
            if(x == null)
                return null;
            synchronized(s) {
                A y = s.getMatch(x);
                if((y != null) && f.selects(y))
                    y = s.removeMatch(x);
                else
                    y = null;
                return y;
            }
        }

        public A pollFirst() {
            synchronized(s) {
                A y = first();
                remove(y);
                return y;
            }
        }

        public A pollLast() {
            synchronized(s) {
                A y = last();
                remove(y);
                return y;
            }
        }

        public boolean isEmpty() {
            return size() == 0;
        }

        public void clear() {
            s.removeAll(f);
        }

        public int size() {
            TraversalActionPerformer.Counter<A> c =
                    TraversalActionPerformer.Counter.getThreadLocal();
            s.traverseSelected(IN_ORDER, c, f);
            return c.getCounter();
        }

        public int maxSearchDepth() {
            return s.maxSearchDepth();
        }

        public A getComparable(Comparable<? super A> x) {
            if(x == null)
                return null;
            A y = s.getComparable(x);
            if((y == null) || !f.selects(y))
                return null;
            return y;
        }

        public <B> A getMatch(B x) {
            if(x == null)
                return null;
            A y = s.getMatch(x);
            if((y == null) || !f.selects(y))
                return null;
            return y;
        }

        public A get(A x) {
            if(x == null)
                return null;
            A y = s.get(x);
            if((y == null) || !f.selects(y))
                return null;
            return y;
        }

        public A set(A x) {
            if((x == null) || !f.selects(x))
                return null;
            return s.set(x);
        }

        private A psearchNext(A x) {
//            while((x!=null)&&!f.selects(x))
//                x = s.higher(x);
//            return(x);
            if((x != null) && f.selects(x))
                return x;
            return psearchNext(s.root, x);
        }

        private A psearchNext(Node<A> t, A x) {
            if(t == null)
                return null;
            int c = x != null ? s.comp.compare(x, t.val) : -1;
            if(c < 0) {
                A y = psearchNext(t.left, x);
                if(y != null)
                    return y;
            }
            if((c <= 0) && f.selects(t.val))
                return t.val;
            return psearchNext(t.right, x);
        }

        private A psearchPrev(A x) {
//            while((x!=null)&&!f.selects(x))
//                x = s.lower(x);
//            return(x);
            if((x != null) && f.selects(x))
                return x;
            return psearchPrev(s.root, x);
        }

        private A psearchPrev(Node<A> t, A x) {
            if(t == null)
                return null;
            int c = x != null ? s.comp.compare(x, t.val) : 1;
            if(c > 0) {
                A y = psearchPrev(t.right, x);
                if(y != null)
                    return y;
            }
            if((c >= 0) && f.selects(t.val))
                return t.val;
            return psearchPrev(t.left, x);
        }

        public A first() {
            synchronized(s) {
                return reverseOrder ? psearchPrev(f.upperBound()) : psearchNext(f.
                        lowerBound());
            }
        }

        public A last() {
            synchronized(s) {
                return reverseOrder ? psearchNext(f.lowerBound()) : psearchPrev(f.
                        upperBound());
            }
        }

        public A higher(A x) {
            if(x == null)
                return reverseOrder ? last() : first();
            synchronized(s) {
                return reverseOrder ? psearchPrev(s.lower(x))
                        : psearchNext(s.higher(x));
            }
        }

        public A higherComparable(Comparable<? super A> x) {
            if(x == null)
                return reverseOrder ? last() : first();
            synchronized(s) {
                return reverseOrder ? psearchPrev(s.lowerComparable(x))
                        : psearchNext(s.higherComparable(x));
            }
        }

        public <B> A higherMatch(B x) {
            if(x == null)
                return reverseOrder ? last() : first();
            synchronized(s) {
                return reverseOrder ? psearchPrev(s.lowerMatch(x))
                        : psearchNext(s.higherMatch(x));
            }
        }

        public A lower(A x) {
            if(x == null)
                return reverseOrder ? first() : last();
            synchronized(s) {
                return reverseOrder ? psearchNext(s.higher(x))
                        : psearchPrev(s.lower(x));
            }
        }

        public A lowerComparable(Comparable<? super A> x) {
            if(x == null)
                return reverseOrder ? first() : last();
            synchronized(s) {
                return reverseOrder ? psearchNext(s.higherComparable(x))
                        : psearchPrev(s.lowerComparable(x));
            }
        }

        public <B> A lowerMatch(B x) {
            if(x == null)
                return reverseOrder ? first() : last();
            synchronized(s) {
                return reverseOrder ? psearchNext(s.higherMatch(x))
                        : psearchPrev(s.lowerMatch(x));
            }
        }

        public A floor(A x) {
            if(x == null)
                return reverseOrder ? first() : last();
            synchronized(s) {
                return reverseOrder ? psearchNext(s.ceiling(x))
                        : psearchPrev(s.floor(x));
            }
        }

        public A floorComparable(Comparable<? super A> x) {
            if(x == null)
                return reverseOrder ? first() : last();
            synchronized(s) {
                return reverseOrder ? psearchNext(s.ceilComparable(x))
                        : psearchPrev(s.floorComparable(x));
            }
        }

        public <B> A floorMatch(B x) {
            if(x == null)
                return reverseOrder ? first() : last();
            synchronized(s) {
                return reverseOrder ? psearchNext(s.ceilMatch(x))
                        : psearchPrev(s.floorMatch(x));
            }
        }

        public A ceiling(A x) {
            if(x == null)
                return reverseOrder ? last() : first();
            synchronized(s) {
                return reverseOrder ? psearchPrev(s.floor(x))
                        : psearchNext(s.ceiling(x));
            }
        }

        public A ceilComparable(Comparable<? super A> x) {
            if(x == null)
                return reverseOrder ? last() : first();
            synchronized(s) {
                return reverseOrder ? psearchPrev(s.floorComparable(x))
                        : psearchNext(s.ceilComparable(x));
            }
        }

        public <B> A ceilMatch(B x) {
            if(x == null)
                return reverseOrder ? last() : first();
            synchronized(s) {
                return reverseOrder ? psearchPrev(s.floorMatch(x))
                        : psearchNext(s.ceilMatch(x));
            }
        }

        public boolean isContainedIn(Collection<?> c) {
            if(c == null)
                return isEmpty();
            if(c == this)
                return true;
            TraversalActionPerformer.LogicalJunctor<A> t =
                    new TraversalActionPerformer.LogicalJunctor<A>(
                    TraversalActionPerformer.LOGICAL_AND,
                    (c instanceof OrderedSet ? (Selector<A>)c : new Selector.ContainmentChecker<A>(
                    c)));
            s.traverseSelected(IN_ORDER, t, f);
            return t.getState();
        }

        public boolean subSetOf(Collection<?> c) {
            return isContainedIn(c);
        }

        public boolean superSetOf(Collection<?> c) {
            return s.containsAll(c, f);
        }

        public BBTree<A> join(Collection<? extends A> c) {
            BBTree<A> r = this.clone();
            r.addAll(c);
            return r;
        }

        public BBTree<A> intersect(Collection<? super A> c) {
            synchronized(s) {
                BBTree<A> r = new BBTree(s.comp != s ? s.comp : null,
                                         reverseOrder);
                r.addAll(s, new Selector.Conjunctor(f,
                                                    (c instanceof OrderedSet) ? (OrderedSet<A>)c
                                                    : new Selector.ContainmentChecker(
                        c)), false, null);
                return r;
            }
        }

        public BBTree<A> without(Collection<? super A> c) {
            synchronized(s) {
                BBTree<A> r = new BBTree(s.comp != s ? s.comp : null,
                                         reverseOrder);
                r.addAll(s, new Selector.Conjunctor(f,
                                                    new Selector.Inverter<A>((c instanceof OrderedSet) ? (OrderedSet<A>)c
                                                                             : new Selector.ContainmentChecker(
                        c))), false, null);
                return r;
            }
        }

        public Traversable.Set<A> reorder(Comparator<? super A> c,
                                          boolean make_new) {
            return reorder(c, null, make_new);
        }

        public Traversable.Set<A> reorder(Comparator<? super A> c, Merger<A> m,
                                          boolean make_new) {
            if(make_new)
                synchronized(s) {
                    BBTree<A> r = c == s
                                  ? new BBTree<A>((Comparator<A>)null) : c == this
                                                                         ? new BBTree<A>(
                            (Comparator<A>)null, reverseOrder) : new BBTree<A>(c);
                    r.addAll(s, f, false, m);
                    return r;
                }
            return s.reorder(c, m, false);
        }

        public Object[] toArray() {
            TraversalActionPerformer.ArrayBuilder<A> a =
                    new TraversalActionPerformer.ArrayBuilder<A>(size());
            s.traverseSelected(IN_ORDER, a, f);
            return a.toArray();
        }

        public <B> B[] toArray(B[] b) {
            int count = size();
            TraversalActionPerformer.ArrayBuilder<A> a = null;
            if(b.length >= count)
                a = new TraversalActionPerformer.ArrayBuilder<A>(b);
            else
                a = new TraversalActionPerformer.ArrayBuilder<A>(
                        (B[])java.lang.reflect.Array.newInstance(
                        b.getClass().getComponentType(), count));
            s.traverseSelected(IN_ORDER, a, f);
            return (B[])a.toArray();
        }

        public <B> B[] toArray(Class<B> componentType) {
            int count = size();
            TraversalActionPerformer.ArrayBuilder<A> a = new TraversalActionPerformer.ArrayBuilder<A>(
                    (B[])java.lang.reflect.Array.newInstance(
                    componentType, count));
            s.traverseSelected(IN_ORDER, a, f);
            return (B[])a.toArray();
        }

        public boolean isFinal() {
            return s.isFinal();
        }

        public Final<A> toFinal() {
            synchronized(s) {
                return new Final(this);
            }
        }

        public Traversable.Set<A> copySubSet(Selector<A> g) {
            synchronized(s) {
                BBTree<A> r = new BBTree(s.comp != s ? s.comp : null,
                                         reverseOrder);
                r.addAll(this, g, false);
                return r;
            }
        }

        public Traversable.Set<A> copySubSet(A fromElement, A toElement) {
            return copySubSet(fromElement, true, toElement, false);
        }

        public Traversable.Set<A> copySubSet(A min, boolean min_included, A max,
                                             boolean max_included) {
            synchronized(s) {
                return copySubSet(new RangeSelector<A>(
                        min, min_included, max, max_included, s.comparator()));
            }
        }

        public Traversable.Set<A> copyHeadSet(A toElement) {
            return copyHeadSet(toElement, false);
        }

        public Traversable.Set<A> copyHeadSet(A max, boolean max_included) {
            synchronized(s) {
                return copySubSet(new Selector.HalfRangeSelector<A>(
                        max, max_included, Selector.LEFT, s.comparator()));
            }
        }

        public Traversable.Set<A> copyTailSet(A fromElement) {
            return copyTailSet(fromElement, true);
        }

        public Traversable.Set<A> copyTailSet(A min, boolean min_included) {
            synchronized(s) {
                return copySubSet(new Selector.HalfRangeSelector<A>(
                        min, min_included, Selector.RIGHT, s.comparator()));
            }
        }

        public Traversable.Set<A> descendingSet() {
            return descendingSubSet(null);
        }

        public Traversable.Set<A> subSet(Selector<A> g) {
            if(g != null)
                return new SubSetStub<A>(s,
                                          f == Selector.Accepter
                                          ? reverseOrder ? new Selector.Reverser<A>(
                        g) : g
                                          : new Selector.Conjunctor<A>(f,
                                                                       reverseOrder
                                                                       ? new Selector.Reverser<A>(
                        g) : g), reverseOrder);
            else
                return this;
        }

        public Traversable.Set<A> descendingSubSet(Selector<A> g) {
            if(g != null)
                return new SubSetStub<A>(s,
                                          f == Selector.Accepter
                                          ? reverseOrder ? g : new Selector.Reverser<A>(
                        g)
                                          : new Selector.Conjunctor<A>(f,
                                                                       reverseOrder
                                                                       ? g : new Selector.Reverser<A>(
                        g)),
                                          !reverseOrder);
            else
                return new SubSetStub<A>(s, f, !reverseOrder);
        }

        public Traversable.Set<A> subSet(A fromElement, A toElement) {
            return subSet(fromElement, true, toElement, false);
        }

        public Traversable.Set<A> descendingSubSet(A fromElement, A toElement) {
            return descendingSubSet(fromElement, true, toElement, false);
        }

        public Traversable.Set<A> subSet(A min, boolean min_included, A max,
                                         boolean max_included) {
            synchronized(s) {
                return subSet(new Selector.RangeSelector<A>(
                        min, min_included, max, max_included, s.comparator()));
            }
        }

        public Traversable.Set<A> descendingSubSet(A min, boolean min_included,
                                                   A max, boolean max_included) {
            synchronized(s) {
                return descendingSubSet(new Selector.RangeSelector<A>(
                        min, min_included, max, max_included, s.comparator()));
            }
        }

        public Traversable.Set<A> headSet(A toElement) {
            return headSet(toElement, false);
        }

        public Traversable.Set<A> descendingHeadSet(A toElement) {
            return descendingHeadSet(toElement, false);
        }

        public Traversable.Set<A> headSet(A max, boolean max_included) {
            synchronized(s) {
                return subSet(new Selector.HalfRangeSelector<A>(
                        max, max_included, Selector.LEFT, s.comparator()));
            }
        }

        public Traversable.Set<A> descendingHeadSet(A max, boolean max_included) {
            synchronized(s) {
                return descendingSubSet(new Selector.HalfRangeSelector<A>(
                        max, max_included, Selector.LEFT, s.comparator()));
            }
        }

        public Traversable.Set<A> tailSet(A min) {
            return tailSet(min, true);
        }

        public Traversable.Set<A> descendingTailSet(A min) {
            return descendingTailSet(min, true);
        }

        public Traversable.Set<A> tailSet(A min, boolean min_included) {
            synchronized(s) {
                return subSet(new Selector.HalfRangeSelector<A>(
                        min, min_included, Selector.RIGHT, s.comparator()));
            }
        }

        public Traversable.Set<A> descendingTailSet(A min, boolean min_included) {
            synchronized(s) {
                return descendingSubSet(new Selector.HalfRangeSelector<A>(
                        min, min_included, Selector.RIGHT, s.comparator()));
            }
        }

        public ResettableIterator<A> iterator() {
            return subSetIterator(null);
        }

        public ResettableIterator<A> descendingIterator() {
            return descendingSubSetIterator(null);
        }

        public ResettableIterator<A> subSetIterator(Selector<A> g) {
            if(g != null)
                return reverseOrder
                        ? s.descendingSubSetIterator(
                        f == Selector.Accepter ? new Selector.Reverser<A>(g)
                        : new Selector.Conjunctor<A>(f,
                                                     new Selector.Reverser<A>(g)))
                        : s.subSetIterator(
                        f == Selector.Accepter ? g
                        : new Selector.Conjunctor<A>(f, g));
            else
                return reverseOrder
                        ? s.descendingSubSetIterator(f) : s.subSetIterator(f);
        }

        public ResettableIterator<A> descendingSubSetIterator(Selector<A> g) {
            if(g != null)
                return reverseOrder
                        ? s.subSetIterator(
                        f == Selector.Accepter ? g
                        : new Selector.Conjunctor<A>(f, g))
                        : s.descendingSubSetIterator(
                        f == Selector.Accepter ? new Selector.Reverser<A>(g)
                        : new Selector.Conjunctor<A>(f,
                                                     new Selector.Reverser<A>(g)));
            else
                return reverseOrder
                        ? s.subSetIterator(f) : s.descendingSubSetIterator(f);
        }

        public ResettableIterator<A> subSetIterator(A fromElement, A toElement) {
            return subSetIterator(fromElement, true, toElement, false);
        }

        public ResettableIterator<A> descendingSubSetIterator(A fromElement,
                                                              A toElement) {
            return descendingSubSetIterator(fromElement, true, toElement, false);
        }

        public ResettableIterator<A> subSetIterator(A min, boolean min_included,
                                                    A max, boolean max_included) {
            return subSetIterator(new Selector.RangeSelector<A>(
                    min, min_included, max, max_included, s.comparator()));
        }

        public ResettableIterator<A> descendingSubSetIterator(A min,
                                                              boolean min_included,
                                                              A max,
                                                              boolean max_included) {
            return descendingSubSetIterator(new Selector.RangeSelector<A>(
                    min, min_included, max, max_included, s.comparator()));
        }

        public ResettableIterator<A> headSetIterator(A toElement) {
            return headSetIterator(toElement, false);
        }

        public ResettableIterator<A> descendingHeadSetIterator(A toElement) {
            return descendingHeadSetIterator(toElement, false);
        }

        public ResettableIterator<A> headSetIterator(A max, boolean max_included) {
            return subSetIterator(new Selector.HalfRangeSelector<A>(
                    max, max_included, Selector.LEFT, s.comparator()));
        }

        public ResettableIterator<A> descendingHeadSetIterator(A max,
                                                               boolean max_included) {
            return descendingSubSetIterator(new Selector.HalfRangeSelector<A>(
                    max, max_included, Selector.LEFT, s.comparator()));
        }

        public ResettableIterator<A> tailSetIterator(A fromElement) {
            return tailSetIterator(fromElement, true);
        }

        public ResettableIterator<A> descendingTailSetIterator(A fromElement) {
            return descendingTailSetIterator(fromElement, true);
        }

        public ResettableIterator<A> tailSetIterator(A min, boolean min_included) {
            return subSetIterator(new Selector.HalfRangeSelector<A>(
                    min, min_included, Selector.RIGHT, s.comparator()));
        }

        public ResettableIterator<A> descendingTailSetIterator(A min,
                                                               boolean min_included) {
            return descendingSubSetIterator(new Selector.HalfRangeSelector<A>(
                    min, min_included, Selector.RIGHT, s.comparator()));
        }

        public BBTree<A> clone() {
            BBTree<A> r = new BBTree(s.comparator() != s ? s.comparator() : null,
                                     reverseOrder);
            r.addAll(s, f, false, null);
            return r;
        }

        public boolean equals(Object o) {
            if(o == null)
                return false;
            if(o == this)
                return true;
            if(o instanceof OrderedSet)
                return compareTo((OrderedSet<A>)o) == 0;
            if(o instanceof Collection)
                synchronized(s) {
                    return this.containsAll((Collection)o) && isContainedIn(
                            (Collection)o);
                }
            else
                return false;
        }

        public int compareTo(OrderedSet<A> b) {
            synchronized(s) {
                return compareTo(b, s.comp);
            }
        }

        public int compareTo(OrderedSet<A> b, Comparator<? super A> c) {
            if(b == null)
                return 1;
            if(this == b)
                return 0;
            if(b instanceof Final)
                return -b.compareTo(this);
            synchronized(s) {
                if(b instanceof Traversable) {
                    TraversalActionPerformer.NavigableSetComparator<A> sc =
                            TraversalActionPerformer.NavigableSetComparator.
                            getThreadLocal(c);
                    if(((Traversable<A>)b).traverse(IN_ORDER, sc, this) == 0)
                        return first() == null ? 0 : 1;
                    if((sc.c == 0) && (sc.last != null) && (higher(sc.last) != null))
                        return 1;
                    return -sc.c;
                } else {
                    TraversalActionPerformer.NavigableSetComparator<A> sc =
                            TraversalActionPerformer.NavigableSetComparator.
                            getThreadLocal(c);
                    if(traverse(IN_ORDER, sc, b) == 0)
                        return b.first() == null ? 0 : -1;
                    if((sc.c == 0) && (sc.last != null) && (b.higher(sc.last) != null))
                        return -1;
                    return sc.c;
                }
            }
        }

        public int hashCode() {
            TraversalActionPerformer.Hasher<A> h =
                    TraversalActionPerformer.Hasher.getThreadLocal();
            s.traverseSelected(IN_ORDER, h, f);
            return h.getHash();
        }

        public String toString() {
            return toStringBuilder(null).toString();
        }

        public StringBuilder toStringBuilder(StringBuilder sb) {
            TraversalActionPerformer.RepresentationStringBuilder<A> b =
                    new TraversalActionPerformer.RepresentationStringBuilder<A>(
                    sb);
            s.traverseSelected(IN_ORDER, b, f);
            return b.getClosedStringBuilder();
        }

        public void writeToStream(Writer w) throws IOException {
            if(w == null)
                return;
            w.write('{');
            TraversalActionPerformer.RepresentationWriter<A> v =
                    new TraversalActionPerformer.RepresentationWriter<A>(w);
            s.traverseSelected(IN_ORDER, v, f);
            w.write('}');
        }

        public int compare(A x, A y) {
            return reverseOrder ? -s.compare(x, y) : s.compare(x, y);
        }
        public <B> int asymmetricCompare(A x, B y) {
            return reverseOrder ? -s.asymmetricCompare(x, y) : s.asymmetricCompare(x, y);
        }

        public Comparator<? super A> comparator() {
            if(reverseOrder)
                return Utils.reverseComparator(s.comparator());
            else
                return s.comparator();
        }

        public boolean selects(A x) {
            return f.selects(x) && s.selects(x);
        }

        public A upperBound() {
            return last();
        }

        public A lowerBound() {
            return first();
        }

        public int traverse(int order, TraversalActionPerformer<? super A> p) {
            return traverseSelectedRange(order, p, null, null, null, false,
                                          null, false);
        }

        public int traverse(int order, TraversalActionPerformer<? super A> p,
                            Object o) {
            return traverseSelectedRange(order, p, o, null, null, false, null,
                                          false);
        }

        public int traverseSelected(int order,
                                    TraversalActionPerformer<? super A> p,
                                    Selector<A> s) {
            return traverseSelectedRange(order, p, null, s, null, false, null,
                                          false);
        }

        public int traverseSelected(int order,
                                    TraversalActionPerformer<? super A> p,
                                    Object o, Selector<A> s) {
            return traverseSelectedRange(order, p, o, s, null, false, null,
                                          false);
        }

        public int traverseRange(int order,
                                 TraversalActionPerformer<? super A> p,
                                 A l, A r) {
            return traverseSelectedRange(order, p, null, null, l, true, r,
                                          false);
        }

        public int traverseRange(int order,
                                 TraversalActionPerformer<? super A> p,
                                 Object o, A l, A r) {
            return traverseSelectedRange(order, p, o, null, l, true, r, false);
        }

        public int traverseRange(int order,
                                 TraversalActionPerformer<? super A> p,
                                 A l, boolean left_included, A r,
                                 boolean right_included) {
            return traverseSelectedRange(order, p, null, null, l, left_included,
                                          r, right_included);
        }

        public int traverseRange(int order,
                                 TraversalActionPerformer<? super A> p, Object o,
                                 A l, boolean left_included, A r,
                                 boolean right_included) {
            return traverseSelectedRange(order, p, o, null, l, left_included, r,
                                          right_included);
        }

        public int traverseSelectedRange(int order,
                                         TraversalActionPerformer<? super A> p,
                                         Selector<A> s, A l, A r) {
            return traverseSelectedRange(order, p, null, s, l, true, r, false);
        }

        public int traverseSelectedRange(int order,
                                         TraversalActionPerformer<? super A> p,
                                         Object o, Selector<A> s, A l, A r) {
            return traverseSelectedRange(order, p, o, s, l, true, r, false);
        }

        public int traverseSelectedRange(int order,
                                         TraversalActionPerformer<? super A> p,
                                         Selector<A> s, A l,
                                         boolean left_included,
                                         A r, boolean right_included) {
            return traverseSelectedRange(order, p, null, s, l, left_included, r,
                                          right_included);
        }

        public int traverseSelectedRange(int order,
                                         TraversalActionPerformer<? super A> p,
                                         Object o, Selector<A> s, A l,
                                         boolean left_included,
                                         A r, boolean right_included) {
            if(reverseOrder)
                if(f != Selector.Accepter)
                    return this.s.traverseSelectedRange(
                            order ^ REVERSE_ORDER, p,
                            new Selector.Conjunctor<A>(f,
                                                       new Selector.Reverser<A>(
                            s)),
                            r, right_included, l, left_included);
                else
                    return this.s.traverseSelectedRange(
                            order ^ REVERSE_ORDER, p,
                            new Selector.Reverser<A>(s),
                            r, right_included, l, left_included);
            else if(f != Selector.Accepter)
                return this.s.traverseSelectedRange(
                        order, p, new Selector.Conjunctor<A>(f, s),
                        l, left_included, r, right_included);
            else
                return this.s.traverseSelectedRange(
                        order, p, s, l, left_included, r, right_included);
        }

        public int emptyTo(TraversalActionPerformer<? super A> p) {
            return emptyTo(p, null);
        }

        public int emptyTo(TraversalActionPerformer<? super A> p, Object o) {
            if(p == null)
                throw new NullPointerException("Performer is null");
            synchronized(s) {
                if(s.root == null)
                    return 0;
                int h = s.traverseSelected(IN_ORDER, p, o, f);
                clear();
                return h;
            }
        }
    }/**/


    public int emptyTo(TraversalActionPerformer<? super A> p) {
        return emptyTo(p, null);
    }

    public synchronized int emptyTo(TraversalActionPerformer<? super A> p,
                                    Object o) {
        if(p == null)
            throw new NullPointerException("Performer is null");
        if(root == null)
            return 0;
        int h = ptraverse(root, IN_ORDER, p, o, null, null, false, null, false);
        clear();
        return h;
    }

    private void readObject(java.io.ObjectInputStream s)
            throws java.io.IOException, ClassNotFoundException {
        s.defaultReadObject();
        this.root = null;
        int remain = this.count;
        while(remain-- > 0)
            add((A)s.readObject());
    }

    private synchronized void writeObject(java.io.ObjectOutputStream s)
            throws java.io.IOException, ClassNotFoundException {
        s.defaultWriteObject();
        traverse(IN_ORDER, TraversalActionPerformer.SERIALIZER, s);
    }
    private static final long serialVersionUID = 0;
}
