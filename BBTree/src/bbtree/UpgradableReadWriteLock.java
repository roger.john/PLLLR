/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package bbtree;

/**
 *
 * @author RJ
 */
public class UpgradableReadWriteLock {

    private static class Int {
        public volatile int val = 0;
    }

    private final Int cr        = new Int();
    private final Int cw        = new Int();
    private volatile int ww     = 0;
    private volatile Thread cwt = null;

    public UpgradableReadWriteLock() {}
    public void registerReader() {
        synchronized(cw) {
            if((cw.val>0)&&(Thread.currentThread()==cwt))
                throw(new RuntimeException(getClass().getCanonicalName()
                    +".registerReader: current thread "+Thread.currentThread()
                    +" already has write-lock"));
            while((cw.val>0)||(ww>0)) {
                try { cw.wait(); }
                catch(InterruptedException e) {
                    System.out.println("\r\nThe thread "
                            +Thread.currentThread().getName()+" was interrupted:");
                    e.printStackTrace(System.out);
                }
            }
            synchronized(cr) {
                cr.val++;
                if(cr.val<0)
                    throw(new RuntimeException(getClass().getCanonicalName()
                            +".registerReader: overflow occured, two many readers registered"));
            }
        }
    }
    public void unregisterReader() {
        synchronized(cr) {
            if(cr.val<=0)
                throw(new RuntimeException(getClass().getCanonicalName()
                        +".unregisterReader: reader registrations out of sync "
                        +"(caused by thread "+Thread.currentThread().getName()+")"));
            cr.val--;
            if(cr.val==0)
                cr.notifyAll();
        }
    }
    public void writeLock() {
        synchronized(cw) {
            if((cw.val>0)&&(Thread.currentThread()==cwt))
                throw(new RuntimeException(getClass().getCanonicalName()
                    +".writeLock: current thread "+Thread.currentThread()
                    +" already has write-lock"));
            if(cw.val>0) {
                ww++;
                if(ww<0)
                    throw(new RuntimeException(getClass().getCanonicalName()
                            +".writeLock: overflow occured, too many writers waiting"));
                while(cw.val>0) {
                    try { cw.wait(); }
                    catch(InterruptedException e) {
                        System.out.println("\r\nThe thread "
                                +Thread.currentThread().getName()+" was interrupted:");
                        e.printStackTrace(System.out);
                    }
                }
                ww--;
                if(ww<0)
                    throw(new RuntimeException(getClass().getCanonicalName()
                            +".writeLock: number of waiting writers is out of sync"));
            }
            cw.val = 1;
            cwt = Thread.currentThread();
        }
        synchronized(cr) {
            while(cr.val>0) {
                try { cr.wait(); }
                catch(InterruptedException e) {
                    System.out.println("\r\nThe thread "
                            +Thread.currentThread().getName()+" was interrupted:");
                    e.printStackTrace(System.out);
                }
            }
        }
    }
    public void writeUnlock() {
        synchronized(cw) {
            if((cw.val==0)||(Thread.currentThread()!=cwt))
                throw(new RuntimeException(getClass().getCanonicalName()
                        +".writeUnlock: the current thread "
                        +Thread.currentThread().getName()
                        +" does not own the write lock"));
            cw.val = 0;
            cwt = null;
            cw.notifyAll();
        }
    }
    public boolean hasWriteLock() {
        synchronized(cw) {
            return((cw.val>0)&&(Thread.currentThread()==cwt));
        }
    }
    public boolean isWriteLocked() {
        synchronized(cw) {
            return(cw.val>0);
        }
    }
    public boolean upgradeLock() {       //assumes that current thread
        boolean success = false;        //registered for read exactly once
        synchronized(cw) {
            if((cw.val>0)&&(cwt==Thread.currentThread()))
                throw(new RuntimeException(getClass().getCanonicalName()
                    +".upgradeLock: current thread "+Thread.currentThread().getName()
                    +" already has write-lock, reentracy not supported"));
            else if(cw.val==0) {
                cw.val = 1;
                cwt = Thread.currentThread();
                success = true;
            }
        }
        if(success)
            synchronized(cr) {
                if(cr.val<=0)
                    throw(new RuntimeException(getClass().getCanonicalName()
                        +".upgradeLock: cr is "+cr.val+", should be >0"));
                cr.val--;
                while(cr.val>0) {
                    try { cr.wait(); }
                    catch(InterruptedException e) {
                        System.out.println("\r\nThe thread "
                                +Thread.currentThread().getName()+" was interrupted:");
                        e.printStackTrace(System.out);
                    }
                }
            }
        return(success);
    }
    public void downgradeLock() {                           //releases all write locks
        synchronized(cw) {                                  //and immediately registers
            if((cw.val==0)||(Thread.currentThread()!=cwt))  //as reader
                throw(new RuntimeException(getClass().getCanonicalName()
                        +".writeUnlock: the current thread "
                        +Thread.currentThread().getName()
                        +" does not own the write lock"));
            cw.val = 0;
            cwt = null;
            synchronized(cr) {
                if(cr.val!=0)
                    throw(new RuntimeException(getClass().getCanonicalName()
                        +".downgradeLock: cr is "+cr.val+", should be 0"));
                cr.val++;
            }
            cw.notifyAll();
        }
    }
    public int writeLockCount() {
        synchronized(cw) {
            return(cw.val);
        }
    }
    public int writersWating() {
        synchronized(cw) {
            return(ww);
        }
    }
    public int readLockCount() {
        synchronized(cr) {
            return(cr.val);
        }
    }
    public boolean isReadLocked() {
        synchronized(cr) {
            return(cr.val>0);
        }
    }
}
