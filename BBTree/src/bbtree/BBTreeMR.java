/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bbtree;

import bbtree.Utils.AsymmetricComparator;
import bbtree.Utils.KeyedElementFactory;
import java.util.*;
import java.io.*;

/**
 *
 * @author RJ
 */
public class BBTreeMR<A> implements Traversable.Set<A> {

    private static class Int {

        public volatile int val = 0;
    }
    private final Int cr = new Int();
    private final Int cw = new Int();
    private transient volatile int ww = 0;
    private transient volatile Thread cwt = null;

    private void registerReader() {
        synchronized(cw) {
            if((cw.val > 0) && (Thread.currentThread() == cwt))
                throw new RuntimeException(getClass().getCanonicalName()
                                            + ".registerReader: current thread " + Thread.
                        currentThread()
                                            + " already has write-lock");
            while((cw.val > 0) || (ww > 0))
                try {
                    cw.wait();
                } catch(InterruptedException e) {
                    System.out.println(
                            "\r\nThe thread "
                            + Thread.currentThread().getName() + " was interrupted:");
                    e.printStackTrace(System.out);
                }
            synchronized(cr) {
                cr.val++;
                if(cr.val < 0)
                    throw new RuntimeException(
                            "overflow occured, two many readers registered");
            }
        }
    }

    private void unregisterReader() {
        synchronized(cr) {
            if(cr.val <= 0)
                throw new RuntimeException("reader registrations out of sync "
                                            + "(caused by thread " + Thread.
                        currentThread().getName() + ")");
            cr.val--;
            if(cr.val == 0)
                cr.notifyAll();
        }
    }

    private void writeLock() {
        synchronized(cw) {
            if((cw.val > 0) && (Thread.currentThread() == cwt))
                throw new RuntimeException(getClass().getCanonicalName()
                                            + ".writeLock: current thread " + Thread.
                        currentThread()
                                            + " already has write-lock");
            if(cw.val > 0) {
                ww++;
                if(ww < 0)
                    throw new RuntimeException(
                            "overflow occured, too many writers waiting");
                while(cw.val > 0)
                    try {
                        cw.wait();
                    } catch(InterruptedException e) {
                        System.out.println(
                                "\r\nThe thread "
                                + Thread.currentThread().getName() + " was interrupted:");
                        e.printStackTrace(System.out);
                    }
                ww--;
                if(ww < 0)
                    throw new RuntimeException(
                            "number of waiting writers is out of sync");
            }
            cw.val = 1;
            cwt = Thread.currentThread();
        }
        synchronized(cr) {
            while(cr.val > 0)
                try {
                    cr.wait();
                } catch(InterruptedException e) {
                    System.out.println(
                            "\r\nThe thread "
                            + Thread.currentThread().getName() + " was interrupted:");
                    e.printStackTrace(System.out);
                }
        }
    }

    private void writeUnlock() {
        synchronized(cw) {
            if((cw.val == 0) || (Thread.currentThread() != cwt))
                throw new RuntimeException("the current thread "
                                            + Thread.currentThread().getName()
                                            + " does not own the write lock");
            cw.val = 0;
            cwt = null;
            cw.notifyAll();
        }
    }

    private boolean hasWriteLock() {
        synchronized(cw) {
            return (cw.val > 0) && (Thread.currentThread() == cwt);
        }
    }

    public boolean isWriteLocked() {
        synchronized(cw) {
            return cw.val > 0;
        }
    }

    private boolean upgradeLock() {       //assumes that current thread
        boolean success = false;        //registered for read exactly once
        synchronized(cw) {
            if((cw.val > 0) && (cwt == Thread.currentThread()))
                throw new RuntimeException(
                        getClass().getCanonicalName()
                        + ".upgradeLock: current thread " + Thread.currentThread().
                        getName()
                        + " already has write-lock, reentracy not supported");
            else if(cw.val == 0) {
                cw.val = 1;
                cwt = Thread.currentThread();
                success = true;
            }
        }
        if(success)
            synchronized(cr) {
                if(cr.val <= 0)
                    throw new RuntimeException(
                            "cr is " + cr.val + ", should be >0");
                cr.val--;
                while(cr.val > 0)
                    try {
                        cr.wait();
                    } catch(InterruptedException e) {
                        System.out.println(
                                "\r\nThe thread "
                                + Thread.currentThread().getName() + " was interrupted:");
                        e.printStackTrace(System.out);
                    }
            }
        return success;
    }

    private void downgradeLock() {                           //releases all write locks
        synchronized(cw) {                                  //and immediately registers
            if((cw.val == 0) || (Thread.currentThread() != cwt))  //as reader
                throw new RuntimeException("Thread does not own the write lock");
            cw.val = 0;
            cwt = null;
            synchronized(cr) {
                if(cr.val != 0)
                    throw new RuntimeException(
                            "cr is " + cr.val + ", should be 0");
                cr.val++;
            }
            cw.notifyAll();
        }
    }
    //private constants for balancing
    private static final int BALANCED = 0;
    private static final int LEFT_HIGH = -1;
    private static final int RIGHT_HIGH = 1;
    //private constants for traversing
    private static final int FROM_TOP = 0;
    private static final int DOWN_LEFT = 1;
    private static final int DOWN_RIGHT = 2;
    //private masks for bit fields
    private static final int NO_CHANGE = 0;
    private static final int DEPTH_CHANGED = 1;
    private static final int LEAF_DELETED = 2;
    private static final int NEED_LOCK = 3;
    //vars that describe a binary BALANCED tree
    private transient Node<A> root = null;
    private Comparator<? super A> comp = null;
    private int count = 0;
    private static final ThreadLocal<Merger.Getter> getter =
            new ThreadLocal<Merger.Getter>() {

                protected Merger.Getter initialValue() {
                    return new Merger.Getter();
                }
            };
    private static final ThreadLocal<Merger.Setter> setter =
            new ThreadLocal<Merger.Setter>() {

                protected Merger.Setter initialValue() {
                    return new Merger.Setter();
                }
            };

    /**
     * Creates a new (empty) instance of BBTree with natural ordering.
     */
    public BBTreeMR() {
        this((Comparator<? super A>)null, false);
    }

    public BBTreeMR(boolean reverseOrder) {
        this((Comparator<? super A>)null, reverseOrder);
    }

    /**
     * Creates a new (empty) instance of BBTree with the specified comparator for ordering.
     * If the given comparator is null, natural ordering is used.
     * @param c the comparator which shall be used for ordering the elements of this tree.
     * If it is null, natural ordering is used.
     */
    public BBTreeMR(Comparator<? super A> c) {
        this(c, false);
    }

    public BBTreeMR(Comparator<? super A> c, boolean reverseOrder) {
        this.comp = Utils.wrapComparator(c, reverseOrder);
    }

    public BBTreeMR(Collection<? extends A> c) {
        this(c, false);
    }

    public BBTreeMR(Collection<? extends A> c, boolean reverseOrder) {
        this((Comparator<? super A>)null, reverseOrder);
        addAll(c);
    }

    public BBTreeMR(SortedSet<? extends A> c) {
        this(c, false);
    }

    public BBTreeMR(SortedSet<? extends A> s, boolean reverseOrder) {
        this((Comparator<? super A>)s.comparator(), reverseOrder);
        addAll(s);
    }

    public int compare(A x, A y) {
        return comp.compare(x,y);
    }
    public <B> int asymmetricCompare(A x, B y) {
        return ((AsymmetricComparator<A>)comp).asymmetricCompare(x,y);
    }

    public Comparator<? super A> comparator() {
        Comparator<? super A> r = comp;
        return r != Utils.NATURAL_COMPARATOR ? r : null;
    }

    public boolean isEmpty() {
        return root == null;
    }

    public int size() {
        return count;
    }

    public int maxSearchDepth() {
        registerReader();
        int r = pmaxSearchDepth(root);
        unregisterReader();
        return r;
    }

    public boolean contains(Object o) {
        return o == null ? false : get((A)o) != null;
    }

    public boolean containsAll(Collection<?> c) {
        return containsAll(c, null, true);
    }

    public boolean containsAll(Collection<?> c, Selector<?> s) {
        return containsAll(c, s, true);
    }

    private boolean containsAll(Collection<?> c, Selector<?> s, boolean lock) {
        if((c == null) || (c == this))
            return true;
        if(lock)
            registerReader();
        if(c instanceof Traversable) {
            TraversalActionPerformer.LogicalJunctor<Object> a =
                    new TraversalActionPerformer.LogicalJunctor<Object>(
                    TraversalActionPerformer.LOGICAL_AND,
                                                                        new Selector.Conjunctor(
                    (Selector<Object>)this, (Selector<Object>)s));
            try {
                ((Traversable<Object>)c).traverse(IN_ORDER, a);
            } finally {
                if(lock)
                    unregisterReader();
            }
            return a.getState();
        }
        if(s != null) {
            for(Object o : c)
                if(!((Selector<Object>)s).selects(o) || !contains(o)) {
                    if(lock)
                        unregisterReader();
                    return false;
                }
        } else
            for(Object o : c)
                if(!contains(o)) {
                    if(lock)
                        unregisterReader();
                    return false;
                }
        if(lock)
            unregisterReader();
        return true;
    }

    public boolean isContainedIn(Collection<?> c) {
        return isContainedIn(c, true);
    }

    private boolean isContainedIn(Collection<?> c, boolean lock) {
        if(c == null)
            return false;
        if(c == this)
            return true;
        if(lock)
            registerReader();
        TraversalActionPerformer.LogicalJunctor<A> a =
                new TraversalActionPerformer.LogicalJunctor<A>(
                TraversalActionPerformer.LOGICAL_AND,
                (c instanceof OrderedSet) ? (Selector<A>)c
                : new Selector.ContainmentChecker<A>(c));
        try {
            ptraverse(root, IN_ORDER, a, null, null, null, true, null, true);
        } finally {
            if(lock)
                unregisterReader();
        }
        return a.getState();
    }

    public A getComparable(Comparable<? super A> x) {
        if(x == null)
            return null;
        registerReader();
        Node<A> t = root;
        int c;
        while(t != null) {
            c = x.compareTo(t.val);
            if(c == 0) {
                A r = t.val;
                unregisterReader();
                return r;
            } else if(c < 0)
                t = t.left;
            else
                t = t.right;
        }
        unregisterReader();
        return null;
    }

    public <B> A getMatch(B x) {
        if(x == null)
            return null;
        registerReader();
        Node<A> t = root;
        int c;
        while(t != null) {
            c = -((AsymmetricComparator<A>)comp).asymmetricCompare(t.val,x);
            if(c == 0) {
                A r = t.val;
                unregisterReader();
                return r;
            } else if(c < 0)
                t = t.left;
            else
                t = t.right;
        }
        unregisterReader();
        return null;
    }

    public A get(A x) {
        if(x == null)
            return null;
        registerReader();
        Node<A> t = root;
        int c;
        while(t != null) {
            c = comp.compare(x, t.val);
            if(c == 0) {
                A r = t.val;
                unregisterReader();
                return r;
            } else if(c < 0)
                t = t.left;
            else
                t = t.right;
        }
        unregisterReader();
        return null;
    }

    public A set(A x) {
        if(x == null)
            return null;
        Merger.Setter<A> m = setter.get();
        add(x, m);
        return m.old;
    }

    public A first() {
        if(root == null)
            return null;
        registerReader();
        if(root == null) {
            unregisterReader();
            return null;
        }
        Node<A> t = root;
        while(t.left != null)
            t = t.left;
        A y = t.val;
        unregisterReader();
        return y;
    }

    private A pfirst() {
        if(root == null)
            return null;
        if(root == null)
            return null;
        Node<A> t = root;
        while(t.left != null)
            t = t.left;
        return t.val;
    }

    public A last() {
        if(root == null)
            return null;
        registerReader();
        if(root == null) {
            unregisterReader();
            return null;
        }
        Node<A> t = root;
        while(t.right != null)
            t = t.right;
        A y = t.val;
        unregisterReader();
        return y;
    }

    private A plast() {
        if(root == null)
            return null;
        if(root == null)
            return null;
        Node<A> t = root;
        while(t.right != null)
            t = t.right;
        return t.val;
    }

    public A ceiling(A x) {
        if(x == null)
            return first();
        if(root == null)
            return null;
        registerReader();
        Node<A> t = root;
        A y = null;
        int c;
        while(t != null) {
            c = comp.compare(x, t.val);
            if(c == 0) {
                y = t.val;
                unregisterReader();
                return y;
            }
            if(c > 0)
                t = t.right;
            else {
                y = t.val;
                t = t.left;
            }
        }
        unregisterReader();
        return y;
    }

    public A ceilComparable(Comparable<? super A> x) {
        if(x == null)
            return first();
        if(root == null)
            return null;
        registerReader();
        Node<A> t = root;
        A y = null;
        int c;
        while(t != null) {
            c = x.compareTo(t.val);
            if(c == 0) {
                y = t.val;
                unregisterReader();
                return y;
            }
            if(c > 0)
                t = t.right;
            else {
                y = t.val;
                t = t.left;
            }
        }
        unregisterReader();
        return y;
    }

    public <B> A ceilMatch(B x) {
        if(x == null)
            return first();
        if(root == null)
            return null;
        registerReader();
        Node<A> t = root;
        A y = null;
        int c;
        while(t != null) {
            c = ((AsymmetricComparator<A>)comp).asymmetricCompare(t.val,x);
            if(c == 0) {
                y = t.val;
                unregisterReader();
                return y;
            }
            if(c < 0)
                t = t.right;
            else {
                y = t.val;
                t = t.left;
            }
        }
        unregisterReader();
        return y;
    }

    public A floor(A x) {
        if(x == null)
            return last();
        if(root == null)
            return null;
        registerReader();
        Node<A> t = root;
        A y = null;
        int c;
        while(t != null) {
            c = comp.compare(x, t.val);
            if(c == 0) {
                y = t.val;
                unregisterReader();
                return y;
            }
            if(c > 0) {
                y = t.val;
                t = t.right;
            } else
                t = t.left;
        }
        unregisterReader();
        return y;
    }

    public A floorComparable(Comparable<? super A> x) {
        if(x == null)
            return last();
        if(root == null)
            return null;
        registerReader();
        Node<A> t = root;
        A y = null;
        int c;
        while(t != null) {
            c = x.compareTo(t.val);
            if(c == 0) {
                y = t.val;
                unregisterReader();
                return y;
            }
            if(c > 0) {
                y = t.val;
                t = t.right;
            } else
                t = t.left;
        }
        unregisterReader();
        return y;
    }

    public <B> A floorMatch(B x) {
        if(x == null)
            return last();
        if(root == null)
            return null;
        registerReader();
        Node<A> t = root;
        A y = null;
        int c;
        while(t != null) {
            c = ((AsymmetricComparator<A>)comp).asymmetricCompare(t.val,x);
            if(c == 0) {
                y = t.val;
                unregisterReader();
                return y;
            }
            if(c < 0) {
                y = t.val;
                t = t.right;
            } else
                t = t.left;
        }
        unregisterReader();
        return y;
    }

    public A higher(A x) {
        if(x == null)
            return first();
        if(root == null)
            return null;
        registerReader();
        Node<A> t = root;
        A y = null;
        while(t != null)
            if(comp.compare(x, t.val) < 0) {
                y = t.val;
                t = t.left;
            } else
                t = t.right;
        unregisterReader();
        return y;
    }

    private A phigher(A x) {
        if(x == null)
            return pfirst();
        if(root == null)
            return null;
        Node<A> t = root;
        A y = null;
        while(t != null)
            if(comp.compare(x, t.val) < 0) {
                y = t.val;
                t = t.left;
            } else
                t = t.right;
        return y;
    }

    public A higherComparable(Comparable<? super A> x) {
        if(x == null)
            return first();
        if(root == null)
            return null;
        registerReader();
        Node<A> t = root;
        A y = null;
        while(t != null)
            if(x.compareTo(t.val) < 0) {
                y = t.val;
                t = t.left;
            } else
                t = t.right;
        unregisterReader();
        return y;
    }

    public <B> A higherMatch(B x) {
        if(x == null)
            return first();
        if(root == null)
            return null;
        registerReader();
        Node<A> t = root;
        A y = null;
        while(t != null)
            if(((AsymmetricComparator<A>)comp).asymmetricCompare(t.val,x) > 0) {
                y = t.val;
                t = t.left;
            } else
                t = t.right;
        unregisterReader();
        return y;
    }

    public A lower(A x) {
        if(x == null)
            return last();
        if(root == null)
            return null;
        registerReader();
        Node<A> t = root;
        A y = null;
        while(t != null)
            if(comp.compare(x, t.val) > 0) {
                y = t.val;
                t = t.right;
            } else
                t = t.left;
        unregisterReader();
        return y;
    }

    private A plower(A x) {
        if(x == null)
            return last();
        if(root == null)
            return null;
        Node<A> t = root;
        A y = null;
        while(t != null)
            if(comp.compare(x, t.val) > 0) {
                y = t.val;
                t = t.right;
            } else
                t = t.left;
        return y;
    }

    public A lowerComparable(Comparable<? super A> x) {
        if(x == null)
            return last();
        if(root == null)
            return null;
        registerReader();
        Node<A> t = root;
        A y = null;
        while(t != null)
            if(x.compareTo(t.val) > 0) {
                y = t.val;
                t = t.right;
            } else
                t = t.left;
        unregisterReader();
        return y;
    }

    public <B> A lowerMatch(B x) {
        if(x == null)
            return last();
        if(root == null)
            return null;
        registerReader();
        Node<A> t = root;
        A y = null;
        while(t != null)
            if(((AsymmetricComparator<A>)comp).asymmetricCompare(t.val,x) < 0) {
                y = t.val;
                t = t.right;
            } else
                t = t.left;
        unregisterReader();
        return y;
    }

    public boolean add(A x) {
        return add(x, Merger.Keeper);
    }

    public boolean addAll(Collection<? extends A> c) {
        return addAll(c, null, false, Merger.Keeper);
    }

    public A addOrGet(A x) {
        Merger.Getter<A> m = getter.get();
        add(x, m);
        return m.result;
    }

    public <B> A addOrGetMatch(B x, KeyedElementFactory<A, B> f) {
        int c;
        if(f == null)
            throw new NullPointerException("Element factory is null");
        f.reset();
        if(hasWriteLock()) {
            if(root == null) {
                A y = f.createElement(x);
                if(y == null)
                    throw new NullPointerException(
                            "Element factory created null element");
                root = new Node<A>(y);
                count = 1;
                return y;
            }
            c = count;
            Merger.Getter<A> m = getter.get();
            paddOrGetMatch(root, x, f, m);
            c = count - c;
            assert c >= 0;
            return m.result;
        }
        registerReader();
        c = count;
        if(root == null) {
            if(!upgradeLock()) {
                unregisterReader();
                writeLock();
                if(root != null) {
                    c = count;
                    Merger.Getter<A> m = getter.get();
                    paddOrGetMatch(root, x, f, m);
                    c = count - c;
                    writeUnlock();
                    assert c >= 0;
                    return m.result;
                }
            }
            A y = f.createElement(x);
            if(y == null) {
                writeUnlock();
                throw new NullPointerException(
                        "Element factory created null element");
            }
            root = new Node<A>(y);
            count = 1;
            writeUnlock();
            return y;
        }
        Merger.Getter<A> m = getter.get();
        if(paddOrGetMatch(root, x, f, m) == NEED_LOCK) {
            unregisterReader();
            writeLock();
            if(root == null) {
                A y = f.createElement(x);
                if(y == null) {
                    writeUnlock();
                    throw new NullPointerException(
                            "Element factory created null element");
                }
                root = new Node<A>(y);
                count = 1;
                writeUnlock();
                return y;
            }
            c = count;
            paddOrGetMatch(root, x, f, m);
        }
        c = count - c;
        if(hasWriteLock())
            writeUnlock();
        else
            unregisterReader();
        assert c >= 0;
        return m.result;
    }

    public boolean add(A x, Merger<A> m) {
        if(x == null)
            return false;
        int c;
        if(hasWriteLock()) {
            if(root == null) {
                if(m != null)
                    m.merge(null, x);
                root = new Node<A>(x);
                count = 1;
                return true;
            }
            c = count;
            padd(root, x, m);
            c = count - c;
            assert c >= 0;
            return c > 0;
        }
        registerReader();
        c = count;
        if(root == null) {
            if(!upgradeLock()) {
                unregisterReader();
                writeLock();
                if(root != null) {
                    c = count;
                    padd(root, x, m);
                    c = count - c;
                    writeUnlock();
                    assert c >= 0;
                    return c > 0;
                }
            }
            if(m != null)
                m.merge(null, x);
            root = new Node<A>(x);
            count = 1;
            writeUnlock();
            return true;
        }
        if(padd(root, x, m) == NEED_LOCK) {
            unregisterReader();
            writeLock();
            if(root == null) {
                if(m != null)
                    m.merge(null, x);
                root = new Node<A>(x);
                count = 1;
                writeUnlock();
                return true;
            }
            c = count;
            padd(root, x, m);
        }
        c = count - c;
        if(hasWriteLock())
            writeUnlock();
        else
            unregisterReader();
        assert c >= 0;
        return c > 0;
    }

    public boolean addAll(Collection<? extends A> c, Merger<A> m) {
        return addAll(c, null, false, m);
    }

    public boolean addAll(Collection<? extends A> c, Selector<? super A> s,
                          boolean abort_on_illegal_argument) {
        return addAll(c, s, abort_on_illegal_argument, Merger.Keeper);
    }

    public boolean addAll(Collection<? extends A> c, Selector<? super A> s,
                          boolean abort_on_illegal_argument, Merger<A> m) {
        if((c == null) || (c == this))
            return false;
        writeLock();
        int old_count = count;
        if(c instanceof Traversable)
            try {
                ((Traversable<A>)c).traverse(IN_ORDER,
                                             new TraversalActionPerformer.OrderedSetAdder<A>(
                        this, s,
                                                                                             abort_on_illegal_argument,
                                                                                             m));
            } finally {
                writeUnlock();
            }
        else
            if(s != null)
                for(A x : c)
                    if(s.selects(x))
                        add(x, m);
                    else if(abort_on_illegal_argument) {
                        writeUnlock();
                        throw new IllegalArgumentException(
                                "Argument not selected by selector");
                    }
            else
                for(A y : c)
                    add(y, m);
        old_count = count - old_count;
        writeUnlock();
        assert old_count >= 0;
        return old_count > 0;
    }

    public boolean removeAll(Collection<?> c) {
        return removeAll(c, null);
    }

    public synchronized <B> boolean removeAll(Collection<B> c,
                                              Selector<? super B> s) {
        if(c == null)
            return false;
        writeLock();
        int old_count = count;
        if(c instanceof Traversable)
            try {
                ((Traversable<Object>)c).traverseSelected(IN_ORDER,
                                                          new TraversalActionPerformer.CollectionDropper<Object>(
                        (Collection<Object>)this), (Selector<Object>)s);
            } catch(RuntimeException e) {
                writeUnlock();
                throw e;
            }
        else if(s != null) {
            for(B o : c)
                if(s.selects(o))
                    remove(o);
        } else
            for(Object o : c)
                remove(o);
        old_count -= count;
        writeUnlock();
        assert old_count >= 0;
        return old_count > 0;
    }

    public boolean removeAll(Selector<? super A> s) {
        if(s == null)
            return false;
        writeLock();
        int old_count = count;
        Node<A> l = toLinkedList(), r;
        count = 0;
        while(l != null) {
            r = l.right;
            if(!s.selects(l.val))
                addNode(l, null);
            l = r;
        }
        old_count -= count;
        writeUnlock();
        assert old_count >= 0;
        return old_count > 0;
    }

    public boolean retainAll(Collection<?> c) {
        if(c == null) {
            writeLock();
            int old_count = count;
            clear();
            writeLock();
            return old_count > 0;
        }
        return retainAll((Selector<A>)((c instanceof OrderedSet)
                                        ? c : new Selector.ContainmentChecker<A>(
                (Collection<Object>)c)));
    }

    public synchronized boolean retainAll(Selector<? super A> s) {
        if(s == null)
            return false;
        writeLock();
        int old_count = count;
        Node<A> l = toLinkedList(), r;
        count = 0;
        while(l != null) {
            r = l.right;
            if(s.selects(l.val))
                addNode(l, null);
            l = r;
        }
        old_count -= count;
        writeUnlock();
        assert old_count >= 0;
        return old_count > 0;
    }

    public A removeComparable(Comparable<? super A> x) {
        if((x == null) || (root == null))
            return null;
        int r;
        Merger.Getter<A> m;
        if(hasWriteLock()) {
            if(root == null)
                return null;
            m = getter.get();
            if(premoveComparable(root, x, m) == LEAF_DELETED) {
                root = null;
                if(count != 0)
                    throw new RuntimeException("Tree should be empty now");
            }
            return m.result;
        }
        registerReader();
        if(root == null) {
            unregisterReader();
            return null;
        }
        m = getter.get();
        r = premoveComparable(root, x, m);
        if(r == NEED_LOCK) {
            unregisterReader();
            writeLock();
            if(root == null) {
                writeUnlock();
                return null;
            }
            r = premoveComparable(root, x, m);
        }
        if(r == LEAF_DELETED) {
            if(!hasWriteLock())
                throw new RuntimeException("Thread should have write lock");
            root = null;
            if(count != 0)
                throw new RuntimeException("Tree should be empty now");
        }
        if(hasWriteLock())
            writeUnlock();
        else
            unregisterReader();
        return m.result;
    }

    public <B> A removeMatch(B x) {
        if((x == null) || (root == null))
            return null;
        int r;
        Merger.Getter<A> m;
        if(hasWriteLock()) {
            if(root == null)
                return null;
            m = getter.get();
            if(premoveMatch(root, x, m) == LEAF_DELETED) {
                root = null;
                if(count != 0)
                    throw new RuntimeException("Tree should be empty now");
            }
            return m.result;
        }
        registerReader();
        if(root == null) {
            unregisterReader();
            return null;
        }
        m = getter.get();
        r = premoveMatch(root, x, m);
        if(r == NEED_LOCK) {
            unregisterReader();
            writeLock();
            if(root == null) {
                writeUnlock();
                return null;
            }
            r = premoveMatch(root, x, m);
        }
        if(r == LEAF_DELETED) {
            if(!hasWriteLock())
                throw new RuntimeException("Thread should have write lock");
            root = null;
            if(count != 0)
                throw new RuntimeException("Tree should be empty now");
        }
        if(hasWriteLock())
            writeUnlock();
        else
            unregisterReader();
        return m.result;
    }

    public boolean remove(Object o) {
        if((o == null) || (root == null))
            return false;
        int r, c;
        if(hasWriteLock()) {
            if(root == null)
                return false;
            c = count;
            if(premove(root, (A)o, null) == LEAF_DELETED) {
                root = null;
                if(count != 0)
                    throw new RuntimeException("Tree should be empty now");
            }
            c = c - count;
            return c > 0;
        }
        registerReader();
        if(root == null) {
            unregisterReader();
            return false;
        }
        c = count;
        r = premove(root, (A)o, null);
        if(r == NEED_LOCK) {
            unregisterReader();
            writeLock();
            if(root == null) {
                writeUnlock();
                return false;
            }
            c = count;
            r = premove(root, (A)o, null);
        }
        if(r == LEAF_DELETED) {
            if(!hasWriteLock())
                throw new RuntimeException("thread should have write lock");
            root = null;
            if(count != 0)
                throw new RuntimeException("Tree should be empty now");
        }
        c = c - count;
        if(hasWriteLock())
            writeUnlock();
        else
            unregisterReader();
        return c > 0;
    }

    public int traverse(int order, TraversalActionPerformer<? super A> p) {
        return traverseSelectedRange(order, p, null, null, null, false, null,
                                      false);
    }

    public int traverse(int order, TraversalActionPerformer<? super A> p,
                        Object o) {
        return traverseSelectedRange(order, p, o, null, null, false, null,
                                      false);
    }

    public int traverseSelected(int order, TraversalActionPerformer<? super A> p,
                                Selector<A> s) {
        return traverseSelectedRange(order, p, null, s, null, false, null,
                                      false);
    }

    public int traverseSelected(int order, TraversalActionPerformer<? super A> p,
                                Object o, Selector<A> s) {
        return traverseSelectedRange(order, p, o, s, null, false, null, false);
    }

    public int traverseRange(int order, TraversalActionPerformer<? super A> p,
                             A l, A r) {
        return traverseSelectedRange(order, p, null, null, l, true, r, false);
    }

    public int traverseRange(int order, TraversalActionPerformer<? super A> p,
                             Object o, A l, A r) {
        return traverseSelectedRange(order, p, o, null, l, true, r, false);
    }

    public int traverseRange(int order, TraversalActionPerformer<? super A> p,
                             A l, boolean left_included, A r,
                             boolean right_included) {
        return traverseSelectedRange(order, p, null, null, l, left_included, r,
                                      right_included);
    }

    public int traverseRange(int order, TraversalActionPerformer<? super A> p,
                             Object o,
                             A l, boolean left_included, A r,
                             boolean right_included) {
        return traverseSelectedRange(order, p, o, null, l, left_included, r,
                                      right_included);
    }

    public int traverseSelectedRange(int order,
                                     TraversalActionPerformer<? super A> p,
                                     Selector<A> s, A l, A r) {
        return traverseSelectedRange(order, p, null, s, l, true, r, false);
    }

    public int traverseSelectedRange(int order,
                                     TraversalActionPerformer<? super A> p,
                                     Object o, Selector<A> s, A l, A r) {
        return traverseSelectedRange(order, p, o, s, l, true, r, false);
    }

    public int traverseSelectedRange(int order,
                                     TraversalActionPerformer<? super A> p,
                                     Selector<A> s, A l, boolean left_included,
                                     A r, boolean right_included) {
        return traverseSelectedRange(order, p, null, s, l, left_included, r,
                                      right_included);
    }

    public int traverseSelectedRange(int order,
                                     TraversalActionPerformer<? super A> p,
                                     Object o, Selector<A> s, A l,
                                     boolean left_included,
                                     A r, boolean right_included) {
        if((p == null) || (root == null))
            return 0;
        A sl, sr;
        registerReader();
        if(s != null) {
            sl = s.lowerBound();
            if((l != null) && ((sl == null) || (comp.compare(sl, l) < 0)))
                sl = l;
            else
                left_included = (sl == null) || s.selects(sl);
            sr = s.upperBound();
            if((r != null) && ((sr == null) || (comp.compare(sr, r) > 0)))
                sr = r;
            else
                right_included = (sr == null) || s.selects(sr);
        } else {
            sl = l;
            sr = r;
        }
        try {
            return ptraverse(root, order, p, o, s, sl, left_included, sr,
                              right_included);
        } finally {
            unregisterReader();
        }
    }

    public void clear() {
        if(hasWriteLock()) {
            pclear(root);
            root = null;
            count = 0;
        } else {
            writeLock();
            pclear(root);
            root = null;
            count = 0;
            writeUnlock();
        }
    }

    public BBTreeMR<A> clone() {
        registerReader();
        BBTreeMR<A> a = new BBTreeMR<A>(this.comp != this ? this.comp : null);
        try {
            a.root = pclone(this.root);
            a.count = this.count;
        } finally {
            unregisterReader();
        }
        return a;
    }

    public Object[] toArray() {
        registerReader();
        TraversalActionPerformer.ArrayBuilder<A> a = new TraversalActionPerformer.ArrayBuilder<A>(
                count);
        try {
            ptraverse(root, IN_ORDER, a, null, null, null, false, null, false);
        } finally {
            unregisterReader();
        }
        return a.toArray();
    }

    public <B> B[] toArray(B[] b) {
        TraversalActionPerformer.ArrayBuilder<A> a = null;
        registerReader();
        if(b.length >= count)
            a = new TraversalActionPerformer.ArrayBuilder<A>(b);
        else
            a = new TraversalActionPerformer.ArrayBuilder<A>((B[])java.lang.reflect.Array.
                    newInstance(b.getClass().getComponentType(), count));
        try {
            ptraverse(root, IN_ORDER, a, null, null, null, false, null, false);
        } finally {
            unregisterReader();
        }
        return (B[])a.toArray();
    }

    public <B> B[] toArray(Class<B> componentType) {
        registerReader();
        TraversalActionPerformer.ArrayBuilder<A> a = new TraversalActionPerformer.ArrayBuilder<A>((B[])java.lang.reflect.Array.
                newInstance(componentType, count));
        try {
            ptraverse(root, IN_ORDER, a, null, null, null, false, null, false);
        } finally {
            unregisterReader();
        }
        return (B[])a.toArray();
    }

    public BBTreeMR<A> reorder(Comparator<? super A> c, boolean make_new) {
        return reorder(c, null, make_new);
    }

    public BBTreeMR<A> reorder(Comparator<? super A> c, Merger<A> m,
                               boolean make_new) {
        if(make_new) {
            if((c == this) || ((c == null) && (comp == this)))
                return clone();
            BBTreeMR<A> r = new BBTreeMR<A>(c != this ? c : null);
            traverse(PRE_ORDER, new TraversalActionPerformer.OrderedSetAdder<A>(
                    r, m));
            return r;
        }
        if((c == this) || ((c == null) && (comp == this)))
            return this;
        writeLock();
        Node<A> l = toLinkedList(), r;
        if(c != null)
            comp = c;
        else
            comp = this;
        count = 0;
        while(l != null) {
            r = l.right;
            addNode(l, m);
            l = r;
        }
        writeUnlock();
        return this;
    }

    public BBTreeMR<A> copySubSet(Selector<A> s) {
        if(s == null)
            return this.clone();
        A sl, sr;
        boolean left_included, right_included;
        registerReader();
        sl = s.lowerBound();
        left_included = (sl == null) || s.selects(sl);
        sr = s.upperBound();
        right_included = (sr == null) || s.selects(sr);
        BBTreeMR<A> t = new BBTreeMR<A>(comp != this ? comp : null);
        try {
            ptraverse(root, PRE_ORDER,
                      new TraversalActionPerformer.CollectionAdder<A>(t),
                      null, s, sl, left_included, sr, right_included);
        } finally {
            unregisterReader();
        }
        return t;
    }

    public BBTreeMR<A> copySubSet(A fromElement, A toElement) {
        return copySubSet(fromElement, true, toElement, false);
    }

    public BBTreeMR<A> copySubSet(A min, boolean min_included, A max,
                                  boolean max_included) {
        if((min == null) && (max == null))
            return this.clone();
        registerReader();
        BBTreeMR<A> t = new BBTreeMR<A>(comp != this ? comp : null);
        try {
            ptraverse(root, PRE_ORDER,
                      new TraversalActionPerformer.CollectionAdder<A>(t),
                      null, null, min, min_included, max, max_included);
        } finally {
            unregisterReader();
        }
        return t;
    }

    public BBTreeMR<A> copyHeadSet(A toElement) {
        return copySubSet(null, true, toElement, false);
    }

    public BBTreeMR<A> copyHeadSet(A max, boolean max_included) {
        return copySubSet(null, true, max, max_included);
    }

    public BBTreeMR<A> copyTailSet(A fromElement) {
        return copySubSet(fromElement, true, null, true);
    }

    public BBTreeMR<A> copyTailSet(A min, boolean min_included) {
        return copySubSet(min, min_included, null, true);
    }

    public BBTreeMR<A> join(Collection<? extends A> c) {
        BBTreeMR<A> r = clone();
        if((c == null) || (c == this))
            return r;
        if(c instanceof Traversable)
            ((Traversable<A>)c).traverse(IN_ORDER,
                                         new TraversalActionPerformer.CollectionAdder<A>(
                    r));
        else
            for(A h : c)
                r.add(h, null);
        return r;
    }

    public BBTreeMR<A> intersect(Collection<? super A> c) {
        if(c == this)
            return clone();
        registerReader();
        BBTreeMR<A> r = new BBTreeMR<A>(comp != this ? comp : null);
        if(c == null) {
            unregisterReader();
            return r;
        }
        try {
            ptraverse(root, IN_ORDER,
                      new TraversalActionPerformer.CollectionAdder<A>(r), null,
                      (c instanceof OrderedSet) ? (Selector<A>)c
                      : new Selector.ContainmentChecker<A>(c),
                      null, true, null, true);
        } finally {
            unregisterReader();
        }
        return r;
    }

    public BBTreeMR<A> without(Collection<? super A> c) {
        if(c == null)
            return clone();
        BBTreeMR<A> r = new BBTreeMR<A>(comp != this ? comp : null);
        if(c == this) {
            unregisterReader();
            return r;
        }
        try {
            ptraverse(root, IN_ORDER,
                      new TraversalActionPerformer.CollectionAdder<A>(r), null,
                      new Selector.Inverter<A>(
                    (c instanceof OrderedSet) ? (Selector<A>)c
                    : new Selector.ContainmentChecker<A>(c)),
                      null, true, null, true);
        } finally {
            unregisterReader();
        }
        return r;
    }

    public String toString() {
        return toStringBuilder(null).toString();
    }

    public StringBuilder toStringBuilder(StringBuilder sb) {
        TraversalActionPerformer.RepresentationStringBuilder<A> b =
                new TraversalActionPerformer.RepresentationStringBuilder<A>(sb);
        traverse(IN_ORDER, b);
        return b.getClosedStringBuilder();
    }

    public void writeToStream(Writer w) throws IOException {
        if(w == null)
            return;
        w.write('{');
        TraversalActionPerformer.RepresentationWriter<A> v =
                new TraversalActionPerformer.RepresentationWriter<A>(w);
        traverse(IN_ORDER, v);
        w.write('}');
    }

    public boolean subSetOf(Collection<?> s) {
        return isContainedIn(s, true);
    }

    public boolean superSetOf(Collection<?> s) {
        if(s == null)
            return true;
        return containsAll(s);
    }

    public int compareTo(OrderedSet<A> b) {
        if(b == null)
            return 1;
        if(this == b)
            return 0;
        if(b instanceof Final)
            return -b.compareTo(this);
        registerReader();
        TraversalActionPerformer.NavigableSetComparator<A> sc =
                TraversalActionPerformer.NavigableSetComparator.getThreadLocal(
                comp);
        if(ptraverse(root, IN_ORDER, sc, b, null, null, false, null, false) == 0) {
            unregisterReader();
            return b.first() == null ? 0 : -1;
        }
        unregisterReader();
        if((sc.c == 0) && (sc.last != null) && (b.higher(sc.last) != null))
            return -1;
        return sc.c;
    }

    public int compareTo(OrderedSet<A> b, Comparator<? super A> c) {
        if(b == null)
            return 1;
        if(this == b)
            return 0;
        if(b instanceof Final)
            return -b.compareTo(this);
        TraversalActionPerformer.NavigableSetComparator<A> sc =
                TraversalActionPerformer.NavigableSetComparator.getThreadLocal(c);
        if(traverse(IN_ORDER, sc, b) == 0)
            return b.first() == null ? 0 : -1;
        if((sc.c == 0) && (sc.last != null) && (b.higher(sc.last) != null))
            return -1;
        return sc.c;
    }

    public boolean equals(Object o) {
        if(o == null)
            return false;
        if(o == this)
            return true;
        if(o instanceof OrderedSet)
            return compareTo((OrderedSet<A>)o) == 0;
        if(o instanceof Collection) {
            registerReader();
            boolean r = (containsAll((Collection)o, null, false)
                         && isContainedIn((Collection)o, false));
            unregisterReader();
            return r;
        } else
            return false;
    }

    public int hashCode() {
        TraversalActionPerformer.Hasher<A> h = new TraversalActionPerformer.Hasher<A>();
        this.traverse(IN_ORDER, h);
        return h.getHash();
    }

    public boolean selects(A x) {
        return contains(x);
    }

    public A lowerBound() {
        return first();
    }

    public A upperBound() {
        return last();
    }

    public ResettableIterator<A> iterator() {
        return subSetIterator(null);
    }

    public ResettableIterator<A> descendingIterator() {
        return descendingSubSetIterator(null);
    }

    public ResettableIterator<A> subSetIterator(Selector<A> f) {
        return Thread.holdsLock(this)
                ? new ResettableIterator.NonLockingIterator<A>(this, f)
                : new BBTreeMR.LockingIterator<A>(this, f);
    }

    public ResettableIterator<A> descendingSubSetIterator(Selector<A> f) {
        return Thread.holdsLock(this)
                ? new ResettableIterator.NonLockingIterator<A>(this, f, true)
                : new BBTreeMR.LockingIterator<A>(this, f, true);
    }

    public ResettableIterator<A> subSetIterator(A fromElement, A toElement) {
        return subSetIterator(fromElement, true, toElement, false);
    }

    public ResettableIterator<A> descendingSubSetIterator(
            A fromElement, A toElement) {
        return descendingSubSetIterator(fromElement, true, toElement, false);
    }

    public ResettableIterator<A> subSetIterator(
            A min, boolean min_included, A max, boolean max_included) {
        return subSetIterator(new Selector.RangeSelector<A>(min, min_included,
                                                             max, max_included,
                                                             (SortedSet<A>)this));
    }

    public ResettableIterator<A> descendingSubSetIterator(
            A min, boolean min_included, A max, boolean max_included) {
        return subSetIterator(new Selector.RangeSelector<A>(
                min, min_included, max, max_included, (SortedSet<A>)this));
    }

    public ResettableIterator<A> headSetIterator(A toElement) {
        return headSetIterator(toElement, false);
    }

    public ResettableIterator<A> descendingHeadSetIterator(A toElement) {
        return descendingHeadSetIterator(toElement, false);
    }

    public ResettableIterator<A> headSetIterator(A max, boolean max_included) {
        return subSetIterator(new Selector.HalfRangeSelector<A>(
                max, max_included, Selector.LEFT, (SortedSet<A>)this));
    }

    public ResettableIterator<A> descendingHeadSetIterator(
            A max, boolean max_included) {
        return descendingSubSetIterator(new Selector.HalfRangeSelector<A>(
                max, max_included, Selector.LEFT, (SortedSet<A>)this));
    }

    public ResettableIterator<A> tailSetIterator(A fromElement) {
        return tailSetIterator(fromElement, true);
    }

    public ResettableIterator<A> descendingTailSetIterator(A fromElement) {
        return descendingTailSetIterator(fromElement, true);
    }

    public ResettableIterator<A> tailSetIterator(A min, boolean min_included) {
        return subSetIterator(new Selector.HalfRangeSelector<A>(
                min, min_included, Selector.RIGHT, (SortedSet<A>)this));
    }

    public ResettableIterator<A> descendingTailSetIterator(
            A min, boolean min_included) {
        return descendingSubSetIterator(new Selector.HalfRangeSelector<A>(
                min, min_included, Selector.RIGHT, (SortedSet<A>)this));
    }

    public Traversable.Set<A> descendingSet() {
        return descendingSubSet(null);
    }

    public Traversable.Set<A> subSet(Selector<A> f) {
        if(f == null)
            return this;
        //return(new SubSetStub(this,f));
        throw new UnsupportedOperationException("Not supported");
    }

    public Traversable.Set<A> descendingSubSet(Selector<A> f) {
        //return(new SubSetStub(this,f,true));
        throw new UnsupportedOperationException("Not supported");
    }

    public Traversable.Set<A> subSet(A fromElement, A toElement) {
        return subSet(fromElement, true, toElement, false);
    }

    public Traversable.Set<A> descendingSubSet(A fromElement, A toElement) {
        return descendingSubSet(fromElement, true, toElement, false);
    }

    public Traversable.Set<A> subSet(
            A min, boolean min_included, A max, boolean max_included) {
        return subSet(new Selector.RangeSelector<A>(
                min, min_included, max, max_included, (SortedSet<A>)this));
    }

    public Traversable.Set<A> descendingSubSet(
            A min, boolean min_included, A max, boolean max_included) {
        return descendingSubSet(new Selector.RangeSelector<A>(
                min, min_included, max, max_included, (SortedSet<A>)this));
    }

    public Traversable.Set<A> headSet(A toElement) {
        return headSet(toElement, false);
    }

    public Traversable.Set<A> descendingHeadSet(A toElement) {
        return descendingHeadSet(toElement, false);
    }

    public Traversable.Set<A> headSet(A max, boolean max_included) {
        return subSet(new Selector.HalfRangeSelector<A>(
                max, max_included, Selector.LEFT, (SortedSet<A>)this));
    }

    public Traversable.Set<A> descendingHeadSet(A max, boolean max_included) {
        return descendingSubSet(new Selector.HalfRangeSelector<A>(
                max, max_included, Selector.LEFT, (SortedSet<A>)this));
    }

    public Traversable.Set<A> tailSet(A fromElement) {
        return tailSet(fromElement, true);
    }

    public Traversable.Set<A> descendingTailSet(A fromElement) {
        return descendingTailSet(fromElement, true);
    }

    public Traversable.Set<A> tailSet(A min, boolean min_included) {
        return subSet(new Selector.HalfRangeSelector<A>(
                min, min_included, Selector.RIGHT, (SortedSet<A>)this));
    }

    public Traversable.Set<A> descendingTailSet(A min, boolean min_included) {
        return subSet(new Selector.HalfRangeSelector<A>(
                min, min_included, Selector.RIGHT, (SortedSet<A>)this));
    }

    public Final<A> toFinal() {
        return new Final(this);
    }

    public boolean isFinal() {
        return false;
    }

    public A pollFirst() {
        registerReader();
        if(root == null) {
            unregisterReader();
            return null;
        }
        if(!upgradeLock()) {
            unregisterReader();
            writeLock();
            if(root == null) {
                writeUnlock();
                return null;
            }
        }
        A r = pfirst();
        if(!remove(r)) {
            writeUnlock();
            throw new RuntimeException("First element could not be deleted");
        }
        writeUnlock();
        return r;
    }

    public A pollLast() {
        registerReader();
        if(root == null) {
            unregisterReader();
            return null;
        }
        if(!upgradeLock()) {
            unregisterReader();
            writeLock();
            if(root == null) {
                writeUnlock();
                return null;
            }
        }
        A r = plast();
        if(!remove(r)) {
            writeUnlock();
            throw new RuntimeException("First element could not be deleted");
        }
        writeUnlock();
        return r;
    }

    private Node<A> toLinkedList() {
        Node<A> r = null;
        if(root == null)
            return r;
        Node[] s = new Node[pmaxSearchDepth(root)];
        int[] cr = new int[s.length];
        int sp = -1, cc;
        Node<A> t = root, l = null;
        cc = FROM_TOP;
        while((sp >= 0) || (cc != DOWN_RIGHT))
            switch(cc) {
                case FROM_TOP: // pre-order
                    if(t.left != null) {
                        sp++;
                        s[sp] = t;
                        cr[sp] = DOWN_LEFT;
                        t = t.left;
                        cc = FROM_TOP;
                    } else
                        cc = DOWN_LEFT;
                    break;
                case DOWN_LEFT:  // in-order
                    r = t;
                    r.left = l;
                    if(l != null)
                        l.right = r;
                    l = r;
                    if(t.right != null) {
                        sp++;
                        s[sp] = t;
                        cr[sp] = DOWN_RIGHT;
                        t = t.right;
                        cc = FROM_TOP;
                    } else
                        cc = DOWN_RIGHT;
                    break;
                case DOWN_RIGHT: // post-order
                    t = s[sp];
                    cc = cr[sp];
                    sp--;
                    break;
                default:
                    break;
            }
        root = null;
        if(r != null)
            while(r.left != null)
                r = r.left;
        return r;
    }

    private int pclear(Node<A> t) {
        if(t == null)
            return 0;
        int hl = pclear(t.left), hr = pclear(t.right);
        t.val = null;
        t.left = null;
        t.right = null;
        return 1 + (hl < hr ? hr : hl);
    }

    private Node<A> pclone(Node<A> t) {
        if(t == null)
            return null;
        Node<A> r = new Node<A>(t.val);
        r.left = pclone(t.left);
        r.right = pclone(t.right);
        return r;
    }

    private int pmaxSearchDepth(Node<A> t) {
        if(t == null)
            return 0;
        if(t.balance >= 0)
            return pmaxSearchDepth(t.left) + t.balance + 1;
        else
            return pmaxSearchDepth(t.right) - t.balance + 1;
    }

    private int pcheck(Node<A> t) {
        if(t == null)
            return 0;
        if(t.val == null)
            System.out.println("Error in BBTreeMR: check: null entry in node");
        int hl = pcheck(t.left);
        int hr = pcheck(t.right);
        if(hr - hl != t.balance)
            System.out.println(
                    "Error in BBTreeMR: check: balance in node " + t.val + " incorrect: " + t.balance + ", should be " + (hr - hl));
        if((hr - hl < -1) || (hr - hl > 1))
            System.out.println(
                    "Error in BBTreeMR: check: balance in node " + t.val + " out of bounds: " + (hr - hl));
        return 1 + (hl < hr ? hr : hl);
    }

    private int ptraverse(Node<A> t, int traverse_order,
                          TraversalActionPerformer traverse_action, Object o,
                          Selector<? super A> s, A l, boolean li, A r,
                          boolean ri) {
        if(t == null)
            return 0;
        int a = 1, h, c1 = l != null ? comp.compare(l, t.val) : -1,
                c2 = r != null ? comp.compare(r, t.val) : 1;
        boolean sel = s != null ? s.selects(t.val)
                                  && (li ? c1 <= 0 : c1 < 0) && (ri ? c2 >= 0 : c2 > 0)
                      : (li ? c1 <= 0 : c1 < 0) && (ri ? c2 >= 0 : c2 > 0);
        if(sel && ((traverse_order & PRE_ORDER) != 0))
            if(traverse_action.perform(t.val, PRE_ORDER, o))
                return -a;
        if((traverse_order & REVERSE_ORDER) == 0) {
            if(c1 < 0) {
                h = ptraverse(t.left, traverse_order, traverse_action, o, s, l,
                              li, r, ri);
                if(h < 0)
                    return h - a;
                a += h;
            }
            if(sel && ((traverse_order & IN_ORDER) != 0))
                if(traverse_action.perform(t.val, IN_ORDER, o))
                    return -a;
            if(c2 > 0) {
                h = ptraverse(t.right, traverse_order, traverse_action, o, s, l,
                              li, r, ri);
                if(h < 0)
                    return h - a;
                a += h;
            }
            if(sel && ((traverse_order & POST_ORDER) != 0))
                if(traverse_action.perform(t.val, POST_ORDER, o))
                    return -a;
        } else {
            if(c2 > 0) {
                h = ptraverse(t.right, traverse_order, traverse_action, o, s, l,
                              li, r, ri);
                if(h < 0)
                    return h - a;
                a += h;
            }
            if(sel && ((traverse_order & IN_ORDER) != 0))
                if(traverse_action.perform(t.val, IN_ORDER, o))
                    return -a;
            if(c1 < 0) {
                h = ptraverse(t.left, traverse_order, traverse_action, o, s, l,
                              li, r, ri);
                if(h < 0)
                    return h - a;
                a += h;
            }
            if(sel && ((traverse_order & POST_ORDER) != 0))
                if(traverse_action.perform(t.val, POST_ORDER, o))
                    return -a;
        }
        return a;
    }

    private boolean addNode(Node<A> x, Merger<A> m) {
        if((x == null) || (x.val == null))
            return false;
        x.left = null;
        x.right = null;
        x.balance = BALANCED;
        if(root == null) {
            root = x;
            count = 1;
            return true;
        }
        int old_count = count;
        paddNode(root, x, m);
        //check();
        assert count >= old_count;
        return count > old_count;
    }

    private boolean paddNode(Node<A> t, Node<A> x, Merger<A> m) {
        int c = comp.compare(x.val, t.val);
        if(c == 0) {
            if(m != null)
                t.val = m.merge(t.val, x.val);
            else
                x.val = t.val;
            return false;
        }
        if(c < 0)
            if(t.left != null)
                if(paddNode(t.left, x, m))
                    return t.rebalanceAfterLeftExcursion(true);
                else
                    return false;
            else {
                t.left = x;
                t.balance--;
                count++;
                return t.right == null;
            }
        else
            if(t.right != null)
                if(paddNode(t.right, x, m))
                    return t.rebalanceAfterRightExcursion(true);
                else
                    return false;
            else {
                t.right = x;
                t.balance++;
                count++;
                return t.left == null;
            }
    }

    private int padd(Node<A> t, A x, Merger<A> m) {
        int c = comp.compare(x, t.val);
        if(c == 0) {
            A y;
            if(m != null)
                y = m.merge(t.val, x);
            else
                y = x;
            if(t.val != y) {
                if((!hasWriteLock()) && (!upgradeLock()))
                    return NEED_LOCK;
                t.val = y;
            }
            return NO_CHANGE;
        }
        if(c < 0)
            if(t.left != null)
                switch(padd(t.left, x, m)) {
                    case NEED_LOCK:
                        return NEED_LOCK;
                    case DEPTH_CHANGED:
                        if(t.rebalanceAfterLeftExcursion(true))
                            return DEPTH_CHANGED;
                    case NO_CHANGE:
                        return NO_CHANGE;
                    default:
                        throw new RuntimeException("Unknown return code");
                }
            else {
                if(m != null)
                    m.merge(null, x);
                if((!hasWriteLock()) && (!upgradeLock()))
                    return NEED_LOCK;
                t.left = new Node<A>(x);
                t.balance--;
                count++;
                return t.right == null ? DEPTH_CHANGED : NO_CHANGE;
            }
        else
            if(t.right != null)
                switch(padd(t.right, x, m)) {
                    case NEED_LOCK:
                        return NEED_LOCK;
                    case DEPTH_CHANGED:
                        if(t.rebalanceAfterRightExcursion(true))
                            return DEPTH_CHANGED;
                    case NO_CHANGE:
                        return NO_CHANGE;
                    default:
                        throw new RuntimeException("Unknown return code");
                }
            else {
                if(m != null)
                    m.merge(null, x);
                if((!hasWriteLock()) && (!upgradeLock()))
                    return NEED_LOCK;
                t.right = new Node<A>(x);
                t.balance++;
                count++;
                return t.left == null ? DEPTH_CHANGED : NO_CHANGE;
            }
    }

    private <B> int paddOrGetMatch(Node<A> t, B x, KeyedElementFactory<A, B> f,
                                   Merger<A> m) {
        int c = -((AsymmetricComparator<A>)comp).asymmetricCompare(t.val,x);
        if(c == 0) {
            if(m != null)
                m.merge(t.val, null);
            return NO_CHANGE;
        }
        if(c < 0)
            if(t.left != null)
                switch(paddOrGetMatch(t.left, x, f, m)) {
                    case NEED_LOCK:
                        return NEED_LOCK;
                    case DEPTH_CHANGED:
                        if(t.rebalanceAfterLeftExcursion(true))
                            return DEPTH_CHANGED;
                    case NO_CHANGE:
                        return NO_CHANGE;
                    default:
                        throw new RuntimeException("Unknown return code");
                }
            else {
                if((!hasWriteLock()) && (!upgradeLock()))
                    return NEED_LOCK;
                A y = f.createElement(x);
                if(y == null)
                    throw new NullPointerException(
                            "Element factory created null element");
                t.left = new Node<A>(y);
                t.balance--;
                count++;
                m.merge(null, y);
                return t.right == null ? DEPTH_CHANGED : NO_CHANGE;
            }
        else
            if(t.right != null)
                switch(paddOrGetMatch(t.right, x, f, m)) {
                    case NEED_LOCK:
                        return NEED_LOCK;
                    case DEPTH_CHANGED:
                        if(t.rebalanceAfterRightExcursion(true))
                            return DEPTH_CHANGED;
                    case NO_CHANGE:
                        return NO_CHANGE;
                    default:
                        throw new RuntimeException("Unknown return code");
                }
            else {
                if((!hasWriteLock()) && (!upgradeLock()))
                    return NEED_LOCK;
                A y = f.createElement(x);
                if(y == null)
                    throw new NullPointerException(
                            "Element factory created null element");
                t.right = new Node<A>(y);
                t.balance++;
                count++;
                m.merge(null, y);
                return t.left == null ? DEPTH_CHANGED : NO_CHANGE;
            }
    }

    private int premove(Node<A> t, A x, Merger<A> m) {
        if(t == null)
            return 0;
        int c = comp.compare(x, t.val);
        if(c <= 0) {
            if(c == 0) {
                if((!hasWriteLock()) && (!upgradeLock()))
                    return NEED_LOCK;
                if(m != null)
                    m.merge(t.val, null);
                if((t.right != null) && (t.left != null)) {
                    Node<A> r = t.left;
                    while(r.right != null)
                        r = r.right;
                    A y = t.val;
                    t.val = r.val;
                    r.val = y;
                } else {
                    count--;
                    if((t.left == null) && (t.right == null)) {
                        t.val = null;
                        return LEAF_DELETED;
                    } else if(t.left != null) {
                        t.val = t.left.val;
                        t.right = t.left.right;
                        t.left.val = null;
                        t.balance = t.left.balance;
                        t.left = t.left.left;
                        if((t.left != null) || (t.right != null))
                            throw new RuntimeException(
                                    "Left subtree expected to be leaf");
                    } else {
                        t.val = t.right.val;
                        t.left = t.right.left;
                        t.right.val = null;
                        t.balance = t.right.balance;
                        t.right = t.right.right;
                        if((t.left != null) || (t.right != null))
                            throw new RuntimeException(
                                    "Rigt subtree expected to be leaf");
                    }
                    return DEPTH_CHANGED;
                }
            }
            switch(premove(t.left, x, m)) {
                case NEED_LOCK:
                    return NEED_LOCK;
                case LEAF_DELETED:
                    t.left = null;
                case DEPTH_CHANGED:
                    if(t.rebalanceAfterRightExcursion(false))
                        return DEPTH_CHANGED;
                case NO_CHANGE:
                    return NO_CHANGE;
                default:
                    throw new RuntimeException("Unknown return code");
            }
        } else
            switch(premove(t.right, x, m)) {
                case NEED_LOCK:
                    return NEED_LOCK;
                case LEAF_DELETED:
                    t.right = null;
                case DEPTH_CHANGED:
                    if(t.rebalanceAfterLeftExcursion(false))
                        return DEPTH_CHANGED;
                case NO_CHANGE:
                    return NO_CHANGE;
                default:
                    throw new RuntimeException("Unknown return code");
            }
    }

    private int premoveComparable(Node<A> t, Comparable<? super A> x,
                                  Merger<A> m) {
        if(t == null)
            return 0;
        int c = x.compareTo(t.val);
        if(c <= 0) {
            if(c == 0) {
                if((!hasWriteLock()) && (!upgradeLock()))
                    return NEED_LOCK;
                if(m != null)
                    m.merge(t.val, null);
                if((t.right != null) && (t.left != null)) {
                    Node<A> r = t.left;
                    while(r.right != null)
                        r = r.right;
                    A y = t.val;
                    t.val = r.val;
                    r.val = y;
                } else {
                    count--;
                    if((t.left == null) && (t.right == null)) {
                        t.val = null;
                        return LEAF_DELETED;
                    } else if(t.left != null) {
                        t.val = t.left.val;
                        t.right = t.left.right;
                        t.left.val = null;
                        t.balance = t.left.balance;
                        t.left = t.left.left;
                        if((t.left != null) || (t.right != null))
                            throw new RuntimeException(
                                    "Left subtree expected to be leaf");
                    } else {
                        t.val = t.right.val;
                        t.left = t.right.left;
                        t.right.val = null;
                        t.balance = t.right.balance;
                        t.right = t.right.right;
                        if((t.left != null) || (t.right != null))
                            throw new RuntimeException(
                                    "Rigt subtree expected to be leaf");
                    }
                    return DEPTH_CHANGED;
                }
            }
            switch(premoveComparable(t.left, x, m)) {
                case NEED_LOCK:
                    return NEED_LOCK;
                case LEAF_DELETED:
                    t.left = null;
                case DEPTH_CHANGED:
                    if(t.rebalanceAfterRightExcursion(false))
                        return DEPTH_CHANGED;
                case NO_CHANGE:
                    return NO_CHANGE;
                default:
                    throw new RuntimeException("Unknown return code");
            }
        } else
            switch(premoveComparable(t.right, x, m)) {
                case NEED_LOCK:
                    return NEED_LOCK;
                case LEAF_DELETED:
                    t.right = null;
                case DEPTH_CHANGED:
                    if(t.rebalanceAfterLeftExcursion(false))
                        return DEPTH_CHANGED;
                case NO_CHANGE:
                    return NO_CHANGE;
                default:
                    throw new RuntimeException("Unknown return code");
            }
    }

    private <B> int premoveMatch(Node<A> t, B x, Merger<A> m) {
        if(t == null)
            return 0;
        int c = ((AsymmetricComparator<A>)comp).asymmetricCompare(t.val,x);
        if(c >= 0) {
            if(c == 0) {
                if((!hasWriteLock()) && (!upgradeLock()))
                    return NEED_LOCK;
                if(m != null)
                    m.merge(t.val, null);
                if((t.right != null) && (t.left != null)) {
                    Node<A> r = t.left;
                    while(r.right != null)
                        r = r.right;
                    A y = t.val;
                    t.val = r.val;
                    r.val = y;
                } else {
                    count--;
                    if((t.left == null) && (t.right == null)) {
                        t.val = null;
                        return LEAF_DELETED;
                    } else if(t.left != null) {
                        t.val = t.left.val;
                        t.right = t.left.right;
                        t.left.val = null;
                        t.balance = t.left.balance;
                        t.left = t.left.left;
                        if((t.left != null) || (t.right != null))
                            throw new RuntimeException(
                                    "Left subtree expected to be leaf");
                    } else {
                        t.val = t.right.val;
                        t.left = t.right.left;
                        t.right.val = null;
                        t.balance = t.right.balance;
                        t.right = t.right.right;
                        if((t.left != null) || (t.right != null))
                            throw new RuntimeException(
                                    "Rigt subtree expected to be leaf");
                    }
                    return DEPTH_CHANGED;
                }
            }
            switch(premoveMatch(t.left, x, m)) {
                case NEED_LOCK:
                    return NEED_LOCK;
                case LEAF_DELETED:
                    t.left = null;
                case DEPTH_CHANGED:
                    if(t.rebalanceAfterRightExcursion(false))
                        return DEPTH_CHANGED;
                case NO_CHANGE:
                    return NO_CHANGE;
                default:
                    throw new RuntimeException("Unknown return code");
            }
        } else
            switch(premoveMatch(t.right, x, m)) {
                case NEED_LOCK:
                    return NEED_LOCK;
                case LEAF_DELETED:
                    t.right = null;
                case DEPTH_CHANGED:
                    if(t.rebalanceAfterLeftExcursion(false))
                        return DEPTH_CHANGED;
                case NO_CHANGE:
                    return NO_CHANGE;
                default:
                    throw new RuntimeException("Unknown return code");
            }
    }

    private static class Node<A> {

        A val;
        Node<A> left = null, right = null;
        int balance = 0;

        /** Creates a new instance of Node */
        Node(A val) {
            this.val = val;
        }

        Node(A val, Node<A> left, Node<A> right) {
            this.val = val;
            this.left = left;
            this.right = right;
        }

        public A getValue() {
            return val;
        }

        private boolean rebalanceAfterRightExcursion(boolean elementAdded) {
            switch(balance) {
                case BALANCED:
                    balance = RIGHT_HIGH;
                    return elementAdded;
                case LEFT_HIGH:
                    balance = BALANCED;
                    return !elementAdded;
                case RIGHT_HIGH:
                    Node<A> r = right;
                    A temp = val;
                    switch(r.balance) {
                        case BALANCED:
                            val = r.val;
                            r.val = temp;
                            right = r.right;
                            r.right = r.left;
                            r.left = left;
                            left = r;
                            r.balance = RIGHT_HIGH;
                            balance = LEFT_HIGH;
                            return elementAdded;
                        case LEFT_HIGH:
                            Node<A> rl = r.left;
                            switch(rl.balance) {
                                case BALANCED:
                                    val = rl.val;
                                    rl.val = temp;
                                    r.left = rl.right;
                                    rl.right = rl.left;
                                    rl.left = left;
                                    left = rl;
                                    left.balance = BALANCED;
                                    right.balance = BALANCED;
                                    balance = BALANCED;
                                    return !elementAdded;
                                case LEFT_HIGH:
                                    val = rl.val;
                                    rl.val = temp;
                                    r.left = rl.right;
                                    rl.right = rl.left;
                                    rl.left = left;
                                    left = rl;
                                    left.balance = BALANCED;
                                    right.balance = RIGHT_HIGH;
                                    balance = BALANCED;
                                    return !elementAdded;
                                case RIGHT_HIGH:
                                    val = rl.val;
                                    rl.val = temp;
                                    r.left = rl.right;
                                    rl.right = rl.left;
                                    rl.left = left;
                                    left = rl;
                                    left.balance = LEFT_HIGH;
                                    right.balance = BALANCED;
                                    balance = BALANCED;
                                    return !elementAdded;
                                default:
                                    throw new RuntimeException(
                                            "Unbalanced child node discovered");
                            }
                        case RIGHT_HIGH:
                            val = r.val;
                            r.val = temp;
                            right = r.right;
                            r.right = r.left;
                            r.left = left;
                            left = r;
                            r.balance = BALANCED;
                            balance = BALANCED;
                            return !elementAdded;
                        default:
                            throw new RuntimeException(
                                    "Unbalanced child node discovered");
                    }
                default:
                    throw new RuntimeException("Unbalanced node discovered");
            }
        }

        private boolean rebalanceAfterLeftExcursion(boolean elementAdded) {
            switch(balance) {
                case BALANCED:
                    balance = LEFT_HIGH;
                    return elementAdded;
                case RIGHT_HIGH:
                    balance = BALANCED;
                    return !elementAdded;
                case LEFT_HIGH:
                    Node<A> l = left;
                    A temp = val;
                    switch(l.balance) {
                        case BALANCED:
                            val = l.val;
                            l.val = temp;
                            left = l.left;
                            l.left = l.right;
                            l.right = right;
                            right = l;
                            l.balance = LEFT_HIGH;
                            balance = RIGHT_HIGH;
                            return elementAdded;
                        case LEFT_HIGH:
                            val = l.val;
                            l.val = temp;
                            left = l.left;
                            l.left = l.right;
                            l.right = right;
                            right = l;
                            l.balance = BALANCED;
                            balance = BALANCED;
                            return !elementAdded;
                        case RIGHT_HIGH:
                            Node<A> lr = l.right;
                            switch(lr.balance) {
                                case BALANCED:
                                    val = lr.val;
                                    lr.val = temp;
                                    l.right = lr.left;
                                    lr.left = lr.right;
                                    lr.right = right;
                                    right = lr;
                                    left.balance = BALANCED;
                                    right.balance = BALANCED;
                                    balance = BALANCED;
                                    return !elementAdded;
                                case LEFT_HIGH:
                                    val = lr.val;
                                    lr.val = temp;
                                    l.right = lr.left;
                                    lr.left = lr.right;
                                    lr.right = right;
                                    right = lr;
                                    left.balance = BALANCED;
                                    right.balance = RIGHT_HIGH;
                                    balance = BALANCED;
                                    return !elementAdded;
                                case RIGHT_HIGH:
                                    val = lr.val;
                                    lr.val = temp;
                                    l.right = lr.left;
                                    lr.left = lr.right;
                                    lr.right = right;
                                    right = lr;
                                    left.balance = LEFT_HIGH;
                                    right.balance = BALANCED;
                                    balance = BALANCED;
                                    return !elementAdded;
                                default:
                                    throw new RuntimeException(
                                            "Unbalanced child node discovered");
                            }
                        default:
                            throw new RuntimeException(
                                    "Unbalanced child node discovered");
                    }
                default:
                    throw new RuntimeException("Unbalanced node discovered");
            }
        }
    }

    private static class LockingIterator<A> implements ResettableIterator<A> {

        private final Selector<A> s;
        private final BBTreeMR<A> b;
        private final boolean reverseOrder;
        private volatile A last;
        private Node<A>[] nst;
        private int[] ost;
        private int sp;
        private java.util.LinkedList<A> l;
        private volatile int status;
        private static final int LAST_REMOVED = 0x01;
        private static final int HOLDS_READ_LOCK = 0x02;

        public LockingIterator(BBTreeMR<A> b) {
            this(b, null, false);
        }

        public LockingIterator(BBTreeMR<A> b, Selector<A> s) {
            this(b, s, false);
        }

        public LockingIterator(BBTreeMR<A> b, boolean reverseOrder) {
            this(b, null, reverseOrder);
        }

        public LockingIterator(BBTreeMR<A> b, Selector<A> s,
                               boolean reverseOrder) {
            if(b == null)
                throw new IllegalArgumentException(
                        "Tree to be iterated is null!");
            this.b = b;
            if(s != null)
                this.s = s;
            else
                this.s = Selector.Accepter;
            this.reverseOrder = reverseOrder;
            reset();
        }

        public void reset() {
            if((status & HOLDS_READ_LOCK) == 0) {
                b.registerReader();
                status = HOLDS_READ_LOCK;
            } else {
                b.unregisterReader();
                status &= ~HOLDS_READ_LOCK;
                b.registerReader();
                status = HOLDS_READ_LOCK;
            }
            last = null;
            sp = -1;
            int depth = b.pmaxSearchDepth(b.root);
            if(depth > 0) {
                if((nst == null) || (nst.length < depth))
                    nst = new Node[depth];
                if((ost == null) || (ost.length < depth))
                    ost = new int[depth];
                sp = 0;
                nst[sp] = b.root;
                ost[sp] = FROM_TOP;
                if(reverseOrder) {
                    while(nst[sp].right != null) {
                        ost[sp] = DOWN_RIGHT;
                        sp++;
                        nst[sp] = nst[sp - 1].right;
                        ost[sp] = FROM_TOP;
                    }
                    ost[sp] = DOWN_RIGHT;
                } else {
                    while(nst[sp].left != null) {
                        ost[sp] = DOWN_LEFT;
                        sp++;
                        nst[sp] = nst[sp - 1].left;
                        ost[sp] = FROM_TOP;
                    }
                    ost[sp] = DOWN_LEFT;
                }
                while((sp >= 0) && !this.s.selects(nst[sp].val))
                    pnext();
            }

        }

        public OrderedSet<A> iteratedSet() {
            return b;
        }

        public ResettableIterator<A> iterator() {
            reset();
            return this;
        }

        private void pnext() {
            if(sp < 0)
                throw new NoSuchElementException("No elements to iterate");
            if(reverseOrder) {
                if(ost[sp] != DOWN_RIGHT)
                    throw new RuntimeException("Internal error 1 occured");
                ost[sp] = DOWN_LEFT;
                if(nst[sp].left != null) {
                    sp++;
                    nst[sp] = nst[sp - 1].left;
                    ost[sp] = FROM_TOP;
                    while(nst[sp].right != null) {
                        ost[sp] = DOWN_RIGHT;
                        sp++;
                        nst[sp] = nst[sp - 1].right;
                        ost[sp] = FROM_TOP;
                    }
                    ost[sp] = DOWN_LEFT;
                } else {
                    sp--;
                    while(sp >= 0) {
                        if(ost[sp] == DOWN_RIGHT)
                            break;
                        if(ost[sp] == DOWN_LEFT) {
                            sp--;
                            continue;
                        }
                        if(ost[sp] == FROM_TOP)
                            throw new RuntimeException(
                                    "Internal error 2 occured");
                        else
                            throw new RuntimeException(
                                    "Internal error 3 occured");
                    }
                }
            } else {
                if(ost[sp] != DOWN_LEFT)
                    throw new RuntimeException("Internal error 1 occured");
                ost[sp] = DOWN_RIGHT;
                if(nst[sp].right != null) {
                    sp++;
                    nst[sp] = nst[sp - 1].right;
                    ost[sp] = FROM_TOP;
                    while(nst[sp].left != null) {
                        ost[sp] = DOWN_LEFT;
                        sp++;
                        nst[sp] = nst[sp - 1].left;
                        ost[sp] = FROM_TOP;
                    }
                    ost[sp] = DOWN_LEFT;
                } else {
                    sp--;
                    while(sp >= 0) {
                        if(ost[sp] == DOWN_LEFT)
                            break;
                        if(ost[sp] == DOWN_RIGHT) {
                            sp--;
                            continue;
                        }
                        if(ost[sp] == FROM_TOP)
                            throw new RuntimeException(
                                    "Internal error 2 occured");
                        else
                            throw new RuntimeException(
                                    "Internal error 3 occured");
                    }
                }
            }
            if(sp < 0) {
                if(nst != null)
                    for(int i = 0; i < nst.length; i++)
                        nst[i] = null;
                if((status & HOLDS_READ_LOCK) != 0) {
                    b.unregisterReader();
                    status &= ~HOLDS_READ_LOCK;
                }
                if(l != null) {
                    b.removeAll(l);
                    l = null;
                }
            }
        }

        public synchronized A next() {
            if(sp < 0)
                throw new NoSuchElementException("No more elements to iterate");
            last = nst[sp].val;
            status &= ~LAST_REMOVED;
            pnext();
            while((sp >= 0) && (!s.selects(nst[sp].val)))
                pnext();
            return last;
        }

        public synchronized boolean hasNext() {
            return sp >= 0;
        }

        public synchronized void remove() {
            if(last == null)
                throw new IllegalStateException("Iteration has not yet started");
            if((status & LAST_REMOVED) == 0) {
                if(sp < 0)
                    b.remove(last);
                else {
                    if(l == null)
                        l = new LinkedList<A>();
                    l.add(last);
                }
                status |= LAST_REMOVED;
            }
        }

        public synchronized void finalize() {
            nst = null;
            ost = null;
            if((status & HOLDS_READ_LOCK) != 0) {
                b.unregisterReader();
                status &= ~HOLDS_READ_LOCK;
            }
            if(l != null)
                b.removeAll(l);
            l = null;
        }
    }
    /*
    private static class SubSetStub<A> implements OrderedSet<A> {
    private final BBTreeMR<A> s;
    private final Selector<? super A> f;
    public SubSetStub(BBTreeMR<A> s) {
    this(s,null);
    }
    public SubSetStub(BBTreeMR<A> s,Selector<? super A> f) {
    this.s = s;
    s.isEmpty();
    if(f!=null)
    this.f = f;
    else
    this.f = Selector.Accepter;
    }
    public boolean contains(Object o) {
    return(f.selects((A)o)&&s.contains(o));
    }
    public boolean containsAll(Collection<?> c) {
    return(s.containsAll(c,f));
    }
    public boolean containsAll(Collection<?> c,Selector<?> g) {
    return(s.containsAll(c,new Selector.Conjunctor<Object>(
    (Selector<Object>)g,(Selector<Object>)f)));
    }
    public boolean insert(A x,Merger<A> m) {
    if(!f.selects(x))
    throw(new IllegalArgumentException("argument not selected by selector"));
    return(s.insert(x,m));
    }
    public boolean insertAll(Collection<? extends A> c,Merger<A> m) {
    return(s.insertAll(c,f,true,m));
    }
    public A addOrGet(A x) {
    if(!f.selects(x))
    throw(new IllegalArgumentException("argument not selected by selector"));
    return(s.addOrGet(x));
    }
    
    public boolean add(A x) {
    return(insert(x,null));
    }
    public boolean addAll(Collection<? extends A> c) {
    return(s.insertAll(c,f,true,null));
    }
    public boolean remove(Object o) {
    if((o==null)||!f.selects((A)o))
    return(false);
    return(s.remove(o));
    }
    public boolean removeAll(Collection<?> c) {
    if(c==null)
    return(false);
    return(s.removeAll(c,f,false));
    }
    public boolean removeAll(Selector<? super A> t) {
    if(t==null)
    return(false);
    return(s.removeAll(new Selector.Conjunctor<A>(f,t)));
    }
    public boolean retainAll(Collection<?> c) {
    if(c==null)
    return(retainAll(f));
    else
    return(retainAll(new Selector.Disjunctor<A>(f,(c instanceof OrderedSet)?
    (Selector<A>)c:new Selector.ContainmentChecker<A>((Collection<Object>)c))));
    }
    public boolean retainAll(Selector<? super A> t) {
    if(t==null)
    return(false);
    return(s.retainAll(new Selector.Disjunctor<A>(new Selector.Inverter<A>(f),t)));
    }
    public A delete(Comparable<? super A> x) {
    if(x==null)
    return(null);
    A y = s.search(x);
    if((y!=null)&&f.selects(y))
    y = s.delete(x);
    else
    y = null;
    return(y);
    }
    public boolean isEmpty() {
    return(size()==0);
    }
    public void clear() {
    s.removeAll(f);
    }
    public int size() {
    TraverseActionPerformer.Counter<A> c = new TraverseActionPerformer.Counter<A>(f);
    s.traverse(IN_ORDER,c);
    return(c.getCounter());
    }
    public A search(Comparable<? super A> x) {
    if(x==null)
    return(null);
    A y = s.search(x);
    if((y==null)||!f.selects(y))
    return(null);
    return(y);
    }
    public A get(A x) {
    if(x==null)
    return(null);
    A y = s.get(x);
    if((y==null)||!f.selects(y))
    return(null);
    return(y);
    }
    public A set(A x) {
    if((x==null)||!f.selects(x))
    return(null);
    return(s.set(x));
    }
    public A min() {
    return(first());
    }
    public A max() {
    return(last());
    }
    public A first() {
    TraverseActionPerformer.LogicalJunctor<A> t =
    new TraverseActionPerformer.LogicalJunctor<A>(TraverseActionPerformer.LOGICAL_OR,
    null,f);
    s.traverse(IN_ORDER,t);
    return(t.getLast());
    }
    public A last() {
    TraverseActionPerformer.LogicalJunctor<A> t =
    new TraverseActionPerformer.LogicalJunctor<A>(TraverseActionPerformer.LOGICAL_OR,
    null,f);
    s.writeLock();
    s.ordering = !s.ordering;
    try { s.ptraverse(s.root,IN_ORDER,t,null); }
    catch(TraverseActionPerformer.TraversalAbortedException e) {}
    s.ordering = !s.ordering;
    s.writeUnlock();
    return(t.getLast());
    }
    public A next(A x) {
    if(x==null)
    return(first());
    s.registerReader();
    TraverseActionPerformer.LogicalJunctor<A> t =
    new TraverseActionPerformer.LogicalJunctor<A>(TraverseActionPerformer.LOGICAL_OR,
    new Selector.HalfRangeSelector(x,false,Selector.RIGHT,s.comparator()),f);
    try { s.ptraverse(s.root,IN_ORDER,t,null); }
    catch(TraverseActionPerformer.TraversalAbortedException e) {}
    s.unregisterReader();
    return(t.getLast());
    }
    public A next(Comparable<? super A> x) {
    if(x==null)
    return(first());
    s.registerReader();
    TraverseActionPerformer.LogicalJunctor<A> t =
    new TraverseActionPerformer.LogicalJunctor<A>(TraverseActionPerformer.LOGICAL_OR,
    new Selector.HalfRangeSelector(x,false,Selector.RIGHT,(Comparator<A>)null),f);
    try { s.ptraverse(s.root,IN_ORDER,t,null); }
    catch(TraverseActionPerformer.TraversalAbortedException e) {}
    s.unregisterReader();
    return(t.getLast());
    }
    public A prev(A x) {
    if(x==null)
    return(first());
    s.writeLock();
    TraverseActionPerformer.LogicalJunctor<A> t =
    new TraverseActionPerformer.LogicalJunctor<A>(TraverseActionPerformer.LOGICAL_OR,
    new Selector.HalfRangeSelector(x,false,Selector.RIGHT,s.comparator()),f);
    s.ordering = !s.ordering;
    try { s.ptraverse(s.root,IN_ORDER,t,null); }
    catch(TraverseActionPerformer.TraversalAbortedException e) {}
    s.ordering = !s.ordering;
    s.writeUnlock();
    return(t.getLast());
    }
    public A prev(Comparable<? super A> x) {
    if(x==null)
    return(first());
    s.writeLock();
    TraverseActionPerformer.LogicalJunctor<A> t =
    new TraverseActionPerformer.LogicalJunctor<A>(TraverseActionPerformer.LOGICAL_OR,
    new Selector.HalfRangeSelector(x,false,Selector.RIGHT,(Comparator<A>)null),f);
    s.ordering = !s.ordering;
    try { s.ptraverse(s.root,IN_ORDER,t,null); }
    catch(TraverseActionPerformer.TraversalAbortedException e) {}
    s.ordering = !s.ordering;
    s.writeUnlock();
    return(t.getLast());
    }
    public A floor(A x) {
    if(x==null)
    return(null);
    s.writeLock();
    TraverseActionPerformer.LogicalJunctor<A> t =
    new TraverseActionPerformer.LogicalJunctor<A>(TraverseActionPerformer.LOGICAL_OR,
    new Selector.HalfRangeSelector(x,true,Selector.LEFT,s.comparator()),f);
    s.ordering = !s.ordering;
    try { s.ptraverse(s.root,IN_ORDER,t,null); }
    catch(TraverseActionPerformer.TraversalAbortedException e) {}
    s.ordering = !s.ordering;
    s.writeUnlock();
    return(t.getLast());
    }
    public A floor(Comparable<? super A> x) {
    if(x==null)
    return(null);
    s.writeLock();
    TraverseActionPerformer.LogicalJunctor<A> t =
    new TraverseActionPerformer.LogicalJunctor<A>(TraverseActionPerformer.LOGICAL_OR,
    new Selector.HalfRangeSelector(x,true,Selector.LEFT,(Comparator<A>)null),f);
    s.ordering = !s.ordering;
    try { s.ptraverse(s.root,IN_ORDER,t,null); }
    catch(TraverseActionPerformer.TraversalAbortedException e) {}
    s.ordering = !s.ordering;
    s.writeUnlock();
    return(t.getLast());
    }
    public A ceiling(A x) {
    if(x==null)
    return(null);
    s.registerReader();
    TraverseActionPerformer.LogicalJunctor<A> t =
    new TraverseActionPerformer.LogicalJunctor<A>(TraverseActionPerformer.LOGICAL_OR,
    new Selector.HalfRangeSelector(x,true,Selector.RIGHT,s.comparator()),f);
    try { s.ptraverse(s.root,IN_ORDER,t,null); }
    catch(TraverseActionPerformer.TraversalAbortedException e) {}
    s.unregisterReader();
    return(t.getLast());
    }
    public A ceiling(Comparable<? super A> x) {
    if(x==null)
    return(null);
    s.registerReader();
    TraverseActionPerformer.LogicalJunctor<A> t =
    new TraverseActionPerformer.LogicalJunctor<A>(TraverseActionPerformer.LOGICAL_OR,
    new Selector.HalfRangeSelector(x,true,Selector.RIGHT,(Comparator<A>)null),f);
    try { s.ptraverse(s.root,IN_ORDER,t,null); }
    catch(TraverseActionPerformer.TraversalAbortedException e) {}
    s.unregisterReader();
    return(t.getLast());
    }
    public boolean subSetOf(Collection<?> c) {
    if(c==null)
    return(isEmpty());
    TraverseActionPerformer.LogicalJunctor<A> t =
    new TraverseActionPerformer.LogicalJunctor<A>(TraverseActionPerformer.LOGICAL_AND,
    (c instanceof OrderedSet?(Selector<A>)c:new Selector.ContainmentChecker<A>(c)),
    f);
    s.traverse(BBTree.IN_ORDER,t);
    return(t.getState());
    }
    public boolean superSetOf(Collection<?> c) {
    return(s.containsAll(c,f));
    }
    public OrderedSet<A> join(Collection<? extends A> c) {
    BBTree<A> r = this.clone();
    r.addAll(c);
    return(r);
    }
    public OrderedSet<A> intersect(Collection<? super A> c) {
    Iterator<A> it = s.subSetIterator(new Selector.Conjunctor(f,
    (c instanceof OrderedSet)?
    (OrderedSet<A>)c:new Selector.ContainmentChecker(c)));
    BBTreeMR<A> r = new BBTreeMR(s.comparator(),s.getMerger());
    r.setOrdering(s.getOrdering());
    while(it.hasNext())
    r.add(it.next());
    hdts
    return(r);
    }
    public OrderedSet<A> without(Collection<? super A> c) {
    synchronized(s) {
    BBTree<A> r = new BBTree(s.comparator(),s.getMerger());
    r.setOrdering(s.getOrdering());
    r.insertAll(s,new Selector.Conjunctor(f,
    new Selector.Inverter<A>((c instanceof OrderedSet)?
    (OrderedSet<A>)c:new Selector.ContainmentChecker(c))),false,null);
    return(r);
    }
    }
    public OrderedSet<A> reorder(Comparator<? super A> c,boolean make_new) {
    synchronized(s) {
    if(make_new) {
    BBTree<A> r = new BBTree(c,s.getMerger());
    r.setOrdering(s.getOrdering());
    r.insertAll(s,f,false,null);
    return(r);
    }
    return(s.reorder(c,false));
    }
    }
    public Object[] toArray() {
    TraverseActionPerformer.ArrayBuilder<A> a = new TraverseActionPerformer.ArrayBuilder<A>(size(),f);
    s.traverse(IN_ORDER,a);
    return(a.toArray());
    }
    public <B> B[] toArray(B[] b) {
    synchronized(s) {
    int count = size();
    TraverseActionPerformer.ArrayBuilder<A> a = null;
    if(b.length>=count)
    a = new TraverseActionPerformer.ArrayBuilder<A>(b,f);
    else
    a = new TraverseActionPerformer.ArrayBuilder<A>(
    (B[])java.lang.reflect.Array.newInstance(b.getClass().getComponentType(),count),f);
    s.traverse(IN_ORDER,a);
    return((B[])a.toArray());
    }
    }
    public boolean isFinal() {
    return(s.isFinal());
    }
    public Final<A> toFinal() {
    return(new Final(this));
    }
    public boolean getOrdering() {
    return(s.getOrdering());
    }
    public boolean toggleOrdering() {
    return(s.toggleOrdering());
    }
    public boolean setOrdering(boolean new_order) {
    return(s.setOrdering(new_order));
    }
    public Merger<A> getMerger() {
    return(s.getMerger());
    }
    public Merger<A> setMerger(Merger<A> m) {
    return(s.setMerger(m));
    }
    public OrderedSet<A> copySubSet(Selector<? super A> g) {
    return(s.copySubSet(new Selector.Conjunctor(f,g)));
    }
    public OrderedSet<A> copySubSet(A fromElement,A toElement) {
    return(s.copySubSet(new Selector.Conjunctor<A>(f,
    new RangeSelector<A>(fromElement,toElement,
    true,false,(SortedSet<A>)s))));
    }
    public OrderedSet<A> copySubSet(A min,A max,boolean min_included,boolean max_included) {
    return(s.copySubSet(new Selector.Conjunctor<A>(f,
    new RangeSelector<A>(min,max,
    min_included,max_included,(SortedSet<A>)s))));
    }
    public OrderedSet<A> copyHeadSet(A toElement) {
    return(s.copySubSet(new Selector.Conjunctor<A>(f,
    new Selector.HalfRangeSelector<A>(toElement,false,
    Selector.LEFT,(SortedSet<A>)s))));
    }
    public OrderedSet<A> copyHeadSet(A max,boolean max_included) {
    return(s.copySubSet(new Selector.Conjunctor<A>(f,
    new Selector.HalfRangeSelector<A>(max,max_included,
    Selector.LEFT,(SortedSet<A>)s))));
    }
    public OrderedSet<A> copyTailSet(A fromElement) {
    return(s.copySubSet(new Selector.Conjunctor<A>(f,
    new Selector.HalfRangeSelector<A>(fromElement,true,
    Selector.RIGHT,(SortedSet<A>)s))));
    }
    public OrderedSet<A> copyTailSet(A min,boolean min_included) {
    return(s.copySubSet(new Selector.Conjunctor<A>(f,
    new Selector.HalfRangeSelector<A>(min,min_included,
    Selector.RIGHT,(SortedSet<A>)s))));
    }
    public OrderedSet<A> subSet(Selector<? super A> g) {
    return(s.subSet(new Selector.Conjunctor<A>(f,g)));
    }
    public OrderedSet<A> subSet(A fromElement,A toElement) {
    return(s.subSet(new Selector.Conjunctor<A>(f,
    new Selector.RangeSelector<A>(fromElement,toElement,true,
    false,(SortedSet<A>)s))));
    }
    public OrderedSet<A> subSet(A min,A max,boolean min_included,boolean max_included){
    return(s.subSet(new Selector.Conjunctor<A>(f,
    new Selector.RangeSelector<A>(min,max,min_included,
    max_included,(SortedSet<A>)s))));
    }
    public OrderedSet<A> headSet(A toElement) {
    return(s.subSet(new Selector.Conjunctor<A>(f,
    new Selector.HalfRangeSelector<A>(toElement,false,
    Selector.LEFT,(SortedSet<A>)s))));
    }
    public OrderedSet<A> headSet(A max,boolean max_included) {
    return(s.subSet(new Selector.Conjunctor<A>(f,
    new Selector.HalfRangeSelector<A>(max,max_included,
    Selector.LEFT,(SortedSet<A>)s))));
    }
    public OrderedSet<A> tailSet(A fromElement) {
    return(s.subSet(new Selector.Conjunctor<A>(f,
    new Selector.HalfRangeSelector<A>(fromElement,true,
    Selector.RIGHT,(SortedSet<A>)s))));
    }
    public OrderedSet<A> tailSet(A min,boolean min_included) {
    return(s.subSet(new Selector.Conjunctor<A>(f,
    new Selector.HalfRangeSelector<A>(min,min_included,
    Selector.RIGHT,(SortedSet<A>)s))));
    }
    public Iterator<A> iterator() {
    return(s.subSetIterator(f));
    }
    public Iterator<A> subSetIterator(Selector<? super A> g) {
    return(s.subSetIterator(new Selector.Conjunctor<A>(f,g)));
    }
    public Iterator<A> subSetIterator(A fromElement,A toElement) {
    return(s.subSetIterator(new Selector.Conjunctor<A>(f,
    new Selector.RangeSelector<A>(fromElement,toElement,true,
    false,(SortedSet<A>)s))));
    }
    public Iterator<A> subSetIterator(A min,A max,boolean min_included,boolean max_included) {
    return(s.subSetIterator(new Selector.Conjunctor<A>(f,
    new Selector.RangeSelector<A>(min,max,min_included,max_included,
    (SortedSet<A>)s))));
    }
    public Iterator<A> headSetIterator(A toElement) {
    return(s.subSetIterator(new Selector.Conjunctor<A>(f,
    new Selector.HalfRangeSelector<A>(toElement,false,
    Selector.LEFT,(SortedSet<A>)s))));
    }
    public Iterator<A> headSetIterator(A max,boolean max_included){
    return(s.subSetIterator(new Selector.Conjunctor<A>(f,
    new Selector.HalfRangeSelector<A>(max,max_included,
    Selector.LEFT,(SortedSet<A>)s))));
    }
    public Iterator<A> tailSetIterator(A fromElement) {
    return(s.subSetIterator(new Selector.Conjunctor<A>(f,
    new Selector.HalfRangeSelector<A>(fromElement,true,
    Selector.RIGHT,(SortedSet<A>)s))));
    }
    public Iterator<A> tailSetIterator(A min,boolean min_included) {
    return(s.subSetIterator(new Selector.Conjunctor<A>(f,
    new Selector.HalfRangeSelector<A>(min,min_included,
    Selector.RIGHT,(SortedSet<A>)s))));
    }
    public BBTree<A> clone() {
    synchronized(s) {
    BBTree<A> r = new BBTree(s.comparator(),s.getMerger());
    r.setOrdering(s.getOrdering());
    r.insertAll(s,f,false,null);
    return(r);
    }
    }
    public boolean equals(Object o) {
    if(this==o)
    return(true);
    if((o!=null)&&(o instanceof Set)) {
    synchronized(s) {
    return(this.containsAll((Set)o)&&((Set)o).containsAll(this));
    }
    } else
    return(false);
    }
    public int compareTo(OrderedSet<A> b) {
    if(b==null)
    return(1);
    if(this==b)
    return(0);
    int c = 0;
    synchronized(s) {
    A x = this.first(),y = b.first();
    c = (x==null)?((y==null)?0:-1):((y==null)?1:s.default_comparator.compare(x,y));
    while((c==0)&&(x!=null)&&(y!=null)) {
    x = this.next(x);
    y = b.next(y);
    c = (x==null)?((y==null)?0:-1):((y==null)?1:s.default_comparator.compare(x,y));
    }
    return(c);
    }
    }
    public int hashCode() {
    TraverseActionPerformer.Hasher<A> h = new TraverseActionPerformer.Hasher<A>(f);
    s.traverse(IN_ORDER,h);
    return(h.getHash());
    }
    public String toString() {
    TraverseActionPerformer.RepresentationStringBuilder<A> b =
    new TraverseActionPerformer.RepresentationStringBuilder<A>(null,null,null,f);
    s.traverse(IN_ORDER,b);
    return(b.toString());
    }
    public void writeToStream(Writer w) throws IOException {
    if(w==null)
    return;
    w.write('{');
    TraverseActionPerformer.RepresentationWriter<A> v =
    new TraverseActionPerformer.RepresentationWriter<A>(w,f);
    s.traverse(IN_ORDER,v);
    w.write('}');
    }
    public int compare(A x,A y) {
    return(s.compare(x,y));
    }
    public Comparator<? super A> comparator() {
    return(s.comparator());
    }
    public boolean selects(A x) {
    return(f.selects(x)&&s.selects(x));
    }
    }
     */

    public int emptyTo(TraversalActionPerformer<? super A> p) {
        return emptyTo(p, null);
    }

    public int emptyTo(TraversalActionPerformer<? super A> p, Object o) {
        if(p == null)
            throw new NullPointerException("Performer is null");
        if(root == null)
            return 0;
        writeLock();
        int h = ptraverse(root, IN_ORDER, p, o, null, null, false, null, false);
        clear();
        writeUnlock();
        return h;
    }

    private void readObject(java.io.ObjectInputStream s)
            throws java.io.IOException, ClassNotFoundException {
        writeLock();
        s.defaultReadObject();
        this.root = null;
        int remain = this.count;
        while(remain-- > 0)
            add((A)s.readObject());
        writeUnlock();
    }

    private synchronized void writeObject(java.io.ObjectOutputStream s)
            throws java.io.IOException, ClassNotFoundException {
        registerReader();
        s.defaultWriteObject();
        try {
            ptraverse(root, IN_ORDER, TraversalActionPerformer.SERIALIZER, s,
                      null, null, true, null, true);
        } finally {
            unregisterReader();
        }
    }
    private static final long serialVersionUID = 0;
}
