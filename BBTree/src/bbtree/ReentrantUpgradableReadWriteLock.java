/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package bbtree;

/**
 *
 * @author RJ
 */
public class ReentrantUpgradableReadWriteLock {
    private static class Int {
        public volatile int val = 0;
    }

    private volatile int cr     = 0;
    private final Int cw        = new Int();
    private volatile int ww     = 0;
    private volatile Thread cwt = null;
    private static final class ThreadReadEntry implements Comparable<ThreadReadEntry> {
        public volatile int readLocks = 0;
        public final Thread t;
        public ThreadReadEntry() {
            this(Thread.currentThread());
        }
        public ThreadReadEntry(Thread t) {
            this.t = t;
        }
        public int compareTo(ThreadReadEntry x) {
            if(x==null)
                return(1);
            if(this==x)
                return(0);
            if(this.t==x.t)
                return(0);
            if(this.t==null)
                return(-1);
            long c = this.t.getId()-x.t.getId();
            if(c!=0)
                return(c>0?1:-1);
            String s1 = this.t.getName(),s2 = x.t.getName();
            if(s1==null)
                return(s2!=null?-1:0);
            if(s2==null)
                return(1);
            return(s1.compareTo(s2));
        }
    }
    private final OrderedSet<ThreadReadEntry> crt = new BBTreeMR<ThreadReadEntry>();

    public ReentrantUpgradableReadWriteLock() {}
    public void readLock() {
        ThreadReadEntry r = crt.get(new ThreadReadEntry());
        if((r!=null)&&(r.readLocks>0)) {
            r.readLocks++;
            cr++;
            return;
        }
        if(r!=null)
            System.err.println(getClass().getCanonicalName()+
                    ".readLock: ThreadReadEntry with non-positive "+
                    "readlocks-entry discovered("+r.readLocks+")");
        synchronized(cw) {
            if((cw.val>0)&&(cwt!=Thread.currentThread()))
                while((cw.val>0)||(ww>0)) {
                    try { cw.wait(); }
                    catch(InterruptedException e) {
                        System.out.println("\r\nThe thread "
                                +Thread.currentThread().getName()+" was interrupted:");
                        e.printStackTrace(System.out);
                    }
                }
            r = crt.addOrGet(new ThreadReadEntry());
            if(r.readLocks!=0)
                System.err.println(getClass().getCanonicalName()+
                        ".readLock: ThreadReadEntry should be new");
            r.readLocks = 1;
            cr++;
            if(cr<0)
                throw(new RuntimeException(getClass().getCanonicalName()
                        +".readLock: overflow occured, two many readers registered"));
        }
    }
    public void readUnlock() {
        ThreadReadEntry r = crt.get(new ThreadReadEntry());
        if((r==null)||(r.readLocks<=0))
            throw(new RuntimeException(getClass().getCanonicalName()
                    +".readUnock: the current thread "
                    +Thread.currentThread().getName()
                    +" does not own a read lock"));
        r.readLocks--;
        if(r.readLocks==0)
            crt.remove(r);
        cr--;
        if(cr<0)
            throw(new RuntimeException(getClass().getCanonicalName()
                    +".unregisterReader: reader registrations out of sync "
                    +"(caused by thread "+Thread.currentThread().getName()+")"));
        if(cr==0)
            synchronized(crt) {
                crt.notifyAll();
            }
    }
    public void writeLock() {
        synchronized(cw) {
            if((cw.val>0)&&(Thread.currentThread()==cwt)) {
                cw.val++;
                if(cw.val<0)
                    throw(new RuntimeException(getClass().getCanonicalName()
                            +".writeLock: overflow occured, too many write locks issued"));
                return;
            }
            if(cw.val<0)
                throw(new RuntimeException(getClass().getCanonicalName()
                    +".tryReadLock: negative write lock count"));
            if(cw.val>0) {
                ww++;
                if(ww<0)
                    throw(new RuntimeException(getClass().getCanonicalName()
                            +".writeLock: overflow occured, too many writers waiting"));
                while(cw.val>0) {
                    try { cw.wait(); }
                    catch(InterruptedException e) {
                        System.out.println("\r\nThe thread "
                                +Thread.currentThread().getName()+" was interrupted:");
                        e.printStackTrace(System.out);
                    }
                }
                ww--;
                if(ww<0)
                    throw(new RuntimeException(getClass().getCanonicalName()
                            +".writeLock: number of waiting writers is out of sync"));
            }
            if(cw.val<0)
                throw(new RuntimeException(getClass().getCanonicalName()
                    +".writeLock: negative write lock count"));
            cw.val = 1;
            cwt = Thread.currentThread();
        }
        ThreadReadEntry t = crt.get(new ThreadReadEntry());
        int ctrl = t!=null?t.readLocks:0;
        if(cr<ctrl)
            throw(new RuntimeException(getClass().getCanonicalName()
                +".upgradeLock: cr is "+cr+", should be >="+ctrl));
        if(cr>ctrl) {
            cr -= ctrl;
            synchronized(crt) {
                while(cr>0) {
                    try { crt.wait(); }
                    catch(InterruptedException e) {
                        System.out.println("\r\nThe thread "
                                +Thread.currentThread().getName()+" was interrupted:");
                        e.printStackTrace(System.out);
                    }
                }
            }
            cr += ctrl;
        }
    }
    public void writeUnlock() {
        synchronized(cw) {
            if((cw.val==0)||(Thread.currentThread()!=cwt))
                throw(new RuntimeException(getClass().getCanonicalName()
                        +".writeUnlock: the current thread "
                        +Thread.currentThread().getName()
                        +" does not own the write lock"));
            cw.val--;
            if(cw.val==0) {
                cwt = null;
                cw.notifyAll();
            }
        }
    }
    public boolean hasWriteLock() {
        synchronized(cw) {
            return((cw.val>0)&&(Thread.currentThread()==cwt));
        }
    }
    public boolean isWriteLocked() {
        return(cw.val>0);
    }
    public int writeLockCount() {
        return(cw.val);
    }
    public int writersWating() {
        return(ww);
    }
    public int totalReadLockCount() {
        return(cr);
    }
    public boolean hasReadLock() {
        return(readLockCount()>0);
    }
    public int readLockCount() {
        ThreadReadEntry r = crt.get(new ThreadReadEntry());
        if(r==null)
            return(0);
        if(r.readLocks<=0) {
            System.err.println(getClass().getCanonicalName()+
                    ".hasReadLock: ThreadReadEntry with non-positive "+
                    "readlocks-entry discovered, removing");
            crt.remove(r);
            return(0);
        }
        return(r.readLocks);
    }
    public boolean isReadLocked() {
        return(cr>0);
    }
    public boolean upgradeLock() {      //tries to immediately acquire a write lock
        boolean success = false;        //releases one read lock and returns true on success
        synchronized(cw) {              //returns false otherwise
            if(cw.val==0) {
                cw.val = 1;
                cwt = Thread.currentThread();
                success = true;
            } else if(cw.val<0)
                throw(new RuntimeException(getClass().getCanonicalName()
                    +".upgradeLock: negative write lock count"));
            else if(Thread.currentThread()==cwt) {
                cw.val++;
                success = true;
            }
        }
        if(success) {
            ThreadReadEntry t = crt.get(new ThreadReadEntry());
            if(t==null)
                throw(new RuntimeException(getClass().getCanonicalName()
                    +".upgradeLock: the calling thread does not own a read lock"));
            if(t.readLocks<=0)
                throw(new RuntimeException(getClass().getCanonicalName()
                    +".upgradeLock: non-positive entry in reader entry => "+
                    "the calling thread does not own a read lock"));
            t.readLocks--;
            cr--;
            int ctrl = t.readLocks;
            if(ctrl==0)
                crt.remove(t);
            if(cr<ctrl)
                throw(new RuntimeException(getClass().getCanonicalName()
                    +".upgradeLock: cr is "+cr+", should be >="+ctrl));
            if(cr>ctrl) {
                cr -= ctrl;
                synchronized(crt) {
                    while(cr>0) {
                        try { crt.wait(); }
                        catch(InterruptedException e) {
                            System.out.println("\r\nThe thread "
                                    +Thread.currentThread().getName()+" was interrupted:");
                            e.printStackTrace(System.out);
                        }
                    }
                }
                cr += ctrl;
            }
        }
        return(success);
    }
    public void downgradeLock() {                           //releases one write lock
        synchronized(cw) {                                  //and immediately acquires
            if((cw.val==0)||(Thread.currentThread()!=cwt))  //one read lock
                throw(new RuntimeException(getClass().getCanonicalName()
                        +".writeUnlock: the current thread "
                        +Thread.currentThread().getName()
                        +" does not own the write lock"));
            else if(cw.val<0)
                throw(new RuntimeException(getClass().getCanonicalName()
                    +".downgradeLock: negative write lock count"));
            cw.val--;
            ThreadReadEntry t = crt.addOrGet(new ThreadReadEntry());
            t.readLocks++;
            if(t.readLocks<0)
                throw(new RuntimeException(getClass().getCanonicalName()
                        +".downgradeLock: overflow occured, too many read locks issued"));
            cr++;
            if(cr<0)
                throw(new RuntimeException(getClass().getCanonicalName()
                        +".downgradeLock: overflow occured, too many read locks issued"));
            if(cw.val==0) {
                cwt = null;
                cw.notifyAll();
            }
        }
    }
    public boolean tryReadLock() {
        ThreadReadEntry r = crt.get(new ThreadReadEntry());
        if((r!=null)&&(r.readLocks>0)) {
            r.readLocks++;
            cr++;
            return(true);
        }
        if(r!=null) {
            System.err.println(getClass().getCanonicalName()+
                    ".tryReadLock: ThreadReadEntry with non-positive "+
                    "readlocks-entry discovered, removing");
            crt.remove(r);
        }
        synchronized(cw) {
            if(((cw.val==0)&&(ww==0))||((cw.val>0)&&(Thread.currentThread()==cwt))) {
                synchronized(crt) {
                    ThreadReadEntry t = crt.addOrGet(new ThreadReadEntry());
                    if(t.readLocks!=0)
                        throw(new RuntimeException(getClass().getCanonicalName()
                                +".tryReadLock: a strange error occured"));
                    t.readLocks++;
                    cr++;
                    return(true);
                }
            } else if(cw.val<0)
                throw(new RuntimeException(getClass().getCanonicalName()
                    +".tryReadLock: negative write lock count"));
        }
        return(false);
    }
    public boolean tryWriteLock() {
        boolean success = false;
        synchronized(cw) {
            if(cw.val==0) {
                cw.val = 1;
                cwt = Thread.currentThread();
                success = true;
            } else if(cw.val<0)
                throw(new RuntimeException(getClass().getCanonicalName()
                    +".upgradeLock: negative write lock count"));
            else if(Thread.currentThread()==cwt) {
                cw.val++;
                success = true;
            }
        }
        if(success) {
            ThreadReadEntry t = crt.get(new ThreadReadEntry());
            int ctrl = 0;
            if(t!=null) {
                if(t.readLocks<=0)
                    throw(new RuntimeException(getClass().getCanonicalName()
                        +".upgradeLock: non-positive entry in reader entry => "+
                        "the calling thread does not own a read lock"));
                ctrl = t.readLocks;
            }
            if(cr<ctrl)
                throw(new RuntimeException(getClass().getCanonicalName()
                    +".upgradeLock: cr is "+cr+", should be >=0"));
            if(cr>ctrl) {
                cr -= ctrl;
                synchronized(crt) {
                    while(cr>0) {
                        try { crt.wait(); }
                        catch(InterruptedException e) {
                            System.out.println("\r\nThe thread "
                                    +Thread.currentThread().getName()+" was interrupted:");
                            e.printStackTrace(System.out);
                        }
                    }
                }
                cr += ctrl;
            }
        }
        return(success);
    }
}
