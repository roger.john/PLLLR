/*
 * Merger.java
 *
 * Created on 27. Juli 2006, 08:42
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package bbtree;

/**
 *
 * @author RJ
 */
public interface Merger<A> extends Cloneable {
    public A merge(A x,A y);
    public Merger<A> clone();
    public static final Merger Keeper = new Merger() {
        public Object merge(Object x,Object y) { return(x); }
        public Merger clone() { return(this); }
    };
    public static final Merger Replacer = new Merger() {
        public Object merge(Object x,Object y) { return(y); }
        public Merger clone() { return(this); }
    };
    
    public static class Setter<A> implements Merger<A> {
        public A old;
        public Setter() {
            old = null;
        }
        public Setter(A old) {
            this.old = old;
        }
        public A merge(A x,A y) { old = x; return(y); }
        public Setter<A> clone() { return(new Setter<A>(old)); }
    }
    public static class Getter<A> implements Merger<A> {
        public A result;
        public Getter() {
            result = null;
        }
        public Getter(A result) {
            this.result = result;
        }
        public A merge(A x,A y) {
            if(x==null)
                result = y;
            else
                result = x;
            return(result);
        }
        public Getter<A> clone() { return(new Getter<A>(result)); }
    }
    public static interface Set<A> extends Merger<java.util.Set<A>> {
        public static final Set<Object> Adder = new Set<Object>() {
            public java.util.Set merge(java.util.Set x, java.util.Set y) {
                x.addAll(y);
                return x;
            }
            public Merger<java.util.Set<Object>> clone() {
                return this;
            }
        };
        public static final Set<Object> Intersector = new Set<Object>() {
            public java.util.Set merge(java.util.Set x, java.util.Set y) {
                x.retainAll(y);
                return x;
            }
            public Merger<java.util.Set<Object>> clone() {
                return this;
            }
        };
        public static final Set<Object> Remover = new Set<Object>() {
            public java.util.Set merge(java.util.Set x, java.util.Set y) {
                x.removeAll(y);
                return x;
            };
            public Merger<java.util.Set<Object>> clone() {
                return this;
            };
        };
    }
    public static interface Wrapper<A> extends Merger<Value<A>> {
        public Merger<A> getMerger();
        public Merger<A> setMerger(Merger<A> merger);
        public static class Keeper<A> implements Wrapper<A> {
            public Merger<A> merger;
            public Keeper() {
                this(null);
            }
            public Keeper(Merger<A> merger) {
                this.merger = merger;
            }
            public Merger<A> getMerger() {
                return merger;
            }
            public Merger<A> setMerger(Merger<A> merger) {
                Merger<A> old = this.merger;
                this.merger = merger;
                return old;
            }
            public Value<A> merge(Value<A> x, Value<A> y) {
                x.setValue(merger.merge(x.getValue(), y.getValue()));
                return x;
            }
            public Merger<Value<A>> clone() {
                return new Keeper<A>(merger);
            }
        }
        public static class Replacer<A> implements Wrapper<A> {
            public Merger<A> merger;
            public Replacer() {
                this(null);
            }
            public Replacer(Merger<A> merger) {
                this.merger = merger;
            }
            public Merger<A> getMerger() {
                return merger;
            }
            public Merger<A> setMerger(Merger<A> merger) {
                Merger<A> old = this.merger;
                this.merger = merger;
                return old;
            }
            public Value<A> merge(Value<A> x, Value<A> y) {
                y.setValue(merger.merge(x.getValue(), y.getValue()));
                return y;
            }
            public Merger<Value<A>> clone() {
                return new Replacer<A>(merger);
            }
        }
    }
}
