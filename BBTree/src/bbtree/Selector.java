/*
 * Selector.java
 *
 * Created on 29. Januar 2007, 13:25
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package bbtree;
import java.util.Collection;
import java.util.Comparator;
import java.util.SortedSet;

/**
 *
 * @author RJ
 */

public interface Selector<A> {
    
    public static final boolean LEFT  = false;
    public static final boolean RIGHT = true;
    
    public boolean selects(A x);
    public A upperBound();
    public A lowerBound();

    public static class RangeSelector<A> implements Selector<A>, Comparator<A> {
        private A min,max;
        private Comparator<? super A> c;
        private SortedSet<? super A> s;
        private boolean min_included,max_included;
        public RangeSelector(A min,boolean min_included,
                            A max,boolean max_included,Comparator<? super A> c) {
            this.min = min;
            this.max = max;
            this.min_included = min_included;
            this.max_included = max_included;
            if(c!=null)
                this.c = c;
            else
                this.c = this;
        }
        public RangeSelector(A min,boolean min_included,
                            A max,boolean max_included,SortedSet<? super A> s) {
            this.min = min;
            this.max = max;
            this.min_included = min_included;
            this.max_included = max_included;
            if(s!=null)
                this.s = s;
            else
                this.c = this;
        }
        public synchronized boolean selects(A x) {
            Comparator<? super A> d = c;
            if((d==null)&&(s!=null))
                d = s.comparator();
            if(d==null)
                d = this;
            int c1 = d.compare(min,x),c2 = d.compare(max,x);
            return(((c1<0)&&(c2>0))||((c1==0)&&min_included)||((c2==0)&&max_included));
        }
        public A getMin() {
            return(min);
        }
        public A lowerBound() {
            return(min);
        }
        public A getMax() {
            return(max);
        }
        public A upperBound() {
            return(max);
        }
        public boolean minIncluded() {
            return(min_included);
        }
        public boolean maxIncluded() {
            return(min_included);
        }
        public synchronized A setMin(A min) {
            A h = this.min;
            this.min = min;
            return(h);
        }
        public synchronized A setMax(A max) {
            A h = this.max;
            this.max = max;
            return(h);
        }
        public synchronized boolean setMinIncluded(boolean min_included) {
            boolean h = this.min_included;
            this.min_included = min_included;
            return(h);
        }
        public synchronized boolean setMaxIncluded(boolean max_included) {
            boolean h = this.max_included;
            this.max_included = max_included;
            return(h);
        }
        public Comparator<? super A> getComparator() {
            if(c!=null)
                return(c);
            if(s!=null)
                return(s.comparator());
            return(this);
        }
        public Comparator<? super A> setComparator(Comparator<? super A> c) {
            Comparator<? super A> h = this.c;
            if(c!=null)
                this.c = c;
            else
                this.c = this;
            return(h);
        }
        public int compare(A x,A y) {
            return(((Comparable<A>)x).compareTo(y));
        }
    }
    public static class HalfRangeSelector<A> implements Selector<A>, Comparator<A> {
        private A border;
        private Comparator<? super A> c;
        private SortedSet<? super A> s;
        private boolean border_included,direction;
        public HalfRangeSelector(A border,boolean border_included,boolean direction,Comparator<? super A> c) {
            this.border = border;
            if(c!=null)
                this.c = c;
            else
                this.c = this;
            this.border_included = border_included;
            this.direction  = direction;
        }
        public HalfRangeSelector(A border,boolean border_included,boolean direction,SortedSet<? super A> s) {
            this.border = border;
            if(s!=null)
                this.s = s;
            else
                this.c = this;
            this.border_included = border_included;
            this.direction  = direction;
        }
        public synchronized boolean selects(A x) {
            Comparator<? super A> e = c;
            if((e==null)&&(s!=null))
                e = s.comparator();
            if(e==null)
                e = this;
            int d = e.compare(border,x);
            return((border_included&&(d==0))||(direction?d<0:d>0));
        }
        public A getBorder() {
            return(border);
        }
        public A upperBound() {
            if(direction)
                return(null);
            else
                return(border);
        }
        public A lowerBound() {
            if(direction)
                return(border);
            else
                return(null);
        }
        public synchronized boolean borderIncluded() {
            return(border_included);
        }
        public synchronized boolean direction() {
            return(direction);
        }
        public synchronized A setBorder(A border) {
            A h = this.border;
            this.border= border;
            return(h);
        }
       public synchronized boolean setBorderIncluded(boolean border_included) {
            boolean h = this.border_included;
            this.border_included = border_included;
            return(h);
        }
        public synchronized boolean setDirection(boolean direction) {
            boolean h = this.direction;
            this.direction = direction;
            return(h);
        }
        public Comparator<? super A> getComparator() {
            if(c!=null)
                return(c);
            if(s!=null)
                return(s.comparator());
            return(this);
        }
        public Comparator<? super A> setComparator(Comparator<? super A> c) {
            Comparator<? super A> h = this.c;
            if(c!=null)
                this.c = c;
            else
                this.c = this;
            return(h);
        }
        public int compare(A x,A y) {
            return(((Comparable<A>)x).compareTo(y));
        }
    }
    public static final Selector Accepter = new Selector() {
        public boolean selects(Object x) {
            return(true);
        }
        public Object upperBound() {
            return(null);
        }
        public Object lowerBound() {
            return(null);
        }
    };
    public static final Selector Rejecter = new Selector() {
        public boolean selects(Object x) {
            return(false);
        }
        public Object upperBound() {
            return(null);
        }
        public Object lowerBound() {
            return(null);
        }
    };
    public static class Inverter<A> implements Selector<A> {
        private Selector<A> op;
        public Inverter(Selector<A> op) {
            if(op!=null)
                this.op = op;
            else
                this.op = (Selector<A>)(Selector.Rejecter);
        }
        public boolean selects(A x) {
            return(!op.selects(x));
        }
        public Selector<? super A> getOperand() {
            return(this.op);
        }
        public synchronized Selector<? super A> setOperand(Selector<A> op) {
            Selector<? super A> h = this.op;
            if(op!=null)
                this.op = op;
            else
                this.op = (Selector<A>)(Selector.Rejecter);
            return(h);
        }
        public A upperBound() {
            return(op.lowerBound());
        }
        public A lowerBound() {
            return(op.upperBound());
        }
    }
    public static class Conjunctor<A> implements Selector<A> {
        private Selector<A> left_op,right_op;
        public Conjunctor(Selector<A> left_op,Selector<A> right_op) {
            if(left_op!=null)
                this.left_op = left_op;
            else
                this.left_op = Selector.Accepter;
            if(right_op!=null)
                this.right_op = right_op;
            else
                this.right_op = Selector.Accepter;
        }
        public synchronized boolean selects(A x) {
            return(left_op.selects(x)&&right_op.selects(x));
        }
        public Selector<? super A> getLeftOperand() {
            return(this.left_op);
        }
        public Selector<? super A> getRightOperand() {
            return(this.right_op);
        }
        public synchronized Selector<? super A> setLeftOperand(Selector<A> left_op) {
            Selector<? super A> h = this.left_op;
            if(left_op!=null)
                this.left_op = left_op;
            else
                this.left_op = Selector.Accepter;
            return(h);
        }
        public synchronized Selector<? super A> setRightOperand(Selector<A> right_op) {
            Selector<? super A> h = this.right_op;
            if(right_op!=null)
                this.right_op = right_op;
            else
                this.right_op = Selector.Accepter;
            return(h);
        }
        public A upperBound() {
            A l = left_op.upperBound(),r = right_op.upperBound();
            if(l==null)
                return(r);
            if(r==null)
                return(l);
            if(l instanceof Comparable)
                return(((Comparable<A>)l).compareTo(r)<=0?l:r);
            if(r instanceof Comparable)
                return(((Comparable<A>)r).compareTo(l)>=0?l:r);
            return(null);
        }
        public A lowerBound() {
            A l = left_op.lowerBound(),r = right_op.lowerBound();
            if(l==null)
                return(r);
            if(r==null)
                return(l);
            if(l instanceof Comparable)
                return(((Comparable<A>)l).compareTo(r)>=0?l:r);
            if(r instanceof Comparable)
                return(((Comparable<A>)r).compareTo(l)<=0?l:r);
            return(null);
        }
    }
    public static class Disjunctor<A> implements Selector<A> {
        private Selector<A> left_op,right_op;
        public Disjunctor(Selector<A> left_op,Selector<A> right_op) {
            if(left_op!=null)
                this.left_op = left_op;
            else
                this.left_op = Selector.Accepter;
            if(right_op!=null)
                this.right_op = right_op;
            else
                this.right_op = Selector.Accepter;
        }
        public synchronized boolean selects(A x) {
            return(left_op.selects(x)||right_op.selects(x));
        }
        public Selector<? super A> getLeftOperand() {
            return(this.left_op);
        }
        public Selector<? super A> getRightOperand() {
            return(this.right_op);
        }
        public synchronized Selector<A> setLeftOperand(Selector<A> left_op) {
            Selector<A> h = this.left_op;
            if(left_op!=null)
                this.left_op = left_op;
            else
                this.left_op = Selector.Accepter;
            return(h);
        }
        public synchronized Selector<? super A> setRightOperand(Selector<A> right_op) {
            Selector<A> h = this.right_op;
            if(right_op!=null)
                this.right_op = right_op;
            else
                this.right_op = Selector.Accepter;
            return(h);
        }
        public A upperBound() {
            A l = left_op.upperBound(),r = right_op.upperBound();
            if(l==null)
                return(null);
            if(r==null)
                return(null);
            if(l instanceof Comparable)
                return(((Comparable<A>)l).compareTo(r)>=0?l:r);
            if(r instanceof Comparable)
                return(((Comparable<A>)r).compareTo(l)<=0?l:r);
            return(null);
        }
        public A lowerBound() {
            A l = left_op.lowerBound(),r = right_op.lowerBound();
            if(l==null)
                return(null);
            if(r==null)
                return(null);
            if(l instanceof Comparable)
                return(((Comparable<A>)l).compareTo(r)<=0?l:r);
            if(r instanceof Comparable)
                return(((Comparable<A>)r).compareTo(l)>=0?l:r);
            return(null);
        }
    }
    public static class Exjunctor<A> implements Selector<A> {
        private Selector<A> left_op,right_op;
        public Exjunctor(Selector<A> left_op,Selector<A> right_op) {
            if(left_op!=null)
                this.left_op = left_op;
            else
                this.left_op = Selector.Accepter;
            if(right_op!=null)
                this.right_op = right_op;
            else
                this.right_op = Selector.Accepter;
        }
        public synchronized boolean selects(A x) {
            return(left_op.selects(x)^right_op.selects(x));
        }
        public Selector<A> getLeftOperand() {
            return(this.left_op);
        }
        public Selector<A> getRightOperand() {
            return(this.right_op);
        }
        public synchronized Selector<A> setLeftOperand(Selector<A> left_op) {
            Selector<A> h = this.left_op;
            if(left_op!=null)
                this.left_op = left_op;
            else
                this.left_op = Selector.Accepter;
            return(h);
        }
        public synchronized Selector<A> setRightOperand(Selector<A> right_op) {
            Selector<A> h = this.right_op;
            if(right_op!=null)
                this.right_op = right_op;
            else
                this.right_op = Selector.Accepter;
            return(h);
        }
        public A upperBound() {
            A l = left_op.upperBound(),r = right_op.upperBound();
            if(l==null)
                return(null);
            if(r==null)
                return(null);
            if(l instanceof Comparable)
                return(((Comparable<A>)l).compareTo(r)>=0?l:r);
            if(r instanceof Comparable)
                return(((Comparable<A>)r).compareTo(l)<=0?l:r);
            return(null);
        }
        public A lowerBound() {
            A l = left_op.lowerBound(),r = right_op.lowerBound();
            if(l==null)
                return(null);
            if(r==null)
                return(null);
            if(l instanceof Comparable)
                return(((Comparable<A>)l).compareTo(r)<=0?l:r);
            if(r instanceof Comparable)
                return(((Comparable<A>)r).compareTo(l)>=0?l:r);
            return(null);
        }
    }
    public static class ContainmentChecker<A> implements Selector<A> {
        private Collection<?> s;
        public ContainmentChecker(Collection<?> s) {
            this.s = s;
        }
        public boolean selects(A x) {
            return(s.contains(x));
        }
        public Collection<?> getOperand() {
            return(this.s);
        }
        public synchronized Collection<?> setOperand(Collection<?> s) {
            Collection<?> h = this.s;
            this.s = s;
            return(h);
        }
        public A upperBound() {
            if((s!=null)&&(s instanceof SortedSet))
                return(((SortedSet<A>)s).last());
            else
                return(null);
        }
        public A lowerBound() {
            if((s!=null)&&(s instanceof SortedSet))
                return(((SortedSet<A>)s).first());
            else
                return(null);
        }
    }
    public static class Reverser<A> implements Selector<A> {
        private Selector<A> op;
        public Reverser(Selector<A> op) {
            this.op = op;
        }
        public boolean selects(A x) {
            return(op.selects(x));
        }
        public Selector<A> getOperand() {
            return(this.op);
        }
        public Selector<A> setOperand(Selector<A> op) {
            Selector<A> h = this.op;
            this.op = op;
            return(h);
        }
        public A upperBound() {
            return(op.lowerBound());
        }
        public A lowerBound() {
            return(op.upperBound());
        }

    }
}
