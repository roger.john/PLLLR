/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package bbtree;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Spliterator;

/**
 *
 * @author rj
 */
public interface Traversable<A>  {
    //public constants for traversing, can be combined
    public static final int PRE_ORDER     = 1;
    public static final int IN_ORDER      = 2;
    public static final int POST_ORDER    = 4;
    public static final int REVERSE_ORDER = 8;

    public int traverse(int order,TraversalActionPerformer<? super A> p);
    public int traverse(int order,TraversalActionPerformer<? super A> p,
                    Object o);
    public int traverseRange(int order,TraversalActionPerformer<? super A> p,
                    A l,A r);
    public int traverseRange(int order,TraversalActionPerformer<? super A> p,
                    Object o,A l,A r);
    public int traverseRange(int order,TraversalActionPerformer<? super A> p,
                    A l,boolean left_included,A r,boolean right_included);
    public int traverseRange(int order,TraversalActionPerformer<? super A> p,
                    Object o,A l,boolean left_included,A r,boolean right_included);
//    public int traverseComparableRange(int order,TraversalActionPerformer<? super A> p,
//                    Comparable<? super A> l,Comparable<? super A> r);
//    public int traverseComparableRange(int order,TraversalActionPerformer<? super A> p,
//                    Object o,Comparable<? super A> l,Comparable<? super A> r);
//    public int traverseComparableRange(int order,TraversalActionPerformer<? super A> p,
//                    Comparable<? super A> l,boolean left_included,
//                            Comparable<? super A> r,boolean right_included);
//    public int traverseComparableRange(int order,TraversalActionPerformer<? super A> p,
//                    Object o,Comparable<? super A> l,boolean left_included,
//                            Comparable<? super A> r,boolean right_included);
//    public <B> int traverseMatchingRange(int order,TraversalActionPerformer<? super A> p,
//                    B l,B r);
//    public <B> int traverseMatchingRange(int order,TraversalActionPerformer<? super A> p,
//                    Object o,B l,B r);
//    public <B> int traverseMatchingRange(int order,TraversalActionPerformer<? super A> p,
//                    B l,boolean left_included,B r,boolean right_included);
//    public <B> int traverseMatchingRange(int order,TraversalActionPerformer<? super A> p,
//                    Object o,B l,boolean left_included,B r,boolean right_included);
    public int traverseSelected(int order,TraversalActionPerformer<? super A> p,
                    Selector<A> s);
    public int traverseSelected(int order,TraversalActionPerformer<? super A> p,
                    Object o,Selector<A> s);
    public int traverseSelectedRange(int order,TraversalActionPerformer<? super A> p,
                    Selector<A> s,A l,A r);
    public int traverseSelectedRange(int order,TraversalActionPerformer<? super A> p,
                    Object o,Selector<A> s,A l,A r);
    public int traverseSelectedRange(int order,TraversalActionPerformer<? super A> p,
                    Selector<A> s,A l,boolean left_included,A r,boolean right_included);
    public int traverseSelectedRange(int order,TraversalActionPerformer<? super A> p,
                    Object o,Selector<A> s,A l,boolean left_included,
                            A r,boolean right_included);
//    public int traverseSelectedComparableRange(int order,TraversalActionPerformer<? super A> p,
//                    Selector<A> s,Comparable<? super A> l,Comparable<? super A> r);
//    public int traverseSelectedComparableRange(int order,TraversalActionPerformer<? super A> p,
//                    Object o,Selector<A> s,Comparable<? super A> l,Comparable<? super A> r);
//    public int traverseSelectedComparableRange(int order,TraversalActionPerformer<? super A> p,
//                    Selector<A> s,Comparable<? super A> l,boolean left_included,
//                            Comparable<? super A> r,boolean right_included);
//    public int traverseSelectedComparableRange(int order,TraversalActionPerformer<? super A> p,
//                    Object o,Selector<A> s,Comparable<? super A> l,boolean left_included,
//                            Comparable<? super A> r,boolean right_included);
//    public <B> int traverseSelectedMatchingRange(int order,TraversalActionPerformer<? super A> p,
//                    Selector<A> s,B l,B r);
//    public <B> int traverseSelectedMatchingRange(int order,TraversalActionPerformer<? super A> p,
//                    Object o,Selector<A> s,B l,B r);
//    public <B> int traverseSelectedMatchingRange(int order,TraversalActionPerformer<? super A> p,
//                    Selector<A> s,B l,boolean left_included,B r,boolean right_included);
//    public <B> int traverseSelectedMatchingRange(int order,TraversalActionPerformer<? super A> p,
//                    Object o,Selector<A> s,B l,boolean left_included,B r,boolean right_included);
    public int emptyTo(TraversalActionPerformer<? super A> p);
    public int emptyTo(TraversalActionPerformer<? super A> p,Object o);
    public static interface Set<A> extends OrderedSet<A>, Traversable<A> {
        public Set<A> clone();
        public Set<A> join(Collection<? extends A> s);
        public Set<A> intersect(Collection<? super A> s);
        public Set<A> without(Collection<? super A> s);
        public Set<A> reorder(Comparator<? super A> c,boolean make_new);
        public Set<A> reorder(Comparator<? super A> c,Merger<A> m,boolean make_new);

        public Set<A> copySubSet(Selector<A> f);
        public Set<A> copySubSet(A fromElement,A toElement);
        public Set<A> copySubSet(A min,boolean min_included,A max,boolean max_included);
        public Set<A> copyHeadSet(A toElement);
        public Set<A> copyHeadSet(A max,boolean max_included);
        public Set<A> copyTailSet(A fromElement);
        public Set<A> copyTailSet(A min,boolean min_included);

        public Set<A> descendingSet();
        public Set<A> subSet(Selector<A> f);
        public Set<A> descendingSubSet(Selector<A> f);
        public Set<A> subSet(A fromElement,A toElement);
        public Set<A> descendingSubSet(A fromElement,A toElement);
        public Set<A> subSet(A min,boolean min_included,A max,boolean max_included);
        public Set<A> descendingSubSet(A min,boolean min_included,A max,boolean max_included);
        public Set<A> headSet(A toElement);
        public Set<A> descendingHeadSet(A toElement);
        public Set<A> headSet(A max,boolean max_included);
        public Set<A> descendingHeadSet(A max,boolean max_included);
        public Set<A> tailSet(A fromElement);
        public Set<A> descendingTailSet(A fromElement);
        public Set<A> tailSet(A min,boolean min_included);
        public Set<A> descendingTailSet(A min,boolean min_included);
        public static interface Indexed<A> extends Traversable.Set<A>, List<A> {
            public void add(int i,A x);
            public boolean addAll(int i,Collection<? extends A> x);
            public A get(int i);
            public int indexOf(Object x);
            public int indexOfComparable(Comparable<? super A> x);
            public <B> int indexOfMatch(B x);
            public int indexOfHigher(A x);
            public int indexOfHigherComparable(Comparable<? super A> x);
            public <B> int indexOfHigherMatch(B x);
            public int indexOfLower(A x);
            public int indexOfLowerComparable(Comparable<? super A> x);
            public <B> int indexOfLowerMatch(B x);
            public int indexOfCeiled(A x);
            public int indexOfCeiledComparable(Comparable<? super A> x);
            public <B> int indexOfCeiledMatch(B x);
            public int indexOfFloored(A x);
            public int indexOfFlooredComparable(Comparable<? super A> x);
            public <B> int indexOfFlooredMatch(B x);
            public int lastIndexOf(Object x);
            public A remove(int i);
            public A set(int i,A x);
            public Indexed<A> subList(int from,int to);
            public ResettableListIterator<A> listIterator();
            public ResettableListIterator<A> listIterator(int from);
            public ResettableListIterator<A> subListIterator(int l,int r);
            public ResettableListIterator<A> subListIterator(int l,int from,int r);

            public Indexed<A> clone();
            public Indexed<A> join(Collection<? extends A> s);
            public Indexed<A> intersect(Collection<? super A> s);
            public Indexed<A> without(Collection<? super A> s);
            public Indexed<A> reorder(Comparator<? super A> c,boolean make_new);
            public Indexed<A> reorder(Comparator<? super A> c,Merger<A> m,boolean make_new);

            public Indexed<A> copySubSet(Selector<A> f);
            public Indexed<A> copySubSet(int from,int to);
            public Indexed<A> copySubSet(A fromElement,A toElement);
            public Indexed<A> copySubSet(A min,boolean min_included,A max,boolean max_included);
            public Indexed<A> copyHeadSet(A toElement);
            public Indexed<A> copyHeadSet(A max,boolean max_included);
            public Indexed<A> copyTailSet(A fromElement);
            public Indexed<A> copyTailSet(A min,boolean min_included);
            public int traverseRange(int order,TraversalActionPerformer<? super A> p,
                            int l,int r);
            public int traverseRange(int order,TraversalActionPerformer<? super A> p,
                            Object o,int l,int r);
            public int traverseSelectedRange(int order,TraversalActionPerformer<? super A> p,
                            Selector<A> s,int l,int r);
            public int traverseSelectedRange(int order,TraversalActionPerformer<? super A> p,
                            Object o,Selector<A> s,int l,int r);
			@Override
			default Spliterator<A> spliterator() {
				// TODO Auto-generated method stub
				return Set.super.spliterator();
			}
        }
    }
}
