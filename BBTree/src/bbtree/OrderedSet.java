/*
 * OrderedSet.java
 *
 * Created on 29. Januar 2007, 13:22
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */
package bbtree;

import bbtree.Utils.AsymmetricComparator;
import bbtree.Utils.KeyedElementFactory;
import java.io.*;
import java.util.*;

/**
 *
 * @author RJ
 */
public interface OrderedSet<A> extends Comparator<A>, AsymmetricComparator<A>, Selector<A>, Cloneable,
                                       NavigableSet<A>, Serializable, Writeable,
                                       Comparable<OrderedSet<A>> {

    //public constants for ordering
    public static final boolean ORDER_NORMAL = false;
    public static final boolean ORDER_REVERSED = true;

    public boolean contains(Object o);

    public boolean containsAll(Collection<?> c);

    public boolean isContainedIn(Collection<?> c);

    public A pollFirst();

    public A pollLast();

    public boolean add(A x);

    public boolean add(A x, Merger<A> m);

    public A addOrGet(A x);

    public <B> A addOrGetMatch(B x, KeyedElementFactory<A, B> f);

    public boolean addAll(Collection<? extends A> c);

    public boolean addAll(Collection<? extends A> c, Merger<A> m);

    public boolean addAll(Collection<? extends A> c, Selector<? super A> s,
                          boolean abort_on_illegal_argument);

    public boolean addAll(Collection<? extends A> c, Selector<? super A> s,
                          boolean abort_on_illegal_argument, Merger<A> m);

    public boolean remove(Object o);

    public A removeComparable(Comparable<? super A> x);

    public <B> A removeMatch(B x);

    public boolean removeAll(Collection<?> c);

    public boolean removeAll(Selector<? super A> s);

    public boolean retainAll(Collection<?> c);

    public boolean retainAll(Selector<? super A> s);

    public boolean isEmpty();

    public void clear();

    public int size();

    public int maxSearchDepth();

    public A get(A x);

    public A getComparable(Comparable<? super A> x);

    public <B> A getMatch(B x);

    public A set(A x);

    public A first();

    public A last();

    public A higher(A x);

    public A higherComparable(Comparable<? super A> x);

    public <B> A higherMatch(B x);

    public A lower(A x);

    public A lowerComparable(Comparable<? super A> x);

    public <B> A lowerMatch(B x);

    public A floor(A x);

    public A floorComparable(Comparable<? super A> x);

    public <B> A floorMatch(B x);

    public A ceiling(A x);

    public A ceilComparable(Comparable<? super A> x);

    public <B> A ceilMatch(B x);

    public boolean subSetOf(Collection<?> s);

    public boolean superSetOf(Collection<?> s);

    public boolean isFinal();

    public Final<A> toFinal();

    public Object[] toArray();

    public <B> B[] toArray(B[] b);

    public <B> B[] toArray(Class<B> componentType);

    public OrderedSet<A> clone();

    public OrderedSet<A> join(Collection<? extends A> s);

    public OrderedSet<A> intersect(Collection<? super A> s);

    public OrderedSet<A> without(Collection<? super A> s);

    public OrderedSet<A> reorder(Comparator<? super A> c, boolean make_new);

    public OrderedSet<A> reorder(Comparator<? super A> c, Merger<A> m,
                                 boolean make_new);

    public OrderedSet<A> copySubSet(Selector<A> f);

    public OrderedSet<A> copySubSet(A fromElement, A toElement);

    public OrderedSet<A> copySubSet(A min, boolean min_included, A max,
                                    boolean max_included);

    public OrderedSet<A> copyHeadSet(A toElement);

    public OrderedSet<A> copyHeadSet(A max, boolean max_included);

    public OrderedSet<A> copyTailSet(A fromElement);

    public OrderedSet<A> copyTailSet(A min, boolean min_included);

    public OrderedSet<A> descendingSet();

    public OrderedSet<A> subSet(Selector<A> f);

    public OrderedSet<A> descendingSubSet(Selector<A> f);

    public OrderedSet<A> subSet(A fromElement, A toElement);

    public OrderedSet<A> descendingSubSet(A fromElement, A toElement);

    public OrderedSet<A> subSet(A min, boolean min_included, A max,
                                boolean max_included);

    public OrderedSet<A> descendingSubSet(A min, boolean min_included, A max,
                                          boolean max_included);

    public OrderedSet<A> headSet(A toElement);

    public OrderedSet<A> descendingHeadSet(A toElement);

    public OrderedSet<A> headSet(A max, boolean max_included);

    public OrderedSet<A> descendingHeadSet(A max, boolean max_included);

    public OrderedSet<A> tailSet(A fromElement);

    public OrderedSet<A> descendingTailSet(A fromElement);

    public OrderedSet<A> tailSet(A min, boolean min_included);

    public OrderedSet<A> descendingTailSet(A min, boolean min_included);

    public ResettableIterator<A> iterator();

    public ResettableIterator<A> descendingIterator();

    public ResettableIterator<A> subSetIterator(Selector<A> f);

    public ResettableIterator<A> descendingSubSetIterator(Selector<A> f);

    public ResettableIterator<A> subSetIterator(A fromElement, A toElement);

    public ResettableIterator<A> descendingSubSetIterator(A fromElement,
                                                          A toElement);

    public ResettableIterator<A> subSetIterator(A min, boolean min_included,
                                                A max, boolean max_included);

    public ResettableIterator<A> descendingSubSetIterator(A min,
                                                          boolean min_included,
                                                          A max,
                                                          boolean max_included);

    public ResettableIterator<A> headSetIterator(A toElement);

    public ResettableIterator<A> descendingHeadSetIterator(A toElement);

    public ResettableIterator<A> headSetIterator(A max, boolean max_included);

    public ResettableIterator<A> descendingHeadSetIterator(A max,
                                                           boolean max_included);

    public ResettableIterator<A> tailSetIterator(A fromElement);

    public ResettableIterator<A> descendingTailSetIterator(A fromElement);

    public ResettableIterator<A> tailSetIterator(A min, boolean min_included);

    public ResettableIterator<A> descendingTailSetIterator(A min,
                                                           boolean min_included);

    public boolean equals(Object o);

    public int compareTo(OrderedSet<A> b);

    public int compareTo(OrderedSet<A> b, Comparator<? super A> c);

    public int hashCode();

    public String toString();

    public int compare(A x, A y);

    public Comparator<? super A> comparator();

    public boolean selects(A x);

    public void writeToStream(Writer w) throws IOException;

//    public static class IteratorStub<A> implements Iterator<A> {
//        Iterator<A> it;
//        Selector<? super A> f;
//        A next;
//        public IteratorStub(Iterator<A> it) {
//            this(it,null);
//        }
//        public IteratorStub(Iterator<A> it,Selector<? super A> f) {
//            this.it = it;
//            if(f!=null)
//                this.f = f;
//            else
//                this.f = Selector.Accepter;
//            next();
//        }
//        public boolean hasNext() {
//            return(next!=null);
//        }
//        public A next() {
//            if(next!=null)
//                throw(new java.util.NoSuchElementException("bbtree.OrderedSet.IteratorStub.next: no more elements to iterate"));
//            A h = next;
//            while(it.hasNext()&&!f.selects(next = it.next())) {}
//            if((next!=null)&&!f.selects(next))
//                next = null;
//            return(h);
//        }
//        public void remove() {
//            throw(new UnsupportedOperationException("bbtree.OrderedSet.IteratorStub.remove: removal not supported"));
//        }
//    }
}
