/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package bbtree;

import java.util.ListIterator;
import java.util.NoSuchElementException;

/**
 *
 * @author rj
 */
public interface ResettableListIterator<A> extends ResettableIterator<A>, ListIterator<A> {
    public void reset();
    public Traversable.Set.Indexed<A> iteratedSet();
    public ResettableIterator<A> iterator();
    public boolean hasNext();
    public boolean hasPrevious();
    public A next();
    public A previous();
    public int nextIndex();
    public int previousIndex();
    public void add(A x);
    public void remove();
    public void set(A x);

    public static class ArrayIterator<A> implements ResettableListIterator<A> {
        private final Object[] a;
        private final Selector<A> s;
        private final Traversable.Set.Indexed<A> b;
        private final int l,r,from;
        private final boolean reverseOrder;
        private int in,ip;
        public ArrayIterator(Object[] a) {
            this(a,null,false);
        }
        public ArrayIterator(Object[] a,Selector<A> s) {
            this(a,s,false);
        }
        public ArrayIterator(Object[] a,boolean reverseOrder) {
            this(a,null,reverseOrder);
        }
        public ArrayIterator(Object[] a,Selector<A> s,boolean reverseOrder) {
            this.a = a;
            this.b = null;
            this.reverseOrder = reverseOrder;
            if(a==null) {
                l = 0;
                r = 0;
            } else {
                l = 0;
                r = a.length;
            }
            if(s==null)
                this.s = Selector.Accepter;
            else
                this.s = s;
            this.from = -1;
            reset();
        }
        public ArrayIterator(Object[] a,int from) {
            this(a,from,0,a!=null?a.length:0,false);
        }
        public ArrayIterator(Object[] a,int from,boolean reverseOrder) {
            this(a,from,0,a!=null?a.length:0,reverseOrder);
        }
        public ArrayIterator(Object[] a,int l,int r) {
            this(a,l,l,r,false);
        }
        public ArrayIterator(Object[] a,int l,int from,int r) {
            this(a,from,l,r,false);
        }
        public ArrayIterator(Object[] a,int l,int from,int r,boolean reverseOrder) {
            this.a = a;
            this.b = null;
            this.reverseOrder = reverseOrder;
            int la = 0;
            if(a!=null)
                la = a.length;
            if((l<0)||(r>la)||(l>r))
                throw(new IndexOutOfBoundsException("Indices are out of bounds"));
            if((from<0)||(from>r))
                throw(new IndexOutOfBoundsException("From-index is out of bounds"));
            this.l = l;
            this.r = r;
            this.from = from;
            this.s = Selector.Accepter;
            reset();
        }
        public ArrayIterator(Final<A> f) {
            this(f,null,false);
        }
        public ArrayIterator(Final<A> f,boolean reverseOrder) {
            this(f,null,reverseOrder);
        }
        public ArrayIterator(Final<A> f,Selector<A> s) {
            this(f,s,false);
        }
        public ArrayIterator(Final<A> f,Selector<A> s,boolean reverseOrder) {
            if(f!=null)
                this.a = f.b;
            else
                this.a = null;
            this.b = f;
            this.reverseOrder = reverseOrder;
            if(s==null)
                this.s = Selector.Accepter;
            else
                this.s = s;
            if(this.a==null) {
                l = 0;
                r = -1;
            } else {
                A y = this.s.lowerBound();
                in = f.indexOfCeiled(y);
                if(in<0)
                    l = 0;
                else if(this.s.selects(y))
                    l = in;
                else
                    l = in+1;
                y = this.s.upperBound();
                in = f.indexOfFloored(y);
                if(in<0)
                    r = a.length;
                else if(this.s.selects(y))
                    r = in+1;
                else
                    r = in;
            }
            from = -1;
            reset();
        }
        public ArrayIterator(Final<A> f,int from) {
            this(f,from,0,f!=null?f.size():0,false);
        }
        public ArrayIterator(Final<A> f,int from,boolean reverseOrder) {
            this(f,from,0,f!=null?f.size():0,reverseOrder);
        }
        public ArrayIterator(Final<A> f,int l,int r) {
            this(f,l,l,r,false);
        }
        public ArrayIterator(Final<A> f,int from,int l,int r) {
            this(f,from,l,r,false);
        }
        public ArrayIterator(Final<A> f,int from,int l,int r,boolean reverseOrder) {
            if(f!=null)
                this.a = f.b;
            else
                this.a = null;
            this.b = f;
            this.reverseOrder = reverseOrder;
            int la = 0;
            if(a!=null)
                la = a.length;
            if((l<0)||(r>la)||(l>r))
                throw(new IndexOutOfBoundsException("Indices are out of bounds"));
            if((from<l)||(from>r))
                throw(new IndexOutOfBoundsException("From-index is out of bounds"));
            this.l = l;
            this.r = r;
            this.from = from;
            this.s = Selector.Accepter;
            reset();
        }
        private void searchNext() {
            if(reverseOrder) {
                while(in>=l) {
                    if(s.selects((A)(a[in])))
                        break;
                    in--;
                }
            } else {
                while(in<r) {
                    if(s.selects((A)(a[in])))
                        break;
                    in++;
                }
            }
        }
        private void searchPrev() {
            if(reverseOrder) {
                while(ip<r-1) {
                    if(s.selects((A)(a[ip+1])))
                        break;
                    ip++;
                }
            } else {
                while(ip>l) {
                    if(s.selects((A)(a[ip-1])))
                        break;
                    ip--;
                }
            }
        }
        public void reset() {
            if(from!=-1) {
                if(reverseOrder) {
                    in = from-1;
                    ip = from;
                } else {
                    in = from;
                    ip = from-1;
                }
            } else {
                if(reverseOrder) {
                    in = r-1;
                    ip = r;
                } else {
                    in = l;
                    ip = l-1;
                }
                searchNext();
                searchPrev();
            }
        }
        public Traversable.Set.Indexed<A> iteratedSet() {
            return(b);
        }
        public ResettableIterator<A> iterator() {
            reset();
            return(this);
        }
        public boolean hasNext() {
            return(reverseOrder?in>=l:in<r);
        }
        public boolean hasPrevious() {
            return(reverseOrder?ip<r-1:ip>l);
        }
        public int nextIndex() {
            return(in-l);
        }
        public int previousIndex() {
            return(ip-1-l);
        }
        public A next() {
            if(reverseOrder) {
                if(in<l)
                    throw(new java.util.NoSuchElementException("No more elements to iterate"));
                ip = in;
                A r = (A)(a[in--]);
                searchNext();
                return(r);
            } else {
                if(in>=r)
                    throw(new java.util.NoSuchElementException("No more elements to iterate"));
                ip = in;
                A r = (A)(a[in++]);
                searchNext();
                return(r);
            }
        }
        public A previous() {
            if(reverseOrder) {
                if(ip>=r-1)
                    throw(new java.util.NoSuchElementException("No more elements to iterate"));
                A r = (A)(a[++ip]);
                in = ip;
                searchPrev();
                return(r);
            } else {
                if(ip<=l)
                    throw(new java.util.NoSuchElementException("No more elements to iterate"));
                A r = (A)(a[ip--]);
                in = ip;
                searchPrev();
                return(r);
            }
        }
        public void add(A x) {
            throw(new UnsupportedOperationException("Modifications are not supported"));
        }
        public void set(A x) {
            throw(new UnsupportedOperationException("Modifications are not supported"));
        }
        public void remove() {
            throw(new UnsupportedOperationException("Modifications are not supported"));
        }
    }
    public static class NonLockingListIterator<A> implements ResettableListIterator<A> {
        private final Traversable.Set.Indexed<A> b;
        private final int from;
        private A last;
        private int i,l,r;

        public NonLockingListIterator(Traversable.Set.Indexed<A> b) {
            this(b,0,0,b.size());
        }
        public NonLockingListIterator(Traversable.Set.Indexed<A> b,int from) {
            this(b,0,from,b.size());
        }
        public NonLockingListIterator(Traversable.Set.Indexed<A> b,int l,int r) {
            this(b,l,l,r);
        }
        public NonLockingListIterator(Traversable.Set.Indexed<A> b,int l,int from,int r) {
            if(b==null)
                throw(new IllegalArgumentException("Tree to be iterated is null!"));
            this.b = b;
            if((l<0)||(r>b.size())||(l>r))
                throw(new IndexOutOfBoundsException("Indices are out of bounds"));
            if((from<0)||(from>r))
                throw(new IndexOutOfBoundsException("From-index is out of bounds"));
            this.l = l;
            this.r = r;
            this.from = from;
            reset();
        }
        public void reset() {
            last = null;
            i = from;
        }
        public Traversable.Set.Indexed<A> iteratedSet() {
            return(b);
        }
        public ResettableIterator<A> iterator() {
            reset();
            return(this);
        }
        public boolean hasNext() {
            return(i<r);
        }
        public boolean hasPrevious() {
            return(i>l);
        }
        public int nextIndex() {
            return(i);
        }
        public int previousIndex() {
            return(i-1);
        }
        public A next() {
            if(i>=r)
                throw(new NoSuchElementException("No more elements to iterate"));
            last = b.get(i++);
            return(last);
        }
        public A previous() {
            if(i<=l)
                throw(new NoSuchElementException("No more elements to iterate"));
            last = b.get(--i);
            return(last);
        }
        public void remove() {
            if(last==null)
                throw(new IllegalStateException("Iteration has not yet started"));
            if(!b.remove(last))
                throw(new IllegalStateException("Element has already been removed"));
            r--;
            i--;
        }
        public void add(A x) {
            if(b.add(x)) {
                r++;
                if(i<=b.indexOf(x))
                    i++;
            }
        }
        public void set(A x) {
            throw(new UnsupportedOperationException("Modifications are not supported"));
        }
    }
}
