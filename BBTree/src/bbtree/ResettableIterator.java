/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package bbtree;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.NoSuchElementException;

/**
 *
 * @author rj
 */
public interface ResettableIterator<A> extends Iterator<A>,Iterable<A> {
    public void reset();
    public OrderedSet<A> iteratedSet();
    public ResettableIterator<A> iterator();
    public boolean hasNext();
    public A next();
    public void remove();
    // Achtung: Der Thread, der den LockingIterator-constructor aufruft,
    // darf keinen Monitor f�r den zu iterierenden Baum besitzen
    // wenn doch: Dead-Lock beim ersten Aufruf von hasNext() oder next()
    public static class LockingIterator<A> implements ResettableIterator<A>,
                                    TraversalActionPerformer<A>, Runnable {
        private final Selector<A> s;
        private final OrderedSet<A> b;
        private final boolean reverseOrder;
        private volatile A next,last;
        private java.util.LinkedList<A> l;
        private volatile int status;

        private static final int MAY_RUN    = 0x01;
        private static final int GIVE_NEXT  = 0x02;
        private static final int LAST_REM   = 0x04;
        private static final int INIT_DONE  = 0x08;
        private static final int RUNNING    = 0x10;

        public LockingIterator(OrderedSet<A> b) {
            this(b,null);
        }
        public LockingIterator(OrderedSet<A> b,boolean reverseOrder) {
            this(b,null,reverseOrder);
        }
        public LockingIterator(OrderedSet<A> b,Selector<A> s) {
            this(b,s,false);
        }
        public LockingIterator(OrderedSet<A> b,Selector<A> s,boolean reverseOrder) {
            if(b==null)
                throw(new IllegalArgumentException("Tree to be iterated is null!"));
            this.b = b;
            if(s!=null)
                this.s = s;
            else
                this.s = Selector.Accepter;
            this.reverseOrder = reverseOrder;
            last = null;
            status = MAY_RUN|GIVE_NEXT|RUNNING;
            new Thread(this).start();
        }

        public synchronized boolean hasNext() {
            while((status&INIT_DONE)==0) {
                notifyAll();
                try { wait(); }
                catch(InterruptedException e)
                    { throw new RuntimeException("Thread was interrupted"); }
            }
            return(next!=null);
        }
        public synchronized A next() {
            while((status&INIT_DONE)==0) {
                notifyAll();
                try { wait(); }
                catch(InterruptedException e)
                    { throw new RuntimeException("Thread was interrupted"); }
            }
            if(next==null)
                throw(new NoSuchElementException("No more elements to iterate"));
            last = next;
            status = (status&(~LAST_REM))|GIVE_NEXT;
            while((status&GIVE_NEXT)!=0) {
                notifyAll();
                try { wait(); }
                catch(InterruptedException e)
                    { throw new RuntimeException("Thread was interrupted");}
            }
            return(last);
        }
        public void remove() {
            if(last==null)
                throw(new IllegalStateException("Iteration has not yet started"));
            if((status&LAST_REM)==0) {
                if(l==null)
                    l = new LinkedList<A>();
                l.add(last);
                status |= LAST_REM;
            }
        }
        public synchronized void run() {
            synchronized(b) {
                status |= INIT_DONE;
                if((status&MAY_RUN)==0) {
                    status &= ~GIVE_NEXT;
                    status &= ~RUNNING;
                    b.notifyAll();
                    notifyAll();
                }
                if(b instanceof Traversable.Set.Indexed) {
                    Traversable.Set.Indexed<A> b1 = (Traversable.Set.Indexed<A>)b;
                    int i,  l = b1.indexOfCeiled(s.lowerBound()),
                            r = b1.indexOfFloored(s.upperBound());
                    if(l<0)
                        l = 0;
                    if(r<0)
                        r = b1.size()-1;
                    i = reverseOrder?r:l;
                    while(reverseOrder?i>=l:i<=r) {
                        next = b1.get(i);
                        if(!s.selects(next)) {
                            if(reverseOrder)
                                i--;
                            else
                                i++;
                            continue;
                        }
                        status &= ~GIVE_NEXT;
                        while(((status&MAY_RUN)!=0)&&((status&GIVE_NEXT)==0)) {
                            notifyAll();
                            try { wait(); }
                            catch(InterruptedException e)
                                { throw new RuntimeException("Iterating thread was interrupted"); }
                        }
                        if((status&MAY_RUN)==0)
                            break;
                    }
                } else if(b instanceof Traversable)
                    ((Traversable<A>)b).traverseSelected(
                            reverseOrder?REVERSE_ORDER|IN_ORDER:IN_ORDER,this,s);
                else {
                    next = reverseOrder?b.last():b.first();
                    while(next!=null) {
                        if(!s.selects(next)) {
                            next = reverseOrder?b.lower(next):b.higher(next);
                            continue;
                        }
                        status &= ~GIVE_NEXT;
                        while(((status&MAY_RUN)!=0)&&((status&GIVE_NEXT)==0)) {
                            notifyAll();
                            try { wait(); }
                            catch(InterruptedException e)
                                { throw new RuntimeException("Iterating thread was interrupted"); }
                        }
                        if((status&MAY_RUN)==0)
                            break;
                    }
                }
                if(l!=null)
                    b.removeAll(l);
                status &= ~GIVE_NEXT;
                status &= ~RUNNING;
                next = null;
                b.notifyAll();
                notifyAll();
            }
        }
        public synchronized void reset() {
            status &= ~MAY_RUN;
            while((status&RUNNING)!=0) {
                notifyAll();
                try { wait(); }
                catch(InterruptedException e)
                    { throw new RuntimeException("Thread was interrupted: "+
                                                    Thread.currentThread());}
            }
            last = null;
            status = MAY_RUN|GIVE_NEXT|RUNNING;
            new Thread(this).start();
        }
        public ResettableIterator<A> iterator() {
            reset();
            return(this);
        }
        public OrderedSet<A> iteratedSet() {
            return(b);
        }
        public boolean perform(A x,int order,Object o) {
//            if((order!=IN_ORDER)||!s.selects(x))
//                return(false);
            next = x;
            status &= ~GIVE_NEXT;
            while(((status&MAY_RUN)!=0)&&((status&GIVE_NEXT)==0)) {
                notifyAll();
                try { wait(); }
                catch(InterruptedException e)
                    { System.err.println("thread was interrupted");
                      throw new RuntimeException(getClass().getCanonicalName()
                                                    +"thread was interrupted");}
            }
            return((status&MAY_RUN)==0);
        }
        public synchronized void finalize() {
            if((status&MAY_RUN)!=0) {
                status &= ~MAY_RUN;
                notifyAll();
            }
        }
    }
    public static class NonLockingIterator<A> implements ResettableIterator<A> {
        private final Selector<A> s;
        private final OrderedSet<A> b;
        private A next,last;
        private final boolean reverseOrder;
        public NonLockingIterator(OrderedSet<A> b) {
            this(b,null,false);
        }
        public NonLockingIterator(OrderedSet<A> b,boolean reverseOrder) {
            this(b,null,reverseOrder);
        }
        public NonLockingIterator(OrderedSet<A> b,Selector<A> s) {
            this(b,s,false);
        }
        public NonLockingIterator(OrderedSet<A> b,Selector<A> s,boolean reverseOrder) {
            if(b==null)
                throw(new IllegalArgumentException("Tree to be iterated is null!"));
            this.b = b;
            if(s!=null)
                this.s = s;
            else
                this.s = Selector.Accepter;
            this.reverseOrder = reverseOrder;
            reset();
        }
        public void reset() {
            last = null;
            if(reverseOrder) {
                next = b.last();
                while((next!=null)&&!this.s.selects(next))
                 next = b.lower(next);
            } else {
                next = b.first();
                while((next!=null)&&!this.s.selects(next))
                     next = b.higher(next);
            }
        }
        public OrderedSet<A> iteratedSet() {
            return(b);
        }
        public ResettableIterator<A> iterator() {
            reset();
            return(this);
        }
        public boolean hasNext() {
            return(next!=null);
        }
        public A next() {
            if(next==null)
                throw(new NoSuchElementException("No more elements to iterate"));
            last = next;
            if(reverseOrder) {
                next = b.lower(next);
                while((next!=null)&&!s.selects(next))
                     next = b.lower(next);
            } else {
                next = b.higher(next);
                while((next!=null)&&!s.selects(next))
                     next = b.higher(next);
            }
            return(last);
        }
        public void remove() {
            if(!b.remove(last))
                throw(new IllegalStateException("Iteration has not yet started"));
        }
    }
}
