/*
 * Writeable.java
 *
 * Created on 10. Oktober 2007, 11:53
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package bbtree;
import java.io.*;

/**
 * Represents an writeable object that can be written to a character stream.
 * @author RJ
 */
public interface Writeable {
    
    /**
     * Writes this Object to <CODE>w</CODE>.
     * @param w The character stream to which this object shall be written
     * @throws java.io.IOException If an I/O error occurs
     */
    public void writeToStream(Writer w) throws IOException;

	/**
	 * Append this object as string to <CODE>sb</CODE> or a new <CODE>StringBuilder</CODE>, if <CODE>sb</CODE> is <CODE>null</CODE>.
	 * @param sb The string builder to which this object shall be appended
	 * @return the string builder <CODE>sb</CODE> to which this object was appended
	 */
	public StringBuilder toStringBuilder(StringBuilder sb);
    
}
