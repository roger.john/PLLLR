/*
 * TraverseActionPerformer.java
 *
 * Created on 27. Juli 2006, 09:01
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */
package bbtree;

import java.io.*;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author RJ
 */
public interface TraversalActionPerformer<A> {

    /**
     * Indicates that results should be merged with a logical or. Used with LogicalJunctor.
     * @see LogicalJunctor
     */
    public static final boolean LOGICAL_OR = false;
    /**
     * Indicates that results should be merged with a logical and. Used with LogicalJunctor.
     * @see LogicalJunctor
     */
    public static final boolean LOGICAL_AND = true;
    //public constants for traversing, can be combined
    /**
     * Indicates that traversion should be done pre-order.
     * @see BBTree#traverse(int,TraverseActionPerformer)
     * @see BBTree#traverse(int,TraverseActionPerformer,Object)
     * @see BBTree#traverseRange(int,TraverseActionPerformer,Object,Object)
     * @see BBTree#traverseRange(int,TraverseActionPerformer,Object,Object,Object)
     * @see BBTree#traverseRange(int,TraverseActionPerformer,Comparable,Comparable)
     * @see BBTree#traverseRange(int,TraverseActionPerformer,Object,Comparable,Comparable)
     */
    public static final int PRE_ORDER = 1;
    /**
     * Indicates that traversion should be done in-order.
     * @see BBTree#traverse(int,TraverseActionPerformer)
     * @see BBTree#traverse(int,TraverseActionPerformer,Object)
     * @see BBTree#traverseRange(int,TraverseActionPerformer,Object,Object)
     * @see BBTree#traverseRange(int,TraverseActionPerformer,Object,Object,Object)
     * @see BBTree#traverseRange(int,TraverseActionPerformer,Comparable,Comparable)
     * @see BBTree#traverseRange(int,TraverseActionPerformer,Object,Comparable,Comparable)
     */
    public static final int IN_ORDER = 2;
    /**
     * Indicates that traversion should be done post-order.
     * @see BBTree#traverse(int,TraverseActionPerformer)
     * @see BBTree#traverse(int,TraverseActionPerformer,Object)
     * @see BBTree#traverseRange(int,TraverseActionPerformer,Object,Object)
     * @see BBTree#traverseRange(int,TraverseActionPerformer,Object,Object,Object)
     * @see BBTree#traverseRange(int,TraverseActionPerformer,Comparable,Comparable)
     * @see BBTree#traverseRange(int,TraverseActionPerformer,Object,Comparable,Comparable)
     */
    public static final int POST_ORDER = 4;
    /**
     * Indicates that traversion should be done in reverse order.
     * @see BBTree#traverse(int,TraverseActionPerformer)
     * @see BBTree#traverse(int,TraverseActionPerformer,Object)
     * @see BBTree#traverseRange(int,TraverseActionPerformer,Object,Object)
     * @see BBTree#traverseRange(int,TraverseActionPerformer,Object,Object,Object)
     * @see BBTree#traverseRange(int,TraverseActionPerformer,Comparable,Comparable)
     * @see BBTree#traverseRange(int,TraverseActionPerformer,Object,Comparable,Comparable)
     */
    public static final int REVERSE_ORDER = 8;

    /**
     * Performs the desired action on the given object. The performer assumes that this is issued in the given order.
     * The identifier may help to distinguish between different traversion that are taken part in.
     * @param x The object on which the action shall be performed
     * @param order The order in which the traversion performs this action
     * @param o An identifier for this object to determine the actual traversion process, or <CODE>null</CODE>
     * @returns true if the the traversion should be aborted, false otherwise
     */
    public boolean perform(A x, int order, Object o);

    public static class CollectionAdder<A> implements
            TraversalActionPerformer<A> {

        public final Selector<? super A> f;
        public final Collection<? super A> s;
        public final boolean abort_on_illegal_argument;
        private int count = 0;

        public CollectionAdder(Collection<? super A> s) {
            this(s, null, false);
        }

        public CollectionAdder(Collection<? super A> s, Selector<? super A> f,
                               boolean abort_on_illegal_argument) {
            this.s = s;
            if(f != null)
                this.f = f;
            else
                this.f = Selector.Accepter;
            this.abort_on_illegal_argument = abort_on_illegal_argument;
        }

        public boolean perform(A x, int order, Object o) {
            if(f.selects(x)) {
                if(s.add(x))
                    count++;
                return (false);
            } else
                return (abort_on_illegal_argument);
        }

        public int addedItems() {
            return (count);
        }

        public Collection<? super A> toCollection() {
            return (s);
        }
    }

    public static class OrderedSetAdder<A> implements
            TraversalActionPerformer<A> {

        public final Selector<? super A> f;
        public final Merger<A> m;
        public final OrderedSet<A> s;
        public final boolean abort_on_illegal_argument;
        private int count = 0;

        public OrderedSetAdder(OrderedSet<A> s) {
            this(s, null, false, null);
        }

        public OrderedSetAdder(OrderedSet<A> s, Merger<A> m) {
            this(s, null, false, m);
        }

        public OrderedSetAdder(OrderedSet<A> s, Selector<? super A> f,
                               boolean abort_on_illegal_argument) {
            this(s, f, abort_on_illegal_argument, null);
        }

        public OrderedSetAdder(OrderedSet<A> s, Selector<? super A> f,
                               boolean abort_on_illegal_argument, Merger<A> m) {
            this.s = s;
            if(f != null)
                this.f = f;
            else
                this.f = Selector.Accepter;
            this.m = m;
            this.abort_on_illegal_argument = abort_on_illegal_argument;
        }

        public boolean perform(A x, int order, Object o) {
            if(f.selects(x)) {
                if(s.add(x, m))
                    count++;
                return (false);
            } else
                return (abort_on_illegal_argument);
        }

        public int addedItems() {
            return (count);
        }

        public Collection<? super A> toCollection() {
            return (s);
        }
    }

    public static class CollectionDropper<A> implements
            TraversalActionPerformer<A> {

        public final Collection<? super A> s;
        private int count = 0;

        public CollectionDropper(Collection<? super A> s) {
            this.s = s;
        }

        public boolean perform(A x, int order, Object o) {
            if(s.remove(x))
                count++;
            return (false);
        }

        public int droppedItems() {
            return (count);
        }

        public Collection<? super A> toCollection() {
            return (s);
        }
    }

    public static class ArrayBuilder<A> implements TraversalActionPerformer<A> {

        Object[] a = null;
        int current_index = 0;

        public ArrayBuilder(int size) {
            a = new Object[size];
        }

        public ArrayBuilder(Object[] a) {
            a.hashCode();
            this.a = a;
        }

        public boolean perform(A x, int order, Object o) {
            switch(order) {
                case IN_ORDER:
                    a[current_index++] = x;
                    break;
                default:
                    throw (new RuntimeException("wrong order"));
            }
            return (false);
        }

        public Object[] toArray() {
            return (a);
        }

        public boolean finished() {
            return (current_index == a.length);
        }
    }

    public static class RepresentationStringBuilder<A> implements
            TraversalActionPerformer<A> {

        StringBuilder sb;
        String left_bracket, comma, right_bracket;
        boolean notfirst = false;

        public RepresentationStringBuilder() {
            this(null, null, null, null);
        }

        public RepresentationStringBuilder(StringBuilder sb) {
            this(sb, null, null, null);
        }

        public RepresentationStringBuilder(String left_bracket, String comma,
                                           String right_bracket) {
            this(null, left_bracket, comma, right_bracket);
        }

        public RepresentationStringBuilder(StringBuilder sb, String left_bracket,
                                           String comma, String right_bracket) {
            if(sb == null)
                sb = new StringBuilder();
            this.sb = sb;
            this.left_bracket = left_bracket != null ? left_bracket : "{";
            this.comma = comma != null ? comma : ",";
            this.right_bracket = right_bracket != null ? right_bracket : "}";
            this.sb.append(this.left_bracket);
        }

        public boolean perform(A x, int order, Object o) {
            switch(order) {
                case IN_ORDER:
                    if(notfirst)
                        sb.append(comma);
                    else
                        notfirst = true;
                    sb.append(x);
                    break;
                default:
                    break;
            }
            return false;
        }

        public String toString() {
            return sb.toString() + right_bracket;
        }

        public StringBuilder getClosedStringBuilder() {
            return sb.append(right_bracket);
        }
    }

    public static class RepresentationWriter<A> implements
            TraversalActionPerformer<A> {

        Writer w;
        boolean notfirst = false;

        public RepresentationWriter(Writer w) {
            if(w == null)
                throw (new NullPointerException(
                        "writer is null"));
            else
                this.w = w;
        }

        public boolean perform(A x, int order, Object o) {
            if(order == IN_ORDER)
                try {
                    if(notfirst)
                        w.write(',');
                    else
                        notfirst = true;
                    if(x != null)
                        if(x instanceof Writeable)
                            ((Writeable)x).writeToStream(w);
                        else
                            w.write(x.toString());
                } catch(IOException e) {
                    throw (new RuntimeException("Erro while writing", e));
                }
            return false;
        }
    }
    public static final TraversalActionPerformer<Object> SERIALIZER = new TraversalActionPerformer<Object>() {

        public boolean perform(Object x, int order, Object o) {
            if(order == IN_ORDER)
                try {
                    ((java.io.ObjectOutputStream)o).writeObject(x);
                } catch(IOException ex) {
                    throw (new RuntimeException(
                            "Erro while writing", ex));
                }
            return false;
        }
    };

    public static class Hasher<A> implements TraversalActionPerformer<A> {

        int hash;

        public Hasher<A> init() {
            hash = 0;
            return (this);
        }

        public boolean perform(A x, int order, Object o) {
            switch(order) {
                case IN_ORDER:
                    hash += x.hashCode();
            }
            return (false);
        }

        public int getHash() {
            return (hash);
        }

        public int reset() {
            int h = hash;
            hash = 0;
            return (h);
        }

        public static Hasher getThreadLocal() {
            return (TLV.get().init());
        }
        private static final ThreadLocal<Hasher> TLV = new ThreadLocal<Hasher>() {

            protected Hasher initialValue() {
                return (new Hasher());
            }
        };
    }

    public static class LogicalJunctor<A> implements TraversalActionPerformer<A> {

        boolean state;
        boolean and;
        Selector<? super A> s1, s2;
        A last;

        public LogicalJunctor(boolean and, Selector<? super A> s1) {
            this(and, s1, null);
        }
        // checks only elements that are selected by s2

        public LogicalJunctor(boolean and, Selector<? super A> s1,
                              Selector<? super A> s2) {
            this.and = and;
            this.state = and;
            if(s1 != null)
                this.s1 = s1;
            else
                this.s1 = Selector.Accepter;
            if(s2 != null)
                this.s2 = s2;
            else
                this.s2 = Selector.Accepter;
            last = null;
        }

        public boolean perform(A x, int order, Object o) {
            switch(order) {
                case IN_ORDER:
                    if((x != null) && s2.selects(x)) {
                        if(and)
                            state &= s1.selects(x);
                        else
                            state |= s1.selects(x);
                        last = x;
                    }
                    break;
                default:
                    break;
            }
            if(state ^ and)
                return (true);
            return (false);
        }

        public boolean getState() {
            return (state);
        }

        public boolean reset() {
            boolean h = state;
            state = and;
            return (h);
        }

        public A getLast() {
            return (last);
        }
    }

    static class Counter<A> implements TraversalActionPerformer<A> {

        int counter;

        public Counter<A> init() {
            counter = 0;
            return (this);
        }

        public boolean perform(A x, int order, Object o) {
            switch(order) {
                case IN_ORDER:
                    counter++;
            }
            return (false);
        }

        public int getCounter() {
            return (counter);
        }

        public int reset() {
            int h = counter;
            counter = 0;
            return (h);
        }

        public static Counter getThreadLocal() {
            return (TLV.get().init());
        }
        private static final ThreadLocal<Counter> TLV = new ThreadLocal<Counter>() {

            protected Counter initialValue() {
                return (new Counter());
            }
        };
    }

    static class NavigableSetComparator<A> implements
            TraversalActionPerformer<A> {

        int c;
        A last;
        private Comparator<? super A> comp;

        NavigableSetComparator init(Comparator<? super A> comp) {
            last = null;
            c = 0;
            this.comp = comp;
            return (this);
        }

        public boolean perform(A x, int order, Object o) {
            NavigableSet<A> f = (NavigableSet<A>)o;
            A y = f.higher(last);
            if(y == null) {
                c = (x == null) ? 0 : 1;
                return (true);
            }
            Comparator<? super A> comp = this.comp;
            c = comp.compare(x, y);
            last = y;
            this.comp = comp;
            return (c != 0);
        }

        public static NavigableSetComparator getThreadLocal(Comparator comp) {
            return (TLV.get().init(comp));
        }
        private static final ThreadLocal<NavigableSetComparator> TLV =
                new ThreadLocal<NavigableSetComparator>() {

                    protected NavigableSetComparator initialValue() {
                        return (new NavigableSetComparator());
                    }
                };
    }

    static class IndexedSetComparator<A> implements TraversalActionPerformer<A> {

        int i, c;
        private Comparator<? super A> comp;

        IndexedSetComparator init(Comparator<? super A> comp) {
            i = 0;
            c = 0;
            this.comp = comp;
            return (this);
        }

        public boolean perform(A x, int order, Object o) {
            Traversable.Set.Indexed<A> f = (Traversable.Set.Indexed<A>)o;
            int j = i;
            if(j >= f.size()) {
                c = (x == null) ? 0 : 1;
                i = j;
                return (true);
            }
            A y = f.get(j);
            c = comp.compare(x, y);
            i = j + 1;
            return (c != 0);
        }

        static IndexedSetComparator getThreadLocal(Comparator comp) {
            return (TLV.get().init(comp));
        }
        private static final ThreadLocal<IndexedSetComparator> TLV =
                new ThreadLocal<IndexedSetComparator>() {

                    protected IndexedSetComparator initialValue() {
                        return (new IndexedSetComparator());
                    }
                };
    }
}
