/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bbtree;

import bbtree.Utils.AsymmetricComparable;
import bbtree.Utils.KeyedElementFactory;
import java.io.IOException;
import java.io.Writer;

/**
 *
 * @author RJ
 */
public interface Value<V> {

    public V getValue();

    public V setValue(V x);

    public static interface Comparable<V>
            extends Value<V>, java.lang.Comparable<Value<? extends V>> {

        public static class Indexed<V> implements Comparable<V>,
                                                  AsymmetricComparable<Number>,
                                                  Writeable {

            public int index;
            public V value;

            public Indexed() {
                this(0, null);
            }

            public Indexed(int index) {
                this(index, null);
            }

            public Indexed(int index, V value) {
                this.index = index;
                this.value = value;
            }

            public int getIndex() {
                return index;
            }

            public int setIndex(int index) {
                int old = this.index;
                this.index = index;
                return old;
            }

            public V getValue() {
                return value;
            }

            public V setValue(V value) {
                V old = this.value;
                this.value = value;
                return old;
            }

            public int compareTo(Value<? extends V> o) {
                if(o == null)
                    return 1;
                if(!(o instanceof Indexed)) {
                    if(o instanceof Keyed) {
                        Object key = ((Keyed)o).key;
                        if((key != null) && (key instanceof Number))
                            return ((Number)key).intValue() - index;
                    }
                    return this.getClass().getCanonicalName().compareTo(o.
                            getClass().getCanonicalName());
                }
                return this.index - ((Indexed<?>)o).index;
            }

            public int asymmetricCompareTo(Number x) {
                if(x == null)
                    return 1;
                return this.index - x.intValue();
            }

            public boolean equals(Object o) {
                return ((this == o) || ((o instanceof Indexed) && (compareTo(
                        (Indexed)o) == 0)));
            }

            public String toString() {
                return index + " -> " + value;
            }

            public StringBuilder toStringBuilder(StringBuilder sb) {
                if(sb == null)
                    sb = new StringBuilder();
                return sb.append(index).append(" -> ").append(value);
            }

            public void writeToStream(Writer w) throws IOException {
                w.write(index + " -> ");
                if(value != null)
                    if(value instanceof Writeable)
                        ((Writeable)value).writeToStream(w);
                    else
                        w.write(value.toString());
                else
                    w.write("null");
            }

            public static class Factory<V1> implements
                    KeyedElementFactory<Indexed<V1>, Number> {

                public Indexed<V1> last;

                public Indexed<V1> createElement(Number x) {
                    last = new Indexed<V1>(x.intValue());
                    return last;
                }

                public Indexed<V1> lastCreated() {
                    return last;
                }

                public void reset() {
                    last = null;
                }

                public Number extractKey(Indexed<V1> x) {
                    return x.index;
                }
            }
        }

        public static class Keyed<K extends java.lang.Comparable<? super K>, V>
                implements Comparable<V>, AsymmetricComparable<K>, Writeable {

            public K key;
            public V value;

            public Keyed() {
                this(null, null);
            }

            public Keyed(K key) {
                this(key, null);
            }

            public Keyed(K key, V value) {
                this.key = key;
                this.value = value;
            }

            public K getKey() {
                return key;
            }

            public K setKey(K key) {
                K old = this.key;
                this.key = key;
                return old;
            }

            public V getValue() {
                return value;
            }

            public V setValue(V value) {
                V old = this.value;
                this.value = value;
                return old;
            }

            public int compareTo(Value<? extends V> o) {
                if(o == null)
                    return 1;
                if(!(o instanceof Keyed)) {
                    if((o instanceof Indexed) && (key != null) && (key instanceof Number))
                        return ((Number)key).intValue() - ((Indexed)o).getIndex();
                    return this.getClass().getCanonicalName().compareTo(o.
                            getClass().getCanonicalName());
                }
                return asymmetricCompareTo(((Keyed<K, ?>)o).key);
            }

            public int asymmetricCompareTo(K x) {
                if(this.key == null)
                    return x == null ? 0 : -1;
                return this.key.compareTo(x);
            }

            public boolean equals(Object o) {
                return ((this == o) || ((o instanceof Keyed) && (compareTo(
                        (Keyed)o) == 0)));
            }

            public String toString() {
                return key + " -> " + value;
            }

            public StringBuilder toStringBuilder(StringBuilder sb) {
                if(sb == null)
                    sb = new StringBuilder();
                return sb.append(key).append(" -> ").append(value);
            }

            public void writeToStream(Writer w) throws IOException {
                if(key != null)
                    if(key instanceof Writeable)
                        ((Writeable)key).writeToStream(w);
                    else
                        w.write(key.toString());
                else
                    w.write("null");
                w.write(" -> ");
                if(value != null)
                    if(value instanceof Writeable)
                        ((Writeable)value).writeToStream(w);
                    else
                        w.write(value.toString());
                else
                    w.write("null");
            }

            public static class Factory<K1 extends java.lang.Comparable<? super K1>, V1>
                    implements KeyedElementFactory<Keyed<K1, V1>, K1> {

                public Keyed<K1, V1> last;

                public Keyed<K1, V1> createElement(K1 x) {
                    last = new Keyed<K1, V1>(x);
                    return last;
                }

                public Keyed<K1, V1> lastCreated() {
                    return last;
                }

                public void reset() {
                    last = null;
                }

                public K1 extractKey(Keyed<K1, V1> x) {
                    return x.key;
                }
            }
        }
    }
}
