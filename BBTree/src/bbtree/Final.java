/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bbtree;

import bbtree.ResettableListIterator.ArrayIterator;
import bbtree.Utils.AsymmetricComparator;
import bbtree.Utils.KeyedElementFactory;
import java.io.IOException;
import java.io.Writer;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;

/**
 *
 * @author rj
 */
public class Final<A> implements Traversable.Set.Indexed<A> {

    private static final long serialVersionUID = 0;
    final Object[] b;
    final Comparator<? super A> comp;

    public Final(OrderedSet<A> s) {
        if(s == null)
            throw new NullPointerException("Set is null");
        this.comp = Utils.wrapComparator(s.comparator(),false);
        this.b = s.toArray();
    }

    public Final(Object[] a) {
        this(a, null, false);
    }

    public Final(Object[] a, Comparator<? super A> comp) {
        this(a, comp, false);
    }

    public Final(Object[] a, Comparator<? super A> comp, boolean alreadySorted) {
        this.comp = Utils.wrapComparator(comp,false);
        this.b = a;
        if((b != null) && (!alreadySorted))
            java.util.Arrays.sort(b, (Comparator<Object>)this.comp);
    }

    public Final(Path<A> p) {
        this(p, null);
    }

    public Final(Path<A> p, Comparator<? super A> comp) {
        this.comp = Utils.wrapComparator(comp,false);
        if(p != null) {
            int i = 0, c;
            boolean sorted = true;
            A last = null;
            Path.Node<A> n = p.head;
            while(n != null) {
                if(n.x != null) {
                    c = last != null ? comp.compare(last, n.x) : -1;
                    if(c > 0)
                        sorted = false;
                    if(c == 0)
                        throw new IllegalArgumentException(
                                "Duplicate entry discovered");
                    i++;
                    last = n.x;
                }
                n = n.next;
            }
            if(i != 0) {
                b = new Object[i];
                n = p.head;
                i = 0;
                while(n != null) {
                    if(n.x != null)
                        b[i++] = n.x;
                    n = n.next;
                }
                if(!sorted)
                    java.util.Arrays.sort(b, (Comparator<Object>)this.comp);
            } else
                b = null;
        } else
            b = null;
    }

    private int psearch(A x) {
        if((x == null) || (b == null) || (b.length == 0))
            return 0x80000000;
        int l = 0, h = b.length - 1, m, c;
        while(l <= h) {
            m = (l + h) >>> 1;
            c = comp.compare((A)(b[m]), x);
            if(c == 0)
                return m;
            if(c < 0)
                l = m + 1;
            else
                h = m - 1;
        }
        return -2 - h;
    }

    private int psearchComparable(Comparable<? super A> x) {
        if((x == null) || (b == null) || (b.length == 0))
            return 0x80000000;
        int l = 0, h = b.length - 1, m, c;
        while(l <= h) {
            m = (l + h) >>> 1;
            c = x.compareTo((A)(b[m]));
            if(c == 0)
                return m;
            if(c > 0)
                l = m + 1;
            else
                h = m - 1;
        }
        return -2 - h;
    }

    private <B> int psearchMatch(B x) {
        if((x == null) || (b == null) || (b.length == 0))
            return 0x80000000;
        int l = 0, h = b.length - 1, m, c;
        while(l <= h) {
            m = (l + h) >>> 1;
            c = -((AsymmetricComparator<A>)comp).asymmetricCompare((A)b[m],x);
            if(c == 0)
                return m;
            if(c > 0)
                l = m + 1;
            else
                h = m - 1;
        }
        return -2 - h;
    }

    public A get(A x) {
        if(x == null)
            return null;
        int k = psearch(x);
        if(k < 0)
            return null;
        else
            return (A)(b[k]);
    }

    public A get(int i) {
//        if((i<0)||(i>=b.length))
//            throw(new IndexOutOfBoundsException("Index "+i+" is out of bounds"));
        return (A)(b[i]);
    }

    public int indexOf(Object x) {
        if(x == null)
            return -1;
        int k = psearch((A)x);
        if(k < 0)
            return -1;
        else
            return k;
    }

    public int indexOfComparable(Comparable<? super A> x) {
        if(x == null)
            return -1;
        int k = psearchComparable(x);
        if(k < 0)
            return -1;
        else
            return k;
    }

    public <B> int indexOfMatch(B x) {
        if(x == null)
            return -1;
        int k = psearchMatch(x);
        if(k < 0)
            return -1;
        else
            return k;
    }

    public int lastIndexOf(Object x) {
        return indexOf(x);
    }

    public A set(A x) {
        if(x == null)
            return null;
        int k = psearch(x);
        if(k < 0)
            throw new UnsupportedOperationException(
                    "Modifications are not supported");
        A r = (A)(b[k]);
        b[k] = x;
        return r;
    }

    public A set(int i, A x) {
        if((i < 0) || (i >= size()))
            throw new UnsupportedOperationException(
                    "Modifications are not supported");
        if(x == null)
            throw new NullPointerException("Null entries are not allowed");
        A y = (A)(b[i]);
        if(comp.compare(x, y) != 0)
            throw new UnsupportedOperationException(
                    "Modifications are not supported");
        b[i] = x;
        return y;
    }

    public A pollFirst() {
        throw new UnsupportedOperationException(
                "Modifications are not supported");
    }

    public A pollLast() {
        throw new UnsupportedOperationException(
                "Modifications are not supported");
    }

    public void add(int i, A x) {
        throw new UnsupportedOperationException(
                "Modifications are not supported");
    }

    public A remove(int i) {
        throw new UnsupportedOperationException(
                "Modifications are not supported");
    }

    public boolean contains(Object o) {
        return psearch((A)o) >= 0;
    }

    public boolean containsAll(Collection<?> c) {
        return containsAll(c, null);
    }

    public boolean containsAll(Collection<?> c, Selector<?> s) {
        if((c == null) || (c == this))
            return true;
        if(c instanceof Traversable) {
            TraversalActionPerformer.LogicalJunctor<Object> a =
                    new TraversalActionPerformer.LogicalJunctor<Object>(
                    TraversalActionPerformer.LOGICAL_AND,
                    new Selector.Conjunctor(
                    (Selector<Object>)this, (Selector<Object>)s));
            ((Traversable<Object>)c).traverse(IN_ORDER, a);
            return a.getState();
        }
        if(s != null) {
            for(Object o : c)
                if(!((Selector<Object>)s).selects(o) || !contains(o))
                    return false;
        } else
            for(Object o : c)
                if(!contains(o))
                    return false;
        return true;
    }

    public boolean isContainedIn(Collection<?> c) {
        if(c == null)
            return false;
        if(c == this)
            return true;
        int l = size();
        for(int i = 0; i < l; i++)
            if(!c.contains(b[i]))
                return false;
        return true;
    }

    public boolean add(A x) {
        throw new UnsupportedOperationException(
                "Modifications are not supported");
    }

    public boolean add(A x, Merger<A> m) {
        throw new UnsupportedOperationException(
                "Modifications are not supported");
    }

    public boolean addAll(Collection<? extends A> c, Merger<A> m) {
        throw new UnsupportedOperationException(
                "Modifications are not supported");
    }

    public boolean addAll(Collection<? extends A> c, Selector<? super A> s,
                          boolean b) {
        throw new UnsupportedOperationException(
                "Modifications are not supported");
    }

    public boolean addAll(Collection<? extends A> c, Selector<? super A> s,
                          boolean b, Merger<A> m) {
        throw new UnsupportedOperationException(
                "Modifications are not supported");
    }

    public A addOrGet(A x) {
        if(x == null)
            return null;
        A r = get(x);
        if(r == null)
            throw new UnsupportedOperationException(
                    "Modifications are not supported");
        return r;
    }

    public <B> A addOrGetMatch(B x, KeyedElementFactory<A, B> f) {
        if(x == null)
            return null;
        A r = getMatch(x);
        if(r == null)
            throw new UnsupportedOperationException(
                    "Modifications are not supported");
        return r;
    }

    public boolean addAll(Collection<? extends A> c) {
        throw new UnsupportedOperationException(
                "Modifications are not supported");
    }

    public boolean addAll(int i, Collection<? extends A> c) {
        throw new UnsupportedOperationException(
                "Modifications are not supported");
    }

    public boolean remove(Object o) {
        throw new UnsupportedOperationException(
                "Modifications are not supported");
    }

    public boolean removeAll(Collection<?> c) {
        throw new UnsupportedOperationException(
                "Modifications are not supported");
    }

    public boolean removeAll(Selector<? super A> s) {
        throw new UnsupportedOperationException(
                "Modifications are not supported");
    }

    public boolean retainAll(Collection<?> c) {
        throw new UnsupportedOperationException(
                "Modifications are not supported");
    }

    public boolean retainAll(Selector<? super A> s) {
        throw new UnsupportedOperationException(
                "Modifications are not supported");
    }

    public A removeComparable(Comparable<? super A> x) {
        throw new UnsupportedOperationException(
                "Modifications are not supported");
    }

    public <B> A removeMatch(B x) {
        throw new UnsupportedOperationException(
                "Modifications are not supported");
    }

    public boolean isEmpty() {
        return (b == null) || (b.length == 0);
    }

    public void clear() {
        throw new UnsupportedOperationException(
                "Modifications are not supported");
    }

    public int size() {
        return b == null ? 0 : b.length;
    }

    public int maxSearchDepth() {
        int n = size(), k = 0;
        while(n != 0) {
            k++;
            n >>>= 1;
        }
        return k;
    }

    public A getComparable(Comparable<? super A> x) {
        int i = psearchComparable(x);
        if(i >= 0)
            return (A)(b[i]);
        else
            return null;
    }

    public <B> A getMatch(B x) {
        int i = psearchMatch(x);
        if(i >= 0)
            return (A)(b[i]);
        else
            return null;
    }

    public A first() {
        if((b == null) || (b.length == 0))
            return null;
        return (A)(b[0]);
    }

    public A last() {
        if((b == null) || (b.length == 0))
            return null;
        return (A)(b[b.length - 1]);
    }

    public A higher(A x) {
        if((b == null) || (b.length == 0))
            return null;
        if(x == null)
            return first();
        int i = psearch(x);
        if(i < 0)
            i = -i - 2;
        if(i >= b.length - 1)
            return null;
        else
            return (A)(b[i + 1]);
    }

    public A higherComparable(Comparable<? super A> x) {
        if((b == null) || (b.length == 0))
            return null;
        if(x == null)
            return first();
        int i = psearchComparable(x);
        if(i < 0)
            i = -i - 2;
        if(i >= b.length - 1)
            return null;
        else
            return (A)(b[i + 1]);
    }

    public <B> A higherMatch(B x) {
        if((b == null) || (b.length == 0))
            return null;
        if(x == null)
            return first();
        int i = psearchMatch(x);
        if(i < 0)
            i = -i - 2;
        if(i >= b.length - 1)
            return null;
        else
            return (A)(b[i + 1]);
    }

    public int indexOfHigher(A x) {
        if((b == null) || (b.length == 0))
            return -1;
        if(x == null)
            return 0;
        int i = psearch((A)x);
        if(i < 0)
            i = -i - 2;
        if(i >= b.length - 1)
            return -1;
        else
            return i + 1;
    }

    public int indexOfHigherComparable(Comparable<? super A> x) {
        if((b == null) || (b.length == 0))
            return -1;
        if(x == null)
            return 0;
        int i = psearchComparable(x);
        if(i < 0)
            i = -i - 2;
        if(i >= b.length - 1)
            return -1;
        else
            return i + 1;
    }

    public <B> int indexOfHigherMatch(B x) {
        if((b == null) || (b.length == 0))
            return -1;
        if(x == null)
            return 0;
        int i = psearchMatch(x);
        if(i < 0)
            i = -i - 2;
        if(i >= b.length - 1)
            return -1;
        else
            return i + 1;
    }

    public A lower(A x) {
        if((b == null) || (b.length == 0))
            return null;
        if(x == null)
            return last();
        int i = psearch(x);
        if(i >= 0)
            if(i == 0)
                return null;
            else
                return (A)(b[i - 1]);
        i = -i - 2;
        if(i <= 0)
            return null;
        else
            return (A)(b[i]);
    }

    public A lowerComparable(Comparable<? super A> x) {
        if((b == null) || (b.length == 0))
            return null;
        if(x == null)
            return last();
        int i = psearchComparable(x);
        if(i >= 0)
            if(i == 0)
                return null;
            else
                return (A)(b[i - 1]);
        i = -i - 2;
        if(i <= 0)
            return null;
        else
            return (A)(b[i]);
    }

    public <B> A lowerMatch(B x) {
        if((b == null) || (b.length == 0))
            return null;
        if(x == null)
            return last();
        int i = psearchMatch(x);
        if(i >= 0)
            if(i == 0)
                return null;
            else
                return (A)(b[i - 1]);
        i = -i - 2;
        if(i <= 0)
            return null;
        else
            return (A)(b[i]);
    }

    public int indexOfLower(A x) {
        if((b == null) || (b.length == 0))
            return -1;
        if(x == null)
            return b.length - 1;
        int i = psearch((A)x);
        if(i >= 0)
            return i - 1;
        i = -i - 2;
        if(i <= 0)
            return -1;
        else
            return i;
    }

    public int indexOfLowerComparable(Comparable<? super A> x) {
        if((b == null) || (b.length == 0))
            return -1;
        if(x == null)
            return b.length - 1;
        int i = psearchComparable(x);
        if(i >= 0)
            return i - 1;
        i = -i - 2;
        if(i <= 0)
            return -1;
        else
            return i;
    }

    public <B> int indexOfLowerMatch(B x) {
        if((b == null) || (b.length == 0))
            return -1;
        if(x == null)
            return b.length - 1;
        int i = psearchMatch(x);
        if(i >= 0)
            return i - 1;
        i = -i - 2;
        if(i <= 0)
            return -1;
        else
            return i;
    }

    public A floor(A x) {
        if((x == null) || (b == null) || (b.length == 0))
            return null;
        int i = psearch(x);
        if(i >= 0)
            return (A)(b[i]);
        i = -i - 2;
        if(i <= 0)
            return null;
        else
            return (A)(b[i]);
    }

    public A floorComparable(Comparable<? super A> x) {
        if((x == null) || (b == null) || (b.length == 0))
            return null;
        int i = psearchComparable(x);
        if(i >= 0)
            return (A)(b[i]);
        i = -i - 2;
        if(i <= 0)
            return null;
        else
            return (A)(b[i]);
    }

    public <B> A floorMatch(B x) {
        if((x == null) || (b == null) || (b.length == 0))
            return null;
        int i = psearchMatch(x);
        if(i >= 0)
            return (A)(b[i]);
        i = -i - 2;
        if(i <= 0)
            return null;
        else
            return (A)(b[i]);
    }

    public int indexOfFloored(A x) {
        if((x == null) || (b == null) || (b.length == 0))
            return -1;
        int i = psearch((A)x);
        if(i >= 0)
            return i;
        i = -i - 2;
        if(i <= 0)
            return -1;
        else
            return i;
    }

    public int indexOfFlooredComparable(Comparable<? super A> x) {
        if((x == null) || (b == null) || (b.length == 0))
            return -1;
        int i = psearchComparable(x);
        if(i >= 0)
            return i;
        i = -i - 2;
        if(i <= 0)
            return -1;
        else
            return i;
    }

    public <B> int indexOfFlooredMatch(B x) {
        if((x == null) || (b == null) || (b.length == 0))
            return -1;
        int i = psearchMatch(x);
        if(i >= 0)
            return i;
        i = -i - 2;
        if(i <= 0)
            return -1;
        else
            return i;
    }

    public A ceiling(A x) {
        if((x == null) || (b == null) || (b.length == 0))
            return null;
        int i = psearch(x);
        if(i >= 0)
            return (A)(b[i]);
        i = -i - 2;
        if(i >= b.length - 1)
            return null;
        else
            return (A)(b[i + 1]);
    }

    public A ceilComparable(Comparable<? super A> x) {
        if((x == null) || (b == null) || (b.length == 0))
            return null;
        int i = psearchComparable(x);
        if(i >= 0)
            return (A)(b[i]);
        i = -i - 2;
        if(i >= b.length - 1)
            return null;
        else
            return (A)(b[i + 1]);
    }

    public <B> A ceilMatch(B x) {
        if((x == null) || (b == null) || (b.length == 0))
            return null;
        int i = psearchMatch(x);
        if(i >= 0)
            return (A)(b[i]);
        i = -i - 2;
        if(i >= b.length - 1)
            return null;
        else
            return (A)(b[i + 1]);
    }

    public int indexOfCeiled(A x) {
        if((x == null) || (b == null) || (b.length == 0))
            return -1;
        int i = psearch((A)x);
        if(i >= 0)
            return i;
        i = -i - 2;
        if(i >= b.length - 1)
            return -1;
        else
            return i + 1;
    }

    public int indexOfCeiledComparable(Comparable<? super A> x) {
        if((x == null) || (b == null) || (b.length == 0))
            return -1;
        int i = psearchComparable(x);
        if(i >= 0)
            return i;
        i = -i - 2;
        if(i >= b.length - 1)
            return -1;
        else
            return i + 1;
    }

    public <B> int indexOfCeiledMatch(B x) {
        if((x == null) || (b == null) || (b.length == 0))
            return -1;
        int i = psearchMatch(x);
        if(i >= 0)
            return i;
        i = -i - 2;
        if(i >= b.length - 1)
            return -1;
        else
            return i + 1;
    }

    public boolean subSetOf(Collection<?> s) {
        return isContainedIn(s);
    }

    public boolean superSetOf(Collection<?> s) {
        if(s == null)
            return true;
        return containsAll(s);
    }

    public Traversable.Set.Indexed<A> join(Collection<? extends A> s) {
        Traversable.Set.Indexed<A> r = clone();
        r.addAll(s);
        return r;
    }

    public Traversable.Set.Indexed<A> intersect(Collection<? super A> s) {
        Traversable.Set.Indexed<A> r = new IBBTree<A>(comp != this ? comp : null);
        if((b == null) || (b.length == 0) || (s == null) || s.isEmpty())
            return r;
        for(int i = 0; i < b.length; i++)
            if(s.contains(b[i]))
                r.add((A)(b[i]));
        return r;
    }

    public Traversable.Set.Indexed<A> without(Collection<? super A> s) {
        if((b == null) || (b.length == 0) || (s == null) || s.isEmpty())
            return clone();
        Traversable.Set.Indexed<A> r = new IBBTree<A>(comp != this ? comp : null);
        for(int i = 0; i < b.length; i++)
            if(!s.contains(b[i]))
                r.add((A)(b[i]));
        return r;
    }

    public Traversable.Set.Indexed<A> reorder(Comparator<? super A> c,
                                              boolean make_new) {
        return reorder(c, null, make_new);
    }

    public Traversable.Set.Indexed<A> reorder(Comparator<? super A> c,
                                              Merger<A> m, boolean make_new) {
        if(make_new) {
            Traversable.Set.Indexed<A> r = new IBBTree<A>(c != this ? c : null);
            if((b == null) || (b.length == 0))
                return r;
            for(int i = 0; i < b.length; i++)
                r.add((A)(b[i]), m);
            return r;
        }
        throw new UnsupportedOperationException(
                "Modifications are not supported");
    }

    public Object[] toArray() {
        if((b == null) || (b.length == 0))
            return new Object[0];
        Object[] c = new Object[b.length];
        System.arraycopy(b, 0, c, 0, b.length);
        return c;
    }

    public <B> B[] toArray(B[] c) {
        B[] r;
        if(c.length >= b.length)
            r = c;
        else
            r = (B[])java.lang.reflect.Array.newInstance(c.getClass().
                    getComponentType(), b.length);
        System.arraycopy(b, 0, r, 0, b.length);
        return r;
    }

    public <B> B[] toArray(Class<B> componentType) {
        B[] r = (B[])java.lang.reflect.Array.newInstance(componentType, b.length);
        System.arraycopy(b, 0, r, 0, b.length);
        return r;
    }

    public boolean isFinal() {
        return true;
    }

    public Final<A> toFinal() {
        return this;
    }

    public Traversable.Set.Indexed<A> copySubSet(Selector<A> f) {
        if((b == null) || (b.length == 0) || (f == null))
            return clone();
        Traversable.Set.Indexed<A> h = new IBBTree<A>(comp != this ? comp : null);
        int l = indexOfCeiled(f.lowerBound()), r = indexOfFloored(f.lowerBound());
        if(l < 0)
            l = 0;
        if(r < 0)
            r = b.length - 1;
        for(int i = l; i <= r; i++)
            if(f.selects((A)(b[i])))
                h.add((A)(b[i]));
        return h;
    }

    public Traversable.Set.Indexed<A> copySubSet(int from, int to) {
        if((from < 0) || (to > size()) || (from > to))
            throw new IndexOutOfBoundsException("Indices are out of bounds");
        if((b == null) || (b.length == 0))
            return clone();
        Traversable.Set.Indexed<A> h = new IBBTree<A>(comp != this ? comp : null);
        for(int i = from; i < to; i++)
            h.add((A)(b[i]));
        return h;
    }

    public Traversable.Set.Indexed<A> copySubSet(A fromElement, A toElement) {
        return copySubSet(fromElement, true, toElement, false);
    }

    public Traversable.Set.Indexed<A> copySubSet(A min, boolean min_included,
                                                 A max, boolean max_included) {
        return copySubSet(new Selector.RangeSelector<A>(min, min_included, max,
                                                        max_included, comp));
    }

    public Traversable.Set.Indexed<A> copyHeadSet(A toElement) {
        return copyHeadSet(toElement, false);
    }

    public Traversable.Set.Indexed<A> copyHeadSet(A max, boolean max_included) {
        return copySubSet(
                new Selector.HalfRangeSelector<A>(max, max_included,
                                                  Selector.LEFT, comp));
    }

    public Traversable.Set.Indexed<A> copyTailSet(A fromElement) {
        return copyTailSet(fromElement, true);
    }

    public Traversable.Set.Indexed<A> copyTailSet(A min, boolean min_included) {
        return copySubSet(new Selector.HalfRangeSelector<A>(min, min_included,
                                                            Selector.RIGHT,
                                                            comp));
    }

    public Traversable.Set<A> descendingSet() {
        return new SubSetStub<A>(this, true);
    }

    public Traversable.Set<A> subSet(Selector<A> f) {
        return new SubSetStub<A>(this, f);
    }

    public Traversable.Set<A> descendingSubSet(Selector<A> f) {
        return new SubSetStub<A>(this, f, true);
    }

    public Traversable.Set<A> subSet(A fromElement, A toElement) {
        return subSet(fromElement, true, toElement, false);
    }

    public Traversable.Set<A> descendingSubSet(A fromElement, A toElement) {
        return descendingSubSet(fromElement, true, toElement, false);
    }

    public Traversable.Set<A> subSet(A min, boolean min_included, A max,
                                     boolean max_included) {
        return subSet(new Selector.RangeSelector<A>(min, min_included, max,
                                                    max_included, comp));
    }

    public Traversable.Set<A> descendingSubSet(A min, boolean min_included,
                                               A max, boolean max_included) {
        return descendingSubSet(new Selector.RangeSelector<A>(min, min_included,
                                                              max, max_included,
                                                              comp));
    }

    public Traversable.Set<A> headSet(A toElement) {
        return headSet(toElement, false);
    }

    public Traversable.Set<A> descendingHeadSet(A toElement) {
        return descendingHeadSet(toElement, false);
    }

    public Traversable.Set<A> headSet(A max, boolean max_included) {
        return subSet(new Selector.HalfRangeSelector<A>(max, max_included,
                                                        Selector.LEFT, comp));
    }

    public Traversable.Set<A> descendingHeadSet(A max, boolean max_included) {
        return descendingSubSet(new Selector.HalfRangeSelector<A>(max,
                                                                  max_included,
                                                                  Selector.LEFT,
                                                                  comp));
    }

    public Traversable.Set<A> tailSet(A fromElement) {
        return tailSet(fromElement, true);
    }

    public Traversable.Set<A> descendingTailSet(A fromElement) {
        return descendingTailSet(fromElement, true);
    }

    public Traversable.Set<A> tailSet(A min, boolean min_included) {
        return subSet(new Selector.HalfRangeSelector<A>(min, min_included,
                                                        Selector.RIGHT, comp));
    }

    public Traversable.Set<A> descendingTailSet(A min, boolean min_included) {
        return descendingSubSet(new Selector.HalfRangeSelector<A>(min,
                                                                  min_included,
                                                                  Selector.RIGHT,
                                                                  comp));
    }

    public Traversable.Set.Indexed<A> subList(int from, int to) {
        if((from < 0) || (to > size()) || (from > to))
            throw new IndexOutOfBoundsException("Indices are out of bounds");
        if(size() == 0)
            return this;
        return new SubSetStub(this, from, to);
    }

    public ResettableListIterator<A> listIterator() {
        return subListIterator(0, 0, size());
    }

    public ResettableListIterator<A> listIterator(int from) {
        return subListIterator(0, from, size());
    }

    public ResettableListIterator<A> subListIterator(int l, int r) {
        return subListIterator(l, l, r);
    }

    public ResettableListIterator<A> subListIterator(int l, int from, int r) {
        if((l < 0) || (r > size()) || (l > r))
            throw new IndexOutOfBoundsException("Indices are out of bounds");
        if((from < l) || (from > r))
            throw new IndexOutOfBoundsException("From-Index is out of bounds");
        return new ArrayIterator<A>(this, l, from, r);
    }

    public ResettableListIterator<A> iterator() {
        return new ArrayIterator<A>(this);
    }

    public ResettableListIterator<A> descendingIterator() {
        return new ArrayIterator<A>(this, true);
    }

    public ResettableIterator<A> subSetIterator(Selector<A> f) {
        return new ArrayIterator<A>(this, f);
    }

    public ResettableIterator<A> descendingSubSetIterator(Selector<A> f) {
        return new ArrayIterator<A>(this, f, true);
    }

    public ResettableIterator<A> subSetIterator(A fromElement, A toElement) {
        return subSetIterator(fromElement, true, toElement, false);
    }

    public ResettableIterator<A> descendingSubSetIterator(A fromElement,
                                                          A toElement) {
        return descendingSubSetIterator(fromElement, true, toElement, false);
    }

    public ResettableIterator<A> subSetIterator(A min, boolean min_included,
                                                A max, boolean max_included) {
        return subSetIterator(new Selector.RangeSelector<A>(min, min_included,
                                                            max, max_included,
                                                            comp));
    }

    public ResettableIterator<A> descendingSubSetIterator(A min,
                                                          boolean min_included,
                                                          A max,
                                                          boolean max_included) {
        return descendingSubSetIterator(new Selector.RangeSelector<A>(
                min, min_included, max, max_included, comp));
    }

    public ResettableIterator<A> headSetIterator(A toElement) {
        return headSetIterator(toElement, false);
    }

    public ResettableIterator<A> descendingHeadSetIterator(A toElement) {
        return descendingHeadSetIterator(toElement, false);
    }

    public ResettableIterator<A> descendingHeadSetIterator(A max,
                                                           boolean max_included) {
        return descendingSubSetIterator(new Selector.HalfRangeSelector<A>(
                max, max_included, Selector.LEFT, comp));
    }

    public ResettableIterator<A> headSetIterator(A max, boolean max_included) {
        return subSetIterator(new Selector.HalfRangeSelector<A>(max,
                                                                max_included,
                                                                Selector.LEFT,
                                                                comp));
    }

    public ResettableIterator<A> tailSetIterator(A fromElement) {
        return tailSetIterator(fromElement, true);
    }

    public ResettableIterator<A> descendingTailSetIterator(A fromElement) {
        return descendingTailSetIterator(fromElement, true);
    }

    public ResettableIterator<A> tailSetIterator(A min, boolean min_included) {
        return subSetIterator(new Selector.HalfRangeSelector<A>(min,
                                                                min_included,
                                                                Selector.RIGHT,
                                                                comp));
    }

    public ResettableIterator<A> descendingTailSetIterator(A min,
                                                           boolean min_included) {
        return descendingSubSetIterator(new Selector.HalfRangeSelector<A>(min,
                                                                          min_included,
                                                                          Selector.RIGHT,
                                                                          comp));
    }

    public boolean equals(Object o) {
        if(o == null)
            return false;
        if(o == this)
            return true;
        if(o instanceof OrderedSet)
            return compareTo((OrderedSet<A>)o) == 0;
        if(o instanceof Set)
            return this.containsAll((Set)o) && ((Set)o).containsAll(this);
        else
            return false;
    }

    public int compareTo(OrderedSet<A> b) {
        return compareTo(b, comp);
    }

    public int compareTo(OrderedSet<A> b, Comparator<? super A> d) {
        if(b == null)
            return 1;
        if(this == b)
            return 0;
        if(b instanceof Traversable) {
            TraversalActionPerformer.IndexedSetComparator<A> sc =
                    TraversalActionPerformer.IndexedSetComparator.getThreadLocal(
                    d);
            if(((Traversable<A>)b).traverse(IN_ORDER, sc, this) == 0)
                return size();
            if((sc.c == 0) && (sc.i < size()))
                return 1;
            return -sc.c;
        }
        int c = 0, i = 0;
        A x = this.first(), y = b.first();
        c = (x == null) ? ((y == null) ? 0 : -1) : ((y == null) ? 1 : d.compare(
                x, y));
        while((c == 0) && (x != null) && (y != null)) {
            if(++i < this.b.length)
                x = (A)this.b[i];
            else
                x = null;
            y = b.higher(y);
            c = (x == null) ? ((y == null) ? 0 : -1) : ((y == null) ? 1 : d.
                    compare(x, y));
        }
        return c;
    }

    public IBBTree<A> clone() {
        IBBTree<A> r = new IBBTree<A>(comp != this ? comp : null);
        if((b == null) || (b.length == 0))
            return r;
        for(int i = 0; i < b.length; i++) {
            r.add((A)(b[i]));
            r.check();
        }
        return r;
    }

    public int hashCode() {
        if((b == null) || (b.length == 0))
            return 0;
        int h = 0;
        for(int i = 0; i < b.length; i++)
            h += b[i].hashCode();
        return h;
    }

    public String toString() {
        if((b == null) || (b.length == 0))
            return "{}";
        return toStringBuilder(null).toString();
    }

    public StringBuilder toStringBuilder(StringBuilder sb) {
        if(sb == null)
            sb = new StringBuilder();
        if((b == null) || (b.length == 0))
            return sb.append("{}");
        sb.append('{');
        for(int i = 0; i < b.length; i++) {
            sb.append(b[i]);
            if(i < b.length - 1)
                sb.append(',');
        }
        return sb.append('}');
    }

    public int compare(A x, A y) {
        return comp.compare(x, y);
    }

    public <B> int asymmetricCompare(A x, B y) {
        return ((AsymmetricComparator<A>)comp).asymmetricCompare(x, y);
    }

    public Comparator<? super A> comparator() {
        return comp != Utils.NATURAL_COMPARATOR ? comp : null;
    }

    public boolean selects(A x) {
        return contains(x);
    }

    public A upperBound() {
        return last();
    }

    public A lowerBound() {
        return first();
    }

    public void writeToStream(Writer w) throws IOException {
        if(w == null)
            return;
        w.write('{');
        if((b != null) && (b.length != 0))
            for(int i = 0; i < b.length; i++) {
                if(b[i] != null)
                    if(b[i] instanceof Writeable)
                        ((Writeable)(b[i])).writeToStream(w);
                    else
                        w.write(b[i].toString());
                if(i + 1 < b.length)
                    w.write(',');
            }
        w.write('}');
    }

    public int traverse(int order, TraversalActionPerformer<? super A> p) {
        return traverse(order, p, null);
    }

    public int traverse(int order, TraversalActionPerformer<? super A> p,
                        Object o) {
        if((p == null) || (b == null) || (b.length == 0))
            return 0;
        return traverseRange(order, p, o, 0, b.length);
    }

    public int traverseSelected(int order, TraversalActionPerformer<? super A> p,
                                Selector<A> s) {
        return traverseSelected(order, p, null, s);
    }

    public int traverseSelected(int order, TraversalActionPerformer<? super A> p,
                                Object o, Selector<A> s) {
        if((p == null) || (b == null) || (b.length == 0))
            return 0;
        return traverseSelectedRange(order, p, null, s, 0, b.length);
    }

    public int traverseRange(int order, TraversalActionPerformer<? super A> p,
                             int l, int r) {
        return traverseSelectedRange(order, p, null, null, l, r);
    }

    public int traverseRange(int order, TraversalActionPerformer<? super A> p,
                             Object o, int l, int r) {
        return traverseSelectedRange(order, p, o, null, l, r);
    }

    public int traverseSelectedRange(int order,
                                     TraversalActionPerformer<? super A> p,
                                     Selector<A> s, int l, int r) {
        return traverseSelectedRange(order, p, null, s, l, r);
    }

    public int traverseSelectedRange(int order,
                                     TraversalActionPerformer<? super A> p,
                                     Object o, Selector<A> s, int l, int r) {
        if((p == null) || (b == null) || (b.length == 0))
            return 0;
        if((l < 0) || (r > b.length) || (l > r))
            throw new IndexOutOfBoundsException("Indices are out of bounds");
        int sl, sr;
        if(s != null) {
            sl = indexOfCeiled(s.lowerBound());
            sr = indexOfFloored(s.upperBound());
            if(sl < l)
                sl = l;
            if((sr < 0) || (sr >= r))
                sr = r - 1;
        } else {
            sl = l;
            sr = r - 1;
        }
        if(order == IN_ORDER) {
            int ks = 0;
            int k = sl;
            while(k <= sr) {
                if((s == null) || s.selects((A)(b[k]))) {
                    ks++;
                    if(p.perform((A)(b[k]), IN_ORDER, o))
                        return -ks;
                }
                k++;
            }
            return ks;
        }
        if(order == (REVERSE_ORDER | IN_ORDER)) {
            int ks = 0;
            int k = sr;
            while(k >= sl) {
                if((s == null) || s.selects((A)(b[k]))) {
                    ks++;
                    if(p.perform((A)(b[k]), IN_ORDER, o))
                        return -ks;
                }
                k--;
            }
            return ks;
        }
        return ptraverseSelected(sl, sr, order, p, o, s);
    }

    public int traverseRange(int order, TraversalActionPerformer<? super A> p,
                             A l, A r) {
        return traverseSelectedRange(order, p, null, null, l, true, r, false);
    }

    public int traverseRange(int order, TraversalActionPerformer<? super A> p,
                             Object o, A l, A r) {
        return traverseSelectedRange(order, p, o, null, l, true, r, false);
    }

    public int traverseRange(int order, TraversalActionPerformer<? super A> p,
                             A l, boolean left_included, A r,
                             boolean right_included) {
        return traverseSelectedRange(order, p, null, null, l, left_included, r,
                                     right_included);
    }

    public int traverseRange(int order, TraversalActionPerformer<? super A> p,
                             Object o, A l, boolean left_included, A r,
                             boolean right_included) {
        return traverseSelectedRange(order, p, o, null, l, left_included, r,
                                     right_included);
    }

    public int traverseSelectedRange(int order,
                                     TraversalActionPerformer<? super A> p,
                                     Selector<A> s, A l, A r) {
        return traverseSelectedRange(order, p, null, s, l, true, r, false);
    }

    public int traverseSelectedRange(int order,
                                     TraversalActionPerformer<? super A> p,
                                     Object o, Selector<A> s, A l, A r) {
        return traverseSelectedRange(order, p, o, s, l, true, r, false);
    }

    public int traverseSelectedRange(int order,
                                     TraversalActionPerformer<? super A> p,
                                     Selector<A> s, A l, boolean left_included,
                                     A r, boolean right_included) {
        return traverseSelectedRange(order, p, null, s, l, left_included, r,
                                     right_included);
    }

    public int traverseSelectedRange(int order,
                                     TraversalActionPerformer<? super A> p,
                                     Object o, Selector<A> s, A l,
                                     boolean left_included, A r,
                                     boolean right_included) {
        if((p == null) || (b == null) || (b.length == 0))
            return 0;
        int sl, sr;
        if(s != null) {
            sl = left_included ? indexOfCeiled(l) : indexOfHigher(l);
            sr = right_included ? indexOfFloored(r) : indexOfLower(r);
            if(sl < 0)
                sl = 0;
            if((sr < 0) || (sr >= b.length))
                sr = b.length - 1;
            if(sl > sr)
                return 0;
        } else {
            sl = 0;
            sr = b.length - 1;
        }
        return traverseSelectedRange(order, p, o, s, sl, sr + 1);
    }
//    public int traverseComparableRange(int order,TraversalActionPerformer<? super A> p,
//                    Comparable<? super A> l,Comparable<? super A> r) {
//        return(traverseSelectedComparableRange(order,p,null,null,l,true,r,false));
//    }
//    public int traverseComparableRange(int order,TraversalActionPerformer<? super A> p,
//                    Object o,Comparable<? super A> l,Comparable<? super A> r) {
//        return(traverseSelectedComparableRange(order,p,o,null,l,true,r,false));
//    }
//    public int traverseComparableRange(int order,TraversalActionPerformer<? super A> p,
//                    Comparable<? super A> l,boolean left_included,
//                            Comparable<? super A> r,boolean right_included) {
//        return(traverseSelectedComparableRange(order,p,null,null,
//                                            l,left_included,r,right_included));
//    }
//    public int traverseComparableRange(int order,TraversalActionPerformer<? super A> p,
//                    Object o,Comparable<? super A> l,boolean left_included,
//                            Comparable<? super A> r,boolean right_included) {
//        return(traverseSelectedComparableRange(order,p,o,null,
//                                            l,left_included,r,right_included));
//    }
//    public int traverseSelectedComparableRange(int order,TraversalActionPerformer<? super A> p,
//                    Selector<A> s,Comparable<? super A> l,Comparable<? super A> r) {
//        return(traverseSelectedComparableRange(order,p,null,s,l,true,r,false));
//    }
//    public int traverseSelectedComparableRange(int order,TraversalActionPerformer<? super A> p,
//                    Object o,Selector<A> s,Comparable<? super A> l,Comparable<? super A> r) {
//        return(traverseSelectedComparableRange(order,p,o,s,l,true,r,false));
//    }
//    public int traverseSelectedComparableRange(int order,TraversalActionPerformer<? super A> p,
//                    Selector<A> s,Comparable<? super A> l,boolean left_included,
//                            Comparable<? super A> r,boolean right_included) {
//        return(traverseSelectedComparableRange(order,p,null,s,
//                                            l,left_included,r,right_included));
//    }
//    public int traverseSelectedComparableRange(int order,TraversalActionPerformer<? super A> p,
//                    Object o,Selector<A> s,Comparable<? super A> l,boolean left_included,
//                            Comparable<? super A> r,boolean right_included) {
//        if((p==null)||(b==null)||(b.length==0))
//            return(0);
//        int sl,sr;
//        if(s!=null) {
//            sl = left_included?indexOfCeiledComparable(l):indexOfHigherComparable(l);
//            sr = right_included?indexOfFlooredComparable(r):indexOfLowerComparable(r);
//            if(sl<0)
//                sl = 0;
//            if((sr<0)||(sr>=b.length))
//                sr = b.length-1;
//            if(sl>sr)
//                return(0);
//        } else {
//            sl = 0;
//            sr = b.length-1;
//        }
//        return(traverseSelectedRange(order,p,o,s,sl,sr+1));
//    }
//    public <B> int traverseMatchingRange(int order,TraversalActionPerformer<? super A> p,
//                    B l,B r) {
//        return(traverseSelectedMatchingRange(order,p,null,null,l,true,r,false));
//    }
//    public <B> int traverseMatchingRange(int order,TraversalActionPerformer<? super A> p,
//                    Object o,B l,B r) {
//        return(traverseSelectedMatchingRange(order,p,o,null,l,true,r,false));
//    }
//    public <B> int traverseMatchingRange(int order,TraversalActionPerformer<? super A> p,
//                    B l,boolean left_included,B r,boolean right_included) {
//        return(traverseSelectedMatchingRange(order,p,null,null,
//                                            l,left_included,r,right_included));
//    }
//    public <B> int traverseMatchingRange(int order,TraversalActionPerformer<? super A> p,
//                    Object o,B l,boolean left_included,B r,boolean right_included) {
//        return(traverseSelectedMatchingRange(order,p,o,null,
//                                            l,left_included,r,right_included));
//    }
//    public <B> int traverseSelectedMatchingRange(int order,TraversalActionPerformer<? super A> p,
//                    Selector<A> s,B l,B r) {
//        return(traverseSelectedMatchingRange(order,p,null,s,l,true,r,false));
//    }
//    public <B> int traverseSelectedMatchingRange(int order,TraversalActionPerformer<? super A> p,
//                    Object o,Selector<A> s,B l,B r) {
//        return(traverseSelectedMatchingRange(order,p,o,s,l,true,r,false));
//    }
//    public <B> int traverseSelectedMatchingRange(int order,TraversalActionPerformer<? super A> p,
//                    Selector<A> s,B l,boolean left_included,B r,boolean right_included) {
//        return(traverseSelectedMatchingRange(order,p,null,s,
//                                            l,left_included,r,right_included));
//    }
//    public <B> int traverseSelectedMatchingRange(int order,TraversalActionPerformer<? super A> p,
//                    Object o,Selector<A> s,B l,boolean left_included,B r,boolean right_included) {
//        if((p==null)||(b==null)||(b.length==0))
//            return(0);
//        int sl,sr;
//        if(s!=null) {
//            sl = left_included?indexOfCeiledMatch(l):indexOfHigherMatch(l);
//            sr = right_included?indexOfFlooredMatch(r):indexOfLowerMatch(r);
//            if(sl<0)
//                sl = 0;
//            if((sr<0)||(sr>=b.length))
//                sr = b.length-1;
//            if(sl>sr)
//                return(0);
//        } else {
//            sl = 0;
//            sr = b.length-1;
//        }
//        return(traverseSelectedRange(order,p,o,s,sl,sr+1));
//    }

    private int ptraverseSelected(int l, int r, int traverse_order,
                                  TraversalActionPerformer traverse_action,
                                  Object o, Selector<A> s) {
        if(l > r)
            return 0;
        int a = 1, m = (l + r) >>> 1, h;
        boolean sel = s != null ? s.selects((A)(b[m])) : true;
        if(sel && ((traverse_order & PRE_ORDER) != 0))
            if(traverse_action.perform((A)(b[m]), PRE_ORDER, o))
                return -a;
        if((traverse_order & REVERSE_ORDER) == 0) {
            h = ptraverseSelected(l, m - 1, traverse_order, traverse_action, o,
                                  s);
            if(h < 0)
                return h - a;
            a += h;
            if(sel && ((traverse_order & IN_ORDER) != 0))
                if(traverse_action.perform((A)(b[m]), IN_ORDER, o))
                    return -a;
            h = ptraverseSelected(m + 1, r, traverse_order, traverse_action, o,
                                  s);
            if(h < 0)
                return h - a;
            a += h;
        } else {
            h = ptraverseSelected(m + 1, r, traverse_order, traverse_action, o,
                                  s);
            if(h < 0)
                return h - a;
            a += h;
            if(sel && ((traverse_order & IN_ORDER) != 0))
                if(traverse_action.perform((A)(b[m]), IN_ORDER, o))
                    return -a;
            h = ptraverseSelected(l, m - 1, traverse_order, traverse_action, o,
                                  s);
            if(h < 0)
                return h - a;
            a += h;
        }
        if(sel && ((traverse_order & POST_ORDER) != 0))
            if(traverse_action.perform((A)(b[m]), POST_ORDER, o))
                return -a;
        return a;
    }

    private static class SubSetStub<A> implements Traversable.Set.Indexed<A> {

        private static final long serialVersionUID = 0;
        private final Final<A> s;
        private final Selector<A> f;
        private final boolean reverseOrder;
        private final int l, r, count;

        public SubSetStub(Final<A> s) {
            this(s, null, false);
        }

        public SubSetStub(Final<A> s, boolean reverseOrder) {
            this(s, null, reverseOrder);
        }

        public SubSetStub(Final<A> s, Selector<A> f) {
            this(s, f, false);
        }

        public SubSetStub(Final<A> s, Selector<A> f, boolean reverseOrder) {
            if(s == null)
                throw new NullPointerException("Set is null");
            this.s = s;
            if(f != null)
                this.f = f;
            else
                this.f = Selector.Accepter;
            A y = f.lowerBound();
            int i = s.indexOfCeiled(y);
            if(i < 0)
                this.l = 0;
            else if(f.selects(y))
                this.l = i;
            else
                this.l = i + 1;
            y = f.upperBound();
            i = s.indexOfFloored(y);
            if(i < 0)
                this.r = s.size();
            else if(f.selects(y))
                this.r = i + 1;
            else
                this.r = i;
            int c = 0;
            for(i = l; i < r; i++)
                if(f.selects(s.get(i)))
                    c++;
            count = c;
            this.reverseOrder = reverseOrder;
        }

        public SubSetStub(Final<A> s, int l, int r) {
            this(s, l, r, false);
        }

        public SubSetStub(Final<A> s, int l, int r, boolean reverseOrder) {
            if(s == null)
                throw new NullPointerException("Set is null");
            this.s = s;
            if((l < 0) || (r > s.size()) || (l > r))
                throw new IndexOutOfBoundsException("Indices are out of bounds");
            this.l = l;
            this.r = r;
            count = r - l;
            this.reverseOrder = reverseOrder;
            this.f = Selector.Accepter;
        }

        public boolean contains(Object o) {
            return f.selects((A)o) && s.contains(o);
        }

        public boolean containsAll(Collection<?> c) {
            if((c == null) || (c == this))
                return true;
            if(c instanceof Traversable) {
                TraversalActionPerformer.LogicalJunctor<Object> a =
                        new TraversalActionPerformer.LogicalJunctor<Object>(
                        TraversalActionPerformer.LOGICAL_AND,
                        (Selector<Object>)this);
                ((Traversable<Object>)c).traverse(IN_ORDER, a);
                return a.getState();
            }
            if(s != null) {
                for(Object o : c)
                    if(!((Selector<Object>)s).selects(o) || !contains(o))
                        return false;
            } else
                for(Object o : c)
                    if(!contains(o))
                        return false;
            return true;
        }

        public boolean isContainedIn(Collection<?> c) {
            if(c == null)
                return false;
            if(c == this)
                return true;
            for(int i = l; i < r; i++)
                if(f.selects((A)(s.b[i])) && !c.contains(s.b[i]))
                    return false;
            return true;
        }

        public boolean add(A x) {
            throw new UnsupportedOperationException(
                    "Modifications are not supported");
        }

        public void add(int i, A x) {
            throw new UnsupportedOperationException(
                    "Modifications are not supported");
        }

        public boolean add(A x, Merger<A> m) {
            throw new UnsupportedOperationException(
                    "Modifications are not supported");
        }

        public boolean addAll(Collection<? extends A> c, Merger<A> m) {
            throw new UnsupportedOperationException(
                    "Modifications are not supported");
        }

        public boolean addAll(Collection<? extends A> c, Selector<? super A> s,
                              boolean abort_on_illegal_argument) {
            throw new UnsupportedOperationException(
                    "Modifications are not supported");
        }

        public boolean addAll(Collection<? extends A> c, Selector<? super A> s,
                              boolean abort_on_illegal_argument, Merger<A> m) {
            throw new UnsupportedOperationException(
                    "Modifications are not supported");
        }

        public A addOrGet(A x) {
            if(x == null)
                return null;
            A r = get(x);
            if(r == null)
                throw new UnsupportedOperationException(
                        "Modifications are not supported");
            return r;
        }

        public <B> A addOrGetMatch(B x, KeyedElementFactory<A, B> f) {
            if(x == null)
                return null;
            A r = getMatch(x);
            if(r == null)
                throw new UnsupportedOperationException(
                        "Modifications are not supported");
            return r;
        }

        public boolean addAll(Collection<? extends A> c) {
            throw new UnsupportedOperationException(
                    "Modifications are not supported");
        }

        public boolean addAll(int i, Collection<? extends A> c) {
            throw new UnsupportedOperationException(
                    "Modifications are not supported");
        }

        public A pollFirst() {
            throw new UnsupportedOperationException(
                    "Modifications are not supported");
        }

        public A pollLast() {
            throw new UnsupportedOperationException(
                    "Modifications are not supported");
        }

        public boolean remove(Object o) {
            throw new UnsupportedOperationException(
                    "Modifications are not supported");
        }

        public A remove(int i) {
            throw new UnsupportedOperationException(
                    "Modifications are not supported");
        }

        public A removeComparable(Comparable<? super A> x) {
            throw new UnsupportedOperationException(
                    "Modifications are not supported");
        }

        public <B> A removeMatch(B x) {
            throw new UnsupportedOperationException(
                    "Modifications are not supported");
        }

        public boolean removeAll(Collection<?> c) {
            throw new UnsupportedOperationException(
                    "Modifications are not supported");
        }

        public boolean removeAll(Selector<? super A> t) {
            throw new UnsupportedOperationException(
                    "Modifications are not supported");
        }

        public boolean retainAll(Collection<?> c) {
            throw new UnsupportedOperationException(
                    "Modifications are not supported");
        }

        public boolean retainAll(Selector<? super A> t) {
            throw new UnsupportedOperationException(
                    "Modifications are not supported");
        }

        public boolean isEmpty() {
            return count == 0;
        }

        public void clear() {
            throw new UnsupportedOperationException(
                    "Modifications are not supported");
        }

        public int size() {
            return count;
        }

        public int maxSearchDepth() {
            return s.maxSearchDepth();
        }

        public A getComparable(Comparable<? super A> x) {
            if(x == null)
                return null;
            A y = s.getComparable(x);
            if((y == null) || !f.selects(y))
                return null;
            return y;
        }

        public <B> A getMatch(B x) {
            if(x == null)
                return null;
            A y = s.getMatch(x);
            if((y == null) || !f.selects(y))
                return null;
            return y;
        }

        public A get(A x) {
            if(x == null)
                return null;
            A y = s.get(x);
            if((y == null) || !f.selects(y))
                return null;
            return y;
        }

        public A get(int i) {
            if((i < 0) || (i >= count))
                throw new IndexOutOfBoundsException(
                        "Index " + i + " is out of bounds");
            A y = s.get(i + l);
            if(f.selects(y))
                return y;
            else
                throw new IllegalArgumentException(
                        "Element is not selected by Selector");
        }

        public A set(A x) {
            throw new UnsupportedOperationException(
                    "Modifications are not supported");
        }

        public A set(int i, A x) {
            throw new UnsupportedOperationException(
                    "Modifications are not supported");
        }

        public A first() {
            if(count == 0)
                return null;
            return reverseOrder ? psearchPrev(r - 1) : psearchNext(l);
        }

        public A last() {
            if(count == 0)
                return null;
            return reverseOrder ? psearchNext(l) : psearchPrev(r - 1);
        }

        public A higher(A x) {
            if(count == 0)
                return null;
            if(x == null)
                return reverseOrder ? last() : first();
            return reverseOrder ? psearchPrev(s.indexOfLower(x))
                   : psearchNext(s.indexOfHigher(x));
        }

        public A higherComparable(Comparable<? super A> x) {
            if(count == 0)
                return null;
            if(x == null)
                return reverseOrder ? last() : first();
            return reverseOrder ? psearchPrev(s.indexOfLowerComparable(x))
                   : psearchNext(s.indexOfHigherComparable(x));
        }

        public <B> A higherMatch(B x) {
            if(count == 0)
                return null;
            if(x == null)
                return reverseOrder ? last() : first();
            return reverseOrder ? psearchPrev(s.indexOfLowerMatch(x))
                   : psearchNext(s.indexOfHigherMatch(x));
        }

        public A lower(A x) {
            if(count == 0)
                return null;
            if(x == null)
                return reverseOrder ? first() : last();
            return reverseOrder ? psearchNext(s.indexOfHigher(x))
                   : psearchPrev(s.indexOfLower(x));
        }

        public A lowerComparable(Comparable<? super A> x) {
            if(count == 0)
                return null;
            if(x == null)
                return reverseOrder ? first() : last();
            return reverseOrder ? psearchNext(s.indexOfHigherComparable(x))
                   : psearchPrev(s.indexOfLowerComparable(x));
        }

        public <B> A lowerMatch(B x) {
            if(count == 0)
                return null;
            if(x == null)
                return reverseOrder ? first() : last();
            return reverseOrder ? psearchNext(s.indexOfHigherMatch(x))
                   : psearchPrev(s.indexOfLowerMatch(x));
        }

        public A floor(A x) {
            if(count == 0)
                return null;
            if(x == null)
                return reverseOrder ? first() : last();
            return reverseOrder ? psearchNext(s.indexOfCeiled(x))
                   : psearchPrev(s.indexOfFloored(x));
        }

        public A floorComparable(Comparable<? super A> x) {
            if(count == 0)
                return null;
            if(x == null)
                return reverseOrder ? first() : last();
            return reverseOrder ? psearchNext(s.indexOfCeiledComparable(x))
                   : psearchPrev(s.indexOfFlooredComparable(x));
        }

        public <B> A floorMatch(B x) {
            if(count == 0)
                return null;
            if(x == null)
                return reverseOrder ? first() : last();
            return reverseOrder ? psearchNext(s.indexOfCeiledMatch(x))
                   : psearchPrev(s.indexOfFlooredMatch(x));
        }

        public A ceiling(A x) {
            if(count == 0)
                return null;
            if(x == null)
                return reverseOrder ? last() : first();
            return reverseOrder ? psearchPrev(s.indexOfFloored(x))
                   : psearchNext(s.indexOfCeiled(x));
        }

        public A ceilComparable(Comparable<? super A> x) {
            if(count == 0)
                return null;
            if(x == null)
                return reverseOrder ? last() : first();
            return reverseOrder ? psearchPrev(s.indexOfFlooredComparable(x))
                   : psearchNext(s.indexOfCeiledComparable(x));
        }

        public <B> A ceilMatch(B x) {
            if(count == 0)
                return null;
            if(x == null)
                return reverseOrder ? last() : first();
            return reverseOrder ? psearchPrev(s.indexOfFlooredMatch(x))
                   : psearchNext(s.indexOfCeiledMatch(x));
        }

        public int indexOf(Object x) {
            if((x == null) || !f.selects((A)x))
                return -1;
            int i = s.indexOf((A)x);
            if((i < l) || (i >= r))
                return -1;
            return i - l;
        }

        public int lastIndexOf(Object x) {
            return indexOf(x);
        }

        public int indexOfComparable(Comparable<? super A> x) {
            if(x == null)
                return -1;
            int i = s.indexOfComparable(x);
            if((i < l) || (i >= r))
                return -1;
            if(!f.selects(s.get(i)))
                return -1;
            return i - l;
        }

        public <B> int indexOfMatch(B x) {
            if(x == null)
                return -1;
            int i = s.indexOfMatch(x);
            if((i < l) || (i >= r))
                return -1;
            if(!f.selects(s.get(i)))
                return -1;
            return i - l;
        }

        public int indexOfHigher(A x) {
            if(count == 0)
                return -1;
            if(x == null)
                return reverseOrder ? r - 1 : l;
            int i = reverseOrder ? psearchPrevIndex(s.indexOfLower(x))
                    : psearchNextIndex(s.indexOfHigher(x));
            if(i < 0)
                return -1;
            return i - l;
        }

        public int indexOfHigherComparable(Comparable<? super A> x) {
            if(count == 0)
                return -1;
            if(x == null)
                return reverseOrder ? r - 1 : l;
            int i = reverseOrder ? psearchPrevIndex(s.indexOfLowerComparable(x))
                    : psearchNextIndex(s.indexOfHigherComparable(x));
            if(i < 0)
                return -1;
            return i - l;
        }

        public <B> int indexOfHigherMatch(B x) {
            if(count == 0)
                return -1;
            if(x == null)
                return reverseOrder ? r - 1 : l;
            int i = reverseOrder ? psearchPrevIndex(s.indexOfLowerMatch(x))
                    : psearchNextIndex(s.indexOfHigherMatch(x));
            if(i < 0)
                return -1;
            return i - l;
        }

        public int indexOfLower(A x) {
            if(count == 0)
                return -1;
            if(x == null)
                return reverseOrder ? l : r - 1;
            int i = reverseOrder ? psearchNextIndex(s.indexOfHigher(x))
                    : psearchPrevIndex(s.indexOfLower(x));
            if(i < 0)
                return -1;
            return i - l;
        }

        public int indexOfLowerComparable(Comparable<? super A> x) {
            if(count == 0)
                return -1;
            if(x == null)
                return reverseOrder ? l : r - 1;
            int i = reverseOrder ? psearchNextIndex(s.indexOfHigherComparable(x))
                    : psearchPrevIndex(s.indexOfLowerComparable(x));
            if(i < 0)
                return -1;
            return i - l;
        }

        public <B> int indexOfLowerMatch(B x) {
            if(count == 0)
                return -1;
            if(x == null)
                return reverseOrder ? l : r - 1;
            int i = reverseOrder ? psearchNextIndex(s.indexOfHigherMatch(x))
                    : psearchPrevIndex(s.indexOfLowerMatch(x));
            if(i < 0)
                return -1;
            return i - l;
        }

        public int indexOfFloored(A x) {
            if(count == 0)
                return -1;
            if(x == null)
                return reverseOrder ? l : r - 1;
            int i = reverseOrder ? psearchNextIndex(s.indexOfCeiled(x))
                    : psearchPrevIndex(s.indexOfFloored(x));
            if(i < 0)
                return -1;
            return i - l;
        }

        public int indexOfFlooredComparable(Comparable<? super A> x) {
            if(count == 0)
                return -1;
            if(x == null)
                return reverseOrder ? l : r - 1;
            int i = reverseOrder ? psearchNextIndex(s.indexOfCeiledComparable(x))
                    : psearchPrevIndex(s.indexOfFlooredComparable(x));
            if(i < 0)
                return -1;
            return i - l;
        }

        public <B> int indexOfFlooredMatch(B x) {
            if(count == 0)
                return -1;
            if(x == null)
                return reverseOrder ? l : r - 1;
            if(count == 0)
                return -1;
            int i = reverseOrder ? psearchNextIndex(s.indexOfCeiledMatch(x))
                    : psearchPrevIndex(s.indexOfFlooredMatch(x));
            if(i < 0)
                return -1;
            return i - l;
        }

        public int indexOfCeiled(A x) {
            if(count == 0)
                return -1;
            if(x == null)
                return reverseOrder ? r - 1 : l;
            int i = reverseOrder ? psearchPrevIndex(s.indexOfFloored(x))
                    : psearchNextIndex(s.indexOfCeiled(x));
            if(i < 0)
                return -1;
            return i - l;
        }

        public int indexOfCeiledComparable(Comparable<? super A> x) {
            if(count == 0)
                return -1;
            if(x == null)
                return reverseOrder ? r - 1 : l;
            int i = reverseOrder ? psearchPrevIndex(
                    s.indexOfFlooredComparable(x))
                    : psearchNextIndex(s.indexOfCeiledComparable(x));
            if(i < 0)
                return -1;
            return i - l;
        }

        public <B> int indexOfCeiledMatch(B x) {
            if(count == 0)
                return -1;
            if(x == null)
                return reverseOrder ? r - 1 : l;
            int i = reverseOrder ? psearchPrevIndex(s.indexOfFlooredMatch(x))
                    : psearchNextIndex(s.indexOfCeiledMatch(x));
            if(i < 0)
                return -1;
            return i - l;
        }

        private A psearchNext(int i) {
            if((i < 0) || (i >= r))
                return null;
            if(i < l)
                i = l;
            A y = s.get(i);
            while(!f.selects(y)) {
                if(++i >= r)
                    return null;
                y = s.get(i);
            }
            return y;
        }

        private A psearchPrev(int i) {
            if(i < l)
                return null;
            if(i >= r)
                i = r - 1;
            A y = s.get(i);
            while(!f.selects(y)) {
                if(--i >= r)
                    return null;
                y = s.get(i);
            }
            return y;
        }

        private int psearchNextIndex(int i) {
            if((i < 0) || (i >= r))
                return -1;
            if(i < l)
                i = l;
            while(!f.selects(s.get(i)))
                if(++i >= r)
                    return -1;
            return i;
        }

        private int psearchPrevIndex(int i) {
            if(i < l)
                return -1;
            if(i >= r)
                i = r - 1;
            while(!f.selects(s.get(i)))
                if(--i < l)
                    return -1;
            return i;
        }

        public boolean subSetOf(Collection<?> c) {
            return isContainedIn(c);
        }

        public boolean superSetOf(Collection<?> c) {
            return containsAll(c);
        }

        public Traversable.Set.Indexed<A> join(Collection<? extends A> c) {
            IBBTree<A> r = this.clone();
            r.addAll(c);
            return r;
        }

        public Traversable.Set.Indexed<A> intersect(Collection<? super A> c) {
            IBBTree<A> r = new IBBTree(s.comparator());
            r.addAll(s, new Selector.Conjunctor(f,
                                                (c instanceof OrderedSet) ? (OrderedSet<A>)c
                                                : new Selector.ContainmentChecker(
                    c)), false, null);
            return r;
        }

        public Traversable.Set.Indexed<A> without(Collection<? super A> c) {
            IBBTree<A> r = new IBBTree(s.comparator());
            r.addAll(s, new Selector.Conjunctor(f,
                                                new Selector.Inverter<A>((c instanceof OrderedSet) ? (OrderedSet<A>)c
                                                                         : new Selector.ContainmentChecker(
                    c))), false, null);
            return r;
        }

        public Traversable.Set.Indexed<A> reorder(Comparator<? super A> c,
                                                  boolean make_new) {
            return reorder(c, null, make_new);
        }

        public Traversable.Set.Indexed<A> reorder(Comparator<? super A> c,
                                                  Merger<A> m,
                                                  boolean make_new) {
            if(make_new) {
                IBBTree<A> r = c == s
                               ? new IBBTree<A>((Comparator<A>)null) : c == this
                                                                       ? new IBBTree<A>(
                        (Comparator<A>)null, reverseOrder) : new IBBTree<A>(c);
                r.addAll(s, f, false, m);
                return r;
            }
            throw new UnsupportedOperationException(
                    "Modifications are not supported");
        }

        public Object[] toArray() {
            int i = 0;
            Object[] a = new Object[count];
            for(A x : this)
                a[i++] = x;
            return a;
        }

        public <B> B[] toArray(B[] b) {
            int l = count, i = 0;
            B[] a = l >= b.length ? (B[])java.lang.reflect.Array.newInstance(
                    b.getClass().getComponentType(), l) : b;
            for(A x : this)
                a[i++] = (B)x;
            if(i < a.length)
                a[i] = null;
            return a;
        }

        public <B> B[] toArray(Class<B> componentType) {
            int i = 0;
            B[] a = (B[])java.lang.reflect.Array.newInstance(
                    componentType, count);
            for(A x : this)
                a[i++] = (B)x;
            if(i < a.length)
                a[i] = null;
            return a;
        }

        public boolean isFinal() {
            return true;
        }

        public Final<A> toFinal() {
            return new Final(this);
        }

        public Traversable.Set.Indexed<A> copySubSet(Selector<A> g) {
            IBBTree<A> r = new IBBTree(
                    s.comparator() != s ? s.comparator() : null, reverseOrder);
            Iterator<A> it = subSetIterator(g);
            while(it.hasNext())
                r.add(it.next());
            return r;
        }

        public Traversable.Set.Indexed<A> copySubSet(int from, int to) {
            if((from < 0) || (to > count) || (from > to))
                throw new IndexOutOfBoundsException("Indices are out of bounds");
            IBBTree<A> r = new IBBTree(
                    s.comparator() != s ? s.comparator() : null, reverseOrder);
            Iterator<A> it = listIterator(from);
            int len = to - from;
            while(it.hasNext() && (len > 0)) {
                r.add(it.next());
                len--;
            }
            if(len != 0)
                throw new RuntimeException("Could not collect enough elements");
            return r;
        }

        public Traversable.Set.Indexed<A> copySubSet(A fromElement, A toElement) {
            return copySubSet(fromElement, true, toElement, false);
        }

        public Traversable.Set.Indexed<A> copySubSet(A min, boolean min_included,
                                                     A max, boolean max_included) {
            return copySubSet(new RangeSelector<A>(
                    min, min_included, max, max_included, s.comparator()));
        }

        public Traversable.Set.Indexed<A> copyHeadSet(A toElement) {
            return copyHeadSet(toElement, false);
        }

        public Traversable.Set.Indexed<A> copyHeadSet(A max,
                                                      boolean max_included) {
            return copySubSet(new Selector.HalfRangeSelector<A>(
                    max, max_included, Selector.LEFT, s.comparator()));
        }

        public Traversable.Set.Indexed<A> copyTailSet(A fromElement) {
            return copyTailSet(fromElement, true);
        }

        public Traversable.Set.Indexed<A> copyTailSet(A min,
                                                      boolean min_included) {
            return copySubSet(new Selector.HalfRangeSelector<A>(
                    min, min_included, Selector.RIGHT, s.comparator()));
        }

        public Traversable.Set<A> descendingSet() {
            return descendingSubSet(null);
        }

        public Traversable.Set<A> subSet(Selector<A> g) {
            if(count == 0)
                return this;
            if(g != null)
                return new SubSetStub(s, new Selector.Conjunctor<A>(
                        f == Selector.Accepter
                        ? new Selector.RangeSelector<A>(
                        s.get(l), true, s.get(r - 1), true, s.comparator())
                        : f, reverseOrder ? new Selector.Reverser<A>(g) : g),
                                      reverseOrder);
            else
                return this;
        }

        public Traversable.Set<A> descendingSubSet(Selector<A> g) {
            if(g != null)
                return new SubSetStub(s, new Selector.Conjunctor<A>(
                        f == Selector.Accepter
                        ? new Selector.RangeSelector<A>(
                        s.get(l), true, s.get(r - 1), true, s.comparator())
                        : f, reverseOrder ? g : new Selector.Reverser<A>(g)),
                                      !reverseOrder);
            else if(f != Selector.Accepter)
                return new SubSetStub(s, f, !reverseOrder);
            else
                return new SubSetStub(s, l, r, !reverseOrder);
        }

        public Traversable.Set<A> subSet(A fromElement, A toElement) {
            return subSet(fromElement, true, toElement, false);
        }

        public Traversable.Set<A> descendingSubSet(A fromElement, A toElement) {
            return descendingSubSet(fromElement, true, toElement, false);
        }

        public Traversable.Set<A> subSet(A min, boolean min_included, A max,
                                         boolean max_included) {
            return subSet(new Selector.RangeSelector<A>(
                    min, min_included, max, max_included, s.comparator()));
        }

        public Traversable.Set<A> descendingSubSet(A min, boolean min_included,
                                                   A max, boolean max_included) {
            return descendingSubSet(new Selector.RangeSelector<A>(
                    min, min_included, max, max_included, s.comparator()));
        }

        public Traversable.Set<A> headSet(A toElement) {
            return headSet(toElement, false);
        }

        public Traversable.Set<A> descendingHeadSet(A toElement) {
            return descendingHeadSet(toElement, false);
        }

        public Traversable.Set<A> headSet(A max, boolean max_included) {
            return subSet(new Selector.HalfRangeSelector<A>(
                    max, max_included, Selector.LEFT, s.comparator()));
        }

        public Traversable.Set<A> descendingHeadSet(A max, boolean max_included) {
            return descendingSubSet(new Selector.HalfRangeSelector<A>(
                    max, max_included, Selector.LEFT, s.comparator()));
        }

        public Traversable.Set<A> tailSet(A fromElement) {
            return subSet(new Selector.HalfRangeSelector<A>(
                    fromElement, true, Selector.RIGHT, s.comparator()));
        }

        public Traversable.Set<A> descendingTailSet(A fromElement) {
            return descendingSubSet(new Selector.HalfRangeSelector<A>(
                    fromElement, true, Selector.RIGHT, s.comparator()));
        }

        public Traversable.Set<A> tailSet(A min, boolean min_included) {
            return tailSet(min, min_included);
        }

        public Traversable.Set<A> descendingTailSet(A min, boolean min_included) {
            return descendingTailSet(min, min_included);
        }

        public Traversable.Set.Indexed<A> subList(int l, int r) {
            if((l < 0) || (r > count) || (l > r))
                throw new IndexOutOfBoundsException("Indices are out of bounds");
            return s.subList(this.l + l, this.l + r);
        }

        public ResettableListIterator<A> listIterator() {
            return subListIterator(0, 0, count);
        }

        public ResettableListIterator<A> listIterator(int from) {
            return subListIterator(0, from, count);
        }

        public ResettableListIterator<A> subListIterator(int l, int r) {
            return subListIterator(l, l, r);
        }

        public ResettableListIterator<A> subListIterator(int l, int from, int r) {
            if((l < 0) || (r > count) || (l > r))
                throw new IndexOutOfBoundsException("Indices are out of bounds");
            if((from < l) || (from > r))
                throw new IndexOutOfBoundsException(
                        "From-Index is out of bounds");
            if(f != Selector.Accepter)
                throw new RuntimeException("Illegal casting discovered");
            return new ArrayIterator(s, this.l + l, this.l + from, this.l + r,
                                     reverseOrder);
        }

        public ResettableIterator<A> iterator() {
            return subSetIterator(null);
        }

        public ResettableIterator<A> descendingIterator() {
            return descendingSubSetIterator(null);
        }

        public ResettableIterator<A> subSetIterator(Selector<A> g) {
            if(g != null)
                return new ArrayIterator(s, new Selector.Conjunctor<A>(
                        f == Selector.Accepter
                        ? new Selector.RangeSelector<A>(
                        s.get(l), true, s.get(r - 1), true, s.comparator())
                        : f, g), reverseOrder);
            else if(f != Selector.Accepter)
                return new ArrayIterator(s, f, reverseOrder);
            else
                return new ArrayIterator(s, 0, l, r, reverseOrder);
        }

        public ResettableIterator<A> descendingSubSetIterator(Selector<A> g) {
            if(g != null)
                return new ArrayIterator(s, new Selector.Conjunctor<A>(
                        f == Selector.Accepter
                        ? new Selector.RangeSelector<A>(
                        s.get(l), true, s.get(r - 1), true, s.comparator())
                        : f, g), !reverseOrder);
            else if(f != Selector.Accepter)
                return new ArrayIterator(s, f, !reverseOrder);
            else
                return new ArrayIterator(s, 0, l, r, !reverseOrder);
        }

        public ResettableIterator<A> subSetIterator(A fromElement, A toElement) {
            return subSetIterator(fromElement, true, toElement, false);
        }

        public ResettableIterator<A> descendingSubSetIterator(A fromElement,
                                                              A toElement) {
            return descendingSubSetIterator(fromElement, true, toElement, false);
        }

        public ResettableIterator<A> subSetIterator(A min, boolean min_included,
                                                    A max, boolean max_included) {
            return subSetIterator(new Selector.RangeSelector<A>(
                    min, min_included, max, max_included, s.comparator()));
        }

        public ResettableIterator<A> descendingSubSetIterator(A min,
                                                              boolean min_included,
                                                              A max,
                                                              boolean max_included) {
            return descendingSubSetIterator(new Selector.RangeSelector<A>(
                    min, min_included, max, max_included, s.comparator()));
        }

        public ResettableIterator<A> headSetIterator(A toElement) {
            return headSetIterator(toElement, false);
        }

        public ResettableIterator<A> descendingHeadSetIterator(A toElement) {
            return descendingHeadSetIterator(toElement, false);
        }

        public ResettableIterator<A> headSetIterator(A max, boolean max_included) {
            return subSetIterator(new Selector.HalfRangeSelector<A>(
                    max, max_included, Selector.LEFT, s.comparator()));
        }

        public ResettableIterator<A> descendingHeadSetIterator(A max,
                                                               boolean max_included) {
            return descendingSubSetIterator(new Selector.HalfRangeSelector<A>(
                    max, max_included, Selector.LEFT, s.comparator()));
        }

        public ResettableIterator<A> tailSetIterator(A fromElement) {
            return tailSetIterator(fromElement, true);
        }

        public ResettableIterator<A> descendingTailSetIterator(A fromElement) {
            return descendingTailSetIterator(fromElement, true);
        }

        public ResettableIterator<A> tailSetIterator(A min, boolean min_included) {
            return subSetIterator(new Selector.HalfRangeSelector<A>(
                    min, min_included, Selector.RIGHT, s.comparator()));
        }

        public ResettableIterator<A> descendingTailSetIterator(A min,
                                                               boolean min_included) {
            return descendingSubSetIterator(new Selector.HalfRangeSelector<A>(
                    min, min_included, Selector.RIGHT, s.comparator()));
        }

        public IBBTree<A> clone() {
            IBBTree<A> r = new IBBTree(
                    s.comparator() != s ? s.comparator() : null, reverseOrder);
            for(A x : this)
                r.add(x);
            return r;
        }

        public int compareTo(OrderedSet<A> b) {
            return compareTo(b, s.comp);
        }

        public int compareTo(OrderedSet<A> b, Comparator<? super A> c) {
            if(b == null)
                return 1;
            if(this == b)
                return 0;
            if(b instanceof Final)
                return -b.compareTo(this, c);
            if(b instanceof Traversable) {
                TraversalActionPerformer.NavigableSetComparator<A> sc =
                        TraversalActionPerformer.NavigableSetComparator.
                        getThreadLocal(c);
                if(((Traversable<A>)b).traverse(IN_ORDER, sc, this) == 0)
                    return first() == null ? 0 : 1;
                if((sc.c == 0) && (sc.last != null) && (higher(sc.last) != null))
                    return 1;
                return -sc.c;
            } else {
                TraversalActionPerformer.NavigableSetComparator<A> sc =
                        TraversalActionPerformer.NavigableSetComparator.
                        getThreadLocal(c);
                if(traverse(IN_ORDER, sc, b) == 0)
                    return b.first() == null ? 0 : -1;
                if((sc.c == 0) && (sc.last != null) && (b.higher(sc.last) != null))
                    return -1;
                return sc.c;
            }
        }

        public boolean equals(Object o) {
            if(o == null)
                return false;
            if(o == this)
                return true;
            if(o instanceof OrderedSet)
                return compareTo((OrderedSet<A>)o) == 0;
            if(o instanceof Set)
                return this.containsAll((Set)o) && ((Set)o).containsAll(this);
            else
                return false;
        }

        public int hashCode() {
            int h = 0;
            for(A x : this)
                h += x.hashCode();
            return h;
        }

        public String toString() {
            return toStringBuilder(null).toString();
        }

        public StringBuilder toStringBuilder(StringBuilder sb) {
            if(sb == null)
                sb = new StringBuilder();
            sb.append('{');
            int i = 0;
            for(A x : this) {
                if(i != 0)
                    sb.append(',');
                else
                    i++;
                sb.append(x.toString());
            }
            sb.append('}');
            return sb;
        }

        public void writeToStream(Writer w) throws IOException {
            if(w == null)
                return;
            w.write('{');
            int i = 0;
            for(A x : this) {
                if(i != 0)
                    w.write(',');
                else
                    i++;
                if(x instanceof Writeable)
                    ((Writeable)x).writeToStream(w);
                else
                    w.write(x.toString());
            }
            w.write('}');
        }

        public int compare(A x, A y) {
            return reverseOrder ? -s.compare(x, y) : s.compare(x, y);
        }
        public <B> int asymmetricCompare(A x, B y) {
            return reverseOrder ? -s.asymmetricCompare(x, y) : s.asymmetricCompare(x, y);
        }

        public Comparator<? super A> comparator() {
            if(reverseOrder)
                return Utils.reverseComparator(s.comparator());
            else
                return s.comparator();
        }

        public boolean selects(A x) {
            return f.selects(x) && s.selects(x);
        }

        public A lowerBound() {
            return first();
        }

        public A upperBound() {
            return last();
        }

        public int traverse(int order, TraversalActionPerformer<? super A> p) {
            return traverse(order, p, null);
        }

        public int traverse(int order, TraversalActionPerformer<? super A> p,
                            Object o) {
            if((p == null) || (count == 0))
                return 0;
            return traverseRange(order, p, o, 0, count);
        }

        public int traverseSelected(int order,
                                    TraversalActionPerformer<? super A> p,
                                    Selector<A> s) {
            return traverseSelected(order, p, null, s);
        }

        public int traverseSelected(int order,
                                    TraversalActionPerformer<? super A> p,
                                    Object o, Selector<A> s) {
            if((p == null) || (count == 0))
                return 0;
            return traverseSelectedRange(order, p, null, s, 0, count);
        }

        public int traverseRange(int order,
                                 TraversalActionPerformer<? super A> p, int l,
                                 int r) {
            return traverseSelectedRange(order, p, null, null, l, r);
        }

        public int traverseRange(int order,
                                 TraversalActionPerformer<? super A> p, Object o,
                                 int l, int r) {
            return traverseSelectedRange(order, p, o, null, l, r);
        }

        public int traverseSelectedRange(int order,
                                         TraversalActionPerformer<? super A> p,
                                         Selector<A> s, int l, int r) {
            return traverseSelectedRange(order, p, null, s, l, r);
        }

        public int traverseSelectedRange(int order,
                                         TraversalActionPerformer<? super A> p,
                                         Object o, Selector<A> s, int l, int r) {
            if((l < 0) || (r > count) || (l > r))
                throw new IndexOutOfBoundsException("Indices are out of bounds");
            if((p == null) || (count == 0))
                return 0;
            return f != Accepter
                   ? this.s.traverseSelectedRange(
                    reverseOrder ? order ^ REVERSE_ORDER : order, p,
                    new Selector.Conjunctor<A>(f,
                                               reverseOrder ? new Selector.Reverser<A>(
                    s) : s),
                    this.l + l, this.l + r)
                   : this.s.traverseSelectedRange(
                    reverseOrder ? order ^ REVERSE_ORDER : order, p,
                    reverseOrder ? new Selector.Reverser<A>(s) : s,
                    this.l + l, this.l + r);
        }

        public int traverseRange(int order,
                                 TraversalActionPerformer<? super A> p, A l, A r) {
            return traverseSelectedRange(order, p, null, null, l, true, r,
                                         false);
        }

        public int traverseRange(int order,
                                 TraversalActionPerformer<? super A> p,
                                 Object o, A l, A r) {
            return traverseSelectedRange(order, p, o, null, l, true, r, false);
        }

        public int traverseRange(int order,
                                 TraversalActionPerformer<? super A> p,
                                 A l, boolean left_included, A r,
                                 boolean right_included) {
            return traverseSelectedRange(order, p, null, null, l, left_included,
                                         r, right_included);
        }

        public int traverseRange(int order,
                                 TraversalActionPerformer<? super A> p,
                                 Object o, A l, boolean left_included, A r,
                                 boolean right_included) {
            return traverseSelectedRange(order, p, o, null, l, left_included, r,
                                         right_included);
        }

        public int traverseSelectedRange(int order,
                                         TraversalActionPerformer<? super A> p,
                                         Selector<A> s, A l, A r) {
            return traverseSelectedRange(order, p, null, s, l, true, r, false);
        }

        public int traverseSelectedRange(int order,
                                         TraversalActionPerformer<? super A> p,
                                         Object o, Selector<A> s, A l, A r) {
            return traverseSelectedRange(order, p, o, s, l, true, r, false);
        }

        public int traverseSelectedRange(int order,
                                         TraversalActionPerformer<? super A> p,
                                         Selector<A> s, A l,
                                         boolean left_included, A r,
                                         boolean right_included) {
            return traverseSelectedRange(order, p, null, s, l, left_included, r,
                                         right_included);
        }

        public int traverseSelectedRange(int order,
                                         TraversalActionPerformer<? super A> p,
                                         Object o, Selector<A> s, A l,
                                         boolean left_included, A r,
                                         boolean right_included) {
            if((p == null) || (count == 0))
                return 0;
            int sl, sr;
            if(s != null) {
                sl = left_included ? indexOfCeiled(l) : indexOfHigher(l);
                sr = right_included ? indexOfFloored(r) : indexOfLower(r);
                if(sl < 0)
                    sl = 0;
                if((sr < 0) || (sr >= count))
                    sr = count - 1;
                if(sl > sr)
                    return 0;
            } else {
                sl = 0;
                sr = count - 1;
            }
            return traverseSelectedRange(order, p, o, s, sl, sr + 1);
        }
//        public int traverseComparableRange(int order,TraversalActionPerformer<? super A> p,
//                        Comparable<? super A> l,Comparable<? super A> r) {
//            return(traverseSelectedComparableRange(order,p,null,null,l,true,r,false));
//        }
//        public int traverseComparableRange(int order,TraversalActionPerformer<? super A> p,
//                        Object o,Comparable<? super A> l,Comparable<? super A> r) {
//            return(traverseSelectedComparableRange(order,p,o,null,l,true,r,false));
//        }
//        public int traverseComparableRange(int order,TraversalActionPerformer<? super A> p,
//                        Comparable<? super A> l,boolean left_included,
//                                Comparable<? super A> r,boolean right_included) {
//            return(traverseSelectedComparableRange(order,p,null,null,
//                                                l,left_included,r,right_included));
//        }
//        public int traverseComparableRange(int order,TraversalActionPerformer<? super A> p,
//                        Object o,Comparable<? super A> l,boolean left_included,
//                                Comparable<? super A> r,boolean right_included) {
//            return(traverseSelectedComparableRange(order,p,o,null,
//                                                l,left_included,r,right_included));
//        }
//        public int traverseSelectedComparableRange(int order,TraversalActionPerformer<? super A> p,
//                        Selector<A> s,Comparable<? super A> l,Comparable<? super A> r) {
//            return(traverseSelectedComparableRange(order,p,null,s,l,true,r,false));
//        }
//        public int traverseSelectedComparableRange(int order,TraversalActionPerformer<? super A> p,
//                        Object o,Selector<A> s,Comparable<? super A> l,Comparable<? super A> r) {
//            return(traverseSelectedComparableRange(order,p,o,s,l,true,r,false));
//        }
//        public int traverseSelectedComparableRange(int order,TraversalActionPerformer<? super A> p,
//                        Selector<A> s,Comparable<? super A> l,boolean left_included,
//                                Comparable<? super A> r,boolean right_included) {
//            return(traverseSelectedComparableRange(order,p,null,s,
//                                                l,left_included,r,right_included));
//        }
//        public int traverseSelectedComparableRange(int order,TraversalActionPerformer<? super A> p,
//                        Object o,Selector<A> s,Comparable<? super A> l,boolean left_included,
//                                Comparable<? super A> r,boolean right_included) {
//            if((p==null)||(count==0))
//                return(0);
//            int sl,sr;
//            if(s!=null) {
//                sl = left_included?indexOfCeiledComparable(l):indexOfHigherComparable(l);
//                sr = right_included?indexOfFlooredComparable(r):indexOfLowerComparable(r);
//                if(sl<0)
//                    sl = 0;
//                if((sr<0)||(sr>=count))
//                    sr = count-1;
//                if(sl>sr)
//                    return(0);
//            } else {
//                sl = 0;
//                sr = count-1;
//            }
//            return(traverseSelectedRange(order,p,o,s,sl,sr+1));
//        }
//        public <B> int traverseMatchingRange(int order,TraversalActionPerformer<? super A> p,
//                        B l,B r) {
//            return(traverseSelectedMatchingRange(order,p,null,null,l,true,r,false));
//        }
//        public <B> int traverseMatchingRange(int order,TraversalActionPerformer<? super A> p,
//                        Object o,B l,B r) {
//            return(traverseSelectedMatchingRange(order,p,o,null,l,true,r,false));
//        }
//        public <B> int traverseMatchingRange(int order,TraversalActionPerformer<? super A> p,
//                        B l,boolean left_included,B r,boolean right_included) {
//            return(traverseSelectedMatchingRange(order,p,null,null,
//                                                l,left_included,r,right_included));
//        }
//        public <B> int traverseMatchingRange(int order,TraversalActionPerformer<? super A> p,
//                        Object o,B l,boolean left_included,B r,boolean right_included) {
//            return(traverseSelectedMatchingRange(order,p,o,null,
//                                                l,left_included,r,right_included));
//        }
//        public <B> int traverseSelectedMatchingRange(int order,TraversalActionPerformer<? super A> p,
//                        Selector<A> s,B l,B r) {
//            return(traverseSelectedMatchingRange(order,p,null,s,l,true,r,false));
//        }
//        public <B> int traverseSelectedMatchingRange(int order,TraversalActionPerformer<? super A> p,
//                        Object o,Selector<A> s,B l,B r) {
//            return(traverseSelectedMatchingRange(order,p,o,s,l,true,r,false));
//        }
//        public <B> int traverseSelectedMatchingRange(int order,TraversalActionPerformer<? super A> p,
//                        Selector<A> s,B l,boolean left_included,B r,boolean right_included) {
//            return(traverseSelectedMatchingRange(order,p,null,s,
//                                                l,left_included,r,right_included));
//        }
//        public <B> int traverseSelectedMatchingRange(int order,TraversalActionPerformer<? super A> p,
//                        Object o,Selector<A> s,B l,boolean left_included,B r,boolean right_included) {
//            if((p==null)||(count==0))
//                return(0);
//            int sl,sr;
//            if(s!=null) {
//                sl = left_included?indexOfCeiledMatch(l):indexOfHigherMatch(l);
//                sr = right_included?indexOfFlooredMatch(r):indexOfLowerMatch(r);
//                if(sl<0)
//                    sl = 0;
//                if((sr<0)||(sr>=count))
//                    sr = count-1;
//                if(sl>sr)
//                    return(0);
//            } else {
//                sl = 0;
//                sr = count-1;
//            }
//            return(traverseSelectedRange(order,p,o,s,sl,sr+1));
//        }

        public int emptyTo(TraversalActionPerformer<? super A> p) {
            return emptyTo(p, null);
        }

        public int emptyTo(TraversalActionPerformer<? super A> p, Object o) {
            if(p == null)
                throw new NullPointerException("Performer is null");
            if(size() == 0)
                return 0;
            throw new UnsupportedOperationException(
                    "Modifications are not supported");
        }
    }

    public int emptyTo(TraversalActionPerformer<? super A> p) {
        return emptyTo(p, null);
    }

    public int emptyTo(TraversalActionPerformer<? super A> p, Object o) {
        if(p == null)
            throw new NullPointerException("Performer is null");
        if(size() == 0)
            return 0;
        throw new UnsupportedOperationException(
                "Modifications are not supported");
    }
}
