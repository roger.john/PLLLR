/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bbtree;

import java.util.Collections;
import java.util.Comparator;

/**
 *
 * @author rj
 */
public class Utils {

    private Utils() {
    }

    public static class Comparer<A> implements Comparable<A> {

        private A x;
        private Comparator<? super A> c;

        public Comparer(A x, Comparator<? super A> c) {
            this.x = x;
            this.c = c;
        }

        public A getX() {
            return (x);
        }

        public A setX(A x) {
            A t = this.x;
            this.x = x;
            return (t);
        }

        public Comparator<? super A> getComparator() {
            return (c);
        }

        public Comparator<? super A> setComparator(Comparator<? super A> c) {
            Comparator<? super A> t = this.c;
            this.c = c;
            return (t);
        }

        public Comparer<A> set(A x, Comparator<? super A> c) {
            this.x = x;
            this.c = c;
            return (this);
        }

        public int compareTo(A y) {
            return (c.compare(x, y));
        }
    }
    public static final Comparator<Object> HashCodeComparator = new Comparator<Object>() {

        public int compareUnsigned(int x, int y) {
            if((x & Integer.MIN_VALUE) != 0)
                if((y & Integer.MIN_VALUE) != 0)
                    return (x - y);
                else
                    return (1);
            else if((y & Integer.MIN_VALUE) != 0)
                return (-1);
            else
                return (x - y);
        }

        public int compare(Object x, Object y) {
            return (compareUnsigned(System.identityHashCode(x), System.
                    identityHashCode(y)));
        }
    };

    public interface AsymmetricComparator<A> {

        public <B> int asymmetricCompare(A x, B y);
    }

    public interface AsymmetricComparable<A> {

        public int asymmetricCompareTo(A x);
    }

    public interface KeyedElementFactory<A, B> {

        public A createElement(B x);

        public B extractKey(A x);

        public A lastCreated();

        public void reset();

        public static class Cascaded<C, D, E> implements
                KeyedElementFactory<C, E> {

            public final KeyedElementFactory<C, D> mainFactory;
            public final KeyedElementFactory<D, E> subFactory;

            public Cascaded(KeyedElementFactory<C, D> mainFactory,
                            KeyedElementFactory<D, E> subFactory) {
                if(mainFactory == null)
                    throw new NullPointerException("main factory");
                if(subFactory == null)
                    throw new NullPointerException("sub factory");
                this.mainFactory = mainFactory;
                this.subFactory = subFactory;
            }

            public C createElement(E x) {
                return mainFactory.createElement(subFactory.createElement(x));
            }

            public E extractKey(C x) {
                return subFactory.extractKey(mainFactory.extractKey(x));
            }

            public C lastCreated() {
                return mainFactory.lastCreated();
            }

            public void reset() {
                subFactory.reset();
                mainFactory.reset();
            }
        }
    }
    public static final NaturalComparator NATURAL_COMPARATOR = new NaturalComparator();

    public static class NaturalComparator<B> implements Comparator<B>,
                                                        AsymmetricComparator<B> {

        private NaturalComparator() {
        }

        public int compare(B x, B y) {
            if(x == null)
                return y == null ? 0 : -1;
            return ((Comparable<B>)x).compareTo(y);
        }

        public <C> int asymmetricCompare(B x, C y) {
            if(x == null)
                return y == null ? 0 : -1;
            return ((AsymmetricComparable<C>)x).asymmetricCompareTo(y);
        }
    }
    public static final NaturalComparator REVERSE_NATURAL_COMPARATOR = new ReverseNaturalComparator();

    public static class ReverseNaturalComparator<B> extends NaturalComparator<B> {

        private ReverseNaturalComparator() {
            super();
        }

        public int compare(B x, B y) {
            if(x == null)
                return y == null ? 0 : 1;
            return -((Comparable<B>)x).compareTo(y);
        }

        public <C> int asymmetricCompare(B x, C y) {
            if(x == null)
                return y == null ? 0 : 1;
            return -((AsymmetricComparable<C>)x).asymmetricCompareTo(y);
        }
    }

    private static class ReverseComparator<B, C> implements Comparator<B>,
                                                            AsymmetricComparator<B> {

        private final Comparator<? super B> comp;

        private ReverseComparator(Comparator<? super B> comp) {
            if(comp != null)
                this.comp = comp;
            else
                this.comp = NATURAL_COMPARATOR;
        }

        public int compare(B x, B y) {
            return -comp.compare(x, y);
        }

        public <C> int asymmetricCompare(B x, C y) {
            if(x == null)
                return y == null ? 0 : 1;
            return -((AsymmetricComparator<B>)comp).asymmetricCompare(x, y);
        }
    }

    public static <B> Comparator<B> wrapComparator(Comparator<B> comp,
                                                   boolean reverseOrder) {
        if(comp != null)
            if(reverseOrder)
                return new ReverseComparator<B, Object>(comp);
            else
                return comp;
        else if(reverseOrder)
            return Utils.REVERSE_NATURAL_COMPARATOR;
        else
            return Utils.NATURAL_COMPARATOR;
    }

    public static <B> Comparator<B> reverseComparator(Comparator<B> comp) {
        return comp != null ? new ReverseComparator<B, Object>(comp) : REVERSE_NATURAL_COMPARATOR;
    }
}
