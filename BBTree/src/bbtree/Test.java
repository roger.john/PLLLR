/*
 * Test.java
 *
 * Created on 21. Juli 2006, 12:34
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package bbtree;

import java.util.Collection;
import java.util.TreeSet;

/**
 *
 * @author RJ
 */
public class Test {

    private static final int SETS_TO_TEST = 3; // number of sets to test
    private static final int DOUBLING_RUNS = 7; // standard number of times to double set size
    private static final int REPETITION_RUNS = 3; // standard amount of repetitions of each test
    private static final int START_SET_SIZE = 1024; // standard initial set size
    
    /** Creates a new instance of Test */
    public Test() {
    }

    public static String padRight(String s,int toLength,char padChar) {
        if(toLength<=0)
            return(s!=null?s:"");
        int l = s!=null?s.length():0;
        if(l>=toLength)
            return(s);
        char[] pad = new char[toLength-l];
        for(int i=0;i<pad.length;i++)
            pad[i] = padChar;
        if(l==0)
            return(new String(pad));
        else
            return(s+(new String(pad)));
    }
    
    public static void showHelp() {
        System.out.println("Set speed scaling test");
        System.out.println("Usage: java -jar BBtree.jar [options]");
        System.out.println("where options are one or more of the following:");
        System.out.println("-?    : show this and exit");
        System.out.println("-s<n> : use <n> as seed of the random number generator");
        System.out.println("-i<n> : set the initial set size to <n>, default is "
                +START_SET_SIZE);
        System.out.println("-d<n> : set the number of times to double set size to <n>, default is "
                +DOUBLING_RUNS);
        System.out.println("-r<n> : set the number of times to repeat tests to <n>, default is "
                +REPETITION_RUNS);
        System.out.println();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        long seed = System.currentTimeMillis(),s,e;
        java.util.Random r = new java.util.Random();
        int k;
        java.util.Collection<Integer> t, tt[] = new java.util.Collection[SETS_TO_TEST];
        String tn[] = new String[SETS_TO_TEST];
        int setSize = START_SET_SIZE,doublingRuns = DOUBLING_RUNS,
            repetitionRuns = REPETITION_RUNS,maxClassStringLength = 0;

        //option handling
        for(int i=0;i<args.length;i++) {
            if(args[i].startsWith("-")&&(args[i].length()>1)) {
                switch(args[i].charAt(1)) {
                    case '?':
                    case 'h':   showHelp();
                                return;
                    case 's':   if(args[i].length()>2) {
                                    try {
                                        seed = Long.parseLong(args[i].substring(2));
                                    } catch (NumberFormatException nfe) {
                                        System.out.println("Expected decimal number"
                                                +" for random seed, got: "
                                                +args[i].substring(2));
                                        return;
                                    }
                                } else
                                    seed = System.currentTimeMillis();
                                break;
                    case 'i':   try {
                                    setSize = Integer.parseInt(args[i].substring(2));
                                } catch (NumberFormatException nfe) { setSize = 0; }
                                if(setSize<=0) {
                                    System.out.println("Expected positive decimal number"
                                            +" for initial set size, got: "
                                            +args[i].substring(2));
//                                    System.out.println("Setting to "+
//                                            START_SIZE);
//                                    setSize = START_SIZE;
                                    return;
                                }
                                break;
                    case 'd':   try {
                                    doublingRuns = Integer.parseInt(args[i].substring(2));
                                } catch (NumberFormatException nfe) { doublingRuns = -1; }
                                if(doublingRuns<0) {
                                    System.out.println("Expected non negative decimal number"
                                            +" for number of times to double set size, got: "
                                            +args[i].substring(2));
//                                    System.out.println("Setting to "+DOUBLING_RUNS);
//                                    doublingRuns = DOUBLING_RUNS;
                                    return;
                                }
                                break;
                    case 'r':   try {
                                    repetitionRuns = Integer.parseInt(args[i].substring(2));
                                } catch (NumberFormatException nfe) { repetitionRuns = 0; }
                                if(repetitionRuns<0) {
                                    System.out.println("Expected positive decimal number"
                                            +" for number of times to repeat test, got: "
                                            +args[i].substring(2));
//                                    System.out.println("Setting to"+REPETITION_RUNS);
//                                    doublingRuns = REPETITION_RUNS;
                                    return;
                                }
                                break;
                    default:    System.out.println("Illegal argument: "+args[i]);
                                return;
                }
            } else {
                System.out.println("Illegal argument: "+args[i]);
                return;
            }
        }
        r.setSeed(seed);
        //sets to test
        tt[0] = new java.util.TreeSet<Integer>();
        tt[1] = new BBTree<Integer>();
        tt[2] = new IBBTree<Integer>();

        for(int i=0;i<SETS_TO_TEST;i++) {
            tn[i] = tt[i].getClass().getSimpleName();
            k = tn[i].length();
            if(k>maxClassStringLength)
                maxClassStringLength = k;
        }
        for(int i=0;i<SETS_TO_TEST;i++)
            tn[i] = padRight(tn[i],maxClassStringLength,' ');
        double[][] sa = new double[SETS_TO_TEST][doublingRuns+1],
                   ss = new double[SETS_TO_TEST][doublingRuns+1],
                   sd = new double[SETS_TO_TEST][doublingRuns+1];
        for(int x=0;x<=doublingRuns;x++) {
            for(int y=0;y<SETS_TO_TEST*repetitionRuns;y++) {
                System.out.println("Using "+tn[y%SETS_TO_TEST]+"...");
                t = tt[y%SETS_TO_TEST];
                r.setSeed(seed);
                System.out.println("Checking tree for "+setSize+" entries...");
                System.out.print(" Now adding "+setSize+" random numbers...");
                s = System.nanoTime();
                for(int i=1;i<=setSize;i++) {
                    k = r.nextInt();
                    t.add(new Integer(k));
                }
                e = System.nanoTime();
                sa[y%SETS_TO_TEST][x] += ((double)(e-s))/(setSize*1000.0);
                System.out.printf("average %.3fus per entry\r\n",(((double)(e-s))/(setSize*1000.0)));
                System.out.println(" Current item count is "+t.size()+", max search depth is "+(t instanceof OrderedSet?((OrderedSet<Integer>)t).maxSearchDepth():"?"));
                System.out.print(" Now searching...");
                r.setSeed(seed);
                s = System.nanoTime();
                for(int i=1;i<=setSize;i++) {
                    k = r.nextInt();
                    if(!t.contains(new Integer(k)))
                        System.out.println("error: entry "+i+" not found");
                }
                e = System.nanoTime();
                ss[y%SETS_TO_TEST][x] += ((double)(e-s))/(setSize*1000.0);
                System.out.printf("average %.3fus per entry\r\n",(((double)(e-s))/(setSize*1000.0)));
                System.out.print(" Now deleting...");
                r.setSeed(seed);
                s = System.nanoTime();
                for(int i=1;i<=setSize;i++) {
                    k = r.nextInt();
                    t.remove(new Integer(k));
                }
                e = System.nanoTime();
                sd[y%SETS_TO_TEST][x] += ((double)(e-s))/(setSize*1000.0);
                System.out.printf("average %.3fus per entry\r\n",(((double)(e-s))/(setSize*1000.0)));
                //System.out.println(" Remaining item count is "+t.size()+", max search depth is "+t.maxSearchDepth());
                System.out.print(" Destroying tree...");
                t.clear();
                System.out.println("ok");
            }
            System.out.println("Done with "+setSize+" entries.");
            for(int y=0;y<SETS_TO_TEST;y++) {
                sa[y][x] /= repetitionRuns;
                ss[y][x] /= repetitionRuns;
                sd[y][x] /= repetitionRuns;
            }
            setSize *= 2;
        }
        setSize >>>= (doublingRuns+1);
        double[][] sg = new double[SETS_TO_TEST][3];
        for(int x=0;x<=doublingRuns;x++) {
            for(int i=0;i<SETS_TO_TEST;i++) {
                System.out.printf(tn[i]+" add    %d: %.3fus average per entry\r\n",setSize,sa[i][x]);
                sg[i][0] += sa[i][x];
                System.out.printf(tn[i]+" search %d: %.3fus average per entry\r\n",setSize,ss[i][x]);
                sg[i][1] += ss[i][x];
                System.out.printf(tn[i]+" delete %d: %.3fus average per entry\r\n",setSize,sd[i][x]);
                sg[i][2] += sd[i][x];
            }
            setSize *= 2;
        }
        for(int i=0;i<SETS_TO_TEST;i++) {
            sg[i][0] /= (doublingRuns+1);
            sg[i][1] /= (doublingRuns+1);
            sg[i][2] /= (doublingRuns+1);
            System.out.printf(tn[i]+" add    total: %.3fus average per entry\r\n",sg[i][0]);
            System.out.printf(tn[i]+" search total: %.3fus average per entry\r\n",sg[i][1]);
            System.out.printf(tn[i]+" delete total: %.3fus average per entry\r\n",sg[i][2]);
        }

//        
//         OrderedSet<Integer> t = new BBTreeS<Integer>();
//         for(int k=1;k<=20;k++)
//            t.add(new Integer(k));
//        Selector<Integer> p = new Selector<Integer>() {
//            public boolean selects(Integer x) {
//                int y = x.intValue();
//                if(y==1)
//                    return(false);
//                if(y==2)
//                    return(true);
//                if((y%2)==0)
//                    return(false);
//                for(int i=3;i<=(y/2);i+=2)
//                    if((y%i)==0)
//                        return(false);
//                return(true);
//            }
//           };
//         t = ((BBTreeS<Integer>)t).copySubSet(p).toFinal();
//         for(Integer k : t)
//             System.out.print(k+" ");
//         System.out.println();
//         System.out.println(t);
//         Object[] ia = t.toArray();
//         System.out.println(ia);
//         t.toggleOrdering();
//         System.out.println(t);
//         ia = t.toArray();
//         System.out.println(ia);
//         t.toggleOrdering();
//         System.out.println(t.min()+"<=t<="+t.max());
//         for(int k=0;k<=20;k++) {
//            System.out.println("Floor("+k+")="+t.floor(new Integer(k)));
//            System.out.println("Ceiling("+k+")="+t.ceiling(new Integer(k)));
//            System.out.println("Next("+k+")="+t.next(new Integer(k)));
//            System.out.println("Previous("+k+")="+t.prev(new Integer(k)));
//         }
//         System.out.println(t.copySubSet(new Integer(3),new Integer(16),true,false));
//         for(Integer i:t)
//             System.out.println(i);
//         t.clear();
//         System.out.println(t);
//        Selector<Integer> m = new Selector<Integer>() {
//            public boolean selects(Integer x) {
//                int y = x.intValue()+1;
//                while((y>0)&&(y%2==0))
//                    y /=2;
//                return(y==1);
//            }
//           };
//           
//        System.out.println(t.subSet(p));
//        //System.out.println(t.subSet(s).add(4));
//        System.out.println(t.copySubSet(p));
//        System.out.println(t.subSet(p).subSet(new Integer(20),new Integer(50)).join(
//                t.copySubSet(p).subSet(new Integer(60),new Integer(83))));
//        System.out.println(t.subSet(p).intersect(t.subSet(m)));
//        for(Integer i:t.subSet(p).intersect(t.subSet(m)))
//             System.out.println(i);
//        System.out.println(t.clone().equals(t));
//        for(int i=20;i<256;i++)
//            System.out.print((char)i);
//        System.out.println();
    }
    
}
