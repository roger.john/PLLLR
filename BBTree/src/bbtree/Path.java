/*
 * Path.java
 *
 * Created on 12. Februar 2007, 10:02
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */
package bbtree;

import bbtree.Utils.AsymmetricComparable;
import bbtree.Utils.KeyedElementFactory;
import java.io.Serializable;
import java.util.*;

/**
 *
 * @author RJ
 */
public class Path<A> implements Cloneable, Comparator<A>, Serializable,
                                Comparable<Object>, AsymmetricComparable<Object>,
                                Iterable<A>,
                                bbtree.TraversalActionPerformer<A> {

    public Node<A> head = null, tail = null;

    /** Creates a new instance of Path */
    public Path() {
    }

    public Path<A> clone() {
        Path<A> r = new Path<A>();
        if(head == null)
            return (r);
        r.head = new Node<A>(head.x);
        r.tail = r.head;
        Node<A> t = head.next;
        while(t != null) {
            r.tail.next = new Node<A>(t.x);
            r.tail = r.tail.next;
            t = t.next;
        }
        return (r);
    }

    public Path<A> cloneBounded(int bound) {
        if(bound < 0)
            throw (new IllegalArgumentException(
                    getClass().getCanonicalName()
                    + ".cloneBounded: negative bound"));
        Path<A> r = new Path<A>();
        if((head == null) || (bound == 0))
            return (r);
        r.head = new Node<A>(head.x);
        r.tail = r.head;
        Node<A> t = head.next;
        bound--;
        while((t != null) && (bound > 0)) {
            r.tail.next = new Node<A>(t.x);
            r.tail = r.tail.next;
            t = t.next;
            bound--;
        }
        return (r);
    }

    public Path<A> bound(int bound) {
        if(bound < 0)
            throw (new IllegalArgumentException(getClass().getCanonicalName()
                                                + ".bound: negative bound"));
        if(head == null)
            return (this);
        if(bound == 0) {
            head = null;
            tail = null;
            return (this);
        }
        Node<A> t = head;
        bound--;
        while((t.next != null) && (bound > 0)) {
            t = t.next;
            bound--;
        }
        t.next = null;
        tail = t;
        return (this);
    }

    public Path<A> cloneReversed() {
        Path<A> r = new Path<A>();
        if(head == null)
            return (r);
        r.head = new Node<A>(head.x);
        r.tail = r.head;
        Node<A> t = head.next, l = null;
        while(t != null) {
            l = r.head;
            r.head = new Node<A>(t.x, l);
            t = t.next;
        }
        return (r);
    }

    public Path<A> reverse() {
        if(head == tail)
            return this;
        Node<A> u, v = head;
        while(head.next != null) {
            u = head.next;
            head.next = head.next.next;
            u.next = v;
            v = u;
        }
        tail = head;
        head = v;
        return this;
    }

    public int compare(A x, A y) {
        if(x == y)
            return (0);
        if(x == null)
            return (y != null ? -1 : 0);
        if(y == null)
            return (1);
        if(x.equals(y))
            return (0);
        if(x instanceof Comparable)
            return (((Comparable<A>)x).compareTo(y));
        if(y instanceof Comparable)
            return (-((Comparable<A>)x).compareTo(x));
        return (x.hashCode() - x.hashCode());
    }

    public boolean equals(Object o) {
        return ((o == this) || (compareTo(o) == 0));
    }

    public int compareTo(Object o) {
        if(o == null)
            return (1);
        if(o instanceof Path)
            return compareToNode(((Path<A>)o).head);
        if(o instanceof Node)
            return compareToNode((Node<A>)o);
        if(o instanceof Object[])
            return compareToArray((A[])o);
        if(o instanceof Iterable)
            return compareToIterable((Iterable<A>)o);
        throw new ClassCastException(this.getClass().getCanonicalName()
                                     + " cannot be compared to " + o.getClass().
                getCanonicalName());
    }

    public int compareToNode(Node<A> p) {
        if(p == null)
            return (head != null ? 1 : 0);
        if(head == null)
            return (-1);
        Node<A> s = head, t = p;
        int c = 0;
        while((s != null) && (t != null)) {
            c = (s.x != null) && (t.x != null) ? compare(s.x, t.x) : (s.x != null ? 1 : (t.x != null ? -1 : 0));
            if(c != 0)
                return (c);
            s = s.next;
            t = t.next;
        }
        if(s == null)
            return (t == null ? 0 : -1);
        return (1);
    }

    public int compareToIterable(Iterable<A> p) {
        if(p == null)
            return 0;
        java.util.Iterator<A> i = p.iterator();
        if(i == null)
            return (head == null ? 0 : 1);
        Node<A> s = head;
        int c = 0;
        A t;
        while((s != null) && i.hasNext()) {
            t = i.next();
            c = (s.x != null) && (t != null) ? compare(s.x, t) : (s.x != null ? 1 : (t != null ? -1 : 0));
            if(c != 0)
                return (c);
            s = s.next;
        }
        if(s == null)
            return (i.hasNext() ? -1 : 0);
        return 1;
    }

    public int compareToArray(A[] p) {
        if(p == null)
            return (1);
        if(head == null)
            return (p.length == 0 ? 0 : -1);
        Node<A> s = head;
        int c = 0, i = 0;
        while((s != null) && (i < p.length)) {
            c = (s.x != null) && (p[i] != null) ? compare(s.x, p[i]) : (s.x != null ? 1 : (p[i] != null ? -1 : 0));
            if(c != 0)
                return (c);
            s = s.next;
            i++;
        }
        if(s == null)
            return (i < p.length ? -1 : 0);
        return (1);
    }

    public int asymmetricCompareTo(Object o) {
        return compareTo(o);
    }

    public void clear() {
        head = tail = null;
    }

    public Path<A> prepend(A x) {
        return (push(x));
    }

    public Path<A> push(A x) {
        if(head == null) {
            head = new Node<A>(x);
            tail = head;
        } else
            head = new Node<A>(x, head);
        return (this);
    }

    public Path<A> append(A x) {
        if(head == null) {
            head = new Node<A>(x);
            tail = head;
        } else {
            tail.next = new Node<A>(x, null);
            tail = tail.next;
        }
        return (this);
    }

    public Path<A> insertAfter(Node<A> w, A x) {
        if(head == null) {
            head = new Node<A>(x);
            tail = head;
        } else if(w == null)
            head = new Node<A>(x, head);
        else
            w.next = new Node<A>(x, w.next);
        return (this);
    }

    public A pop() {
        if(head == null)
            return (null);
        A r = head.x;
        head = head.next;
        if(head == null)
            tail = null;
        return (r);
    }

    public A peek() {
        return (head != null ? head.x : null);
    }

    public A first() {
        return (head != null ? head.x : null);
    }

    public A cutTail() {
        if(head == null)
            return (null);
        A r = null;
        if(head.next == null) {
            r = head.x;
            head = null;
            tail = null;
            return (r);
        }
        Node<A> t = head;
        while(t.next.next != null)
            t = t.next;
        r = t.next.x;
        t.next = null;
        tail = t;
        return (r);
    }

    public A last() {
        return (tail != null ? tail.x : null);
    }

    public Path<A> shift() {
        if(head == null)
            return (null);
        Path<A> r = new Path<A>();
        r.head = head.next;
        r.tail = r.head == null ? null : tail;
        return (r);
    }

    public boolean isEmpty() {
        return (head == null);
    }

    public boolean isSingle() {
        return ((head != null) && (head == tail));
    }

    public int length() {
        int r = 0;
        Node<A> t = head;
        while(t != null) {
            r++;
            t = t.next;
        }
        return (r);
    }

    public String toString() {
        if(head == null)
            return ("()");
        Node<A> t = head;
        StringBuilder s = new StringBuilder().append('(').append(t.x.toString());
        t = t.next;
        while(t != null) {
            s.append(" ");
            s.append(t.x);
            t = t.next;
        }
        return (s.append(')').toString());
    }

    public Object[] toArray() {
        Object[] r = new Object[length()];
        Node<A> t = head;
        int i = 0;
        while(t != null) {
            r[i++] = t.x;
            t = t.next;
        }
        return (r);
    }

    public <B> B[] toArray(B[] b) {
        int l = length();
        B[] r = b.length >= l ? b : (B[])java.lang.reflect.Array.newInstance(b.
                getClass().getComponentType(), l);
        Node<A> t = head;
        int i = 0;
        while(t != null) {
            r[i++] = (B)t.x;
            t = t.next;
        }
        return (r);
    }

    public Path<A> prepend(Path<A> x) {
        if((x != null) && (x.tail != null)) {
            x.tail.next = head;
            head = x.head;
        }
        return (this);
    }

    public Path<A> append(Path<A> x) {
        if(x != null)
            if(tail == null) {
                head = x.head;
                tail = x.tail;
            } else {
                tail.next = x.head;
                tail = x.tail;
            }
        return (this);
    }

    public Path<A> prepend(Node<A> x) {
        if(x != null) {
            Node<A> y = x;
            while(y.next != null) {
                y = y.next;
                if(y == x)
                    throw new IllegalArgumentException("argument is cyclic");
            }
            y.next = head;
            head = x;
        }
        return this;
    }

    public Path<A> append(Node<A> x) {
        if(x != null) {
            if(tail == null)
                head = x;
            else
                tail.next = x;
            tail = x;
            while(tail.next != null) {
                tail = tail.next;
                if(tail == x)
                    throw new IllegalArgumentException("argument is cyclic");
            }
        }
        return this;
    }

    public Path<A> prepend(Iterable<A> x) {
        if(x == null)
            return (this);
        synchronized(x) {
            java.util.Iterator<A> it = x.iterator();
            if(it == null)
                return (this);
            while(it.hasNext())
                prepend(it.next());
        }
        return (this);
    }

    public Path<A> append(Iterable<A> x) {
        if(x == null)
            return (this);
        synchronized(x) {
            if(x instanceof Traversable)
                ((Traversable<A>)x).traverse(IN_ORDER, this);
            else {
                java.util.Iterator<? extends A> it = x.iterator();
                if(it == null)
                    return (this);
                while(it.hasNext())
                    append(it.next());
            }
        }
        return (this);
    }

    public Path<A> prepend(A[] x) {
        if(x == null)
            return (this);
        for(A y : x)
            prepend(y);
        return this;
    }

    public Path<A> append(A[] x) {
        if(x == null)
            return (this);
        for(A y : x)
            append(y);
        return this;
    }

    public Path<A> appendSome(Iterable<? extends A> x, bbtree.Selector<A> s) {
        if(x == null)
            return (this);
        synchronized(x) {
            if(x instanceof Traversable)
                ((Traversable<A>)x).traverseSelected(IN_ORDER, this, s);
            else {
                if(s == null)
                    s = bbtree.Selector.Accepter;
                java.util.Iterator<? extends A> it = x.iterator();
                if(it == null)
                    return (this);
                A t;
                while(it.hasNext())
                    if(s.selects(t = it.next()))
                        append(t);
            }
        }
        return (this);
    }

    public Path<A> insertAfter(Node<A> w, Path<A> x) {
        if(x != null)
            if(head == null) {
                head = x.head;
                tail = x.tail;
            } else if(w == null)
                append(x);
            else {
                x.tail = w.next;
                w.next = x.head;
            }
        return (this);
    }

    public Path<A> insertAt(int index, A x) {
        if((index <= 0) || (head == null)) {
            head = new Node<A>(x, head);
            if(tail == null)
                tail = head;
            return (this);
        }
        Node<A> t = head;
        while((t.next != null) && (index > 1)) {
            t = t.next;
            index--;
        }
        t.next = new Node<A>(x, t.next);
        if(tail == t)
            tail = t.next;
        return (this);
    }

    public Path<A> insertAt(int index, Path<A> x) {
        if((x == null) || (x.head == null))
            return (this);
        if((index <= 0) || (head == null)) {
            x.tail.next = head;
            if(tail == null)
                tail = x.tail;
            else
                x.tail = tail;
            head = x.head;
            return (this);
        }
        Node<A> t = head;
        while((t.next != null) && (index > 1)) {
            t = t.next;
            index--;
        }
        if(t.next != null) {
            x.tail.next = t.next;
            x.tail = tail;
        } else
            tail = x.tail;
        t.next = x.head;
        return (this);
    }

    public A get(int index) {
        if(index < 0)
            return (null);
        Node<A> t = head;
        while((t != null) && (index > 0)) {
            t = t.next;
            index--;
        }
        if(t != null)
            return (t.x);
        return (null);
    }

    public A set(int index, A x) {
        if(index < 0)
            return (null);
        Node<A> t = head;
        A r = null;
        while((t != null) && (index > 0)) {
            t = t.next;
            index--;
        }
        if(t != null) {
            r = t.x;
            t.x = x;
            return (r);
        }
        return (null);
    }

    public A delete(int index) {
        if((index < 0) || (head == null))
            return (null);
        A r = null;
        if(index == 0) {
            r = head.x;
            head = head.next;
            return (r);
        }
        Node<A> t = head;
        while((t.next != null) && (index > 1)) {
            t = t.next;
            index--;
        }
        if(t.next != null) {
            r = t.next.x;
            t.next = t.next.next;
            if(t.next == null)
                tail = t;
        }
        return (r);
    }

    public java.util.Iterator<A> iterator() {
        return (new Iterator<A>(this));
    }

    public static Path<Character> valueOf(String s) {
        if(s == null)
            return (null);
        Path<Character> p = new Path<Character>();
        for(int i = 0; i < s.length(); i++)
            p.append(s.charAt(i));
        return (p);
    }

    public static <B extends Comparable<B>> Path<B> valueOf(B[] b) {
        if(b == null)
            return (null);
        Path<B> p = new Path<B>();
        for(int i = 0; i < b.length; i++)
            p.append(b[i]);
        return (p);
    }

    public static <B extends Comparable<B>> Path<B> valueOf(Iterable<B> b) {
        if(b == null)
            return (null);
        Path<B> p = new Path<B>();
        p.append(b);
        return (p);
    }

    private static class Iterator<A> implements java.util.Iterator<A> {

        private static final int HAS_NEXT = 1;
        private static final int HAS_STARTED = 2;
        private static final int HAS_REMOVED = 4;
        private Node<A> cn, pn, sn;
        private Path<A> cp;
        private int status;

        public Iterator(Path<A> p) {
            cp = p;
            cn = null;
            pn = null;
            if(p == null)
                sn = null;
            else
                sn = p.head;
            status = sn != null ? HAS_NEXT : 0;
        }

        public Iterator(Path.Node<A> n) {
            cp = null;
            sn = n;
            cn = null;
            pn = null;
            status = sn != null ? HAS_NEXT : 0;
        }

        public boolean hasNext() {
            return ((status & HAS_NEXT) != 0);
        }

        public A next() {
            if(!hasNext())
                throw (new java.util.NoSuchElementException(
                        "Path.Node.next: no more elements to iterate"));
            if((status & HAS_STARTED) == 0) {
                cn = sn;
                pn = null;
                status = HAS_STARTED;
                if(cn.next != null)
                    status |= HAS_NEXT;
            } else {
                pn = cn;
                cn = cn.next;
                status &= ~HAS_REMOVED;
                if(cn.next != null)
                    status |= HAS_NEXT;
                else
                    status &= ~HAS_NEXT;
            }
            return (cn.x);
        }

        public void remove() {
            if((status & HAS_REMOVED) != 0)
                throw (new IllegalStateException(
                        "Path.PathIterator.remove: element was already removed"));
            if((status & HAS_STARTED) == 0)
                throw (new IllegalStateException(
                        "Path.PathIterator.remove: iterations has not started yet"));
            if(pn != null) {
                pn.next = cn.next;
                cn = pn;
                status |= HAS_REMOVED;
                if(cn.next != null)
                    status |= HAS_NEXT;
                else if(cp != null) {
                    status &= ~HAS_NEXT;
                    cp.tail = cn;
                }
            } else if(cp != null) {
                cp.head = cn.next;
                status |= HAS_REMOVED;
                sn = cn.next;
                cn = null;
                status &= ~HAS_STARTED;
                if(sn != null)
                    status |= HAS_NEXT;
                else {
                    status &= ~HAS_NEXT;
                    cp.tail = null;
                }
            } else
                throw (new UnsupportedOperationException(
                        "Path.PathIterator.remove: can not remove first node of an anonymous chain"));
        }
    }

    public static class Node<A> implements Cloneable, Comparator<A>,
                                           Comparable<Node<? extends A>> {

        public A x;
        public Node<A> next = null;

        public Node(A x) {
            this.x = x;
        }

        public Node(A x, Node<A> next) {
            this.x = x;
            this.next = next;
        }

        public Node<A> clone() {
            Node<A> r, s = new Node<A>(x, next);
            r = s;
            while(s.next != null) {
                s.next = new Node<A>(s.next.x, s.next.next);
                s = s.next;
            }
            return (r);
        }

        public void finalize() {
            //System.out.println("Path.Node "+this+" ("+x+","+next+") is being finalized.");
        }

        public boolean equals(Object o) {
            if(o == null)
                return (false);
            if(this == o)
                return (true);
            if(o instanceof Node)
                return (compareTo((Node<A>)o) == 0);
            return (false);
        }

        public int compare(A x, A y) {
            if(x == y)
                return (0);
            if(x == null)
                return (y != null ? -1 : 0);
            if(y == null)
                return (1);
            if(x.equals(y))
                return (0);
            if(x instanceof Comparable)
                return (((Comparable<A>)x).compareTo(y));
            if(y instanceof Comparable)
                return (-((Comparable<A>)x).compareTo(x));
            return (x.hashCode() - x.hashCode());
        }

        public int compareTo(Node<? extends A> y) {
            if(y == null)
                return (1);
            return (compare(x, y.x));
        }
    }

    @Override
    public boolean perform(A x, int order, Object o) {
        append(x);
        return (false);
    }

    public static class Factory<B extends Comparable<B>> implements
            KeyedElementFactory<Path<B>, Object> {

        Path<B> last;

        public Path<B> createElement(Object x) {
            if(x == null)
                throw new NullPointerException("argument");
            if(x instanceof Path)
                last = new Path<B>().append((Path<B>)x);
            else if(x instanceof Node)
                last = new Path<B>().append((Node<B>)x);
            else if(x instanceof Object[])
                last = Path.valueOf((B[])x);
            else if(x instanceof Iterable)
                last = Path.valueOf((Iterable<B>)x);
            else
                throw new ClassCastException(Path.class.getCanonicalName()
                                             + " cannot be created from " + x.
                        getClass().getCanonicalName());
            return last;
        }

        public Path<B> lastCreated() {
            return last;
        }

        public void reset() {
            last = null;
        }

        public Object extractKey(Path<B> x) {
            return x;
        }
    }
}
